// 
// Decompiled by Procyon v0.5.36
// 

package com.sothawo.mapjfx.cache;

import java.net.Proxy;
import java.io.IOException;
import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.net.URLStreamHandler;
import java.util.Map;
import org.apache.log4j.Logger;
import java.net.URLStreamHandlerFactory;

public class CachingURLStreamHandlerFactory implements URLStreamHandlerFactory
{
    public static final String PROTO_HTTP = "http";
    public static final String PROTO_HTTPS = "https";
    private static final Logger logger;
    private final OfflineCache cache;
    private final Map<String, URLStreamHandler> handlers;
    
    CachingURLStreamHandlerFactory(final OfflineCache cache) {
        this.handlers = new ConcurrentHashMap<String, URLStreamHandler>();
        this.cache = cache;
        this.handlers.put("http", this.getURLStreamHandler("http"));
        this.handlers.put("https", this.getURLStreamHandler("https"));
    }
    
    private URLStreamHandler getURLStreamHandler(final String protocol) {
        try {
            final Method method = URL.class.getDeclaredMethod("getURLStreamHandler", String.class);
            method.setAccessible(true);
            return (URLStreamHandler)method.invoke(null, protocol);
        }
        catch (Exception e) {
            if (CachingURLStreamHandlerFactory.logger.isTraceEnabled()) {
                CachingURLStreamHandlerFactory.logger.warn("could not access URL.getUrlStreamHandler");
            }
            return null;
        }
    }
    
    @Override
    public URLStreamHandler createURLStreamHandler(final String protocol) {
        if (null == protocol) {
            throw new IllegalArgumentException("null protocol not allowed");
        }
        if (CachingURLStreamHandlerFactory.logger.isTraceEnabled()) {
            CachingURLStreamHandlerFactory.logger.trace("need to create URLStreamHandler for protocol " + protocol);
        }
        final String proto = protocol.toLowerCase();
        if ("http".equals(proto) || "https".equals(proto)) {
            return new URLStreamHandler() {
                @Override
                protected URLConnection openConnection(final URL url) throws IOException {
                    if (CachingURLStreamHandlerFactory.logger.isTraceEnabled()) {
                        CachingURLStreamHandlerFactory.logger.trace("should open connection to " + url.toExternalForm());
                    }
                    final URLConnection defaultUrlConnection = new URL(protocol, url.getHost(), url.getPort(), url.getFile(), CachingURLStreamHandlerFactory.this.handlers.get(protocol)).openConnection();
                    if (!CachingURLStreamHandlerFactory.this.cache.urlShouldBeCached(url)) {
                        if (CachingURLStreamHandlerFactory.logger.isTraceEnabled()) {
                            CachingURLStreamHandlerFactory.logger.trace("not using cache for " + url);
                        }
                        return defaultUrlConnection;
                    }
                    if (CachingURLStreamHandlerFactory.this.cache.isCached(url)) {
                        return new CachingHttpURLConnection(CachingURLStreamHandlerFactory.this.cache, (HttpURLConnection)defaultUrlConnection);
                    }
                    final String val$proto = proto;
                    switch (val$proto) {
                        case "http": {
                            return new CachingHttpURLConnection(CachingURLStreamHandlerFactory.this.cache, (HttpURLConnection)defaultUrlConnection);
                        }
                        case "https": {
                            return new CachingHttpsURLConnection(CachingURLStreamHandlerFactory.this.cache, (HttpsURLConnection)defaultUrlConnection);
                        }
                        default: {
                            throw new IOException("no matching handler");
                        }
                    }
                }
                
                @Override
                protected URLConnection openConnection(final URL u, final Proxy p) throws IOException {
                    if (CachingURLStreamHandlerFactory.logger.isTraceEnabled()) {
                        CachingURLStreamHandlerFactory.logger.trace("should open connection to " + u.toExternalForm() + " via " + p);
                    }
                    final URLConnection defaultUrlConnection = new URL(protocol, u.getHost(), u.getPort(), u.getFile(), CachingURLStreamHandlerFactory.this.handlers.get(protocol)).openConnection(p);
                    return defaultUrlConnection;
                }
            };
        }
        return null;
    }
    
    static {
        logger = Logger.getLogger(CachingURLStreamHandlerFactory.class);
    }
}
