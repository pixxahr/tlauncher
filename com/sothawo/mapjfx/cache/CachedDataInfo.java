// 
// Decompiled by Procyon v0.5.36
// 

package com.sothawo.mapjfx.cache;

import java.util.List;
import java.util.Map;
import java.io.Serializable;

public class CachedDataInfo implements Serializable
{
    private String contentType;
    private String contentEncoding;
    private long contentLength;
    private long timestamp;
    private Map<String, List<String>> headerFields;
    
    public CachedDataInfo() {
        this.contentLength = -1L;
        this.timestamp = -1L;
    }
    
    public Map<String, List<String>> getHeaderFields() {
        return this.headerFields;
    }
    
    public void setHeaderFields(final Map<String, List<String>> headerFields) {
        this.headerFields = headerFields;
    }
    
    public String getContentEncoding() {
        return this.contentEncoding;
    }
    
    public void setContentEncoding(final String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }
    
    public String getContentType() {
        return this.contentType;
    }
    
    public void setContentType(final String contentType) {
        this.contentType = contentType;
    }
    
    public long getTimestamp() {
        return this.timestamp;
    }
    
    public long getContentLength() {
        return this.contentLength;
    }
    
    public void setTimestamp(final long l) {
        this.timestamp = l;
    }
    
    public void setContentLength(final long l) {
        this.contentLength = l;
    }
}
