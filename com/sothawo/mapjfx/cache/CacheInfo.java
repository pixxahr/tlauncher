// 
// Decompiled by Procyon v0.5.36
// 

package com.sothawo.mapjfx.cache;

public class CacheInfo
{
    private boolean caching;
    private int seconds;
    
    public CacheInfo(final String s) {
        this.caching = false;
        this.seconds = 0;
        if (s == null) {
            return;
        }
        final String[] ss = s.split("=");
        if (ss.length > 1) {
            try {
                this.seconds = Integer.parseInt(ss[1]);
                this.caching = true;
            }
            catch (Exception ex) {}
        }
    }
    
    public boolean isCaching() {
        return this.caching;
    }
    
    public int getSeconds() {
        return this.seconds;
    }
}
