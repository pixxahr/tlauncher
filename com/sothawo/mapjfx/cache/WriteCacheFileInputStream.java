// 
// Decompiled by Procyon v0.5.36
// 

package com.sothawo.mapjfx.cache;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FilterInputStream;

public class WriteCacheFileInputStream extends FilterInputStream
{
    private final OutputStream out;
    private Runnable notifyOnClose;
    
    protected WriteCacheFileInputStream(final InputStream in, final OutputStream out) {
        super(in);
        this.out = out;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        final int numBytes = super.read(b, off, len);
        if (null != this.out && numBytes > 0) {
            this.out.write(b, off, numBytes);
        }
        return numBytes;
    }
    
    @Override
    public void close() throws IOException {
        super.close();
        if (null != this.out) {
            this.out.flush();
            this.out.close();
        }
        if (null != this.notifyOnClose) {
            this.notifyOnClose.run();
        }
    }
    
    public void onInputStreamClose(final Runnable r) {
        this.notifyOnClose = r;
    }
}
