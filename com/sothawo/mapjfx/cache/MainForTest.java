// 
// Decompiled by Procyon v0.5.36
// 

package com.sothawo.mapjfx.cache;

import javafx.scene.web.WebEngine;
import javafx.scene.Node;
import javafx.scene.web.WebView;
import javafx.scene.layout.VBox;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.stage.Stage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import javafx.application.Application;

public class MainForTest extends Application
{
    public static void main(final String[] args) throws IOException {
        final Path p = Paths.get("target/cache", new String[0]);
        if (Files.notExists(p, new LinkOption[0])) {
            Files.createDirectory(p, (FileAttribute<?>[])new FileAttribute[0]);
        }
        OfflineCache.INSTANCE.setCacheDirectory(p);
        OfflineCache.INSTANCE.setActive(true);
        final URL url = new URL("https://dl2.tlrepo.com/pictures/73878_logo.png");
        final HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        if (connection.getResponseCode() == 200) {
            Files.copy(connection.getInputStream(), Paths.get(p.normalize().toString() + "/its_working_only_when_u_using_image.png", new String[0]), StandardCopyOption.REPLACE_EXISTING);
            connection.getInputStream().close();
            System.out.println("ok");
        }
        OfflineCache.INSTANCE.removeExpiredCache();
    }
    
    public void start(final Stage stage) {
        stage.setTitle("HTML");
        stage.setWidth(1280.0);
        stage.setHeight(720.0);
        final Scene scene = new Scene((Parent)new Group());
        final VBox root = new VBox();
        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();
        scene.setRoot((Parent)root);
        stage.setScene(scene);
        root.getChildren().addAll((Object[])new Node[] { (Node)browser });
        webEngine.load("http://page.tlauncher.org/update/downloads/configs/client/index.html");
        stage.show();
    }
}
