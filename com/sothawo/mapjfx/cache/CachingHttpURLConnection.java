// 
// Decompiled by Procyon v0.5.36
// 

package com.sothawo.mapjfx.cache;

import java.net.ProtocolException;
import java.security.Permission;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.net.URL;
import java.io.InputStream;
import java.nio.file.Path;
import org.apache.log4j.Logger;
import java.net.HttpURLConnection;

public class CachingHttpURLConnection extends HttpURLConnection
{
    private static final Logger logger;
    private final HttpURLConnection delegate;
    private final Path cacheFile;
    private final OfflineCache cache;
    private boolean readFromCache;
    private InputStream inputStream;
    private CachedDataInfo cachedDataInfo;
    private CacheInfo cinfo;
    
    private CachingHttpURLConnection(final URL u) {
        super(u);
        this.readFromCache = false;
        this.cinfo = null;
        this.cache = null;
        this.delegate = null;
        this.cacheFile = null;
    }
    
    public CachingHttpURLConnection(final OfflineCache cache, final HttpURLConnection delegate) throws IOException {
        super(delegate.getURL());
        this.readFromCache = false;
        this.cinfo = null;
        this.cache = cache;
        this.delegate = delegate;
        this.cacheFile = cache.filenameForURL(delegate.getURL());
        this.cachedDataInfo = cache.readCachedDataInfo(this.cacheFile);
        if (!(this.readFromCache = (cache.isCached(delegate.getURL()) && null != this.cachedDataInfo))) {
            this.cachedDataInfo = new CachedDataInfo();
        }
        if (CachingHttpURLConnection.logger.isTraceEnabled()) {
            CachingHttpURLConnection.logger.trace("in cache: " + this.readFromCache + ", URL: " + delegate.getURL().toExternalForm() + ", cache file: " + this.cacheFile);
        }
    }
    
    @Override
    public void connect() throws IOException {
        if (!this.readFromCache) {
            if (CachingHttpURLConnection.logger.isTraceEnabled()) {
                CachingHttpURLConnection.logger.trace("connect to " + this.delegate.getURL().toExternalForm());
            }
            this.delegate.connect();
        }
    }
    
    @Override
    public String getHeaderFieldKey(final int n) {
        return this.delegate.getHeaderFieldKey(n);
    }
    
    @Override
    public void setFixedLengthStreamingMode(final int contentLength) {
        this.delegate.setFixedLengthStreamingMode(contentLength);
    }
    
    @Override
    public void setFixedLengthStreamingMode(final long contentLength) {
        this.delegate.setFixedLengthStreamingMode(contentLength);
    }
    
    @Override
    public void addRequestProperty(final String key, final String value) {
        this.delegate.addRequestProperty(key, value);
    }
    
    @Override
    public void setChunkedStreamingMode(final int chunklen) {
        this.delegate.setChunkedStreamingMode(chunklen);
    }
    
    @Override
    public String getHeaderField(final int n) {
        return this.delegate.getHeaderField(n);
    }
    
    @Override
    public void disconnect() {
        if (!this.readFromCache) {
            this.delegate.disconnect();
        }
    }
    
    @Override
    public boolean getAllowUserInteraction() {
        return this.delegate.getAllowUserInteraction();
    }
    
    @Override
    public int getConnectTimeout() {
        return this.readFromCache ? 10 : this.delegate.getConnectTimeout();
    }
    
    @Override
    public Object getContent() throws IOException {
        return this.delegate.getContent();
    }
    
    @Override
    public Object getContent(final Class[] classes) throws IOException {
        return this.delegate.getContent(classes);
    }
    
    @Override
    public String getContentEncoding() {
        if (!this.readFromCache) {
            this.cachedDataInfo.setContentEncoding(this.delegate.getContentEncoding());
        }
        return this.cachedDataInfo.getContentEncoding();
    }
    
    @Override
    public int getContentLength() {
        return this.readFromCache ? ((int)this.cachedDataInfo.getContentLength()) : this.delegate.getContentLength();
    }
    
    @Override
    public long getContentLengthLong() {
        if (!this.readFromCache) {
            this.cachedDataInfo.setContentLength(this.delegate.getContentLengthLong());
        }
        return this.readFromCache ? this.cachedDataInfo.getContentLength() : this.delegate.getContentLengthLong();
    }
    
    @Override
    public String getContentType() {
        if (!this.readFromCache) {
            this.cachedDataInfo.setContentType(this.delegate.getContentType());
        }
        return this.cachedDataInfo.getContentType();
    }
    
    @Override
    public long getDate() {
        return this.readFromCache ? 0L : this.delegate.getDate();
    }
    
    @Override
    public boolean getDefaultUseCaches() {
        return this.delegate.getDefaultUseCaches();
    }
    
    @Override
    public boolean getDoInput() {
        return this.delegate.getDoInput();
    }
    
    @Override
    public boolean getDoOutput() {
        return this.delegate.getDoOutput();
    }
    
    @Override
    public InputStream getErrorStream() {
        return this.delegate.getErrorStream();
    }
    
    @Override
    public long getExpiration() {
        return this.readFromCache ? 0L : this.delegate.getExpiration();
    }
    
    @Override
    public String getHeaderField(final String name) {
        return this.delegate.getHeaderField(name);
    }
    
    @Override
    public long getHeaderFieldDate(final String name, final long Default) {
        return this.delegate.getHeaderFieldDate(name, Default);
    }
    
    @Override
    public int getHeaderFieldInt(final String name, final int Default) {
        return this.delegate.getHeaderFieldInt(name, Default);
    }
    
    @Override
    public long getHeaderFieldLong(final String name, final long Default) {
        return this.delegate.getHeaderFieldLong(name, Default);
    }
    
    @Override
    public Map<String, List<String>> getHeaderFields() {
        if (!this.readFromCache) {
            this.cachedDataInfo.setHeaderFields(this.delegate.getHeaderFields());
        }
        return this.cachedDataInfo.getHeaderFields();
    }
    
    @Override
    public long getIfModifiedSince() {
        return this.delegate.getIfModifiedSince();
    }
    
    private boolean expired() {
        return !this.readFromCache || this.cachedDataInfo.getTimestamp() <= System.currentTimeMillis();
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        if (this.readFromCache || this.cinfo == null || !this.cinfo.isCaching()) {
            return this.delegate.getInputStream();
        }
        if (null == this.inputStream) {
            if (this.readFromCache && this.getContentLengthLong() == this.delegate.getContentLengthLong() && !this.expired()) {
                this.inputStream = new FileInputStream(this.cacheFile.toFile());
            }
            else {
                final WriteCacheFileInputStream wis = new WriteCacheFileInputStream(this.delegate.getInputStream(), new FileOutputStream(this.cacheFile.toFile()));
                int responseCode;
                wis.onInputStreamClose(() -> {
                    try {
                        responseCode = this.delegate.getResponseCode();
                        if (responseCode == 200) {
                            this.cachedDataInfo.setTimestamp(System.currentTimeMillis() + this.cinfo.getSeconds() * 1000);
                            this.cache.saveCachedDataInfo(this.cacheFile, this.cachedDataInfo);
                        }
                        else if (CachingHttpURLConnection.logger.isTraceEnabled()) {
                            CachingHttpURLConnection.logger.warn("not caching because of response code " + responseCode + ": " + this.getURL());
                        }
                    }
                    catch (IOException e) {
                        if (CachingHttpURLConnection.logger.isTraceEnabled()) {
                            CachingHttpURLConnection.logger.warn("cannot retrieve response code");
                        }
                    }
                    return;
                });
                this.inputStream = wis;
            }
        }
        return this.inputStream;
    }
    
    @Override
    public boolean getInstanceFollowRedirects() {
        return this.delegate.getInstanceFollowRedirects();
    }
    
    @Override
    public long getLastModified() {
        return this.readFromCache ? 0L : this.delegate.getLastModified();
    }
    
    @Override
    public OutputStream getOutputStream() throws IOException {
        return this.delegate.getOutputStream();
    }
    
    @Override
    public Permission getPermission() throws IOException {
        return this.delegate.getPermission();
    }
    
    @Override
    public int getReadTimeout() {
        return this.delegate.getReadTimeout();
    }
    
    @Override
    public String getRequestMethod() {
        return this.delegate.getRequestMethod();
    }
    
    @Override
    public Map<String, List<String>> getRequestProperties() {
        return this.delegate.getRequestProperties();
    }
    
    @Override
    public String getRequestProperty(final String key) {
        return this.delegate.getRequestProperty(key);
    }
    
    @Override
    public int getResponseCode() throws IOException {
        return this.readFromCache ? 200 : this.delegate.getResponseCode();
    }
    
    @Override
    public String getResponseMessage() throws IOException {
        return this.readFromCache ? "OK" : this.delegate.getResponseMessage();
    }
    
    @Override
    public URL getURL() {
        return this.delegate.getURL();
    }
    
    @Override
    public boolean getUseCaches() {
        return this.delegate.getUseCaches();
    }
    
    @Override
    public void setAllowUserInteraction(final boolean allowuserinteraction) {
        this.delegate.setAllowUserInteraction(allowuserinteraction);
    }
    
    @Override
    public void setConnectTimeout(final int timeout) {
        this.delegate.setConnectTimeout(timeout);
    }
    
    @Override
    public void setDefaultUseCaches(final boolean defaultusecaches) {
        this.delegate.setDefaultUseCaches(defaultusecaches);
    }
    
    @Override
    public void setDoInput(final boolean doinput) {
        this.delegate.setDoInput(doinput);
    }
    
    @Override
    public void setDoOutput(final boolean dooutput) {
        this.delegate.setDoOutput(dooutput);
    }
    
    @Override
    public void setIfModifiedSince(final long ifmodifiedsince) {
        this.delegate.setIfModifiedSince(ifmodifiedsince);
    }
    
    @Override
    public void setInstanceFollowRedirects(final boolean followRedirects) {
        this.delegate.setInstanceFollowRedirects(followRedirects);
    }
    
    @Override
    public void setReadTimeout(final int timeout) {
        this.delegate.setReadTimeout(timeout);
    }
    
    @Override
    public void setRequestMethod(final String method) throws ProtocolException {
        this.delegate.setRequestMethod(method);
        this.cinfo = new CacheInfo(this.delegate.getHeaderField("cache-control"));
    }
    
    @Override
    public void setRequestProperty(final String key, final String value) {
        this.delegate.setRequestProperty(key, value);
    }
    
    @Override
    public void setUseCaches(final boolean usecaches) {
        this.delegate.setUseCaches(usecaches);
    }
    
    @Override
    public boolean usingProxy() {
        return this.delegate.usingProxy();
    }
    
    static {
        logger = Logger.getLogger(CachingHttpURLConnection.class);
    }
}
