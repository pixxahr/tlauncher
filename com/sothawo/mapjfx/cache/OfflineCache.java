// 
// Decompiled by Procyon v0.5.36
// 

package com.sothawo.mapjfx.cache;

import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.SimpleFileVisitor;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.URLStreamHandlerFactory;
import java.net.URL;
import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.LinkOption;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.io.IOException;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.util.ArrayList;
import java.nio.file.Path;
import java.util.regex.Pattern;
import java.util.Collection;
import org.apache.log4j.Logger;

public enum OfflineCache
{
    INSTANCE;
    
    private static final Logger logger;
    private static final String TILE_OPENSTREETMAP_ORG = "[a-z]\\.tile\\.openstreetmap\\.org";
    private final Collection<Pattern> noCachePatterns;
    private boolean urlStreamHandlerFactoryIsInitialized;
    private boolean active;
    private Path cacheDirectory;
    
    private OfflineCache() {
        this.noCachePatterns = new ArrayList<Pattern>();
        this.urlStreamHandlerFactoryIsInitialized = false;
        this.active = false;
    }
    
    static void clearDirectory(final Path path) throws IOException {
        Files.walkFileTree(path, new DeletingFileVisitor(path));
    }
    
    public Collection<String> getNoCacheFilters() {
        return this.noCachePatterns.stream().map((Function<? super Pattern, ?>)Pattern::toString).collect((Collector<? super Object, ?, Collection<String>>)Collectors.toList());
    }
    
    public void setNoCacheFilters(final Collection<String> noCacheFilters) {
        this.noCachePatterns.clear();
        if (null != noCacheFilters) {
            noCacheFilters.stream().map((Function<? super String, ?>)Pattern::compile).forEach(this.noCachePatterns::add);
        }
    }
    
    public Path getCacheDirectory() {
        return this.cacheDirectory;
    }
    
    public void setCacheDirectory(final Path cacheDirectory) {
        final Path dir = Objects.requireNonNull(cacheDirectory);
        if (!Files.isDirectory(dir, new LinkOption[0]) || !Files.isWritable(dir)) {
            throw new IllegalArgumentException("cacheDirectory: " + dir);
        }
        this.cacheDirectory = dir;
    }
    
    public void setCacheDirectory(final String cacheDirectory) {
        this.setCacheDirectory(FileSystems.getDefault().getPath(Objects.requireNonNull(cacheDirectory), new String[0]));
    }
    
    public void removeExpiredCache() {
        try {
            for (final File f : this.cacheDirectory.toFile().listFiles()) {
                final String[] ss = f.getName().split("\\.");
                if (ss.length >= 2) {
                    if (!ss[ss.length - 1].equalsIgnoreCase("datainfo")) {
                        final File info = new File(f.getAbsoluteFile() + ".dataInfo");
                        if (info.exists()) {
                            final CachedDataInfo cdi = this.readCachedDataInfo(info);
                            if (cdi != null) {
                                if (cdi.getTimestamp() <= System.currentTimeMillis()) {
                                    f.delete();
                                    info.delete();
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            OfflineCache.logger.error("error with cleaning", ex);
        }
    }
    
    boolean urlShouldBeCached(final URL u) {
        if (!this.isActive()) {
            return false;
        }
        final String urlString = u.toString();
        return this.noCachePatterns.stream().filter(pattern -> pattern.matcher(urlString).matches()).noneMatch(pattern -> true);
    }
    
    public boolean isActive() {
        return this.active;
    }
    
    public void setActive(final boolean active) {
        if (active && null == this.cacheDirectory) {
            throw new IllegalArgumentException("cannot setActive when no cacheDirectory is set");
        }
        if (active) {
            this.setupURLStreamHandlerFactory();
        }
        this.active = active;
    }
    
    private void setupURLStreamHandlerFactory() {
        if (!this.urlStreamHandlerFactoryIsInitialized) {
            String msg;
            try {
                URL.setURLStreamHandlerFactory(new CachingURLStreamHandlerFactory(this));
                this.urlStreamHandlerFactoryIsInitialized = true;
                return;
            }
            catch (Error e) {
                msg = "cannot setup URLStreamFactoryHandler, it is already set in this application. " + e.getMessage();
                if (OfflineCache.logger.isTraceEnabled()) {
                    OfflineCache.logger.error(msg);
                }
            }
            catch (SecurityException e2) {
                msg = "cannot setup URLStreamFactoryHandler. " + e2.getMessage();
                if (OfflineCache.logger.isTraceEnabled()) {
                    OfflineCache.logger.error(msg);
                }
            }
            throw new IllegalStateException(msg);
        }
    }
    
    boolean isCached(final URL url) {
        try {
            final Path cacheFile = this.filenameForURL(url);
            return Files.exists(cacheFile, new LinkOption[0]) && Files.isReadable(cacheFile) && Files.size(cacheFile) > 0L;
        }
        catch (Exception e) {
            if (OfflineCache.logger.isTraceEnabled()) {
                OfflineCache.logger.warn(e.getMessage());
            }
            return false;
        }
    }
    
    Path filenameForURL(final URL url) throws UnsupportedEncodingException {
        if (null == this.cacheDirectory) {
            throw new IllegalStateException("cannot resolve filename for url");
        }
        final String mappedString = Objects.requireNonNull(this.doMappings(url.toExternalForm()));
        final String encodedString = URLEncoder.encode(mappedString, "UTF-8");
        return this.cacheDirectory.resolve(encodedString);
    }
    
    private String doMappings(final String urlString) {
        if (null == urlString || urlString.isEmpty()) {
            return urlString;
        }
        return urlString.replaceAll("[a-z]\\.tile\\.openstreetmap\\.org", "x.tile.openstreetmap.org");
    }
    
    void saveCachedDataInfo(final Path cacheFile, final CachedDataInfo cachedDataInfo) {
        final Path cacheDataFile = Paths.get(cacheFile + ".dataInfo", new String[0]);
        try (final ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(cacheDataFile.toFile()))) {
            oos.writeObject(cachedDataInfo);
            oos.flush();
            if (OfflineCache.logger.isTraceEnabled()) {
                OfflineCache.logger.trace("saved dataInfo " + cacheDataFile);
            }
        }
        catch (Exception e) {
            if (OfflineCache.logger.isTraceEnabled()) {
                OfflineCache.logger.warn("could not save dataInfo " + cacheDataFile);
            }
        }
    }
    
    CachedDataInfo readCachedDataInfo(final Path cacheFile) {
        CachedDataInfo cachedDataInfo = null;
        final Path cacheDataFile = Paths.get(cacheFile + ".dataInfo", new String[0]);
        if (Files.exists(cacheDataFile, new LinkOption[0])) {
            try (final ObjectInputStream ois = new ObjectInputStream(new FileInputStream(cacheDataFile.toFile()))) {
                cachedDataInfo = (CachedDataInfo)ois.readObject();
            }
            catch (Exception e) {
                if (OfflineCache.logger.isTraceEnabled()) {
                    OfflineCache.logger.warn("could not read dataInfo from " + cacheDataFile + ", " + e.getMessage());
                }
            }
        }
        return cachedDataInfo;
    }
    
    private CachedDataInfo readCachedDataInfo(final File f) {
        CachedDataInfo cachedDataInfo = null;
        try (final ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))) {
            cachedDataInfo = (CachedDataInfo)ois.readObject();
        }
        catch (Exception e) {
            if (OfflineCache.logger.isTraceEnabled()) {
                OfflineCache.logger.warn("could not read dataInfo from " + f + ", " + e.getMessage());
            }
        }
        return cachedDataInfo;
    }
    
    public void clear() throws IOException {
        if (null != this.cacheDirectory) {
            clearDirectory(this.cacheDirectory);
        }
    }
    
    static {
        logger = Logger.getLogger(OfflineCache.class);
    }
    
    private static class DeletingFileVisitor extends SimpleFileVisitor<Path>
    {
        private final Path rootDir;
        
        public DeletingFileVisitor(final Path path) {
            this.rootDir = path;
        }
        
        @Override
        public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
            if (!attrs.isDirectory()) {
                Files.delete(file);
            }
            return FileVisitResult.CONTINUE;
        }
        
        @Override
        public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
            if (!dir.equals(this.rootDir)) {
                Files.delete(dir);
            }
            return FileVisitResult.CONTINUE;
        }
    }
}
