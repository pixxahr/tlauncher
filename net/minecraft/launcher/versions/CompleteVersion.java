// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions;

import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonObject;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import com.google.gson.JsonParseException;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import net.minecraft.launcher.versions.json.RepoTypeAdapter;
import org.tlauncher.util.gson.serializer.ModpackDTOTypeAdapter;
import java.lang.reflect.Type;
import net.minecraft.launcher.versions.json.DateTypeAdapter;
import com.google.gson.TypeAdapterFactory;
import net.minecraft.launcher.versions.json.LowerCaseEnumTypeAdapterFactory;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonSerializer;
import org.tlauncher.util.U;
import java.util.HashMap;
import java.util.Objects;
import net.minecraft.launcher.updater.VersionSyncInfo;
import java.io.IOException;
import org.tlauncher.tlauncher.managers.VersionManager;
import java.util.HashSet;
import java.util.Set;
import org.tlauncher.util.OS;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.io.File;
import java.util.Collections;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import net.minecraft.launcher.updater.VersionList;
import org.tlauncher.tlauncher.repository.Repo;
import net.minecraft.launcher.versions.json.Argument;
import java.util.List;
import net.minecraft.launcher.versions.json.ArgumentType;
import java.util.Map;
import java.util.Date;

public class CompleteVersion extends AbstractVersion implements Cloneable
{
    String id;
    String jar;
    String inheritsFrom;
    Date time;
    Date releaseTime;
    ReleaseType type;
    String jvmArguments;
    String minecraftArguments;
    String mainClass;
    Integer minimumLauncherVersion;
    Integer tlauncherVersion;
    Map<ArgumentType, List<Argument>> arguments;
    String assets;
    Repo source;
    VersionList list;
    private boolean skinVersion;
    List<Library> libraries;
    List<Rule> rules;
    List<String> deleteEntries;
    private List<Library> modsLibraries;
    private Map<String, MetadataDTO> downloads;
    private AssetsMetadata assetIndex;
    private List<MetadataDTO> additionalFiles;
    private ModpackDTO modpack;
    
    public CompleteVersion() {
        this.minimumLauncherVersion = 0;
        this.tlauncherVersion = 0;
    }
    
    public boolean isModpack() {
        return this.modpack != null;
    }
    
    public AssetsMetadata getAssetIndex() {
        return this.assetIndex;
    }
    
    public void setAssetIndex(final AssetsMetadata assetIndex) {
        this.assetIndex = assetIndex;
    }
    
    public Map<String, MetadataDTO> getDownloads() {
        return this.downloads;
    }
    
    public void setDownloads(final Map<String, MetadataDTO> downloads) {
        this.downloads = downloads;
    }
    
    public List<Library> getModsLibraries() {
        return this.modsLibraries;
    }
    
    public void setModsLibraries(final List<Library> modsLibraries) {
        this.modsLibraries = modsLibraries;
    }
    
    @Override
    public boolean isSkinVersion() {
        return this.skinVersion;
    }
    
    @Override
    public void setSkinVersion(final boolean skinVersion) {
        this.skinVersion = skinVersion;
    }
    
    @Override
    public String getID() {
        return this.id;
    }
    
    @Override
    public void setID(final String id) {
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("ID is NULL or empty");
        }
        this.id = id;
    }
    
    @Override
    public ReleaseType getReleaseType() {
        return this.type;
    }
    
    @Override
    public Repo getSource() {
        return this.source;
    }
    
    @Override
    public void setSource(final Repo repo) {
        if (repo == null) {
            throw new NullPointerException();
        }
        this.source = repo;
    }
    
    @Override
    public Date getUpdatedTime() {
        return this.time;
    }
    
    public void setUpdatedTime(final Date time) {
        if (time == null) {
            throw new NullPointerException("Time is NULL!");
        }
        this.time = time;
    }
    
    @Override
    public Date getReleaseTime() {
        return this.releaseTime;
    }
    
    @Override
    public VersionList getVersionList() {
        return this.list;
    }
    
    @Override
    public void setVersionList(final VersionList list) {
        if (list == null) {
            throw new NullPointerException("VersionList cannot be NULL!");
        }
        this.list = list;
    }
    
    @Override
    public String getJar() {
        return this.jar;
    }
    
    public String getInheritsFrom() {
        return this.inheritsFrom;
    }
    
    public String getJVMArguments() {
        return this.jvmArguments;
    }
    
    public String getMinecraftArguments() {
        return this.minecraftArguments;
    }
    
    public void setMinecraftArguments(final String args) {
        this.minecraftArguments = args;
    }
    
    public String getMainClass() {
        return this.mainClass;
    }
    
    public void setMainClass(final String clazz) {
        this.mainClass = clazz;
    }
    
    public List<Library> getLibraries() {
        return this.libraries;
    }
    
    public List<Rule> getRules() {
        return Collections.unmodifiableList((List<? extends Rule>)this.rules);
    }
    
    public List<String> getDeleteEntries() {
        return this.deleteEntries;
    }
    
    public int getMinimumLauncherVersion() {
        return this.minimumLauncherVersion;
    }
    
    public int getMinimumCustomLauncherVersion() {
        return this.tlauncherVersion;
    }
    
    public String getAssets() {
        return (this.assets == null) ? "legacy" : this.assets;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.hashCode() == o.hashCode()) {
            return true;
        }
        if (!(o instanceof Version)) {
            return false;
        }
        final Version compare = (Version)o;
        return compare.getID() != null && compare.getID().equals(this.id);
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{id='" + this.id + "', time=" + this.time + ", release=" + this.releaseTime + ", type=" + this.type + ", class=" + this.mainClass + ", minimumVersion=" + this.minimumLauncherVersion + ", assets='" + this.assets + "', source=" + this.source + ", list=" + this.list + ", libraries=" + this.libraries + "}";
    }
    
    public File getFile(final File base) {
        return new File(base, "versions/" + this.getID() + "/" + this.getID() + ".jar");
    }
    
    public boolean appliesToCurrentEnvironment() {
        if (this.rules == null) {
            return true;
        }
        for (final Rule rule : this.rules) {
            final Rule.Action action = rule.getAppliedAction();
            if (action == Rule.Action.DISALLOW) {
                return false;
            }
        }
        return true;
    }
    
    public Collection<Library> getRelevantLibraries() {
        final List<Library> result = new ArrayList<Library>();
        for (final Library library : this.libraries) {
            if (library.appliesToCurrentEnvironment()) {
                result.add(library);
            }
        }
        return result;
    }
    
    public Collection<File> getClassPath(final OS os, final File base) {
        final Collection<Library> libraries = this.getRelevantLibraries();
        final Collection<File> result = new ArrayList<File>();
        for (final Library library : libraries) {
            if (library.getNatives() == null) {
                result.add(new File(base, "libraries/" + library.getArtifactPath()));
            }
        }
        result.add(new File(base, "versions/" + this.getID() + "/" + this.getID() + ".jar"));
        return result;
    }
    
    public Collection<File> getClassPath(final File base) {
        return this.getClassPath(OS.CURRENT, base);
    }
    
    public Collection<String> getNatives(final OS os) {
        final Collection<Library> libraries = this.getRelevantLibraries();
        final Collection<String> result = new ArrayList<String>();
        for (final Library library : libraries) {
            final Map<OS, String> natives = library.getNatives();
            if (natives != null && natives.containsKey(os)) {
                result.add("libraries/" + library.getArtifactPath(natives.get(os)));
            }
        }
        return result;
    }
    
    public Collection<String> getNatives() {
        return this.getNatives(OS.CURRENT);
    }
    
    public Set<String> getRequiredFiles(final OS os) {
        final Set<String> neededFiles = new HashSet<String>();
        for (final Library library : this.getRelevantLibraries()) {
            if (library.getNatives() != null) {
                final String natives = library.getNatives().get(os);
                if (natives == null) {
                    continue;
                }
                neededFiles.add("libraries/" + library.getArtifactPath(natives));
            }
            else {
                neededFiles.add("libraries/" + library.getArtifactPath());
            }
        }
        return neededFiles;
    }
    
    public Collection<String> getExtractFiles(final OS os) {
        final Collection<Library> libraries = this.getRelevantLibraries();
        final Collection<String> result = new ArrayList<String>();
        for (final Library library : libraries) {
            final Map<OS, String> natives = library.getNatives();
            if (natives != null && natives.containsKey(os)) {
                result.add("libraries/" + library.getArtifactPath(natives.get(os)));
            }
        }
        return result;
    }
    
    public CompleteVersion resolve(final VersionManager vm) throws IOException {
        return this.resolve(vm, false);
    }
    
    public CompleteVersion resolve(final VersionManager vm, final boolean useLatest) throws IOException {
        return this.resolve(vm, useLatest, new ArrayList<String>());
    }
    
    protected CompleteVersion resolve(final VersionManager vm, final boolean useLatest, final List<String> inheristance) throws IOException {
        if (vm == null) {
            throw new NullPointerException("version manager");
        }
        if (this.inheritsFrom == null) {
            return this;
        }
        this.log("Resolving...");
        if (inheristance.contains(this.id)) {
            throw new IllegalArgumentException(this.id + " should be already resolved.");
        }
        inheristance.add(this.id);
        this.log("Inherits from", this.inheritsFrom);
        final VersionSyncInfo parentSyncInfo = vm.getVersionSyncInfo(this.inheritsFrom);
        CompleteVersion result;
        try {
            if (parentSyncInfo == null) {
                return null;
            }
            result = (CompleteVersion)parentSyncInfo.getCompleteVersion(useLatest).resolve(vm, useLatest, inheristance).clone();
        }
        catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        return this.partCopyInto(result);
    }
    
    private CompleteVersion partCopyInto(final CompleteVersion result) {
        result.id = this.id;
        if (this.jar != null) {
            result.jar = this.jar;
        }
        result.inheritsFrom = null;
        if (this.time.getTime() != 0L) {
            result.time = this.time;
        }
        if (this.type != ReleaseType.UNKNOWN) {
            result.type = this.type;
        }
        if (this.jvmArguments != null) {
            result.jvmArguments = this.jvmArguments;
        }
        if (this.minecraftArguments != null) {
            result.minecraftArguments = this.minecraftArguments;
        }
        if (Objects.nonNull(result.arguments) || Objects.nonNull(this.arguments)) {
            final Map<ArgumentType, List<Argument>> args = new HashMap<ArgumentType, List<Argument>>();
            args.put(ArgumentType.GAME, new ArrayList<Argument>());
            args.put(ArgumentType.JVM, new ArrayList<Argument>());
            if (Objects.nonNull(result.arguments)) {
                result.arguments.forEach((key, value) -> args.get(key).addAll(value));
            }
            if (Objects.nonNull(this.arguments)) {
                this.arguments.forEach((key, value) -> args.get(key).addAll(value));
            }
            result.arguments = args;
        }
        if (this.mainClass != null) {
            result.mainClass = this.mainClass;
        }
        if (this.libraries != null) {
            final List<Library> newLibraries = new ArrayList<Library>(this.libraries);
            if (result.libraries != null) {
                newLibraries.addAll(result.libraries);
            }
            result.libraries = newLibraries;
        }
        if (this.rules != null) {
            if (result.rules != null) {
                result.rules.addAll(this.rules);
            }
            else {
                final List<Rule> rulesCopy = new ArrayList<Rule>(this.rules.size());
                Collections.copy((List<? super Object>)rulesCopy, (List<?>)this.rules);
                result.rules = this.rules;
            }
        }
        if (this.deleteEntries != null) {
            if (result.deleteEntries != null) {
                result.deleteEntries.addAll(this.deleteEntries);
            }
            else {
                result.deleteEntries = new ArrayList<String>(this.deleteEntries);
            }
        }
        if (this.minimumLauncherVersion != 0) {
            result.minimumLauncherVersion = this.minimumLauncherVersion;
        }
        if (this.tlauncherVersion != 0) {
            result.tlauncherVersion = this.tlauncherVersion;
        }
        if (this.assets != null && !this.assets.equals("legacy")) {
            result.assets = this.assets;
        }
        if (this.source != null) {
            result.source = this.source;
        }
        result.setSkinVersion(this.skinVersion);
        if (this.url != null) {
            result.setUrl(this.getUrl());
        }
        if (this.assetIndex != null) {
            result.setAssetIndex(this.getAssetIndex());
        }
        if (this.downloads != null) {
            result.setDownloads(this.getDownloads());
        }
        if (this.modsLibraries != null) {
            final List<Library> newLibraries = new ArrayList<Library>(this.modsLibraries);
            if (result.modsLibraries != null) {
                newLibraries.addAll(result.modsLibraries);
            }
            result.modsLibraries = newLibraries;
        }
        if (this.modpack != null) {
            final ModpackDTO newModpack = new ModpackDTO();
            this.modpack.copy(newModpack);
            result.modpack = newModpack;
        }
        if (result.releaseTime == null) {
            result.releaseTime = this.releaseTime;
        }
        if (Objects.nonNull(this.getAdditionalFiles())) {
            result.setAdditionalFiles(new ArrayList<MetadataDTO>(this.getAdditionalFiles()));
        }
        result.list = this.list;
        return result;
    }
    
    public CompleteVersion fullCopy(final CompleteVersion c) {
        this.partCopyInto(c);
        c.inheritsFrom = this.inheritsFrom;
        return c;
    }
    
    public Map<ArgumentType, List<Argument>> getArguments() {
        return this.arguments;
    }
    
    private void log(final Object... o) {
        U.log("[Version:" + this.id + "]", o);
    }
    
    public ModpackDTO getModpack() {
        return this.modpack;
    }
    
    public void setModpackDTO(final ModpackDTO modpack) {
        this.modpack = modpack;
    }
    
    public List<MetadataDTO> getAdditionalFiles() {
        return this.additionalFiles;
    }
    
    public void setAdditionalFiles(final List<MetadataDTO> additionalFiles) {
        this.additionalFiles = additionalFiles;
    }
    
    public static class AssetsMetadata
    {
        private String id;
        private int totalSize;
        protected String name;
        protected long size;
        protected String path;
        protected String url;
        
        public String getId() {
            return this.id;
        }
        
        public int getTotalSize() {
            return this.totalSize;
        }
        
        public String getName() {
            return this.name;
        }
        
        public long getSize() {
            return this.size;
        }
        
        public String getPath() {
            return this.path;
        }
        
        public String getUrl() {
            return this.url;
        }
        
        public void setId(final String id) {
            this.id = id;
        }
        
        public void setTotalSize(final int totalSize) {
            this.totalSize = totalSize;
        }
        
        public void setName(final String name) {
            this.name = name;
        }
        
        public void setSize(final long size) {
            this.size = size;
        }
        
        public void setPath(final String path) {
            this.path = path;
        }
        
        public void setUrl(final String url) {
            this.url = url;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof AssetsMetadata)) {
                return false;
            }
            final AssetsMetadata other = (AssetsMetadata)o;
            if (!other.canEqual(this)) {
                return false;
            }
            final Object this$id = this.getId();
            final Object other$id = other.getId();
            Label_0065: {
                if (this$id == null) {
                    if (other$id == null) {
                        break Label_0065;
                    }
                }
                else if (this$id.equals(other$id)) {
                    break Label_0065;
                }
                return false;
            }
            if (this.getTotalSize() != other.getTotalSize()) {
                return false;
            }
            final Object this$name = this.getName();
            final Object other$name = other.getName();
            Label_0115: {
                if (this$name == null) {
                    if (other$name == null) {
                        break Label_0115;
                    }
                }
                else if (this$name.equals(other$name)) {
                    break Label_0115;
                }
                return false;
            }
            if (this.getSize() != other.getSize()) {
                return false;
            }
            final Object this$path = this.getPath();
            final Object other$path = other.getPath();
            Label_0166: {
                if (this$path == null) {
                    if (other$path == null) {
                        break Label_0166;
                    }
                }
                else if (this$path.equals(other$path)) {
                    break Label_0166;
                }
                return false;
            }
            final Object this$url = this.getUrl();
            final Object other$url = other.getUrl();
            if (this$url == null) {
                if (other$url == null) {
                    return true;
                }
            }
            else if (this$url.equals(other$url)) {
                return true;
            }
            return false;
        }
        
        protected boolean canEqual(final Object other) {
            return other instanceof AssetsMetadata;
        }
        
        @Override
        public int hashCode() {
            final int PRIME = 59;
            int result = 1;
            final Object $id = this.getId();
            result = result * 59 + (($id == null) ? 43 : $id.hashCode());
            result = result * 59 + this.getTotalSize();
            final Object $name = this.getName();
            result = result * 59 + (($name == null) ? 43 : $name.hashCode());
            final long $size = this.getSize();
            result = result * 59 + (int)($size >>> 32 ^ $size);
            final Object $path = this.getPath();
            result = result * 59 + (($path == null) ? 43 : $path.hashCode());
            final Object $url = this.getUrl();
            result = result * 59 + (($url == null) ? 43 : $url.hashCode());
            return result;
        }
        
        @Override
        public String toString() {
            return "CompleteVersion.AssetsMetadata(id=" + this.getId() + ", totalSize=" + this.getTotalSize() + ", name=" + this.getName() + ", size=" + this.getSize() + ", path=" + this.getPath() + ", url=" + this.getUrl() + ")";
        }
    }
    
    public static class CompleteVersionSerializer implements JsonSerializer<CompleteVersion>, JsonDeserializer<CompleteVersion>
    {
        private final Gson defaultContext;
        
        public CompleteVersionSerializer() {
            final GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
            builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
            builder.registerTypeAdapter(Library.class, new Library.LibrarySerializer());
            builder.registerTypeAdapter(Argument.class, new Argument.Serializer());
            builder.registerTypeAdapter(ModpackDTO.class, new ModpackDTOTypeAdapter());
            builder.registerTypeAdapter(Repo.class, new RepoTypeAdapter());
            builder.enableComplexMapKeySerialization();
            builder.disableHtmlEscaping();
            builder.setPrettyPrinting();
            this.defaultContext = builder.create();
        }
        
        @Override
        public CompleteVersion deserialize(final JsonElement elem, final Type type, final JsonDeserializationContext context) throws JsonParseException {
            final JsonObject object = elem.getAsJsonObject();
            final JsonElement originalId = object.get("original_id");
            if (originalId != null && originalId.isJsonPrimitive()) {
                final String jar = originalId.getAsString();
                object.remove("original_id");
                object.addProperty("jar", jar);
            }
            if (Objects.nonNull(object.get("inheritsFrom")) && Objects.isNull(object.get("jar"))) {
                object.addProperty("jar", object.get("inheritsFrom").getAsString());
            }
            final JsonElement unnecessaryEntries = object.get("unnecessaryEntries");
            if (unnecessaryEntries != null && unnecessaryEntries.isJsonArray()) {
                object.remove("unnecessaryEntries");
                object.add("deleteEntries", unnecessaryEntries);
            }
            final CompleteVersion version = this.defaultContext.fromJson(elem, CompleteVersion.class);
            if (version.id == null) {
                throw new JsonParseException("Version ID is NULL!");
            }
            if (version.type == null) {
                version.type = ReleaseType.UNKNOWN;
            }
            if (version.source == null) {
                version.source = ClientInstanceRepo.LOCAL_VERSION_REPO;
            }
            if (version.time == null) {
                version.time = new Date(0L);
            }
            if (version.assets == null) {
                version.assets = "legacy";
            }
            return version;
        }
        
        @Override
        public JsonElement serialize(final CompleteVersion version0, final Type type, final JsonSerializationContext context) {
            CompleteVersion version;
            try {
                version = (CompleteVersion)version0.clone();
            }
            catch (CloneNotSupportedException e) {
                U.log("Cloning of CompleteVersion is not supported O_o", e);
                return this.defaultContext.toJsonTree(version0, type);
            }
            version.list = null;
            final JsonObject object = (JsonObject)this.defaultContext.toJsonTree(version, type);
            final JsonElement jar = object.get("jar");
            if (jar == null) {
                object.remove("downloadJarLibraries");
            }
            return object;
        }
    }
}
