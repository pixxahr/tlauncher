// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.tlauncher.util.OS;
import java.util.Map;

public class Rule
{
    private Action action;
    private OSRestriction os;
    private Map<String, Object> features;
    
    public Rule() {
        this.action = Action.ALLOW;
    }
    
    public Rule(final Rule rule) {
        this.action = Action.ALLOW;
        this.action = rule.action;
        if (rule.os != null) {
            this.os = new OSRestriction(rule.os);
        }
    }
    
    public Action getAppliedAction() {
        if (this.features != null && this.features.get("is_demo_user") != null) {
            return Action.DISALLOW;
        }
        if (this.os != null && !this.os.isCurrentOperatingSystem()) {
            return null;
        }
        return this.action;
    }
    
    @Override
    public String toString() {
        return "Rule{action=" + this.action + ", os=" + this.os + ", features=" + this.features + '}';
    }
    
    public enum Action
    {
        ALLOW, 
        DISALLOW;
    }
    
    public class OSRestriction
    {
        private OS name;
        private String version;
        
        public OSRestriction(final OSRestriction osRestriction) {
            this.name = osRestriction.name;
            this.version = osRestriction.version;
        }
        
        public boolean isCurrentOperatingSystem() {
            if (this.name != null && this.name != OS.CURRENT) {
                return false;
            }
            if (this.version != null) {
                try {
                    final Pattern pattern = Pattern.compile(this.version);
                    final Matcher matcher = pattern.matcher(System.getProperty("os.version"));
                    if (!matcher.matches()) {
                        return false;
                    }
                }
                catch (Throwable t) {}
            }
            return true;
        }
        
        @Override
        public String toString() {
            return "OSRestriction{name=" + this.name + ", version='" + this.version + '\'' + '}';
        }
    }
}
