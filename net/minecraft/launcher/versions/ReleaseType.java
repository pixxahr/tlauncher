// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions;

import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Date;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public enum ReleaseType
{
    RELEASE("release", false, true), 
    SNAPSHOT("snapshot"), 
    MODIFIED("modified", true, true), 
    OLD_BETA("old-beta"), 
    OLD_ALPHA("old-alpha"), 
    UNKNOWN("unknown", false, false);
    
    private static final Map<String, ReleaseType> lookup;
    private static final List<ReleaseType> defaultTypes;
    private static final List<ReleaseType> definableTypes;
    private final String name;
    private final boolean isDefinable;
    private final boolean isDefault;
    
    private ReleaseType(final String name, final boolean isDefinable, final boolean isDefault) {
        this.name = name;
        this.isDefinable = isDefinable;
        this.isDefault = isDefault;
    }
    
    private ReleaseType(final String name) {
        this(name, true, false);
    }
    
    String getName() {
        return this.name;
    }
    
    public boolean isDefault() {
        return this.isDefault;
    }
    
    public boolean isDefinable() {
        return this.isDefinable;
    }
    
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    
    public static ReleaseType getByName(final String name) {
        return ReleaseType.lookup.get(name);
    }
    
    public static Collection<ReleaseType> valuesCollection() {
        return ReleaseType.lookup.values();
    }
    
    public static List<ReleaseType> getDefault() {
        return ReleaseType.defaultTypes;
    }
    
    public static List<ReleaseType> getDefinable() {
        return ReleaseType.definableTypes;
    }
    
    static {
        final HashMap<String, ReleaseType> types = new HashMap<String, ReleaseType>(values().length);
        final ArrayList<ReleaseType> deflTypes = new ArrayList<ReleaseType>();
        final ArrayList<ReleaseType> defnTypes = new ArrayList<ReleaseType>();
        for (final ReleaseType type : values()) {
            types.put(type.getName(), type);
            if (type.isDefault()) {
                deflTypes.add(type);
            }
            if (type.isDefinable()) {
                defnTypes.add(type);
            }
        }
        lookup = Collections.unmodifiableMap((Map<? extends String, ? extends ReleaseType>)types);
        defaultTypes = Collections.unmodifiableList((List<? extends ReleaseType>)deflTypes);
        definableTypes = Collections.unmodifiableList((List<? extends ReleaseType>)defnTypes);
    }
    
    public enum SubType
    {
        OLD_RELEASE("old_release") {
            private final Date marker;
            
            {
                final GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
                calendar.set(2013, 3, 20, 15, 0);
                this.marker = calendar.getTime();
            }
            
            @Override
            public boolean isSubType(final Version version) {
                return !version.getReleaseType().toString().startsWith("old") && version.getReleaseTime().getTime() >= 0L && version.getReleaseTime().before(this.marker);
            }
        }, 
        REMOTE("remote") {
            @Override
            public boolean isSubType(final Version version) {
                return version.getSource() != ClientInstanceRepo.LOCAL_VERSION_REPO;
            }
        };
        
        private static final Map<String, SubType> lookup;
        private static final List<SubType> defaultSubTypes;
        private final String name;
        private final boolean isDefault;
        
        private SubType(final String name, final boolean isDefault) {
            this.name = name;
            this.isDefault = isDefault;
        }
        
        private SubType(final String name) {
            this(name, true);
        }
        
        public String getName() {
            return this.name;
        }
        
        public boolean isDefault() {
            return this.isDefault;
        }
        
        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
        
        public static SubType getByName(final String name) {
            return SubType.lookup.get(name);
        }
        
        public static Collection<SubType> valuesCollection() {
            return SubType.lookup.values();
        }
        
        public static List<SubType> getDefault() {
            return SubType.defaultSubTypes;
        }
        
        public static SubType get(final Version version) {
            for (final SubType subType : values()) {
                if (subType.isSubType(version)) {
                    return subType;
                }
            }
            return null;
        }
        
        public abstract boolean isSubType(final Version p0);
        
        static {
            final HashMap<String, SubType> subTypes = new HashMap<String, SubType>(values().length);
            final ArrayList<SubType> defSubTypes = new ArrayList<SubType>();
            for (final SubType subType : values()) {
                subTypes.put(subType.getName(), subType);
                if (subType.isDefault()) {
                    defSubTypes.add(subType);
                }
            }
            lookup = Collections.unmodifiableMap((Map<? extends String, ? extends SubType>)subTypes);
            defaultSubTypes = Collections.unmodifiableList((List<? extends SubType>)defSubTypes);
        }
    }
}
