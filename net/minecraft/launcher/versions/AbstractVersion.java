// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions;

public abstract class AbstractVersion implements Version
{
    protected String url;
    
    @Override
    public String getUrl() {
        return this.url;
    }
    
    @Override
    public void setUrl(final String url) {
        this.url = url;
    }
}
