// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions;

import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonParseException;
import java.util.Set;
import com.google.gson.JsonObject;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import net.minecraft.launcher.versions.json.DateTypeAdapter;
import java.util.Date;
import com.google.gson.TypeAdapterFactory;
import net.minecraft.launcher.versions.json.LowerCaseEnumTypeAdapterFactory;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonSerializer;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Closeable;
import org.tlauncher.util.U;
import java.util.zip.ZipEntry;
import java.util.jar.JarEntry;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.util.jar.Pack200;
import java.io.OutputStream;
import java.util.jar.JarOutputStream;
import java.io.FileOutputStream;
import org.tlauncher.util.FileUtil;
import java.util.Arrays;
import org.tlauncher.tlauncher.downloader.RetryDownloadException;
import org.tukaani.xz.XZInputStream;
import java.io.FileInputStream;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.tlauncher.downloader.Downloadable;
import java.io.File;
import org.tlauncher.tlauncher.repository.Repo;
import java.util.Iterator;
import java.util.Collections;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import org.tlauncher.util.OS;
import java.util.Map;
import java.util.List;
import org.apache.commons.lang3.text.StrSubstitutor;

public class Library
{
    private static final String FORGE_LIB_SUFFIX = ".pack.xz";
    private static final StrSubstitutor SUBSTITUTOR;
    String url;
    String exact_url;
    String packed;
    String checksum;
    private String name;
    private List<Rule> rules;
    private Map<OS, String> natives;
    private ExtractRules extract;
    private List<String> deleteEntries;
    private MetadataDTO artifact;
    private Map<String, MetadataDTO> classifies;
    
    private String getArtifactBaseDir() {
        if (this.name == null) {
            throw new IllegalStateException("Cannot get artifact dir of empty/blank artifact");
        }
        final String[] parts = this.name.split(":", 3);
        return String.format("%s/%s/%s", parts[0].replaceAll("\\.", "/"), parts[1], parts[2]);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Library) {
            return false;
        }
        final Library lib = (Library)o;
        return (this.name == null) ? (lib.name == null) : this.name.equalsIgnoreCase(lib.name);
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getPlainName() {
        final String[] split = this.name.split(":", 3);
        return split[0] + "." + split[1];
    }
    
    public List<Rule> getRules() {
        return (this.rules == null) ? null : Collections.unmodifiableList((List<? extends Rule>)this.rules);
    }
    
    boolean appliesToCurrentEnvironment() {
        if (this.rules == null) {
            return true;
        }
        Rule.Action lastAction = Rule.Action.DISALLOW;
        for (final Rule rule : this.rules) {
            final Rule.Action action = rule.getAppliedAction();
            if (action != null) {
                lastAction = action;
            }
        }
        return lastAction == Rule.Action.ALLOW;
    }
    
    public Map<OS, String> getNatives() {
        return this.natives;
    }
    
    public ExtractRules getExtractRules() {
        return this.extract;
    }
    
    public String getChecksum() {
        final MetadataDTO meta = this.defineMetadataLibrary(OS.CURRENT);
        if (meta != null) {
            return meta.getSha1();
        }
        if (this.artifact != null) {
            return this.artifact.getSha1();
        }
        return this.checksum;
    }
    
    public List<String> getDeleteEntriesList() {
        return this.deleteEntries;
    }
    
    public String getArtifactPath(final String classifier) {
        if (this.name == null) {
            throw new IllegalStateException("Cannot get artifact path of empty/blank artifact");
        }
        return String.format("%s/%s", this.getArtifactBaseDir(), this.getArtifactFilename(classifier));
    }
    
    public String getArtifactPath() {
        return this.getArtifactPath(null);
    }
    
    private String getArtifactFilename(final String classifier) {
        if (this.name == null) {
            throw new IllegalStateException("Cannot get artifact filename of empty/blank artifact");
        }
        final String[] parts = this.name.split(":", 3);
        String result;
        if (classifier == null) {
            result = String.format("%s-%s.jar", parts[1], parts[2]);
        }
        else {
            result = String.format("%s-%s%s.jar", parts[1], parts[2], "-" + classifier);
        }
        return Library.SUBSTITUTOR.replace(result);
    }
    
    @Override
    public String toString() {
        return "Library{name='" + this.name + '\'' + ", rules=" + this.rules + ", natives=" + this.natives + ", extract=" + this.extract + ", packed='" + this.packed + "'}";
    }
    
    public Downloadable getDownloadable(final Repo versionSource, final File file, final OS os) {
        Repo repo = null;
        final boolean isForge = "forge".equals(this.packed);
        final LibraryDownloadable lib = this.determineNewSource(file, os);
        if (lib != null) {
            return lib;
        }
        String path;
        if (this.exact_url == null) {
            final String nativePath = (this.natives != null && this.appliesToCurrentEnvironment()) ? this.natives.get(os) : null;
            path = this.getArtifactPath(nativePath) + (isForge ? ".pack.xz" : "");
            if (this.url == null) {
                repo = ClientInstanceRepo.LIBRARY_REPO;
            }
            else if (this.url.startsWith("/")) {
                repo = versionSource;
                path = this.url.substring(1) + path;
            }
            else {
                path = this.url + path;
            }
        }
        else {
            path = this.exact_url;
        }
        if (isForge) {
            final File tempFile = new File(file.getAbsolutePath() + ".pack.xz");
            return new ForgeLibDownloadable(ClientInstanceRepo.EMPTY_REPO, path, tempFile, file);
        }
        return (repo == null) ? new LibraryDownloadable(ClientInstanceRepo.EMPTY_REPO, path, file) : new LibraryDownloadable(repo, path, file);
    }
    
    private LibraryDownloadable determineNewSource(final File file, final OS os) {
        final MetadataDTO meta = this.defineMetadataLibrary(os);
        if (meta != null) {
            return new LibraryDownloadable(ClientInstanceRepo.EMPTY_REPO, meta.getUrl(), file);
        }
        if (this.artifact != null) {
            return new LibraryDownloadable(ClientInstanceRepo.EMPTY_REPO, this.artifact.getUrl(), file);
        }
        return null;
    }
    
    public void setUrl(final String url) {
        this.url = url;
    }
    
    private static synchronized void unpackLibrary(final File library, final File output, final boolean retryOnOutOfMemory) throws IOException {
        forgeLibLog("Synchronized unpacking:", library);
        output.delete();
        InputStream in = null;
        JarOutputStream jos = null;
        try {
            in = new FileInputStream(library);
            in = (InputStream)new XZInputStream(in);
            forgeLibLog("Decompressing...");
            final byte[] decompressed = readFully(in);
            forgeLibLog("Decompressed successfully");
            final String end = new String(decompressed, decompressed.length - 4, 4);
            if (!end.equals("SIGN")) {
                throw new RetryDownloadException("signature missing");
            }
            forgeLibLog("Signature matches!");
            final int x = decompressed.length;
            final int len = (decompressed[x - 8] & 0xFF) | (decompressed[x - 7] & 0xFF) << 8 | (decompressed[x - 6] & 0xFF) << 16 | (decompressed[x - 5] & 0xFF) << 24;
            forgeLibLog("Now getting checksums...");
            final byte[] checksums = Arrays.copyOfRange(decompressed, decompressed.length - len - 8, decompressed.length - 8);
            FileUtil.createFile(output);
            final FileOutputStream jarBytes = new FileOutputStream(output);
            jos = new JarOutputStream(jarBytes);
            forgeLibLog("Now unpacking...");
            Pack200.newUnpacker().unpack(new ByteArrayInputStream(decompressed), jos);
            forgeLibLog("Unpacked successfully");
            forgeLibLog("Now trying to write checksums...");
            jos.putNextEntry(new JarEntry("checksums.sha1"));
            jos.write(checksums);
            jos.closeEntry();
            forgeLibLog("Now finishing...");
        }
        catch (OutOfMemoryError oomE) {
            forgeLibLog("Out of memory, oops", oomE);
            U.gc();
            if (retryOnOutOfMemory) {
                forgeLibLog("Retrying...");
                close(in, jos);
                FileUtil.deleteFile(library);
                unpackLibrary(library, output, false);
                return;
            }
            throw oomE;
        }
        catch (IOException e) {
            output.delete();
            throw e;
        }
        finally {
            close(in, jos);
            FileUtil.deleteFile(library);
        }
        forgeLibLog("Done:", output);
    }
    
    private static synchronized void unpackLibrary(final File library, final File output) throws IOException {
        unpackLibrary(library, output, true);
    }
    
    private static void close(final Closeable... closeables) {
        for (final Closeable c : closeables) {
            try {
                c.close();
            }
            catch (Exception ex) {}
        }
    }
    
    private static byte[] readFully(final InputStream stream) throws IOException {
        final byte[] data = new byte[4096];
        final ByteArrayOutputStream entryBuffer = new ByteArrayOutputStream();
        int len;
        do {
            len = stream.read(data);
            if (len > 0) {
                entryBuffer.write(data, 0, len);
            }
        } while (len != -1);
        return entryBuffer.toByteArray();
    }
    
    private static void forgeLibLog(final Object... o) {
        U.log("[ForgeLibDownloadable]", o);
    }
    
    private MetadataDTO defineMetadataLibrary(final OS os) {
        if (this.classifies != null && this.natives != null) {
            if (this.classifies.get(os.name().toLowerCase()) != null) {
                return this.classifies.get(os.name().toLowerCase());
            }
            if (this.classifies.get(os.name().toLowerCase() + "-" + OS.Arch.CURRENT.name().replace("x", "")) != null) {
                return this.classifies.get(os.name().toLowerCase() + "-" + OS.Arch.CURRENT.name().replace("x", ""));
            }
        }
        return null;
    }
    
    static {
        final HashMap<String, String> map = new HashMap<String, String>();
        map.put("platform", OS.CURRENT.getName());
        map.put("arch", OS.Arch.CURRENT.asString());
        SUBSTITUTOR = new StrSubstitutor((Map)map);
    }
    
    public enum TYPE
    {
        CLASSIFIES, 
        ARTIFACT;
    }
    
    public class LibraryDownloadable extends Downloadable
    {
        private LibraryDownloadable(final Repo repo, final String path, final File file) {
            super(repo, path, file);
        }
        
        private LibraryDownloadable(final String path, final File file) {
            super(path, file);
        }
        
        public Library getDownloadableLibrary() {
            return Library.this;
        }
        
        public Library getLibrary() {
            return Library.this;
        }
    }
    
    public static class LibrarySerializer implements JsonSerializer<Library>, JsonDeserializer<Library>
    {
        private Gson gson;
        
        public LibrarySerializer() {
            final GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
            builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
            builder.enableComplexMapKeySerialization();
            this.gson = builder.create();
        }
        
        @Override
        public Library deserialize(final JsonElement elem, final Type type, final JsonDeserializationContext context) throws JsonParseException {
            final JsonObject object = elem.getAsJsonObject();
            final JsonObject downloads = object.getAsJsonObject("downloads");
            if (downloads != null) {
                final JsonElement artifact = downloads.get("artifact");
                if (artifact != null) {
                    object.add("artifact", artifact);
                }
                final JsonObject classifies = downloads.getAsJsonObject("classifiers");
                if (classifies != null) {
                    final Set<Map.Entry<String, JsonElement>> set = classifies.entrySet();
                    final JsonObject ob = new JsonObject();
                    for (final Map.Entry<String, JsonElement> el : set) {
                        final String s = el.getKey();
                        switch (s) {
                            case "natives-linux": {
                                ob.add(OS.LINUX.name().toLowerCase(), el.getValue());
                                continue;
                            }
                            case "natives-windows": {
                                ob.add(OS.WINDOWS.name().toLowerCase(), el.getValue());
                                continue;
                            }
                            case "natives-macos": {
                                ob.add(OS.OSX.name().toLowerCase(), el.getValue());
                                continue;
                            }
                            case "natives-windows-32": {
                                ob.add(OS.WINDOWS.name().toLowerCase() + "-32", el.getValue());
                                continue;
                            }
                            case "natives-windows-64": {
                                ob.add(OS.WINDOWS.name().toLowerCase() + "-64", el.getValue());
                                continue;
                            }
                            case "natives-osx": {
                                ob.add(OS.OSX.name().toLowerCase(), el.getValue());
                                continue;
                            }
                            case "tests": {
                                ob.add("tests", el.getValue());
                                continue;
                            }
                            case "sources": {
                                ob.add("sources", el.getValue());
                                continue;
                            }
                            case "javadoc": {
                                ob.add("javadoc", el.getValue());
                                continue;
                            }
                            default: {
                                U.log("can't find proper config for ", el);
                                continue;
                            }
                        }
                    }
                    object.add("classifies", ob);
                }
                object.remove("downloads");
            }
            final Library library = this.gson.fromJson(elem, Library.class);
            return library;
        }
        
        @Override
        public JsonElement serialize(final Library library, final Type type, final JsonSerializationContext context) {
            final JsonObject object = (JsonObject)this.gson.toJsonTree(library, type);
            return object;
        }
    }
    
    public class ForgeLibDownloadable extends LibraryDownloadable
    {
        private final File unpacked;
        
        public ForgeLibDownloadable(final Repo rep, final String url, final File packedLib, final File unpackedLib) {
            super(rep, url, packedLib);
            this.unpacked = unpackedLib;
        }
        
        @Override
        protected void onComplete() throws RetryDownloadException {
            super.onComplete();
            try {
                unpackLibrary(this.getDestination(), this.unpacked);
            }
            catch (Throwable t) {
                throw new RetryDownloadException("cannot unpack forge library", t);
            }
        }
    }
}
