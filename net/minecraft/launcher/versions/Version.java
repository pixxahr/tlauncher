// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions;

import net.minecraft.launcher.updater.VersionList;
import java.util.Date;
import org.tlauncher.tlauncher.repository.Repo;

public interface Version
{
    String getID();
    
    void setID(final String p0);
    
    String getJar();
    
    ReleaseType getReleaseType();
    
    Repo getSource();
    
    void setSource(final Repo p0);
    
    Date getUpdatedTime();
    
    Date getReleaseTime();
    
    VersionList getVersionList();
    
    void setVersionList(final VersionList p0);
    
    boolean isSkinVersion();
    
    void setSkinVersion(final boolean p0);
    
    String getUrl();
    
    void setUrl(final String p0);
}
