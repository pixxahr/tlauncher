// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions;

import net.minecraft.launcher.updater.VersionList;
import org.tlauncher.tlauncher.repository.Repo;
import java.util.Date;

public class PartialVersion extends AbstractVersion
{
    private String id;
    private String jar;
    private Date time;
    private Date releaseTime;
    private ReleaseType type;
    private boolean skinVersion;
    private Repo source;
    private VersionList list;
    
    @Override
    public boolean isSkinVersion() {
        return this.skinVersion;
    }
    
    @Override
    public void setSkinVersion(final boolean skinVersion) {
        this.skinVersion = skinVersion;
    }
    
    @Override
    public String getID() {
        return this.id;
    }
    
    @Override
    public String getJar() {
        return this.jar;
    }
    
    @Override
    public void setID(final String id) {
        this.id = id;
    }
    
    @Override
    public ReleaseType getReleaseType() {
        return this.type;
    }
    
    @Override
    public Repo getSource() {
        return this.source;
    }
    
    @Override
    public void setSource(final Repo repo) {
        if (repo == null) {
            throw new NullPointerException();
        }
        this.source = repo;
    }
    
    @Override
    public Date getUpdatedTime() {
        return this.time;
    }
    
    @Override
    public Date getReleaseTime() {
        return this.releaseTime;
    }
    
    @Override
    public VersionList getVersionList() {
        return this.list;
    }
    
    @Override
    public void setVersionList(final VersionList list) {
        if (list == null) {
            throw new NullPointerException();
        }
        this.list = list;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.hashCode() == o.hashCode()) {
            return true;
        }
        if (!(o instanceof Version)) {
            return false;
        }
        final Version compare = (Version)o;
        return compare.getID() != null && compare.getID().equals(this.id);
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{id='" + this.id + "', time=" + this.time + ", release=" + this.releaseTime + ", type=" + this.type + ", source=" + this.source + ", skinVersion=" + this.skinVersion + ", list=" + this.list + "}";
    }
}
