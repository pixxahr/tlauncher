// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions.json;

import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonParseException;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import com.google.gson.JsonDeserializer;
import java.util.regex.Pattern;
import com.google.gson.JsonSerializer;

public class PatternTypeAdapter implements JsonSerializer<Pattern>, JsonDeserializer<Pattern>
{
    @Override
    public Pattern deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        final String string = json.getAsString();
        return StringUtils.isBlank((CharSequence)string) ? null : Pattern.compile(string);
    }
    
    @Override
    public JsonElement serialize(final Pattern src, final Type typeOfSrc, final JsonSerializationContext context) {
        return new JsonPrimitive((src == null) ? null : src.toString());
    }
}
