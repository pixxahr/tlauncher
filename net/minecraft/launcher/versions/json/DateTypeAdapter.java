// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions.json;

import org.tlauncher.exceptions.ParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.DateFormat;
import com.google.gson.JsonSerializer;
import java.util.Date;
import com.google.gson.JsonDeserializer;

public class DateTypeAdapter implements JsonDeserializer<Date>, JsonSerializer<Date>
{
    private final DateFormat enUsFormat;
    private final DateFormat iso8601Format;
    
    public DateTypeAdapter() {
        this.enUsFormat = DateFormat.getDateTimeInstance(2, 2, Locale.US);
        this.iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    }
    
    @Override
    public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        if (!(json instanceof JsonPrimitive)) {
            throw new JsonParseException("The date should be a string value");
        }
        final Date date = this.toDate(json.getAsString());
        if (typeOfT == Date.class) {
            return date;
        }
        throw new IllegalArgumentException(this.getClass() + " cannot deserialize to " + typeOfT);
    }
    
    @Override
    public JsonElement serialize(final Date src, final Type typeOfSrc, final JsonSerializationContext context) {
        synchronized (this.enUsFormat) {
            return new JsonPrimitive(this.toString(src));
        }
    }
    
    public String toString(final Date date) {
        synchronized (this.enUsFormat) {
            final String result = this.iso8601Format.format(date);
            return result.substring(0, 22) + ":" + result.substring(22);
        }
    }
    
    public Date toDate(final String string) {
        synchronized (this.enUsFormat) {
            try {
                return this.enUsFormat.parse(string);
            }
            catch (Exception ex) {
                try {
                    return this.iso8601Format.parse(string);
                }
                catch (Exception ex2) {
                    try {
                        String cleaned = string.replace("Z", "+00:00");
                        cleaned = cleaned.substring(0, 22) + cleaned.substring(23);
                        // monitorexit(this.enUsFormat)
                        return this.iso8601Format.parse(cleaned);
                    }
                    catch (Exception e) {
                        throw new ParseException("Invalid date: " + string, e);
                    }
                }
            }
        }
    }
}
