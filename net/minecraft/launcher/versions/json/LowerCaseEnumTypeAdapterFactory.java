// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions.json;

import java.util.Locale;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import com.google.gson.stream.JsonWriter;
import java.util.Map;
import java.util.HashMap;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.TypeAdapterFactory;

public class LowerCaseEnumTypeAdapterFactory implements TypeAdapterFactory
{
    @Override
    public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> type) {
        final Class<?> rawType = type.getRawType();
        if (!rawType.isEnum()) {
            return null;
        }
        final Map<String, Object> lowercaseToConstant = new HashMap<String, Object>();
        for (final Object constant : rawType.getEnumConstants()) {
            lowercaseToConstant.put(toLowercase(constant), constant);
        }
        return new TypeAdapter<T>() {
            @Override
            public void write(final JsonWriter out, final Object value) throws IOException {
                if (value == null) {
                    out.nullValue();
                }
                else {
                    out.value(toLowercase(value));
                }
            }
            
            @Override
            public T read(final JsonReader reader) throws IOException {
                if (reader.peek() == JsonToken.NULL) {
                    reader.nextNull();
                    return null;
                }
                return lowercaseToConstant.get(reader.nextString());
            }
        };
    }
    
    private static String toLowercase(final Object o) {
        return o.toString().toLowerCase(Locale.US);
    }
}
