// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions.json;

import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonParseException;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializer;
import org.tlauncher.tlauncher.repository.Repo;
import com.google.gson.JsonDeserializer;

public class RepoTypeAdapter implements JsonDeserializer<Repo>, JsonSerializer<Repo>
{
    @Override
    public Repo deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return ClientInstanceRepo.find(jsonElement.getAsString());
    }
    
    @Override
    public JsonElement serialize(final Repo repo, final Type type, final JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(repo.getName().toLowerCase());
    }
}
