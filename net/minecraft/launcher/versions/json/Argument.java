// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.versions.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.util.ArrayList;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import com.google.gson.JsonDeserializer;
import java.util.Arrays;
import java.util.Iterator;
import net.minecraft.launcher.versions.Rule;
import java.util.List;

public class Argument
{
    private final String[] values;
    private final List<Rule> rules;
    
    public Argument(final String[] values, final List<Rule> rules) {
        this.values = values;
        this.rules = rules;
    }
    
    public boolean appliesToCurrentEnvironment() {
        if (this.rules == null || this.rules.isEmpty()) {
            return true;
        }
        Rule.Action lastAction = Rule.Action.DISALLOW;
        for (final Rule compatibilityRule : this.rules) {
            final Rule.Action action = compatibilityRule.getAppliedAction();
            if (action != null) {
                lastAction = action;
            }
        }
        return lastAction == Rule.Action.ALLOW;
    }
    
    public String[] getValues() {
        return this.values;
    }
    
    @Override
    public String toString() {
        return "Argument{values=" + Arrays.toString(this.values) + '}';
    }
    
    public static class Serializer implements JsonDeserializer<Argument>
    {
        @Override
        public Argument deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            if (json.isJsonPrimitive()) {
                return new Argument(new String[] { json.getAsString() }, null);
            }
            if (json.isJsonObject()) {
                final JsonObject obj = json.getAsJsonObject();
                JsonElement value = obj.get("value");
                if (value == null) {
                    value = obj.get("values");
                }
                String[] values;
                if (value.isJsonPrimitive()) {
                    values = new String[] { value.getAsString() };
                }
                else {
                    final JsonArray array = value.getAsJsonArray();
                    values = new String[array.size()];
                    for (int i = 0; i < array.size(); ++i) {
                        values[i] = array.get(i).getAsString();
                    }
                }
                final List<Rule> rules = new ArrayList<Rule>();
                if (obj.has("rules")) {
                    final JsonArray array2 = obj.getAsJsonArray("rules");
                    for (final JsonElement element : array2) {
                        rules.add(context.deserialize(element, Rule.class));
                    }
                }
                return new Argument(values, rules);
            }
            throw new JsonParseException("Invalid argument, must be object or string");
        }
    }
}
