// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher;

import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import java.util.zip.ZipInputStream;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.MalformedURLException;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import javax.net.ssl.SSLException;
import org.tlauncher.util.TlauncherUtil;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import org.tlauncher.util.U;
import java.net.HttpURLConnection;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import com.google.common.base.Strings;
import java.util.Map;

public class Http
{
    public static final String JSON_CONTENT_TYPE = "application/json";
    public static final String TEXT_PLAIN = "text/plain";
    public static final String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    
    public static String get(final String url, final Map<String, Object> query) {
        final String line = buildQuery(query);
        if (Strings.isNullOrEmpty(line)) {
            return url;
        }
        return url + "?" + line;
    }
    
    public static String get(final String url, final String key, final String value) {
        final Map<String, Object> map = new HashMap<String, Object>();
        map.put(key, value);
        final String line = buildQuery(map);
        if (Strings.isNullOrEmpty(line)) {
            return url;
        }
        return url + "?" + line;
    }
    
    public static String performPost(final URL url, final Map<String, Object> query) throws IOException {
        return performPost(url, buildQuery(query), "application/x-www-form-urlencoded");
    }
    
    public static String performGet(final URL url, final int connTimeout, final int readTimeout) throws IOException {
        try {
            final HttpURLConnection connection = (HttpURLConnection)url.openConnection(U.getProxy());
            connection.setConnectTimeout(connTimeout);
            connection.setReadTimeout(readTimeout);
            connection.setRequestMethod("GET");
            String res = "";
            try {
                if (connection.getContentType() != null && connection.getContentType().equalsIgnoreCase("application/zip")) {
                    res = readZip(connection);
                }
                else {
                    final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
                    final StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                    reader.close();
                    res = response.toString();
                }
            }
            finally {
                if (TLauncher.DEBUG) {
                    String shortRes = res.contains("\r") ? res.replaceAll("\r", "") : res;
                    if (shortRes.length() > 400) {
                        shortRes = shortRes.substring(0, 400);
                    }
                    U.debug("request: " + url + ", responce: " + shortRes);
                }
            }
            return res;
        }
        catch (SSLException e) {
            TlauncherUtil.deactivateSSL();
            throw e;
        }
    }
    
    public static String performGet(final String url) throws IOException {
        return performGet(constantURL(url), U.getConnectionTimeout(), U.getReadTimeout());
    }
    
    public static String performGet(final URL url) throws IOException {
        return performGet(url, U.getConnectionTimeout(), U.getReadTimeout());
    }
    
    public static String performGet(final String url, final Map<String, Object> map, final int connTimeout, final int readTimeout) throws IOException {
        final StringBuilder stringBuilder = new StringBuilder(url);
        stringBuilder.append("?");
        for (final Map.Entry<String, Object> e : map.entrySet()) {
            stringBuilder.append(e.getKey()).append("=").append(encode(e.getValue().toString())).append("&");
        }
        stringBuilder.setLength(stringBuilder.length() - 1);
        return performGet(new URL(stringBuilder.toString()), connTimeout, readTimeout);
    }
    
    public static String performPost(final URL url, byte[] body, final String contentType, final boolean gzip) throws IOException {
        final HttpURLConnection connection = (HttpURLConnection)url.openConnection(U.getProxy());
        connection.setConnectTimeout(U.getConnectionTimeout());
        connection.setReadTimeout(U.getReadTimeout());
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", contentType + "; charset=utf-8");
        connection.setRequestProperty("Content-Language", "en-US");
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        if (gzip) {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final GZIPOutputStream g = new GZIPOutputStream(out);
            g.write(body);
            g.close();
            body = out.toByteArray();
        }
        connection.setRequestProperty("Content-Length", "" + body.length);
        final OutputStream writer = new DataOutputStream(connection.getOutputStream());
        writer.write(body);
        writer.close();
        final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        final StringBuilder response = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            response.append(line);
            response.append('\r');
        }
        reader.close();
        return response.toString();
    }
    
    public static String performPost(final URL url, final String body, final String contentType) throws IOException {
        return performPost(url, body.getBytes(StandardCharsets.UTF_8), contentType, false);
    }
    
    public static URL constantURL(final String input) {
        try {
            return new URL(input);
        }
        catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static String encode(final String s) {
        try {
            return URLEncoder.encode(s, "UTF-8").replaceAll("\\+", "%20").replaceAll("%3A", ":").replaceAll("%2F", "/").replaceAll("%21", "!").replaceAll("%27", "'").replaceAll("%28", "(").replaceAll("%29", ")").replaceAll("%7E", "~");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 is not supported.", e);
        }
    }
    
    public static String readBody(final Socket socket) throws IOException {
        final BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        String line = in.readLine();
        final StringBuilder raw = new StringBuilder();
        raw.append(line);
        final boolean isPost = raw.toString().startsWith("POST");
        int contentLength = 0;
        while (!(line = in.readLine()).equals("")) {
            raw.append('\n').append(line);
            if (isPost) {
                final String contentHeader = "Content-Length: ";
                if (!line.startsWith("Content-Length: ")) {
                    continue;
                }
                contentLength = Integer.parseInt(line.substring("Content-Length: ".length()));
            }
        }
        final StringBuilder body = new StringBuilder();
        if (isPost) {
            for (int i = 0; i < contentLength; ++i) {
                final int c = in.read();
                body.append((char)c);
            }
        }
        raw.append(body.toString());
        out.write("HTTP/1.1 200 OK\r\n");
        out.write("Content-Type: text/html\r\n");
        out.write("Access-Control-Allow-Origin: *\r\n");
        out.write("\r\n");
        out.close();
        in.close();
        socket.close();
        return body.toString();
    }
    
    private static String buildQuery(final Map<String, Object> query) {
        final StringBuilder builder = new StringBuilder();
        for (final Map.Entry<String, Object> entry : query.entrySet()) {
            if (builder.length() > 0) {
                builder.append('&');
            }
            try {
                builder.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            }
            catch (UnsupportedEncodingException e) {
                U.log("Unexpected exception building query", e);
            }
            if (entry.getValue() != null) {
                builder.append('=');
                try {
                    builder.append(URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
                }
                catch (UnsupportedEncodingException e) {
                    U.log(e);
                }
            }
        }
        return builder.toString();
    }
    
    private static String readZip(final HttpURLConnection connection) throws IOException {
        final ZipInputStream zip = new ZipInputStream(connection.getInputStream(), StandardCharsets.UTF_8);
        zip.getNextEntry();
        final String res = IOUtils.toString(zip);
        zip.closeEntry();
        zip.close();
        return res;
    }
}
