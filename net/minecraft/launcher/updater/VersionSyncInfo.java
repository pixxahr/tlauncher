// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import org.tlauncher.util.FileUtil;
import java.util.Objects;
import java.util.Iterator;
import org.tlauncher.tlauncher.repository.Repo;
import net.minecraft.launcher.versions.Library;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.HashSet;
import org.tlauncher.tlauncher.downloader.Downloadable;
import java.util.Set;
import java.io.File;
import org.tlauncher.util.OS;
import java.io.IOException;
import org.tlauncher.tlauncher.managers.VersionManager;
import net.minecraft.launcher.versions.CompleteVersion;
import net.minecraft.launcher.versions.Version;

public class VersionSyncInfo
{
    Version localVersion;
    Version remoteVersion;
    private CompleteVersion completeLocal;
    private CompleteVersion completeRemote;
    private String id;
    
    public VersionSyncInfo(final Version localVersion, final Version remoteVersion) {
        if (localVersion == null && remoteVersion == null) {
            throw new NullPointerException("Cannot createScrollWrapper sync info from NULLs!");
        }
        this.localVersion = localVersion;
        this.remoteVersion = remoteVersion;
        if (localVersion != null && remoteVersion != null) {
            localVersion.setVersionList(remoteVersion.getVersionList());
        }
        if (this.getID() == null) {
            throw new NullPointerException("Cannot createScrollWrapper sync info from versions that have NULL IDs");
        }
    }
    
    public VersionSyncInfo(final VersionSyncInfo info) {
        this(info.getLocal(), info.getRemote());
    }
    
    private VersionSyncInfo() {
        this.localVersion = null;
        this.remoteVersion = null;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (this.getID() == null || !(o instanceof VersionSyncInfo)) {
            return false;
        }
        final VersionSyncInfo v = (VersionSyncInfo)o;
        return this.getID().equals(v.getID());
    }
    
    public Version getLocal() {
        return this.localVersion;
    }
    
    public void setLocal(final Version version) {
        this.localVersion = version;
        if (version instanceof CompleteVersion) {
            this.completeLocal = (CompleteVersion)version;
        }
    }
    
    public Version getRemote() {
        return this.remoteVersion;
    }
    
    public void setRemote(final Version version) {
        this.remoteVersion = version;
        if (version instanceof CompleteVersion) {
            this.completeRemote = (CompleteVersion)version;
        }
    }
    
    public String getID() {
        if (this.id != null) {
            return this.id;
        }
        if (this.localVersion != null) {
            return this.localVersion.getID();
        }
        if (this.remoteVersion != null) {
            return this.remoteVersion.getID();
        }
        return null;
    }
    
    public void setID(final String id) {
        if (id != null && id.isEmpty()) {
            throw new IllegalArgumentException("ID cannot be empty!");
        }
        this.id = id;
    }
    
    public Version getLatestVersion() {
        if (this.remoteVersion != null) {
            return this.remoteVersion;
        }
        return this.localVersion;
    }
    
    public Version getAvailableVersion() {
        if (this.localVersion != null) {
            return this.localVersion;
        }
        return this.remoteVersion;
    }
    
    public boolean isInstalled() {
        return this.localVersion != null;
    }
    
    public boolean hasRemote() {
        return this.remoteVersion != null;
    }
    
    public boolean isUpToDate() {
        return this.localVersion != null && (this.remoteVersion == null || this.localVersion.getUpdatedTime().compareTo(this.remoteVersion.getUpdatedTime()) >= 0);
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{id='" + this.getID() + "',\nlocal=" + this.localVersion + ",\nremote=" + this.remoteVersion + ", isInstalled=" + this.isInstalled() + ", hasRemote=" + this.hasRemote() + ", isUpToDate=" + this.isUpToDate() + "}";
    }
    
    public CompleteVersion resolveCompleteVersion(final VersionManager manager, final boolean latest) throws IOException {
        Version version;
        if (latest) {
            version = this.getLatestVersion();
        }
        else if (this.isInstalled()) {
            version = this.getLocal();
        }
        else {
            version = this.getRemote();
        }
        if (version.equals(this.localVersion) && this.completeLocal != null && this.completeLocal.getInheritsFrom() == null) {
            return this.completeLocal;
        }
        if (version.equals(this.remoteVersion) && this.completeRemote != null && this.completeRemote.getInheritsFrom() == null) {
            return this.completeRemote;
        }
        final CompleteVersion complete = version.getVersionList().getCompleteVersion(version).resolve(manager, latest);
        if (version == this.localVersion) {
            this.completeLocal = complete;
        }
        else if (version == this.remoteVersion) {
            this.completeRemote = complete;
        }
        return complete;
    }
    
    public CompleteVersion getCompleteVersion(final boolean latest) throws IOException {
        Version version;
        if (latest) {
            version = this.getLatestVersion();
        }
        else if (this.isInstalled()) {
            version = this.getLocal();
        }
        else {
            version = this.getRemote();
        }
        if (version.equals(this.localVersion) && this.completeLocal != null) {
            return this.completeLocal;
        }
        if (version.equals(this.remoteVersion) && this.completeRemote != null) {
            return this.completeRemote;
        }
        final CompleteVersion complete = version.getVersionList().getCompleteVersion(version);
        if (version == this.localVersion) {
            this.completeLocal = complete;
        }
        else if (version == this.remoteVersion) {
            this.completeRemote = complete;
        }
        return complete;
    }
    
    public CompleteVersion getLatestCompleteVersion() throws IOException {
        return this.getCompleteVersion(true);
    }
    
    public CompleteVersion getLocalCompleteVersion() {
        return this.completeLocal;
    }
    
    private Set<Downloadable> getRequiredDownloadables(final OS os, final File targetDirectory, final boolean force, final boolean tlauncher) throws IOException {
        final Set<Downloadable> neededFiles = new HashSet<Downloadable>();
        CompleteVersion version = this.getCompleteVersion(force);
        version = TLauncher.getInstance().getTLauncherManager().addReplacedLibraries(version, tlauncher);
        final Repo source = this.hasRemote() ? this.remoteVersion.getSource() : ClientInstanceRepo.OFFICIAL_VERSION_REPO;
        if (!source.isSelectable()) {
            return neededFiles;
        }
        for (final Library library : version.getRelevantLibraries()) {
            final File local = this.analizeFolderLibrary(os, targetDirectory, force, library);
            if (local == null) {
                continue;
            }
            neededFiles.add(library.getDownloadable(source, local, os));
        }
        if (version.getModsLibraries() != null) {
            for (final Library library : version.getModsLibraries()) {
                final File local = this.analizeFolderLibrary(os, targetDirectory, force, library);
                if (local == null) {
                    continue;
                }
                neededFiles.add(library.getDownloadable(source, local, os));
            }
        }
        return neededFiles;
    }
    
    private File analizeFolderLibrary(final OS os, final File targetDirectory, final boolean force, final Library library) {
        String file = null;
        if (library.getNatives() != null) {
            final String natives = library.getNatives().get(os);
            if (natives != null) {
                file = library.getArtifactPath(natives);
            }
        }
        else {
            file = library.getArtifactPath();
        }
        if (file == null) {
            return null;
        }
        final File local = new File(targetDirectory, "libraries/" + file);
        if (!force && local.isFile()) {
            if (library.getChecksum() == null || Objects.nonNull(library.getDeleteEntriesList())) {
                return null;
            }
            if (library.getChecksum().equals(FileUtil.getChecksum(local, "SHA-1"))) {
                return null;
            }
        }
        return local;
    }
    
    public Set<Downloadable> getRequiredDownloadables(final File targetDirectory, final boolean force, final boolean tlauncher) throws IOException {
        return this.getRequiredDownloadables(OS.CURRENT, targetDirectory, force, tlauncher);
    }
    
    public static VersionSyncInfo createEmpty() {
        return new VersionSyncInfo();
    }
}
