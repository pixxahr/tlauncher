// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import net.minecraft.launcher.versions.Version;
import org.tlauncher.util.OS;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;
import java.util.Map;
import java.util.Arrays;
import java.util.HashSet;
import net.minecraft.launcher.versions.json.ArgumentType;
import net.minecraft.launcher.versions.json.Argument;
import java.util.Objects;
import java.util.List;
import java.util.Iterator;
import org.tlauncher.util.U;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.util.FileUtil;
import java.io.IOException;
import java.io.File;

public class LocalVersionList extends StreamVersionList
{
    private File baseDirectory;
    private File baseVersionsDir;
    
    public LocalVersionList(final File baseDirectory) throws IOException {
        this.setBaseDirectory(baseDirectory);
    }
    
    public File getBaseDirectory() {
        return this.baseDirectory;
    }
    
    public void setBaseDirectory(final File directory) throws IOException {
        if (directory == null) {
            throw new IllegalArgumentException("Base directory is NULL!");
        }
        FileUtil.createFolder(directory);
        this.log("Base directory:", directory.getAbsolutePath());
        this.baseDirectory = directory;
        this.baseVersionsDir = new File(this.baseDirectory, "versions");
    }
    
    @Override
    public synchronized void refreshVersions() {
        this.clearCache();
        final File[] files = this.baseVersionsDir.listFiles();
        if (files == null) {
            return;
        }
        for (final File directory : files) {
            final String id = directory.getName();
            final File jsonFile = new File(directory, id + ".json");
            if (directory.isDirectory()) {
                if (jsonFile.isFile()) {
                    try {
                        CompleteVersion version = this.gson.fromJson(this.getUrl("versions/" + id + "/" + id + ".json"), CompleteVersion.class);
                        if (version == null) {
                            this.log("JSON descriptor of version \"" + id + "\" in NULL, it won't be added in list as local.");
                        }
                        else {
                            version.setID(id);
                            version.setSource(ClientInstanceRepo.LOCAL_VERSION_REPO);
                            version.setVersionList(this);
                            if (version.getID().equals("17w43b") && version.getArguments() == null) {
                                this.log("skip 17w43b version ");
                            }
                            else {
                                boolean save = false;
                                if (version.getID().length() > 256) {
                                    version.setID(id);
                                    save = true;
                                }
                                if (version.isModpack()) {
                                    if (version.getModpack().getName().length() > 256) {
                                        version.getModpack().setName(id);
                                        save = true;
                                    }
                                    for (final GameType type : GameType.getSubEntities()) {
                                        final List<? extends GameEntityDTO> list = ((ModpackVersionDTO)version.getModpack().getVersion()).getByType(type);
                                        final Iterator<? extends GameEntityDTO> it = list.iterator();
                                        while (it.hasNext()) {
                                            final GameEntityDTO el = (GameEntityDTO)it.next();
                                            if (type.equals(GameType.MAP)) {
                                                if (el.getName().length() <= 256 && el.getVersion().getMetadata().getPath().length() <= 256) {
                                                    continue;
                                                }
                                                it.remove();
                                                save = true;
                                            }
                                            else {
                                                if (el.getName().length() <= 256) {
                                                    continue;
                                                }
                                                save = true;
                                                el.setName("fixed_bug_2.49_" + U.n());
                                            }
                                        }
                                    }
                                }
                                if (version.getID().equalsIgnoreCase(version.getInheritsFrom())) {
                                    version = this.renameVersion(version, this.findNewVersionName(version.getID(), 0));
                                }
                                if (save) {
                                    this.saveVersion(version);
                                    U.gc();
                                }
                                this.checkDoubleArgument(version);
                                this.addVersion(version);
                            }
                        }
                    }
                    catch (Throwable ex) {
                        this.log("Error occurred while parsing local version", id, ex);
                    }
                }
            }
        }
    }
    
    private void checkDoubleArgument(final CompleteVersion version) throws IOException {
        final Map<ArgumentType, List<Argument>> map = version.getArguments();
        if (Objects.nonNull(map)) {
            final List<Argument> list = map.get(ArgumentType.GAME);
            if (Objects.isNull(list)) {
                return;
            }
            if (list.size() > 30) {
                final Set<String> set = new HashSet<String>();
                final String value;
                final Set<String> set2;
                list.removeIf(e -> {
                    value = Arrays.toString(e.getValues());
                    if (set2.contains(value)) {
                        U.log("removed arg " + e);
                        return true;
                    }
                    else {
                        set2.add(value);
                        return false;
                    }
                });
                this.saveVersion(version);
            }
        }
    }
    
    private String findNewVersionName(final String name, int i) {
        final String newName = name + " fixed " + i;
        final File f = new File(this.baseVersionsDir, newName);
        if (f.exists()) {
            return this.findNewVersionName(name, ++i);
        }
        return newName;
    }
    
    public synchronized void saveVersion(final CompleteVersion version) throws IOException {
        final String text = this.serializeVersion(version);
        final File target = new File(this.baseVersionsDir, version.getID() + "/" + version.getID() + ".json");
        FileUtil.writeFile(target, text);
    }
    
    public synchronized void deleteVersion(final String id, final boolean deleteLibraries) throws IOException {
        final CompleteVersion version = this.getCompleteVersion(id);
        if (version == null) {
            throw new IllegalArgumentException("Version is not installed! id = " + id);
        }
        final File dir = new File(this.baseVersionsDir, id + '/');
        if (!dir.isDirectory()) {
            throw new IOException("Cannot find directory: " + dir.getAbsolutePath());
        }
        FileUtil.deleteDirectory(dir);
        if (!deleteLibraries) {
            return;
        }
        for (final File library : version.getClassPath(this.baseDirectory)) {
            FileUtil.deleteFile(library);
        }
        for (final String nativeLib : version.getNatives()) {
            FileUtil.deleteFile(new File(this.baseDirectory, nativeLib));
        }
    }
    
    @Override
    protected InputStream getInputStream(final String uri) throws IOException {
        return new FileInputStream(new File(this.baseDirectory, uri));
    }
    
    @Override
    public boolean hasAllFiles(final CompleteVersion version, final OS os) {
        final Set<String> files = version.getRequiredFiles(os);
        for (final String file : files) {
            final File required = new File(this.baseDirectory, file);
            if (!required.isFile() || required.length() == 0L) {
                return false;
            }
        }
        return true;
    }
    
    public synchronized CompleteVersion renameVersion(final CompleteVersion version, final String name) throws IOException {
        final CompleteVersion newVersion = version.fullCopy(new CompleteVersion());
        newVersion.setID(name);
        if (Objects.nonNull(version.getModpack())) {
            newVersion.getModpack().setName(name);
        }
        final File newFolder = new File(this.baseVersionsDir.toString(), newVersion.getID());
        if (newFolder.exists()) {
            throw new IOException("folder exists " + newFolder);
        }
        final String oldName = version.getID();
        final File oldJar = new File(this.baseVersionsDir.toString(), version.getID() + "/" + version.getID() + ".jar");
        final File newJar = new File(this.baseVersionsDir.toString(), version.getID() + "/" + newVersion.getID() + ".jar");
        if (oldJar.exists() && !oldJar.renameTo(newJar)) {
            throw new IOException("can't rename from " + oldJar + "to " + newJar);
        }
        final File oldFolder = new File(this.baseVersionsDir.toString(), oldName);
        if (!oldFolder.renameTo(newFolder)) {
            throw new IOException("can't rename from " + version.getID() + "to " + newVersion.getID());
        }
        FileUtil.deleteFile(new File(this.baseVersionsDir.toString(), newVersion.getID() + "/" + version.getID() + ".json"));
        this.saveVersion(newVersion);
        return newVersion;
    }
    
    public synchronized void refreshLocalVersion(final CompleteVersion version) throws IOException {
        this.saveVersion(version);
        version.setSource(ClientInstanceRepo.LOCAL_VERSION_REPO);
        version.setVersionList(this);
        this.byName.put(version.getID(), version);
        for (int i = 0; i < this.versions.size(); ++i) {
            if (this.versions.get(i).getID().equalsIgnoreCase(version.getID())) {
                this.versions.remove(i);
                this.versions.add(i, version);
                return;
            }
        }
    }
}
