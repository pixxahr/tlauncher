// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import net.minecraft.launcher.versions.Version;
import net.minecraft.launcher.versions.ReleaseType;

public class LatestVersionSyncInfo extends VersionSyncInfo
{
    private final ReleaseType type;
    
    public LatestVersionSyncInfo(final ReleaseType type, final Version localVersion, final Version remoteVersion) {
        super(localVersion, remoteVersion);
        if (type == null) {
            throw new NullPointerException("ReleaseType cannot be NULL!");
        }
        this.type = type;
        this.setID("latest-" + type.toString());
    }
    
    public LatestVersionSyncInfo(final ReleaseType type, final VersionSyncInfo syncInfo) {
        this(type, syncInfo.getLocal(), syncInfo.getRemote());
    }
    
    public String getVersionID() {
        if (this.localVersion != null) {
            return this.localVersion.getID();
        }
        if (this.remoteVersion != null) {
            return this.remoteVersion.getID();
        }
        return null;
    }
    
    public ReleaseType getReleaseType() {
        return this.type;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{id='" + this.getID() + "', releaseType=" + this.type + ",\nlocal=" + this.localVersion + ",\nremote=" + this.remoteVersion + ", isInstalled=" + this.isInstalled() + ", hasRemote=" + this.hasRemote() + ", isUpToDate=" + this.isUpToDate() + "}";
    }
}
