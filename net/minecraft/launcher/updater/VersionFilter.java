// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import net.minecraft.launcher.versions.Version;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import net.minecraft.launcher.versions.ReleaseType;
import java.util.Set;

public class VersionFilter
{
    private final Set<ReleaseType> types;
    private final Set<ReleaseType.SubType> subTypes;
    
    public VersionFilter() {
        this.types = new HashSet<ReleaseType>(ReleaseType.valuesCollection());
        this.subTypes = new HashSet<ReleaseType.SubType>(ReleaseType.SubType.valuesCollection());
    }
    
    public Set<ReleaseType> getTypes() {
        return this.types;
    }
    
    public Set<ReleaseType.SubType> getSubTypes() {
        return this.subTypes;
    }
    
    public VersionFilter onlyForType(final ReleaseType... types) {
        this.types.clear();
        this.include(types);
        return this;
    }
    
    public VersionFilter onlyForType(final ReleaseType.SubType... subTypes) {
        this.subTypes.clear();
        this.include(subTypes);
        return this;
    }
    
    public VersionFilter include(final ReleaseType... types) {
        if (types != null) {
            Collections.addAll(this.types, types);
        }
        return this;
    }
    
    public VersionFilter include(final ReleaseType.SubType... types) {
        if (types != null) {
            Collections.addAll(this.subTypes, types);
        }
        return this;
    }
    
    public VersionFilter exclude(final ReleaseType... types) {
        if (types != null) {
            for (final ReleaseType type : types) {
                this.types.remove(type);
            }
        }
        return this;
    }
    
    public VersionFilter exclude(final ReleaseType.SubType... types) {
        if (types != null) {
            for (final ReleaseType.SubType type : types) {
                this.subTypes.remove(type);
            }
        }
        return this;
    }
    
    public boolean satisfies(final Version v) {
        final ReleaseType releaseType = v.getReleaseType();
        if (releaseType == null) {
            return true;
        }
        if (!this.types.contains(releaseType)) {
            return false;
        }
        final ReleaseType.SubType subType = ReleaseType.SubType.get(v);
        return subType == null || this.subTypes.contains(subType);
    }
    
    @Override
    public String toString() {
        return "VersionFilter" + this.types;
    }
}
