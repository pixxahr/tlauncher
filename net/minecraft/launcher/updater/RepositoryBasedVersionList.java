// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import org.tlauncher.util.OS;
import com.google.gson.JsonSyntaxException;
import net.minecraft.launcher.versions.CompleteVersion;
import java.io.IOException;
import net.minecraft.launcher.versions.Version;
import java.util.Iterator;
import net.minecraft.launcher.versions.PartialVersion;
import org.tlauncher.tlauncher.repository.Repo;

public class RepositoryBasedVersionList extends RemoteVersionList
{
    protected final Repo repo;
    
    RepositoryBasedVersionList(final Repo repo) {
        if (repo == null) {
            throw new NullPointerException();
        }
        this.repo = repo;
    }
    
    @Override
    public RawVersionList getRawList() throws IOException {
        final RawVersionList rawList = super.getRawList();
        for (final Version version : rawList.getVersions()) {
            version.setSource(this.repo);
        }
        return rawList;
    }
    
    @Override
    public CompleteVersion getCompleteVersion(final Version version) throws JsonSyntaxException, IOException {
        final CompleteVersion complete = super.getCompleteVersion(version);
        complete.setSource(this.repo);
        return complete;
    }
    
    @Override
    public boolean hasAllFiles(final CompleteVersion paramCompleteVersion, final OS paramOperatingSystem) {
        return true;
    }
    
    @Override
    protected String getUrl(final String uri) throws IOException {
        return this.repo.getUrl(uri);
    }
}
