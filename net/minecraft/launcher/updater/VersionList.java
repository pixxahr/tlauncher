// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import java.util.EnumMap;
import org.tlauncher.util.U;
import org.tlauncher.util.OS;
import java.util.Iterator;
import net.minecraft.launcher.versions.PartialVersion;
import org.tlauncher.util.Time;
import java.io.IOException;
import com.google.gson.JsonSyntaxException;
import net.minecraft.launcher.versions.CompleteVersion;
import java.lang.reflect.Type;
import net.minecraft.launcher.versions.json.DateTypeAdapter;
import java.util.Date;
import com.google.gson.TypeAdapterFactory;
import net.minecraft.launcher.versions.json.LowerCaseEnumTypeAdapterFactory;
import com.google.gson.GsonBuilder;
import java.util.Hashtable;
import java.util.Collections;
import java.util.ArrayList;
import net.minecraft.launcher.versions.ReleaseType;
import java.util.List;
import net.minecraft.launcher.versions.Version;
import java.util.Map;
import com.google.gson.Gson;

public abstract class VersionList
{
    public final Gson gson;
    protected final Map<String, Version> byName;
    protected final List<Version> versions;
    protected final Map<ReleaseType, Version> latest;
    
    VersionList() {
        this.versions = Collections.synchronizedList(new ArrayList<Version>());
        this.byName = new Hashtable<String, Version>();
        this.latest = new Hashtable<ReleaseType, Version>();
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        builder.registerTypeAdapter(CompleteVersion.class, new CompleteVersion.CompleteVersionSerializer());
        builder.enableComplexMapKeySerialization();
        builder.setPrettyPrinting();
        builder.disableHtmlEscaping();
        this.gson = builder.create();
    }
    
    public List<Version> getVersions() {
        return this.versions;
    }
    
    public Map<ReleaseType, Version> getLatestVersions() {
        return this.latest;
    }
    
    public Version getVersion(final String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be NULL or empty");
        }
        return this.byName.get(name);
    }
    
    public CompleteVersion getCompleteVersion(final Version version) throws JsonSyntaxException, IOException {
        if (version instanceof CompleteVersion) {
            return (CompleteVersion)version;
        }
        if (version == null) {
            throw new NullPointerException("Version cannot be NULL!");
        }
        final CompleteVersion complete = this.gson.fromJson(this.getUrl("versions/" + version.getID() + "/" + version.getID() + ".json"), CompleteVersion.class);
        complete.setID(version.getID());
        complete.setVersionList(this);
        complete.setSkinVersion(version.isSkinVersion());
        Collections.replaceAll(this.versions, version, complete);
        return complete;
    }
    
    public CompleteVersion getCompleteVersion(final String name) throws JsonSyntaxException, IOException {
        final Version version = this.getVersion(name);
        if (version == null) {
            return null;
        }
        return this.getCompleteVersion(version);
    }
    
    public Version getLatestVersion(final ReleaseType type) {
        if (type == null) {
            throw new NullPointerException();
        }
        return this.latest.get(type);
    }
    
    public RawVersionList getRawList() throws IOException {
        final Object lock = new Object();
        Time.start(lock);
        final RawVersionList list = this.gson.fromJson(this.getUrl("versions/versions.json"), RawVersionList.class);
        for (final PartialVersion version : list.versions) {
            version.setVersionList(this);
        }
        this.log("Got in", Time.stop(lock), "ms");
        return list;
    }
    
    public void refreshVersions(final RawVersionList versionList) {
        this.clearCache();
        for (final Version version : versionList.getVersions()) {
            if (version != null) {
                if (version.getID() == null) {
                    continue;
                }
                this.versions.add(version);
                this.byName.put(version.getID(), version);
            }
        }
        for (final Map.Entry<ReleaseType, String> en : versionList.latest.entrySet()) {
            final ReleaseType releaseType = en.getKey();
            if (releaseType == null) {
                this.log("Unknown release type for latest version entry:", en);
            }
            else {
                final Version version2 = this.getVersion(en.getValue());
                if (version2 == null) {
                    throw new NullPointerException("Cannot find version for latest version entry: " + en);
                }
                this.latest.put(releaseType, version2);
            }
        }
    }
    
    public void refreshVersions() throws IOException {
        this.refreshVersions(this.getRawList());
    }
    
    protected CompleteVersion addVersion(final CompleteVersion version) {
        if (version.getID() == null) {
            throw new IllegalArgumentException("Cannot add blank version");
        }
        if (this.getVersion(version.getID()) != null) {
            this.log("Version '" + version.getID() + "' is already tracked");
            return version;
        }
        this.versions.add(version);
        this.byName.put(version.getID(), version);
        return version;
    }
    
    void removeVersion(final Version version) {
        if (version == null) {
            throw new NullPointerException("Version cannot be NULL!");
        }
        this.versions.remove(version);
        this.byName.remove(version);
    }
    
    public void removeVersion(final String name) {
        final Version version = this.getVersion(name);
        if (version == null) {
            return;
        }
        this.removeVersion(version);
    }
    
    public String serializeVersion(final CompleteVersion version) {
        if (version == null) {
            throw new NullPointerException("CompleteVersion cannot be NULL!");
        }
        return this.gson.toJson(version);
    }
    
    public abstract boolean hasAllFiles(final CompleteVersion p0, final OS p1);
    
    protected abstract String getUrl(final String p0) throws IOException;
    
    protected void clearCache() {
        this.byName.clear();
        this.versions.clear();
        this.latest.clear();
    }
    
    void log(final Object... obj) {
        U.log("[" + this.getClass().getSimpleName() + "]", obj);
    }
    
    public static class RawVersionList
    {
        List<PartialVersion> versions;
        Map<ReleaseType, String> latest;
        
        public RawVersionList() {
            this.versions = new ArrayList<PartialVersion>();
            this.latest = new EnumMap<ReleaseType, String>(ReleaseType.class);
        }
        
        public List<PartialVersion> getVersions() {
            return this.versions;
        }
        
        public Map<ReleaseType, String> getLatestVersions() {
            return this.latest;
        }
    }
}
