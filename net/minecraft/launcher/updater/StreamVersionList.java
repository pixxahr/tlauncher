// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.commons.io.Charsets;

public abstract class StreamVersionList extends VersionList
{
    @Override
    protected String getUrl(final String uri) throws IOException {
        final InputStream inputStream = this.getInputStream(uri);
        final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charsets.UTF_8));
        final StringBuilder result = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            if (result.length() > 0) {
                result.append('\n');
            }
            result.append(line);
        }
        reader.close();
        return result.toString();
    }
    
    protected abstract InputStream getInputStream(final String p0) throws IOException;
}
