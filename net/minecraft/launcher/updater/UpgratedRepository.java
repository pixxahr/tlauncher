// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import com.google.gson.JsonSyntaxException;
import java.util.Collections;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import net.minecraft.launcher.versions.CompleteVersion;
import java.io.IOException;
import net.minecraft.launcher.versions.Version;
import java.util.Iterator;
import net.minecraft.launcher.versions.PartialVersion;
import org.tlauncher.util.Time;
import org.tlauncher.tlauncher.repository.Repo;

public abstract class UpgratedRepository extends RepositoryBasedVersionList
{
    UpgratedRepository(final Repo repo) {
        super(repo);
    }
    
    @Override
    public RawVersionList getRawList() throws IOException {
        final Object lock = new Object();
        Time.start(lock);
        final RawVersionList list = this.gson.fromJson(this.repo.getUrl("version_manifest.json"), RawVersionList.class);
        for (final PartialVersion version : list.versions) {
            version.setVersionList(this);
        }
        this.log("Got in", Time.stop(lock), "ms");
        for (final Version version2 : list.getVersions()) {
            version2.setSource(this.repo);
        }
        return list;
    }
    
    @Override
    public CompleteVersion getCompleteVersion(final Version version) throws JsonSyntaxException, IOException {
        if (version instanceof CompleteVersion) {
            return (CompleteVersion)version;
        }
        if (version == null) {
            throw new NullPointerException("Version cannot be NULL!");
        }
        final CompleteVersion complete = this.gson.fromJson(ClientInstanceRepo.EMPTY_REPO.getUrl(version.getUrl()), CompleteVersion.class);
        complete.setID(version.getID());
        complete.setVersionList(this);
        complete.setSkinVersion(version.isSkinVersion());
        complete.setUpdatedTime(version.getUpdatedTime());
        Collections.replaceAll(this.versions, version, complete);
        complete.setSource(this.repo);
        return complete;
    }
}
