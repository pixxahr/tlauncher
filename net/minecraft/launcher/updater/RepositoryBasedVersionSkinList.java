// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.updater;

import java.io.IOException;
import net.minecraft.launcher.versions.Version;
import java.util.Iterator;
import net.minecraft.launcher.versions.PartialVersion;
import org.tlauncher.util.Time;
import org.tlauncher.tlauncher.repository.Repo;

public class RepositoryBasedVersionSkinList extends RepositoryBasedVersionList
{
    public RepositoryBasedVersionSkinList(final Repo repo) {
        super(repo);
    }
    
    @Override
    public RawVersionList getRawList() throws IOException {
        final Object lock = new Object();
        Time.start(lock);
        final RawVersionList list = this.gson.fromJson(this.getUrl("versions/versions-1.0.json"), RawVersionList.class);
        for (final PartialVersion version : list.versions) {
            version.setVersionList(this);
        }
        this.log("Got in", Time.stop(lock), "ms");
        for (final Version version2 : list.getVersions()) {
            version2.setSource(this.repo);
        }
        return list;
    }
}
