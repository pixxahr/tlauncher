// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.process;

public interface JavaProcessListener
{
    void onJavaProcessLog(final JavaProcess p0, final String p1);
    
    void onJavaProcessEnded(final JavaProcess p0);
    
    void onJavaProcessError(final JavaProcess p0, final Throwable p1);
}
