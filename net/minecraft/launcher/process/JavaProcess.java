// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.process;

import java.util.List;

public class JavaProcess
{
    private static final int MAX_SYSOUT_LINES = 5;
    private final List<String> commands;
    private final Process process;
    private final LimitedCapacityList<String> sysOutLines;
    
    public JavaProcess(final List<String> commands, final Process process, final JavaProcessListener listener) {
        this.sysOutLines = new LimitedCapacityList<String>(String.class, 5);
        this.commands = commands;
        this.process = process;
        final ProcessMonitorThread monitor = new ProcessMonitorThread(this, listener);
        monitor.start();
    }
    
    public Process getRawProcess() {
        return this.process;
    }
    
    public List<String> getStartupCommands() {
        return this.commands;
    }
    
    public String getStartupCommand() {
        return this.process.toString();
    }
    
    public LimitedCapacityList<String> getSysOutLines() {
        return this.sysOutLines;
    }
    
    public boolean isRunning() {
        try {
            this.process.exitValue();
        }
        catch (IllegalThreadStateException ex) {
            return true;
        }
        return false;
    }
    
    public int getExitCode() {
        try {
            return this.process.exitValue();
        }
        catch (IllegalThreadStateException ex) {
            ex.fillInStackTrace();
            throw ex;
        }
    }
    
    @Override
    public String toString() {
        return "JavaProcess[commands=" + this.commands + ", isRunning=" + this.isRunning() + "]";
    }
    
    public void stop() {
        this.process.destroy();
    }
}
