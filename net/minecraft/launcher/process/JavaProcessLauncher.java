// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.process;

import java.util.Iterator;
import org.tlauncher.util.TlauncherUtil;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import java.io.File;
import java.util.List;

public class JavaProcessLauncher
{
    private final String jvmPath;
    private final List<String> commands;
    private File directory;
    private ProcessBuilder process;
    private JavaProcessListener listener;
    
    public JavaProcessLauncher(final String jvmPath, final String[] commands) {
        this.jvmPath = jvmPath;
        Collections.addAll(this.commands = new ArrayList<String>(), commands);
    }
    
    public JavaProcess start() throws IOException {
        final List<String> full = this.getFullCommands();
        return new JavaProcess(full, this.createProcess().start(), this.listener);
    }
    
    public ProcessBuilder createProcess() {
        if (this.process == null) {
            this.process = new ProcessBuilder(this.getFullCommands()).directory(this.directory).redirectErrorStream(true);
        }
        if (TlauncherUtil.contains_OPTIONS_JAVA_XMX()) {
            this.process.environment().put("_JAVA_OPTIONS", "");
        }
        return this.process;
    }
    
    private List<String> getFullCommands() {
        final List<String> result = new ArrayList<String>(this.commands);
        result.add(0, this.jvmPath);
        return result;
    }
    
    public String getCommandsAsString() {
        final List<String> parts = this.getFullCommands();
        final StringBuilder full = new StringBuilder();
        boolean first = true;
        for (final String part : parts) {
            if (first) {
                first = false;
            }
            else {
                full.append(' ');
            }
            full.append(part);
        }
        return full.toString();
    }
    
    public List<String> getCommands() {
        return this.commands;
    }
    
    public void addCommand(final Object command) {
        this.commands.add(command.toString());
    }
    
    public void addCommand(final Object key, final Object value) {
        this.commands.add(key.toString());
        this.commands.add(value.toString());
    }
    
    public void addCommands(final Object[] commands) {
        for (final Object c : commands) {
            this.commands.add(c.toString());
        }
    }
    
    public void addSplitCommands(final Object commands) {
        this.addCommands(commands.toString().split(" "));
    }
    
    public JavaProcessLauncher directory(final File directory) {
        this.directory = directory;
        return this;
    }
    
    public File getDirectory() {
        return this.directory;
    }
    
    @Override
    public String toString() {
        return "JavaProcessLauncher[commands=" + this.commands + ", java=" + this.jvmPath + "]";
    }
    
    public void setListener(final JavaProcessListener listener) {
        this.listener = listener;
    }
}
