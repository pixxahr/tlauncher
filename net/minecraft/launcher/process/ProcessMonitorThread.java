// 
// Decompiled by Procyon v0.5.36
// 

package net.minecraft.launcher.process;

import java.util.Objects;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class ProcessMonitorThread extends Thread
{
    private final JavaProcess process;
    private JavaProcessListener listener;
    
    public ProcessMonitorThread(final JavaProcess process, final JavaProcessListener listener) {
        this.process = process;
        this.listener = listener;
    }
    
    @Override
    public void run() {
        final Process raw = this.process.getRawProcess();
        final InputStreamReader reader = new InputStreamReader(raw.getInputStream());
        final BufferedReader buf = new BufferedReader(reader);
        while (this.process.isRunning()) {
            try {
                String line;
                while ((line = buf.readLine()) != null) {
                    if (this.listener != null) {
                        this.listener.onJavaProcessLog(this.process, line);
                    }
                    this.process.getSysOutLines().add(line);
                }
            }
            catch (Throwable t) {
                try {
                    buf.close();
                }
                catch (IOException ex) {
                    Logger.getLogger(ProcessMonitorThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            finally {
                try {
                    buf.close();
                }
                catch (IOException ex2) {
                    Logger.getLogger(ProcessMonitorThread.class.getName()).log(Level.SEVERE, null, ex2);
                }
            }
        }
        if (Objects.nonNull(this.listener)) {
            this.listener.onJavaProcessEnded(this.process);
        }
    }
}
