// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import org.tlauncher.tlauncher.minecraft.auth.AccountListener;

public interface ProfileManagerListener extends AccountListener
{
    void onProfilesRefreshed(final ProfileManager p0);
    
    void onProfileManagerChanged(final ProfileManager p0);
}
