// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import java.io.IOException;
import com.google.gson.JsonSyntaxException;
import org.tlauncher.util.FileUtil;
import java.util.Objects;
import java.util.ArrayList;
import java.util.Iterator;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import java.util.HashSet;
import java.util.Set;
import java.io.File;
import org.tlauncher.tlauncher.downloader.Downloadable;
import java.util.Collection;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;
import net.minecraft.launcher.updater.AssetIndex;
import java.util.List;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.tlauncher.rmo.TLauncher;
import com.google.gson.Gson;
import org.tlauncher.tlauncher.component.ComponentDependence;
import org.tlauncher.tlauncher.component.LauncherComponent;

@ComponentDependence({ VersionManager.class, VersionLists.class })
public class AssetsManager extends LauncherComponent
{
    private final Gson gson;
    private final Object assetsFlushLock;
    
    public AssetsManager(final ComponentManager manager) throws Exception {
        super(manager);
        this.gson = TLauncher.getGson();
        this.assetsFlushLock = new Object();
    }
    
    public DownloadableContainer downloadResources(final CompleteVersion version, final List<AssetIndex.AssetObject> list, final boolean force) {
        final File baseDirectory = this.manager.getLauncher().getVersionManager().getLocalList().getBaseDirectory();
        final DownloadableContainer container = new DownloadableContainer();
        container.addAll(getResourceFiles(version, baseDirectory, list));
        return container;
    }
    
    private static Set<Downloadable> getResourceFiles(final CompleteVersion version, final File baseDirectory, final List<AssetIndex.AssetObject> list) {
        final Set<Downloadable> result = new HashSet<Downloadable>();
        final File objectsFolder = new File(baseDirectory, "assets/objects");
        for (final AssetIndex.AssetObject object : list) {
            final String filename = object.getFilename();
            final Downloadable d = new Downloadable(ClientInstanceRepo.ASSETS_REPO, filename, new File(objectsFolder, filename), false, true);
            result.add(d);
        }
        return result;
    }
    
    List<AssetIndex.AssetObject> getResourceFiles(final CompleteVersion version, final File baseDirectory, final boolean local) {
        List<AssetIndex.AssetObject> list = null;
        if (!local) {
            try {
                list = this.getRemoteResourceFilesList(version, baseDirectory, true);
            }
            catch (Exception e) {
                this.log("Cannot get remote assets list. Trying to use the local one.", e);
            }
        }
        if (list == null) {
            list = this.getLocalResourceFilesList(version, baseDirectory);
        }
        if (list == null) {
            try {
                list = this.getRemoteResourceFilesList(version, baseDirectory, true);
            }
            catch (Exception e) {
                this.log("Gave up trying to get assets list.", e);
            }
        }
        return list;
    }
    
    private List<AssetIndex.AssetObject> getLocalResourceFilesList(final CompleteVersion version, final File baseDirectory) {
        final List<AssetIndex.AssetObject> result = new ArrayList<AssetIndex.AssetObject>();
        final String indexName = version.getAssets();
        final File indexesFolder = new File(baseDirectory, "assets/indexes/");
        final File indexFile = new File(indexesFolder, indexName + ".json");
        if (Objects.nonNull(version.getAssetIndex())) {
            final long size = version.getAssetIndex().getSize();
            if (size != 0L && version.getID().equals("1.14") && indexFile.length() != size) {
                this.log("not new assets index file");
                return null;
            }
        }
        this.log("Reading indexes from file", indexFile);
        String json;
        try {
            json = FileUtil.readFile(indexFile);
        }
        catch (Exception e) {
            this.log("Cannot read local resource files list for index:", indexName, e);
            return null;
        }
        AssetIndex index = null;
        try {
            index = this.gson.fromJson(json, AssetIndex.class);
        }
        catch (JsonSyntaxException e2) {
            this.log("JSON file is invalid", e2);
        }
        if (index == null) {
            this.log("Cannot read data from JSON file.");
            return null;
        }
        result.addAll(index.getUniqueObjects());
        return result;
    }
    
    private List<AssetIndex.AssetObject> getRemoteResourceFilesList(final CompleteVersion version, final File baseDirectory, final boolean save) throws IOException {
        final List<AssetIndex.AssetObject> result = new ArrayList<AssetIndex.AssetObject>();
        String indexName = version.getAssets();
        if (indexName == null) {
            indexName = "legacy";
        }
        final File assets = new File(baseDirectory, "assets");
        final File indexesFolder = new File(assets, "indexes");
        final File indexFile = new File(indexesFolder, indexName + ".json");
        this.log("Reading from repository...");
        String json;
        if (version.getAssetIndex() != null) {
            json = ClientInstanceRepo.EMPTY_REPO.getUrl(version.getAssetIndex().getUrl());
        }
        else {
            json = ClientInstanceRepo.OFFICIAL_VERSION_REPO.getUrl("indexes/" + indexName + ".json");
        }
        if (save) {
            synchronized (this.assetsFlushLock) {
                FileUtil.writeFile(indexFile, json);
            }
        }
        final AssetIndex index = this.gson.fromJson(json, AssetIndex.class);
        result.addAll(index.getUniqueObjects());
        return result;
    }
    
    List<AssetIndex.AssetObject> checkResources(final CompleteVersion version, final File baseDirectory, final boolean fast) {
        this.log("Checking resources...");
        final List<AssetIndex.AssetObject> r = new ArrayList<AssetIndex.AssetObject>();
        final List<AssetIndex.AssetObject> list = this.getResourceFiles(version, baseDirectory, fast);
        if (list == null) {
            this.log("Cannot get assets list. Aborting.");
            return r;
        }
        this.log("Fast comparing:", fast);
        for (final AssetIndex.AssetObject resource : list) {
            if (!checkResource(baseDirectory, resource, fast)) {
                r.add(resource);
            }
        }
        return r;
    }
    
    public List<AssetIndex.AssetObject> checkResources(final CompleteVersion version, final boolean fast) {
        return this.checkResources(version, this.manager.getComponent(VersionLists.class).getLocal().getBaseDirectory(), fast);
    }
    
    private static boolean checkResource(final File baseDirectory, final AssetIndex.AssetObject local, final boolean fast) {
        final String path = local.getFilename();
        final File file = new File(baseDirectory, "assets/objects/" + path);
        final long size = file.length();
        return file.isFile() && size != 0L && (fast || (local.getSize() == size && (local.getHash() == null || local.getHash().equals(FileUtil.getChecksum(file, "SHA-1")))));
    }
}
