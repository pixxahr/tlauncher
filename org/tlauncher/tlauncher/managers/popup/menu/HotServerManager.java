// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers.popup.menu;

import java.io.IOException;
import ch.jamiete.mcping.MinecraftPingOptions;
import ch.jamiete.mcping.MinecraftPing;
import ch.jamiete.mcping.MinecraftPingReply;
import org.apache.commons.lang3.time.DateUtils;
import java.util.Date;
import com.google.gson.reflect.TypeToken;
import org.tlauncher.util.async.AsyncThread;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.tlauncher.tlauncher.entity.hot.AdditionalHotServer;
import org.tlauncher.util.gson.DownloadUtil;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.tlauncher.managers.VersionManager;
import java.util.function.Consumer;
import java.util.Objects;
import java.util.ArrayList;
import java.util.Iterator;
import org.tlauncher.tlauncher.minecraft.crash.Crash;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.tlauncher.entity.server.Server;
import java.awt.datatransfer.Clipboard;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import org.tlauncher.util.statistics.StatisticsUtil;
import org.tlauncher.tlauncher.entity.server.RemoteServer;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.ui.menu.PopupMenuView;
import org.tlauncher.util.U;
import net.minecraft.launcher.versions.ReleaseType;
import java.util.Collections;
import java.util.HashMap;
import org.tlauncher.tlauncher.rmo.TLauncher;
import com.google.inject.Inject;
import org.tlauncher.tlauncher.minecraft.launcher.server.InnerMinecraftServer;
import org.tlauncher.tlauncher.entity.ServerInfo;
import java.util.List;
import org.tlauncher.tlauncher.entity.hot.AdditionalHotServers;
import net.minecraft.launcher.updater.VersionFilter;
import org.tlauncher.tlauncher.ui.menu.PopupMenuModel;
import java.util.Map;
import com.google.inject.Singleton;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListener;
import org.tlauncher.tlauncher.managers.VersionManagerAdapter;

@Singleton
public class HotServerManager extends VersionManagerAdapter implements MinecraftListener
{
    private Map<String, PopupMenuModel> hashMap;
    private boolean serviceAvailable;
    private PopupMenuModel current;
    private VersionFilter filter;
    private AdditionalHotServers additionalHotServers;
    private List<ServerInfo> hotServers;
    @Inject
    private InnerMinecraftServer innerMinecraftServer;
    @Inject
    private TLauncher tLauncher;
    
    public HotServerManager() {
        this.hashMap = Collections.synchronizedMap(new HashMap<String, PopupMenuModel>());
        this.serviceAvailable = true;
        this.filter = new VersionFilter().exclude(ReleaseType.SNAPSHOT);
    }
    
    public void processingEvent(final String serverId) {
        if (this.serviceAvailable) {
            this.current = this.hashMap.get(serverId);
            if (this.current != null) {
                final PopupMenuView view = this.current.isMainPage() ? TLauncher.getInstance().getFrame().mp.defaultScene.getPopupMenuView() : TLauncher.getInstance().getFrame().mp.additionalHostServerScene.getPopupMenuView();
                view.showSelectedModel(this.current);
            }
            else {
                U.log("server id hasn't found = " + serverId);
            }
        }
    }
    
    public void launchGame(final VersionSyncInfo name) {
        this.changeVersion(name);
        this.block();
        TLauncher.getInstance().getFrame().mp.openDefaultScene();
        final RemoteServer s = new RemoteServer();
        if (this.current.getInfo() instanceof ServerInfo) {
            try {
                this.getMinecraftPingReplyAndResolvedAddress(this.current.getInfo());
            }
            catch (Throwable e) {
                U.log(e);
            }
        }
        s.setAddress(this.current.getResolvedAddress());
        s.setName(this.current.getName());
        this.sendStat();
        this.addServerToList(false, name);
        TLauncher.getInstance().getFrame().mp.defaultScene.loginForm.startLauncher(s);
    }
    
    private void sendStat() {
        String type;
        if (this.current.isMainPage()) {
            type = "main/page";
        }
        else {
            type = "additional";
        }
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("version", this.current.getServerId());
        StatisticsUtil.startSending("save/hot/server/" + type, null, map);
    }
    
    public void copyAddress() {
        final StringSelection selection = new StringSelection(this.current.getAddress());
        final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
        Alert.showMessage("", Localizable.get().get("menu.copy.done"));
    }
    
    private void changeVersion(final VersionSyncInfo version) {
        this.current.setSelected(version);
        this.tLauncher.getFrame().mp.defaultScene.loginForm.versions.setSelectedValue(version);
    }
    
    public void addServerToList(final boolean showMessage, final VersionSyncInfo v) {
        this.innerMinecraftServer.initInnerServers();
        final Server server = new Server();
        server.setAddress(this.current.getAddress());
        server.setName(server.getIp());
        try {
            if (v.isInstalled() && ((CompleteVersion)v.getLocal()).isModpack()) {
                this.innerMinecraftServer.addPageServerToModpack(server, v.getLocal());
            }
            else {
                this.innerMinecraftServer.addPageServer(server);
            }
        }
        catch (Throwable e) {
            U.log(e);
        }
        if (showMessage) {
            Alert.showMessage("", Localizable.get().get("menu.favorite.done"));
        }
    }
    
    @Override
    public void onMinecraftPrepare() {
        this.block();
    }
    
    @Override
    public void onMinecraftAbort() {
        this.enablePopup();
    }
    
    @Override
    public void onMinecraftLaunch() {
        this.block();
        try {
            if (!this.tLauncher.getLauncher().getVersion().isModpack()) {
                this.innerMinecraftServer.initInnerServers();
                this.innerMinecraftServer.searchRemovedServers();
                this.innerMinecraftServer.prepareInnerServer();
            }
        }
        catch (Throwable e) {
            U.log(e);
        }
    }
    
    @Override
    public void onMinecraftClose() {
        this.enablePopup();
    }
    
    @Override
    public void onMinecraftError(final Throwable e) {
        this.enablePopup();
    }
    
    @Override
    public void onMinecraftKnownError(final MinecraftException e) {
        this.enablePopup();
    }
    
    @Override
    public void onMinecraftCrash(final Crash crash) {
        this.enablePopup();
    }
    
    public void enablePopup() {
        this.serviceAvailable = true;
    }
    
    private void block() {
        this.serviceAvailable = false;
    }
    
    private void addServers(final List<? extends ServerInfo> list, final boolean mainPage, final List<VersionSyncInfo> versions) {
        for (final ServerInfo info : list) {
            if (this.hashMap.get(info.getServerId()) != null) {
                U.log("!!!the same id was found: " + info.getServerId());
            }
            else {
                final PopupMenuModel p = new PopupMenuModel(this.findAvailableVersions(info, versions), info, mainPage);
                this.hashMap.put(info.getServerId(), p);
            }
        }
    }
    
    private List<VersionSyncInfo> findAvailableVersions(final ServerInfo serverInfo, final List<VersionSyncInfo> versionList) {
        final List<VersionSyncInfo> list = new ArrayList<VersionSyncInfo>();
        for (final VersionSyncInfo v2 : versionList) {
            if (this.filter.satisfies(v2.getAvailableVersion())) {
                if (serverInfo.getIgnoreVersions().contains(v2.getID())) {
                    continue;
                }
                list.add(v2);
                if (Objects.equals(serverInfo.getMinVersion(), v2.getID())) {
                    break;
                }
                continue;
            }
        }
        versionList.stream().filter(v -> this.filter.satisfies(v.getAvailableVersion())).filter(v -> serverInfo.getIncludeVersions().contains(v.getID())).forEach(list::add);
        return list;
    }
    
    @Override
    public void onVersionsRefreshed(final VersionManager manager) {
        final List<VersionSyncInfo> list = manager.getVersions();
        this.hashMap.clear();
        AdditionalHotServers servers;
        final List<VersionSyncInfo> versions;
        AsyncThread.execute(() -> {
            try {
                if (Objects.isNull(this.additionalHotServers)) {
                    servers = DownloadUtil.loadByRepository(ClientInstanceRepo.ADD_HOT_SERVERS_REPO, AdditionalHotServers.class);
                    servers.setList((List<AdditionalHotServer>)servers.getList().stream().filter(AdditionalHotServer::isActive).collect((Collector<? super Object, ?, List<? super Object>>)Collectors.toList()));
                    if (servers.isShuffle()) {
                        Collections.shuffle(servers.getList());
                    }
                    this.additionalHotServers = servers;
                }
                this.addServers(this.additionalHotServers.getList(), false, versions);
            }
            catch (Throwable e) {
                U.log("can't load additional servers", e);
            }
            return;
        });
        final List<VersionSyncInfo> versions2;
        AsyncThread.execute(() -> {
            try {
                if (Objects.isNull(this.hotServers)) {
                    this.hotServers = DownloadUtil.loadByRepository(ClientInstanceRepo.HOT_SERVERS_REPO, new TypeToken<List<ServerInfo>>() {}.getType());
                }
                this.addServers(this.hotServers, true, versions2);
            }
            catch (Throwable e2) {
                U.log("can't load hot servers", e2);
            }
        });
    }
    
    public AdditionalHotServers getAdditionalHotServers() {
        return this.additionalHotServers;
    }
    
    public boolean isReady() {
        return Objects.nonNull(this.additionalHotServers);
    }
    
    public void fillServer(final AdditionalHotServer s) {
        try {
            final MinecraftPingReply data = this.getMinecraftPingReplyAndResolvedAddress(s);
            s.setOnline(data.getPlayers().getOnline());
            s.setMax(data.getPlayers().getMax());
            s.setUpdated(DateUtils.addMinutes(new Date(), 15));
            s.setImage(data.getFavicon());
        }
        catch (Throwable t) {
            U.log(t);
        }
    }
    
    private MinecraftPingReply getMinecraftPingReplyAndResolvedAddress(final ServerInfo s) throws IOException {
        final String[] serverConfig = s.getAddress().split(":");
        final MinecraftPing p = new MinecraftPing();
        final MinecraftPingOptions options = new MinecraftPingOptions().setHostname(serverConfig[0]).setPort(Integer.valueOf(serverConfig[1]));
        MinecraftPingReply data;
        try {
            data = p.getPing(options);
        }
        catch (Throwable t) {
            p.resolveDNS(options);
            s.setRedirectAddress(String.format("%s:%s", options.getHostname(), options.getPort()));
            data = p.getPing(options);
        }
        return data;
    }
}
