// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import org.tlauncher.util.async.AsyncObjectGotErrorException;
import org.tlauncher.tlauncher.repository.Repo;
import java.util.Set;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import org.tlauncher.util.FileUtil;
import java.io.File;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.tlauncher.downloader.Downloadable;
import java.util.Date;
import java.util.Collection;
import java.util.HashMap;
import org.tlauncher.tlauncher.rmo.TLauncher;
import net.minecraft.launcher.updater.VersionFilter;
import net.minecraft.launcher.updater.LatestVersionSyncInfo;
import net.minecraft.launcher.versions.CompleteVersion;
import net.minecraft.launcher.updater.VersionSyncInfo;
import java.io.IOException;
import org.tlauncher.util.async.AsyncObjectContainer;
import org.tlauncher.util.async.AsyncThread;
import java.util.Iterator;
import org.tlauncher.util.U;
import net.minecraft.launcher.updater.VersionList;
import org.tlauncher.util.async.AsyncObject;
import org.tlauncher.util.Time;
import java.util.Collections;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import net.minecraft.launcher.versions.Version;
import net.minecraft.launcher.versions.ReleaseType;
import java.util.Map;
import net.minecraft.launcher.updater.RemoteVersionList;
import net.minecraft.launcher.updater.LocalVersionList;
import org.tlauncher.tlauncher.component.ComponentDependence;
import org.tlauncher.tlauncher.component.InterruptibleComponent;

@ComponentDependence({ AssetsManager.class, VersionLists.class, TLauncherManager.class })
public class VersionManager extends InterruptibleComponent
{
    private final LocalVersionList localList;
    private final RemoteVersionList[] remoteLists;
    private Map<ReleaseType, Version> latestVersions;
    private final List<VersionManagerListener> listeners;
    private final Object versionFlushLock;
    private boolean hadRemote;
    private boolean localRefresh;
    
    public VersionManager(final ComponentManager manager) throws Exception {
        super(manager);
        final VersionLists list = manager.getComponent(VersionLists.class);
        this.localList = list.getLocal();
        this.remoteLists = list.getRemoteLists();
        this.latestVersions = new LinkedHashMap<ReleaseType, Version>();
        this.listeners = Collections.synchronizedList(new ArrayList<VersionManagerListener>());
        this.versionFlushLock = new Object();
    }
    
    public void addListener(final VersionManagerListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        this.listeners.add(listener);
    }
    
    public LocalVersionList getLocalList() {
        synchronized (this.versionFlushLock) {
            return this.localList;
        }
    }
    
    public Map<ReleaseType, Version> getLatestVersions() {
        synchronized (this.versionFlushLock) {
            return Collections.unmodifiableMap((Map<? extends ReleaseType, ? extends Version>)this.latestVersions);
        }
    }
    
    boolean refresh(final int refreshID, boolean local) {
        this.refreshList[refreshID] = true;
        this.localRefresh = local;
        local |= !this.manager.getLauncher().getConfiguration().getBoolean("minecraft.versions.sub.remote");
        this.hadRemote |= !local;
        if (local) {
            this.log("Refreshing versions locally...");
        }
        else {
            this.log("Refreshing versions remotely...");
            this.latestVersions.clear();
            synchronized (this.listeners) {
                for (final VersionManagerListener listener : this.listeners) {
                    listener.onVersionsRefreshing(this);
                }
            }
        }
        final Object lock = new Object();
        Time.start(lock);
        Map<AsyncObject<VersionList.RawVersionList>, VersionList.RawVersionList> result = null;
        Throwable e = null;
        try {
            result = this.refreshVersions(local);
        }
        catch (Throwable e2) {
            e = e2;
        }
        if (this.isCancelled(refreshID)) {
            this.log("Version refresh has been cancelled (" + Time.stop(lock) + " ms)");
            return false;
        }
        if (e != null) {
            synchronized (this.listeners) {
                for (final VersionManagerListener listener2 : this.listeners) {
                    listener2.onVersionsRefreshingFailed(this);
                }
            }
            this.log("Cannot refresh versions (" + Time.stop(lock) + " ms)", e);
            return true;
        }
        if (result != null) {
            synchronized (this.versionFlushLock) {
                for (final AsyncObject<VersionList.RawVersionList> object : result.keySet()) {
                    final VersionList.RawVersionList rawList = result.get(object);
                    if (rawList == null) {
                        continue;
                    }
                    final AsyncRawVersionListObject listObject = (AsyncRawVersionListObject)object;
                    final RemoteVersionList versionList = listObject.getVersionList();
                    versionList.refreshVersions(rawList);
                    this.latestVersions.putAll(versionList.getLatestVersions());
                }
            }
        }
        this.latestVersions = U.sortMap(this.latestVersions, ReleaseType.values());
        this.log("Versions has been refreshed (" + Time.stop(lock) + " ms)");
        this.refreshList[refreshID] = false;
        synchronized (this.listeners) {
            for (final VersionManagerListener listener2 : this.listeners) {
                listener2.onVersionsRefreshed(this);
            }
        }
        return true;
    }
    
    @Override
    protected boolean refresh(final int queueID) {
        return this.refresh(queueID, false);
    }
    
    public void startRefresh(final boolean local) {
        this.refresh(this.nextID(), local);
    }
    
    @Override
    public synchronized void stopRefresh() {
        super.stopRefresh();
        this.startRefresh(true);
    }
    
    public void asyncRefresh(final boolean local) {
        AsyncThread.execute(() -> this.startRefresh(local));
    }
    
    @Override
    public void asyncRefresh() {
        this.asyncRefresh(false);
    }
    
    private Map<AsyncObject<VersionList.RawVersionList>, VersionList.RawVersionList> refreshVersions(final boolean local) throws IOException {
        this.localList.refreshVersions();
        if (local) {
            return null;
        }
        final AsyncObjectContainer<VersionList.RawVersionList> container = new AsyncObjectContainer<VersionList.RawVersionList>();
        for (final RemoteVersionList remoteList : this.remoteLists) {
            container.add(new AsyncRawVersionListObject(remoteList));
        }
        return container.execute();
    }
    
    public void updateVersionList() {
        if (!this.hadRemote) {
            this.asyncRefresh();
        }
        else {
            for (final VersionManagerListener listener : this.listeners) {
                listener.onVersionsRefreshed(this);
            }
        }
    }
    
    public VersionSyncInfo getVersionSyncInfo(final Version version) {
        return this.getVersionSyncInfo(version.getID());
    }
    
    public VersionSyncInfo getVersionSyncInfo(String name) {
        if (name == null) {
            throw new NullPointerException("Cannot get sync info of NULL!");
        }
        if (name.startsWith("latest-")) {
            final String realID = name.substring(7);
            name = null;
            for (final Map.Entry<ReleaseType, Version> entry : this.latestVersions.entrySet()) {
                if (entry.getKey().toString().equals(realID)) {
                    name = entry.getValue().getID();
                    break;
                }
            }
            if (name == null) {
                return null;
            }
        }
        Version localVersion = this.localList.getVersion(name);
        if (localVersion instanceof CompleteVersion && ((CompleteVersion)localVersion).getInheritsFrom() != null) {
            try {
                localVersion = ((CompleteVersion)localVersion).resolve(this, false);
            }
            catch (IOException ioE) {
                throw new RuntimeException("Can't resolve version " + localVersion, ioE);
            }
        }
        Version remoteVersion = null;
        for (final RemoteVersionList list : this.remoteLists) {
            final Version currentVersion = list.getVersion(name);
            if (currentVersion != null) {
                remoteVersion = currentVersion;
                break;
            }
        }
        return (localVersion == null && remoteVersion == null) ? null : new VersionSyncInfo(localVersion, remoteVersion);
    }
    
    public LatestVersionSyncInfo getLatestVersionSyncInfo(final Version version) {
        if (version == null) {
            throw new NullPointerException("Cannot get latest sync info of NULL!");
        }
        final VersionSyncInfo syncInfo = this.getVersionSyncInfo(version);
        return new LatestVersionSyncInfo(version.getReleaseType(), syncInfo);
    }
    
    public List<VersionSyncInfo> getVersions(final VersionFilter filter, final boolean includeLatest) {
        return this.getVersions0(filter, includeLatest);
    }
    
    public List<VersionSyncInfo> getVersions(final boolean includeLatest) {
        return this.getVersions((TLauncher.getInstance() == null) ? null : TLauncher.getInstance().getConfiguration().getVersionFilter(), includeLatest);
    }
    
    public synchronized List<VersionSyncInfo> getVersions() {
        return this.getVersions(true);
    }
    
    private synchronized List<VersionSyncInfo> getVersions0(VersionFilter filter, final boolean includeLatest) {
        if (filter == null) {
            filter = new VersionFilter();
        }
        final List<VersionSyncInfo> plainResult = new ArrayList<VersionSyncInfo>();
        final List<VersionSyncInfo> result = new ArrayList<VersionSyncInfo>();
        final Map<String, VersionSyncInfo> lookup = new HashMap<String, VersionSyncInfo>();
        if (includeLatest) {
            for (final Version version : this.latestVersions.values()) {
                if (!filter.satisfies(version)) {
                    continue;
                }
                final LatestVersionSyncInfo syncInfo = this.getLatestVersionSyncInfo(version);
                if (result.contains(syncInfo)) {
                    continue;
                }
                result.add(syncInfo);
            }
        }
        final List<Version> listVersion = this.localList.getVersions();
        for (int i = 0; i < listVersion.size(); ++i) {
            if (filter.satisfies(listVersion.get(i))) {
                final VersionSyncInfo syncInfo2 = this.getVersionSyncInfo(listVersion.get(i));
                if (syncInfo2 != null) {
                    lookup.put(listVersion.get(i).getID(), syncInfo2);
                    plainResult.add(syncInfo2);
                }
            }
        }
        for (final RemoteVersionList remoteList : this.remoteLists) {
            for (final Version version2 : remoteList.getVersions()) {
                if (!lookup.containsKey(version2.getID())) {
                    if (!filter.satisfies(version2)) {
                        continue;
                    }
                    final VersionSyncInfo syncInfo3 = this.getVersionSyncInfo(version2);
                    lookup.put(version2.getID(), syncInfo3);
                    plainResult.add(syncInfo3);
                }
            }
        }
        final Date aDate;
        final Date bDate;
        Collections.sort(plainResult, (a, b) -> {
            aDate = a.getLatestVersion().getReleaseTime();
            bDate = b.getLatestVersion().getReleaseTime();
            if (aDate == null || bDate == null) {
                return 1;
            }
            else {
                return bDate.compareTo(aDate);
            }
        });
        result.addAll(plainResult);
        return result;
    }
    
    public List<VersionSyncInfo> getInstalledVersions(final VersionFilter filter) {
        final List<VersionSyncInfo> result = new ArrayList<VersionSyncInfo>();
        for (final Version version : this.localList.getVersions()) {
            result.add(this.getVersionSyncInfo(version));
        }
        return result;
    }
    
    public List<VersionSyncInfo> getInstalledVersions() {
        return this.getInstalledVersions((TLauncher.getInstance() == null) ? null : TLauncher.getInstance().getConfiguration().getVersionFilter());
    }
    
    public VersionSyncInfoContainer downloadVersion(final VersionSyncInfo syncInfo, final boolean tlauncher, final boolean force) throws IOException {
        final VersionSyncInfoContainer container = new VersionSyncInfoContainer(syncInfo);
        final CompleteVersion completeVersion = syncInfo.getCompleteVersion(force);
        final File baseDirectory = this.localList.getBaseDirectory();
        final Set<Downloadable> required = syncInfo.getRequiredDownloadables(baseDirectory, force, tlauncher);
        container.addAll(required);
        this.log("Required for version " + syncInfo.getID() + ':', required);
        final String originalId = completeVersion.getJar();
        if (!syncInfo.hasRemote() && originalId == null) {
            return container;
        }
        final String id = completeVersion.getID();
        String jarFile = "versions/";
        String saveFile = "versions/";
        Repo repo;
        if (originalId == null) {
            repo = syncInfo.getRemote().getSource();
            jarFile = (saveFile = jarFile + id + "/" + id + ".jar");
        }
        else {
            repo = ClientInstanceRepo.OFFICIAL_VERSION_REPO;
            jarFile = jarFile + originalId + "/" + originalId + ".jar";
            saveFile = saveFile + id + "/" + id + ".jar";
        }
        final File file = new File(baseDirectory, saveFile);
        if (!badFile(file)) {
            return container;
        }
        if (!force && originalId != null) {
            final File originalFile = new File(baseDirectory, jarFile);
            final File originalFileBak = new File(baseDirectory, jarFile + ".bak");
            if (originalFile.isFile() && originalFileBak.isFile() && originalFile.length() == originalFileBak.length()) {
                FileUtil.copyFile(originalFile, file, true);
                return container;
            }
        }
        if (completeVersion.getDownloads() != null && completeVersion.getDownloads().get("client") != null) {
            repo = ClientInstanceRepo.EMPTY_REPO;
            jarFile = completeVersion.getDownloads().get("client").getUrl();
        }
        final Downloadable d = new Downloadable(repo, jarFile, new File(baseDirectory, saveFile), force);
        d.addAdditionalDestination(new File(d.getDestination() + ".bak"));
        this.log("Jar for " + syncInfo.getID() + ':', d);
        container.add(d);
        return container;
    }
    
    public boolean isLocalRefresh() {
        return this.localRefresh;
    }
    
    private static boolean badFile(final File file) {
        return !file.isFile() || file.length() == 0L;
    }
    
    class AsyncRawVersionListObject extends AsyncObject<VersionList.RawVersionList>
    {
        private final RemoteVersionList remoteList;
        
        AsyncRawVersionListObject(final RemoteVersionList remoteList) {
            this.remoteList = remoteList;
        }
        
        RemoteVersionList getVersionList() {
            return this.remoteList;
        }
        
        @Override
        protected VersionList.RawVersionList execute() throws AsyncObjectGotErrorException {
            try {
                return this.remoteList.getRawList();
            }
            catch (Exception e) {
                LauncherComponent.this.log("Error refreshing version list:", e);
                throw new AsyncObjectGotErrorException(this, e);
            }
        }
    }
}
