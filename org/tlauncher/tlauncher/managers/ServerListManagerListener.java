// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

public interface ServerListManagerListener
{
    void onServersRefreshing(final ServerListManager p0);
    
    void onServersRefreshingFailed(final ServerListManager p0);
    
    void onServersRefreshed(final ServerListManager p0);
}
