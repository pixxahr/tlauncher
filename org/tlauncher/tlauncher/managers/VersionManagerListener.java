// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

public interface VersionManagerListener
{
    void onVersionsRefreshing(final VersionManager p0);
    
    void onVersionsRefreshingFailed(final VersionManager p0);
    
    void onVersionsRefreshed(final VersionManager p0);
}
