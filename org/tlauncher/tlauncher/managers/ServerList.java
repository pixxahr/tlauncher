// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import java.util.ArrayList;
import org.tlauncher.tlauncher.entity.server.RemoteServer;
import java.util.List;

public class ServerList
{
    private List<RemoteServer> newServers;
    private List<String> removedServers;
    
    ServerList() {
        this.newServers = new ArrayList<RemoteServer>();
        this.removedServers = new ArrayList<String>();
    }
    
    public List<RemoteServer> getNewServers() {
        return this.newServers;
    }
    
    public List<String> getRemovedServers() {
        return this.removedServers;
    }
    
    public void setNewServers(final List<RemoteServer> newServers) {
        this.newServers = newServers;
    }
    
    public void setRemovedServers(final List<String> removedServers) {
        this.removedServers = removedServers;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ServerList)) {
            return false;
        }
        final ServerList other = (ServerList)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$newServers = this.getNewServers();
        final Object other$newServers = other.getNewServers();
        Label_0065: {
            if (this$newServers == null) {
                if (other$newServers == null) {
                    break Label_0065;
                }
            }
            else if (this$newServers.equals(other$newServers)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$removedServers = this.getRemovedServers();
        final Object other$removedServers = other.getRemovedServers();
        if (this$removedServers == null) {
            if (other$removedServers == null) {
                return true;
            }
        }
        else if (this$removedServers.equals(other$removedServers)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof ServerList;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $newServers = this.getNewServers();
        result = result * 59 + (($newServers == null) ? 43 : $newServers.hashCode());
        final Object $removedServers = this.getRemovedServers();
        result = result * 59 + (($removedServers == null) ? 43 : $removedServers.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "ServerList(newServers=" + this.getNewServers() + ", removedServers=" + this.getRemovedServers() + ")";
    }
}
