// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.tlauncher.repository.Repo;
import com.google.gson.Gson;
import org.tlauncher.tlauncher.component.RefreshableComponent;

public class ServerListManager extends RefreshableComponent
{
    private final Gson gson;
    private final Repo repo;
    private ServerList serverList;
    
    public ServerListManager(final ComponentManager manager) throws Exception {
        super(manager);
        this.serverList = new ServerList();
        this.repo = ClientInstanceRepo.SERVER_LIST_REPO;
        this.gson = TLauncher.getGson();
    }
    
    @Override
    protected boolean refresh() {
        try {
            this.serverList = this.gson.fromJson(this.repo.getUrl(), ServerList.class);
            return true;
        }
        catch (Throwable t) {
            return false;
        }
    }
    
    public ServerList getList() {
        return this.serverList;
    }
}
