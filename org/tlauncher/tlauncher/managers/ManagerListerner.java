// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

public interface ManagerListerner<T>
{
    void startedDownloading();
    
    void downloadedData(final T p0);
    
    void preparedGame();
}
