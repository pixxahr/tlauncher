// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import java.util.Map;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.util.advertising.AdvertisingStatusObserver;
import org.tlauncher.tlauncher.ui.browser.BrowserHolder;
import org.tlauncher.util.advertising.AdvertisingStatusObservable;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.util.FileUtil;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import org.tlauncher.util.MinecraftUtil;
import java.io.Reader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.Closeable;
import org.tlauncher.util.U;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.tlauncher.tlauncher.minecraft.auth.UUIDTypeAdapter;
import net.minecraft.launcher.versions.json.FileTypeAdapter;
import java.lang.reflect.Type;
import net.minecraft.launcher.versions.json.DateTypeAdapter;
import java.util.Date;
import com.google.gson.TypeAdapterFactory;
import net.minecraft.launcher.versions.json.LowerCaseEnumTypeAdapterFactory;
import com.google.gson.GsonBuilder;
import java.util.Collections;
import java.util.ArrayList;
import org.tlauncher.tlauncher.minecraft.auth.AuthenticatorDatabase;
import java.util.UUID;
import java.io.File;
import com.google.gson.Gson;
import org.tlauncher.tlauncher.minecraft.auth.AccountListener;
import java.util.List;
import org.tlauncher.tlauncher.component.RefreshableComponent;

public class ProfileManager extends RefreshableComponent
{
    private static final String DEFAULT_PROFILE_FILENAME = "TlauncherProfiles.json";
    private static final String OLD_PROFILE_FILENAME = "tlauncher_profiles.json";
    private static final String LAUNCHER_PROFILE_FILENAME = "launcher_profiles.json";
    private final List<ProfileManagerListener> listeners;
    private final AccountListener accountListener;
    private final Gson gson;
    private File file;
    private UUID clientToken;
    private AuthenticatorDatabase authDatabase;
    
    ProfileManager(final ComponentManager manager, final File file) throws Exception {
        super(manager);
        if (file == null) {
            throw new NullPointerException();
        }
        this.file = file;
        this.listeners = Collections.synchronizedList(new ArrayList<ProfileManagerListener>());
        this.clientToken = UUID.randomUUID();
        final Iterator<ProfileManagerListener> iterator;
        AccountListener listener;
        this.accountListener = (db -> {
            this.listeners.iterator();
            while (iterator.hasNext()) {
                listener = iterator.next();
                listener.onAccountsRefreshed(db);
            }
            return;
        });
        (this.authDatabase = new AuthenticatorDatabase()).setListener(this.accountListener);
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        builder.registerTypeAdapter(File.class, new FileTypeAdapter());
        builder.registerTypeAdapter(AuthenticatorDatabase.class, new AuthenticatorDatabase.Serializer());
        builder.registerTypeAdapter(UUIDTypeAdapter.class, new UUIDTypeAdapter());
        builder.setPrettyPrinting();
        this.gson = builder.create();
    }
    
    public ProfileManager(final ComponentManager manager) throws Exception {
        this(manager, getDefaultFile());
    }
    
    public void recreate() {
        this.setFile(getDefaultFile());
        this.refresh();
    }
    
    public boolean refresh() {
        this.loadProfiles();
        for (final ProfileManagerListener listener : this.listeners) {
            listener.onProfilesRefreshed(this);
        }
        try {
            this.saveProfiles();
        }
        catch (IOException e) {
            return false;
        }
        return true;
    }
    
    private void loadProfiles() {
        this.log("Refreshing profiles from:", this.file);
        final File oldFile = new File(this.file.getParentFile(), "tlauncher_profiles.json");
        OutputStreamWriter writer = null;
        if (!oldFile.isFile()) {
            try {
                writer = new OutputStreamWriter(new FileOutputStream(oldFile), Charset.forName("UTF-8"));
                this.gson.toJson(new OldProfileList(), writer);
                writer.close();
            }
            catch (Exception e) {
                this.log("Cannot write into", "tlauncher_profiles.json", e);
            }
            finally {
                U.close(writer);
            }
        }
        RawProfileList raw = null;
        InputStreamReader reader = null;
        try {
            reader = new InputStreamReader(new FileInputStream(this.file.isFile() ? this.file : oldFile), Charset.forName("UTF-8"));
            raw = this.gson.fromJson(reader, RawProfileList.class);
        }
        catch (Exception e2) {
            this.log("Cannot read from", "TlauncherProfiles.json", e2);
        }
        finally {
            U.close(reader);
        }
        if (raw == null) {
            raw = new RawProfileList();
        }
        final File launcherProfile = new File(MinecraftUtil.getWorkingDirectory(), "launcher_profiles.json");
        if (Files.notExists(launcherProfile.toPath(), new LinkOption[0])) {
            try {
                FileUtil.writeFile(launcherProfile, "{\n\"clientToken\": \"" + raw.clientToken + "\"\n,\"profiles\": {}}");
            }
            catch (IOException e3) {
                this.log(e3);
            }
        }
        this.clientToken = raw.clientToken;
        raw.authenticationDatabase.cleanFreeAccount();
        (this.authDatabase = raw.authenticationDatabase).setListener(this.accountListener);
    }
    
    public void saveProfiles() throws IOException {
        final RawProfileList raw = new RawProfileList();
        raw.clientToken = this.clientToken;
        raw.authenticationDatabase = this.authDatabase;
        FileUtil.writeFile(this.file, this.gson.toJson(raw));
    }
    
    public AuthenticatorDatabase getAuthDatabase() {
        return this.authDatabase;
    }
    
    public File getFile() {
        return this.file;
    }
    
    public void setFile(final File file) {
        if (file == null) {
            throw new NullPointerException();
        }
        this.file = file;
        for (final ProfileManagerListener listener : this.listeners) {
            listener.onProfileManagerChanged(this);
        }
    }
    
    public UUID getClientToken() {
        return this.clientToken;
    }
    
    public void setClientToken(final String uuid) {
        this.clientToken = UUID.fromString(uuid);
    }
    
    public void addListener(final ProfileManagerListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        if (!this.listeners.contains(listener)) {
            this.listeners.add(listener);
        }
    }
    
    private static File getDefaultFile() {
        return new File(MinecraftUtil.getWorkingDirectory(), "TlauncherProfiles.json");
    }
    
    public static void init() {
        final ProfileManager profileManager = TLauncher.getInstance().getProfileManager();
        final TLauncherManager TLauncherManager = TLauncher.getInstance().getTLauncherManager();
        profileManager.addListener(new ProfileManagerListener() {
            @Override
            public void onAccountsRefreshed(final AuthenticatorDatabase db) {
                TLauncherManager.asyncRefresh();
            }
            
            @Override
            public void onProfilesRefreshed(final ProfileManager pm) {
                this.onAccountsRefreshed(pm.getAuthDatabase());
            }
            
            @Override
            public void onProfileManagerChanged(final ProfileManager pm) {
                this.onAccountsRefreshed(pm.getAuthDatabase());
            }
        });
        final ProfileManagerListener listener = new ProfileManagerListener() {
            @Override
            public void onAccountsRefreshed(final AuthenticatorDatabase db) {
            }
            
            @Override
            public void onProfilesRefreshed(final ProfileManager pm) {
                final AdvertisingStatusObservable adStatus = new AdvertisingStatusObservable();
                adStatus.setManager(pm);
                if (BrowserHolder.getInstance().getBrowser() != null) {
                    adStatus.addListeners(BrowserHolder.getInstance().getBrowser());
                }
                adStatus.run();
            }
            
            @Override
            public void onProfileManagerChanged(final ProfileManager pm) {
            }
        };
        profileManager.addListener(listener);
        profileManager.refresh();
    }
    
    public boolean hasPremium() {
        return this.authDatabase.getAccounts().stream().anyMatch(Account::isPremiumAccount);
    }
    
    static class RawProfileList
    {
        UUID clientToken;
        AuthenticatorDatabase authenticationDatabase;
        
        RawProfileList() {
            this.clientToken = UUID.randomUUID();
            this.authenticationDatabase = new AuthenticatorDatabase();
        }
    }
    
    static class OldProfileList
    {
        UUID clientToken;
        Map<?, ?> profiles;
        
        OldProfileList() {
            this.clientToken = UUID.randomUUID();
        }
    }
}
