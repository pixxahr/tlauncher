// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import java.io.IOException;
import org.tlauncher.util.U;
import com.google.gson.reflect.TypeToken;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import java.lang.annotation.Annotation;
import com.google.inject.Key;
import com.google.inject.name.Names;
import org.tlauncher.tlauncher.rmo.TLauncher;
import com.google.gson.Gson;
import java.util.ArrayList;
import org.tlauncher.tlauncher.entity.AdditionalAsset;
import java.util.List;
import org.tlauncher.tlauncher.component.InterruptibleComponent;

public class AdditionalAssetsComponent extends InterruptibleComponent
{
    private List<AdditionalAsset> additionalAssets;
    
    public AdditionalAssetsComponent(final ComponentManager manager) throws Exception {
        super(manager);
        this.additionalAssets = new ArrayList<AdditionalAsset>();
    }
    
    @Override
    protected boolean refresh(final int refreshID) {
        try {
            final Gson gson = (Gson)TLauncher.getInjector().getInstance(Key.get((Class)Gson.class, (Annotation)Names.named("GsonAdditionalFile")));
            this.additionalAssets = gson.fromJson(ClientInstanceRepo.EXTRA_VERSION_REPO.getUrl("additional_assets-1.0.json"), new TypeToken<List<AdditionalAsset>>() {}.getType());
            return true;
        }
        catch (IOException e) {
            U.log(e);
            return false;
        }
    }
    
    public List<AdditionalAsset> getAdditionalAssets() {
        return this.additionalAssets;
    }
}
