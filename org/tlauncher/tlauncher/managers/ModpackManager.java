// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import org.tlauncher.util.OS;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import javax.swing.SwingUtilities;
import org.tlauncher.modpack.domain.client.version.ModVersionDTO;
import org.tlauncher.tlauncher.ui.modpack.HandleInstallModpackElementFrame;
import org.tlauncher.modpack.domain.client.share.Category;
import org.tlauncher.tlauncher.exceptions.SameMapFoldersException;
import java.nio.file.NoSuchFileException;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import java.util.Optional;
import org.tlauncher.modpack.domain.client.share.DependencyType;
import org.tlauncher.modpack.domain.client.GameEntityDependencyDTO;
import org.tlauncher.modpack.domain.client.SubModpackDTO;
import com.google.gson.JsonSyntaxException;
import java.util.HashSet;
import java.net.URL;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.net.SocketTimeoutException;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.exceptions.GameEntityNotFound;
import org.tlauncher.exceptions.ParseModPackException;
import java.util.zip.ZipFile;
import com.github.junrar.Archive;
import java.io.FilenameFilter;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.tlauncher.entity.MinecraftInstance;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.tlauncher.util.async.AsyncThread;
import java.util.Collection;
import org.tlauncher.tlauncher.ui.modpack.ModpackBackupFrame;
import org.tlauncher.modpack.domain.client.version.MapMetadataDTO;
import org.tlauncher.modpack.domain.client.MapDTO;
import java.nio.file.Paths;
import org.tlauncher.modpack.domain.client.ResourcePackDTO;
import org.tlauncher.modpack.domain.client.share.StateGameElement;
import java.nio.file.Path;
import org.tlauncher.tlauncher.downloader.DownloadableContainerHandler;
import org.tlauncher.tlauncher.downloader.mods.GameEntityHandler;
import org.tlauncher.tlauncher.downloader.mods.UnzipEntityDownloader;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import net.minecraft.launcher.versions.Version;
import org.tlauncher.tlauncher.downloader.mods.MapDownloader;
import org.tlauncher.tlauncher.downloader.mods.ArchiveGameEntityDownloader;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import java.util.Iterator;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import java.util.Objects;
import net.minecraft.launcher.updater.VersionSyncInfo;
import java.io.IOException;
import com.google.gson.reflect.TypeToken;
import org.tlauncher.modpack.domain.client.ModDTO;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;
import net.minecraft.launcher.Http;
import org.tlauncher.util.MinecraftUtil;
import org.tlauncher.util.U;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityAdapter;
import org.tlauncher.util.FileUtil;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Set;
import org.tlauncher.modpack.domain.client.share.InfoMod;
import org.tlauncher.tlauncher.configuration.InnerConfiguration;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.inject.Named;
import javax.inject.Inject;
import com.google.gson.Gson;
import java.io.File;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityListener;
import java.util.List;
import org.tlauncher.modpack.domain.client.share.GameType;
import java.util.Map;
import com.google.inject.Singleton;

@Singleton
public class ModpackManager implements VersionManagerListener
{
    private final Map<GameType, List<GameEntityListener>> gameListeners;
    private final File STATUS_MODPACK_FILE;
    @Inject
    @Named("GsonCompleteVersion")
    private Gson gson;
    @com.google.inject.Inject
    private TLauncher tLauncher;
    private InnerConfiguration innerConfiguration;
    private InfoMod infoMod;
    private Set<Long> statusModpackElement;
    private AtomicBoolean addedVersionListener;
    
    public ModpackManager() {
        this.gameListeners = Collections.synchronizedMap((Map<GameType, List<GameEntityListener>>)new HashMap<GameType, List<GameEntityListener>>() {
            {
                this.put(GameType.MAP, Collections.synchronizedList(new ArrayList<GameEntityListener>()));
                this.put(GameType.MOD, Collections.synchronizedList(new ArrayList<GameEntityListener>()));
                this.put(GameType.MODPACK, Collections.synchronizedList(new ArrayList<GameEntityListener>()));
                this.put(GameType.RESOURCEPACK, Collections.synchronizedList(new ArrayList<GameEntityListener>()));
            }
        });
        this.STATUS_MODPACK_FILE = FileUtil.getRelativeConfigFile("status.modpack");
        this.innerConfiguration = TLauncher.getInnerSettings();
        this.addedVersionListener = new AtomicBoolean(false);
        final GameEntityAdapter listener = new GameEntityAdapter() {
            @Override
            public void installEntity(final CompleteVersion e) {
                this.update();
            }
            
            private void update() {
                TLauncher.getInstance().getVersionManager().getLocalList().refreshVersions();
            }
            
            @Override
            public void removeCompleteVersion(final CompleteVersion e) {
                this.update();
            }
        };
        this.gameListeners.get(GameType.MODPACK).add(listener);
    }
    
    public Set<Long> getStatusModpackElement() {
        return this.statusModpackElement;
    }
    
    private void log(final Object... s) {
        U.log("[Modpack] ", s);
    }
    
    public synchronized void loadInfo() {
        U.gc();
        final List<CompleteVersion> versions = this.getModpackVersions();
        if (this.getInfoMod() == null || this.getInfoMod().getGameVersions().isEmpty()) {
            final String lang = this.tLauncher.getConfiguration().getLocale().toString().toUpperCase();
            InfoMod infoMod;
            try {
                final File cached = MinecraftUtil.getTLauncherFile("cache/modpacks_" + this.tLauncher.getConfiguration().getLocale().getLanguage());
                boolean usedCache = false;
                if (cached.exists()) {
                    final String url = String.format("%sseparated/%s?lang=%s&version=%s", this.innerConfiguration.get("modpack.url"), "hash", lang, this.innerConfiguration.get("version"));
                    usedCache = Http.performGet(url).equalsIgnoreCase(FileUtil.getChecksum(cached));
                }
                if (!usedCache) {
                    final String url = String.format("%sseparated/%s?lang=%s&version=%s", this.innerConfiguration.get("modpack.url"), "infomod", lang, this.innerConfiguration.get("version"));
                    final DownloadableContainer container = new DownloadableContainer();
                    container.add(new Downloadable(ClientInstanceRepo.EMPTY_REPO, url, cached, true, true));
                    this.tLauncher.getDownloader().add(container);
                    this.tLauncher.getDownloader().startDownloadAndWait();
                }
                infoMod = this.gson.fromJson(FileUtil.readZippedFile(cached, "entities.json"), InfoMod.class);
                infoMod.setMods(this.gson.fromJson(FileUtil.readZippedFile(cached, "mods.json"), new TypeToken<List<ModDTO>>() {}.getType()));
            }
            catch (IOException e) {
                U.log(e);
                infoMod = new InfoMod();
            }
            U.debug("loaded data and created data from modpack server");
            this.setInfoMod(infoMod);
            this.processGameElementByStatus();
            this.installedDirectlySkinCapeMod();
        }
        this.tLauncher.getFrame().mp.modpackScene.prepareView(this.getInfoMod(), versions);
        if (!this.addedVersionListener.get()) {
            this.addedVersionListener.set(true);
            this.tLauncher.getVersionManager().addListener(this);
        }
    }
    
    private void installedDirectlySkinCapeMod() {
        try {
            boolean f = false;
            final List<VersionSyncInfo> list = this.tLauncher.getVersionManager().getInstalledVersions();
            for (final VersionSyncInfo v : list) {
                CompleteVersion completeVersion = v.getLocalCompleteVersion();
                if (Objects.isNull(completeVersion) && v.getLocal() instanceof CompleteVersion) {
                    completeVersion = (CompleteVersion)v.getLocal();
                }
                if (Objects.nonNull(completeVersion) && completeVersion.isModpack() && completeVersion.isSkinVersion()) {
                    final ModpackVersionDTO versionDTO = (ModpackVersionDTO)completeVersion.getModpack().getVersion();
                    if (!versionDTO.getMods().stream().noneMatch(e -> ModDTO.SKIN_MODS.contains(e.getId()))) {
                        continue;
                    }
                    final GameEntityDTO gameEntityDTO = this.findById(ModDTO.TL_SKIN_CAPE_ID, GameType.MOD);
                    final BaseModpackFilter filter = BaseModpackFilter.getBaseModpackStandardFilters(gameEntityDTO, GameType.MOD, completeVersion.getModpack());
                    final List<VersionDTO> l = (List<VersionDTO>)ModpackUtil.sortByDate(filter.findAll(gameEntityDTO.getVersions()));
                    if (!l.isEmpty()) {
                        final GameEntityDTO installEntity = this.readFromServer(ModDTO.class, gameEntityDTO, l.get(0));
                        this.addEntityToModpack(installEntity, completeVersion, GameType.MOD);
                        this.resaveVersion(completeVersion);
                        f = true;
                        U.log("added mod cape and skin for " + completeVersion.getModpack().getName());
                    }
                    else {
                        U.log("can't find proper cape and skin for " + completeVersion.getModpack().getName());
                    }
                }
            }
            if (f) {
                this.tLauncher.getVersionManager().asyncRefresh();
            }
        }
        catch (Throwable t) {
            U.log("can't install mods", t);
        }
    }
    
    public DownloadableContainer getContainer(final CompleteVersion version, final boolean force) {
        this.log("got container");
        final DownloadableContainer container = new DownloadableContainer();
        final Path versionFolder = ModpackUtil.getPathByVersion(version);
        if (version.getModpack() != null && version.getModpack().getVersion() != null) {
            final ModpackVersionDTO versionDTO = (ModpackVersionDTO)version.getModpack().getVersion();
            List<MetadataDTO> list = this.checkResources(versionDTO, force, versionFolder);
            for (final MetadataDTO m : list) {
                final Downloadable d = new ArchiveGameEntityDownloader(ClientInstanceRepo.createModpackRepo(), force, m, versionFolder);
                container.add(d);
            }
            list = this.checkCompositeResources(versionDTO, force, versionFolder);
            for (final MetadataDTO m : list) {
                final Downloadable d = new MapDownloader(force, m, versionFolder);
                container.add(d);
            }
            if (versionDTO.getAdditionalFile() != null) {
                final MetadataDTO metadataDTO = versionDTO.getAdditionalFile();
                final Path additionalFile = ModpackUtil.getPathByVersion(version, metadataDTO.getPath());
                if (Files.notExists(additionalFile, new LinkOption[0]) || !FileUtil.getChecksum(additionalFile.toFile()).equals(metadataDTO.getSha1())) {
                    container.add(new UnzipEntityDownloader(force, versionDTO.getAdditionalFile(), versionFolder));
                }
            }
            container.addHandler(new GameEntityHandler());
        }
        return container;
    }
    
    private List<MetadataDTO> checkResources(final ModpackVersionDTO version, final boolean force, final Path versionFolder) {
        this.log("check resources");
        final List<MetadataDTO> list = new ArrayList<MetadataDTO>();
        for (final ModDTO mod : version.getMods()) {
            if (!mod.isUserInstall() && mod.getStateGameElement() != StateGameElement.NO_ACTIVE && this.notExistOrCorrect(versionFolder, mod, force) && !this.fillFromCache(GameType.MOD, mod, version, versionFolder)) {
                list.add(mod.getVersion().getMetadata());
            }
        }
        for (final ResourcePackDTO resourcePack : version.getResourcePacks()) {
            if (!resourcePack.isUserInstall() && resourcePack.getStateGameElement() != StateGameElement.NO_ACTIVE && this.notExistOrCorrect(versionFolder, resourcePack, force) && !this.fillFromCache(GameType.RESOURCEPACK, resourcePack, version, versionFolder)) {
                list.add(resourcePack.getVersion().getMetadata());
            }
        }
        return list;
    }
    
    private boolean notExistOrCorrect(final Path versionFolder, final GameEntityDTO e, final boolean hash) {
        final Path path = Paths.get(versionFolder.toString(), e.getVersion().getMetadata().getPath());
        return Files.notExists(path, new LinkOption[0]) || (hash && !FileUtil.getChecksum(path.toFile()).equals(e.getVersion().getMetadata().getSha1()));
    }
    
    private List<MetadataDTO> checkCompositeResources(final ModpackVersionDTO version, final boolean force, final Path versionFolder) {
        this.log("check CompositeResources");
        final List<MetadataDTO> list = new ArrayList<MetadataDTO>();
        for (final MapDTO map : version.getMaps()) {
            if (!map.isUserInstall()) {
                list.add(map.getVersion().getMetadata());
            }
        }
        final Iterator<MetadataDTO> it = list.iterator();
        while (it.hasNext()) {
            final MetadataDTO meta = it.next();
            U.debug(meta);
            if (((MapMetadataDTO)meta).getFolders() != null) {
                if (((MapMetadataDTO)meta).getFolders().size() == 0) {
                    continue;
                }
                final String folder = versionFolder.toString() + "/saves/" + ((MapMetadataDTO)meta).getFolders().get(0);
                if (!new File(folder).exists()) {
                    continue;
                }
                it.remove();
            }
        }
        return list;
    }
    
    public void backupModPack(final List<CompleteVersion> list, final File saveFolder, final ModpackBackupFrame.HandleListener handleListener) {
        final File versionFolder;
        final List<File> files;
        final Iterator<CompleteVersion> iterator;
        CompleteVersion v;
        File version;
        final Map<String, String> map;
        final List<String> names;
        final Iterator<CompleteVersion> iterator2;
        CompleteVersion v2;
        AsyncThread.execute(() -> {
            this.log("backuping modpack");
            versionFolder = FileUtil.getRelative("versions").toFile();
            files = new ArrayList<File>();
            list.iterator();
            while (iterator.hasNext()) {
                v = iterator.next();
                version = new File(versionFolder, v.getID());
                files.addAll(this.findCopiedFiles(v, version));
            }
            map = new HashMap<String, String>();
            names = new ArrayList<String>();
            list.iterator();
            while (iterator2.hasNext()) {
                v2 = iterator2.next();
                map.put(v2.getID() + ".json", this.gson.toJson(v2));
                names.add(v2.getID());
            }
            try {
                if (saveFolder.exists() && !saveFolder.delete()) {
                    throw new IOException("can't delete old version of the file");
                }
                else {
                    FileUtil.backupModpacks(map, files, versionFolder.toPath(), saveFolder, names);
                    handleListener.operationSuccess();
                }
            }
            catch (IOException e) {
                this.log(e);
                handleListener.processError(e);
            }
        });
    }
    
    private List<File> findCopiedFiles(final CompleteVersion completeVersion, final File version) {
        final IOFileFilter filter = FileFilterUtils.notFileFilter(FileFilterUtils.or(FileFilterUtils.nameFileFilter("natives"), FileFilterUtils.nameFileFilter(FileUtil.GameEntityFolder.SAVES.toString()), FileFilterUtils.nameFileFilter(FileUtil.GameEntityFolder.MODS.toString()), FileFilterUtils.nameFileFilter(FileUtil.GameEntityFolder.RESOURCEPACKS.toString())));
        final IOFileFilter filesFilter = FileFilterUtils.notFileFilter(FileFilterUtils.or(FileFilterUtils.nameFileFilter(completeVersion.getID() + ".jar"), FileFilterUtils.nameFileFilter(completeVersion.getID() + ".jar.bak")));
        final List<File> list = (List<File>)(List)FileUtils.listFiles(version, filesFilter, filter);
        if (TLauncher.DEBUG) {
            U.log("filter by IOFileFilter");
            for (final File f : list) {
                U.log(f);
            }
        }
        final ModpackVersionDTO modpackVersion = (ModpackVersionDTO)completeVersion.getModpack().getVersion();
        for (final GameEntityDTO en : modpackVersion.getMaps()) {
            final File map = new File(version, "saves/" + FilenameUtils.getBaseName(en.getVersion().getMetadata().getPath()));
            if (map.exists()) {
                list.addAll(FileUtils.listFiles(map, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE));
            }
        }
        for (final GameEntityDTO en : modpackVersion.getMods()) {
            if (en.isUserInstall()) {
                this.addToList(version, list, en);
            }
        }
        for (final GameEntityDTO en : modpackVersion.getResourcePacks()) {
            if (en.isUserInstall()) {
                this.addToList(version, list, en);
            }
        }
        if (TLauncher.DEBUG) {
            U.log("backed files");
            for (final File f2 : list) {
                U.log(f2);
            }
        }
        return list;
    }
    
    private void addToList(final File version, final List<File> list, final GameEntityDTO en) {
        list.add(new File(version, en.getVersion().getMetadata().getPath()));
    }
    
    public void installModPack(final File file, final ModpackBackupFrame.HandleListener handleListener) {
        final File versionFolder;
        List<String> modpackNames;
        final Iterator<String> iterator;
        String name;
        File modPackFolder;
        final File file2;
        File fileVersion;
        CompleteVersion version;
        final Iterator<GameEntityListener> iterator2;
        GameEntityListener listener;
        AsyncThread.execute(() -> {
            this.log("installModPack");
            versionFolder = FileUtil.getRelative("versions").toFile();
            try {
                modpackNames = this.analizeArchiver(file);
                FileUtil.unzipUniversal(file, versionFolder);
                modpackNames.iterator();
                while (iterator.hasNext()) {
                    name = iterator.next();
                    modPackFolder = new File(versionFolder, name);
                    new File(modPackFolder, name + ".json");
                    fileVersion = file2;
                    if (Files.notExists(fileVersion.toPath(), new LinkOption[0])) {
                        version = this.parseCurse(versionFolder, name, modPackFolder, fileVersion);
                    }
                    else {
                        version = this.gson.fromJson(FileUtil.readFile(fileVersion), CompleteVersion.class);
                    }
                    this.gameListeners.get(GameType.MODPACK).iterator();
                    while (iterator2.hasNext()) {
                        listener = iterator2.next();
                        listener.installEntity(version);
                    }
                }
                handleListener.installedSuccess(modpackNames);
            }
            catch (Exception e) {
                U.log(e);
                handleListener.processError(e);
            }
        });
    }
    
    private CompleteVersion parseCurse(final File versionFolder, final String modpackName, final File modPackFolder, final File version) throws Exception {
        final File instance = new File(modPackFolder, "minecraftinstance.json");
        if (!Files.exists(instance.toPath(), new LinkOption[0])) {
            throw new Exception("dont' find config file");
        }
        final File mods = new File(modPackFolder, "mods");
        final File resourcepacks = new File(modPackFolder, "resourcepacks");
        final File maps = new File(modPackFolder, "saves");
        if (!Files.exists(mods.toPath(), new LinkOption[0]) && !Files.exists(resourcepacks.toPath(), new LinkOption[0]) && !Files.exists(maps.toPath(), new LinkOption[0])) {
            this.log("modpack doesn't exist");
        }
        final MinecraftInstance minecraftInstance = this.gson.fromJson(FileUtil.readFile(instance), MinecraftInstance.class);
        if (minecraftInstance == null) {
            throw new Exception("broken config");
        }
        final ModpackDTO modPack = new ModpackDTO();
        modPack.setId(-U.n());
        final ModpackVersionDTO modpackVersion = new ModpackVersionDTO();
        final String[] formats = { "jar", "zip" };
        final CompleteVersion completeVersion = this.gson.fromJson(minecraftInstance.baseModLoader.VersionJson, CompleteVersion.class);
        modpackVersion.setForgeVersion(completeVersion.getID());
        modpackVersion.setGameVersion(minecraftInstance.baseModLoader.MinecraftVersion);
        completeVersion.setID(modpackName);
        modPack.setName(completeVersion.getID());
        modpackVersion.setId(-U.n() - 1L);
        modpackVersion.setName("1.0");
        modpackVersion.setMods((List<ModDTO>)this.createHandleGameEntities(mods, formats, ModDTO.class));
        modpackVersion.setResourcePacks((List<ResourcePackDTO>)this.createHandleGameEntities(resourcepacks, formats, ResourcePackDTO.class));
        modpackVersion.setMaps(this.createMapsByFolder(maps));
        modPack.setVersion(modpackVersion);
        completeVersion.setModpackDTO(modPack);
        FileUtil.writeFile(version, this.gson.toJson(completeVersion));
        return completeVersion;
    }
    
    private List<MapDTO> createMapsByFolder(final File maps) {
        final List<MapDTO> list = new ArrayList<MapDTO>();
        final FilenameFilter filter = (dir, name) -> dir.isDirectory();
        for (final File file : Objects.requireNonNull(maps.listFiles(filter))) {
            final MapDTO map = new MapDTO();
            final VersionDTO v = new VersionDTO();
            v.setName("1.0");
            final MetadataDTO meta = new MetadataDTO();
            meta.setPath(FileUtil.GameEntityFolder.getPath(GameType.MAP) + "/" + file.getName() + ".zip");
            v.setMetadata(meta);
            map.setUserInstall(true);
            map.setName(file.getName());
            map.setVersion(v);
            list.add(map);
        }
        return list;
    }
    
    public List<String> analizeArchiver(final File file) throws ParseModPackException {
        try {
            final String ext = FilenameUtils.getExtension(file.getCanonicalPath());
            List<String> list = new ArrayList<String>();
            final String s = ext;
            switch (s) {
                case "rar": {
                    list = FileUtil.topFolders(new Archive(file));
                    break;
                }
                case "zip": {
                    list = FileUtil.topFolders(new ZipFile(file));
                    break;
                }
            }
            if (list.isEmpty()) {
                throw new ParseModPackException("The archive doesn't contain any folders");
            }
            if (!this.checkNameVersion(list)) {
                throw new ParseModPackException("there is a version with same name");
            }
            return list;
        }
        catch (Exception e) {
            throw new ParseModPackException(e);
        }
    }
    
    public boolean checkNameVersion(final List<String> list) {
        for (final String s : list) {
            if (Objects.nonNull(this.tLauncher.getVersionManager().getVersionSyncInfo(s))) {
                return false;
            }
        }
        return true;
    }
    
    private List<? extends GameEntityDTO> createHandleGameEntities(final File folder, final String[] exts, final Class<? extends GameEntityDTO> t) {
        final List<GameEntityDTO> list = new ArrayList<GameEntityDTO>();
        final List<File> files = (List<File>)(List)FileUtils.listFiles(folder, exts, true);
        for (final File f : files) {
            try {
                final GameEntityDTO c = this.createHandleGameEntity(folder, t, f);
                list.add(c);
            }
            catch (InstantiationException | IllegalAccessException ex2) {
                final ReflectiveOperationException ex;
                final ReflectiveOperationException e = ex;
                this.log(e);
            }
        }
        return list;
    }
    
    private GameEntityDTO createHandleGameEntity(final File folder, final Class<? extends GameEntityDTO> t, final File f) throws InstantiationException, IllegalAccessException {
        final GameEntityDTO c = (GameEntityDTO)t.newInstance();
        c.setId(-U.n());
        c.setName(FilenameUtils.getBaseName(f.getName()));
        c.setUserInstall(true);
        final MetadataDTO meta = FileUtil.createMetadata(f, folder, t);
        meta.setPath(FileUtil.GameEntityFolder.getPath(t, true).concat(meta.getPath()));
        meta.setUrl(FileUtil.GameEntityFolder.getPath(t, true).concat(meta.getUrl()));
        final VersionDTO standardVersion = new VersionDTO();
        standardVersion.setId(-U.n());
        standardVersion.setName("1.0");
        standardVersion.setMetadata(meta);
        c.setVersion(standardVersion);
        return c;
    }
    
    public void createModpack(final String name, final ModpackDTO modpackDTO, final boolean usedSkin) {
        try {
            final CompleteVersion completeVersion = this.getForgeVersion(((ModpackVersionDTO)modpackDTO.getVersion()).getForgeVersion());
            completeVersion.setID(name);
            completeVersion.setModpackDTO(modpackDTO);
            this.tLauncher.getVersionManager().getLocalList().saveVersion(completeVersion);
            for (final GameEntityListener l : this.gameListeners.get(GameType.MODPACK)) {
                l.installEntity(completeVersion);
                l.installEntity(completeVersion.getModpack(), GameType.MODPACK);
            }
            if (usedSkin) {
                final GameEntityDTO entityDTO = this.findById(this.getSkinId(modpackDTO), GameType.MOD);
                this.addSkinMod(modpackDTO, entityDTO);
            }
        }
        catch (IOException e) {
            U.log(e);
        }
    }
    
    public Long getSkinId(final ModpackDTO modpackDTO) {
        final GameEntityDTO gameEntityDTO = this.findById(ModDTO.TL_SKIN_CAPE_ID, GameType.MOD);
        final BaseModpackFilter filter = BaseModpackFilter.getBaseModpackStandardFilters(gameEntityDTO, GameType.MOD, modpackDTO);
        final List<VersionDTO> l = (List<VersionDTO>)ModpackUtil.sortByDate(filter.findAll(gameEntityDTO.getVersions()));
        if (l.isEmpty()) {
            return ModDTO.TL_SKIN_ID;
        }
        return ModDTO.TL_SKIN_CAPE_ID;
    }
    
    private void addSkinMod(final ModpackDTO modpackDTO, final GameEntityDTO entityDTO) {
        if (entityDTO != null) {
            final List<VersionDTO> versionDTOS = BaseModpackFilter.getBaseModpackStandardFilters(entityDTO, GameType.MOD, modpackDTO).findAll(entityDTO.getVersions());
            if (!versionDTOS.isEmpty()) {
                this.installEntity(entityDTO, versionDTOS.get(0), GameType.MOD, false);
            }
        }
    }
    
    private GameEntityDTO findById(final Long id, final GameType type) {
        for (final GameEntityDTO dto : this.getInfoMod().getByType(type)) {
            if (dto.getId().equals(id)) {
                return dto;
            }
        }
        return null;
    }
    
    void addEntityToModpack(final GameEntityDTO gameEntity, final CompleteVersion completeVersion, final GameType type) throws IOException {
        final ModpackDTO modPack = completeVersion.getModpack();
        ModpackVersionDTO modpackVersion = (ModpackVersionDTO)modPack.getVersion();
        if (modpackVersion == null) {
            modpackVersion = new ModpackVersionDTO();
            modpackVersion.setMaps(new ArrayList<MapDTO>());
            modpackVersion.setMods(new ArrayList<ModDTO>());
            modpackVersion.setResourcePacks(new ArrayList<ResourcePackDTO>());
            modPack.setVersion(modpackVersion);
        }
        try {
            final GameEntityDTO removedEntity = this.findAndRemoveGameEntity(completeVersion, gameEntity, type);
            for (final GameEntityListener l : this.gameListeners.get(type)) {
                l.removeEntity(removedEntity);
            }
        }
        catch (GameEntityNotFound gameEntityNotFound) {}
        switch (type) {
            case MOD: {
                modpackVersion.getMods().add((ModDTO)gameEntity);
                break;
            }
            case RESOURCEPACK: {
                modpackVersion.getResourcePacks().add((ResourcePackDTO)gameEntity);
                break;
            }
            case MAP: {
                modpackVersion.getMaps().add((MapDTO)gameEntity);
                break;
            }
            default: {
                U.log("there is the problem with", gameEntity);
                break;
            }
        }
    }
    
    private boolean checkAddedElement(final VersionDTO version, final GameType type, final GameEntityDTO entity) {
        final ModpackVersionDTO v = (ModpackVersionDTO)version;
        if (v != null && type != GameType.MODPACK) {
            for (final GameEntityDTO en : v.getByType(type)) {
                if (en.getId().equals(entity.getId())) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public void showFullGameEntity(final GameEntityDTO entity, final GameType type) {
        GameEntityDTO remote;
        MainPane mp;
        AsyncThread.execute(() -> {
            synchronized (this) {
                try {
                    remote = this.findRemoteEntity(entity, type);
                    if (remote != null) {
                        this.fillPicturesAndDescription(type, remote);
                        mp = this.tLauncher.getFrame().mp;
                        if (type == GameType.MODPACK) {
                            mp.modpackEnitityScene.showModpackEntity(remote);
                        }
                        else {
                            mp.completeSubEntityScene.showFullGameEntity(remote, type);
                        }
                    }
                }
                catch (SocketTimeoutException e2) {
                    Alert.showLocError("modpack.internet.update");
                }
                catch (IOException e) {
                    U.log(e);
                    Alert.showError("", Localizable.get("modpack.remote.not.found", Localizable.get("modpack.try.later")), null);
                }
            }
        });
    }
    
    private void fillPicturesAndDescription(final GameType type, final GameEntityDTO remote) throws IOException {
        if (remote.getPictures() == null) {
            final String request = this.innerConfiguration.get("modpack.url") + remote.getClass().getSimpleName().toLowerCase() + "/update/full";
            final Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", remote.getId());
            map.put("lang", this.tLauncher.getConfiguration().getLocale().toString().toUpperCase());
            if (type == GameType.MODPACK) {
                map.put("versionId", remote.getVersions().get(0).getId());
            }
            final String result = Http.performGet(request, map, this.innerConfiguration.getInteger("modpack.update.time.connect"), this.innerConfiguration.getInteger("modpack.update.time.connect"));
            final GameEntityDTO updated = this.gson.fromJson(result, remote.getClass());
            remote.setPictures(updated.getPictures());
            remote.setDescription(updated.getDescription());
            remote.setOfficialSite(updated.getOfficialSite());
            if (type == GameType.MODPACK) {
                remote.setVersion(updated.getVersion());
            }
        }
    }
    
    private GameEntityDTO findRemoteEntity(final GameEntityDTO entity, final GameType type) {
        for (final GameEntityDTO en : this.infoMod.getByType(type)) {
            if (en.getId().equals(entity.getId())) {
                return en;
            }
        }
        return null;
    }
    
    public void showInnerModpackElement(GameEntityDTO entity, final GameEntityDTO parent, final GameType type) {
        entity = this.findRemoteEntity(entity, type);
        if (entity == null) {
            return;
        }
        try {
            this.fillPicturesAndDescription(type, entity);
            this.tLauncher.getFrame().mp.completeSubEntityScene.showModpackElement(entity, type);
        }
        catch (SocketTimeoutException e2) {
            Alert.showLocError("modpack.internet.update");
        }
        catch (IOException e) {
            U.log(e);
            Alert.showError("", Localizable.get("modpack.remote.not.found", Localizable.get("modpack.try.later")), null);
        }
    }
    
    public <T> T readFromServer(final Class<T> t, final GameEntityDTO e, final VersionDTO version) throws IOException {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("id", e.getId());
        map.put("versionId", version.getId());
        final String res = Http.performGet(this.innerConfiguration.get("modpack.url") + t.getSimpleName().toLowerCase(), map, U.getConnectionTimeout(), this.innerConfiguration.getInteger("modpack.update.time.connect"));
        return this.gson.fromJson(res, t);
    }
    
    public void sendToServer(final ModpackServerCommand command, final GameEntityDTO entity, final GameType type) {
        final URL url2;
        URL url;
        AsyncThread.execute(() -> {
            try {
                switch (command) {
                    case UPDATE:
                    case DOWNLOAD: {
                        new URL(this.innerConfiguration.get("modpack.operation.url") + command.toString().toLowerCase() + "/" + entity.getId());
                        url = url2;
                        try {
                            Http.performPost(url, String.valueOf(new HashMap()), "application/json");
                        }
                        catch (IOException e2) {
                            U.log("can't send operation " + command);
                        }
                        break;
                    }
                }
            }
            catch (IOException e) {
                Alert.showMonologError(Localizable.get().get("modpack.error.send.unsuccess"), 0);
                U.log(e);
            }
        });
    }
    
    @Override
    public void onVersionsRefreshing(final VersionManager manager) {
    }
    
    @Override
    public void onVersionsRefreshingFailed(final VersionManager manager) {
    }
    
    @Override
    public void onVersionsRefreshed(final VersionManager manager) {
        if (!manager.isLocalRefresh()) {
            this.loadInfo();
        }
    }
    
    private void processGameElementByStatus() {
        this.readStatusGameElement();
        this.setStatusByType(GameType.MAP);
        this.setStatusByType(GameType.MOD);
        this.setStatusByType(GameType.MODPACK);
        this.setStatusByType(GameType.RESOURCEPACK);
    }
    
    private void setStatusByType(final GameType type) {
        for (final GameEntityDTO remote : this.infoMod.getByType(type)) {
            if (this.statusModpackElement.contains(remote.getId())) {
                remote.setPopulateStatus(true);
            }
        }
    }
    
    public void addStatusElement(final GameEntityDTO e, final GameType type) {
        this.statusModpackElement.add(e.getId());
        this.writeStatusGameElement();
        for (final GameEntityListener l : this.gameListeners.get(type)) {
            l.populateStatus(e, type, true);
        }
        for (final GameEntityDTO en : this.infoMod.getByType(type)) {
            if (en.getId().equals(e.getId())) {
                en.setPopulateStatus(true);
            }
        }
    }
    
    public void removeStatusElement(final GameEntityDTO e, final GameType type) {
        this.statusModpackElement.remove(e.getId());
        this.writeStatusGameElement();
        for (final GameEntityListener l : this.gameListeners.get(type)) {
            l.populateStatus(e, type, false);
        }
        for (final GameEntityDTO en : this.infoMod.getByType(type)) {
            if (en.getId().equals(e.getId())) {
                en.setPopulateStatus(false);
            }
        }
    }
    
    private void readStatusGameElement() {
        try {
            if (this.STATUS_MODPACK_FILE.exists()) {
                this.statusModpackElement = this.gson.fromJson(FileUtil.readFile(this.STATUS_MODPACK_FILE), new TypeToken<HashSet<Long>>() {}.getType());
            }
            else {
                this.statusModpackElement = new HashSet<Long>();
                this.writeStatusGameElement();
            }
        }
        catch (IOException | JsonSyntaxException ex2) {
            final Exception ex;
            final Exception e = ex;
            U.log(e);
            this.statusModpackElement = new HashSet<Long>();
            this.writeStatusGameElement();
        }
    }
    
    private void writeStatusGameElement() {
        try {
            FileUtil.writeFile(this.STATUS_MODPACK_FILE, this.gson.toJson(this.statusModpackElement));
        }
        catch (IOException e1) {
            U.log(e1);
        }
    }
    
    public synchronized InfoMod getInfoMod() {
        return this.infoMod;
    }
    
    private synchronized void setInfoMod(final InfoMod infoMod) {
        this.infoMod = infoMod;
    }
    
    private List<CompleteVersion> getModpackVersions() {
        final List<CompleteVersion> versions = new ArrayList<CompleteVersion>();
        for (final Version v : TLauncher.getInstance().getVersionManager().getLocalList().getVersions()) {
            if (((CompleteVersion)v).isModpack()) {
                versions.add((CompleteVersion)v);
            }
        }
        return versions;
    }
    
    public void changeModpackElementState(final GameEntityDTO entity, final GameType type) {
        final CompleteVersion completeVersion = this.tLauncher.getFrame().mp.modpackScene.getSelectedCompleteVersion();
        final ModpackVersionDTO versionDTO = (ModpackVersionDTO)completeVersion.getModpack().getVersion();
        final Optional<? extends GameEntityDTO> op = (Optional<? extends GameEntityDTO>)versionDTO.getByType(type).stream().filter(e -> e.getId().equals(entity.getId())).findFirst();
        if (!op.isPresent()) {
            return;
        }
        final SubModpackDTO en = (SubModpackDTO)op.get();
        final Iterator<GameEntityListener> iterator;
        GameEntityListener l;
        final SubModpackDTO entity2;
        final CompleteVersion version;
        final Iterator<GameEntityDependencyDTO> iterator2;
        GameEntityDependencyDTO d;
        final ModpackVersionDTO modpackVersionDTO;
        Optional<? extends GameEntityDTO> optional;
        final Iterator<GameEntityListener> iterator3;
        GameEntityListener i;
        final Iterator<GameEntityListener> iterator4;
        GameEntityListener j;
        AsyncThread.execute(() -> {
            this.gameListeners.get(type).iterator();
            while (iterator.hasNext()) {
                l = iterator.next();
                l.activationStarted(entity2);
            }
            try {
                switch (type) {
                    case MOD:
                    case RESOURCEPACK: {
                        this.changeActivation(entity2, type, version);
                        if (entity2.getDependencies() != null && entity2.getStateGameElement() == StateGameElement.ACTIVE) {
                            entity2.getDependencies().iterator();
                            while (iterator2.hasNext()) {
                                d = iterator2.next();
                                if (d.getDependencyType() == DependencyType.REQUIRED) {
                                    optional = (Optional<? extends GameEntityDTO>)modpackVersionDTO.getByType(type).stream().filter(e -> e.getId().equals(d.getId())).filter(e -> e.getStateGameElement() == StateGameElement.NO_ACTIVE).findFirst();
                                    if (optional.isPresent()) {
                                        this.changeActivation((SubModpackDTO)optional.get(), d.getGameType(), version);
                                    }
                                    else {
                                        continue;
                                    }
                                }
                            }
                            break;
                        }
                        else {
                            break;
                        }
                        break;
                    }
                    default: {
                        this.log("strange type of ", type, "for entity ", entity);
                        break;
                    }
                }
                this.gameListeners.get(GameType.MODPACK).iterator();
                while (iterator3.hasNext()) {
                    i = iterator3.next();
                    i.updateVersion(version, version);
                }
                this.tLauncher.getVersionManager().getLocalList().refreshLocalVersion(version);
            }
            catch (Exception e2) {
                this.log(e2);
                this.gameListeners.get(type).iterator();
                while (iterator4.hasNext()) {
                    j = iterator4.next();
                    j.activationError(entity, e2);
                }
            }
        });
    }
    
    private void changeActivation(final SubModpackDTO entity, final GameType type, final CompleteVersion completeVersion) throws IOException {
        final File modpackFolder = FileUtil.getRelative("versions/" + completeVersion.getID()).toFile();
        try {
            final File target = new File(modpackFolder, entity.getVersion().getMetadata().getPath());
            if (entity.getStateGameElement() == StateGameElement.ACTIVE) {
                Files.move(target.toPath(), Paths.get(target.toString() + ".deactivation", new String[0]), StandardCopyOption.REPLACE_EXISTING);
            }
            else {
                Files.move(Paths.get(target.toString() + ".deactivation", new String[0]), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        }
        catch (NoSuchFileException e) {
            this.log(entity.getStateGameElement() + " ", e.getMessage());
        }
        if (entity.getStateGameElement() == StateGameElement.NO_ACTIVE) {
            entity.setStateGameElement(StateGameElement.ACTIVE);
        }
        else {
            entity.setStateGameElement(StateGameElement.NO_ACTIVE);
        }
        for (final GameEntityListener l : this.gameListeners.get(type)) {
            l.activation(entity);
        }
    }
    
    public synchronized void installEntity(final GameEntityDTO e, final VersionDTO version, final GameType type) {
        if (type != GameType.MODPACK && !this.tLauncher.getFrame().mp.modpackScene.isSelectedCompleteVersion()) {
            Alert.showLocMessage("modpack.select.modpack");
            return;
        }
        for (final GameEntityListener l : this.gameListeners.get(type)) {
            l.processingStarted(e, version);
        }
        try {
            GameEntityDTO installEntity;
            if (type == GameType.MODPACK) {
                final CompleteVersion completeVersion = this.readFromServer(CompleteVersion.class, e, version);
                installEntity = completeVersion.getModpack();
                completeVersion.setID(installEntity.getName() + " " + installEntity.getVersion().getName());
                this.tLauncher.getVersionManager().getLocalList().saveVersion(completeVersion);
                for (final GameEntityListener i : this.gameListeners.get(type)) {
                    i.installEntity(completeVersion);
                }
            }
            else {
                final CompleteVersion completeVersion = this.tLauncher.getFrame().mp.modpackScene.getSelectedCompleteVersion();
                installEntity = this.readFromServer(e.getClass(), e, version);
                if (ModDTO.SKIN_MODS.contains(installEntity.getId())) {
                    completeVersion.setSkinVersion(true);
                }
                this.checkMapFolders(completeVersion, installEntity, type);
                this.addDependencies(e, completeVersion);
                this.addEntityToModpack(installEntity, completeVersion, type);
                this.resaveVersion(completeVersion);
            }
            for (final GameEntityListener j : this.gameListeners.get(type)) {
                j.installEntity(installEntity, type);
            }
            this.sendToServer(ModpackServerCommand.DOWNLOAD, installEntity, type);
        }
        catch (IOException e2) {
            U.log(e2);
            for (final GameEntityListener j : this.gameListeners.get(type)) {
                j.installError(e, version, e2);
            }
            Alert.showLocMessage("", "modpack.try.later", null);
        }
        catch (SameMapFoldersException e3) {
            Alert.showLocWarning("modpack.map.same.folder");
            for (final GameEntityListener j : this.gameListeners.get(type)) {
                j.installError(e, version, e3);
            }
        }
    }
    
    private void checkMapFolders(final CompleteVersion completeVersion, final GameEntityDTO installEntity, final GameType type) throws SameMapFoldersException {
        if (type != GameType.MAP) {
            return;
        }
        final List<String> remoteFolders = ((MapMetadataDTO)installEntity.getVersion().getMetadata()).getFolders();
        for (final MapDTO mapDTO : ((ModpackVersionDTO)completeVersion.getModpack().getVersion()).getMaps()) {
            final List<String> folders = ((MapMetadataDTO)mapDTO.getVersion().getMetadata()).getFolders();
            if (folders != null) {
                if (remoteFolders == null) {
                    continue;
                }
                if (!Collections.disjoint(folders, remoteFolders)) {
                    throw new SameMapFoldersException(String.format("maps have same folders local: %s remote: %s", folders.toString(), remoteFolders.toString()));
                }
                continue;
            }
        }
    }
    
    public void installEntity(final GameEntityDTO e, final VersionDTO version, final GameType type, final boolean async) {
        if (e.getCategories().contains(Category.CHITA) && type == GameType.MOD && !this.tLauncher.getProfileManager().hasPremium()) {
            Alert.showLocMessage("", "account.premium.chita", null);
            return;
        }
        if (async) {
            AsyncThread.execute(() -> this.installEntity(e, version, type));
        }
        else {
            this.installEntity(e, version, type);
        }
    }
    
    private void addDependencies(final GameEntityDTO e, final CompleteVersion version) throws IOException {
        final List<GameEntityDependencyDTO> list = e.getDependencies();
        if (list == null || list.size() == 0) {
            return;
        }
        final Map<GameEntityDTO, GameType> downloadedList = new HashMap<GameEntityDTO, GameType>();
        for (final GameEntityDependencyDTO d : list) {
            if (d.getDependencyType() != DependencyType.INCOMPATIBLE) {
                if (d.getDependencyType() == DependencyType.OPTIONAL) {
                    continue;
                }
                if (e.getId().equals(d.getId())) {
                    continue;
                }
                GameEntityDTO dto = new GameEntityDTO();
                dto.setId(d.getId());
                dto = this.findRemoteEntity(dto, d.getGameType());
                if (Objects.isNull(dto)) {
                    this.log("not found proper game entity for " + e.getName() + " " + version.getID());
                }
                else {
                    final List<VersionDTO> versions = BaseModpackFilter.getBaseModpackStandardFilters(dto, d.getGameType(), version.getModpack()).findAll(dto.getVersions());
                    if (versions.size() == 0) {
                        this.log("not found proper game entity for " + e.getName() + " " + version.getID());
                    }
                    else if (this.findGameFromCollection(dto, d.getGameType(), (ModpackVersionDTO)version.getModpack().getVersion()) != null) {
                        this.log("it has already added   " + dto.getName() + " " + dto.getId());
                    }
                    else {
                        this.addDependencies(dto, version);
                        final GameEntityDTO install = this.readFromServer(GameType.createDTO(d.getGameType()), dto, (VersionDTO)ModpackUtil.sortByDate(versions).get(0));
                        downloadedList.put(install, d.getGameType());
                    }
                }
            }
        }
        for (final Map.Entry<GameEntityDTO, GameType> entry : downloadedList.entrySet()) {
            this.addEntityToModpack(entry.getKey(), version, entry.getValue());
            for (final GameEntityListener l : this.gameListeners.get(entry.getValue())) {
                l.installEntity(entry.getKey(), entry.getValue());
            }
        }
    }
    
    public void installHandleEntity(final File[] files, final CompleteVersion completeVersion, final GameType type, final HandleInstallModpackElementFrame.HandleListener handleListener) {
        final Path folder = FileUtil.getRelative("versions/" + completeVersion.getID() + "/" + FileUtil.GameEntityFolder.getPath(type));
        folder.toFile().mkdir();
        try {
            final List<GameEntityDTO> list = new ArrayList<GameEntityDTO>();
            for (final File f : files) {
                final GameEntityDTO entity = this.initHanldeEntity(type, f, folder);
                final Path target = Paths.get(folder.toString(), f.getName());
                if (!this.checkAddedElement(completeVersion.getModpack().getVersion(), type, entity)) {
                    throw new ParseModPackException("entity exists" + entity);
                }
                Files.copy(f.toPath(), target, new CopyOption[0]);
                if (type == GameType.MAP) {
                    FileUtil.unzipUniversal(target.toFile(), target.toFile().getParentFile());
                    FileUtil.deleteFile(target.toFile());
                }
                this.addEntityToModpack(entity, completeVersion, type);
                list.add(entity);
            }
            for (final GameEntityDTO entity2 : list) {
                for (final GameEntityListener l : this.gameListeners.get(type)) {
                    l.installEntity(entity2, type);
                }
            }
            this.resaveVersion(completeVersion);
            handleListener.installedSuccess();
        }
        catch (Exception e) {
            U.log(e);
            handleListener.processError(e);
        }
    }
    
    private GameEntityDTO initHanldeEntity(final GameType type, final File f, final Path folder) throws ParseModPackException {
        VersionDTO versionDTO = new VersionDTO();
        SubModpackDTO entity = null;
        switch (type) {
            case MOD: {
                final ModDTO modDTO = new ModDTO();
                final ModVersionDTO modVersionDTO = new ModVersionDTO();
                modVersionDTO.setIncompatibleMods(new ArrayList<String>());
                modVersionDTO.setIncompatibleMods(new ArrayList<String>());
                versionDTO = modVersionDTO;
                entity = modDTO;
                break;
            }
            case MAP: {
                entity = new MapDTO();
                break;
            }
            case RESOURCEPACK: {
                entity = new ResourcePackDTO();
                break;
            }
            default: {
                throw new ParseModPackException("not proper type");
            }
        }
        entity.setId(-U.n());
        versionDTO.setId(-U.n() - 1L);
        entity.setVersion(versionDTO);
        entity.setStateGameElement(StateGameElement.ACTIVE);
        entity.setName(FilenameUtils.getBaseName(f.getName()));
        entity.setUserInstall(true);
        final VersionDTO v = new VersionDTO();
        v.setName("1.0");
        MetadataDTO meta = new MetadataDTO();
        if (type == GameType.MAP) {
            final MapMetadataDTO mapMetadata = new MapMetadataDTO();
            mapMetadata.setFolders(this.analizeArchiver(f));
            meta = mapMetadata;
        }
        if (type == GameType.MAP) {
            meta.setPath("saves/" + f.getName());
        }
        else {
            meta.setPath(type.toString() + "s/" + f.getName());
        }
        v.setMetadata(meta);
        entity.setVersion(v);
        return entity;
    }
    
    public synchronized void removeEntity(final GameEntityDTO entity, final VersionDTO versionDTO, final GameType type) {
        for (final GameEntityListener l : this.gameListeners.get(type)) {
            l.processingStarted(entity, versionDTO);
        }
        try {
            GameEntityDTO removedEntity = entity;
            switch (type) {
                case MODPACK: {
                    final CompleteVersion version = this.tLauncher.getFrame().mp.modpackScene.getCompleteVersion((ModpackDTO)entity, versionDTO);
                    this.tLauncher.getVersionManager().getLocalList().deleteVersion(version.getID(), false);
                    for (final GameEntityListener i : this.gameListeners.get(type)) {
                        i.removeCompleteVersion(version);
                    }
                    this.tLauncher.getVersionManager().getLocalList().refreshVersions();
                    removedEntity = version.getModpack();
                    break;
                }
                case MOD:
                case RESOURCEPACK:
                case MAP: {
                    removedEntity = this.findAndRemoveGameEntity(this.tLauncher.getFrame().mp.modpackScene.getSelectedCompleteVersion(), entity, type);
                    break;
                }
            }
            for (final GameEntityListener j : this.gameListeners.get(type)) {
                j.removeEntity(removedEntity);
            }
        }
        catch (Throwable e) {
            U.log(e);
            final Iterator<GameEntityListener> iterator4;
            GameEntityListener k;
            final Throwable t;
            SwingUtilities.invokeLater(() -> {
                this.gameListeners.get(type).iterator();
                while (iterator4.hasNext()) {
                    k = iterator4.next();
                    k.installError(entity, versionDTO, t);
                }
            });
        }
    }
    
    public void removeEntity(final GameEntityDTO entity, final VersionDTO versionDTO, final GameType type, final boolean sync) {
        if (sync) {
            this.removeEntity(entity, versionDTO, type);
        }
        else {
            AsyncThread.execute(() -> this.removeEntity(entity, versionDTO, type));
        }
    }
    
    private GameEntityDTO findAndRemoveGameEntity(final CompleteVersion selected, final GameEntityDTO entity, final GameType type) throws IOException, GameEntityNotFound {
        final ModpackVersionDTO versionDTO = (ModpackVersionDTO)selected.getModpack().getVersion();
        final GameEntityDTO current = this.findGameFromCollection(entity, type, versionDTO);
        if (current != null) {
            final MetadataDTO meta = current.getVersion().getMetadata();
            File removedFile = null;
            switch (type) {
                case MOD:
                case RESOURCEPACK: {
                    removedFile = FileUtil.getRelative("versions/" + selected.getID() + "/" + meta.getPath()).toFile();
                    break;
                }
                case MAP: {
                    removedFile = FileUtil.getRelative("versions/" + selected.getID() + FilenameUtils.removeExtension(meta.getPath())).toFile();
                    break;
                }
            }
            FileUtil.deleteFile(removedFile);
            versionDTO.getByType(type).remove(current);
            if (ModDTO.SKIN_MODS.contains(current.getId())) {
                selected.setSkinVersion(false);
            }
            this.tLauncher.getVersionManager().getLocalList().refreshLocalVersion(selected);
            return current;
        }
        throw new GameEntityNotFound("can't find in complete version: " + selected.getID() + " gameEntity: " + entity.getName());
    }
    
    private GameEntityDTO findGameFromCollection(final GameEntityDTO entity, final GameType type, final ModpackVersionDTO versionDTO) {
        for (final GameEntityDTO dto : versionDTO.getByType(type)) {
            if (entity.getId().equals(dto.getId())) {
                return dto;
            }
        }
        return null;
    }
    
    public void addGameListener(final GameType type, final GameEntityListener listener) {
        this.gameListeners.get(type).add(listener);
    }
    
    public void removeGameListener(final GameType type, final GameEntityListener listener) {
        this.gameListeners.get(type).remove(listener);
    }
    
    public void renameModpack(final CompleteVersion version, final String newName) {
        try {
            final String oldName = version.getID();
            final CompleteVersion newVersion = this.tLauncher.getVersionManager().getLocalList().renameVersion(version, newName);
            this.tLauncher.getVersionManager().getLocalList().refreshVersions();
            for (final GameEntityListener l : this.gameListeners.get(GameType.MODPACK)) {
                l.updateVersion(version, newVersion);
            }
            version.setID(newName);
        }
        catch (IOException e) {
            U.log(e);
            Alert.showError(Localizable.get("modpack.rename.exception.title"), Localizable.get("modpack.rename.exception"));
        }
    }
    
    public void resaveVersion(final CompleteVersion completeVersion) {
        try {
            TLauncher.getInstance().getVersionManager().getLocalList().refreshLocalVersion(completeVersion);
            for (final GameEntityListener l : this.gameListeners.get(GameType.MODPACK)) {
                l.updateVersion(completeVersion, completeVersion);
            }
        }
        catch (IOException e) {
            U.log(e);
            Alert.showError(Localizable.get("modpack.resave.exception.title"), Localizable.get("modpack.resave.exception"));
        }
    }
    
    public void checkFolderSubGameEntity(final CompleteVersion selectedValue, final GameType current) {
        final ModpackVersionDTO versionDTO = (ModpackVersionDTO)selectedValue.getModpack().getVersion();
        final Path subFolder = ModpackUtil.getPathByVersion(selectedValue, FileUtil.GameEntityFolder.getPath(current));
        if (Files.notExists(subFolder, new LinkOption[0])) {
            return;
        }
        boolean find = false;
        switch (current) {
            case MOD: {
                find = this.isFind(FileUtils.listFiles(subFolder.toFile(), new String[] { "jar", "zip" }, true), current, versionDTO, subFolder);
                break;
            }
            case RESOURCEPACK: {
                find = this.isFind(FileUtils.listFiles(subFolder.toFile(), new String[] { "zip" }, true), current, versionDTO, subFolder);
                break;
            }
            case MAP: {
                find = this.isFindMap(current, versionDTO, subFolder);
                break;
            }
            default: {
                return;
            }
        }
        if (find) {
            this.resaveVersion(selectedValue);
        }
    }
    
    private boolean isFindMap(final GameType current, final ModpackVersionDTO versionDTO, final Path subFolder) {
        boolean find = false;
        final Set<String> set = new HashSet<String>();
        for (final GameEntityDTO d : versionDTO.getByType(current)) {
            if (((MapMetadataDTO)d.getVersion().getMetadata()).getFolders() != null) {
                set.addAll(((MapMetadataDTO)d.getVersion().getMetadata()).getFolders());
            }
        }
        final String[] array = subFolder.toFile().list(DirectoryFileFilter.DIRECTORY);
        for (final String m : Objects.requireNonNull(array)) {
            if (!set.contains(m)) {
                try {
                    final GameEntityDTO dto = this.createHandleGameEntity(subFolder.toFile(), MapDTO.class, new File(subFolder.toFile(), m));
                    versionDTO.getByType(current).add((GameEntityDTO)dto);
                }
                catch (InstantiationException | IllegalAccessException ex2) {
                    final ReflectiveOperationException ex;
                    final ReflectiveOperationException e = ex;
                    U.log(new Object[0]);
                }
                find = true;
            }
        }
        return find;
    }
    
    private boolean isFind(final Collection<File> list, final GameType current, final ModpackVersionDTO versionDTO, final Path subFolder) {
        boolean find = false;
        for (final File f : list) {
            boolean foundFile = false;
            final String name = f.getName();
            for (final GameEntityDTO g : versionDTO.getByType(current)) {
                if (g.getVersion().getMetadata().getPath().endsWith(name)) {
                    foundFile = true;
                    break;
                }
            }
            if (!foundFile) {
                try {
                    final GameEntityDTO dto = this.createHandleGameEntity(subFolder.toFile(), GameType.createDTO(current), f);
                    versionDTO.getByType(current).add((GameEntityDTO)dto);
                    find = true;
                }
                catch (Exception e) {
                    this.log(e);
                }
            }
        }
        return find;
    }
    
    public void openModpackFolder(final CompleteVersion version) {
        OS.openFolder(FileUtil.getRelative("versions/" + version.getID()).toFile());
    }
    
    public CompleteVersion getForgeVersion(final String name) throws IOException {
        return this.gson.fromJson(Http.performGet(this.innerConfiguration.get("modpack.url") + "forgeversion?name=" + name), CompleteVersion.class);
    }
    
    private boolean fillFromCache(final GameType t, final GameEntityDTO e, final ModpackVersionDTO versionDTO, final Path versionFolder) {
        for (final CompleteVersion v : this.getModpackVersions()) {
            if (v.getModpack().getVersion() != versionDTO) {
                for (final GameEntityDTO g : ((ModpackVersionDTO)v.getModpack().getVersion()).getByType(t)) {
                    if (((SubModpackDTO)g).getStateGameElement() != StateGameElement.NO_ACTIVE && !g.isUserInstall() && !e.isUserInstall() && e.getId().equals(g.getId()) && e.getVersion().getId().equals(g.getVersion().getId())) {
                        final Path cachedFolder = ModpackUtil.getPathByVersion(v);
                        if (this.notExistOrCorrect(cachedFolder, e, true)) {
                            continue;
                        }
                        final File dest = new File(versionFolder.toFile(), e.getVersion().getMetadata().getPath());
                        try {
                            FileUtil.copyFile(new File(cachedFolder.toFile(), e.getVersion().getMetadata().getPath()), dest, true);
                            return true;
                        }
                        catch (IOException e2) {
                            this.log(e2);
                            if (dest.exists()) {
                                FileUtil.deleteFile(dest);
                            }
                            return false;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    public List<GameEntityDTO> findDependenciesFromGameEntityDTO(final GameEntityDTO entityDTO) {
        final List<GameEntityDTO> list = new ArrayList<GameEntityDTO>();
        final CompleteVersion completeVersion = this.tLauncher.getFrame().mp.modpackScene.getSelectedCompleteVersion();
        if (completeVersion != null && !entityDTO.isUserInstall() && ((SubModpackDTO)entityDTO).getStateGameElement() != StateGameElement.NO_ACTIVE) {
            for (final GameType t : GameType.values()) {
                for (final GameEntityDTO g : ((ModpackVersionDTO)completeVersion.getModpack().getVersion()).getByType(t)) {
                    if (g.getDependencies() != null && ((SubModpackDTO)g).getStateGameElement() != StateGameElement.NO_ACTIVE) {
                        for (final GameEntityDependencyDTO d : g.getDependencies()) {
                            if (d.getDependencyType() == DependencyType.REQUIRED && d.getId().equals(entityDTO.getId())) {
                                list.add(g);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }
    
    public void resetInfoMod() {
        this.infoMod = null;
    }
    
    public enum ModpackServerCommand
    {
        UPDATE, 
        DOWNLOAD, 
        ADD_NEW_GAME_ENTITY;
    }
}
