// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import org.tlauncher.util.async.LoopedThread;
import org.tlauncher.tlauncher.component.InterruptibleComponent;
import org.tlauncher.tlauncher.component.RefreshableComponent;
import java.util.Iterator;
import java.lang.reflect.Constructor;
import org.tlauncher.tlauncher.component.ComponentDependence;
import java.util.Collections;
import java.util.ArrayList;
import org.tlauncher.tlauncher.component.LauncherComponent;
import java.util.List;
import org.tlauncher.tlauncher.rmo.TLauncher;

public class ComponentManager
{
    private TLauncher tlauncher;
    private final List<LauncherComponent> components;
    private final ComponentManagerRefresher refresher;
    
    public ComponentManager(final TLauncher tLauncher) {
        this.tlauncher = tLauncher;
        this.components = Collections.synchronizedList(new ArrayList<LauncherComponent>());
        (this.refresher = new ComponentManagerRefresher(this)).start();
    }
    
    public TLauncher getLauncher() {
        return this.tlauncher;
    }
    
    public <T extends LauncherComponent> T loadComponent(final Class<T> classOfT) {
        if (classOfT == null) {
            throw new NullPointerException();
        }
        if (this.hasComponent(classOfT)) {
            return (T)this.getComponent((Class<LauncherComponent>)classOfT);
        }
        final ComponentDependence dependence = classOfT.getAnnotation(ComponentDependence.class);
        if (dependence != null) {
            for (final Class<?> requiredClass : dependence.value()) {
                this.rawLoadComponent(requiredClass);
            }
        }
        return this.rawLoadComponent(classOfT);
    }
    
    private <T> T rawLoadComponent(final Class<T> classOfT) {
        if (classOfT == null) {
            throw new NullPointerException();
        }
        if (!LauncherComponent.class.isAssignableFrom(classOfT)) {
            throw new IllegalArgumentException("Given class is not a LauncherComponent: " + classOfT.getSimpleName());
        }
        Constructor<T> constructor;
        try {
            constructor = classOfT.getConstructor(ComponentManager.class);
        }
        catch (Exception e) {
            throw new IllegalStateException("Cannot get constructor for component: " + classOfT.getSimpleName(), e);
        }
        T instance;
        try {
            instance = constructor.newInstance(this);
        }
        catch (Exception e2) {
            throw new IllegalStateException("Cannot createScrollWrapper a new instance for component: " + classOfT.getSimpleName(), e2);
        }
        final LauncherComponent component = (LauncherComponent)instance;
        this.components.add(component);
        return instance;
    }
    
    public <T extends LauncherComponent> T getComponent(final Class<T> classOfT) {
        if (classOfT == null) {
            throw new NullPointerException();
        }
        for (final LauncherComponent component : this.components) {
            if (classOfT.isInstance(component)) {
                return (T)component;
            }
        }
        throw new IllegalArgumentException("Cannot find the component!");
    }
    
     <T extends LauncherComponent> boolean hasComponent(final Class<T> classOfT) {
        if (classOfT == null) {
            return false;
        }
        for (final LauncherComponent component : this.components) {
            if (classOfT.isInstance(component)) {
                return true;
            }
        }
        return false;
    }
    
    public <T> List<T> getComponentsOf(final Class<T> classOfE) {
        final List<T> list = new ArrayList<T>();
        if (classOfE == null) {
            return list;
        }
        for (final LauncherComponent component : this.components) {
            if (classOfE.isInstance(component)) {
                list.add((T)component);
            }
        }
        return list;
    }
    
    public void startAsyncRefresh() {
        this.refresher.iterate();
    }
    
    void startRefresh() {
        for (final LauncherComponent component : this.components) {
            if (component instanceof RefreshableComponent) {
                final RefreshableComponent interruptible = (RefreshableComponent)component;
                interruptible.refreshComponent();
            }
        }
    }
    
    public void stopRefresh() {
        for (final LauncherComponent component : this.components) {
            if (component instanceof InterruptibleComponent) {
                final InterruptibleComponent interruptible = (InterruptibleComponent)component;
                interruptible.stopRefresh();
            }
        }
    }
    
    public void setTlauncher(final TLauncher tlauncher) {
        this.tlauncher = tlauncher;
    }
    
    class ComponentManagerRefresher extends LoopedThread
    {
        private final ComponentManager parent;
        
        ComponentManagerRefresher(final ComponentManager manager) {
            super("ComponentManagerRefresher");
            this.parent = manager;
        }
        
        @Override
        protected void iterateOnce() {
            this.parent.startRefresh();
        }
    }
}
