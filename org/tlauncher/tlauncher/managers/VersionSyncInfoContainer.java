// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;

public class VersionSyncInfoContainer extends DownloadableContainer
{
    private final VersionSyncInfo version;
    
    public VersionSyncInfoContainer(final VersionSyncInfo version) {
        if (version == null) {
            throw new NullPointerException();
        }
        this.version = version;
    }
    
    public VersionSyncInfo getVersion() {
        return this.version;
    }
}
