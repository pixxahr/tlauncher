// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

public interface TlauncherManagerListener
{
    void onTlauncherUpdating(final TLauncherManager p0);
    
    void onTlauncherUpdated(final TLauncherManager p0);
}
