// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import java.util.Iterator;
import org.tlauncher.tlauncher.ui.block.Blocker;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.component.LauncherComponent;

public class ComponentManagerListenerHelper extends LauncherComponent implements Blockable, VersionManagerListener, TlauncherManagerListener
{
    private final List<ComponentManagerListener> listeners;
    
    public ComponentManagerListenerHelper(final ComponentManager manager) throws Exception {
        super(manager);
        this.listeners = Collections.synchronizedList(new ArrayList<ComponentManagerListener>());
        manager.getComponent(VersionManager.class).addListener(this);
    }
    
    public void addListener(final ComponentManagerListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        this.listeners.add(listener);
    }
    
    @Override
    public void onVersionsRefreshing(final VersionManager manager) {
        Blocker.block(this, manager);
    }
    
    @Override
    public void onVersionsRefreshingFailed(final VersionManager manager) {
        Blocker.unblock(this, manager);
    }
    
    @Override
    public void onVersionsRefreshed(final VersionManager manager) {
        Blocker.unblock(this, manager);
    }
    
    @Override
    public void block(final Object reason) {
        for (final ComponentManagerListener listener : this.listeners) {
            listener.onComponentsRefreshing(this.manager);
        }
    }
    
    @Override
    public void unblock(final Object reason) {
        for (final ComponentManagerListener listener : this.listeners) {
            listener.onComponentsRefreshed(this.manager);
        }
    }
    
    @Override
    public void onTlauncherUpdating(final TLauncherManager manager) {
        Blocker.block(this, manager);
    }
    
    @Override
    public void onTlauncherUpdated(final TLauncherManager manager) {
        Blocker.unblock(this, manager);
    }
}
