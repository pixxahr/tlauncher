// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import java.io.IOException;
import net.minecraft.launcher.updater.SkinVersionList;
import net.minecraft.launcher.updater.ExtraVersionList;
import net.minecraft.launcher.updater.OfficialVersionList;
import org.tlauncher.util.MinecraftUtil;
import net.minecraft.launcher.updater.RemoteVersionList;
import net.minecraft.launcher.updater.LocalVersionList;
import org.tlauncher.tlauncher.component.LauncherComponent;

public class VersionLists extends LauncherComponent
{
    private final LocalVersionList localList;
    private final RemoteVersionList[] remoteLists;
    
    public VersionLists(final ComponentManager manager) throws Exception {
        super(manager);
        this.localList = new LocalVersionList(MinecraftUtil.getWorkingDirectory());
        final OfficialVersionList officialList = new OfficialVersionList();
        final ExtraVersionList extraList = new ExtraVersionList();
        final SkinVersionList skinVersionList = new SkinVersionList();
        this.remoteLists = new RemoteVersionList[] { officialList, extraList, skinVersionList };
    }
    
    public LocalVersionList getLocal() {
        return this.localList;
    }
    
    public void updateLocal() throws IOException {
        this.localList.setBaseDirectory(MinecraftUtil.getWorkingDirectory());
    }
    
    public RemoteVersionList[] getRemoteLists() {
        return this.remoteLists;
    }
}
