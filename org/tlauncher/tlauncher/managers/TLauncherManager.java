// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

import com.google.gson.JsonSyntaxException;
import java.nio.file.NoSuchFileException;
import java.nio.file.InvalidPathException;
import org.tlauncher.tlauncher.minecraft.crash.Crash;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import org.apache.commons.io.FilenameUtils;
import java.nio.file.Path;
import org.tlauncher.util.FileUtil;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import java.nio.file.Paths;
import java.io.File;
import org.tlauncher.util.MinecraftUtil;
import org.apache.commons.lang3.StringUtils;
import java.util.Arrays;
import net.minecraft.launcher.versions.json.Argument;
import net.minecraft.launcher.versions.json.ArgumentType;
import java.util.Map;
import org.tlauncher.util.gson.DownloadUtil;
import org.tlauncher.tlauncher.entity.AdditionalModLib;
import java.util.Collection;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Iterator;
import net.minecraft.launcher.versions.Library;
import java.util.ArrayList;
import org.tlauncher.tlauncher.entity.TLauncherLib;
import java.util.List;
import net.minecraft.launcher.versions.CompleteVersion;
import java.util.Objects;
import net.minecraft.launcher.versions.Version;
import org.tlauncher.tlauncher.entity.TLauncherVersionChanger;
import com.google.gson.Gson;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListener;
import org.tlauncher.tlauncher.component.RefreshableComponent;

public class TLauncherManager extends RefreshableComponent implements MinecraftListener
{
    private Gson gson;
    private TLauncherVersionChanger tLauncherVersionChanger;
    
    public TLauncherManager(final ComponentManager manager) throws Exception {
        super(manager);
        this.gson = new Gson();
    }
    
    public boolean useTlauncherAuthlib(final Version version) {
        return (Objects.nonNull(this.tLauncherVersionChanger) && this.tLauncherVersionChanger.getTlauncherSkinCapeVersion().contains(version.getID())) || version.isSkinVersion();
    }
    
    private List<TLauncherLib> findAddedLibraries(final CompleteVersion complete) {
        final String id = complete.getID();
        final ArrayList<TLauncherLib> libList = new ArrayList<TLauncherLib>();
        for (final TLauncherLib tlauncherLib : this.tLauncherVersionChanger.getLibraries()) {
            if (tlauncherLib.isSupport(id)) {
                libList.add(tlauncherLib);
            }
            else {
                for (final Library library : complete.getLibraries()) {
                    if (tlauncherLib.isApply(library, complete.getID())) {
                        libList.add(tlauncherLib);
                    }
                }
            }
        }
        return libList;
    }
    
    private boolean applyTlauncherAuthlib(final CompleteVersion original) {
        final Configuration settings = TLauncher.getInstance().getConfiguration();
        return !Account.AccountType.valueOf(settings.get("running.account.type")).equals(Account.AccountType.MOJANG) && settings.getBoolean("skin.status.checkbox.state") && this.useTlauncherAuthlib(original);
    }
    
    public CompleteVersion addReplacedLibraries(final CompleteVersion original, final boolean customTexture) {
        if (Objects.isNull(this.tLauncherVersionChanger)) {
            return original;
        }
        this.log("add required libraries:", original.getID());
        final CompleteVersion complete = original.fullCopy(new CompleteVersion());
        for (final TLauncherLib lib : this.findAddedLibraries(original)) {
            complete.getLibraries().addAll(lib.getRequires());
            complete.getLibraries().add(lib);
        }
        for (final AdditionalModLib l : this.tLauncherVersionChanger.getAddedMods(complete, customTexture)) {
            complete.getLibraries().addAll(l.getRequires());
            complete.getLibraries().add(l);
        }
        return complete;
    }
    
    @Override
    protected boolean refresh() {
        try {
            this.tLauncherVersionChanger = DownloadUtil.loadObjectByKey("skin.config.library", TLauncherVersionChanger.class);
        }
        catch (Throwable e) {
            this.log("Failed to refresh TLancher manager", e);
            return false;
        }
        return true;
    }
    
    public CompleteVersion replacedLibraries(final CompleteVersion original) {
        if (Objects.isNull(this.tLauncherVersionChanger)) {
            return original;
        }
        final List<TLauncherLib> libList = this.findAddedLibraries(original);
        final CompleteVersion complete = original.fullCopy(new CompleteVersion());
        for (final TLauncherLib lib : libList) {
            if ((lib.getName().startsWith("org.tlauncher:authlib") || lib.getName().startsWith("customskinloader:CustomSkinLoader")) && !this.applyTlauncherAuthlib(complete)) {
                continue;
            }
            boolean added = false;
            final List<Library> list = complete.getLibraries();
            for (int i = 0; i < complete.getLibraries().size(); ++i) {
                final Library current = complete.getLibraries().get(i);
                if (lib.isApply(current, complete.getID())) {
                    this.log("library will be replaced:", current.getName());
                    complete.getLibraries().remove(i);
                    complete.getLibraries().add(i, lib);
                    added = true;
                    this.setAdditionalFields(complete, lib);
                    final List<Library> requiredLibraries = lib.getRequires();
                    this.addedRequiredLibraries(list, requiredLibraries);
                    break;
                }
            }
            if (added) {
                continue;
            }
            this.addedRequiredLibraries(complete.getLibraries(), lib.getRequires());
            complete.getLibraries().add(0, lib);
            this.log("library will be added:", lib.getName());
            this.setAdditionalFields(complete, lib);
        }
        return complete;
    }
    
    private void addedRequiredLibraries(final List<Library> list, final List<Library> requiredLibraries) {
        if (Objects.isNull(requiredLibraries)) {
            return;
        }
        for (final Library r : requiredLibraries) {
            boolean addedRequiredLib = false;
            for (int j = 0; j < list.size(); ++j) {
                final Library l = list.get(j);
                if (r.getPlainName().equals(l.getPlainName())) {
                    this.log("library will be replaced as required:", r.getName());
                    list.remove(j);
                    list.add(j, r);
                    addedRequiredLib = true;
                    break;
                }
            }
            if (!addedRequiredLib) {
                list.add(0, r);
                this.log("library will be added as required:", r.getName());
            }
        }
    }
    
    private void setAdditionalFields(final CompleteVersion complete, final TLauncherLib lib) {
        final Map<ArgumentType, List<Argument>> arguments = lib.getArguments();
        if (Objects.nonNull(arguments)) {
            for (final Map.Entry<ArgumentType, List<Argument>> arg : arguments.entrySet()) {
                final ArgumentType type = arg.getKey();
                final List<Argument> libArguments = arg.getValue();
                final List<Argument> versionArguments = complete.getArguments().get(type);
                for (final Argument a : libArguments) {
                    boolean added = false;
                    for (int i = 0; i < versionArguments.size(); ++i) {
                        if (Arrays.equals(a.getValues(), versionArguments.get(i).getValues())) {
                            versionArguments.remove(i);
                            versionArguments.add(i, a);
                            added = true;
                            break;
                        }
                    }
                    if (!added) {
                        versionArguments.add(a);
                    }
                }
            }
        }
        if (StringUtils.isNotBlank((CharSequence)lib.getMainClass())) {
            complete.setMainClass(lib.getMainClass());
        }
    }
    
    @Override
    public void onMinecraftPrepare() {
        this.cleanMods();
    }
    
    @Override
    public void onMinecraftAbort() {
    }
    
    @Override
    public void onMinecraftLaunch() {
        final File optifineTempFile = new File(MinecraftUtil.getWorkingDirectory(), TLauncher.getInnerSettings().get("skin.config.temp.optifine.file.new"));
        Path mods = Paths.get(MinecraftUtil.getWorkingDirectory().getAbsolutePath(), "mods");
        final CompleteVersion completeVersion = TLauncher.getInstance().getLauncher().getVersion();
        if (completeVersion.isModpack()) {
            mods = ModpackUtil.getPathByVersion(completeVersion, "mods");
        }
        this.printModsFiles("mods before", mods);
        if (!Files.exists(mods, new LinkOption[0])) {
            try {
                Files.createDirectory(mods, (FileAttribute<?>[])new FileAttribute[0]);
            }
            catch (IOException e1) {
                this.log(e1);
            }
        }
        final List<String> filesMods = this.readMods();
        this.log("read from ", optifineTempFile, filesMods);
        if (completeVersion.getID().startsWith("ForgeOptiFine")) {
            for (final Library library : completeVersion.getLibraries()) {
                if (library.getName().contains("optifine:OptiFine")) {
                    final Path out = this.copyMods(library);
                    filesMods.add(out.toString());
                    break;
                }
            }
        }
        if (completeVersion.getModsLibraries() != null) {
            final List<Library> libraries = completeVersion.getModsLibraries();
            this.copyListMods(filesMods, libraries);
        }
        if (Objects.nonNull(this.tLauncherVersionChanger)) {
            final List<AdditionalModLib> libraries2 = this.tLauncherVersionChanger.getAddedMods(completeVersion, this.applyTlauncherAuthlib(completeVersion));
            this.copyListMods(filesMods, libraries2);
        }
        try {
            final String result = this.gson.toJson(filesMods, new TypeToken<ArrayList<String>>() {}.getType());
            FileUtil.writeFile(optifineTempFile, result);
            this.log("has written in ", optifineTempFile.getName(), result);
        }
        catch (IOException e2) {
            this.log(e2);
        }
        this.printModsFiles("mods after", mods);
    }
    
    private void copyListMods(final List<String> filesMods, final List<? extends Library> libraries) {
        for (final Library lib : libraries) {
            filesMods.add(this.copyMods(lib).toString());
        }
    }
    
    private Path copyMods(final Library library) {
        final Path in = Paths.get(MinecraftUtil.getWorkingDirectory().getAbsolutePath(), "libraries", library.getArtifactPath());
        final Path out = Paths.get(MinecraftUtil.getWorkingDirectory().getAbsolutePath(), "mods", FilenameUtils.getName(library.getArtifactPath()));
        try {
            Files.copy(in, out, StandardCopyOption.REPLACE_EXISTING);
        }
        catch (IOException e) {
            this.log(e);
        }
        return out;
    }
    
    @Override
    public void onMinecraftClose() {
    }
    
    @Override
    public void onMinecraftError(final Throwable e) {
    }
    
    @Override
    public void onMinecraftKnownError(final MinecraftException e) {
    }
    
    @Override
    public void onMinecraftCrash(final Crash crash) {
    }
    
    public void cleanMods() {
        final Path mods1 = Paths.get(MinecraftUtil.getWorkingDirectory().getAbsolutePath(), "mods");
        this.printModsFiles("before clearLibrary", mods1);
        final Path old = FileUtil.getRelativeConfig("skin.config.temp.optifine.file");
        final Path mods2 = FileUtil.getRelativeConfig("skin.config.temp.optifine.file.new");
        if (Files.exists(old, new LinkOption[0])) {
            try {
                this.log("clear old library");
                Files.delete(Paths.get(FileUtil.readFile(old.toFile()), new String[0]));
                Files.delete(old);
            }
            catch (IOException exception) {
                this.log(exception);
            }
            catch (InvalidPathException ex) {
                try {
                    Files.delete(old);
                }
                catch (IOException e) {
                    this.log(e);
                }
            }
        }
        this.cleanMods(mods2);
        this.printModsFiles("after clearLibrary", mods1);
    }
    
    private void cleanMods(final Path mods) {
        if (Files.exists(mods, new LinkOption[0])) {
            final List<String> list = this.readMods();
            if (!list.isEmpty()) {
                final Iterator<String> it = list.iterator();
                while (it.hasNext()) {
                    final String file = it.next();
                    try {
                        Files.delete(Paths.get(file, new String[0]));
                        it.remove();
                    }
                    catch (NoSuchFileException | InvalidPathException ex2) {
                        final Exception ex;
                        final Exception e = ex;
                        it.remove();
                        this.log(e);
                    }
                    catch (IOException exception) {
                        this.log(exception.getMessage());
                    }
                }
            }
            this.writeStateMod(list);
        }
    }
    
    private void writeStateMod(final List<String> list) {
        try {
            final String result = this.gson.toJson(list, new TypeToken<ArrayList<String>>() {}.getType());
            FileUtil.writeFile(FileUtil.getRelativeConfigFile("skin.config.temp.optifine.file.new"), result);
            this.log("written: ", result);
        }
        catch (IOException e) {
            this.log(e);
        }
    }
    
    private void printModsFiles(final String state, final Path mods) {
        final File[] files = mods.toFile().listFiles(File::isFile);
        if (Objects.nonNull(files)) {
            this.log(state, files);
        }
    }
    
    private List<String> readMods() {
        final Path mods = FileUtil.getRelativeConfig("skin.config.temp.optifine.file.new");
        List<String> list = null;
        if (Files.exists(mods, new LinkOption[0])) {
            try {
                list = this.gson.fromJson(FileUtil.readFile(mods.toFile(), "utf-8"), new TypeToken<ArrayList<String>>() {}.getType());
            }
            catch (IOException | JsonSyntaxException ex2) {
                final Exception ex;
                final Exception e = ex;
                this.log(e);
            }
        }
        return (list == null) ? new ArrayList<String>() : list;
    }
}
