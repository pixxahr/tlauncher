// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.managers;

public interface ComponentManagerListener
{
    void onComponentsRefreshing(final ComponentManager p0);
    
    void onComponentsRefreshed(final ComponentManager p0);
}
