// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.modpack;

import java.nio.file.Path;
import java.util.List;
import net.minecraft.launcher.versions.CompleteVersion;

public interface PreparedMod
{
    List<Path> prepare(final CompleteVersion p0);
}
