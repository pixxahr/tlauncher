// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.modpack;

import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import com.github.junrar.rarfile.FileHeader;
import java.nio.file.Paths;
import com.github.junrar.exception.RarException;
import java.util.Collection;
import com.github.junrar.Archive;
import org.apache.commons.io.FilenameUtils;
import com.github.junrar.extract.ExtractArchive;
import java.io.File;
import java.io.IOException;
import org.tlauncher.util.U;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import java.util.ArrayList;
import java.nio.file.Path;
import java.util.List;
import net.minecraft.launcher.versions.CompleteVersion;

public class PreparedModIMpl implements PreparedMod
{
    @Override
    public List<Path> prepare(final CompleteVersion completeVersion) {
        return new ArrayList<Path>();
    }
    
    private void prepareModeFolders(final Path source, final Path dest, final List<Path> paths) {
        final File[] files = source.toFile().listFiles();
        for (int i = 0; i < files.length; ++i) {
            paths.add(files[i].toPath());
        }
        try {
            Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
        }
        catch (IOException e) {
            U.log(e);
        }
    }
    
    private void prepareGameFolders(final Path source, final Path dest, final List<Path> paths) {
        final File[] files = source.toFile().listFiles();
        final ExtractArchive extractArchive = new ExtractArchive();
        int i = 0;
        while (i < files.length) {
            final String extension;
            final String ext = extension = FilenameUtils.getExtension(files[i].toString());
            switch (extension) {
                case "rar": {
                    try {
                        paths.addAll(this.topFolders(new Archive(files[i])));
                    }
                    catch (IOException | RarException ex2) {
                        final Exception ex;
                        final Exception e = ex;
                        U.log(e);
                    }
                    extractArchive.extractArchive(files[i], dest.toFile());
                }
                case "zip": {}
                default: {
                    U.log("don't find extension");
                    ++i;
                    continue;
                }
            }
        }
    }
    
    private List<Path> topFolders(final Archive rar) throws IOException, RarException {
        FileHeader fh = rar.nextFileHeader();
        final List<Path> list = new ArrayList<Path>();
        while (fh != null) {
            if (fh.isDirectory()) {
                list.add(Paths.get(fh.getFileNameString(), new String[0]));
            }
            fh = rar.nextFileHeader();
        }
        return list;
    }
    
    private List<Path> topFolders(final ZipFile zipFile) {
        final List<Path> list = new ArrayList<Path>();
        final Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            final ZipEntry entry = (ZipEntry)entries.nextElement();
            if (entry.isDirectory()) {
                final File file = new File(entry.getName());
                if (file.getParent() != null) {
                    continue;
                }
                list.add(Paths.get(file.getName(), new String[0]));
            }
        }
        return list;
    }
}
