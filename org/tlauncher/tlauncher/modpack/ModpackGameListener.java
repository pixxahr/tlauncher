// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.modpack;

import java.nio.file.Path;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import org.tlauncher.util.FileUtil;
import net.minecraft.launcher.versions.Version;
import org.tlauncher.tlauncher.minecraft.crash.Crash;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.rmo.TLauncher;
import com.google.inject.Inject;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListener;

public class ModpackGameListener implements MinecraftListener
{
    @Inject
    private ModpackManager modpackManager;
    @Inject
    private TLauncher tLauncher;
    
    @Override
    public void onMinecraftPrepare() {
    }
    
    @Override
    public void onMinecraftAbort() {
    }
    
    @Override
    public void onMinecraftLaunch() {
        U.log("copied to modpack servers.dat");
        this.copy(true);
    }
    
    @Override
    public void onMinecraftClose() {
        U.log("copy from modpack servers.dat ");
        this.copy(false);
    }
    
    @Override
    public void onMinecraftError(final Throwable e) {
    }
    
    @Override
    public void onMinecraftKnownError(final MinecraftException e) {
    }
    
    @Override
    public void onMinecraftCrash(final Crash crash) {
    }
    
    private void copy(final boolean toModpack) {
        final String servers = "servers.dat";
        final Path modpackServers = ModpackUtil.getPathByVersion(this.tLauncher.getLauncher().getVersion(), servers);
        final Path baseServers = FileUtil.getRelative(servers);
        try {
            if (toModpack) {
                Files.copy(baseServers, modpackServers, StandardCopyOption.REPLACE_EXISTING);
            }
            else {
                Files.copy(modpackServers, baseServers, StandardCopyOption.REPLACE_EXISTING);
            }
        }
        catch (IOException e) {
            U.log(e);
        }
    }
}
