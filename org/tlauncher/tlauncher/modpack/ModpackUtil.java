// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.modpack;

import org.tlauncher.modpack.domain.client.ModDTO;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.util.Random;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import java.util.HashSet;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import java.util.Iterator;
import org.tlauncher.modpack.domain.client.share.DependencyType;
import org.tlauncher.modpack.domain.client.GameEntityDependencyDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import java.util.Comparator;
import org.tlauncher.modpack.domain.client.share.ForgeStringComparator;
import com.google.common.collect.Lists;
import java.util.Set;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.util.FileUtil;
import java.nio.file.Path;
import net.minecraft.launcher.versions.Version;
import java.util.Collection;
import java.util.ArrayList;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import java.util.List;

public class ModpackUtil
{
    public static List<? extends VersionDTO> sortByDate(final List<VersionDTO> versions) {
        final List<VersionDTO> sortList = new ArrayList<VersionDTO>(versions);
        sortList.sort((o1, o2) -> o2.getUpdateDate().compareTo(o1.getUpdateDate()));
        return sortList;
    }
    
    public static Path getPathByVersion(final Version version, final String... paths) {
        final StringBuilder builder = new StringBuilder();
        builder.append("versions").append("/").append(version.getID());
        for (final String line : paths) {
            builder.append("/").append(line);
        }
        return FileUtil.getRelative(builder.toString());
    }
    
    public static Path getPathByVersion(final CompleteVersion version) {
        return getPathByVersion(version, "");
    }
    
    public static String getLatestGameVersion(final Set<String> c) {
        if (c.isEmpty()) {
            return null;
        }
        final ArrayList<String> list = (ArrayList<String>)Lists.newArrayList((Iterable)c);
        list.sort(new ForgeStringComparator());
        return list.get(0);
    }
    
    public static void extractIncompatible(final GameEntityDTO en, final Set<Long> set) {
        if (en.getDependencies() == null) {
            return;
        }
        for (final GameEntityDependencyDTO d : en.getDependencies()) {
            if (d.getDependencyType() == DependencyType.INCOMPATIBLE) {
                set.add(d.getId());
            }
        }
    }
    
    public static Set<Long> getAllModpackIds(final ModpackDTO m) {
        final Set<Long> set = new HashSet<Long>();
        if (m.getId() != null) {
            set.add(m.getId());
        }
        for (final GameType t : GameType.getSubEntities()) {
            for (final GameEntityDTO en : ((ModpackVersionDTO)m.getVersion()).getByType(t)) {
                if (en.getId() != null) {
                    set.add(en.getId());
                }
            }
        }
        return set;
    }
    
    public static String getPictureURL(final Integer id, final String type) {
        return TLauncher.getInnerSettings().getArray("modpack.repo")[new Random().nextInt(2)] + "/pictures/" + id + type + ".png";
    }
    
    public static StringBuilder buildMessage(final List<GameEntityDTO> list) {
        final StringBuilder b = new StringBuilder();
        for (final GameEntityDTO dependency : list) {
            b.append(dependency.getName()).append("(").append(Localizable.get("modpack.button." + GameType.create(dependency.getClass()))).append(")").append(" ");
        }
        return b;
    }
    
    public static Path getPath(final CompleteVersion v, final GameType type) {
        switch (type) {
            case RESOURCEPACK: {
                return getPathByVersion(v, "resourcepacks");
            }
            case MOD: {
                return getPathByVersion(v, "mods");
            }
            case MAP: {
                return getPathByVersion(v, "saves");
            }
            default: {
                throw new RuntimeException("not proper type");
            }
        }
    }
    
    public static boolean useSkinMod(final CompleteVersion version) {
        for (final ModDTO m : ((ModpackVersionDTO)version.getModpack().getVersion()).getMods()) {
            if (ModDTO.SKIN_MODS.contains(m.getId())) {
                return true;
            }
        }
        return false;
    }
}
