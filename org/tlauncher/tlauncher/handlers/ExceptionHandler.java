// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.handlers;

import org.tlauncher.util.TlauncherUtil;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.Reflect;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler
{
    private static ExceptionHandler instance;
    private static long gcLastCall;
    
    public static ExceptionHandler getInstance() {
        if (ExceptionHandler.instance == null) {
            ExceptionHandler.instance = new ExceptionHandler();
        }
        return ExceptionHandler.instance;
    }
    
    private ExceptionHandler() {
    }
    
    @Override
    public void uncaughtException(final Thread t, final Throwable e) {
        final OutOfMemoryError asOOM = Reflect.cast(e, OutOfMemoryError.class);
        if (asOOM != null && reduceMemory(asOOM)) {
            return;
        }
        if (scanTrace(e)) {
            try {
                Alert.showError("Exception in thread " + t.getName(), e);
            }
            catch (Exception w) {
                w.printStackTrace();
            }
        }
        else {
            U.log("Hidden exception in thread " + t.getName(), e);
        }
        if (e instanceof UnsatisfiedLinkError && e.getMessage().contains("jfxwebkit.dll")) {
            U.log("should use new jvm");
            TLauncher.getInstance().getConfiguration().set("not.work.jfxwebkit.dll", true);
        }
        TlauncherUtil.sendLog(e);
    }
    
    public static boolean reduceMemory(final OutOfMemoryError e) {
        if (e == null) {
            return false;
        }
        U.log("OutOfMemory error has occurred, solving...");
        final long currentTime = System.currentTimeMillis();
        final long diff = Math.abs(currentTime - ExceptionHandler.gcLastCall);
        if (diff > 5000L) {
            ExceptionHandler.gcLastCall = currentTime;
            U.gc();
            return true;
        }
        U.log("GC is unable to reduce memory usage");
        return false;
    }
    
    private static boolean scanTrace(final Throwable e) {
        final StackTraceElement[] stackTrace;
        final StackTraceElement[] elements = stackTrace = e.getStackTrace();
        for (final StackTraceElement element : stackTrace) {
            if (element.getClassName().startsWith("org.tlauncher")) {
                return true;
            }
        }
        return false;
    }
}
