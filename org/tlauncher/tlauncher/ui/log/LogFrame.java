// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.log;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.HashMap;
import org.tlauncher.tlauncher.configuration.InnerConfiguration;
import org.tlauncher.util.U;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import net.minecraft.launcher.Http;
import com.google.common.collect.Maps;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.JOptionPane;
import org.tlauncher.tlauncher.ui.util.ValidateUtil;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JFrame;

public class LogFrame extends JFrame
{
    private final JFrame parent;
    private JPanel contentPane;
    private JTextField emailField;
    
    public LogFrame(final JFrame frame, final Throwable errorMessage) {
        if (frame == null) {
            this.parent = new JFrame();
        }
        else {
            this.parent = frame;
        }
        this.setBounds(100, 100, 400, 366);
        (this.contentPane = new JPanel()).setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setContentPane(this.contentPane);
        this.contentPane.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setTitle(Localizable.get("log.form.title"));
        SwingUtil.setFavicons(this);
        final JPanel panel = new JPanel();
        panel.setBounds(0, 11, 400, 316);
        this.contentPane.add(panel);
        panel.setLayout(null);
        final JLabel lblEmail = new JLabel(Localizable.get("check.email.name"));
        lblEmail.setBounds(10, 10, 334, 15);
        panel.add(lblEmail);
        (this.emailField = new JTextField()).setBounds(10, 25, 207, 20);
        panel.add(this.emailField);
        this.emailField.setColumns(10);
        final JLabel descriptionErrorLabel = new JLabel(Localizable.get("log.email.error.description"));
        descriptionErrorLabel.setBounds(10, 55, 334, 15);
        panel.add(descriptionErrorLabel);
        final JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(31);
        scrollPane.setBounds(10, 70, 380, 80);
        panel.add(scrollPane);
        final JTextArea descriptionErrorArea = new JTextArea();
        descriptionErrorArea.setLineWrap(true);
        descriptionErrorArea.setRows(5);
        descriptionErrorArea.setColumns(10);
        scrollPane.setViewportView(descriptionErrorArea);
        final JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setHorizontalScrollBarPolicy(31);
        scrollPane_1.setViewportBorder(UIManager.getBorder("TextPane.border"));
        scrollPane_1.setBounds(10, 175, 380, 96);
        panel.add(scrollPane_1);
        final JTextArea outputErrorArea = new JTextArea();
        outputErrorArea.setColumns(10);
        outputErrorArea.setRows(5);
        outputErrorArea.setLineWrap(true);
        scrollPane_1.setViewportView(outputErrorArea);
        outputErrorArea.setText(errorMessage.getClass().getName() + ": " + errorMessage.getMessage());
        outputErrorArea.setEditable(false);
        outputErrorArea.setEnabled(true);
        final JButton btnNewButton = new JButton(Localizable.get("log.form.send"));
        btnNewButton.setBounds(61, 282, 134, 23);
        panel.add(btnNewButton);
        final JLabel outputLabelError = new JLabel(Localizable.get("log.email.error.issue"));
        outputLabelError.setBounds(10, 161, 334, 14);
        panel.add(outputLabelError);
        final JButton noSendButton = new JButton(Localizable.get("log.form.send.no"));
        noSendButton.setBounds(205, 282, 128, 23);
        panel.add(noSendButton);
        this.parent.setEnabled(false);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                LogFrame.this.releaseLog(LogFrame.this.parent);
            }
        });
        InnerConfiguration s;
        HashMap query;
        URL url;
        StringWriter stringWriter;
        btnNewButton.addActionListener(e -> {
            if (!this.emailField.getText().isEmpty() && !ValidateUtil.validateEmail(this.emailField.getText())) {
                JOptionPane.showMessageDialog(this, "check.email.input");
                return;
            }
            else {
                this.releaseLog(this.parent);
                this.setVisible(false);
                this.parent.setEnabled(true);
                s = TLauncher.getInnerSettings();
                try {
                    query = Maps.newHashMap();
                    query.put("version", TLauncher.getVersion());
                    query.put("clientType", s.get("type"));
                    url = Http.constantURL(Http.get(TLauncher.getInnerSettings().get("log.system"), query));
                    Http.performPost(url, TLauncher.getConsole().getOutput().getBytes(StandardCharsets.UTF_8), "text/plain", true);
                    Alert.showMonologError(Localizable.get().get("alert.error.send.log.success"), 1);
                }
                catch (Throwable ex) {
                    stringWriter = new StringWriter();
                    ex.printStackTrace(new PrintWriter(stringWriter));
                    U.log(stringWriter.toString());
                    Alert.showMonologError(Localizable.get().get("alert.error.send.log.unsuccess"), 0);
                }
                this.dispose();
                return;
            }
        });
        noSendButton.addActionListener(e -> this.releaseLog(this.parent));
    }
    
    private void releaseLog(final JFrame parent) {
        parent.setEnabled(true);
        this.dispose();
    }
}
