// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import net.minecraft.launcher.versions.Version;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.ui.alert.Alert;
import net.minecraft.launcher.versions.CompleteVersion;
import java.awt.event.ItemListener;
import org.tlauncher.tlauncher.ui.swing.CheckBoxListener;
import java.awt.Font;
import org.tlauncher.util.OS;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityListener;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.tlauncher.ui.loc.LocalizableCheckbox;
import org.tlauncher.tlauncher.ui.block.BlockablePanel;

public class VersionPanel extends BlockablePanel implements LoginForm.LoginProcessListener
{
    public final VersionComboBox version;
    public final LocalizableCheckbox forceupdate;
    public final LoginForm loginForm;
    private boolean state;
    
    public VersionPanel(final LoginForm lf) {
        this.loginForm = lf;
        this.version = new VersionComboBox(lf);
        ((ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class)).addGameListener(GameType.MODPACK, this.version);
        ((ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class)).addGameListener(GameType.MOD, this.version);
        final SpringLayout springLayout = new SpringLayout();
        this.setLayout(springLayout);
        springLayout.putConstraint("North", this.version, 11, "North", this);
        springLayout.putConstraint("West", this.version, 0, "West", this);
        springLayout.putConstraint("South", this.version, 35, "North", this);
        springLayout.putConstraint("East", this.version, 0, "East", this);
        this.add(this.version);
        (this.forceupdate = new LocalizableCheckbox("loginform.checkbox.forceupdate")).setForeground(Color.WHITE);
        this.forceupdate.setIconTextGap(14);
        this.forceupdate.setFocusPainted(false);
        springLayout.putConstraint("North", this.forceupdate, 44, "North", this);
        springLayout.putConstraint("South", this.forceupdate, -11, "South", this);
        springLayout.putConstraint("West", this.forceupdate, -4, "West", this);
        springLayout.putConstraint("East", this.forceupdate, 0, "East", this);
        this.add(this.forceupdate);
        if (!OS.is(OS.WINDOWS)) {
            final Font f = this.version.getFont();
            this.forceupdate.setFont(new Font(f.getFamily(), f.getStyle(), 11));
        }
        this.forceupdate.addItemListener(new CheckBoxListener() {
            @Override
            public void itemStateChanged(final boolean newstate) {
                VersionPanel.this.state = newstate;
                VersionPanel.this.loginForm.play.updateState();
            }
        });
    }
    
    @Override
    public void logginingIn() throws LoginException {
        final VersionSyncInfo syncInfo = this.loginForm.versions.getVersion();
        if (syncInfo == null) {
            return;
        }
        boolean supporting = syncInfo.hasRemote();
        final boolean installed = syncInfo.isInstalled();
        final Version version = syncInfo.getLocal();
        if (version instanceof CompleteVersion && ((CompleteVersion)version).isModpack()) {
            supporting = true;
        }
        if (this.state) {
            if (!supporting) {
                Alert.showLocError("forceupdate.local");
                throw new LoginException("Cannot update local version!");
            }
            if (installed && !Alert.showLocQuestion("forceupdate.question")) {
                throw new LoginException("User has cancelled force updating.");
            }
        }
    }
    
    @Override
    public void loginFailed() {
    }
    
    @Override
    public void loginSucceed() {
    }
}
