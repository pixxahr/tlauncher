// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

public class LoginWaitException extends LoginException
{
    private final LoginWaitTask waitTask;
    
    public LoginWaitException(final String reason, final LoginWaitTask waitTask) {
        super(reason);
        if (waitTask == null) {
            throw new NullPointerException("wait task");
        }
        this.waitTask = waitTask;
    }
    
    public LoginWaitTask getWaitTask() {
        return this.waitTask;
    }
    
    public interface LoginWaitTask
    {
        void runTask() throws LoginException;
    }
}
