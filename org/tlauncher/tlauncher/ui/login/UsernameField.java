// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.loc.LocalizableTextField;

public class UsernameField extends LocalizableTextField implements Blockable, LoginForm.LoginProcessListener
{
    public UsernameField(final String login) {
        super("account.username");
        this.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(final FocusEvent e) {
                UsernameField.this.setBackground(UsernameField.this.getTheme().getBackground());
            }
            
            @Override
            public void focusLost(final FocusEvent e) {
            }
        });
        this.setValue(login);
    }
    
    public String getUsername() {
        return this.getValue();
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
    
    @Override
    public void logginingIn() throws LoginException {
        if (this.getUsername() != null) {
            return;
        }
        this.setBackground(this.getTheme().getFailure());
        Alert.showLocError("auth.error.nousername");
        throw new LoginException("Invalid user name!");
    }
    
    @Override
    public void loginFailed() {
    }
    
    @Override
    public void loginSucceed() {
    }
}
