// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import org.tlauncher.tlauncher.ui.center.CenterPanelTheme;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import org.tlauncher.tlauncher.ui.center.CenterPanel;
import java.awt.Point;
import java.awt.Dimension;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.tlauncher.ui.swing.ResizeableComponent;
import java.awt.Component;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JComponent;
import java.awt.Insets;
import org.tlauncher.tlauncher.ui.center.LoginHelperTheme;
import org.tlauncher.tlauncher.ui.scenes.DefaultScene;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedLayeredPane;

public class LoginFormHelper extends ExtendedLayeredPane implements LocalizableComponent
{
    private static final int EDGE = 100;
    private final DefaultScene defaultScene;
    private final LoginForm loginForm;
    private final LoginFormHelperTip[] tips;
    private volatile LoginFormHelperState state;
    private static final LoginHelperTheme loginHelperTheme;
    private static final Insets loginHelperInsets;
    
    public LoginFormHelper(final DefaultScene scene) {
        this.defaultScene = scene;
        this.loginForm = scene.loginForm;
        this.tips = new LoginFormHelperTip[] { new LoginFormHelperTip("username", this.loginForm.accountPanel, Position.UP), new LoginFormHelperTip("versions", this.loginForm.versionPanel, Position.UP), new LoginFormHelperTip("play", this.loginForm.playPanel, Position.UP) };
        for (final LoginFormHelperTip tip : this.tips) {
            tip.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                    LoginFormHelper.this.setState(LoginFormHelperState.NONE);
                }
            });
        }
        this.add((Component[])this.tips);
        this.setState(LoginFormHelperState.NONE);
        final MouseAdapter adapter = new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                LoginFormHelper.this.setState(LoginFormHelperState.NONE);
            }
        };
        this.loginForm.play.addMouseListener(adapter);
        this.loginForm.buttons.refresh.addMouseListener(adapter);
        this.loginForm.buttons.folder.addMouseListener(adapter);
        this.loginForm.buttons.settings.addMouseListener(adapter);
    }
    
    public LoginFormHelperState getState() {
        return this.state;
    }
    
    public void setState(final LoginFormHelperState state) {
        if (state == null) {
            throw new NullPointerException();
        }
        this.state = state;
        this.updateTips();
        this.setVisible(state != LoginFormHelperState.NONE);
    }
    
    @Override
    public void onResize() {
        this.setSize(this.loginForm.getWidth(), 100);
        this.setLocation(this.loginForm.getX(), this.loginForm.getY() - 100);
        for (final Component comp : this.getComponents()) {
            if (comp instanceof ResizeableComponent) {
                ((ResizeableComponent)comp).onResize();
            }
        }
        this.updateTips();
    }
    
    protected void updateTips() {
        for (final LoginFormHelperTip tip : this.tips) {
            tip.setVisible(false);
            if (this.state != LoginFormHelperState.NONE) {
                tip.label.setText("loginform.helper." + tip.name);
                tip.label.setVerticalAlignment(0);
                tip.label.setHorizontalAlignment(0);
                final Dimension dim = tip.getPreferredSize();
                final Point pP = SwingUtil.getRelativeLocation(this.loginForm, tip.component);
                final int x = pP.x - dim.width + tip.component.getPreferredSize().width;
                final int y = 100 - dim.height;
                tip.setBounds(x, y, dim.width, dim.height);
                tip.setVisible(true);
            }
        }
    }
    
    @Override
    public void updateLocale() {
        this.updateTips();
    }
    
    static {
        loginHelperTheme = new LoginHelperTheme();
        loginHelperInsets = new Insets(6, 10, 6, 10);
    }
    
    public enum LoginFormHelperState
    {
        NONE, 
        SHOWN;
    }
    
    public enum Position
    {
        UP, 
        DOWN;
    }
    
    private class LoginFormHelperTip extends CenterPanel
    {
        private static final int TRIANGGLE_WIDTH = 10;
        final String name;
        final JComponent component;
        final LocalizableLabel label;
        
        LoginFormHelperTip(final String name, final JComponent comp, final Position pos) {
            super(LoginFormHelper.loginHelperTheme, LoginFormHelperTip.noInsets);
            this.setLayout(new BorderLayout(0, 0));
            if (name == null) {
                throw new NullPointerException("Name is NULL!");
            }
            if (name.isEmpty()) {
                throw new IllegalArgumentException("Name is empty!");
            }
            if (comp == null) {
                throw new NullPointerException("Component is NULL!");
            }
            if (pos == null) {
                throw new NullPointerException("Position is NULL!");
            }
            this.name = name.toLowerCase();
            this.component = comp;
            final ExtendedPanel p = new ExtendedPanel(new BorderLayout(0, 0));
            p.setInsets(LoginFormHelper.loginHelperInsets);
            p.setOpaque(true);
            p.setBackground(LoginFormHelper.loginHelperTheme.getBackground());
            (this.label = new LocalizableLabel()).setFont(new Font(this.label.getFont().getFamily(), 0, 12));
            this.label.setForeground(LoginFormHelper.loginHelperTheme.getForeground());
            p.add(this.label, "Center");
            this.add(p, "Center");
            this.add(new TipTriangle(10, LoginFormHelper.loginHelperTheme.getBorder(), BORDER_POS.UP), "South");
        }
    }
    
    class TipTriangle extends ExtendedPanel
    {
        public static final int DEFAULT_WIDTH = 10;
        private int width;
        private int gap;
        private final BORDER_POS pos;
        private Color triangleColor;
        
        public TipTriangle(final int width, final Color triangle, final BORDER_POS pos) {
            this.gap = 24;
            this.width = width;
            this.pos = pos;
            this.triangleColor = triangle;
        }
        
        @Override
        protected void paintComponent(final Graphics g) {
            final Graphics2D g2 = (Graphics2D)g;
            g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
            g2.setColor(this.triangleColor);
            final Rectangle rec = this.getVisibleRect();
            int[] xT = new int[0];
            int[] yT = new int[0];
            final int startX = rec.x + rec.width + -this.gap;
            final int startY = rec.y;
            xT = new int[] { startX, startX - this.width, startX - this.width * 2 };
            yT = new int[] { startY, startY + this.width, startY };
            g2.fillPolygon(xT, yT, 3);
            g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_DEFAULT);
        }
    }
    
    public enum BORDER_POS
    {
        UP, 
        BOTTOM, 
        LEFT, 
        RIGHT;
    }
}
