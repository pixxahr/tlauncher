// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import javax.swing.JButton;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.tlauncher.ui.swing.extended.VPanel;

public class InputPanel extends VPanel
{
    private static final long serialVersionUID = -4418780541141471184L;
    public final LoginForm loginForm;
    public final UsernameField username;
    public final VersionComboBox version;
    public final CheckBoxPanel checkbox;
    public final AccountComboBox accountComboBox;
    private Account.AccountType type;
    
    public InputPanel(final LoginForm lf) {
        final boolean chooser = TLauncher.getInstance().getConfiguration().getBoolean("chooser.type.account");
        this.loginForm = lf;
        this.username = new UsernameField("test");
        this.accountComboBox = new AccountComboBox(lf);
        if (chooser) {
            this.add(this.accountComboBox, Box.createRigidArea(new Dimension(1, 3)));
            this.type = Account.AccountType.MOJANG;
            lf.addLoginProcessListener(this.accountComboBox);
        }
        else {
            this.add(this.username, Box.createRigidArea(new Dimension(1, 3)));
            this.type = Account.AccountType.FREE;
            lf.addLoginProcessListener(this.username);
        }
        this.add(this.version = new VersionComboBox(lf));
        this.add(this.checkbox = new CheckBoxPanel(lf, this, chooser));
        this.add(new JButton("test"));
    }
    
    public void changeTypeAccount(final Account.AccountType type) {
        if (type.equals(Account.AccountType.SPECIAL)) {
            if (this.contains(this.username)) {
                this.remove(this.username);
            }
            this.add(this.accountComboBox, 0);
            this.loginForm.addLoginProcessListener(this.accountComboBox);
            this.loginForm.removeLoginProcessListener(this.username);
            this.type = Account.AccountType.SPECIAL;
        }
        else {
            if (this.contains(this.accountComboBox)) {
                this.remove(this.accountComboBox);
            }
            this.add(this.username, 0);
            this.loginForm.addLoginProcessListener(this.username);
            this.loginForm.removeLoginProcessListener(this.accountComboBox);
            this.type = Account.AccountType.FREE;
        }
        this.revalidate();
        this.repaint();
    }
    
    public String getUsername() {
        if (this.type.equals(Account.AccountType.FREE)) {
            return this.username.getUsername();
        }
        return ((Account)this.accountComboBox.getSelectedItem()).getUsername();
    }
    
    public Account.AccountType getTypeAccountShow() {
        return this.type;
    }
}
