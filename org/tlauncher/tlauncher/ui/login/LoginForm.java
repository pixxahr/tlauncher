// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import org.tlauncher.util.async.LoopedThread;
import org.tlauncher.tlauncher.minecraft.crash.Crash;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import java.io.IOException;
import org.tlauncher.tlauncher.minecraft.auth.Authenticator;
import org.tlauncher.tlauncher.managers.VersionManager;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.downloader.Downloader;
import org.tlauncher.tlauncher.ui.alert.Alert;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.entity.server.SiteServer;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.util.U;
import java.util.Iterator;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.managers.popup.menu.HotServerManager;
import javax.swing.BorderFactory;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.Box;
import java.awt.LayoutManager;
import java.awt.FlowLayout;
import java.awt.Color;
import java.util.Collections;
import java.util.ArrayList;
import java.awt.Dimension;
import org.tlauncher.tlauncher.entity.server.RemoteServer;
import org.tlauncher.tlauncher.ui.login.buttons.ButtonPanel;
import org.tlauncher.tlauncher.ui.block.BlockablePanel;
import org.tlauncher.tlauncher.ui.login.buttons.PlayButton;
import org.tlauncher.tlauncher.ui.settings.SettingsPanel;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.scenes.DefaultScene;
import java.util.List;
import org.tlauncher.tlauncher.downloader.DownloaderListener;
import org.tlauncher.tlauncher.managers.VersionManagerListener;
import org.tlauncher.tlauncher.minecraft.auth.AuthenticatorListener;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListener;
import org.tlauncher.tlauncher.ui.center.CenterPanel;

public class LoginForm extends CenterPanel implements MinecraftListener, AuthenticatorListener, VersionManagerListener, DownloaderListener
{
    private final List<LoginStateListener> stateListeners;
    private final List<LoginProcessListener> processListeners;
    public final DefaultScene scene;
    public final MainPane pane;
    private final SettingsPanel settings;
    public final VersionComboBox versions;
    public final PlayButton play;
    public final BlockablePanel playPanel;
    public final ButtonPanel buttons;
    public final AccountComboBox accountComboBox;
    public final VersionPanel versionPanel;
    private final StartThread startThread;
    private final StopThread stopThread;
    private LoginState state;
    private RemoteServer server;
    public final AccountPanel accountPanel;
    public static final Dimension LOGIN_SIZE;
    public static final String LOGIN_BLOCK = "login";
    public static final String REFRESH_BLOCK = "refresh";
    public static final String LAUNCH_BLOCK = "launch";
    public static final String AUTH_BLOCK = "auth";
    public static final String UPDATER_BLOCK = "update";
    public static final String DOWNLOADER_BLOCK = "download";
    
    public LoginForm(final DefaultScene scene) {
        super(LoginForm.noInsets);
        this.stateListeners = Collections.synchronizedList(new ArrayList<LoginStateListener>());
        this.processListeners = Collections.synchronizedList(new ArrayList<LoginProcessListener>());
        this.state = LoginState.STOPPED;
        this.setSize(LoginForm.LOGIN_SIZE);
        this.setMaximumSize(LoginForm.LOGIN_SIZE);
        this.setOpaque(true);
        this.setBackground(new Color(113, 169, 76));
        this.scene = scene;
        this.pane = scene.getMainPane();
        this.settings = scene.settingsForm;
        this.startThread = new StartThread();
        this.stopThread = new StopThread();
        this.setLayout(new FlowLayout(0, 0, 0));
        this.add(Box.createHorizontalStrut(19));
        this.play = new PlayButton(this);
        this.accountPanel = new AccountPanel(this, this.global.getBoolean("chooser.type.account"));
        this.buttons = new ButtonPanel(this);
        this.playPanel = new BlockablePanel();
        this.versionPanel = new VersionPanel(this);
        this.accountPanel.setPreferredSize(new Dimension(246, 70));
        this.add(this.accountPanel);
        this.add(Box.createHorizontalStrut(20));
        this.versionPanel.setPreferredSize(new Dimension(246, 70));
        this.add(this.versionPanel);
        this.add(Box.createHorizontalStrut(20));
        this.playPanel.setLayout(new BorderLayout(0, 0));
        this.playPanel.setPreferredSize(new Dimension(240, 70));
        this.playPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 8, 0));
        this.playPanel.add(this.play);
        this.add(this.playPanel);
        this.add(Box.createHorizontalStrut(19));
        this.buttons.setPreferredSize(new Dimension(240, 70));
        this.add(this.buttons);
        this.versions = this.versionPanel.version;
        this.accountComboBox = this.accountPanel.accountComboBox;
        this.processListeners.add(this.settings);
        this.processListeners.add(this.versionPanel);
        this.processListeners.add(this.versions);
        this.stateListeners.add(this.play);
        this.tlauncher.getVersionManager().addListener(this);
        this.tlauncher.getDownloader().addListener(this);
    }
    
    private void runProcess() {
        LoginException error = null;
        boolean success = true;
        synchronized (this.processListeners) {
            for (final LoginProcessListener listener : this.processListeners) {
                try {
                    listener.logginingIn();
                }
                catch (LoginWaitException wait) {
                    this.log("Catched a wait task from this listener, waiting...");
                    try {
                        wait.getWaitTask().runTask();
                    }
                    catch (LoginException waitError) {
                        this.log("Catched an error on a wait task.");
                        error = waitError;
                    }
                }
                catch (LoginException loginError) {
                    this.log("Catched an error on a listener");
                    error = loginError;
                }
                if (error == null) {
                    continue;
                }
                this.log(error);
                success = false;
                break;
            }
            if (success) {
                for (final LoginProcessListener listener : this.processListeners) {
                    listener.loginSucceed();
                }
            }
            else {
                ((HotServerManager)TLauncher.getInjector().getInstance((Class)HotServerManager.class)).enablePopup();
                for (final LoginProcessListener listener : this.processListeners) {
                    listener.loginFailed();
                }
            }
        }
        if (error != null) {
            this.log("Login process has ended with an error.");
            return;
        }
        if (this.accountPanel.getTypeAccountShow() == Account.AccountType.FREE) {
            this.global.setForcefully("running.account.type", Account.AccountType.FREE);
            this.global.setForcefully("login.account", this.accountPanel.getUsername(), false);
        }
        else {
            this.global.setForcefully("running.account.type", this.accountComboBox.getSelectedValue().getType());
            this.global.setForcefully("running.account.username", this.accountComboBox.getSelectedValue().getUsername(), false);
        }
        this.global.setForcefully("login.version.game", this.versions.getVersion().getID(), false);
        this.global.store();
        this.log("Login was OK. Trying to launch now.");
        final boolean force = this.versionPanel.forceupdate.isSelected();
        this.changeState(LoginState.LAUNCHING);
        this.tlauncher.launch(this, this.server, force);
        this.server = null;
        this.versionPanel.forceupdate.setSelected(false);
    }
    
    private void stopProcess() {
        while (!this.tlauncher.isLauncherWorking()) {
            this.log("waiting for launcher");
            U.sleepFor(500L);
        }
        this.changeState(LoginState.STOPPING);
        this.tlauncher.getLauncher().stop();
    }
    
    public void startLauncher() {
        if (Blocker.isBlocked(this)) {
            return;
        }
        this.startThread.iterate();
    }
    
    public void startLauncher(final RemoteServer server) {
        this.server = server;
        this.startLauncher();
    }
    
    public void startLauncher(final SiteServer server) {
        if (server == null) {
            this.log("version is null");
            return;
        }
        boolean find = false;
        for (int i = 0; i < this.versions.getModel().getSize(); ++i) {
            if (this.versions.getModel().getElementAt(i).getID().equals(server.getVersion())) {
                this.versions.setSelectedValue(this.versions.getModel().getElementAt(i));
                find = true;
                break;
            }
        }
        if (!find) {
            Alert.showLocMessage("version.do.not.find");
            return;
        }
        this.server = server;
        this.startLauncher();
    }
    
    public void stopLauncher() {
        this.stopThread.iterate();
    }
    
    private void changeState(final LoginState state) {
        if (state == null) {
            throw new NullPointerException();
        }
        if (this.state == state) {
            return;
        }
        this.state = state;
        for (final LoginStateListener listener : this.stateListeners) {
            listener.loginStateChanged(state);
        }
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.block(reason, this.settings, this.play, this.versionPanel, this.accountPanel);
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblock(reason, this.settings, this.play, this.versionPanel, this.accountPanel);
    }
    
    @Override
    public void onDownloaderStart(final Downloader d, final int files) {
        Blocker.block(this, "download");
        if (this.play.getState() == PlayButton.PlayButtonState.CANCEL) {
            this.play.unblock("downloading finished");
        }
    }
    
    @Override
    public void onDownloaderAbort(final Downloader d) {
        Blocker.unblock(this, "download");
    }
    
    @Override
    public void onDownloaderProgress(final Downloader d, final double progress, final double speed) {
    }
    
    @Override
    public void onDownloaderFileComplete(final Downloader d, final Downloadable file) {
    }
    
    @Override
    public void onDownloaderComplete(final Downloader d) {
        Blocker.unblock(this, "download");
    }
    
    @Override
    public void onVersionsRefreshing(final VersionManager manager) {
        Blocker.block(this, "refresh");
    }
    
    @Override
    public void onVersionsRefreshingFailed(final VersionManager manager) {
        Blocker.unblock(this, "refresh");
    }
    
    @Override
    public void onVersionsRefreshed(final VersionManager manager) {
        Blocker.unblock(this, "refresh");
    }
    
    @Override
    public void onAuthPassing(final Authenticator auth) {
        Blocker.block(this, "auth");
    }
    
    @Override
    public void onAuthPassingError(final Authenticator auth, final Throwable e) {
        Blocker.unblock(this, "auth");
        final Throwable cause = e.getCause();
        if (cause != null && e.getCause() instanceof IOException) {
            return;
        }
        throw new LoginException("Cannot auth!");
    }
    
    @Override
    public void onAuthPassed(final Authenticator auth) {
        Blocker.unblock(this, "auth");
    }
    
    @Override
    public void onMinecraftPrepare() {
        Blocker.block(this, "launch");
    }
    
    @Override
    public void onMinecraftAbort() {
        Blocker.unblock(this, "launch");
        this.play.updateState();
    }
    
    @Override
    public void onMinecraftLaunch() {
        this.changeState(LoginState.LAUNCHED);
    }
    
    @Override
    public void onMinecraftClose() {
        Blocker.unblock(this, "launch");
        this.changeState(LoginState.STOPPED);
        this.tlauncher.getVersionManager().startRefresh(true);
    }
    
    @Override
    public void onMinecraftError(final Throwable e) {
        Blocker.unblock(this, "launch");
        this.changeState(LoginState.STOPPED);
    }
    
    @Override
    public void onMinecraftKnownError(final MinecraftException e) {
        Blocker.unblock(this, "launch");
        this.changeState(LoginState.STOPPED);
    }
    
    @Override
    public void onMinecraftCrash(final Crash crash) {
        Blocker.unblock(this, "launch");
        this.changeState(LoginState.STOPPED);
    }
    
    public void removeLoginProcessListener(final LoginProcessListener listener) {
        this.processListeners.remove(listener);
    }
    
    public void addLoginProcessListener(final LoginProcessListener listener) {
        this.processListeners.add(listener);
    }
    
    static {
        LOGIN_SIZE = new Dimension(1050, 70);
    }
    
    class StartThread extends LoopedThread
    {
        StartThread() {
            this.startAndWait();
        }
        
        @Override
        protected void iterateOnce() {
            try {
                LoginForm.this.runProcess();
            }
            catch (Throwable t) {
                Alert.showError(t);
            }
        }
    }
    
    class StopThread extends LoopedThread
    {
        StopThread() {
            this.startAndWait();
        }
        
        @Override
        protected void iterateOnce() {
            try {
                LoginForm.this.stopProcess();
            }
            catch (Throwable t) {
                Alert.showError(t);
            }
        }
    }
    
    public enum LoginState
    {
        LAUNCHING, 
        STOPPING, 
        STOPPED, 
        LAUNCHED;
    }
    
    public abstract static class LoginListener implements LoginProcessListener
    {
        @Override
        public abstract void logginingIn() throws LoginException;
        
        @Override
        public void loginFailed() {
        }
        
        @Override
        public void loginSucceed() {
        }
    }
    
    class LoginAbortedException extends Exception
    {
    }
    
    public interface LoginProcessListener
    {
        void logginingIn() throws LoginException;
        
        void loginFailed();
        
        void loginSucceed();
    }
    
    public interface LoginStateListener
    {
        void loginStateChanged(final LoginState p0);
    }
}
