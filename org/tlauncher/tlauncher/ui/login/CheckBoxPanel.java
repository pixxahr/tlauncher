// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import java.awt.event.ItemEvent;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.ui.alert.Alert;
import javafx.application.Platform;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import java.awt.event.ItemListener;
import org.tlauncher.tlauncher.ui.swing.CheckBoxListener;
import java.awt.Component;
import java.awt.Font;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.ui.loc.LocalizableCheckbox;
import org.tlauncher.tlauncher.ui.block.BlockablePanel;

public class CheckBoxPanel extends BlockablePanel implements LoginForm.LoginProcessListener
{
    private static final long serialVersionUID = 768489049585749260L;
    public final LocalizableCheckbox forceupdate;
    private static boolean helperOpen;
    private final LocalizableCheckbox chooseTypeAccount;
    private final Configuration settings;
    private boolean state;
    private final LoginForm loginForm;
    private InputPanel inputPanel;
    
    CheckBoxPanel(final LoginForm lf, final InputPanel input, final boolean chooserState) {
        this.loginForm = lf;
        this.inputPanel = input;
        (this.forceupdate = new LocalizableCheckbox("loginform.checkbox.forceupdate")).setFocusable(false);
        this.chooseTypeAccount = new LocalizableCheckbox("loginform.checkbox.account");
        this.settings = TLauncher.getInstance().getConfiguration();
        if (!OS.is(OS.WINDOWS)) {
            final Font f = this.chooseTypeAccount.getFont();
            this.chooseTypeAccount.setFont(new Font(f.getFamily(), f.getStyle(), 11));
            this.forceupdate.setFont(new Font(f.getFamily(), f.getStyle(), 11));
        }
        this.add(this.forceupdate);
        this.add(this.chooseTypeAccount);
        this.chooseTypeAccount.getModel().setSelected(chooserState);
        this.forceupdate.addItemListener(new CheckBoxListener() {
            @Override
            public void itemStateChanged(final boolean newstate) {
                CheckBoxPanel.this.state = newstate;
                CheckBoxPanel.this.loginForm.play.updateState();
            }
        });
        this.chooseTypeAccount.addItemListener(e -> {
            if (e.getStateChange() == 1) {
                Platform.runLater((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        CheckBoxPanel.this.inputPanel.changeTypeAccount(Account.AccountType.SPECIAL);
                        TLauncher.getInstance().getConfiguration().set("chooser.type.account", true);
                        if (CheckBoxPanel.this.settings.isFirstRun() && !CheckBoxPanel.helperOpen) {
                            CheckBoxPanel.this.loginForm.pane.openAccountEditor();
                            CheckBoxPanel.this.loginForm.pane.accountEditor.setShownAccountHelper(true, true);
                            CheckBoxPanel.helperOpen = true;
                            CheckBoxPanel.this.loginForm.pane.accountEditor.addComponentListener(new ComponentListener() {
                                @Override
                                public void componentShown(final ComponentEvent e) {
                                }
                                
                                @Override
                                public void componentResized(final ComponentEvent e) {
                                }
                                
                                @Override
                                public void componentMoved(final ComponentEvent e) {
                                }
                                
                                @Override
                                public void componentHidden(final ComponentEvent e) {
                                    CheckBoxPanel.this.loginForm.pane.accountEditor.setShownAccountHelper(false, false);
                                }
                            });
                        }
                    }
                });
            }
            else {
                this.inputPanel.changeTypeAccount(Account.AccountType.FREE);
                TLauncher.getInstance().getConfiguration().set("chooser.type.account", false);
            }
        });
    }
    
    @Override
    public void logginingIn() throws LoginException {
        final VersionSyncInfo syncInfo = this.loginForm.versions.getVersion();
        if (syncInfo == null) {
            return;
        }
        final boolean supporting = syncInfo.hasRemote();
        final boolean installed = syncInfo.isInstalled();
        if (this.state) {
            if (!supporting) {
                Alert.showLocError("forceupdate.local");
                throw new LoginException("Cannot update local version!");
            }
            if (installed && !Alert.showLocQuestion("forceupdate.question")) {
                throw new LoginException("User has cancelled force updating.");
            }
        }
    }
    
    @Override
    public void loginFailed() {
    }
    
    @Override
    public void loginSucceed() {
    }
    
    static {
        CheckBoxPanel.helperOpen = false;
    }
}
