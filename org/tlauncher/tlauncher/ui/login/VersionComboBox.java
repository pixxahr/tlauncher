// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import java.awt.event.ItemEvent;
import javax.swing.SwingUtilities;
import net.minecraft.launcher.versions.Version;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import java.util.Iterator;
import java.util.Collection;
import java.util.List;
import net.minecraft.launcher.versions.CompleteVersion;
import java.io.IOException;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.swing.VersionCellRenderer;
import org.tlauncher.tlauncher.ui.swing.SimpleComboBoxModel;
import org.tlauncher.tlauncher.managers.VersionManager;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityListener;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.managers.VersionManagerListener;
import org.tlauncher.tlauncher.ui.block.Blockable;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedComboBox;

public class VersionComboBox extends ExtendedComboBox<VersionSyncInfo> implements Blockable, VersionManagerListener, LocalizableComponent, LoginForm.LoginProcessListener, GameEntityListener
{
    private static final long serialVersionUID = -9122074452728842733L;
    private static final VersionSyncInfo LOADING;
    private static final VersionSyncInfo EMPTY;
    private final VersionManager manager;
    private final LoginForm loginForm;
    private final SimpleComboBoxModel<VersionSyncInfo> model;
    private String selectedVersion;
    
    VersionComboBox(final LoginForm lf) {
        super(new VersionCellRenderer() {
            @Override
            public boolean getShowTLauncherVersions() {
                return false;
            }
        });
        this.model = this.getSimpleModel();
        this.loginForm = lf;
        (this.manager = TLauncher.getInstance().getVersionManager()).addListener(this);
        final VersionSyncInfo selected;
        this.addItemListener(e -> {
            this.loginForm.play.updateState();
            selected = this.getVersion();
            if (selected != null) {
                this.selectedVersion = selected.getID();
            }
            return;
        });
        this.selectedVersion = lf.global.get("login.version.game");
    }
    
    public VersionSyncInfo getVersion() {
        final VersionSyncInfo selected = (VersionSyncInfo)this.getSelectedItem();
        return (selected == null || selected.equals(VersionComboBox.LOADING) || selected.equals(VersionComboBox.EMPTY)) ? null : selected;
    }
    
    @Override
    public void logginingIn() throws LoginException {
        final VersionSyncInfo selected = this.getVersion();
        if (selected == null) {
            throw new LoginWaitException("Version list is empty, refreshing", () -> {
                this.manager.refresh();
                if (this.getVersion() == null) {
                    Alert.showLocError("versions.notfound");
                }
                throw new LoginException("Giving user a second chance to choose correct version...");
            });
        }
        if (!selected.hasRemote() || !selected.isInstalled() || selected.isUpToDate()) {
            return;
        }
        if (!Alert.showLocQuestion("versions.found-update")) {
            try {
                final CompleteVersion complete = this.manager.getLocalList().getCompleteVersion(selected.getLocal());
                complete.setUpdatedTime(selected.getLatestVersion().getUpdatedTime());
                this.manager.getLocalList().saveVersion(complete);
            }
            catch (IOException e) {
                Alert.showLocError("versions.found-update.error");
            }
            return;
        }
        this.loginForm.versionPanel.forceupdate.setSelected(true);
    }
    
    @Override
    public void loginFailed() {
    }
    
    @Override
    public void loginSucceed() {
    }
    
    @Override
    public void updateLocale() {
        this.updateList(this.manager.getVersions(), null);
    }
    
    @Override
    public void onVersionsRefreshing(final VersionManager vm) {
        this.updateList(null, null);
    }
    
    @Override
    public void onVersionsRefreshingFailed(final VersionManager vm) {
        this.updateList(this.manager.getVersions(), null);
    }
    
    @Override
    public void onVersionsRefreshed(final VersionManager vm) {
        this.updateList(this.manager.getVersions(), null);
    }
    
    void updateList(final List<VersionSyncInfo> list, String select) {
        if (select == null && this.selectedVersion != null) {
            select = this.selectedVersion;
        }
        this.removeAllItems();
        if (list == null) {
            this.addItem(VersionComboBox.LOADING);
            return;
        }
        if (list.isEmpty()) {
            this.addItem(VersionComboBox.EMPTY);
        }
        else {
            this.model.addElements(list);
            for (final VersionSyncInfo version : list) {
                if (select != null && version.getID().equals(select)) {
                    this.setSelectedItem(version);
                }
            }
        }
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
    
    @Override
    public void activationStarted(final GameEntityDTO e) {
    }
    
    @Override
    public void activation(final GameEntityDTO e) {
    }
    
    @Override
    public void activationError(final GameEntityDTO e, final Throwable t) {
    }
    
    @Override
    public void processingStarted(final GameEntityDTO e, final VersionDTO version) {
    }
    
    @Override
    public void installEntity(final GameEntityDTO e, final GameType type) {
    }
    
    @Override
    public void installEntity(final CompleteVersion e) {
        final VersionSyncInfo versionSyncInfo = new VersionSyncInfo(e, null);
        this.model.insertElementAt(versionSyncInfo, 0);
        this.model.setSelectedItem(versionSyncInfo);
    }
    
    @Override
    public void removeEntity(final GameEntityDTO e) {
        SwingUtilities.invokeLater(this::repaint);
    }
    
    @Override
    public void removeCompleteVersion(final CompleteVersion e) {
        for (int i = 0; i < this.model.getSize(); ++i) {
            if (this.model.getElementAt(i).getLocal() != null && this.model.getElementAt(i).getLocal().getID().equals(e.getID())) {
                this.model.removeElementAt(i);
                break;
            }
        }
    }
    
    @Override
    public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
    }
    
    @Override
    public void populateStatus(final GameEntityDTO status, final GameType type, final boolean state) {
    }
    
    @Override
    public void updateVersion(final CompleteVersion old, final CompleteVersion newVersion) {
        for (int i = 0; i < this.model.getSize(); ++i) {
            final VersionSyncInfo versionSyncInfo = this.model.getElementAt(i);
            if (versionSyncInfo.getLocal() != null && versionSyncInfo.getLocal().getID().equals(old.getID())) {
                versionSyncInfo.setLocal(newVersion);
                this.repaint();
                break;
            }
        }
    }
    
    static {
        LOADING = VersionCellRenderer.LOADING;
        EMPTY = VersionCellRenderer.EMPTY;
    }
}
