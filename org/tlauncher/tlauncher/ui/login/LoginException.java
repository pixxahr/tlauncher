// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

public class LoginException extends RuntimeException
{
    private static final long serialVersionUID = -1186718369369624107L;
    
    public LoginException(final String reason) {
        super(reason);
    }
}
