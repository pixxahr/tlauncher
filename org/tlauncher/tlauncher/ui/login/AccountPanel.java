// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import java.awt.event.ItemEvent;
import javafx.application.Platform;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.Font;
import org.tlauncher.util.OS;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import java.awt.Color;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.BorderFactory;
import javax.swing.border.EmptyBorder;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.tlauncher.ui.loc.LocalizableCheckbox;
import org.tlauncher.tlauncher.ui.block.BlockablePanel;

public class AccountPanel extends BlockablePanel
{
    public final AccountComboBox accountComboBox;
    public final LoginForm loginForm;
    public final UsernameField username;
    public final LocalizableCheckbox chooseTypeAccount;
    private Account.AccountType type;
    private static boolean helperOpen;
    private ExtendedPanel accountPanel;
    
    public AccountPanel(final LoginForm lf, final boolean chooser) {
        this.loginForm = lf;
        this.username = new UsernameField(lf.global.get("login.account"));
        final Border empty = new EmptyBorder(0, 9, 0, 0);
        final CompoundBorder border = new CompoundBorder(BorderFactory.createEmptyBorder(), empty);
        this.username.setBorder(border);
        this.accountComboBox = new AccountComboBox(lf);
        (this.chooseTypeAccount = new LocalizableCheckbox("loginform.checkbox.account")).setForeground(Color.WHITE);
        this.chooseTypeAccount.setIconTextGap(14);
        this.chooseTypeAccount.setFocusPainted(false);
        final SpringLayout springLayout = new SpringLayout();
        this.setLayout(springLayout);
        springLayout.putConstraint("North", this.accountPanel = new ExtendedPanel(new GridLayout(1, 1)), 11, "North", this);
        springLayout.putConstraint("West", this.accountPanel, 0, "West", this);
        springLayout.putConstraint("South", this.accountPanel, 35, "North", this);
        springLayout.putConstraint("East", this.accountPanel, 0, "East", this);
        this.add(this.accountPanel);
        if (chooser) {
            this.accountPanel.add(this.accountComboBox);
            this.type = Account.AccountType.MOJANG;
            lf.addLoginProcessListener(this.accountComboBox);
        }
        else {
            this.accountPanel.add(this.username);
            this.type = Account.AccountType.FREE;
            lf.addLoginProcessListener(this.username);
        }
        if (!OS.is(OS.WINDOWS)) {
            final Font f = this.chooseTypeAccount.getFont();
            this.chooseTypeAccount.setFont(new Font(f.getFamily(), f.getStyle(), 11));
        }
        springLayout.putConstraint("North", this.chooseTypeAccount, 44, "North", this);
        springLayout.putConstraint("South", this.chooseTypeAccount, -11, "South", this);
        springLayout.putConstraint("West", this.chooseTypeAccount, -4, "West", this.accountPanel);
        springLayout.putConstraint("East", this.chooseTypeAccount, 0, "East", this.accountPanel);
        this.add(this.chooseTypeAccount);
        this.chooseTypeAccount.getModel().setSelected(chooser);
        this.chooseTypeAccount.addItemListener(e -> {
            if (e.getStateChange() == 1) {
                Platform.runLater((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        AccountPanel.this.changeTypeAccount(Account.AccountType.SPECIAL);
                        TLauncher.getInstance().getConfiguration().set("chooser.type.account", true);
                        if (TLauncher.getInstance().getConfiguration().isFirstRun() && !AccountPanel.helperOpen) {
                            AccountPanel.this.loginForm.pane.openAccountEditor();
                            AccountPanel.this.loginForm.pane.accountEditor.setShownAccountHelper(true, true);
                            AccountPanel.helperOpen = true;
                            AccountPanel.this.loginForm.pane.accountEditor.addComponentListener(new ComponentListener() {
                                @Override
                                public void componentShown(final ComponentEvent e) {
                                }
                                
                                @Override
                                public void componentResized(final ComponentEvent e) {
                                }
                                
                                @Override
                                public void componentMoved(final ComponentEvent e) {
                                }
                                
                                @Override
                                public void componentHidden(final ComponentEvent e) {
                                    AccountPanel.this.loginForm.pane.accountEditor.setShownAccountHelper(false, false);
                                }
                            });
                        }
                    }
                });
            }
            else {
                this.changeTypeAccount(Account.AccountType.FREE);
                TLauncher.getInstance().getConfiguration().set("chooser.type.account", false);
            }
        });
    }
    
    public void changeTypeAccount(final Account.AccountType type) {
        if (type.equals(Account.AccountType.SPECIAL)) {
            if (this.accountPanel.contains(this.username)) {
                this.accountPanel.remove(this.username);
            }
            this.accountPanel.add(this.accountComboBox);
            this.loginForm.addLoginProcessListener(this.accountComboBox);
            this.loginForm.removeLoginProcessListener(this.username);
            this.type = Account.AccountType.SPECIAL;
        }
        else {
            if (this.accountPanel.contains(this.accountComboBox)) {
                this.accountPanel.remove(this.accountComboBox);
            }
            this.accountPanel.add(this.username);
            this.loginForm.addLoginProcessListener(this.username);
            this.loginForm.removeLoginProcessListener(this.accountComboBox);
            this.type = Account.AccountType.FREE;
        }
        this.revalidate();
        this.repaint();
    }
    
    public String getUsername() {
        if (this.type.equals(Account.AccountType.FREE)) {
            return this.username.getUsername();
        }
        return ((Account)this.accountComboBox.getSelectedItem()).getUsername();
    }
    
    public Account.AccountType getTypeAccountShow() {
        return this.type;
    }
    
    static {
        AccountPanel.helperOpen = false;
    }
}
