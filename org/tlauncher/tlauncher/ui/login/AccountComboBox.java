// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login;

import java.awt.event.ItemEvent;
import java.util.Iterator;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org.tlauncher.tlauncher.minecraft.auth.AuthenticatorDatabase;
import org.tlauncher.tlauncher.minecraft.auth.Authenticator;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.listener.AuthUIListener;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.swing.AccountCellRenderer;
import org.tlauncher.tlauncher.ui.swing.SimpleComboBoxModel;
import org.tlauncher.tlauncher.minecraft.auth.AuthenticatorListener;
import org.tlauncher.tlauncher.managers.ProfileManager;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.managers.ProfileManagerListener;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedComboBox;

public class AccountComboBox extends ExtendedComboBox<Account> implements Blockable, LoginForm.LoginProcessListener, ProfileManagerListener, LocalizableComponent
{
    private static final long serialVersionUID = 6618039863712810645L;
    private static final Account EMPTY;
    private static final Account MANAGE;
    private final ProfileManager manager;
    private final LoginForm loginForm;
    private final AuthenticatorListener listener;
    private final SimpleComboBoxModel<Account> model;
    private Account selectedAccount;
    
    AccountComboBox(final LoginForm lf) {
        super(new AccountCellRenderer());
        this.loginForm = lf;
        this.model = this.getSimpleModel();
        (this.manager = TLauncher.getInstance().getProfileManager()).addListener(this);
        this.listener = new AuthUIListener(lf);
        final Account selected;
        this.addItemListener(e -> {
            selected = (Account)this.getSelectedItem();
            if (selected != null && !selected.equals(AccountComboBox.EMPTY)) {
                if (selected.equals(AccountComboBox.MANAGE)) {
                    this.loginForm.pane.openAccountEditor();
                }
                else if (selected != null) {
                    this.selectedAccount = selected;
                    TLauncher.getInstance().getConfiguration().set("account.selected", this.selectedAccount.getUserID(), true);
                }
            }
        });
    }
    
    public Account getAccount() {
        final Account value = (Account)this.getSelectedItem();
        return (value == null || value.equals(AccountComboBox.EMPTY)) ? null : value;
    }
    
    public void setAccount(final Account account) {
        if (account == null) {
            return;
        }
        if (account.equals(this.getAccount())) {
            return;
        }
        this.setSelectedItem(account);
    }
    
    void setAccount(final String username, final Account.AccountType type) {
        if (username != null) {
            this.setSelectedItem(this.manager.getAuthDatabase().getByUsername(username));
        }
    }
    
    @Override
    public void logginingIn() throws LoginException {
        final Account account = this.getAccount();
        if (account == null) {
            this.loginForm.pane.openAccountEditor();
            Alert.showLocError("account.empty.error");
            throw new LoginException("Account list is empty!");
        }
        if (!account.isFree()) {
            throw new LoginWaitException("Waiting for auth...", () -> Authenticator.instanceFor(account).pass(this.listener));
        }
    }
    
    @Override
    public void loginFailed() {
    }
    
    @Override
    public void loginSucceed() {
    }
    
    public void refreshAccounts(final AuthenticatorDatabase db, Account select) {
        if (select == null && this.selectedAccount != null) {
            select = this.selectedAccount;
        }
        this.removeAllItems();
        final Collection<Account> list = db.getAccounts();
        if (list.isEmpty()) {
            this.addItem(AccountComboBox.EMPTY);
        }
        else {
            String id = null;
            if (this.selectedAccount == null) {
                id = TLauncher.getInstance().getConfiguration().get("account.selected");
            }
            this.model.addElements(list);
            if (StringUtils.isNotBlank((CharSequence)id)) {
                for (final Account account : list) {
                    if (id.equals(account.getUserID())) {
                        select = (this.selectedAccount = account);
                        break;
                    }
                }
            }
            else if (this.selectedAccount != null) {
                TLauncher.getInstance().getConfiguration().set("account.selected", this.selectedAccount.getUserID(), true);
            }
            for (final Account account : list) {
                if (select != null && select.equals(account)) {
                    this.setSelectedItem(account);
                }
            }
        }
        this.addItem(AccountComboBox.MANAGE);
    }
    
    @Override
    public void updateLocale() {
        this.refreshAccounts(this.manager.getAuthDatabase(), this.selectedAccount);
    }
    
    @Override
    public void onAccountsRefreshed(final AuthenticatorDatabase db) {
        this.refreshAccounts(db, this.selectedAccount);
    }
    
    @Override
    public void onProfilesRefreshed(final ProfileManager pm) {
        this.refreshAccounts(pm.getAuthDatabase(), this.selectedAccount);
    }
    
    @Override
    public void onProfileManagerChanged(final ProfileManager pm) {
        this.refreshAccounts(pm.getAuthDatabase(), this.selectedAccount);
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
    
    static {
        EMPTY = AccountCellRenderer.EMPTY;
        MANAGE = AccountCellRenderer.MANAGE;
    }
}
