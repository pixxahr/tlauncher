// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.tlauncher.managers.ComponentManager;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.managers.ComponentManagerListenerHelper;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.updater.client.Updater;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import org.tlauncher.tlauncher.updater.client.UpdaterListener;
import org.tlauncher.tlauncher.managers.ComponentManagerListener;
import org.tlauncher.tlauncher.ui.block.Blockable;

public class RefreshButton extends MainImageButton implements Blockable, ComponentManagerListener, UpdaterListener
{
    private static final long serialVersionUID = -1334187593288746348L;
    private static final int TYPE_REFRESH = 0;
    private static final int TYPE_CANCEL = 1;
    private LoginForm lf;
    private int type;
    private final Image refresh;
    private final Image cancel;
    private Updater updaterFlag;
    
    private RefreshButton(final LoginForm loginform, final int type) {
        super(RefreshButton.DARD_GREEN_COLOR, "refresh-mouse-under.png", "refresh-gray.png");
        this.refresh = ImageCache.getImage("refresh-mouse-under.png");
        this.cancel = ImageCache.getImage("cancel.png");
        super.image = this.refresh;
        this.lf = loginform;
        this.setType(type, false);
        this.addActionListener(e -> this.onPressButton());
        TLauncher.getInstance().getManager().getComponent(ComponentManagerListenerHelper.class).addListener(this);
        TLauncher.getInstance().getUpdater().addListener(this);
    }
    
    RefreshButton(final LoginForm loginform) {
        this(loginform, 0);
    }
    
    private void onPressButton() {
        switch (this.type) {
            case 0: {
                if (this.updaterFlag != null) {
                    this.updaterFlag.asyncFindUpdate();
                }
                TLauncher.getInstance().getManager().startAsyncRefresh();
                break;
            }
            case 1: {
                TLauncher.getInstance().getManager().stopRefresh();
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown type: " + this.type + ". Use RefreshButton.TYPE_* constants.");
            }
        }
        this.lf.defocus();
    }
    
    void setType(final int type) {
        this.setType(type, true);
    }
    
    private void setType(final int type, final boolean repaint) {
        Image image = null;
        switch (type) {
            case 0: {
                image = this.refresh;
                break;
            }
            case 1: {
                image = this.cancel;
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown type: " + type + ". Use RefreshButton.TYPE_* constants.");
            }
        }
        this.type = type;
        super.image = image;
    }
    
    @Override
    public void onUpdaterRequesting(final Updater u) {
    }
    
    @Override
    public void onUpdaterErrored(final Updater.SearchFailed failed) {
        this.updaterFlag = failed.getUpdater();
    }
    
    @Override
    public void onUpdaterSucceeded(final Updater.SearchSucceeded succeeded) {
        this.updaterFlag = null;
    }
    
    @Override
    public void onComponentsRefreshing(final ComponentManager manager) {
        Blocker.block(this, "refresh");
    }
    
    @Override
    public void onComponentsRefreshed(final ComponentManager manager) {
        Blocker.unblock(this, "refresh");
    }
    
    @Override
    public void block(final Object reason) {
        if (reason.equals("refresh")) {
            this.setType(1);
        }
        else {
            this.setEnabled(false);
        }
    }
    
    @Override
    public void unblock(final Object reason) {
        if (reason.equals("refresh")) {
            this.setType(0);
        }
        this.setEnabled(true);
    }
}
