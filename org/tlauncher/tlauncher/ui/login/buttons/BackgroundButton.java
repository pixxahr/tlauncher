// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import java.awt.Graphics;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;

public class BackgroundButton extends LocalizableButton
{
    private Color defaultColor;
    private Color clickColor;
    
    public BackgroundButton(final Color background) {
        this.setContentAreaFilled(false);
        this.setOpaque(true);
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        if (this.getModel().isPressed()) {
            g.setColor(this.clickColor);
        }
        else {
            g.setColor(this.defaultColor);
        }
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        super.paintComponent(g);
    }
}
