// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.util.OS;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import org.tlauncher.tlauncher.ui.block.Unblockable;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;

public class VisitButton extends LocalizableButton implements Unblockable
{
    private static final long serialVersionUID = 1301825302386488945L;
    private static URI ru;
    private static URI en;
    private URI link;
    
    VisitButton() {
        super("loginform.button.visit");
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                OS.openLink(VisitButton.this.link);
            }
        });
        this.updateLocale();
    }
    
    @Override
    public void updateLocale() {
        super.updateLocale();
        this.link = (TLauncher.getInstance().getConfiguration().isUSSRLocale() ? VisitButton.ru : VisitButton.en);
    }
    
    static {
        VisitButton.ru = U.makeURI("http://masken.ru/rum.html");
        VisitButton.en = U.makeURI("http://masken.ru/enmine.html");
    }
}
