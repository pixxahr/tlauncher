// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.GridLayout;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class ButtonPanel extends ExtendedPanel
{
    public final Color mouseUnderButton;
    public final LoginForm loginForm;
    public final RefreshButton refresh;
    public final FolderButton folder;
    public final SettingsButton settings;
    private final MainImageButton mainImageButton;
    
    public ButtonPanel(final LoginForm lf) {
        this.mouseUnderButton = new Color(82, 127, 53);
        this.loginForm = lf;
        this.setLayout(new GridLayout(0, 4));
        this.add(this.mainImageButton = new ModpackButton(new Color(113, 169, 76), "modpack-tl-new.png", "modpack-tl-active-new.png"));
        this.add(this.refresh = new RefreshButton(lf));
        this.add(this.folder = new FolderButton(lf));
        this.add(this.settings = new SettingsButton(lf));
    }
}
