// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import org.tlauncher.tlauncher.managers.VersionManager;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.util.async.AsyncThread;
import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.ui.scenes.PseudoScene;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.scenes.ModpackScene;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import org.tlauncher.tlauncher.managers.VersionManagerListener;
import org.tlauncher.tlauncher.ui.block.Blockable;

public class ModpackButton extends MainImageButton implements Blockable, VersionManagerListener
{
    public ModpackButton(final Color color, final String image, final String mouseUnderImage) {
        super(color, image, mouseUnderImage);
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final ModpackScene scene = TLauncher.getInstance().getFrame().mp.modpackScene;
                AsyncThread.execute(new Runnable() {
                    @Override
                    public void run() {
                        U.debug("memory status before loading" + U.memoryStatus());
                        ((ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class)).loadInfo();
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                TLauncher.getInstance().getFrame().mp.setScene(scene);
                            }
                        });
                    }
                });
            }
        });
        Blocker.block(this, false);
        TLauncher.getInstance().getManager().getComponent(VersionManager.class).addListener(this);
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
    
    @Override
    public void onVersionsRefreshing(final VersionManager manager) {
        this.block(false);
    }
    
    @Override
    public void onVersionsRefreshingFailed(final VersionManager manager) {
        this.unblock(true);
    }
    
    @Override
    public void onVersionsRefreshed(final VersionManager manager) {
        this.unblock(true);
    }
}
