// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import java.awt.event.ActionEvent;
import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.util.OS;
import org.tlauncher.util.MinecraftUtil;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import org.tlauncher.tlauncher.ui.block.Unblockable;

public class FolderButton extends MainImageButton implements Unblockable
{
    FolderButton(final LoginForm loginform) {
        super(FolderButton.DARD_GREEN_COLOR, "folder-mouse-under.png", "folder.png");
        this.addActionListener(e -> AsyncThread.execute(() -> OS.openFolder(MinecraftUtil.getWorkingDirectory())));
    }
}
