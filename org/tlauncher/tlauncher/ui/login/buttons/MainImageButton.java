// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import javax.swing.BorderFactory;
import java.awt.event.MouseAdapter;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;

public class MainImageButton extends ImageUdaterButton
{
    private static final Color mouseUnderColor;
    protected MouseAdapter adapter;
    
    MainImageButton(final Color color, final String image, final String mouseUnderImage) {
        super(color, MainImageButton.mouseUnderColor, image, mouseUnderImage);
        this.setBorder(BorderFactory.createEmptyBorder());
    }
    
    static {
        mouseUnderColor = new Color(82, 127, 53);
    }
}
