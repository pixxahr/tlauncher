// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.ui.block.Blocker;
import javax.swing.SwingUtilities;
import java.awt.Component;
import org.tlauncher.tlauncher.configuration.Configuration;
import java.awt.Dimension;
import java.awt.Color;
import org.tlauncher.tlauncher.entity.ConfigEnum;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.scenes.DefaultScene;
import javax.swing.JMenuItem;
import org.tlauncher.tlauncher.ui.scenes.PseudoScene;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;
import javax.swing.JPopupMenu;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import org.tlauncher.tlauncher.ui.block.Blockable;

public class SettingsButton extends MainImageButton implements Blockable
{
    private static final long serialVersionUID = 1321382157134544911L;
    private final LoginForm lf;
    private final JPopupMenu popup;
    private final LocalizableMenuItem versionManager;
    private final LocalizableMenuItem settings;
    
    SettingsButton(final LoginForm loginform) {
        super(SettingsButton.DARD_GREEN_COLOR, "settings-mouse-under.png", "settings.png");
        this.lf = loginform;
        this.image = ImageCache.getImage("settings-mouse-under.png");
        this.popup = new JPopupMenu();
        (this.settings = new LocalizableMenuItem("loginform.button.settings.launcher")).addActionListener(e -> this.lf.pane.setScene(this.lf.pane.settingsScene));
        this.popup.add(this.settings);
        (this.versionManager = new LocalizableMenuItem("loginform.button.settings.version")).addActionListener(e -> {
            if (this.lf.scene.settingsForm.isVisible()) {
                this.lf.scene.setSidePanel(null);
            }
            this.lf.pane.setScene(this.lf.pane.versionManager);
            return;
        });
        this.popup.add(this.versionManager);
        final Configuration c = TLauncher.getInstance().getConfiguration();
        if (c.getBoolean(ConfigEnum.UPDATER_LAUNCHER)) {
            final LocalizableMenuItem updater = new LocalizableMenuItem("updater.update.now");
            updater.setForeground(Color.RED);
            this.popup.add(updater);
            updater.addActionListener(l -> {
                c.set("updater.delay", 0);
                TLauncher.getInstance().getUpdater().asyncFindUpdate();
                return;
            });
        }
        this.setPreferredSize(new Dimension(30, this.getHeight()));
        this.addActionListener(e -> this.callPopup());
    }
    
    void callPopup() {
        SwingUtilities.invokeLater(() -> {
            this.lf.defocus();
            this.popup.show(this, 0, this.getHeight());
        });
    }
    
    @Override
    public void block(final Object reason) {
        if (reason.equals("auth") || reason.equals("launch")) {
            Blocker.blockComponents(reason, this.versionManager);
        }
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(reason, this.versionManager);
    }
}
