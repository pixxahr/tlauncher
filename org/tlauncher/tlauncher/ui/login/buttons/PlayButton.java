// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.login.buttons;

import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.ui.block.Blocker;
import net.minecraft.launcher.updater.VersionSyncInfo;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.button.RoundImageButton;

public class PlayButton extends RoundImageButton implements Blockable, LoginForm.LoginStateListener
{
    private static final long serialVersionUID = 6944074583143406549L;
    private PlayButtonState state;
    private final LoginForm loginForm;
    
    public PlayButton(final LoginForm lf) {
        super(ImageCache.getImage("play.png"), ImageCache.getImage("play-active.png"));
        this.setForeground(Color.WHITE);
        this.loginForm = lf;
        this.addActionListener(e -> {
            switch (this.state) {
                case CANCEL: {
                    this.loginForm.stopLauncher();
                    break;
                }
                default: {
                    this.loginForm.startLauncher();
                    break;
                }
            }
            return;
        });
        this.setFont(this.getFont().deriveFont(1).deriveFont(18.0f));
        this.setState(PlayButtonState.PLAY);
    }
    
    public PlayButtonState getState() {
        return this.state;
    }
    
    public void setState(final PlayButtonState state) {
        if (state == null) {
            throw new NullPointerException();
        }
        this.state = state;
        this.setText(state.getPath());
        if (state == PlayButtonState.CANCEL) {
            this.setEnabled(true);
        }
    }
    
    public void updateState() {
        if (this.loginForm.versions == null) {
            return;
        }
        final VersionSyncInfo vs = this.loginForm.versions.getVersion();
        if (vs == null) {
            return;
        }
        final boolean installed = vs.isInstalled();
        final boolean force = this.loginForm.versionPanel.forceupdate.getState();
        if (!installed) {
            this.setState(PlayButtonState.INSTALL);
        }
        else {
            this.setState(force ? PlayButtonState.REINSTALL : PlayButtonState.PLAY);
        }
    }
    
    @Override
    public void loginStateChanged(final LoginForm.LoginState state) {
        if (state == LoginForm.LoginState.LAUNCHING) {
            this.setState(PlayButtonState.CANCEL);
            this.setEnabled(false);
        }
        else {
            this.updateState();
            this.setEnabled(!Blocker.isBlocked(this));
        }
    }
    
    @Override
    public void block(final Object reason) {
        if (this.state != PlayButtonState.CANCEL) {
            this.setEnabled(false);
        }
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
    
    public enum PlayButtonState
    {
        REINSTALL("loginform.enter.reinstall"), 
        INSTALL("loginform.enter.install"), 
        PLAY("loginform.enter"), 
        CANCEL("loginform.enter.cancel");
        
        private final String path;
        
        private PlayButtonState(final String path) {
            this.path = path;
        }
        
        public String getPath() {
            return this.path;
        }
    }
}
