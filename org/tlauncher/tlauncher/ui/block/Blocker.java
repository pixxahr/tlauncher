// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.block;

import java.util.Hashtable;
import java.awt.Container;
import java.awt.Component;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Blocker
{
    private static final Map<Blockable, List<Object>> blockMap;
    public static final Object UNIVERSAL_UNBLOCK;
    public static final Object WEAK_BLOCK;
    
    private static void add(final Blockable blockable) {
        if (blockable == null) {
            throw new NullPointerException();
        }
        Blocker.blockMap.put(blockable, Collections.synchronizedList(new ArrayList<Object>()));
    }
    
    public static void cleanUp(final Blockable blockable) {
        if (blockable == null) {
            throw new NullPointerException();
        }
        Blocker.blockMap.remove(blockable);
    }
    
    public static boolean contains(final Blockable blockable) {
        if (blockable == null) {
            throw new NullPointerException();
        }
        return Blocker.blockMap.containsKey(blockable);
    }
    
    public static void block(final Blockable blockable, final Object reason) {
        if (blockable == null) {
            return;
        }
        if (reason == null) {
            throw new NullPointerException("Reason is NULL!");
        }
        if (!Blocker.blockMap.containsKey(blockable)) {
            add(blockable);
        }
        final List<Object> reasons = Blocker.blockMap.get(blockable);
        if (reasons.contains(reason)) {
            return;
        }
        final boolean blocked = !reasons.isEmpty();
        reasons.add(reason);
        if (blocked) {
            return;
        }
        blockable.block(reason);
    }
    
    public static void block(final Object reason, final Blockable... blockables) {
        if (blockables == null || reason == null) {
            throw new NullPointerException("Blockables are NULL: " + (blockables == null) + ", reason is NULL: " + (reason == null));
        }
        for (final Blockable blockable : blockables) {
            block(blockable, reason);
        }
    }
    
    public static boolean unblock(final Blockable blockable, final Object reason) {
        if (blockable == null) {
            return false;
        }
        if (reason == null) {
            throw new NullPointerException("Reason is NULL!");
        }
        if (!Blocker.blockMap.containsKey(blockable)) {
            return true;
        }
        final List<Object> reasons = Blocker.blockMap.get(blockable);
        reasons.remove(reason);
        if (reason.equals(Blocker.UNIVERSAL_UNBLOCK)) {
            reasons.clear();
        }
        if (reasons.contains(Blocker.WEAK_BLOCK)) {
            reasons.remove(Blocker.WEAK_BLOCK);
        }
        if (!reasons.isEmpty()) {
            return false;
        }
        blockable.unblock(reason);
        return true;
    }
    
    public static void unblock(final Object reason, final Blockable... blockables) {
        if (blockables == null || reason == null) {
            throw new NullPointerException("Blockables are NULL: " + (blockables == null) + ", reason is NULL: " + (reason == null));
        }
        for (final Blockable blockable : blockables) {
            unblock(blockable, reason);
        }
    }
    
    public static void setBlocked(final Blockable blockable, final Object reason, final boolean blocked) {
        if (blocked) {
            block(blockable, reason);
        }
        else {
            unblock(blockable, reason);
        }
    }
    
    public static boolean isBlocked(final Blockable blockable) {
        if (blockable == null) {
            throw new NullPointerException();
        }
        return Blocker.blockMap.containsKey(blockable) && !Blocker.blockMap.get(blockable).isEmpty();
    }
    
    public static List<Object> getBlockList(final Blockable blockable) {
        if (blockable == null) {
            throw new NullPointerException();
        }
        if (!Blocker.blockMap.containsKey(blockable)) {
            add(blockable);
        }
        return Collections.unmodifiableList((List<?>)Blocker.blockMap.get(blockable));
    }
    
    public static void blockComponents(final Object reason, final Component... components) {
        if (components == null) {
            throw new NullPointerException("Components is NULL!");
        }
        if (reason == null) {
            throw new NullPointerException("Reason is NULL!");
        }
        for (final Component component : components) {
            if (component instanceof Blockable) {
                block((Blockable)component, reason);
            }
            else if (!(component instanceof Unblockable)) {
                component.setEnabled(false);
                if (component instanceof Container) {
                    blockComponents((Container)component, reason);
                }
            }
        }
    }
    
    public static void blockComponents(final Container container, final Object reason) {
        blockComponents(reason, container.getComponents());
    }
    
    public static void unblockComponents(final Object reason, final Component... components) {
        if (components == null) {
            throw new NullPointerException("Components is NULL!");
        }
        if (reason == null) {
            throw new NullPointerException("Reason is NULL!");
        }
        for (final Component component : components) {
            if (component instanceof Blockable) {
                unblock((Blockable)component, reason);
            }
            else if (!(component instanceof Unblockable)) {
                component.setEnabled(true);
                if (component instanceof Container) {
                    unblockComponents((Container)component, reason);
                }
            }
        }
    }
    
    public static void unblockComponents(final Container container, final Object reason) {
        unblockComponents(reason, container.getComponents());
    }
    
    static {
        blockMap = new Hashtable<Blockable, List<Object>>();
        UNIVERSAL_UNBLOCK = "universal block";
        WEAK_BLOCK = "weak";
    }
}
