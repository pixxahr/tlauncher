// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.block;

public interface Blockable
{
    void block(final Object p0);
    
    void unblock(final Object p0);
}
