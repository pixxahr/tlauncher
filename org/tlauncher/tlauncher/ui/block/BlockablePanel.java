// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.block;

import java.awt.Container;
import java.awt.LayoutManager;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class BlockablePanel extends ExtendedPanel implements Blockable
{
    private static final long serialVersionUID = 1L;
    
    public BlockablePanel(final LayoutManager layout, final boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
    }
    
    public BlockablePanel(final LayoutManager layout) {
        super(layout);
    }
    
    public BlockablePanel() {
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
        Blocker.blockComponents(this, reason);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
        Blocker.unblockComponents(this, reason);
    }
}
