// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.block;

import java.awt.Container;
import javax.swing.JLayeredPane;

public class BlockableLayeredPane extends JLayeredPane implements Blockable
{
    private static final long serialVersionUID = 1L;
    
    @Override
    public void block(final Object reason) {
        Blocker.blockComponents(this, reason);
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(this, reason);
    }
}
