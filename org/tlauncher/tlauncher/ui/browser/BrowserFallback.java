// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.browser;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.ImageObserver;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.block.Blockable;
import javax.swing.JPanel;

public class BrowserFallback extends JPanel implements Blockable
{
    private final BrowserHolder holder;
    private Image image;
    private int imageWidth;
    private int imageHeight;
    
    BrowserFallback(final BrowserHolder holder) {
        this.holder = holder;
    }
    
    private void updateImage() {
        this.image = ImageCache.getImage("plains.png");
        this.imageWidth = this.image.getWidth(null);
        this.imageHeight = this.image.getHeight(null);
    }
    
    public void paintComponent(final Graphics g) {
        if (this.image == null) {
            return;
        }
        final double windowWidth = this.getWidth();
        final double windowHeight = this.getHeight();
        final double ratio = Math.min(this.imageWidth / windowWidth, this.imageHeight / windowHeight);
        double width;
        double height;
        if (ratio < 1.0) {
            width = this.imageWidth;
            height = this.imageHeight;
        }
        else {
            width = this.imageWidth / ratio;
            height = this.imageHeight / ratio;
        }
        final double x = (windowWidth - width) / 2.0;
        final double y = (windowHeight - height) / 2.0;
        g.drawImage(this.image, (int)x, (int)y, (int)width, (int)height, null);
    }
    
    @Override
    public void block(final Object reason) {
        this.holder.removeAll();
        this.holder.setCenter((Component)this.holder.browser);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.updateImage();
        this.holder.removeAll();
        this.holder.setCenter(this);
        if (this.holder.browser != null) {
            this.holder.browser.cleanupResources();
        }
    }
}
