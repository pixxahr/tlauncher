// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.browser;

import org.tlauncher.tlauncher.ui.login.LoginFormHelper;

public class BrowserBridge
{
    private final BrowserHolder holder;
    
    BrowserBridge(final BrowserPanel browser) {
        this.holder = browser.holder;
    }
    
    public void toggleHelper() {
        if (this.holder.pane.defaultScene.loginFormHelper.getState() == LoginFormHelper.LoginFormHelperState.NONE) {
            this.holder.pane.defaultScene.loginFormHelper.setState(LoginFormHelper.LoginFormHelperState.SHOWN);
        }
        else {
            this.holder.pane.defaultScene.loginFormHelper.setState(LoginFormHelper.LoginFormHelperState.NONE);
        }
    }
}
