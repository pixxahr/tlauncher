// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.browser;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.block.Unblockable;
import org.tlauncher.tlauncher.ui.swing.ResizeableComponent;
import org.tlauncher.tlauncher.ui.swing.extended.BorderPanel;

public class BrowserHolder extends BorderPanel implements ResizeableComponent, Unblockable
{
    private static BrowserHolder browserHolder;
    MainPane pane;
    final BrowserFallback fallback;
    final BrowserPanel browser;
    
    private BrowserHolder() {
        this.fallback = new BrowserFallback(this);
        BrowserPanel browser_ = null;
        try {
            browser_ = new BrowserPanel(this);
        }
        catch (Throwable e) {
            log("Cannot load BrowserPanel. Will show BrowserFallback panel.", e);
        }
        this.browser = browser_;
        this.setBrowserShown("fallback");
    }
    
    public void setBrowserShown(final Object reason, final boolean shown) {
        if (!shown && !Blocker.isBlocked(this.fallback)) {
            this.fallback.unblock(reason);
        }
        else {
            Blocker.setBlocked(this.fallback, reason, shown);
        }
    }
    
    public void setBrowserContentShown(final Object reason, final boolean shown) {
        if (this.browser != null) {
            Blocker.setBlocked(this.browser, reason, !shown);
        }
    }
    
    @Override
    public void onResize() {
        if (this.pane == null) {
            log("pane = null so it'c can't resize");
            return;
        }
        final int width = this.pane.getWidth();
        final int height = this.pane.getHeight();
        this.setSize(width, height);
    }
    
    private static void log(final Object... o) {
        U.log("[BrowserHolder]", o);
    }
    
    public MainPane getPane() {
        return this.pane;
    }
    
    public void setPane(final MainPane pane) {
        this.pane = pane;
    }
    
    public static synchronized BrowserHolder getInstance() {
        if (BrowserHolder.browserHolder == null) {
            BrowserHolder.browserHolder = new BrowserHolder();
        }
        return BrowserHolder.browserHolder;
    }
    
    public BrowserPanel getBrowser() {
        return this.browser;
    }
    
    public void setBrowserShown(final String reason) {
        this.setBrowserShown(reason, this.browser != null);
    }
}
