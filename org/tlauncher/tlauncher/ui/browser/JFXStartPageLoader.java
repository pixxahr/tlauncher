// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.browser;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.beans.value.ChangeListener;
import org.tlauncher.util.lang.PageUtil;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.OS;
import javafx.scene.web.WebView;
import javafx.embed.swing.JFXPanel;

public class JFXStartPageLoader extends JFXPanel
{
    private String loadPage;
    private static JFXStartPageLoader jfxStartPageLoader;
    private WebView view;
    private long start;
    
    private JFXStartPageLoader() {
        if (OS.is(OS.LINUX)) {
            try {
                new JFXPanel();
            }
            catch (NoClassDefFoundError e) {
                Alert.showErrorHtml("alert.error.linux.javafx", 400);
                System.exit(1);
            }
        }
        this.loadPage = PageUtil.buildPagePath();
        Platform.runLater((Runnable)new Runnable() {
            @Override
            public void run() {
                JFXStartPageLoader.this.view = new WebView();
                JFXStartPageLoader.this.view.getEngine().getLoadWorker().stateProperty().addListener((ChangeListener)new ChangeListener<Worker.State>() {
                    public void changed(final ObservableValue<? extends Worker.State> arg0, final Worker.State arg1, final Worker.State arg2) {
                        switch (arg2) {
                            case SUCCEEDED: {
                                JFXStartPageLoader.this.log("succeeded load page: " + JFXStartPageLoader.this.loadPage + " during " + (System.currentTimeMillis() - JFXStartPageLoader.this.start) / 1000L);
                                break;
                            }
                            case FAILED: {
                                JFXStartPageLoader.this.log("fail load page: " + JFXStartPageLoader.this.loadPage);
                                break;
                            }
                            case SCHEDULED: {
                                JFXStartPageLoader.this.log("start load page: " + JFXStartPageLoader.this.loadPage);
                                JFXStartPageLoader.this.start = System.currentTimeMillis();
                                break;
                            }
                        }
                    }
                });
                JFXStartPageLoader.this.view.getEngine().load(JFXStartPageLoader.this.loadPage);
            }
        });
    }
    
    public static synchronized JFXStartPageLoader getInstance() {
        try {
            if (JFXStartPageLoader.jfxStartPageLoader == null) {
                JFXStartPageLoader.jfxStartPageLoader = new JFXStartPageLoader();
            }
        }
        catch (RuntimeException e) {
            if (e.getMessage().contains("glass.dll")) {
                String url = " https://tlauncher.org/ru/unsatisfiedlinkerror-java-bin-glass.html";
                if (!TLauncher.getInnerSettings().isUSSRLocale()) {
                    url = " https://tlauncher.org/en/unsatisfiedlinkerror-java-bin-glass.html";
                }
                Alert.showErrorHtml("", Localizable.get("alert.start.message", url));
                TLauncher.kill();
            }
        }
        return JFXStartPageLoader.jfxStartPageLoader;
    }
    
    public WebView getWebView() {
        return this.view;
    }
    
    private void log(final Object... objects) {
        U.log("[JFXStartPageLoader] ", objects);
    }
}
