// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.browser;

import org.tlauncher.tlauncher.ui.alert.Alert;
import javafx.scene.web.WebEvent;
import javafx.beans.value.ObservableValue;
import org.tlauncher.util.lang.PageUtil;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.events.MouseEvent;
import org.w3c.dom.events.Event;
import javafx.scene.web.WebHistory;
import java.util.Random;
import java.util.ArrayList;
import org.tlauncher.tlauncher.configuration.SimpleConfiguration;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.ui.MainPane;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.tlauncher.ui.scenes.PseudoScene;
import org.tlauncher.tlauncher.managers.popup.menu.HotServerManager;
import java.util.Map;
import org.tlauncher.util.statistics.StatisticsUtil;
import com.google.common.collect.Maps;
import org.w3c.dom.html.HTMLAnchorElement;
import org.w3c.dom.events.EventTarget;
import netscape.javascript.JSObject;
import java.util.Locale;
import java.util.Date;
import java.net.URI;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javafx.concurrent.Worker;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.tlauncher.tlauncher.handlers.ExceptionHandler;
import java.net.URL;
import org.apache.commons.io.Charsets;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.util.FileUtil;
import org.launcher.resource.TlauncherResource;
import org.tlauncher.util.U;
import java.io.IOException;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import javafx.application.Platform;
import org.tlauncher.skin.domain.AdvertisingDTO;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.scene.Group;
import org.tlauncher.util.advertising.AdvertisingStatusObserver;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import javafx.embed.swing.JFXPanel;

public class BrowserPanel extends JFXPanel implements LocalizableComponent, Blockable, AdvertisingStatusObserver
{
    private static final long serialVersionUID = 5857121254870623943L;
    private static final boolean freeSurf = false;
    final BrowserHolder holder;
    final BrowserBridge bridge;
    private String failPage;
    private String style;
    private String scripts;
    private String background;
    private String currentDefaultPage;
    private int width;
    private int height;
    private boolean success;
    private int counter;
    private long lastTime;
    private boolean block;
    private volatile boolean specialFlag;
    private Group group;
    private WebView view;
    private WebEngine engine;
    private long start;
    private AdvertisingDTO advertisingDTO;
    private final Object advertisingSynchronizedObject;
    
    BrowserPanel(final BrowserHolder h) throws IOException {
        this.specialFlag = false;
        this.advertisingSynchronizedObject = new Object();
        this.start = System.currentTimeMillis();
        this.holder = h;
        this.bridge = new BrowserBridge(this);
        this.loadResources();
        this.loadPageSync();
        Platform.runLater(() -> {
            Thread.currentThread().setPriority(10);
            log("Running in JavaFX Thread");
            this.prepareFX();
            this.initBrowser();
            return;
        });
        this.setOpaque(false);
        this.addComponentListener((ComponentListener)new ComponentAdapter() {
            @Override
            public void componentResized(final ComponentEvent e) {
                BrowserPanel.this.width = BrowserPanel.this.holder.getWidth();
                BrowserPanel.this.height = BrowserPanel.this.holder.getHeight();
                Platform.runLater(() -> {
                    if (BrowserPanel.this.view != null) {
                        BrowserPanel.this.view.setPrefSize((double)BrowserPanel.this.width, (double)BrowserPanel.this.height);
                    }
                });
            }
        });
    }
    
    private static void log(final Object... o) {
        U.log("[Browser]", o);
    }
    
    private static void engine(final WebEngine engine, final Object... o) {
        U.log("[Browser@" + engine.hashCode() + "]", o);
    }
    
    private static void page(final WebEngine engine, final String text, final String page) {
        engine(engine, text + ':', '\"' + page + '\"');
    }
    
    private synchronized void loadResources() throws IOException {
        log("Loading resources...");
        this.failPage = FileUtil.readStream(TlauncherResource.getResource("fail.html").openStream());
        this.style = FileUtil.readStream(TlauncherResource.getResource("style.css").openStream());
        final URL jqueryResource = TlauncherResource.getResource("jquery.js");
        final URL pageScriptResource = TlauncherResource.getResource("scripts.js");
        this.scripts = FileUtil.readStream(jqueryResource.openStream()) + '\n' + FileUtil.readStream(pageScriptResource.openStream());
        final URL backgroundResource = ImageCache.getRes("plains.png");
        log("Loading background...");
        final Object timeLock = new Object();
        final URL url = TlauncherResource.getResource("background.image.base64.txt");
        this.background = FileUtil.readStream(url.openStream(), Charsets.UTF_8.toString());
        log("Cleaning up after loading:");
        U.gc();
    }
    
    synchronized void cleanupResources() {
        this.style = null;
        this.scripts = null;
        this.background = null;
    }
    
    private void prepareFX() {
        if (!Platform.isFxApplicationThread()) {
            throw new IllegalStateException("Call this method from Platform.runLater()");
        }
        log("Preparing JavaFX...");
        Thread.currentThread().setUncaughtExceptionHandler(ExceptionHandler.getInstance());
        this.group = new Group();
        final Scene scene = new Scene((Parent)this.group);
        this.setScene(scene);
    }
    
    private synchronized void initBrowser() {
        log("Initializing...");
        this.group.getChildren().removeAll((Object[])new Node[0]);
        (this.view = JFXStartPageLoader.getInstance().getWebView()).setContextMenuEnabled(false);
        this.view.setPrefSize((double)this.width, (double)this.height);
        this.engine = this.view.getEngine();
        final WebEngine currentEngine = this.engine;
        this.engine.setOnAlert(event -> Alert.showMessage(currentEngine.getTitle(), (String)event.getData()));
        this.success = true;
        this.engine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            try {
                this.onBrowserStateChanged(currentEngine, newValue);
            }
            catch (Throwable e) {
                engine(currentEngine, "State change handle error:", e);
            }
        });
        if (this.engine.getLoadWorker().getState() == Worker.State.FAILED && !this.specialFlag) {
            final String location = this.engine.getLocation();
            this.failLoad(location);
        }
        if (this.engine.getLoadWorker().getState() == Worker.State.SUCCEEDED && !this.specialFlag) {
            final String location = this.engine.getLocation();
            this.loadSucceeded(location);
        }
        this.group.getChildren().add((Object)this.view);
    }
    
    private void failLoad(final String location) {
        this.specialFlag = true;
        page(this.engine, "Failed to load", location);
        if (!this.success) {
            this.holder.setBrowserShown("error", false);
            return;
        }
        this.success = false;
        this.loadContent(this.failPage);
    }
    
    private void onBrowserStateChanged(final WebEngine engine, final Worker.State val) {
        if (this.engine != engine) {
            return;
        }
        if (val == null) {
            throw new NullPointerException("State is NULL!");
        }
        final String location = engine.getLocation();
        this.view.setMouseTransparent(true);
        switch (val) {
            case SCHEDULED: {
                page(engine, "Loading", location);
                if (location.isEmpty()) {
                    break;
                }
                if (location.startsWith(TLauncher.getInstance().getPagePrefix())) {
                    break;
                }
                U.debug("[state] scheduled in");
                URI uri = U.makeURI(location);
                if (uri == null) {
                    uri = U.fixInvallidLink(location);
                }
                OS.openLink(uri);
                String last;
                Platform.runLater(() -> {
                    if (this.block) {
                        return;
                    }
                    else {
                        this.deniedRedirectedRecursion();
                        last = this.getLastEntry().getUrl();
                        page(this.engine, "Last entry", last);
                        this.loadPage(last);
                        return;
                    }
                });
                break;
            }
            case FAILED: {
                this.failLoad(location);
            }
            case SUCCEEDED: {
                this.loadSucceeded(location);
                break;
            }
        }
    }
    
    private void deniedRedirectedRecursion() {
        if (this.counter > 3) {
            if (new Date().before(new Date(this.lastTime + 2000L))) {
                this.block = true;
            }
            else if (this.counter > 3) {
                this.counter = 0;
            }
        }
        this.lastTime = System.currentTimeMillis();
        ++this.counter;
    }
    
    private void loadSucceeded(final String location) {
        this.specialFlag = true;
        page(this.engine, "Loaded successfully", location);
        this.holder.setBrowserShown("error", true);
        this.success = true;
        final Document document = this.engine.getDocument();
        if (document == null) {
            return;
        }
        final NodeList bodies = document.getElementsByTagName("body");
        final org.w3c.dom.Node body = bodies.item(0);
        if (body == null) {
            engine(this.engine, "What the...? Couldn't find <body> element!");
            return;
        }
        if (!location.isEmpty()) {
            final String locale = TLauncher.getInstance().getConfiguration().getLocale().getLanguage();
            if (!locale.equals(new Locale("zh").getLanguage())) {
                final Element styleElement = document.createElement("style");
                styleElement.setAttribute("type", "text/css");
                styleElement.setTextContent(this.style);
                body.appendChild(styleElement);
            }
        }
        final Element scriptElement = document.createElement("script");
        scriptElement.setAttribute("type", "text/javascript");
        scriptElement.setTextContent(this.scripts);
        body.appendChild(scriptElement);
        this.engine.executeScript(this.background);
        final JSObject jsobj = (JSObject)this.engine.executeScript("window");
        jsobj.setMember("bridge", (Object)this.bridge);
        this.cleanAdvertising(document);
        final NodeList linkList = document.getElementsByTagName("a");
        for (int i = 0; i < linkList.getLength(); ++i) {
            final org.w3c.dom.Node linkNode = linkList.item(i);
            final EventTarget eventTarget = (EventTarget)linkNode;
            final EventTarget target;
            final HTMLAnchorElement anchorElement;
            final String href;
            URI uri;
            eventTarget.addEventListener("click", evt -> {
                target = evt.getCurrentTarget();
                anchorElement = (HTMLAnchorElement)target;
                href = anchorElement.getHref();
                if (href == null || href.isEmpty() || href.startsWith("javascript:")) {
                    return;
                }
                else {
                    uri = U.makeURI(href);
                    if (uri == null) {
                        uri = U.fixInvallidLink(href);
                    }
                    OS.openLink(uri);
                    evt.preventDefault();
                    return;
                }
            }, false);
        }
        final org.w3c.dom.Node adyoutube = document.getElementById("adyoutube");
        if (adyoutube != null) {
            ((EventTarget)adyoutube).addEventListener("click", evt -> StatisticsUtil.startSending("save/adyoutube", null, Maps.newHashMap()), false);
        }
        this.mixServers(document);
        this.addListensOnMenuServer(document);
        final Element serverInfoPage = document.getElementById("server_info_page");
        if (serverInfoPage != null) {
            MainPane mp;
            ((EventTarget)serverInfoPage).addEventListener("click", evt -> {
                if (((HotServerManager)TLauncher.getInjector().getInstance((Class)HotServerManager.class)).isReady()) {
                    mp = TLauncher.getInstance().getFrame().mp;
                    mp.setScene(mp.additionalHostServerScene);
                }
                return;
            }, false);
        }
        this.processSidebarMessage(document);
        if (Blocker.isBlocked(this)) {
            this.block(null);
        }
        this.view.setMouseTransparent(false);
    }
    
    private void processSidebarMessage(final Document document) {
        final Element sidebarButton = document.getElementById("sidebar_1_button");
        final Configuration configuration = TLauncher.getInstance().getConfiguration();
        if (sidebarButton != null) {
            final Element el = document.getElementById("sidebar_1");
            if (configuration.getBoolean("main.cross.close")) {
                if (el != null) {
                    this.removeChilds(el);
                }
            }
            else {
                final org.w3c.dom.Node node;
                final SimpleConfiguration simpleConfiguration;
                ((EventTarget)sidebarButton).addEventListener("click", evt -> {
                    if (node != null) {
                        this.removeChilds(node);
                        simpleConfiguration.set("main.cross.close", true);
                    }
                }, false);
            }
        }
    }
    
    private void cleanAdvertising(final Document document) {
        synchronized (this.advertisingSynchronizedObject) {
            if (this.advertisingDTO == null) {
                try {
                    log("fx thread has waited status of ad");
                    this.advertisingSynchronizedObject.wait();
                    log("fx thread continuing work");
                }
                catch (InterruptedException e) {
                    log(String.format("got status of advertising: %s", this.advertisingDTO));
                }
            }
        }
        if (this.advertisingDTO.isInfoServerAdvertising()) {
            this.removeNode("advertising_block", document);
        }
        if (this.advertisingDTO.isCenterAdvertising()) {
            this.removeNode("adnew", document);
        }
        if (this.advertisingDTO.isVideoAdvertising()) {
            this.removeNode("adyoutube", document);
        }
    }
    
    private void removeNode(final String name, final Document document) {
        final org.w3c.dom.Node node = document.getElementById(name);
        if (node != null) {
            node.getParentNode().removeChild(node);
        }
    }
    
    private void removeChilds(final org.w3c.dom.Node node) {
        while (node.hasChildNodes()) {
            node.removeChild(node.getFirstChild());
        }
    }
    
    private void mixServers(final Document document) {
        final org.w3c.dom.Node node = document.getElementById("advertising_servers");
        if (node == null) {
            return;
        }
        final NodeList list = node.getChildNodes();
        if (list == null || list.getLength() < 2) {
            return;
        }
        final org.w3c.dom.Node element = list.item(1);
        final NodeList listRowServer = element.getChildNodes();
        final ArrayList<org.w3c.dom.Node> listOld = new ArrayList<org.w3c.dom.Node>();
        for (int i = 0; i < listRowServer.getLength(); ++i) {
            final org.w3c.dom.Node el = listRowServer.item(i);
            listOld.add(el);
        }
        this.removeChilds(element);
        final Random random = new Random();
        while (!listOld.isEmpty()) {
            final int index = random.nextInt(listOld.size());
            element.appendChild(listOld.remove(index));
        }
    }
    
    private void addListensOnMenuServer(final Document doc) {
        log("add listens into server_choose");
        final NodeList list = doc.getElementsByTagName("server");
        for (int i = 0; i < list.getLength(); ++i) {
            ((EventTarget)list.item(i)).addEventListener("mouseover", evt -> ((HotServerManager)TLauncher.getInjector().getInstance((Class)HotServerManager.class)).processingEvent(this.extractServer(evt)), false);
        }
        log("finished listens into server_choose");
    }
    
    private WebHistory.Entry getLastEntry() {
        final WebHistory hist = this.engine.getHistory();
        return (WebHistory.Entry)hist.getEntries().get(hist.getCurrentIndex());
    }
    
    private String extractServer(final Event evt) {
        final org.w3c.dom.Node node = (org.w3c.dom.Node)evt.getCurrentTarget();
        final NamedNodeMap map = node.getAttributes();
        if (map == null || map.getLength() < 2) {
            log("map=" + map + "or map.getLength() <3");
            return "";
        }
        final org.w3c.dom.Node idServer = map.item(1);
        if (idServer != null) {
            if (idServer.getNodeValue() != null && !idServer.getNodeValue().isEmpty()) {
                if (evt instanceof MouseEvent) {
                    return idServer.getNodeValue();
                }
                log("problems with your browser");
            }
            else {
                log("idServer is null or empty");
            }
        }
        else {
            log("error the node doesn't have childNodes " + node.toString());
        }
        return null;
    }
    
    private void loadPage(final String url) {
        U.debug("load started after init object = " + (System.currentTimeMillis() - this.start) / 1000L);
        engine(this.engine, "Trying to load URL: \"" + url + "\"");
        this.engine.load(url);
    }
    
    private void loadContent(final String content) {
        this.engine.loadContent(content);
    }
    
    void execute(final String script) {
        try {
            this.engine.executeScript(script);
        }
        catch (Exception e) {
            U.log("Hidden JS exception:", e);
        }
    }
    
    public void block(final Object reason) {
        Platform.runLater(() -> this.execute("page.visibility.hide();"));
    }
    
    public void unblock(final Object reason) {
        Platform.runLater(() -> this.execute("page.visibility.show();"));
    }
    
    public void updateLocale() {
        final String oldDefaultPage = this.currentDefaultPage;
        this.currentDefaultPage = PageUtil.buildPagePath();
        if (oldDefaultPage == null || oldDefaultPage.equals(this.currentDefaultPage)) {
            return;
        }
        Platform.runLater(() -> this.loadPage(this.currentDefaultPage));
    }
    
    private void loadPageSync() {
        final String oldDefaultPage = this.currentDefaultPage;
        this.currentDefaultPage = PageUtil.buildPagePath();
        if (oldDefaultPage == null || oldDefaultPage.equals(this.currentDefaultPage)) {
            return;
        }
        this.loadPage(this.currentDefaultPage);
    }
    
    public void setAdvertising(final AdvertisingDTO dto) {
        this.advertisingDTO = dto;
    }
    
    public void advertisingReceived(final AdvertisingDTO advertisingDTO) {
        synchronized (this.advertisingSynchronizedObject) {
            this.advertisingDTO = advertisingDTO;
            this.advertisingSynchronizedObject.notify();
        }
    }
}
