// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui;

import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.updater.client.Updater;
import org.tlauncher.tlauncher.ui.listener.UpdateUIListener;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.Image;
import org.tlauncher.tlauncher.updater.client.Update;
import org.tlauncher.tlauncher.updater.client.UpdaterListener;
import org.tlauncher.tlauncher.ui.swing.ImagePanel;

public class SideNotifier extends ImagePanel implements UpdaterListener
{
    private static final String LANG_PREFIX = "notifier.";
    private NotifierStatus status;
    private Update update;
    
    SideNotifier() {
        super((Image)null, 1.0f, 0.75f, false);
        TLauncher.getInstance().getUpdater().addListener(this);
    }
    
    public NotifierStatus getStatus() {
        return this.status;
    }
    
    public void setStatus(final NotifierStatus status) {
        if (status == null) {
            throw new NullPointerException();
        }
        this.status = status;
        this.setImage(status.getImage());
        if (status == NotifierStatus.NONE) {
            this.hide();
        }
        else {
            this.show();
        }
    }
    
    @Override
    protected boolean onClick() {
        final boolean result = this.processClick();
        if (result) {
            this.hide();
        }
        return result;
    }
    
    private boolean processClick() {
        if (!super.onClick()) {
            return false;
        }
        switch (this.status) {
            case FAILED: {
                Alert.showWarning(Localizable.get("notifier.failed.title"), Localizable.get("notifier.failed"));
                break;
            }
            case FOUND: {
                if (this.update == null) {
                    throw new IllegalStateException("Update is NULL!");
                }
                final String prefix = "notifier." + this.status.toString() + ".";
                final String title = prefix + "title";
                final String question = prefix + "question";
                final boolean ask = Alert.showQuestion(Localizable.get(title), Localizable.get(question, this.update.getVersion()), this.update.getDescription());
                if (!ask) {
                    return false;
                }
                final UpdateUIListener listener = new UpdateUIListener(this.update);
                listener.push();
                break;
            }
            case NONE: {
                break;
            }
            default: {
                throw new IllegalStateException("Unknown status: " + this.status);
            }
        }
        return true;
    }
    
    @Override
    public void onUpdaterRequesting(final Updater u) {
        this.setFoundUpdate(null);
    }
    
    @Override
    public void onUpdaterErrored(final Updater.SearchFailed failed) {
        this.setStatus(NotifierStatus.FAILED);
    }
    
    @Override
    public void onUpdaterSucceeded(final Updater.SearchSucceeded succeeded) {
        final Update update = succeeded.getResponse();
        this.setFoundUpdate(update.isApplicable() ? update : null);
    }
    
    private void setFoundUpdate(final Update upd) {
        this.update = upd;
        this.setStatus((upd == null) ? NotifierStatus.NONE : NotifierStatus.FOUND);
    }
    
    public enum NotifierStatus
    {
        FAILED("warning.png"), 
        FOUND("down32.png"), 
        NONE;
        
        private final Image image;
        
        private NotifierStatus(final String imagePath) {
            this.image = ((imagePath == null) ? null : ImageCache.getImage(imagePath));
        }
        
        private NotifierStatus() {
            this(null);
        }
        
        public Image getImage() {
            return this.image;
        }
        
        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }
}
