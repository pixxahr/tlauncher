// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import javax.swing.JComponent;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import org.tlauncher.tlauncher.ui.block.Blockable;

public abstract class EditorHandler implements Blockable
{
    private final String path;
    private String value;
    private final List<EditorFieldListener> listeners;
    
    public EditorHandler(final String path) {
        if (path == null) {
            throw new NullPointerException();
        }
        this.path = path;
        this.listeners = Collections.synchronizedList(new ArrayList<EditorFieldListener>());
    }
    
    public boolean addListener(final EditorFieldListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        return this.listeners.add(listener);
    }
    
    public boolean removeListener(final EditorFieldListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        return this.listeners.remove(listener);
    }
    
    public void onChange(final String newvalue) {
        for (final EditorFieldListener listener : this.listeners) {
            listener.onChange(this, this.value, newvalue);
        }
        this.value = newvalue;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public void updateValue(final Object obj) {
        final String val = (obj == null) ? null : obj.toString();
        this.onChange(val);
        this.setValue0(this.value);
    }
    
    public void setValue(final Object obj) {
        final String val = (obj == null) ? null : obj.toString();
        this.setValue0(val);
    }
    
    public abstract boolean isValid();
    
    public abstract JComponent getComponent();
    
    public abstract String getValue();
    
    protected abstract void setValue0(final String p0);
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{path='" + this.path + "', value='" + this.value + "'}";
    }
}
