// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import java.util.Arrays;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EditorGroupHandler
{
    private final List<EditorFieldChangeListener> listeners;
    private final int checkedLimit;
    private int changedFlag;
    private int checkedFlag;
    
    public EditorGroupHandler(final List<? extends EditorHandler> handlers) {
        if (handlers == null) {
            throw new NullPointerException();
        }
        this.checkedLimit = handlers.size();
        final EditorFieldListener listener = new EditorFieldListener() {
            @Override
            protected void onChange(final EditorHandler handler, final String oldValue, final String newValue) {
                if (newValue == null) {
                    return;
                }
                if (!newValue.equals(oldValue)) {
                    ++EditorGroupHandler.this.changedFlag;
                }
                ++EditorGroupHandler.this.checkedFlag;
                if (EditorGroupHandler.this.checkedFlag == EditorGroupHandler.this.checkedLimit) {
                    if (EditorGroupHandler.this.changedFlag > 0) {
                        for (final EditorFieldChangeListener listener : EditorGroupHandler.this.listeners) {
                            listener.onChange(null, null);
                        }
                    }
                    EditorGroupHandler.this.checkedFlag = (EditorGroupHandler.this.changedFlag = 0);
                }
            }
        };
        for (int i = 0; i < handlers.size(); ++i) {
            final EditorHandler handler = (EditorHandler)handlers.get(i);
            if (handler == null) {
                throw new NullPointerException("Handler is NULL at " + i);
            }
            handler.addListener(listener);
        }
        this.listeners = Collections.synchronizedList(new ArrayList<EditorFieldChangeListener>());
    }
    
    public EditorGroupHandler(final EditorHandler... handlers) {
        this(Arrays.asList(handlers));
    }
    
    public boolean addListener(final EditorFieldChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        return this.listeners.add(listener);
    }
    
    public boolean removeListener(final EditorFieldChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        return this.listeners.remove(listener);
    }
}
