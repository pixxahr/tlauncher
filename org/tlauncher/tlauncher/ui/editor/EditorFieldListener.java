// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

public abstract class EditorFieldListener
{
    protected abstract void onChange(final EditorHandler p0, final String p1, final String p2);
}
