// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import javax.swing.plaf.ComboBoxUI;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.plaf.ScrollBarUI;
import java.awt.Component;
import javax.swing.JComponent;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.swing.scroll.VersionScrollBarUI;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import org.tlauncher.tlauncher.ui.ui.TlauncherBasicComboBoxUI;
import org.tlauncher.tlauncher.ui.converter.StringConverter;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedComboBox;

public class EditorComboBox<T> extends ExtendedComboBox<T> implements EditorField
{
    private static final long serialVersionUID = -2320340434786516374L;
    private final boolean allowNull;
    
    public EditorComboBox(final StringConverter<T> converter, final T[] values, final boolean allowNull) {
        super(converter);
        this.allowNull = allowNull;
        if (values == null) {
            return;
        }
        for (final T value : values) {
            this.addItem(value);
        }
    }
    
    public EditorComboBox(final StringConverter<T> converter, final T[] values) {
        this((StringConverter<Object>)converter, values, false);
    }
    
    @Override
    public String getSettingsValue() {
        final T value = this.getSelectedValue();
        return this.convert(value);
    }
    
    @Override
    public void setSettingsValue(final String string) {
        final T value = this.convert(string);
        if (!this.allowNull && string == null) {
            boolean hasNull = false;
            for (int i = 0; i < this.getItemCount(); ++i) {
                if (this.getItemAt(i) == null) {
                    hasNull = true;
                }
            }
            if (!hasNull) {
                return;
            }
        }
        this.setSelectedValue(value);
    }
    
    @Override
    public boolean isValueValid() {
        return true;
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
    
    @Override
    protected void init() {
        final TlauncherBasicComboBoxUI list = new TlauncherBasicComboBoxUI() {
            @Override
            protected ComboPopup createPopup() {
                final BasicComboPopup basic = new BasicComboPopup(this.comboBox) {
                    @Override
                    protected JScrollPane createScroller() {
                        final VersionScrollBarUI barUI = new VersionScrollBarUI() {
                            @Override
                            protected Dimension getMinimumThumbSize() {
                                return new Dimension(10, 40);
                            }
                            
                            @Override
                            public Dimension getMaximumSize(final JComponent c) {
                                final Dimension dim = super.getMaximumSize(c);
                                dim.setSize(10.0, dim.getHeight());
                                return dim;
                            }
                            
                            @Override
                            public Dimension getPreferredSize(final JComponent c) {
                                final Dimension dim = super.getPreferredSize(c);
                                dim.setSize(10.0, dim.getHeight());
                                return dim;
                            }
                        };
                        barUI.setGapThubm(5);
                        final JScrollPane scroller = new JScrollPane(this.list, 20, 31);
                        scroller.getVerticalScrollBar().setUI(barUI);
                        return scroller;
                    }
                };
                basic.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK));
                return basic;
            }
        };
        this.setUI(list);
    }
}
