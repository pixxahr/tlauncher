// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import java.util.Arrays;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.swing.extended.VPanel;
import javax.swing.JComponent;
import java.util.List;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;

public class EditorPair
{
    private final LocalizableLabel label;
    private final List<? extends EditorHandler> handlers;
    private final JComponent[] fields;
    private final VPanel panel;
    
    public EditorPair(final String labelPath, final List<? extends EditorHandler> handlers) {
        (this.label = new LocalizableLabel(labelPath)).setFont(this.label.getFont().deriveFont(1));
        final int num = handlers.size();
        this.fields = new JComponent[num];
        for (int i = 0; i < num; ++i) {
            (this.fields[i] = ((EditorHandler)handlers.get(i)).getComponent()).setAlignmentX(0.0f);
        }
        this.handlers = handlers;
        (this.panel = new VPanel()).add((Component[])this.fields);
    }
    
    public EditorPair(final String labelPath, final EditorHandler... handlers) {
        this(labelPath, Arrays.asList(handlers));
    }
    
    public List<? extends EditorHandler> getHandlers() {
        return this.handlers;
    }
    
    public LocalizableLabel getLabel() {
        return this.label;
    }
    
    public Component[] getFields() {
        return this.fields;
    }
    
    public VPanel getPanel() {
        return this.panel;
    }
}
