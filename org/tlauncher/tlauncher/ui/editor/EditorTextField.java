// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import org.tlauncher.tlauncher.ui.loc.LocalizableTextField;

public class EditorTextField extends LocalizableTextField implements EditorField
{
    private static final long serialVersionUID = 3920711425159165958L;
    private final boolean canBeEmpty;
    
    public EditorTextField(final String prompt, final boolean canBeEmpty) {
        super(prompt);
        this.canBeEmpty = canBeEmpty;
        this.setColumns(1);
    }
    
    public EditorTextField(final String prompt) {
        this(prompt, false);
    }
    
    public EditorTextField(final boolean canBeEmpty) {
        this(null, canBeEmpty);
    }
    
    public EditorTextField() {
        this(false);
    }
    
    @Override
    public String getSettingsValue() {
        return this.getValue();
    }
    
    @Override
    public void setSettingsValue(final String value) {
        this.setText(value);
        this.setCaretPosition(0);
    }
    
    @Override
    public boolean isValueValid() {
        final String text = this.getValue();
        return text != null || this.canBeEmpty;
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
}
