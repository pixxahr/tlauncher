// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import org.tlauncher.tlauncher.ui.loc.LocalizableCheckbox;

public class EditorCheckBox extends LocalizableCheckbox implements EditorField
{
    private static final long serialVersionUID = -2540132118355226609L;
    
    public EditorCheckBox(final String path) {
        super(path, PANEL_TYPE.SETTINGS);
        this.setFocusable(false);
        this.setIconTextGap(10);
    }
    
    @Override
    public String getSettingsValue() {
        return this.isSelected() ? "true" : "false";
    }
    
    @Override
    public void setSettingsValue(final String value) {
        this.setSelected(Boolean.parseBoolean(value));
    }
    
    @Override
    public boolean isValueValid() {
        return true;
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
}
