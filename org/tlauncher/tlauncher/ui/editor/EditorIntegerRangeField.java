// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import org.tlauncher.util.Range;

public class EditorIntegerRangeField extends EditorIntegerField
{
    private final Range<Integer> range;
    
    public EditorIntegerRangeField(final String placeholder, final Range<Integer> range) {
        if (range == null) {
            throw new NullPointerException("range");
        }
        this.range = range;
        this.setPlaceholder(placeholder);
    }
    
    public EditorIntegerRangeField(final Range<Integer> range) {
        this(null, range);
        this.setPlaceholder("settings.range", range.getMinValue(), range.getMaxValue());
    }
    
    @Override
    public boolean isValueValid() {
        try {
            return this.range.fits(Integer.parseInt(this.getSettingsValue()));
        }
        catch (Exception e) {
            return false;
        }
    }
}
