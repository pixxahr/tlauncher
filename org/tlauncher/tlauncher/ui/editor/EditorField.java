// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import org.tlauncher.tlauncher.ui.block.Blockable;

public interface EditorField extends Blockable
{
    String getSettingsValue();
    
    void setSettingsValue(final String p0);
    
    boolean isValueValid();
}
