// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

public class EditorIntegerField extends EditorTextField
{
    private static final long serialVersionUID = -7930510655707946312L;
    
    public EditorIntegerField() {
    }
    
    public EditorIntegerField(final String prompt) {
        super(prompt);
    }
    
    public int getIntegerValue() {
        try {
            return Integer.parseInt(this.getSettingsValue());
        }
        catch (Exception ex) {
            return -1;
        }
    }
    
    @Override
    public boolean isValueValid() {
        try {
            Integer.parseInt(this.getSettingsValue());
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }
}
