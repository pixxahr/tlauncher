// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import org.tlauncher.tlauncher.ui.block.Blocker;
import java.awt.Color;
import org.tlauncher.util.IntegerArray;
import javax.swing.Box;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedLabel;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class EditorResolutionField extends ExtendedPanel implements EditorField
{
    private static final long serialVersionUID = -5565607141889620750L;
    private EditorIntegerField w;
    private EditorIntegerField h;
    private ExtendedLabel x;
    private final int[] defaults;
    
    public EditorResolutionField(final String promptW, final String promptH, final int[] defaults, final boolean showDefault) {
        if (defaults == null) {
            throw new NullPointerException();
        }
        if (defaults.length != 2) {
            throw new IllegalArgumentException("Illegal array size");
        }
        this.defaults = defaults;
        this.setLayout(new BoxLayout(this, 0));
        this.setPreferredSize(new Dimension(161, 21));
        (this.w = new EditorIntegerField(promptW)).setColumns(4);
        this.w.setHorizontalAlignment(0);
        this.w.setPreferredSize(new Dimension(70, 21));
        (this.h = new EditorIntegerField(promptH)).setColumns(4);
        this.h.setHorizontalAlignment(0);
        this.h.setPreferredSize(new Dimension(70, 21));
        (this.x = new ExtendedLabel("X", 0)).setFont(this.x.getFont().deriveFont(1));
        this.add(this.w);
        this.add(Box.createHorizontalStrut(6));
        this.add(this.x);
        this.add(Box.createHorizontalStrut(6));
        this.add(this.h);
    }
    
    @Override
    public String getSettingsValue() {
        return this.w.getSettingsValue() + ';' + this.h.getSettingsValue();
    }
    
    int[] getResolution() {
        try {
            final IntegerArray arr = IntegerArray.parseIntegerArray(this.getSettingsValue());
            return arr.toArray();
        }
        catch (Exception e) {
            return new int[2];
        }
    }
    
    @Override
    public boolean isValueValid() {
        final int[] size = this.getResolution();
        return size[0] >= 1 && size[1] >= 1;
    }
    
    @Override
    public void setSettingsValue(final String value) {
        String width;
        String height;
        try {
            final IntegerArray arr = IntegerArray.parseIntegerArray(value);
            width = String.valueOf(arr.get(0));
            height = String.valueOf(arr.get(1));
        }
        catch (Exception e) {
            width = "";
            height = "";
        }
        this.w.setText(width);
        this.h.setText(height);
    }
    
    @Override
    public void setBackground(final Color bg) {
        if (this.w != null) {
            this.w.setBackground(bg);
        }
        if (this.h != null) {
            this.h.setBackground(bg);
        }
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.blockComponents(reason, this.w, this.h);
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(Blocker.UNIVERSAL_UNBLOCK, this.w, this.h);
    }
}
