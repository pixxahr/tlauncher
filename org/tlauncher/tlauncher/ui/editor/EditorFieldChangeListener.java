// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

public abstract class EditorFieldChangeListener extends EditorFieldListener
{
    @Override
    protected void onChange(final EditorHandler handler, final String oldValue, final String newValue) {
        if (newValue == null && oldValue == null) {
            return;
        }
        if (newValue != null && newValue.equals(oldValue)) {
            return;
        }
        this.onChange(oldValue, newValue);
    }
    
    public abstract void onChange(final String p0, final String p1);
}
