// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.util.Collection;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import org.tlauncher.tlauncher.ui.swing.ScrollPane;
import javax.swing.Box;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.Del;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.awt.Insets;
import org.tlauncher.tlauncher.ui.center.CenterPanelTheme;
import java.util.List;
import org.tlauncher.tlauncher.ui.swing.extended.TabbedPane;
import org.tlauncher.tlauncher.ui.swing.extended.BorderPanel;

public class TabbedEditorPanel extends AbstractEditorPanel
{
    protected final BorderPanel container;
    protected final TabbedPane tabPane;
    protected final List<EditorPanelTab> tabs;
    
    public TabbedEditorPanel(final CenterPanelTheme theme, final Insets insets) {
        super(theme, insets);
        this.tabs = new ArrayList<EditorPanelTab>();
        this.tabPane = new TabbedPane();
        if (this.tabPane.getExtendedUI() != null) {
            this.tabPane.getExtendedUI().setTheme(this.getTheme());
        }
        (this.container = new BorderPanel()).setNorth(this.messagePanel);
        this.container.setCenter(this.tabPane);
        this.setLayout(new BorderLayout());
        super.add(this.container, "Center");
    }
    
    public TabbedEditorPanel(final CenterPanelTheme theme) {
        this(theme, null);
    }
    
    public TabbedEditorPanel(final Insets insets) {
        this(null, insets);
    }
    
    public TabbedEditorPanel() {
        this(TabbedEditorPanel.smallSquareNoTopInsets);
    }
    
    protected void add(final EditorPanelTab tab) {
        if (tab == null) {
            throw new NullPointerException("tab is null");
        }
        this.tabPane.addTab(tab.getTabName(), tab.getTabIcon(), tab.getScroll(), tab.getTabTip());
        this.tabs.add(tab);
    }
    
    protected void remove(final EditorPanelTab tab) {
        if (tab == null) {
            throw new NullPointerException("tab is null");
        }
        final int index = this.tabs.indexOf(tab);
        if (index != -1) {
            this.tabPane.removeTabAt(index);
            this.tabs.remove(index);
        }
    }
    
    protected int getTabOf(final EditorPair pair) {
        return this.tabPane.indexOfComponent(pair.getPanel());
    }
    
    @Override
    protected Del del(final int aligment) {
        Color border;
        try {
            border = this.tabPane.getExtendedUI().getTheme().getBorder();
        }
        catch (Exception e) {
            border = this.getTheme().getBorder();
        }
        return new Del(1, aligment, border);
    }
    
    public static void main(final String[] args) {
        final JFrame f = new JFrame();
        f.setSize(300, 500);
        final JPanel p = new JPanel();
        p.setLayout(new GridBagLayout());
        final GridBagConstraints c = new GridBagConstraints();
        c.fill = 2;
        String test = "";
        for (int i = 0; i < 15; ++i) {
            test += i;
            c.gridx = 0;
            c.gridy = i;
            p.add(new JButton(test), c);
            c.gridx = 1;
            p.add(Box.createHorizontalStrut(0), c);
            c.gridx = 2;
            p.add(new JButton(test), c);
        }
        f.add(p);
        f.setDefaultCloseOperation(3);
        f.setVisible(true);
    }
    
    public class EditorScrollPane extends ScrollPane
    {
        private final EditorPanelTab tab;
        
        EditorScrollPane(final EditorPanelTab tab) {
            super(tab);
            this.tab = tab;
        }
        
        public EditorPanelTab getTab() {
            return this.tab;
        }
    }
    
    public class EditorPanelTab extends ExtendedPanel implements LocalizableComponent
    {
        private final String name;
        private final String tip;
        private final Icon icon;
        private final List<ExtendedPanel> panels;
        private final EditorScrollPane scroll;
        private boolean savingEnabled;
        private final GridBagConstraints c;
        
        public EditorPanelTab(final String name, final String tip, final Icon icon) {
            this.savingEnabled = true;
            this.c = new GridBagConstraints();
            this.c.fill = 2;
            this.c.gridy = 0;
            if (name == null) {
                throw new NullPointerException();
            }
            if (name.isEmpty()) {
                throw new IllegalArgumentException("name is empty");
            }
            this.name = name;
            this.tip = tip;
            this.icon = icon;
            this.panels = new ArrayList<ExtendedPanel>();
            this.setLayout(new GridBagLayout());
            this.scroll = new EditorScrollPane(this);
        }
        
        public EditorPanelTab(final TabbedEditorPanel this$0, final String name) {
            this(this$0, name, null, null);
        }
        
        public String getTabName() {
            return Localizable.get(this.name);
        }
        
        public Icon getTabIcon() {
            return this.icon;
        }
        
        public String getTabTip() {
            return Localizable.get(this.tip);
        }
        
        public EditorScrollPane getScroll() {
            return this.scroll;
        }
        
        public boolean getSavingEnabled() {
            return this.savingEnabled;
        }
        
        public void setSavingEnabled(final boolean b) {
            this.savingEnabled = b;
        }
        
        public void add(final EditorPair pair) {
            final LocalizableLabel label = pair.getLabel();
            final ExtendedPanel field = pair.getPanel();
            this.c.gridx = 0;
            this.c.weightx = 0.0;
            this.add(label, this.c);
            final GridBagConstraints c = this.c;
            ++c.gridx;
            final GridBagConstraints c2 = this.c;
            ++c2.gridx;
            this.add(Box.createHorizontalStrut(50), this.c);
            final GridBagConstraints c3 = this.c;
            ++c3.gridx;
            this.c.weightx = 1.0;
            this.add(field, this.c);
            final GridBagConstraints c4 = this.c;
            ++c4.gridy;
            this.add(Box.createVerticalStrut(20), this.c);
            final GridBagConstraints c5 = this.c;
            ++c5.gridy;
            this.panels.add(field);
            TabbedEditorPanel.this.handlers.addAll(pair.getHandlers());
        }
        
        public void nextPane() {
        }
        
        @Override
        public void updateLocale() {
            final int index = TabbedEditorPanel.this.tabPane.indexOfComponent(this.scroll);
            if (index == -1) {
                throw new RuntimeException("Cannot find scroll component in tabPane for tab: " + this.name);
            }
            TabbedEditorPanel.this.tabPane.setTitleAt(index, this.getTabName());
            TabbedEditorPanel.this.tabPane.setToolTipTextAt(index, this.getTabTip());
        }
        
        public void addButtons(final ExtendedPanel buttonPanel) {
            this.c.gridwidth = 4;
            this.c.gridx = 0;
            this.add(buttonPanel, this.c);
        }
        
        public void addVerticalGap(final int i) {
            this.add(Box.createVerticalStrut(i), this.c);
            final GridBagConstraints c = this.c;
            ++c.gridy;
        }
    }
}
