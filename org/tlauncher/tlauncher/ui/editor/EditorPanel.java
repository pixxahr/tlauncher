// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.util.Collection;
import java.awt.GridBagLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.util.List;
import org.tlauncher.tlauncher.ui.swing.ScrollPane;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class EditorPanel extends AbstractEditorPanel
{
    private static final long serialVersionUID = 3428243378644563729L;
    protected final ExtendedPanel container;
    protected final ScrollPane scroll;
    private final List<ExtendedPanel> panels;
    private final List<GridBagConstraints> constraints;
    protected final List<EditorHandler> handlers;
    private byte paneNum;
    private byte rowNum;
    
    public EditorPanel(final Insets insets) {
        super(insets);
        (this.container = new ExtendedPanel()).setLayout(new BoxLayout(this.container, 3));
        this.panels = new ArrayList<ExtendedPanel>();
        this.constraints = new ArrayList<GridBagConstraints>();
        this.handlers = new ArrayList<EditorHandler>();
        this.scroll = new ScrollPane(this.container);
        this.add(this.messagePanel, this.scroll);
    }
    
    public EditorPanel() {
        this(EditorPanel.smallSquareNoTopInsets);
    }
    
    protected void add(final EditorPair pair) {
        final LocalizableLabel label = pair.getLabel();
        final ExtendedPanel field = pair.getPanel();
        ExtendedPanel panel;
        GridBagConstraints c;
        if (this.paneNum == this.panels.size()) {
            panel = new ExtendedPanel(new GridBagLayout());
            panel.getInsets().set(0, 0, 0, 0);
            c = new GridBagConstraints();
            c.fill = 2;
            this.container.add(panel, this.del(0));
            this.panels.add(panel);
            this.constraints.add(c);
        }
        else {
            panel = this.panels.get(this.paneNum);
            c = this.constraints.get(this.paneNum);
        }
        c.anchor = 17;
        c.gridy = this.rowNum;
        c.gridx = 0;
        c.weightx = 0.1;
        panel.add(label, c);
        c.anchor = 13;
        final GridBagConstraints gridBagConstraints = c;
        final byte rowNum = this.rowNum;
        this.rowNum = (byte)(rowNum + 1);
        gridBagConstraints.gridy = rowNum;
        c.gridx = 1;
        c.weightx = 1.0;
        panel.add(field, c);
        this.handlers.addAll(pair.getHandlers());
    }
    
    protected void nextPane() {
        this.rowNum = 0;
        ++this.paneNum;
    }
}
