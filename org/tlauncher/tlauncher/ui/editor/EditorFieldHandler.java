// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import org.tlauncher.tlauncher.ui.block.Blocker;
import java.awt.Container;
import java.awt.Component;
import java.awt.event.FocusListener;
import javax.swing.JComponent;

public class EditorFieldHandler extends EditorHandler
{
    private final EditorField field;
    private final JComponent comp;
    
    public EditorFieldHandler(final String path, final JComponent component, final FocusListener focus) {
        super(path);
        if (component == null) {
            throw new NullPointerException("comp");
        }
        if (!(component instanceof EditorField)) {
            throw new IllegalArgumentException();
        }
        if (focus != null) {
            this.addFocus(component, focus);
        }
        this.comp = component;
        this.field = (EditorField)component;
    }
    
    public EditorFieldHandler(final String path, final JComponent comp) {
        this(path, comp, null);
    }
    
    @Override
    public JComponent getComponent() {
        return this.comp;
    }
    
    @Override
    public String getValue() {
        return this.field.getSettingsValue();
    }
    
    @Override
    protected void setValue0(final String s) {
        this.field.setSettingsValue(s);
    }
    
    @Override
    public boolean isValid() {
        return this.field.isValueValid();
    }
    
    private void addFocus(final Component comp, final FocusListener focus) {
        comp.addFocusListener(focus);
        if (comp instanceof Container) {
            for (final Component curComp : ((Container)comp).getComponents()) {
                this.addFocus(curComp, focus);
            }
        }
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.blockComponents(reason, this.getComponent());
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(reason, this.getComponent());
    }
}
