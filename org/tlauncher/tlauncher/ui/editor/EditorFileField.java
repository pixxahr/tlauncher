// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import java.awt.event.ActionEvent;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.block.Blocker;
import java.awt.Color;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.io.File;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import java.util.regex.Pattern;
import org.tlauncher.tlauncher.ui.explorer.FileChooser;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;
import org.tlauncher.tlauncher.ui.swing.extended.BorderPanel;

public class EditorFileField extends BorderPanel implements EditorField
{
    private static final long serialVersionUID = 5136327098130653756L;
    public static final char DEFAULT_DELIMITER = ';';
    private final EditorTextField textField;
    private final LocalizableButton explorerButton;
    private final FileChooser explorer;
    private final char delimiterChar;
    private final Pattern delimiterSplitter;
    
    public EditorFileField(final String prompt, final boolean canBeEmpty, final String button, final FileChooser chooser, final char delimiter) {
        super(10, 0);
        if (chooser == null) {
            throw new NullPointerException("FileExplorer should be defined!");
        }
        this.textField = new EditorTextField(prompt, canBeEmpty);
        this.explorerButton = new UpdaterButton(UpdaterButton.GRAY_COLOR, button);
        this.explorer = chooser;
        this.delimiterChar = delimiter;
        this.delimiterSplitter = Pattern.compile(String.valueOf(this.delimiterChar), 16);
        final int result;
        this.explorerButton.addActionListener(e -> {
            this.explorerButton.setEnabled(false);
            this.explorer.setCurrentDirectory(this.getFirstFile());
            result = this.explorer.showDialog(this);
            if (result == 0) {
                this.setRawValue(this.explorer.getSelectedFiles());
            }
            this.explorerButton.setEnabled(true);
            return;
        });
        this.add(this.textField, "Center");
        this.add(this.explorerButton, "East");
    }
    
    public EditorFileField(final String prompt, final boolean canBeEmpty, final FileChooser chooser) {
        this(prompt, canBeEmpty, "explorer.browse", chooser, ';');
    }
    
    public EditorFileField(final String prompt, final FileChooser chooser) {
        this(prompt, false, chooser);
    }
    
    @Override
    public String getSettingsValue() {
        return this.getValueFromRaw(this.getRawValues());
    }
    
    private File[] getRawValues() {
        final String[] paths = this.getRawSplitValue();
        if (paths == null) {
            return null;
        }
        final int len = paths.length;
        final File[] files = new File[len];
        for (int i = 0; i < paths.length; ++i) {
            files[i] = new File(paths[i]);
        }
        return files;
    }
    
    @Override
    public void setSettingsValue(final String value) {
        this.textField.setSettingsValue(value);
    }
    
    private void setRawValue(final File[] fileList) {
        this.setSettingsValue(this.getValueFromRaw(fileList));
    }
    
    private String[] getRawSplitValue() {
        return this.splitString(this.textField.getValue());
    }
    
    private String getValueFromRaw(final File[] files) {
        if (files == null) {
            return null;
        }
        final StringBuilder builder = new StringBuilder();
        for (final File file : files) {
            final String path = file.getAbsolutePath();
            builder.append(this.delimiterChar).append(path);
        }
        return builder.substring(1);
    }
    
    private String[] splitString(final String s) {
        if (s == null) {
            return null;
        }
        final String[] split = this.delimiterSplitter.split(s);
        if (split.length == 0) {
            return null;
        }
        return split;
    }
    
    private File getFirstFile() {
        final File[] files = this.getRawValues();
        if (files == null || files.length == 0) {
            return TLauncher.getDirectory();
        }
        return files[0];
    }
    
    @Override
    public boolean isValueValid() {
        return this.textField.isValueValid();
    }
    
    @Override
    public void setBackground(final Color bg) {
        if (this.textField != null) {
            this.textField.setBackground(bg);
        }
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.blockComponents(reason, this.textField, this.explorerButton);
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(Blocker.UNIVERSAL_UNBLOCK, this.textField, this.explorerButton);
    }
    
    protected void log(final Object... w) {
        U.log("[" + this.getClass().getSimpleName() + "]", w);
    }
}
