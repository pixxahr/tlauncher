// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.editor;

import javax.swing.JLabel;
import org.tlauncher.tlauncher.ui.images.ImageIcon;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import javax.swing.JComponent;
import java.awt.Color;
import java.util.Iterator;
import java.util.ArrayList;
import java.awt.Insets;
import org.tlauncher.tlauncher.ui.center.CenterPanelTheme;
import java.util.List;
import org.tlauncher.tlauncher.ui.center.CenterPanel;

public abstract class AbstractEditorPanel extends CenterPanel
{
    protected final List<EditorHandler> handlers;
    
    public AbstractEditorPanel(final CenterPanelTheme theme, final Insets insets) {
        super(theme, insets);
        this.handlers = new ArrayList<EditorHandler>();
    }
    
    public AbstractEditorPanel(final Insets insets) {
        this(null, insets);
    }
    
    public AbstractEditorPanel() {
        this(null, null);
    }
    
    protected boolean checkValues() {
        boolean allValid = true;
        for (final EditorHandler handler : this.handlers) {
            final boolean valid = handler.isValid();
            this.setValid(handler, valid);
            if (!valid) {
                allValid = false;
            }
        }
        return allValid;
    }
    
    protected void setValid(final EditorHandler handler, final boolean valid) {
        final Color color = valid ? this.getTheme().getBackground() : this.getTheme().getFailure();
        handler.getComponent().setOpaque(!valid);
        handler.getComponent().setBackground(color);
    }
    
    protected JComponent createTip(final String label, final boolean warning) {
        final LocalizableLabel tip = new LocalizableLabel(label);
        if (warning) {
            ImageIcon.setup((JLabel)tip, ImageCache.getIcon("warning.png", 16, 16));
        }
        return tip;
    }
}
