// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import java.awt.FlowLayout;
import java.util.Iterator;
import java.util.List;
import org.tlauncher.tlauncher.managers.VersionLists;
import javax.swing.Box;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.editor.EditorField;
import net.minecraft.launcher.versions.ReleaseType;
import org.tlauncher.tlauncher.ui.editor.EditorFieldChangeListener;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.ui.editor.EditorTextField;
import java.util.ArrayList;
import org.tlauncher.tlauncher.ui.editor.EditorCheckBox;
import org.tlauncher.tlauncher.ui.editor.EditorResolutionField;
import java.io.IOException;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.FileUtil;
import java.io.File;
import org.tlauncher.tlauncher.ui.editor.EditorFileField;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.explorer.FileChooser;
import javax.swing.SpringLayout;

public class MinecraftSettings extends PageSettings
{
    public static final String MINECRAFT_SETTING_RAM = "minecraft.memory.ram2";
    SettingsMemorySlider slider;
    
    public MinecraftSettings() {
        this.setOpaque(false);
        final SpringLayout springLayout = new SpringLayout();
        final FileChooser fileChooser = (FileChooser)TLauncher.getInjector().getInstance((Class)FileChooser.class);
        fileChooser.setFileSelectionMode(1);
        final EditorFileField editorFileField = new EditorFileField("settings.client.gamedir.prompt", fileChooser) {
            @Override
            public boolean isValueValid() {
                try {
                    final File f = new File(this.getSettingsValue(), "testChooserFolder");
                    FileUtil.createFolder(f);
                    FileUtil.deleteDirectory(f);
                }
                catch (IOException e) {
                    Alert.showLocError("settings.client.gamedir.noaccess", e);
                    return false;
                }
                return super.isValueValid();
            }
        };
        final EditorResolutionField editorResolutionField = new EditorResolutionField("settings.client.resolution.width", "settings.client.resolution.height", this.global.getDefaultClientWindowSize(), false);
        final EditorCheckBox box = new EditorCheckBox("settings.client.resolution.fullscreen");
        final List<EditorCheckBox> versions = new ArrayList<EditorCheckBox>();
        final List<HandlerSettings> settings = new ArrayList<HandlerSettings>();
        final EditorTextField jvmArguments = new EditorTextField("settings.java.args.jvm", true);
        final EditorTextField minecraftArguments = new EditorTextField("settings.java.args.minecraft", true);
        final FileChooser chooser = (FileChooser)TLauncher.getInjector().getInstance((Class)FileChooser.class);
        final JavaPathField javaPathField = new JavaPathField(OS.is(OS.WINDOWS) ? "settings.java.path.prompt.win" : "settings.java.path.prompt.other", true, chooser);
        this.slider = new SettingsMemorySlider();
        final EditorFieldChangeListener changeListener = new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldValue, final String newValue) {
                TLauncher.getInstance().getVersionManager().updateVersionList();
            }
        };
        for (final ReleaseType releaseType : ReleaseType.getDefinable()) {
            final EditorCheckBox editorCheckBox = new EditorCheckBox("settings.versions." + releaseType);
            final HandlerSettings handlerSettings = new HandlerSettings("minecraft.versions." + releaseType, editorCheckBox, changeListener);
            this.addHandler(handlerSettings);
            settings.add(handlerSettings);
            versions.add(editorCheckBox);
        }
        final EditorCheckBox oldRelease = new EditorCheckBox("settings.versions.sub." + ReleaseType.SubType.OLD_RELEASE);
        final HandlerSettings handlerSettings2 = new HandlerSettings("minecraft.versions.sub." + ReleaseType.SubType.OLD_RELEASE, oldRelease, changeListener);
        this.addHandler(handlerSettings2);
        settings.add(handlerSettings2);
        versions.add(2, oldRelease);
        final SettingElement directorySettings = new SettingElement("settings.client.gamedir.label", editorFileField, 31);
        final SettingElement resolution = new SettingElement("settings.client.resolution.label", this.doublePanel(editorResolutionField, 16, box), 21);
        final SettingElement versionList = new SettingElement("settings.versions.label", this.createBoxes(versions), 121);
        final SettingElement javaPath = new SettingElement("settings.java.path.label", javaPathField, 31);
        final SettingElement memory = new SettingElement("settings.java.memory.label", this.slider, 84);
        final ExtendedPanel argPanel = new ExtendedPanel();
        argPanel.setLayout(new BoxLayout(argPanel, 1));
        argPanel.add(jvmArguments);
        argPanel.add(Box.createVerticalStrut(9));
        argPanel.add(minecraftArguments);
        final SettingElement arguments = new SettingElement("settings.java.args.label", argPanel, 84);
        this.setLayout(springLayout);
        springLayout.putConstraint("North", directorySettings, 0, "North", this);
        springLayout.putConstraint("West", directorySettings, 0, "West", this);
        springLayout.putConstraint("South", directorySettings, 27, "North", this);
        springLayout.putConstraint("East", directorySettings, 0, "East", this);
        this.add(directorySettings);
        springLayout.putConstraint("North", resolution, 17, "South", directorySettings);
        springLayout.putConstraint("West", resolution, 0, "West", this);
        springLayout.putConstraint("South", resolution, 43, "South", directorySettings);
        springLayout.putConstraint("East", resolution, 0, "East", directorySettings);
        this.add(resolution);
        springLayout.putConstraint("North", versionList, 6, "South", resolution);
        springLayout.putConstraint("West", versionList, 0, "West", this);
        springLayout.putConstraint("South", versionList, 138, "South", resolution);
        springLayout.putConstraint("East", versionList, 0, "East", directorySettings);
        this.add(versionList);
        int gap = 0;
        if (!OS.is(OS.WINDOWS)) {
            gap = 10;
        }
        springLayout.putConstraint("North", arguments, 11, "South", versionList);
        springLayout.putConstraint("West", arguments, 0, "West", this);
        springLayout.putConstraint("South", arguments, 66 + gap, "South", versionList);
        springLayout.putConstraint("East", arguments, 0, "East", directorySettings);
        this.add(arguments);
        springLayout.putConstraint("West", javaPath, 0, "West", directorySettings);
        springLayout.putConstraint("East", javaPath, 0, "East", directorySettings);
        springLayout.putConstraint("North", javaPath, 19 - gap, "South", arguments);
        springLayout.putConstraint("South", javaPath, 40, "South", arguments);
        this.add(javaPath);
        springLayout.putConstraint("North", memory, 19, "South", javaPath);
        springLayout.putConstraint("South", memory, 114, "South", javaPath);
        springLayout.putConstraint("West", memory, 0, "West", directorySettings);
        springLayout.putConstraint("East", memory, 0, "East", directorySettings);
        this.add(memory);
        final EditorFieldChangeListener editorFieldChangeListener = new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldValue, final String newValue) {
                if (!MinecraftSettings.this.tlauncher.isReady()) {
                    return;
                }
                try {
                    MinecraftSettings.this.tlauncher.getManager().getComponent(VersionLists.class).updateLocal();
                }
                catch (IOException e) {
                    Alert.showLocError("settings.client.gamedir.noaccess", e);
                    return;
                }
                MinecraftSettings.this.tlauncher.getVersionManager().asyncRefresh();
                MinecraftSettings.this.tlauncher.getProfileManager().recreate();
            }
        };
        this.addHandler(new HandlerSettings("minecraft.gamedir", editorFileField, editorFieldChangeListener));
        this.addHandler(new HandlerSettings("minecraft.size", editorResolutionField));
        this.addHandler(new HandlerSettings("minecraft.fullscreen", box));
        this.addHandler(new HandlerSettings("minecraft.javaargs", jvmArguments));
        this.addHandler(new HandlerSettings("minecraft.args", minecraftArguments));
        this.addHandler(new HandlerSettings("minecraft.javadir", javaPathField));
        this.addHandler(new HandlerSettings("minecraft.memory.ram2", this.slider));
    }
    
    private ExtendedPanel doublePanel(final JComponent com1, final int gap, final JComponent comp2) {
        final ExtendedPanel extendedPanel = new ExtendedPanel(new FlowLayout(0, 0, 0));
        extendedPanel.add(com1);
        extendedPanel.add(Box.createHorizontalStrut(gap));
        extendedPanel.add(comp2);
        return extendedPanel;
    }
    
    private ExtendedPanel createBoxes(final List<EditorCheckBox> list) {
        final ExtendedPanel extendedPanel = new ExtendedPanel();
        extendedPanel.setLayout(new BoxLayout(extendedPanel, 1));
        for (final EditorCheckBox box : list) {
            extendedPanel.add(box);
        }
        return extendedPanel;
    }
    
    @Override
    public void init() {
        this.slider.initMemoryQuestion();
        super.init();
    }
    
    class JavaPathField extends EditorFileField
    {
        public JavaPathField(final String prompt, final boolean canBeEmpty, final FileChooser chooser) {
            super(prompt, canBeEmpty, chooser);
        }
        
        @Override
        public boolean isValueValid() {
            if (this.checkPath()) {
                return true;
            }
            Alert.showLocError("settings.java.path.doesnotexist");
            return false;
        }
        
        private boolean checkPath() {
            final String path = this.getSettingsValue();
            if (path == null) {
                return true;
            }
            if (!path.endsWith("java") && !path.endsWith("java.exe") && !path.endsWith("javaw.exe")) {
                return false;
            }
            final File javaDir = new File(path);
            return javaDir.isFile();
        }
    }
}
