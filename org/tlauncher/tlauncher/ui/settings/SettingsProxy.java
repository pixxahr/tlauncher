// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import org.tlauncher.tlauncher.ui.swing.extended.BorderPanel;
import org.apache.commons.lang3.StringUtils;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import javax.swing.JRadioButton;
import java.awt.Container;
import org.tlauncher.util.U;
import java.util.Iterator;
import java.util.List;
import org.tlauncher.tlauncher.ui.editor.EditorIntegerRangeField;
import org.tlauncher.util.Range;
import java.util.Map;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.block.Blocker;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.AbstractButton;
import java.util.Arrays;
import org.tlauncher.tlauncher.ui.editor.EditorTextField;
import javax.swing.ButtonGroup;
import java.net.Proxy;
import java.util.LinkedHashMap;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import org.tlauncher.tlauncher.ui.editor.EditorField;
import org.tlauncher.tlauncher.ui.swing.extended.VPanel;

public class SettingsProxy extends VPanel implements EditorField
{
    private static final String path = "settings.connection.proxy";
    private static final String block = "proxyselect";
    private final ExtendedPanel proxyTypePanel;
    private final LinkedHashMap<Proxy.Type, ProxyLocRadio> typeMap;
    private final ButtonGroup group;
    private final ProxySettingsPanel proxySettingsPanel;
    private final EditorTextField addressField;
    private final EditorTextField portField;
    
    SettingsProxy() {
        this.typeMap = new LinkedHashMap<Proxy.Type, ProxyLocRadio>();
        this.group = new ButtonGroup();
        this.setAlignmentX(0.0f);
        final List<Proxy.Type> typeList = Arrays.asList(Proxy.Type.values());
        for (final Proxy.Type type : typeList) {
            final ProxyLocRadio radio = new ProxyLocRadio();
            radio.setText(type.name().toLowerCase());
            radio.setAlignmentX(0.0f);
            radio.setOpaque(false);
            this.group.add(radio);
            this.typeMap.put(type, radio);
        }
        this.typeMap.get(Proxy.Type.DIRECT).addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                final boolean selected = e.getStateChange() == 1;
                if (selected) {
                    Blocker.block(SettingsProxy.this.proxySettingsPanel, "proxyselect");
                }
            }
        });
        (this.proxyTypePanel = new ExtendedPanel()).setAlignmentX(0.0f);
        this.add(this.proxyTypePanel);
        final ItemListener listener = new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                final boolean selected = e.getStateChange() == 1;
                if (selected) {
                    Blocker.unblock(SettingsProxy.this.proxySettingsPanel, "proxyselect");
                }
            }
        };
        for (final Map.Entry<Proxy.Type, ProxyLocRadio> en : this.typeMap.entrySet()) {
            this.proxyTypePanel.add(en.getValue());
            if (en.getKey() == Proxy.Type.DIRECT) {
                continue;
            }
            en.getValue().addItemListener(listener);
        }
        (this.proxySettingsPanel = new ProxySettingsPanel()).setAlignmentX(0.0f);
        this.add(this.proxySettingsPanel);
        this.addressField = new EditorTextField("settings.connection.proxy.address", false);
        this.proxySettingsPanel.setCenter(this.addressField);
        (this.portField = new EditorIntegerRangeField("settings.connection.proxy.port", new Range<Integer>(0, 65535))).setColumns(5);
        this.proxySettingsPanel.setEast(this.portField);
    }
    
    private Map.Entry<Proxy.Type, ProxyLocRadio> getSelectedType() {
        for (final Map.Entry<Proxy.Type, ProxyLocRadio> en : this.typeMap.entrySet()) {
            if (en.getValue().isSelected()) {
                return en;
            }
        }
        return null;
    }
    
    private void setSelectedType(final Proxy.Type type) {
        for (final Map.Entry<Proxy.Type, ProxyLocRadio> en : this.typeMap.entrySet()) {
            if (en.getKey() == type) {
                en.getValue().setSelected(true);
                return;
            }
        }
        this.typeMap.get(Proxy.Type.DIRECT).setSelected(true);
    }
    
    @Override
    public String getSettingsValue() {
        final Map.Entry<Proxy.Type, ProxyLocRadio> selected = this.getSelectedType();
        if (selected == null || selected.getKey() == Proxy.Type.DIRECT) {
            U.log("selected is", selected, "so null");
            return null;
        }
        U.log(selected.getKey().name().toLowerCase() + ';' + this.addressField.getValue() + ';' + this.portField.getValue());
        return selected.getKey().name().toLowerCase() + ';' + this.addressField.getValue() + ';' + this.portField.getValue();
    }
    
    @Override
    public void setSettingsValue(final String value) {
    }
    
    @Override
    public boolean isValueValid() {
        final Map.Entry<Proxy.Type, ProxyLocRadio> selected = this.getSelectedType();
        return selected == null || selected.getKey() == Proxy.Type.DIRECT || (this.addressField.isValueValid() && this.portField.isValueValid());
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.blockComponents(this, reason);
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(this, reason);
    }
    
    private class ProxyLocRadio extends JRadioButton implements LocalizableComponent
    {
        private String currentType;
        
        @Override
        public void setText(final String proxyType) {
            this.currentType = proxyType;
            String text = Localizable.get("settings.connection.proxy.type." + proxyType);
            if (StringUtils.isBlank((CharSequence)text)) {
                text = proxyType;
            }
            super.setText(text);
        }
        
        @Override
        public void updateLocale() {
            this.setText(this.currentType);
        }
    }
    
    private class ProxySettingsPanel extends BorderPanel implements Blockable
    {
        @Override
        public void block(final Object reason) {
            Blocker.blockComponents(this, reason);
        }
        
        @Override
        public void unblock(final Object reason) {
            Blocker.unblockComponents(this, reason);
        }
    }
}
