// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import java.awt.event.ActionEvent;
import net.minecraft.launcher.process.JavaProcessLauncher;
import java.util.List;
import org.tlauncher.util.U;
import org.tlauncher.util.FileUtil;
import org.tlauncher.tlauncher.rmo.TLauncher;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import java.util.Collection;
import org.tlauncher.util.MinecraftUtil;
import org.tlauncher.tlauncher.rmo.Bootstrapper;
import java.io.File;
import java.util.ArrayList;
import org.tlauncher.tlauncher.ui.loc.LocalizableCheckbox;
import javax.swing.JLabel;
import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.editor.EditorCheckBox;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class ResetView extends JPanel
{
    private JButton resetAgain;
    
    public ResetView() {
        final SpringLayout spring = new SpringLayout();
        this.setLayout(spring);
        this.setPreferredSize(new Dimension(420, 280));
        final JLabel label = new LocalizableLabel("settings.reset.message");
        final JLabel mapsNotRemove = new LocalizableLabel("settings.reset.not.remove.maps");
        mapsNotRemove.setHorizontalAlignment(0);
        mapsNotRemove.setForeground(new Color(255, 58, 66));
        SwingUtil.setFontSize(this.resetAgain = new UpdaterButton(new Color(222, 64, 43), new Color(222, 31, 8), Color.WHITE, "settings.reset.button.start"), 13.0f, 1);
        final LocalizableCheckbox tlSettings = new EditorCheckBox("settings.reset.remove.tlauncher.settings");
        tlSettings.setIconTextGap(15);
        tlSettings.setSelected(true);
        final LocalizableCheckbox minecraft = new EditorCheckBox("settings.reset.remove.minecraft.folder");
        minecraft.setIconTextGap(15);
        minecraft.setSelected(true);
        final LocalizableCheckbox versions = new EditorCheckBox("settings.reset.remove.versions");
        versions.setIconTextGap(15);
        versions.setSelected(true);
        spring.putConstraint("North", label, 10, "North", this);
        spring.putConstraint("West", label, 10, "West", this);
        spring.putConstraint("South", label, 40, "North", this);
        spring.putConstraint("East", label, -10, "East", this);
        this.add(label);
        spring.putConstraint("North", tlSettings, 10, "South", label);
        spring.putConstraint("West", tlSettings, 10, "West", this);
        spring.putConstraint("South", tlSettings, 40, "South", label);
        spring.putConstraint("East", tlSettings, -10, "East", this);
        this.add(tlSettings);
        spring.putConstraint("North", versions, 10, "South", tlSettings);
        spring.putConstraint("West", versions, 10, "West", this);
        spring.putConstraint("South", versions, 40, "South", tlSettings);
        spring.putConstraint("East", versions, -10, "East", this);
        this.add(versions);
        spring.putConstraint("North", minecraft, 10, "South", versions);
        spring.putConstraint("West", minecraft, 10, "West", this);
        spring.putConstraint("South", minecraft, 40, "South", versions);
        spring.putConstraint("East", minecraft, -10, "East", this);
        this.add(minecraft);
        spring.putConstraint("North", mapsNotRemove, 30, "South", minecraft);
        spring.putConstraint("West", mapsNotRemove, 0, "West", this);
        spring.putConstraint("South", mapsNotRemove, 55, "South", minecraft);
        spring.putConstraint("East", mapsNotRemove, 0, "East", this);
        this.add(mapsNotRemove);
        spring.putConstraint("North", this.resetAgain, -50, "South", this);
        spring.putConstraint("West", this.resetAgain, 50, "West", this);
        spring.putConstraint("South", this.resetAgain, -10, "South", this);
        spring.putConstraint("East", this.resetAgain, -50, "East", this);
        this.add(this.resetAgain);
        final LocalizableCheckbox tlSettings2;
        final LocalizableCheckbox minecraft2;
        final LocalizableCheckbox versions2;
        this.resetAgain.addActionListener(e1 -> {
            if (Alert.showLocQuestion("settings.reset.question.confirm")) {
                AsyncThread.execute(() -> this.reset(tlSettings2, minecraft2, versions2));
            }
        });
    }
    
    public JButton getResetAgain() {
        return this.resetAgain;
    }
    
    private void reset(final LocalizableCheckbox tlSettings, final LocalizableCheckbox minecraft, final LocalizableCheckbox versions) {
        final List<File> files = new ArrayList<File>();
        final JavaProcessLauncher p = Bootstrapper.restartLauncher();
        if (versions.isSelected()) {
            final File dir2 = new File(MinecraftUtil.getWorkingDirectory(), "versions");
            if (dir2.exists()) {
                files.add(dir2);
            }
        }
        if (minecraft.isSelected()) {
            try {
                final File dir3 = MinecraftUtil.getWorkingDirectory();
                if (dir3.exists()) {
                    files.addAll(IOUtils.readLines(this.getClass().getResourceAsStream("/removedFolders.txt")).stream().map(e -> new File(dir3, e)).filter(File::exists).collect((Collector<? super Object, ?, Collection<? extends File>>)Collectors.toList()));
                }
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        if (tlSettings.isSelected()) {
            final File tlFolder = MinecraftUtil.getTLauncherFile("");
            final File[] listFiles = tlFolder.listFiles(pathname -> !pathname.toString().endsWith("jvms"));
            files.addAll(Lists.newArrayList((Object[])listFiles));
            TLauncher.getInstance().getConfiguration().clear();
            TLauncher.getInstance().getConfiguration().store();
        }
        files.forEach(f -> {
            if (f.isDirectory()) {
                FileUtil.deleteDirectory(f);
            }
            else {
                FileUtil.deleteFile(f);
            }
            return;
        });
        try {
            p.start();
            TLauncher.kill();
        }
        catch (IOException e2) {
            U.log(e2);
        }
    }
}
