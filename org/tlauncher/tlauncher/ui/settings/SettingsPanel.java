// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import org.tlauncher.tlauncher.ui.swing.extended.TabbedPane;
import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;
import javax.swing.Box;
import java.awt.GridBagConstraints;
import java.awt.LayoutManager;
import java.awt.GridBagLayout;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import org.tlauncher.tlauncher.ui.login.LoginException;
import java.awt.Container;
import org.tlauncher.tlauncher.ui.block.Blockable;
import java.util.Iterator;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JMenuItem;
import org.tlauncher.tlauncher.ui.block.Blocker;
import java.awt.Component;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.tlauncher.tlauncher.ui.converter.LocaleConverter;
import org.tlauncher.tlauncher.configuration.enums.ActionOnLaunch;
import org.tlauncher.tlauncher.ui.converter.ActionOnLaunchConverter;
import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import org.tlauncher.tlauncher.ui.converter.ConnectionQualityConverter;
import org.tlauncher.tlauncher.ui.converter.StringConverter;
import org.tlauncher.tlauncher.ui.editor.EditorComboBox;
import org.tlauncher.tlauncher.configuration.enums.ConsoleType;
import org.tlauncher.tlauncher.ui.converter.ConsoleTypeConverter;
import org.tlauncher.tlauncher.ui.center.CenterPanel;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.ui.editor.EditorTextField;
import java.util.List;
import java.util.ArrayList;
import net.minecraft.launcher.versions.ReleaseType;
import org.tlauncher.tlauncher.ui.editor.EditorCheckBox;
import org.tlauncher.tlauncher.ui.editor.EditorResolutionField;
import org.tlauncher.tlauncher.ui.editor.EditorPair;
import org.tlauncher.tlauncher.ui.editor.EditorFieldListener;
import java.io.IOException;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.managers.VersionLists;
import org.tlauncher.tlauncher.ui.editor.EditorFieldChangeListener;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.editor.EditorFileField;
import org.tlauncher.tlauncher.ui.explorer.filters.CommonFilter;
import org.tlauncher.tlauncher.ui.explorer.filters.FolderFilter;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.explorer.FileChooser;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.Insets;
import org.tlauncher.tlauncher.ui.editor.EditorHandler;
import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;
import javax.swing.JPopupMenu;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import org.tlauncher.tlauncher.ui.editor.EditorGroupHandler;
import org.tlauncher.tlauncher.ui.editor.EditorFieldHandler;
import org.tlauncher.tlauncher.ui.scenes.DefaultScene;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import org.tlauncher.tlauncher.ui.editor.TabbedEditorPanel;

public class SettingsPanel extends TabbedEditorPanel implements LoginForm.LoginProcessListener, LocalizableComponent
{
    private final DefaultScene scene;
    private final EditorPanelTab minecraftTab;
    public final EditorFieldHandler directory;
    public final EditorFieldHandler resolution;
    public final EditorFieldHandler fullscreen;
    public final EditorFieldHandler javaArgs;
    public final EditorFieldHandler mcArgs;
    public final EditorFieldHandler memory;
    public final EditorFieldHandler statistics;
    public final EditorGroupHandler versionHandler;
    private final EditorPanelTab tlauncherTab;
    public final EditorFieldHandler console;
    public final EditorFieldHandler fullCommand;
    public final EditorFieldHandler connQuality;
    public final EditorFieldHandler launchAction;
    public final EditorFieldHandler locale;
    private final ExtendedPanel minecraftButtons;
    private final ExtendedPanel tlauncherTabButtons;
    private final JPopupMenu popup;
    private final LocalizableMenuItem infoItem;
    private final LocalizableMenuItem defaultItem;
    private EditorHandler selectedHandler;
    
    public SettingsPanel(final DefaultScene sc) {
        super(SettingsPanel.tipTheme, new Insets(5, 10, 10, 10));
        this.tlauncherTabButtons = this.createButton();
        this.minecraftButtons = this.createButton();
        if (this.tabPane.getExtendedUI() != null) {
            this.tabPane.getExtendedUI().setTheme(SettingsPanel.settingsTheme);
        }
        this.scene = sc;
        final FocusListener warning = new FocusListener() {
            @Override
            public void focusGained(final FocusEvent e) {
                CenterPanel.this.setMessage("settings.warning");
            }
            
            @Override
            public void focusLost(final FocusEvent e) {
                CenterPanel.this.setMessage(null);
            }
        };
        final FocusListener restart = new FocusListener() {
            @Override
            public void focusGained(final FocusEvent e) {
                CenterPanel.this.setMessage("settings.restart");
            }
            
            @Override
            public void focusLost(final FocusEvent e) {
                CenterPanel.this.setMessage(null);
            }
        };
        (this.minecraftTab = new EditorPanelTab("settings.tab.minecraft")).setInsets(new Insets(20, 20, 0, 20));
        final FileChooser chooser = (FileChooser)TLauncher.getInjector().getInstance((Class)FileChooser.class);
        chooser.setFileFilter(new FolderFilter());
        (this.directory = new EditorFieldHandler("minecraft.gamedir", new EditorFileField("settings.client.gamedir.prompt", chooser), warning)).addListener(new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldValue, final String newValue) {
                if (!SettingsPanel.this.tlauncher.isReady()) {
                    return;
                }
                try {
                    SettingsPanel.this.tlauncher.getManager().getComponent(VersionLists.class).updateLocal();
                }
                catch (IOException e) {
                    Alert.showLocError("settings.client.gamedir.noaccess", e);
                    return;
                }
                SettingsPanel.this.tlauncher.getVersionManager().asyncRefresh();
                SettingsPanel.this.tlauncher.getProfileManager().recreate();
            }
        });
        this.minecraftTab.add(new EditorPair("settings.client.gamedir.label", new EditorHandler[] { this.directory }));
        this.resolution = new EditorFieldHandler("minecraft.size", new EditorResolutionField("settings.client.resolution.width", "settings.client.resolution.height", this.global.getDefaultClientWindowSize(), false), restart);
        this.fullscreen = new EditorFieldHandler("minecraft.fullscreen", new EditorCheckBox("settings.client.resolution.fullscreen"));
        this.minecraftTab.add(new EditorPair("settings.client.resolution.label", new EditorHandler[] { this.resolution }));
        final List<ReleaseType> releaseTypes = ReleaseType.getDefinable();
        final List<EditorFieldHandler> versions = new ArrayList<EditorFieldHandler>(releaseTypes.size());
        for (final ReleaseType releaseType : ReleaseType.getDefinable()) {
            versions.add(new EditorFieldHandler("minecraft.versions." + releaseType, new EditorCheckBox("settings.versions." + releaseType)));
        }
        versions.add(new EditorFieldHandler("minecraft.versions.sub." + ReleaseType.SubType.OLD_RELEASE, new EditorCheckBox("settings.versions.sub." + ReleaseType.SubType.OLD_RELEASE)));
        this.versionHandler = new EditorGroupHandler(versions);
        this.minecraftTab.add(new EditorPair("settings.versions.label", versions));
        this.minecraftTab.nextPane();
        this.javaArgs = new EditorFieldHandler("minecraft.javaargs", new EditorTextField("settings.java.args.jvm", true), warning);
        this.mcArgs = new EditorFieldHandler("minecraft.args", new EditorTextField("settings.java.args.minecraft", true), warning);
        this.minecraftTab.add(new EditorPair("settings.java.args.label", new EditorHandler[] { this.javaArgs, this.mcArgs }));
        final boolean isWindows = OS.WINDOWS.isCurrent();
        this.minecraftTab.nextPane();
        this.memory = new EditorFieldHandler("minecraft.memory.ram2", new SettingsMemorySlider(), warning);
        this.minecraftTab.add(new EditorPair("settings.java.memory.label", new EditorHandler[] { this.memory }));
        this.add(this.minecraftTab);
        (this.tlauncherTab = new EditorPanelTab("settings.tab.tlauncher")).setInsets(CenterPanel.ISETS_20);
        this.console = new EditorFieldHandler("gui.console", new EditorComboBox<Object>((StringConverter<?>)new ConsoleTypeConverter(), (Object[])ConsoleType.values()));
        new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldValue, final String newValue) {
            }
        };
        this.console.addListener(new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldvalue, final String newvalue) {
                if (newvalue == null) {
                    return;
                }
                switch (ConsoleType.get(newvalue)) {
                    case GLOBAL: {
                        TLauncher.getConsole().show(false);
                        break;
                    }
                    case MINECRAFT:
                    case NONE: {
                        TLauncher.getConsole().hide();
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException("Unknown console type!");
                    }
                }
            }
        });
        this.tlauncherTab.add(new EditorPair("settings.console.label", new EditorHandler[] { this.console }));
        this.fullCommand = new EditorFieldHandler("gui.console.fullcommand", new EditorCheckBox("settings.console.fullcommand"));
        this.statistics = new EditorFieldHandler("gui.statistics.checkbox", new EditorCheckBox("statistics.settings.checkbox.name"));
        this.tlauncherTab.add(new EditorPair("settings.console.fullcommand.label", new EditorHandler[] { this.fullCommand }));
        this.tlauncherTab.add(new EditorPair("statistics.settings.title", new EditorHandler[] { this.statistics }));
        this.tlauncherTab.nextPane();
        (this.connQuality = new EditorFieldHandler("connection", new EditorComboBox<Object>((StringConverter<?>)new ConnectionQualityConverter(), (Object[])ConnectionQuality.values()))).addListener(new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldValue, final String newValue) {
                SettingsPanel.this.tlauncher.getDownloader().setConfiguration(SettingsPanel.this.global.getConnectionQuality());
            }
        });
        this.tlauncherTab.add(new EditorPair("settings.connection.label", new EditorHandler[] { this.connQuality }));
        this.launchAction = new EditorFieldHandler("minecraft.onlaunch", new EditorComboBox<Object>((StringConverter<?>)new ActionOnLaunchConverter(), (Object[])ActionOnLaunch.values()));
        this.tlauncherTab.add(new EditorPair("settings.launch-action.label", new EditorHandler[] { this.launchAction }));
        this.tlauncherTab.nextPane();
        (this.locale = new EditorFieldHandler("locale", new EditorComboBox<Object>((StringConverter<?>)new LocaleConverter(), (Object[])this.global.getLocales()))).addListener(new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldvalue, final String newvalue) {
                if (SettingsPanel.this.tlauncher.getFrame() != null) {
                    SettingsPanel.this.tlauncher.getFrame().updateLocales();
                }
            }
        });
        this.tlauncherTab.add(new EditorPair("settings.lang.label", new EditorHandler[] { this.locale }));
        this.add(this.tlauncherTab);
        this.tlauncherTab.addVerticalGap(150);
        this.tlauncherTab.addButtons(this.tlauncherTabButtons);
        this.minecraftTab.addButtons(this.minecraftButtons);
        this.tabPane.addChangeListener(new ChangeListener() {
            private final String aboutBlock = "abouttab";
            
            @Override
            public void stateChanged(final ChangeEvent e) {
                if (SettingsPanel.this.tabPane.getSelectedComponent() instanceof EditorScrollPane && !((EditorScrollPane)SettingsPanel.this.tabPane.getSelectedComponent()).getTab().getSavingEnabled()) {
                    Blocker.blockComponents("abouttab", SettingsPanel.this.tlauncherTabButtons);
                    Blocker.blockComponents("abouttab", SettingsPanel.this.minecraftButtons);
                }
                else {
                    Blocker.unblockComponents("abouttab", SettingsPanel.this.tlauncherTabButtons);
                    Blocker.unblockComponents("abouttab", SettingsPanel.this.minecraftButtons);
                }
            }
        });
        this.popup = new JPopupMenu();
        (this.infoItem = new LocalizableMenuItem("settings.popup.info")).setEnabled(false);
        this.popup.add(this.infoItem);
        (this.defaultItem = new LocalizableMenuItem("settings.popup.default")).addActionListener(e -> {
            if (this.selectedHandler == null) {
                return;
            }
            else {
                this.resetValue(this.selectedHandler);
                return;
            }
        });
        this.popup.add(this.defaultItem);
        for (final EditorHandler handler : this.handlers) {
            final Component handlerComponent = handler.getComponent();
            handlerComponent.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                    if (e.getButton() != 3) {
                        return;
                    }
                    SettingsPanel.this.callPopup(e, handler);
                }
            });
        }
        this.updateValues();
        this.updateLocale();
    }
    
    void updateValues() {
        final boolean globalUnSaveable = !this.global.isSaveable();
        for (final EditorHandler handler : this.handlers) {
            final String path = handler.getPath();
            final String value = this.global.get(path);
            handler.updateValue(value);
            this.setValid(handler, true);
            if (globalUnSaveable || !this.global.isSaveable(path)) {
                Blocker.block(handler, "unsaveable");
            }
        }
    }
    
    public boolean saveValues() {
        if (!this.checkValues()) {
            return false;
        }
        for (final EditorHandler handler : this.handlers) {
            final String path = handler.getPath();
            final String value = handler.getValue();
            this.global.set(path, value, false);
            handler.onChange(value);
        }
        this.global.store();
        return true;
    }
    
    void resetValues() {
        for (final EditorHandler handler : this.handlers) {
            this.resetValue(handler);
        }
    }
    
    void resetValue(final EditorHandler handler) {
        final String path = handler.getPath();
        if (!this.global.isSaveable(path)) {
            return;
        }
        final String value = this.global.getDefault(path);
        this.log("Resetting:", handler.getClass().getSimpleName(), path, value);
        handler.setValue(value);
        this.log("Reset!");
    }
    
    boolean canReset(final EditorHandler handler) {
        final String key = handler.getPath();
        return this.global.isSaveable(key) && this.global.getDefault(handler.getPath()) != null;
    }
    
    void callPopup(final MouseEvent e, final EditorHandler handler) {
        if (this.popup.isShowing()) {
            this.popup.setVisible(false);
        }
        this.defocus();
        final int x = e.getX();
        final int y = e.getY();
        this.selectedHandler = handler;
        this.updateResetMenu();
        this.infoItem.setVariables(handler.getPath());
        this.popup.show((Component)e.getSource(), x, y);
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.blockComponents(this.minecraftTab, reason);
        this.updateResetMenu();
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(this.minecraftTab, reason);
        this.updateResetMenu();
    }
    
    private void updateResetMenu() {
        if (this.selectedHandler != null) {
            this.defaultItem.setEnabled(!Blocker.isBlocked(this.selectedHandler));
        }
    }
    
    @Override
    public void logginingIn() throws LoginException {
        if (this.checkValues()) {
            return;
        }
        this.scene.setSidePanel(DefaultScene.SidePanel.SETTINGS);
        throw new LoginException("Invalid settings!");
    }
    
    @Override
    public void loginFailed() {
    }
    
    @Override
    public void loginSucceed() {
    }
    
    @Override
    public void updateLocale() {
    }
    
    private ExtendedPanel createButton() {
        final LocalizableButton saveButton = new UpdaterButton(UpdaterButton.ORRANGE_COLOR, "settings.save");
        saveButton.setFont(saveButton.getFont().deriveFont(1));
        saveButton.addActionListener(e -> this.saveValues());
        final LocalizableButton defaultButton = new UpdaterButton(UpdaterButton.ORRANGE_COLOR, "settings.default");
        defaultButton.addActionListener(e -> {
            if (Alert.showLocQuestion("settings.default.warning")) {
                this.resetValues();
            }
            return;
        });
        final ImageUdaterButton homeButton = new ImageUdaterButton(ImageUdaterButton.GREEN_COLOR, "home.png");
        homeButton.addActionListener(e -> {
            this.updateValues();
            this.scene.setSidePanel(null);
            return;
        });
        final Dimension size = homeButton.getPreferredSize();
        if (size != null) {
            homeButton.setPreferredSize(new Dimension(size.width * 2, size.height));
        }
        final ExtendedPanel buttonPanel = new ExtendedPanel();
        buttonPanel.setLayout(new GridBagLayout());
        final GridBagConstraints c = new GridBagConstraints();
        c.fill = 2;
        c.gridy = 0;
        c.gridx = 0;
        c.weightx = 1.0;
        c.insets = new Insets(0, 0, 0, 10);
        buttonPanel.add(saveButton, c);
        final GridBagConstraints gridBagConstraints = c;
        ++gridBagConstraints.gridx;
        buttonPanel.add(defaultButton, c);
        final GridBagConstraints gridBagConstraints2 = c;
        ++gridBagConstraints2.gridx;
        buttonPanel.add(Box.createHorizontalStrut(20), c);
        final GridBagConstraints gridBagConstraints3 = c;
        ++gridBagConstraints3.gridx;
        buttonPanel.add(homeButton, c);
        return buttonPanel;
    }
}
