// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import java.awt.event.ActionEvent;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.ScrollBarUI;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.swing.scroll.VersionScrollBarUI;
import javax.swing.JScrollPane;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import org.tlauncher.tlauncher.ui.ui.TlauncherBasicComboBoxUI;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import java.awt.event.MouseListener;
import org.tlauncher.util.OS;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JPanel;
import org.tlauncher.tlauncher.ui.alert.Alert;
import javax.swing.JButton;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.editor.EditorField;
import org.tlauncher.tlauncher.ui.editor.EditorFieldChangeListener;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.JComboBox;
import org.tlauncher.tlauncher.ui.converter.LocaleConverter;
import org.tlauncher.tlauncher.configuration.enums.ActionOnLaunch;
import org.tlauncher.tlauncher.ui.converter.ActionOnLaunchConverter;
import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import org.tlauncher.tlauncher.ui.converter.ConnectionQualityConverter;
import javax.swing.JLabel;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.ui.editor.EditorCheckBox;
import org.tlauncher.tlauncher.ui.converter.StringConverter;
import org.tlauncher.tlauncher.configuration.enums.ConsoleType;
import org.tlauncher.tlauncher.ui.converter.ConsoleTypeConverter;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import java.util.Locale;
import org.tlauncher.tlauncher.ui.editor.EditorComboBox;
import org.tlauncher.tlauncher.configuration.LangConfiguration;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.rmo.TLauncher;

public class TlauncherSettings extends PageSettings
{
    public final TLauncher tlauncher;
    public final Configuration global;
    public final LangConfiguration lang;
    private EditorComboBox<Locale> local;
    
    public TlauncherSettings() {
        this.tlauncher = TLauncher.getInstance();
        this.global = this.tlauncher.getConfiguration();
        this.lang = this.tlauncher.getLang();
        final SpringLayout springLayout = new SpringLayout();
        this.setLayout(springLayout);
        final EditorComboBox<ConsoleType> consoleConverter = new EditorComboBox<ConsoleType>(new ConsoleTypeConverter(), ConsoleType.values());
        final EditorCheckBox fullCommand = new EditorCheckBox("settings.console.fullcommand");
        final EditorCheckBox statistics = new EditorCheckBox("statistics.settings.checkbox.name");
        final EditorCheckBox guard = new EditorCheckBox("settings.guard");
        final EditorCheckBox recommendedServers = new EditorCheckBox("settings.servers.recommendation");
        final JLabel questionLabel = new JLabel(ImageCache.getNativeIcon("qestion-option-panel.png"));
        final EditorComboBox<ConnectionQuality> connQuality = new EditorComboBox<ConnectionQuality>(new ConnectionQualityConverter(), ConnectionQuality.values());
        final EditorComboBox<ActionOnLaunch> launchAction = new EditorComboBox<ActionOnLaunch>(new ActionOnLaunchConverter(), ActionOnLaunch.values());
        this.local = new EditorComboBox<Locale>(new LocaleConverter(), this.global.getLocales());
        setTLauncherBasicComboBoxUI(consoleConverter);
        setTLauncherBasicComboBoxUI(connQuality);
        setTLauncherBasicComboBoxUI(launchAction);
        setTLauncherBasicComboBoxUI(this.local);
        final SettingElement settingElement = new SettingElement("settings.console.label", consoleConverter, 21);
        springLayout.putConstraint("North", settingElement, 0, "North", this);
        springLayout.putConstraint("West", settingElement, 0, "West", this);
        springLayout.putConstraint("South", settingElement, 21, "North", this);
        springLayout.putConstraint("East", settingElement, 0, "East", this);
        this.add(settingElement);
        final SettingElement settingElement_1 = new SettingElement("settings.console.fullcommand.label", fullCommand, 17, -1);
        springLayout.putConstraint("North", settingElement_1, 21, "South", settingElement);
        springLayout.putConstraint("West", settingElement_1, 0, "West", this);
        springLayout.putConstraint("South", settingElement_1, 36, "South", settingElement);
        springLayout.putConstraint("East", settingElement_1, 0, "East", this);
        this.add(settingElement_1);
        final SettingElement settingElement_2 = new SettingElement("statistics.settings.title", statistics, 17, -1);
        springLayout.putConstraint("North", settingElement_2, 19, "South", settingElement_1);
        springLayout.putConstraint("West", settingElement_2, 0, "West", this);
        springLayout.putConstraint("East", settingElement_2, 0, "East", this);
        springLayout.putConstraint("South", settingElement_2, 38, "South", settingElement_1);
        this.add(settingElement_2);
        final SettingElement settingElement_guard = new SettingElement("settings.guard.title", guard, 19, -1, questionLabel);
        questionLabel.setBounds(0, 0, 20, 19);
        springLayout.putConstraint("North", settingElement_guard, 15, "South", settingElement_2);
        springLayout.putConstraint("West", settingElement_guard, 0, "West", this);
        springLayout.putConstraint("East", settingElement_guard, 0, "East", this);
        springLayout.putConstraint("South", settingElement_guard, 38, "South", settingElement_2);
        this.add(settingElement_guard);
        final SettingElement settingElement_servers = new SettingElement("settings.servers.recommendation.title", recommendedServers, 19, -1);
        springLayout.putConstraint("North", settingElement_servers, 15, "South", settingElement_guard);
        springLayout.putConstraint("West", settingElement_servers, 0, "West", this);
        springLayout.putConstraint("East", settingElement_servers, 0, "East", this);
        this.add(settingElement_servers);
        final SettingElement settingElement_3 = new SettingElement("settings.connection.label", connQuality, 21);
        springLayout.putConstraint("North", settingElement_3, 20, "South", settingElement_servers);
        springLayout.putConstraint("West", settingElement_3, 0, "West", this);
        springLayout.putConstraint("East", settingElement_3, 0, "East", this);
        this.add(settingElement_3);
        final SettingElement settingElement_4 = new SettingElement("settings.launch-action.label", launchAction, 21);
        springLayout.putConstraint("North", settingElement_4, 19, "South", settingElement_3);
        springLayout.putConstraint("West", settingElement_4, 0, "West", this);
        springLayout.putConstraint("East", settingElement_4, 0, "East", this);
        this.add(settingElement_4);
        final SettingElement settingElement_5 = new SettingElement("settings.lang.label", this.local, 21);
        springLayout.putConstraint("North", settingElement_5, 21, "South", settingElement_4);
        springLayout.putConstraint("West", settingElement_5, 0, "West", settingElement);
        springLayout.putConstraint("East", settingElement_5, 0, "East", settingElement);
        this.add(settingElement_5);
        final JButton reset = new UpdaterButton(new Color(222, 64, 43), new Color(222, 31, 8), Color.WHITE, "settings.reset.button");
        SwingUtil.setFontSize(reset, 13.0f, 1);
        springLayout.putConstraint("North", reset, 19, "South", settingElement_5);
        springLayout.putConstraint("South", reset, 45, "South", settingElement_5);
        springLayout.putConstraint("West", reset, 0, "West", settingElement);
        springLayout.putConstraint("East", reset, 178, "West", settingElement);
        this.add(reset);
        final EditorFieldChangeListener changeListener = new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldvalue, final String newvalue) {
                if (ConsoleType.get(newvalue) == null) {
                    return;
                }
                switch (ConsoleType.get(newvalue)) {
                    case GLOBAL: {
                        TLauncher.getConsole().show(false);
                        break;
                    }
                    case MINECRAFT:
                    case NONE: {
                        TLauncher.getConsole().hide();
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException("Unknown console type!");
                    }
                }
            }
        };
        final EditorFieldChangeListener conQualityListener = new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldValue, final String newValue) {
                TlauncherSettings.this.tlauncher.getDownloader().setConfiguration(TlauncherSettings.this.global.getConnectionQuality());
            }
        };
        final EditorFieldChangeListener localeListener = new EditorFieldChangeListener() {
            @Override
            public void onChange(final String oldvalue, final String newvalue) {
                if (TlauncherSettings.this.tlauncher.getFrame() != null) {
                    TlauncherSettings.this.tlauncher.getFrame().updateLocales();
                }
            }
        };
        this.addHandler(new HandlerSettings("gui.console", consoleConverter, changeListener));
        this.addHandler(new HandlerSettings("gui.console.fullcommand", fullCommand));
        this.addHandler(new HandlerSettings("gui.statistics.checkbox", statistics));
        this.addHandler(new HandlerSettings("gui.settings.guard.checkbox", guard));
        this.addHandler(new HandlerSettings("gui.settings.servers.recommendation", recommendedServers));
        this.addHandler(new HandlerSettings("connection", connQuality, conQualityListener));
        this.addHandler(new HandlerSettings("minecraft.onlaunch", launchAction));
        this.addHandler(new HandlerSettings("locale", this.local, localeListener));
        final ResetView view;
        reset.addActionListener(e -> {
            view = new ResetView();
            Alert.showMessage(Localizable.get("settings.reset.button"), view, new JButton[] { view.getResetAgain() });
            return;
        });
        questionLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    final String url = String.format("https://tlauncher.org/%s/guard.html", TLauncher.getInstance().getConfiguration().isUSSRLocale() ? "ru" : "en");
                    OS.openLink(url);
                }
            }
        });
        final ActionListener l = e -> {
            if (!this.tlauncher.getProfileManager().hasPremium()) {
                ((JCheckBox)e.getSource()).setSelected(true);
                Alert.showHtmlMessage("", Localizable.get("account.premium.not.available"), 1, 400);
            }
            return;
        };
        recommendedServers.addActionListener(l);
        guard.addActionListener(l);
    }
    
    private static <T> void setTLauncherBasicComboBoxUI(final JComboBox<T> comboBox) {
        comboBox.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(149, 149, 149)));
        comboBox.setUI(new TlauncherBasicComboBoxUI() {
            @Override
            protected ComboPopup createPopup() {
                final BasicComboPopup basic = new BasicComboPopup(this.comboBox) {
                    @Override
                    protected JScrollPane createScroller() {
                        final VersionScrollBarUI barUI = new VersionScrollBarUI() {
                            @Override
                            protected Dimension getMinimumThumbSize() {
                                return new Dimension(10, 40);
                            }
                            
                            @Override
                            public Dimension getMaximumSize(final JComponent c) {
                                final Dimension dim = super.getMaximumSize(c);
                                dim.setSize(10.0, dim.getHeight());
                                return dim;
                            }
                            
                            @Override
                            public Dimension getPreferredSize(final JComponent c) {
                                final Dimension dim = super.getPreferredSize(c);
                                dim.setSize(13.0, dim.getHeight());
                                return dim;
                            }
                        };
                        barUI.setGapThubm(5);
                        final JScrollPane scroller = new JScrollPane(this.list, 20, 31);
                        scroller.getVerticalScrollBar().setUI(barUI);
                        return scroller;
                    }
                };
                basic.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.GRAY));
                return basic;
            }
        });
    }
    
    public boolean chooseChinaLocal() {
        return this.local.getSelectedValue().getLanguage().equals(new Locale("zh").getLanguage()) || this.global.getLocale().getLanguage().equals(new Locale("zh").getLanguage());
    }
}
