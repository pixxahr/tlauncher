// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import java.util.Iterator;
import java.util.ArrayList;
import org.tlauncher.tlauncher.ui.editor.EditorFieldChangeListener;
import java.util.List;
import org.tlauncher.tlauncher.ui.editor.EditorField;

class HandlerSettings
{
    private String key;
    private EditorField editorField;
    private List<EditorFieldChangeListener> listeners;
    
    public HandlerSettings(final String key, final EditorField editorField) {
        this.listeners = new ArrayList<EditorFieldChangeListener>();
        this.key = key;
        this.editorField = editorField;
    }
    
    public HandlerSettings(final String key, final EditorField editorField, final EditorFieldChangeListener listener) {
        this(key, editorField);
        this.listeners.add(listener);
    }
    
    public String getKey() {
        return this.key;
    }
    
    public void setKey(final String key) {
        this.key = key;
    }
    
    public EditorField getEditorField() {
        return this.editorField;
    }
    
    public void setEditorField(final EditorField editorField) {
        this.editorField = editorField;
    }
    
    public void onChange(final String oldValue, final String newValue) {
        for (final EditorFieldChangeListener editorFieldListener : this.listeners) {
            editorFieldListener.onChange(oldValue, newValue);
        }
    }
}
