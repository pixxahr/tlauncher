// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import java.awt.Container;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.Component;
import org.apache.commons.lang3.StringUtils;
import org.tlauncher.tlauncher.ui.editor.EditorField;
import java.util.Iterator;
import java.awt.Color;
import javax.swing.JComponent;
import java.util.ArrayList;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.configuration.Configuration;
import java.util.List;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public abstract class PageSettings extends ExtendedPanel implements SettingsHandlerInterface
{
    private final List<HandlerSettings> settingsList;
    protected final Configuration global;
    protected TLauncher tlauncher;
    
    public PageSettings() {
        this.settingsList = new ArrayList<HandlerSettings>();
        this.tlauncher = TLauncher.getInstance();
        this.global = this.tlauncher.getConfiguration();
    }
    
    @Override
    public boolean validateSettings() {
        for (final HandlerSettings handler : this.settingsList) {
            if (!handler.getEditorField().isValueValid()) {
                final EditorField editorField = handler.getEditorField();
                ((JComponent)editorField).setBackground(Color.PINK);
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void setValues() {
        for (final HandlerSettings handler : this.settingsList) {
            final String key = handler.getKey();
            final String oldValue = this.global.get(key);
            final String newValue = handler.getEditorField().getSettingsValue();
            if (!StringUtils.equals((CharSequence)oldValue, (CharSequence)newValue)) {
                this.global.set(key, newValue);
                handler.onChange(oldValue, newValue);
            }
        }
    }
    
    @Override
    public void setDefaultSettings() {
        for (final HandlerSettings handler : this.settingsList) {
            handler.getEditorField().setSettingsValue(this.global.getDefault(handler.getKey()));
        }
    }
    
    public void addHandler(final HandlerSettings handler) {
        this.addFocus((Component)handler.getEditorField(), new FocusListener() {
            @Override
            public void focusLost(final FocusEvent e) {
            }
            
            @Override
            public void focusGained(final FocusEvent e) {
                e.getComponent().setBackground(Color.white);
            }
        });
        this.settingsList.add(handler);
    }
    
    private void addFocus(final Component comp, final FocusListener focus) {
        comp.addFocusListener(focus);
        if (comp instanceof Container) {
            for (final Component curComp : ((Container)comp).getComponents()) {
                this.addFocus(curComp, focus);
            }
        }
    }
    
    @Override
    public void init() {
        for (final HandlerSettings handler : this.settingsList) {
            handler.getEditorField().setSettingsValue(this.global.get(handler.getKey()));
        }
    }
}
