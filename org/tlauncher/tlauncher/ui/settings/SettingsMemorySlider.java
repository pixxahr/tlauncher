// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import org.tlauncher.tlauncher.ui.block.Blocker;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.image.ImageObserver;
import javax.swing.JComponent;
import java.awt.Rectangle;
import org.tlauncher.tlauncher.ui.scenes.SettingsScene;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.JLayeredPane;
import java.awt.Dimension;
import javax.swing.plaf.SliderUI;
import org.tlauncher.tlauncher.ui.ui.SettingsSliderUI;
import org.tlauncher.util.OS;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import org.tlauncher.tlauncher.ui.editor.EditorIntegerField;
import javax.swing.JSlider;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.editor.EditorField;
import org.tlauncher.tlauncher.ui.swing.extended.BorderPanel;

public class SettingsMemorySlider extends BorderPanel implements EditorField
{
    public static final Color HINT_BACKGROUND_COLOR;
    private final JSlider slider;
    private final EditorIntegerField inputField;
    private final LocalizableLabel mb;
    private Configuration c;
    private JLabel question;
    
    SettingsMemorySlider() {
        final SpringLayout springLayout = new SpringLayout();
        this.setLayout(springLayout);
        final JPanel base = new ExtendedPanel();
        springLayout.putConstraint("North", base, 50, "North", this);
        springLayout.putConstraint("West", base, 0, "East", this);
        springLayout.putConstraint("South", base, -236, "South", this);
        springLayout.putConstraint("East", base, 0, "East", this);
        this.add(base);
        (this.slider = new JSlider()).setOpaque(false);
        this.slider.setMinimum(512);
        this.slider.setMaximum(OS.Arch.MAX_MEMORY);
        final int tick = (OS.Arch.MAX_MEMORY - 512) / 5;
        if (tick == 0) {
            this.slider.setMajorTickSpacing(1);
        }
        else {
            this.slider.setMajorTickSpacing(tick);
        }
        this.slider.setSnapToTicks(true);
        this.slider.setPaintLabels(true);
        this.slider.setUI(new SettingsSliderUI(this.slider));
        this.slider.setPreferredSize(new Dimension(336, 35));
        (this.inputField = new EditorIntegerField()).setColumns(5);
        this.mb = new LocalizableLabel("settings.java.memory.mb");
        final SpringLayout spring = new SpringLayout();
        base.setLayout(spring);
        final JLayeredPane sliderPanel = new JLayeredPane();
        spring.putConstraint("North", sliderPanel, 0, "North", base);
        spring.putConstraint("West", sliderPanel, 0, "West", base);
        spring.putConstraint("East", sliderPanel, -70, "East", base);
        spring.putConstraint("South", sliderPanel, 0, "South", base);
        base.add(sliderPanel);
        spring.putConstraint("North", this.inputField, 0, "North", base);
        spring.putConstraint("West", this.inputField, 2, "East", sliderPanel);
        spring.putConstraint("East", this.inputField, 45, "East", sliderPanel);
        base.add(this.inputField);
        spring.putConstraint("North", this.mb, 3, "North", base);
        spring.putConstraint("West", this.mb, 1, "East", this.inputField);
        spring.putConstraint("East", this.mb, 0, "East", base);
        base.add(this.mb);
        sliderPanel.add(this.slider, 0);
        this.slider.setBounds(0, 0, 336, 35);
        this.c = TLauncher.getInstance().getConfiguration();
        (this.question = new JLabel(ImageCache.getNativeIcon("qestion-option-panel.png"))).addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                Alert.showLocMessage(SettingsMemorySlider.this.c.get("memory.problem.message"));
            }
        });
        sliderPanel.add(this.question, 1);
        this.question.setBounds(330, 0, 20, 20);
        this.inputField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                SettingsMemorySlider.this.updateInfo();
            }
            
            @Override
            public void removeUpdate(final DocumentEvent e) {
            }
            
            @Override
            public void changedUpdate(final DocumentEvent e) {
            }
        });
        if (!TLauncher.getInstance().getConfiguration().getBoolean("settings.tip.close")) {
            this.addHint(base, spring);
        }
        this.slider.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(final MouseEvent e) {
                SettingsMemorySlider.this.requestFocusInWindow();
            }
        });
        this.slider.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(final MouseEvent e) {
                SettingsMemorySlider.this.onSliderUpdate();
            }
        });
        this.slider.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent e) {
                SettingsMemorySlider.this.onSliderUpdate();
            }
        });
    }
    
    @Override
    public void setVisible(final boolean aFlag) {
        super.setVisible(aFlag);
    }
    
    private void addHint(final JPanel panel_1, final SpringLayout sl_panel_1) {
        final int ARC_SIZE = 10;
        final JLabel crossTipButton = new JLabel() {
            BufferedImage image = ImageCache.getImage("close-cross.png");
            
            @Override
            protected void paintComponent(final Graphics g) {
                this.paint(g);
            }
            
            @Override
            public void paint(final Graphics g0) {
                ((Graphics2D)g0).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                final Rectangle rec = this.getVisibleRect();
                g0.setColor(SettingsScene.BACKGROUND);
                g0.fillRect(rec.x, rec.y, rec.width, rec.height);
                g0.setColor(this.getBackground());
                g0.fillRect(rec.x, rec.y, rec.width - 10, rec.height);
                g0.fillRoundRect(rec.x, rec.y, rec.width, rec.height, 10, 10);
                final JComponent component = this;
                this.paintPicture(g0, component, rec);
                ((Graphics2D)g0).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            }
            
            protected void paintPicture(final Graphics g, final JComponent c, final Rectangle rect) {
                final Graphics2D g2d = (Graphics2D)g;
                final int x = (this.getWidth() - this.image.getWidth(null)) / 2;
                final int y = (this.getHeight() - this.image.getHeight(null)) / 2;
                g2d.drawImage(this.image, x, y, null);
            }
        };
        crossTipButton.setOpaque(true);
        crossTipButton.setBackground(SettingsMemorySlider.HINT_BACKGROUND_COLOR);
        crossTipButton.setPreferredSize(new Dimension(32, 30));
        crossTipButton.setSize(new Dimension(32, 30));
        crossTipButton.setPreferredSize(new Dimension(32, 30));
        final LocalizableLabel hint = new LocalizableLabel("settings.warning");
        final ExtendedPanel extendedPanel = new ExtendedPanel(new BorderLayout(10, 0)) {
            public void paintComponent(final Graphics g0) {
                final Graphics2D g = (Graphics2D)g0;
                final int x = 0;
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g.setColor(SettingsMemorySlider.HINT_BACKGROUND_COLOR);
                g.fillRoundRect(x, x, this.getWidth(), this.getHeight(), 10, 10);
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            }
        };
        extendedPanel.setInsets(0, 5, 0, 0);
        sl_panel_1.putConstraint("East", extendedPanel, -20, "East", this.slider);
        sl_panel_1.putConstraint("North", extendedPanel, 9, "South", this.slider);
        sl_panel_1.putConstraint("South", extendedPanel, 40, "South", this.slider);
        hint.setVerticalTextPosition(0);
        hint.setHorizontalAlignment(0);
        hint.setVerticalAlignment(0);
        hint.setForeground(new Color(212, 212, 212));
        hint.setFont(hint.getFont().deriveFont(12.0f));
        extendedPanel.setOpaque(true);
        extendedPanel.add(hint, "Center");
        extendedPanel.add(crossTipButton, "East");
        panel_1.add(extendedPanel);
        crossTipButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                if (!SwingUtilities.isLeftMouseButton(e)) {
                    return;
                }
                SwingUtilities.invokeLater(() -> extendedPanel.setVisible(false));
                TLauncher.getInstance().getConfiguration().set("settings.tip.close", true);
            }
            
            @Override
            public void mouseEntered(final MouseEvent e) {
                crossTipButton.setBackground(new Color(124, 124, 124));
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                crossTipButton.setBackground(SettingsMemorySlider.HINT_BACKGROUND_COLOR);
            }
        });
        sl_panel_1.putConstraint("West", extendedPanel, 0, "West", panel_1);
        extendedPanel.setBackground(SettingsMemorySlider.HINT_BACKGROUND_COLOR);
    }
    
    @Override
    public void setBackground(final Color color) {
        if (this.inputField != null) {
            this.inputField.setBackground(color);
        }
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.blockComponents(reason, this.slider, this.inputField);
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(reason, this.slider, this.inputField);
    }
    
    @Override
    public String getSettingsValue() {
        return this.inputField.getValue();
    }
    
    @Override
    public void setSettingsValue(final String value) {
        this.inputField.setValue((Object)value);
        this.updateInfo();
    }
    
    @Override
    public boolean isValueValid() {
        return this.inputField.getIntegerValue() >= 512 && this.inputField.getIntegerValue() <= OS.Arch.MAX_MEMORY;
    }
    
    private void onSliderUpdate() {
        this.inputField.setValue(this.slider.getValue());
        this.updateTip();
    }
    
    private void updateSlider() {
        final int intVal = this.inputField.getIntegerValue();
        if (intVal > 1) {
            this.slider.setValue(intVal);
        }
    }
    
    private void updateTip() {
    }
    
    private void updateInfo() {
        this.updateSlider();
        this.updateTip();
    }
    
    public void initMemoryQuestion() {
        if (!this.c.isExist("memory.problem.message")) {
            this.question.setVisible(false);
        }
        this.question.repaint();
    }
    
    static {
        HINT_BACKGROUND_COLOR = new Color(113, 113, 113);
    }
}
