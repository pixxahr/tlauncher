// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.BorderLayout;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JComponent;
import java.awt.Font;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class SettingElement extends ExtendedPanel
{
    public static int FIRST_PART;
    public static int SECOND_PART;
    public final Font LABEL_FONT;
    
    public SettingElement(final String name, final JComponent panel, final int height) {
        this.LABEL_FONT = new JLabel().getFont().deriveFont(1, 12.0f);
        this.setLayout(new BoxLayout(this, 0));
        final LocalizableLabel label = new LocalizableLabel(name);
        label.setHorizontalAlignment(2);
        label.setVerticalAlignment(0);
        label.setVerticalAlignment(0);
        label.setVerticalTextPosition(0);
        label.setFont(this.LABEL_FONT);
        final ExtendedPanel p = new ExtendedPanel(new BorderLayout(0, 0));
        p.setPreferredSize(new Dimension(SettingElement.FIRST_PART, height));
        p.add(label);
        panel.setPreferredSize(new Dimension(SettingElement.SECOND_PART, height));
        this.add(p);
        this.add(panel);
    }
    
    public SettingElement(final String name, final JComponent panel, final int height, final int labelUpGap) {
        this.LABEL_FONT = new JLabel().getFont().deriveFont(1, 12.0f);
        this.setLayout(new BoxLayout(this, 0));
        final LocalizableLabel label = new LocalizableLabel(name);
        label.setHorizontalAlignment(2);
        label.setVerticalAlignment(0);
        label.setVerticalAlignment(0);
        label.setVerticalTextPosition(0);
        label.setFont(this.LABEL_FONT);
        final ExtendedPanel p = new ExtendedPanel(new BorderLayout(0, 0));
        p.setInsets(labelUpGap, 0, 0, 0);
        p.setPreferredSize(new Dimension(SettingElement.FIRST_PART, height));
        p.add(label);
        panel.setPreferredSize(new Dimension(SettingElement.SECOND_PART, height));
        this.add(p);
        this.add(panel);
    }
    
    public SettingElement(final String name, final JComponent elem, final int height, final int labelUpGap, final JComponent thirdElement) {
        this.LABEL_FONT = new JLabel().getFont().deriveFont(1, 12.0f);
        this.setLayout(new FlowLayout(0, 0, 0));
        final LocalizableLabel label = new LocalizableLabel(name);
        label.setHorizontalAlignment(2);
        label.setVerticalAlignment(0);
        label.setVerticalAlignment(0);
        label.setVerticalTextPosition(0);
        label.setFont(this.LABEL_FONT);
        final ExtendedPanel p = new ExtendedPanel(new BorderLayout(0, 0));
        p.setInsets(labelUpGap, 0, 0, 0);
        p.setPreferredSize(new Dimension(SettingElement.FIRST_PART, height));
        p.add(label);
        this.add(p);
        this.add(elem);
        elem.setPreferredSize(new Dimension(elem.getPreferredSize().width, height));
        this.add(thirdElement);
    }
    
    static {
        SettingElement.FIRST_PART = 162;
        SettingElement.SECOND_PART = 420;
    }
}
