// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.settings;

public interface SettingsHandlerInterface
{
    boolean validateSettings();
    
    void setDefaultSettings();
    
    void setValues();
    
    void init();
}
