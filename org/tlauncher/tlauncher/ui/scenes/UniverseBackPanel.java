// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import java.awt.Color;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.ui.server.BackPanel;

public class UniverseBackPanel extends BackPanel
{
    public UniverseBackPanel(final String titleName) {
        super(titleName, new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    TLauncher.getInstance().getFrame().mp.openDefaultScene();
                }
            }
        }, ImageCache.getIcon("back-arrow.png"));
    }
    
    public UniverseBackPanel(final Color color) {
        super("", new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    TLauncher.getInstance().getFrame().mp.openDefaultScene();
                }
            }
        }, ImageCache.getIcon("back-arrow.png"));
        this.setBackground(color);
    }
}
