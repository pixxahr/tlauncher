// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.Box;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.managers.ModpackManager;
import java.io.IOException;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.rmo.Bootstrapper;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import javax.swing.JButton;
import javax.swing.AbstractButton;
import javax.swing.plaf.ButtonUI;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.ui.RadioSettingsUI;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import org.tlauncher.tlauncher.ui.loc.LocalizableRadioButton;
import java.awt.Container;
import java.awt.event.ComponentListener;
import java.util.Iterator;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import org.tlauncher.tlauncher.ui.settings.MinecraftSettings;
import java.awt.LayoutManager;
import java.awt.FlowLayout;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.server.BackPanel;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.SpringLayout;
import java.util.ArrayList;
import org.tlauncher.tlauncher.ui.settings.TlauncherSettings;
import org.tlauncher.tlauncher.ui.settings.SettingsHandlerInterface;
import java.util.List;
import java.awt.Insets;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.ButtonGroup;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import org.tlauncher.tlauncher.ui.MainPane;
import java.awt.Color;
import java.awt.Dimension;

public class SettingsScene extends PseudoScene
{
    public static final Dimension SIZE;
    private static final Color SWITCH_FOREGROUND;
    private static final Color LEFT_BUTTONS_BACKGROUND;
    private static final Color RIGHT_BUTTONS_BACKGROUND;
    public static final Color BACKGROUND;
    private final MainPane main;
    private ExtendedPanel base;
    private ButtonGroup buttonGroup;
    public final TLauncher tlauncher;
    public final Configuration global;
    private final DefaultScene defaultScene;
    public static final Insets BUTTON_INSETS;
    public static final Insets CENTER_INSETS;
    public static String minecraft;
    public static String tlaucner;
    private List<SettingsHandlerInterface> pages;
    private TlauncherSettings tlauncherSettings;
    
    public SettingsScene(final MainPane main) {
        super(main);
        this.buttonGroup = new ButtonGroup();
        this.pages = new ArrayList<SettingsHandlerInterface>();
        this.main = main;
        this.tlauncher = TLauncher.getInstance();
        this.global = this.tlauncher.getConfiguration();
        this.defaultScene = main.defaultScene;
        final ExtendedPanel topPanel = new ExtendedPanel();
        final SpringLayout sl_topPanel = new SpringLayout();
        this.base = new ExtendedPanel();
        final BackPanel backPanel = new BackPanel("settings.title", new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                SettingsScene.this.main.openDefaultScene();
            }
        }, ImageCache.getIcon("back-arrow.png"));
        final ExtendedPanel switchPanel = new ExtendedPanel();
        final ExtendedPanel centerPanel = new ExtendedPanel();
        final ExtendedPanel buttonPanel = new ExtendedPanel(new FlowLayout(0, 0, 0));
        final MinecraftSettings minecraftSettings = new MinecraftSettings();
        this.tlauncherSettings = new TlauncherSettings();
        this.pages.add(this.tlauncherSettings);
        this.pages.add(minecraftSettings);
        this.base.setSize(SettingsScene.SIZE);
        this.base.setOpaque(true);
        this.base.setBackground(SettingsScene.BACKGROUND);
        switchPanel.setLayout(new GridLayout(0, 2, 0, 0));
        centerPanel.setLayout(new CardLayout(0, 0));
        this.base.setLayout(new BorderLayout(0, 0));
        topPanel.setPreferredSize(new Dimension(SettingsScene.SIZE.width, 49));
        buttonPanel.setPreferredSize(new Dimension(SettingsScene.SIZE.width, 55));
        centerPanel.setInsets(SettingsScene.CENTER_INSETS);
        buttonPanel.setInsets(SettingsScene.BUTTON_INSETS);
        topPanel.setLayout(sl_topPanel);
        sl_topPanel.putConstraint("North", backPanel, 0, "North", topPanel);
        sl_topPanel.putConstraint("West", backPanel, 0, "West", topPanel);
        sl_topPanel.putConstraint("South", backPanel, 25, "North", topPanel);
        sl_topPanel.putConstraint("East", backPanel, 0, "East", topPanel);
        topPanel.add(backPanel);
        sl_topPanel.putConstraint("North", switchPanel, 0, "South", backPanel);
        sl_topPanel.putConstraint("West", switchPanel, 0, "West", topPanel);
        sl_topPanel.putConstraint("South", switchPanel, 0, "South", topPanel);
        sl_topPanel.putConstraint("East", switchPanel, 0, "East", topPanel);
        topPanel.add(switchPanel);
        centerPanel.add(SettingsScene.minecraft, minecraftSettings);
        centerPanel.add(SettingsScene.tlaucner, this.tlauncherSettings);
        final Container container;
        final CardLayout cl;
        final LocalizableRadioButton button = this.createRadioButtoon("settings.tab.minecraft", e -> {
            cl = (CardLayout)container.getLayout();
            cl.show(container, SettingsScene.minecraft);
            this.repaint();
            return;
        });
        final Container container2;
        final CardLayout cl2;
        final LocalizableRadioButton buttonTl = this.createRadioButtoon("settings.tab.tlauncher", e -> {
            cl2 = (CardLayout)container2.getLayout();
            cl2.show(container2, SettingsScene.tlaucner);
            this.repaint();
            return;
        });
        button.setSelected(true);
        switchPanel.add(button);
        switchPanel.add(buttonTl);
        this.fillButtons(buttonPanel);
        this.base.add(centerPanel, "Center");
        this.base.add(topPanel, "North");
        this.base.add(buttonPanel, "South");
        this.add(this.base);
        SwingUtil.changeFontFamily(backPanel, FontTL.ROBOTO_BOLD);
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(final ComponentEvent e) {
                for (final SettingsHandlerInterface settingsHandlerInterface : SettingsScene.this.pages) {
                    settingsHandlerInterface.init();
                }
            }
        });
    }
    
    @Override
    public void onResize() {
        super.onResize();
        this.base.setLocation((int)(this.getWidth() / 2 - SettingsScene.SIZE.getWidth() / 2.0), (int)((this.getHeight() - LoginForm.LOGIN_SIZE.height) / 2 - SettingsScene.SIZE.getHeight() / 2.0));
    }
    
    private LocalizableRadioButton createRadioButtoon(final String name, final ActionListener actionListener) {
        final LocalizableRadioButton button = new LocalizableRadioButton(name);
        button.setUI(new RadioSettingsUI(ImageCache.getImage("background-tab-off.png")));
        button.setOpaque(true);
        button.addActionListener(actionListener);
        button.setForeground(SettingsScene.SWITCH_FOREGROUND);
        button.setFont(button.getFont().deriveFont(1, 16.0f));
        this.buttonGroup.add(button);
        return button;
    }
    
    private void fillButtons(final ExtendedPanel buttons) {
        final Font font = new JButton().getFont().deriveFont(1, 13.0f);
        final UpdaterButton saveButton = new UpdaterButton(UpdaterButton.ORRANGE_COLOR, "settings.save");
        saveButton.setFont(font);
        final Iterator<SettingsHandlerInterface> iterator;
        SettingsHandlerInterface settingsHandlerInterface;
        final boolean restart;
        final Iterator<SettingsHandlerInterface> iterator2;
        SettingsHandlerInterface settingsHandlerInterface2;
        saveButton.addActionListener(e -> {
            this.pages.iterator();
            while (iterator.hasNext()) {
                settingsHandlerInterface = iterator.next();
                if (!settingsHandlerInterface.validateSettings()) {
                    return;
                }
            }
            restart = this.restartClient();
            if (this.tlauncherSettings.chooseChinaLocal() && !restart) {
                this.main.openDefaultScene();
                return;
            }
            else {
                this.pages.iterator();
                while (iterator2.hasNext()) {
                    settingsHandlerInterface2 = iterator2.next();
                    settingsHandlerInterface2.setValues();
                }
                this.global.store();
                if (restart) {
                    try {
                        Bootstrapper.restartLauncher().start();
                    }
                    catch (IOException e1) {
                        U.log(e1);
                    }
                    System.exit(0);
                }
                ((ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class)).resetInfoMod();
                this.main.openDefaultScene();
                return;
            }
        });
        saveButton.setForeground(Color.WHITE);
        final UpdaterButton defaultButton = new UpdaterButton(UpdaterButton.GREEN_COLOR, "settings.default");
        defaultButton.setFont(font);
        final Iterator<SettingsHandlerInterface> iterator3;
        SettingsHandlerInterface settingsHandlerInterface3;
        defaultButton.addActionListener(e -> {
            if (Alert.showLocQuestion("settings.default.warning")) {
                this.pages.iterator();
                while (iterator3.hasNext()) {
                    settingsHandlerInterface3 = iterator3.next();
                    settingsHandlerInterface3.setDefaultSettings();
                }
            }
            return;
        });
        defaultButton.setForeground(Color.WHITE);
        final int preferredHeight = 26;
        buttons.add(Box.createHorizontalStrut(242));
        defaultButton.setPreferredSize(new Dimension(178, preferredHeight));
        buttons.add(defaultButton);
        buttons.add(Box.createHorizontalStrut(20));
        saveButton.setPreferredSize(new Dimension(141, preferredHeight));
        buttons.add(saveButton);
        defaultButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                defaultButton.setBackground(SettingsScene.LEFT_BUTTONS_BACKGROUND);
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                defaultButton.setBackground(defaultButton.getBackgroundColor());
            }
        });
        saveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                saveButton.setBackground(SettingsScene.RIGHT_BUTTONS_BACKGROUND);
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                saveButton.setBackground(saveButton.getBackgroundColor());
            }
        });
    }
    
    private boolean restartClient() {
        return this.tlauncherSettings.chooseChinaLocal() && Alert.showLocQuestion("tlauncher.restart", "tlauncher.restart.message");
    }
    
    static {
        SIZE = new Dimension(620, 529);
        SWITCH_FOREGROUND = new Color(60, 170, 232);
        LEFT_BUTTONS_BACKGROUND = new Color(88, 159, 42);
        RIGHT_BUTTONS_BACKGROUND = new Color(204, 118, 47);
        BACKGROUND = new Color(245, 245, 245);
        BUTTON_INSETS = new Insets(9, 19, 20, 19);
        CENTER_INSETS = new Insets(20, 19, 0, 19);
        SettingsScene.minecraft = "minecraft";
        SettingsScene.tlaucner = "tlauncher";
    }
}
