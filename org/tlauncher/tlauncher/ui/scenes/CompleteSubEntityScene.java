// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import java.awt.Rectangle;
import java.awt.Graphics2D;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.tlauncher.ui.loc.modpack.ModpackTableVersionButton;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import javax.swing.JButton;
import java.io.IOException;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.OS;
import java.awt.Container;
import org.tlauncher.tlauncher.ui.modpack.PicturePanel;
import javax.swing.plaf.ScrollBarUI;
import org.tlauncher.tlauncher.ui.ui.ModpackScrollBarUI;
import javax.swing.JScrollPane;
import org.tlauncher.tlauncher.ui.swing.HtmlTextPane;
import java.awt.CardLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import org.tlauncher.tlauncher.ui.modpack.OldModpackFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import javax.swing.AbstractButton;
import org.tlauncher.tlauncher.ui.swing.GameRadioButton;
import javax.swing.ButtonGroup;
import org.tlauncher.tlauncher.ui.loc.UpdaterFullButton;
import org.tlauncher.tlauncher.controller.modpack.FullGameEntityController;
import org.tlauncher.tlauncher.ui.modpack.GroupPanel;
import com.google.inject.Injector;
import org.tlauncher.tlauncher.ui.loc.modpack.UpInstallButton;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityPanel;
import javax.swing.table.TableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import org.tlauncher.tlauncher.ui.swing.renderer.JTableButtonRenderer;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Dimension;
import javax.swing.JTable;
import net.minecraft.launcher.versions.CompleteVersion;
import javax.swing.table.AbstractTableModel;
import org.tlauncher.tlauncher.ui.loc.modpack.ModpackActButton;
import javax.swing.Icon;
import java.util.Set;
import org.tlauncher.tlauncher.ui.menu.ModpackCategoryPopupMenu;
import org.tlauncher.modpack.domain.client.share.Category;
import org.tlauncher.util.ColorUtil;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import org.tlauncher.tlauncher.ui.swing.TextWrapperLabel;
import java.util.Collection;
import java.util.HashSet;
import java.util.Date;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.text.SimpleDateFormat;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import org.tlauncher.tlauncher.ui.button.StatusStarButton;
import javax.swing.JPanel;
import java.util.List;
import java.util.Iterator;
import org.tlauncher.util.async.AsyncThread;
import java.util.ArrayList;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.net.URL;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import java.awt.Graphics;
import java.util.concurrent.ConcurrentLinkedDeque;
import javax.swing.JLabel;
import org.tlauncher.tlauncher.ui.loc.modpack.GameRightButton;
import javax.swing.JLayeredPane;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityListener;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.server.BackPanel;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.util.U;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import java.awt.Component;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.LayoutManager;
import java.awt.GridLayout;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class CompleteSubEntityScene extends PseudoScene
{
    protected final ExtendedPanel panel;
    protected final String REVIEW_S = "REVIEW";
    protected final String VERSION_S = "VERSIONS";
    protected final String PICTURES_S = "PICTURES";
    protected final ModpackManager manager;
    protected FullGameEntity fullGameEntity;
    public static final int BUTTON_PANEL_SUB_VIEW = 130;
    
    public CompleteSubEntityScene(final MainPane main) {
        super(main);
        this.panel = new ExtendedPanel(new GridLayout(1, 1, 0, 0));
        this.manager = (ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class);
        this.panel.setSize(ModpackScene.SIZE);
        this.panel.setOpaque(true);
        this.add(this.panel);
    }
    
    @Override
    public void onResize() {
        super.onResize();
        this.panel.setLocation(this.getWidth() / 2 - this.panel.getWidth() / 2, (this.getHeight() - LoginForm.LOGIN_SIZE.height) / 2 - this.panel.getHeight() / 2);
    }
    
    public void showFullGameEntity(final GameEntityDTO gameEntityDTO, final GameType type) {
        this.clean(type);
        U.debug("open " + gameEntityDTO.getName() + " " + U.memoryStatus());
        final BackPanel backPanel = new BackPanel("", new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    CompleteSubEntityScene.this.getMainPane().setScene(CompleteSubEntityScene.this.getMainPane().modpackScene);
                    CompleteSubEntityScene.this.clean(type);
                }
            }
        }, ImageCache.getIcon("back-arrow.png"));
        this.fullGameEntity = new FullGameEntity(gameEntityDTO, backPanel, type);
        this.panel.add(this.fullGameEntity);
        if (this.getMainPane().getScene() != this) {
            this.getMainPane().setScene(this);
        }
        else {
            this.panel.revalidate();
            this.panel.repaint();
        }
    }
    
    protected void clean(final GameType type) {
        if (this.fullGameEntity != null) {
            this.panel.remove(this.fullGameEntity);
            this.manager.removeGameListener(type, this.fullGameEntity);
            this.fullGameEntity = null;
        }
    }
    
    public void showModpackElement(final GameEntityDTO completeGameEntity, final GameType type) {
        this.clean(type);
        final BackPanel backPanel = new BackPanel("", new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    CompleteSubEntityScene.this.getMainPane().setScene(CompleteSubEntityScene.this.getMainPane().modpackEnitityScene);
                    CompleteSubEntityScene.this.clean(type);
                }
            }
        }, ImageCache.getIcon("back-arrow.png"));
        this.fullGameEntity = new FullGameEntity(completeGameEntity, backPanel, type);
        this.panel.add(this.fullGameEntity);
        this.revalidate();
        this.repaint();
        this.getMainPane().setScene(this);
    }
    
    public static class ImagePanel extends JLayeredPane
    {
        private GameRightButton gameRightButton;
        private GameEntityDTO entity;
        private JLabel label;
        private static ConcurrentLinkedDeque<String> images;
        private static final Object object;
        
        public ImagePanel(final GameEntityDTO entity) {
            this.entity = entity;
            this.setOpaque(true);
            (this.label = new JLabel() {
                @Override
                protected void paintComponent(final Graphics g) {
                    super.paintComponent(g);
                    ImagePanel.this.initPicture(g);
                }
            }).setOpaque(true);
            this.add(this.label, 1);
            this.label.setBounds(0, 0, 111, 111);
        }
        
        public void addMoapckActButton(final GameRightButton actButton) {
            this.add(this.gameRightButton = actButton, 0);
            actButton.setBounds(10, 80, 90, 23);
        }
        
        public void initPicture(final Graphics g) {
            if (this.entity.getPicture() != null) {
                try {
                    final String picture = ModpackUtil.getPictureURL(this.entity.getPicture(), "_logo");
                    if (!ImageCache.imageInCache(picture)) {
                        synchronized (ImagePanel.object) {
                            ImagePanel.images.push(picture);
                        }
                        if (ImagePanel.images.size() == 1) {
                            this.loadImages();
                        }
                    }
                    else if (ImageCache.loadImage(new URL(picture), false) != null) {
                        final Image image = ImageCache.loadImage(new URL(picture), false);
                        if (image != null) {
                            g.drawImage(image, 0, 0, null);
                        }
                    }
                }
                catch (Exception e) {
                    U.log(e);
                }
            }
        }
        
        private void loadImages() {
            ArrayList<String> list;
            int size;
            int i;
            final Iterator<String> iterator;
            String image;
            AsyncThread.execute(() -> {
                try {
                    Thread.sleep(200L);
                    list = new ArrayList<String>();
                    synchronized (ImagePanel.object) {
                        for (size = ((ImagePanel.images.size() > 5) ? 5 : ImagePanel.images.size()), i = 0; i < size; ++i) {
                            list.add(ImagePanel.images.pop());
                        }
                        ImagePanel.images.clear();
                    }
                    list.iterator();
                    while (iterator.hasNext()) {
                        image = iterator.next();
                        if (ImagePanel.images.size() > 5) {
                            return;
                        }
                        else {
                            ImageCache.loadImage(new URL(image), false);
                        }
                    }
                    if (this.gameRightButton != null) {
                        this.gameRightButton.updateRow();
                    }
                    else {
                        this.label.repaint();
                    }
                }
                catch (Exception e) {
                    U.log(e);
                }
            });
        }
        
        static {
            ImagePanel.images = new ConcurrentLinkedDeque<String>();
            object = new Object();
        }
    }
    
    public static class DescriptionGamePanel extends JPanel
    {
        protected StatusStarButton statusStarButton;
        protected SpringLayout descriptionLayout;
        protected JLabel name;
        protected JTextArea description;
        protected JLabel downloadLabel;
        protected JLabel updateLabel;
        protected JLabel gameVersion;
        protected ImagePanel imagePanel;
        private int gupPair;
        private int gup;
        private SimpleDateFormat format;
        
        public StatusStarButton getStatusStarButton() {
            return this.statusStarButton;
        }
        
        public DescriptionGamePanel(final GameEntityDTO entity, final GameType type) {
            this.gupPair = 30;
            this.gup = 5;
            this.format = new SimpleDateFormat("dd MMMM YYYY", Localizable.get().getSelected());
            this.setLayout(this.descriptionLayout = new SpringLayout());
            this.statusStarButton = new StatusStarButton(entity, type);
            this.imagePanel = new ImagePanel(entity);
            final VersionDTO versionDTO = entity.getVersions().get(0);
            final ExtendedPanel descriptionEntityPanel = new ExtendedPanel();
            this.name = new JLabel(entity.getName());
            final JLabel authorLabel = new LocalizableLabel("modpack.complete.author");
            final JLabel authorValue = new JLabel(entity.getAuthor());
            this.downloadLabel = new LocalizableLabel("modpack.description.download");
            final JLabel downloadValue = new JLabel(this.getStringDownloadingCount(entity.getDownloadALL()));
            this.updateLabel = new LocalizableLabel("modpack.description.date");
            final JLabel updateValue = new JLabel(this.format.format(new Date(versionDTO.getUpdateDate())));
            this.gameVersion = new JLabel();
            final JLabel gameVersionLabel = new LocalizableLabel("modpack.creation.version.game");
            if (versionDTO.getGameVersions() == null || versionDTO.getGameVersions().isEmpty()) {
                this.gameVersion.setVisible(false);
                gameVersionLabel.setVisible(false);
            }
            else {
                final Set<String> versions = new HashSet<String>();
                entity.getVersions().forEach(e -> versions.addAll(e.getGameVersions()));
                this.gameVersion.setText(ModpackUtil.getLatestGameVersion(versions));
            }
            (this.description = new TextWrapperLabel(entity.getShortDescription())).setVisible(false);
            authorValue.setHorizontalAlignment(2);
            downloadValue.setHorizontalAlignment(2);
            updateValue.setHorizontalAlignment(2);
            this.gameVersion.setHorizontalAlignment(2);
            SwingUtil.changeFontFamily(this.name, FontTL.ROBOTO_BOLD, 18);
            SwingUtil.changeFontFamily(authorLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_193);
            SwingUtil.changeFontFamily(this.downloadLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
            SwingUtil.changeFontFamily(gameVersionLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
            SwingUtil.changeFontFamily(this.updateLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
            SwingUtil.changeFontFamily(authorValue, FontTL.ROBOTO_REGULAR, 14, ColorUtil.BLUE_MODPACK);
            SwingUtil.changeFontFamily(downloadValue, FontTL.ROBOTO_REGULAR, 14, ColorUtil.BLUE_MODPACK);
            SwingUtil.changeFontFamily(updateValue, FontTL.ROBOTO_REGULAR, 14, ColorUtil.BLUE_MODPACK);
            SwingUtil.changeFontFamily(this.gameVersion, FontTL.ROBOTO_REGULAR, 14, ColorUtil.BLUE_MODPACK);
            SwingUtil.changeFontFamily(this.description, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_64);
            final SpringLayout descriptionSpring = new SpringLayout();
            descriptionEntityPanel.setLayout(descriptionSpring);
            this.descriptionLayout.putConstraint("West", this.imagePanel, 66, "West", this);
            this.descriptionLayout.putConstraint("East", this.imagePanel, 177, "West", this);
            this.descriptionLayout.putConstraint("North", this.imagePanel, 25, "North", this);
            this.descriptionLayout.putConstraint("South", this.imagePanel, -25, "South", this);
            this.add(this.imagePanel);
            this.descriptionLayout.putConstraint("West", descriptionEntityPanel, 13, "East", this.imagePanel);
            this.descriptionLayout.putConstraint("East", descriptionEntityPanel, 0, "East", this);
            this.descriptionLayout.putConstraint("North", descriptionEntityPanel, 25, "North", this);
            this.descriptionLayout.putConstraint("South", descriptionEntityPanel, -20, "South", this);
            this.add(descriptionEntityPanel);
            descriptionSpring.putConstraint("West", this.name, 0, "West", descriptionEntityPanel);
            descriptionSpring.putConstraint("East", this.name, 250, "West", descriptionEntityPanel);
            descriptionSpring.putConstraint("North", this.name, 0, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", this.name, 23, "North", descriptionEntityPanel);
            descriptionEntityPanel.add(this.name);
            descriptionSpring.putConstraint("West", authorLabel, 0, "West", descriptionEntityPanel);
            descriptionSpring.putConstraint("East", authorLabel, authorLabel.getPreferredSize().width, "West", descriptionEntityPanel);
            descriptionSpring.putConstraint("North", authorLabel, 23, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", authorLabel, 42, "North", descriptionEntityPanel);
            descriptionEntityPanel.add(authorLabel);
            descriptionSpring.putConstraint("West", authorValue, this.gup, "East", authorLabel);
            descriptionSpring.putConstraint("East", authorValue, 0, "East", descriptionEntityPanel);
            descriptionSpring.putConstraint("North", authorValue, 23, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", authorValue, 42, "North", descriptionEntityPanel);
            descriptionEntityPanel.add(authorValue);
            descriptionSpring.putConstraint("West", this.description, 0, "West", descriptionEntityPanel);
            descriptionSpring.putConstraint("East", this.description, -100, "East", descriptionEntityPanel);
            descriptionSpring.putConstraint("North", this.description, 46, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", this.description, 85, "North", descriptionEntityPanel);
            descriptionEntityPanel.add(this.description);
            descriptionSpring.putConstraint("West", this.downloadLabel, 0, "West", descriptionEntityPanel);
            descriptionSpring.putConstraint("East", this.downloadLabel, this.downloadLabel.getPreferredSize().width, "West", descriptionEntityPanel);
            descriptionSpring.putConstraint("North", this.downloadLabel, 90, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", this.downloadLabel, 0, "South", descriptionEntityPanel);
            descriptionEntityPanel.add(this.downloadLabel);
            descriptionSpring.putConstraint("West", downloadValue, this.gup, "East", this.downloadLabel);
            descriptionSpring.putConstraint("East", downloadValue, downloadValue.getPreferredSize().width + this.gup, "East", this.downloadLabel);
            descriptionSpring.putConstraint("North", downloadValue, 90, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", downloadValue, 0, "South", descriptionEntityPanel);
            descriptionEntityPanel.add(downloadValue);
            descriptionSpring.putConstraint("West", this.updateLabel, this.gupPair, "East", downloadValue);
            descriptionSpring.putConstraint("East", this.updateLabel, this.gupPair + this.updateLabel.getPreferredSize().width, "East", downloadValue);
            descriptionSpring.putConstraint("North", this.updateLabel, 90, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", this.updateLabel, 0, "South", descriptionEntityPanel);
            descriptionEntityPanel.add(this.updateLabel);
            descriptionSpring.putConstraint("West", updateValue, this.gup, "East", this.updateLabel);
            descriptionSpring.putConstraint("East", updateValue, updateValue.getPreferredSize().width + this.gup, "East", this.updateLabel);
            descriptionSpring.putConstraint("North", updateValue, 90, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", updateValue, 0, "South", descriptionEntityPanel);
            descriptionEntityPanel.add(updateValue);
            descriptionSpring.putConstraint("West", gameVersionLabel, this.gupPair, "East", updateValue);
            descriptionSpring.putConstraint("East", gameVersionLabel, this.gupPair + gameVersionLabel.getPreferredSize().width, "East", updateValue);
            descriptionSpring.putConstraint("North", gameVersionLabel, 90, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", gameVersionLabel, 0, "South", descriptionEntityPanel);
            descriptionEntityPanel.add(gameVersionLabel);
            descriptionSpring.putConstraint("West", this.gameVersion, this.gup, "East", gameVersionLabel);
            descriptionSpring.putConstraint("East", this.gameVersion, this.gameVersion.getPreferredSize().width + this.gup, "East", gameVersionLabel);
            descriptionSpring.putConstraint("North", this.gameVersion, 90, "North", descriptionEntityPanel);
            descriptionSpring.putConstraint("South", this.gameVersion, 0, "South", descriptionEntityPanel);
            descriptionEntityPanel.add(this.gameVersion);
            final ExtendedPanel pictureCategories = new ExtendedPanel();
            int count = 0;
            for (final Category c : entity.getCategories()) {
                Icon icon = null;
                try {
                    icon = ImageCache.getNativeIcon("category/" + c.toString() + ".png");
                }
                catch (NullPointerException e2) {
                    U.log("don't find category " + c);
                    continue;
                }
                ++count;
                final JLabel label = new JLabel(icon);
                label.setHorizontalAlignment(0);
                label.setAlignmentY(0.5f);
                pictureCategories.add(label);
                final ModpackCategoryPopupMenu popupMenu = new ModpackCategoryPopupMenu(c, label);
                label.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseEntered(final MouseEvent e) {
                        popupMenu.show(label, e.getX() + 15, e.getY() + 15);
                    }
                    
                    @Override
                    public void mouseExited(final MouseEvent e) {
                        popupMenu.setVisible(false);
                    }
                });
            }
            pictureCategories.setLayout(new GridLayout(1, count, 0, 10));
            this.descriptionLayout.putConstraint("West", pictureCategories, -200, "East", this);
            this.descriptionLayout.putConstraint("East", pictureCategories, -25 - (5 - count) * 35, "East", this);
            this.descriptionLayout.putConstraint("North", pictureCategories, 24, "North", this);
            this.descriptionLayout.putConstraint("South", pictureCategories, 50, "North", this);
            this.add(pictureCategories);
            this.descriptionLayout.putConstraint("West", this.statusStarButton, -38, "East", this);
            this.descriptionLayout.putConstraint("East", this.statusStarButton, -25, "East", this);
            this.descriptionLayout.putConstraint("North", this.statusStarButton, -35, "South", this);
            this.descriptionLayout.putConstraint("South", this.statusStarButton, -22, "South", this);
            this.add(this.statusStarButton);
        }
        
        private String getStringDownloadingCount(final Integer i) {
            String res = "";
            if (i < 1000) {
                return i.toString();
            }
            if (i < 1000000) {
                res = i / 1000 + " " + Localizable.get("modpack.thousand");
            }
            else {
                res = i / 1000000 + " " + Localizable.get("modpack.million");
            }
            if (TLauncher.getInstance().getConfiguration().getLocale().getLanguage().equals("en")) {
                res = res.replace(" ", "");
            }
            return res;
        }
    }
    
    private abstract class BaseSubtypeModel<T extends BaseModelElement> extends GameEntityTableModel
    {
        protected List<T> list;
        protected final SimpleDateFormat format;
        
        private BaseSubtypeModel() {
            this.list = new ArrayList<T>();
            this.format = new SimpleDateFormat("dd/MM/YYYY", Localizable.get().getSelected());
        }
        
        public abstract GameEntityDTO getRowObject(final int p0);
        
        public int find(final GameEntityDTO entity) {
            for (int i = 0; i < this.list.size(); ++i) {
                if (entity.getId().equals(this.list.get(i).getEntity().getId())) {
                    return i;
                }
            }
            return -1;
        }
        
        @Override
        public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
            final int index = this.find(e);
            if (index != -1) {
                this.list.get(index).getModpackActButton().reset();
                this.fireTableCellUpdated(index, 4);
            }
        }
        
        @Override
        public void installEntity(final GameEntityDTO e, final GameType type) {
            final int index = this.find(e);
            if (index != -1) {
                this.list.get(index).getModpackActButton().setTypeButton("REMOVE");
                this.fireTableCellUpdated(index, 4);
            }
        }
        
        @Override
        public void removeEntity(final GameEntityDTO e) {
            final int index = this.find(e);
            if (index != -1) {
                this.list.get(index).getModpackActButton().setTypeButton("INSTALL");
                this.fireTableCellUpdated(index, 4);
            }
        }
    }
    
    protected static class BaseModelElement
    {
        private ModpackActButton modpackActButton;
        private GameEntityDTO entity;
        
        public BaseModelElement(final ModpackActButton modpackActButton, final GameEntityDTO entity) {
            this.modpackActButton = modpackActButton;
            this.entity = entity;
        }
        
        public ModpackActButton getModpackActButton() {
            return this.modpackActButton;
        }
        
        public GameEntityDTO getEntity() {
            return this.entity;
        }
        
        public void setModpackActButton(final ModpackActButton modpackActButton) {
            this.modpackActButton = modpackActButton;
        }
        
        public void setEntity(final GameEntityDTO entity) {
            this.entity = entity;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof BaseModelElement)) {
                return false;
            }
            final BaseModelElement other = (BaseModelElement)o;
            if (!other.canEqual(this)) {
                return false;
            }
            final Object this$modpackActButton = this.getModpackActButton();
            final Object other$modpackActButton = other.getModpackActButton();
            Label_0065: {
                if (this$modpackActButton == null) {
                    if (other$modpackActButton == null) {
                        break Label_0065;
                    }
                }
                else if (this$modpackActButton.equals(other$modpackActButton)) {
                    break Label_0065;
                }
                return false;
            }
            final Object this$entity = this.getEntity();
            final Object other$entity = other.getEntity();
            if (this$entity == null) {
                if (other$entity == null) {
                    return true;
                }
            }
            else if (this$entity.equals(other$entity)) {
                return true;
            }
            return false;
        }
        
        protected boolean canEqual(final Object other) {
            return other instanceof BaseModelElement;
        }
        
        @Override
        public int hashCode() {
            final int PRIME = 59;
            int result = 1;
            final Object $modpackActButton = this.getModpackActButton();
            result = result * 59 + (($modpackActButton == null) ? 43 : $modpackActButton.hashCode());
            final Object $entity = this.getEntity();
            result = result * 59 + (($entity == null) ? 43 : $entity.hashCode());
            return result;
        }
        
        @Override
        public String toString() {
            return "CompleteSubEntityScene.BaseModelElement(modpackActButton=" + this.getModpackActButton() + ", entity=" + this.getEntity() + ")";
        }
    }
    
    private class VersionModelElement extends BaseModelElement
    {
        private VersionDTO version;
        
        public VersionModelElement(final ModpackActButton modpackActButton, final GameEntityDTO entity, final VersionDTO version) {
            super(modpackActButton, entity);
            this.version = version;
        }
        
        public VersionDTO getVersion() {
            return this.version;
        }
        
        public void setVersion(final VersionDTO version) {
            this.version = version;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof VersionModelElement)) {
                return false;
            }
            final VersionModelElement other = (VersionModelElement)o;
            if (!other.canEqual(this)) {
                return false;
            }
            final Object this$version = this.getVersion();
            final Object other$version = other.getVersion();
            if (this$version == null) {
                if (other$version == null) {
                    return true;
                }
            }
            else if (this$version.equals(other$version)) {
                return true;
            }
            return false;
        }
        
        @Override
        protected boolean canEqual(final Object other) {
            return other instanceof VersionModelElement;
        }
        
        @Override
        public int hashCode() {
            final int PRIME = 59;
            int result = 1;
            final Object $version = this.getVersion();
            result = result * 59 + (($version == null) ? 43 : $version.hashCode());
            return result;
        }
        
        @Override
        public String toString() {
            return "CompleteSubEntityScene.VersionModelElement(version=" + this.getVersion() + ")";
        }
    }
    
    protected abstract class GameEntityTableModel extends AbstractTableModel implements GameEntityListener
    {
        @Override
        public void activationStarted(final GameEntityDTO e) {
        }
        
        @Override
        public void activation(final GameEntityDTO e) {
        }
        
        @Override
        public void activationError(final GameEntityDTO e, final Throwable t) {
        }
        
        @Override
        public void processingStarted(final GameEntityDTO e, final VersionDTO version) {
        }
        
        @Override
        public void installEntity(final GameEntityDTO e, final GameType type) {
        }
        
        @Override
        public void installEntity(final CompleteVersion e) {
        }
        
        @Override
        public void removeEntity(final GameEntityDTO e) {
        }
        
        @Override
        public void removeCompleteVersion(final CompleteVersion e) {
        }
        
        @Override
        public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
        }
        
        @Override
        public void populateStatus(final GameEntityDTO status, final GameType type, final boolean state) {
        }
        
        @Override
        public void updateVersion(final CompleteVersion v, final CompleteVersion newVersion) {
        }
    }
    
    protected class ModpackTable extends JTable
    {
        protected void init() {
            this.setRowHeight(58);
            this.getColumnModel().setColumnSelectionAllowed(false);
            this.setShowVerticalLines(false);
            this.setCellSelectionEnabled(false);
            this.setGridColor(ColorUtil.COLOR_244);
            final JTableHeader header = this.getTableHeader();
            header.setPreferredSize(new Dimension(header.getPreferredSize().width - 20, 48));
            header.setDefaultRenderer(new TableCellRenderer() {
                DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
                
                @Override
                public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
                    final DefaultTableCellRenderer comp = (DefaultTableCellRenderer)this.cellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    comp.setBorder(BorderFactory.createEmptyBorder());
                    comp.setBackground(new Color(63, 186, 255));
                    comp.setHorizontalAlignment(0);
                    SwingUtil.changeFontFamily(comp, FontTL.ROBOTO_REGULAR, 12, Color.WHITE);
                    return comp;
                }
            });
            final ModpackTableRenderer centerRenderer = new ModpackTableRenderer();
            for (int i = 0; i < this.getModel().getColumnCount() - 1; ++i) {
                this.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
            }
            this.getColumnModel().getColumn(1).setPreferredWidth(250);
            this.getTableHeader().setReorderingAllowed(false);
            this.setDefaultEditor(BaseModelElement.class, new JTableButtonRenderer());
            this.setDefaultRenderer(BaseModelElement.class, new JTableButtonRenderer());
        }
        
        public ModpackTable(final AbstractTableModel model) {
            super(model);
            this.init();
        }
    }
    
    protected class ModpackTableRenderer extends DefaultTableCellRenderer
    {
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
            final DefaultTableCellRenderer cell = (DefaultTableCellRenderer)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            cell.setHorizontalAlignment(0);
            if (column == 1) {
                cell.setHorizontalAlignment(2);
            }
            SwingUtil.changeFontFamily(cell, FontTL.ROBOTO_REGULAR, 12, ColorUtil.COLOR_25);
            cell.setFocusable(false);
            if (hasFocus) {
                this.setBorder(BorderFactory.createEmptyBorder());
            }
            return cell;
        }
    }
    
    protected class FullGameEntity extends GameEntityPanel
    {
        private final Color UP_BACKGROUND;
        private UpInstallButton installButton;
        private GameEntityDTO entity;
        private GameType type;
        private DescriptionGamePanel viewEntity;
        private Injector injector;
        private GroupPanel centerButtons;
        private JPanel centerView;
        private FullGameEntityController controller;
        final /* synthetic */ CompleteSubEntityScene this$0;
        
        public GroupPanel getCenterButtons() {
            return this.centerButtons;
        }
        
        public JPanel getCenterView() {
            return this.centerView;
        }
        
        private FullGameEntity(final GameEntityDTO entity, final BackPanel backPanel, final GameType type) {
            this.UP_BACKGROUND = new Color(60, 170, 232);
            this.controller = new FullGameEntityController();
            this.entity = entity;
            this.type = type;
            this.injector = TLauncher.getInjector();
            final SpringLayout spring = new SpringLayout();
            this.setLayout(spring);
            final JButton officialSite = new UpdaterFullButton(this.UP_BACKGROUND, ColorUtil.BLUE_MODPACK_BUTTON_UP, "modpack.complete.site.button", "official-site.png");
            if (entity.getOfficialSite() == null) {
                officialSite.setVisible(false);
            }
            officialSite.setIconTextGap(15);
            final ButtonGroup group = new ButtonGroup();
            final GameRadioButton reviewButton = new GameRadioButton("modpack.complete.review.button");
            final GameRadioButton picturesButton = new GameRadioButton("modpack.complete.picture.button");
            reviewButton.setSelected(true);
            reviewButton.setActionCommand("REVIEW");
            picturesButton.setActionCommand("PICTURES");
            group.add(reviewButton);
            group.add(picturesButton);
            final Dimension dim = new Dimension(130, 52);
            reviewButton.setPreferredSize(dim);
            picturesButton.setPreferredSize(dim);
            final GameRadioButton versionsButton = new GameRadioButton("modpack.complete.versions.button");
            versionsButton.setActionCommand("VERSIONS");
            group.add(versionsButton);
            versionsButton.setPreferredSize(dim);
            final Color backgroundOldButtonColor = new Color(213, 213, 213);
            final UpdaterButton oldButton = new UpdaterButton(backgroundOldButtonColor, "modpack.complete.old.button");
            oldButton.setForeground(Color.WHITE);
            oldButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(final MouseEvent e) {
                    oldButton.setBackground(new Color(160, 160, 160));
                }
                
                @Override
                public void mouseExited(final MouseEvent e) {
                    oldButton.setBackground(backgroundOldButtonColor);
                }
            });
            oldButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final OldModpackFrame old = new OldModpackFrame(TLauncher.getInstance().getFrame(), entity, type);
                    old.setVisible(true);
                }
            });
            oldButton.setPreferredSize(dim);
            final JLabel originalEnDescription = new JLabel((Icon)ImageCache.getIcon("modpack-original-transation.png"));
            originalEnDescription.setBorder(BorderFactory.createEmptyBorder());
            if (TLauncher.getInstance().getConfiguration().getLocale().getLanguage().equals("en")) {
                originalEnDescription.setVisible(false);
            }
            (this.installButton = new UpInstallButton(entity, type, CompleteSubEntityScene.this.getMainPane().modpackScene.localmodpacks)).setBorder(BorderFactory.createEmptyBorder(0, 19, 0, 0));
            this.installButton.setIconTextGap(18);
            final JPanel reviewPanel = new ExtendedPanel();
            reviewPanel.setLayout(new FlowLayout(0, 0, 0));
            reviewPanel.setOpaque(true);
            reviewPanel.setBackground(ColorUtil.COLOR_246);
            final JPanel versionsPanel = new ExtendedPanel();
            versionsPanel.setLayout(new BorderLayout());
            this.viewEntity = new CompleteDescriptionGamePanel(entity, type);
            final VersionModel model = new VersionModel(entity.getVersions());
            backPanel.addBackListener(new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        CompleteSubEntityScene.this.manager.removeGameListener(type, model);
                    }
                }
            });
            CompleteSubEntityScene.this.manager.addGameListener(type, model);
            final JTable table = new ModpackTable(model);
            table.setBackground(Color.WHITE);
            final JComponent c = ModpackScene.createScrollWrapper(table);
            versionsPanel.add(c, "Center");
            final SpringLayout upSpring = new SpringLayout();
            final ExtendedPanel upButtons = new ExtendedPanel(upSpring);
            upButtons.setOpaque(true);
            upButtons.setBackground(this.UP_BACKGROUND);
            final SpringLayout centerLayout = new SpringLayout();
            (this.centerButtons = new GroupPanel(242)).setLayout(centerLayout);
            this.centerButtons.setOpaque(true);
            SwingUtil.configHorizontalSpingLayout(centerLayout, reviewButton, this.centerButtons, 0);
            centerLayout.putConstraint("West", reviewButton, 66, "West", this.centerButtons);
            centerLayout.putConstraint("East", reviewButton, 196, "West", this.centerButtons);
            this.centerButtons.addInGroup(reviewButton);
            SwingUtil.configHorizontalSpingLayout(centerLayout, picturesButton, reviewButton, 130);
            this.centerButtons.addInGroup(picturesButton);
            SwingUtil.configHorizontalSpingLayout(centerLayout, versionsButton, picturesButton, 130);
            this.centerButtons.addInGroup(versionsButton);
            SwingUtil.configHorizontalSpingLayout(centerLayout, oldButton, this.centerButtons, 130);
            centerLayout.putConstraint("West", oldButton, -215, "East", this.centerButtons);
            centerLayout.putConstraint("East", oldButton, -66, "East", this.centerButtons);
            this.centerButtons.add(oldButton);
            centerLayout.putConstraint("North", originalEnDescription, 12, "North", this.centerButtons);
            centerLayout.putConstraint("South", originalEnDescription, -12, "South", this.centerButtons);
            centerLayout.putConstraint("West", originalEnDescription, -45, "East", this.centerButtons);
            centerLayout.putConstraint("West", originalEnDescription, -45, "East", this.centerButtons);
            centerLayout.putConstraint("East", originalEnDescription, -20, "East", this.centerButtons);
            this.centerButtons.add(originalEnDescription);
            (this.centerView = new JPanel(new CardLayout(0, 0))).setBackground(Color.WHITE);
            this.centerView.setOpaque(true);
            SwingUtil.configHorizontalSpingLayout(upSpring, backPanel, upButtons, 66);
            upSpring.putConstraint("West", backPanel, 0, "West", upButtons);
            upSpring.putConstraint("East", backPanel, 66, "West", upButtons);
            upButtons.add(backPanel);
            SwingUtil.configHorizontalSpingLayout(upSpring, this.installButton, backPanel, 168);
            upButtons.add(this.installButton);
            SwingUtil.configHorizontalSpingLayout(upSpring, officialSite, this.installButton, 0);
            upSpring.putConstraint("West", officialSite, 563, "East", this.installButton);
            upSpring.putConstraint("East", officialSite, 762, "East", this.installButton);
            upButtons.add(officialSite);
            final int gup = 20;
            final HtmlTextPane descriptionFull = HtmlTextPane.get(entity.getDescription());
            descriptionFull.setOpaque(true);
            descriptionFull.setBackground(ColorUtil.COLOR_246);
            final JScrollPane jScrollPane = new JScrollPane(descriptionFull, 20, 31);
            jScrollPane.getVerticalScrollBar().setUI(new ModpackScrollBarUI() {
                @Override
                public Dimension getPreferredSize(final JComponent c) {
                    return new Dimension(13, super.getPreferredSize(c).height);
                }
            });
            jScrollPane.setBorder(BorderFactory.createEmptyBorder());
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    jScrollPane.getVerticalScrollBar().setValue(0);
                }
            });
            final int height = 318;
            final JPanel panelDescription = new ExtendedPanel(new FlowLayout(0, 0, 0));
            panelDescription.setPreferredSize(new Dimension(ModpackScene.SIZE.width, height));
            jScrollPane.setPreferredSize(new Dimension(ModpackScene.SIZE.width - gup * 2, height - 40));
            panelDescription.setBorder(BorderFactory.createEmptyBorder(20, gup, 20, gup));
            panelDescription.add(jScrollPane);
            reviewPanel.add(panelDescription);
            SwingUtil.changeFontFamily(this.installButton, FontTL.ROBOTO_BOLD, 14, Color.WHITE);
            SwingUtil.changeFontFamily(officialSite, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
            SwingUtil.changeFontFamily(reviewButton, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
            SwingUtil.changeFontFamily(versionsButton, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
            SwingUtil.changeFontFamily(picturesButton, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
            SwingUtil.changeFontFamily(oldButton, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
            this.centerView.setBackground(Color.WHITE);
            this.centerView.add(reviewPanel, "REVIEW");
            this.centerView.add(versionsPanel, "VERSIONS");
            final PicturePanel picturePanel = new PicturePanel(entity.getPictures().toArray(new Integer[0]));
            this.centerView.add(picturePanel, "PICTURES");
            spring.putConstraint("North", upButtons, 0, "North", this);
            spring.putConstraint("West", upButtons, 0, "West", this);
            spring.putConstraint("South", upButtons, 56, "North", this);
            spring.putConstraint("East", upButtons, 0, "East", this);
            this.add(upButtons);
            spring.putConstraint("North", this.viewEntity, 0, "South", upButtons);
            spring.putConstraint("West", this.viewEntity, 0, "West", this);
            spring.putConstraint("South", this.viewEntity, 159, "South", upButtons);
            spring.putConstraint("East", this.viewEntity, 0, "East", this);
            this.add(this.viewEntity);
            spring.putConstraint("North", this.centerButtons, 0, "South", this.viewEntity);
            spring.putConstraint("West", this.centerButtons, 0, "West", this);
            spring.putConstraint("South", this.centerButtons, 52, "South", this.viewEntity);
            spring.putConstraint("East", this.centerButtons, 0, "East", this);
            this.add(this.centerButtons);
            spring.putConstraint("North", this.centerView, 0, "South", this.centerButtons);
            spring.putConstraint("West", this.centerView, 0, "West", this);
            spring.putConstraint("South", this.centerView, 321, "South", this.centerButtons);
            spring.putConstraint("East", this.centerView, 0, "East", this);
            this.add(this.centerView);
            final ModpackManager modpackManager = (ModpackManager)this.injector.getInstance((Class)ModpackManager.class);
            final ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    ((CardLayout)FullGameEntity.this.centerView.getLayout()).show(FullGameEntity.this.centerView, e.getActionCommand());
                }
            };
            reviewButton.addActionListener(listener);
            versionsButton.addActionListener(listener);
            picturesButton.addActionListener(listener);
            officialSite.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    if (entity.getOfficialSite() == null) {
                        return;
                    }
                    OS.openLink(entity.getOfficialSite());
                }
            });
            modpackManager.addGameListener(type, this);
            backPanel.addBackListener(new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        CompleteSubEntityScene.this.manager.removeGameListener(type, FullGameEntity.this);
                    }
                }
            });
            originalEnDescription.addMouseListener(new MouseAdapter() {
                private boolean active = true;
                
                @Override
                public void mouseClicked(final MouseEvent mouseEvent) {
                    if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
                        final int currentValue = jScrollPane.getVerticalScrollBar().getValue();
                        if (this.active) {
                            try {
                                if (entity.getEnDescription() == null) {
                                    FullGameEntity.this.controller.getEnDescription(entity);
                                }
                                this.active = false;
                                descriptionFull.setText(entity.getEnDescription());
                            }
                            catch (IOException e) {
                                Alert.showError("", Localizable.get("modpack.remote.not.found", Localizable.get("modpack.try.later")), null);
                            }
                        }
                        else {
                            descriptionFull.setText(entity.getDescription());
                            this.active = true;
                        }
                        SwingUtilities.invokeLater(() -> jScrollPane.getVerticalScrollBar().setValue(currentValue));
                    }
                }
                
                @Override
                public void mouseEntered(final MouseEvent mouseEvent) {
                    originalEnDescription.setIcon((Icon)ImageCache.getIcon("modpack-original-transation-up.png"));
                }
                
                @Override
                public void mouseExited(final MouseEvent mouseEvent) {
                    originalEnDescription.setIcon((Icon)ImageCache.getIcon("modpack-original-transation.png"));
                }
            });
        }
        
        @Override
        public void processingStarted(final GameEntityDTO e, final VersionDTO version) {
        }
        
        @Override
        public void installEntity(final GameEntityDTO e, final GameType type) {
            this.installButton.installEntity(e, type);
        }
        
        @Override
        public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
        }
        
        @Override
        public void populateStatus(final GameEntityDTO entity, final GameType type, final boolean state) {
            if (entity.getId().equals(this.entity.getId())) {
                this.viewEntity.getStatusStarButton().setStatus(state);
            }
        }
        
        @Override
        public void removeEntity(final GameEntityDTO e) {
            this.installButton.removeEntity(e);
        }
        
        class VersionModel extends BaseSubtypeModel<VersionModelElement>
        {
            VersionModel(final List<? extends VersionDTO> list) {
                final ModpackComboBox modpackComboBox = TLauncher.getInstance().getFrame().mp.modpackScene.localmodpacks;
                final BaseModpackFilter<VersionDTO> filter = BaseModpackFilter.getBaseModpackStandardFilters(FullGameEntity.this.entity, FullGameEntity.this.type, modpackComboBox);
                for (final VersionDTO v : list) {
                    final ModpackTableVersionButton button = new ModpackTableVersionButton(FullGameEntity.this.entity, FullGameEntity.this.type, modpackComboBox, v, filter);
                    this.list.add((T)FullGameEntity.this.this$0.new VersionModelElement(button, FullGameEntity.this.entity, v));
                }
            }
            
            @Override
            public int getRowCount() {
                return this.list.size();
            }
            
            @Override
            public int getColumnCount() {
                return 5;
            }
            
            @Override
            public Object getValueAt(final int rowIndex, final int columnIndex) {
                final VersionDTO v = ((VersionModelElement)this.list.get(rowIndex)).getVersion();
                switch (columnIndex) {
                    case 0: {
                        return this.format.format(new Date(v.getUpdateDate()));
                    }
                    case 1: {
                        return v.getName();
                    }
                    case 2: {
                        return v.getType();
                    }
                    case 4: {
                        return ((VersionModelElement)this.list.get(rowIndex)).getModpackActButton();
                    }
                    case 3: {
                        final String version = ModpackUtil.getLatestGameVersion(v.getGameVersions());
                        return (version == null) ? Localizable.get("modpack.version.any") : version;
                    }
                    default: {
                        return null;
                    }
                }
            }
            
            @Override
            public Class<?> getColumnClass(final int columnIndex) {
                if (columnIndex == 4) {
                    return BaseModelElement.class;
                }
                return super.getColumnClass(columnIndex);
            }
            
            @Override
            public String getColumnName(final int column) {
                String line = "";
                switch (column) {
                    case 0: {
                        line = Localizable.get("version.manager.editor.field.time");
                        return line.substring(0, line.length() - 1);
                    }
                    case 1: {
                        return Localizable.get("version.release");
                    }
                    case 2: {
                        line = Localizable.get("version.manager.editor.field.type");
                        return line.substring(0, line.length() - 1);
                    }
                    case 4: {
                        return Localizable.get("modpack.table.pack.element.operation");
                    }
                    case 3: {
                        return Localizable.get("modpack.table.pack.element.version");
                    }
                    default: {
                        return "";
                    }
                }
            }
            
            @Override
            public void installEntity(final GameEntityDTO e, final GameType type) {
                final int index = this.findByVersion(e, e.getVersion());
                if (index != -1) {
                    ((VersionModelElement)this.list.get(index)).getModpackActButton().setTypeButton("REMOVE");
                    this.fireTableCellUpdated(index, 4);
                }
            }
            
            @Override
            public void removeEntity(final GameEntityDTO e) {
                final int index = this.findByVersion(e, e.getVersion());
                if (index != -1) {
                    ((VersionModelElement)this.list.get(index)).getModpackActButton().setTypeButton("INSTALL");
                    this.fireTableCellUpdated(index, 4);
                }
            }
            
            @Override
            public void processingStarted(final GameEntityDTO e, final VersionDTO version) {
                if (e.getId().equals(FullGameEntity.this.entity.getId())) {
                    for (int i = 0; i < this.list.size(); ++i) {
                        if (((VersionModelElement)this.list.get(i)).getVersion().getId().equals(version.getId())) {
                            ((VersionModelElement)this.list.get(i)).getModpackActButton().setTypeButton("PROCESSING");
                            this.fireTableCellUpdated(i, 4);
                            return;
                        }
                    }
                }
            }
            
            @Override
            public GameEntityDTO getRowObject(final int rowIndex) {
                return null;
            }
            
            public int findByVersion(final GameEntityDTO e, final VersionDTO v) {
                if (e.getId().equals(FullGameEntity.this.entity.getId())) {
                    for (int i = 0; i < this.list.size(); ++i) {
                        final VersionModelElement element = (VersionModelElement)this.list.get(i);
                        if (element.getVersion().getId().equals(v.getId())) {
                            return i;
                        }
                    }
                }
                return -1;
            }
            
            @Override
            public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
                final int index = this.findByVersion(e, v);
                if (index != -1) {
                    ((VersionModelElement)this.list.get(index)).getModpackActButton().reset();
                    this.fireTableCellUpdated(index, 4);
                }
            }
            
            @Override
            public boolean isCellEditable(final int rowIndex, final int columnIndex) {
                return columnIndex == 4;
            }
        }
    }
    
    class CompleteDescriptionGamePanel extends DescriptionGamePanel
    {
        public static final int SHADOW_PANEL = 223;
        
        public CompleteDescriptionGamePanel(final GameEntityDTO entity, final GameType type) {
            super(entity, type);
        }
        
        @Override
        protected void paintComponent(final Graphics g0) {
            super.paintComponent(g0);
            final Rectangle rec = this.getVisibleRect();
            int y = rec.y;
            int i = 223;
            final Graphics2D g = (Graphics2D)g0;
            while (y < rec.height + rec.y) {
                g.setColor(new Color(i, i, i));
                if (i != 255) {
                    ++i;
                }
                g.drawLine(rec.x, y, rec.x + rec.width, y);
                ++y;
            }
        }
    }
}
