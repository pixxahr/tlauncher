// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.swing.AnimatedVisibility;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedLayeredPane;

public abstract class PseudoScene extends ExtendedLayeredPane implements AnimatedVisibility
{
    private static final long serialVersionUID = -1L;
    private final MainPane main;
    private boolean shown;
    
    PseudoScene(final MainPane main) {
        super(main);
        this.shown = true;
        this.main = main;
        this.setSize(main.getWidth(), main.getHeight());
    }
    
    public MainPane getMainPane() {
        return this.main;
    }
    
    @Override
    public void setShown(final boolean shown) {
        this.setShown(shown, true);
    }
    
    @Override
    public void setShown(final boolean shown, final boolean animate) {
        if (this.shown == shown) {
            return;
        }
        this.setVisible(this.shown = shown);
    }
}
