// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import org.tlauncher.modpack.domain.client.version.VersionDTO;
import java.util.ArrayList;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import java.util.Iterator;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.tlauncher.ui.loc.modpack.ModpackActButton;
import org.tlauncher.tlauncher.ui.loc.modpack.ModpackTableInstallButton;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.util.List;
import java.text.SimpleDateFormat;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import javax.swing.JLayeredPane;
import org.tlauncher.tlauncher.ui.modpack.GroupPanel;
import java.awt.Container;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import javax.swing.AbstractButton;
import java.awt.Component;
import javax.swing.SpringLayout;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.util.swing.FontTL;
import org.tlauncher.tlauncher.ui.swing.GameRadioButton;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.tlauncher.ui.MainPane;

public class ModpackEnitityScene extends CompleteSubEntityScene
{
    public ModpackEnitityScene(final MainPane main) {
        super(main);
    }
    
    public void showModpackEntity(final GameEntityDTO completeGameEntity) {
        this.showFullGameEntity(completeGameEntity, GameType.MODPACK);
        final ModpackDTO modPack = (ModpackDTO)completeGameEntity;
        final GameRadioButton modReview = new GameRadioButton("modpack.complete.review.mod");
        modReview.setActionCommand(GameType.MOD.toString());
        final GameRadioButton resourceReview = new GameRadioButton("modpack.complete.review.resource");
        resourceReview.setActionCommand(GameType.RESOURCEPACK.toString());
        final GameRadioButton mapReview = new GameRadioButton("modpack.complete.review.map");
        mapReview.setActionCommand(GameType.MAP.toString());
        SwingUtil.changeFontFamily(mapReview, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(modReview, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(resourceReview, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
        final GroupPanel centerButtons = this.fullGameEntity.getCenterButtons();
        final SpringLayout centerLayout = (SpringLayout)centerButtons.getLayout();
        SwingUtil.configHorizontalSpingLayout(centerLayout, modReview, centerButtons, 130);
        centerLayout.putConstraint("West", modReview, 456, "West", centerButtons);
        centerLayout.putConstraint("East", modReview, 586, "West", centerButtons);
        centerButtons.addInGroup(modReview);
        SwingUtil.configHorizontalSpingLayout(centerLayout, resourceReview, modReview, 130);
        centerButtons.addInGroup(resourceReview);
        SwingUtil.configHorizontalSpingLayout(centerLayout, mapReview, resourceReview, 130);
        centerButtons.addInGroup(mapReview);
        final ModpackVersionDTO version = (ModpackVersionDTO)modPack.getVersion();
        final JPanel centerView = this.fullGameEntity.getCenterView();
        this.addModpackTableElement(version, GameType.MOD, completeGameEntity);
        this.addModpackTableElement(version, GameType.RESOURCEPACK, completeGameEntity);
        this.addModpackTableElement(version, GameType.MAP, completeGameEntity);
        modReview.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ((CardLayout)centerView.getLayout()).show(centerView, GameType.MOD.toString());
            }
        });
        resourceReview.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ((CardLayout)centerView.getLayout()).show(centerView, GameType.RESOURCEPACK.toString());
            }
        });
        mapReview.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ((CardLayout)centerView.getLayout()).show(centerView, GameType.MAP.toString());
            }
        });
        ((CardLayout)centerView.getLayout()).show(centerView, "REVIEW");
    }
    
    private void addModpackTableElement(final ModpackVersionDTO versionDTO, final GameType type, final GameEntityDTO entity) {
        final Component table = ModpackScene.createScrollWrapper(new GameEntityTable(versionDTO.getByType(type), entity, type));
        if (versionDTO.getByType(type).size() == 0) {
            final JLayeredPane panel = new JLayeredPane();
            final LocalizableLabel emptyLabel = new LocalizableLabel("modpack.table.empty." + type.toString());
            emptyLabel.setHorizontalAlignment(0);
            emptyLabel.setAlignmentX(0.0f);
            SwingUtil.changeFontFamily(emptyLabel, FontTL.ROBOTO_BOLD, 18, ColorUtil.COLOR_16);
            panel.add(emptyLabel, 1);
            panel.add(table, 0);
            emptyLabel.setBounds(0, 160, MainPane.SIZE.width, 22);
            table.setBounds(0, 0, MainPane.SIZE.width, 321);
            this.fullGameEntity.getCenterView().add(panel, type.toString());
        }
        else {
            this.fullGameEntity.getCenterView().add(table, type.toString());
        }
    }
    
    private class RemoteEntityModel extends BaseSubtypeModel<BaseModelElement>
    {
        private SimpleDateFormat format;
        
        RemoteEntityModel(final List<? extends GameEntityDTO> list, final GameType type) {
            this.format = new SimpleDateFormat("dd MMMM YYYY", Localizable.get().getSelected());
            final ModpackComboBox modpackComboBox = TLauncher.getInstance().getFrame().mp.modpackScene.localmodpacks;
            for (final GameEntityDTO entity : list) {
                GameEntityDTO remoteGameEntity = null;
                for (final GameEntityDTO dto : ModpackEnitityScene.this.manager.getInfoMod().getByType(type)) {
                    if (dto.getId().equals(entity.getId())) {
                        remoteGameEntity = dto;
                        break;
                    }
                }
                if (remoteGameEntity == null) {
                    U.log("[RemoteEntityModel]", "don't find entity" + entity);
                }
                else {
                    final ModpackTableInstallButton button = new ModpackTableInstallButton(remoteGameEntity, type, modpackComboBox);
                    this.list.add((T)new BaseModelElement(button, entity));
                }
            }
        }
        
        @Override
        public int getRowCount() {
            return this.list.size();
        }
        
        @Override
        public int getColumnCount() {
            return 6;
        }
        
        @Override
        public Object getValueAt(final int rowIndex, final int columnIndex) {
            switch (columnIndex) {
                case 0: {
                    return this.format.format(this.list.get(rowIndex).getEntity().getVersion().getUpdateDate());
                }
                case 1: {
                    return this.list.get(rowIndex).getEntity().getName();
                }
                case 2: {
                    return this.list.get(rowIndex).getEntity().getAuthor();
                }
                case 3: {
                    return this.list.get(rowIndex).getEntity().getVersion().getName();
                }
                case 4: {
                    return this.list.get(rowIndex).getEntity().getVersion().getType();
                }
                case 5: {
                    this.list.get(rowIndex).getModpackActButton().initButton();
                    return this.list.get(rowIndex).getModpackActButton();
                }
                default: {
                    return null;
                }
            }
        }
        
        @Override
        public String getColumnName(final int column) {
            String line = "";
            switch (column) {
                case 0: {
                    line = Localizable.get("version.manager.editor.field.time");
                    return line.substring(0, line.length() - 1);
                }
                case 1: {
                    return Localizable.get("modpack.table.pack.element.name");
                }
                case 2: {
                    return Localizable.get("modpack.table.pack.element.author");
                }
                case 3: {
                    return Localizable.get("version.release");
                }
                case 4: {
                    line = Localizable.get("version.manager.editor.field.type");
                    return line.substring(0, line.length() - 1);
                }
                case 5: {
                    return Localizable.get("modpack.table.pack.element.operation");
                }
                default: {
                    return "";
                }
            }
        }
        
        @Override
        public GameEntityDTO getRowObject(final int rowIndex) {
            return this.list.get(rowIndex).getEntity();
        }
        
        @Override
        public Class<?> getColumnClass(final int columnIndex) {
            if (columnIndex == 5) {
                return BaseModelElement.class;
            }
            return super.getColumnClass(columnIndex);
        }
        
        @Override
        public boolean isCellEditable(final int rowIndex, final int columnIndex) {
            return columnIndex == 5;
        }
    }
    
    private class GameEntityTable extends ModpackTable
    {
        GameEntityTable(final List<? extends GameEntityDTO> list, final GameEntityDTO parent, final GameType type) {
            super(new RemoteEntityModel(list, type));
            this.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(final ListSelectionEvent e) {
                    if (GameEntityTable.this.getSelectedRow() == -1) {
                        return;
                    }
                    final int column = GameEntityTable.this.getSelectedColumn();
                    if (column != 5) {
                        final GameEntityDTO gameEntity = ((ModpackEnitityScene.BaseSubtypeModel)GameEntityTable.this.getModel()).getRowObject(GameEntityTable.this.getSelectedRow());
                        ModpackEnitityScene.this.manager.showInnerModpackElement(gameEntity, parent, type);
                    }
                    GameEntityTable.this.getSelectionModel().clearSelection();
                }
            });
            ModpackEnitityScene.this.manager.addGameListener(type, (GameEntityListener)this.getModel());
        }
    }
    
    private abstract class BaseSubtypeModel<T extends BaseModelElement> extends GameEntityTableModel
    {
        protected List<T> list;
        
        private BaseSubtypeModel() {
            this.list = new ArrayList<T>();
        }
        
        public abstract GameEntityDTO getRowObject(final int p0);
        
        public T find(final GameEntityDTO entity) {
            for (final T el : this.list) {
                if (entity.getId().equals(el.getEntity().getId())) {
                    return el;
                }
            }
            return null;
        }
        
        @Override
        public void processingStarted(final GameEntityDTO e, final VersionDTO version) {
            final BaseModelElement baseModelElement = this.find(e);
            if (baseModelElement != null) {
                baseModelElement.getModpackActButton().setTypeButton("PROCESSING");
            }
        }
        
        @Override
        public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
            final BaseModelElement baseModelElement = this.find(e);
            if (baseModelElement != null) {
                baseModelElement.getModpackActButton().reset();
            }
        }
        
        @Override
        public void installEntity(final GameEntityDTO e, final GameType type) {
            final BaseModelElement baseModelElement = this.find(e);
            if (baseModelElement != null) {
                baseModelElement.getModpackActButton().setTypeButton("REMOVE");
            }
        }
        
        @Override
        public void removeEntity(final GameEntityDTO e) {
            final BaseModelElement baseModelElement = this.find(e);
            if (baseModelElement != null) {
                baseModelElement.getModpackActButton().setTypeButton("INSTALL");
            }
        }
    }
}
