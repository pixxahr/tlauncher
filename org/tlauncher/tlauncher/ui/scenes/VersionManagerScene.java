// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import java.awt.Component;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.versions.VersionHandler;

public class VersionManagerScene extends PseudoScene
{
    private static final long serialVersionUID = 758826812081732720L;
    final VersionHandler handler;
    
    public VersionManagerScene(final MainPane main) {
        super(main);
        this.handler = new VersionHandler(this);
        this.add(this.handler.list);
    }
    
    @Override
    public void onResize() {
        super.onResize();
        this.handler.list.setLocation(this.getWidth() / 2 - this.handler.list.getWidth() / 2, this.getHeight() / 2 - this.handler.list.getHeight() / 2);
    }
}
