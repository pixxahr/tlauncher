// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import javax.swing.BoxLayout;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityPanel;
import org.tlauncher.util.OS;
import java.util.Arrays;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.util.concurrent.atomic.AtomicInteger;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import org.tlauncher.tlauncher.ui.button.StateModpackElementButton;
import org.tlauncher.tlauncher.ui.listener.mods.UpdateGameListener;
import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.ui.text.ExtendedTextField;
import javax.swing.JScrollPane;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.tlauncher.ui.modpack.filter.NameFilter;
import org.tlauncher.tlauncher.ui.modpack.filter.Filter;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import org.tlauncher.tlauncher.ui.loc.modpack.ShadowButton;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.tlauncher.ui.loc.LocalizableHTMLLabel;
import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.minecraft.crash.Crash;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import java.util.Iterator;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import org.tlauncher.modpack.domain.client.SubModpackDTO;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import java.awt.Container;
import org.tlauncher.util.U;
import org.tlauncher.modpack.domain.client.share.InfoMod;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.event.DocumentListener;
import org.tlauncher.tlauncher.ui.listener.mods.DeferredDocumentListener;
import org.tlauncher.tlauncher.ui.loc.LocalizableTextField;
import javax.swing.table.TableModel;
import javax.swing.SortOrder;
import javax.swing.RowSorter;
import java.util.ArrayList;
import java.util.Comparator;
import javax.swing.table.TableRowSorter;
import org.tlauncher.tlauncher.ui.modpack.right.panel.RightTableModel;
import javax.swing.plaf.ScrollBarUI;
import org.tlauncher.tlauncher.ui.ui.ModpackScrollBarUI;
import org.tlauncher.tlauncher.ui.swing.ScrollPane;
import org.tlauncher.modpack.domain.client.GameVersionDTO;
import java.util.List;
import org.tlauncher.tlauncher.ui.server.BackPanel;
import javax.swing.JLabel;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityListener;
import javax.swing.event.PopupMenuEvent;
import java.awt.event.ActionListener;
import javax.swing.event.PopupMenuListener;
import java.awt.Point;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import org.tlauncher.tlauncher.ui.modpack.ModpackConfigFrame;
import net.minecraft.launcher.versions.CompleteVersion;
import java.awt.MouseInfo;
import javax.swing.JPopupMenu;
import org.tlauncher.tlauncher.ui.menu.ModpackPopup;
import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JFrame;
import org.tlauncher.tlauncher.ui.modpack.ModpackCreation;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.JComponent;
import javax.swing.Box;
import org.tlauncher.tlauncher.ui.swing.ImageButton;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import org.tlauncher.tlauncher.ui.swing.renderer.CategoryListRenderer;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.swing.renderer.UserCategoryListRenderer;
import org.tlauncher.tlauncher.ui.swing.renderer.ModpackComboxRenderer;
import java.awt.Component;
import javax.swing.plaf.ComboBoxUI;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.Rectangle;
import org.tlauncher.tlauncher.ui.ui.ModpackComboBoxUI;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.loc.UpdaterFullButton;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.BorderLayout;
import javax.swing.SpringLayout;
import java.awt.FlowLayout;
import java.awt.image.ImageObserver;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.CardLayout;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.event.ItemEvent;
import javax.swing.ComboBoxModel;
import org.tlauncher.tlauncher.ui.listener.mods.ModpackBoxListener;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.modpack.domain.client.share.Category;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import org.tlauncher.tlauncher.managers.ModpackManager;
import java.awt.event.ItemListener;
import org.tlauncher.tlauncher.ui.swing.GameRadioTextButton;
import com.google.inject.Injector;
import org.tlauncher.tlauncher.ui.modpack.right.panel.GameEntityRightPanel;
import javax.swing.JPanel;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.modpack.domain.client.share.GameType;
import java.awt.Color;
import java.awt.Dimension;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListener;

public class ModpackScene extends PseudoScene implements MinecraftListener
{
    public static final Dimension SIZE;
    public static final Color UP_BACKGROUND_PANEL_COLOR;
    public static final Color BACKGROUND_RIGHT_UNDER_PANEL;
    private static final GameType[] GAME_TYPES;
    public static int LEFT_WIDTH;
    public static int RIGHT_WIDTH;
    public static String EMPTY;
    public static String NOT_EMPTY;
    protected final ModpackComboBox localmodpacks;
    private final ExtendedPanel panel;
    private final JPanel layoutCenterPanel;
    private final GameEntityRightPanel modpacksPanel;
    private final GameEntityRightPanel modsPanel;
    private final GameEntityRightPanel resourcePackPanel;
    private final GameEntityRightPanel mapsPanel;
    private final ExtendedPanel entitiesPanel;
    private final Injector injector;
    private final GameEntityLeftPanel modLeftPanel;
    private final GameEntityLeftPanel mapLeftPanel;
    private final GameEntityLeftPanel resourceLeftPanel;
    private final GameRadioTextButton modpacks;
    private final ItemListener modpackBoxListener;
    private final SearchPanel search;
    private ModpackManager manager;
    private GameType current;
    private GameType last;
    private ButtonGroup group;
    private JComboBox<Category> categoriesBox;
    private JComboBox<UserCategory> sortBox;
    
    public ModpackScene(final MainPane main) {
        super(main);
        this.localmodpacks = new ModpackComboBox();
        this.panel = new ExtendedPanel();
        this.modLeftPanel = new GameEntityLeftPanel();
        this.mapLeftPanel = new GameEntityLeftPanel();
        this.resourceLeftPanel = new GameEntityLeftPanel();
        this.modpackBoxListener = new ModpackBoxListener();
        this.current = GameType.MODPACK;
        this.group = new ButtonGroup();
        this.search = new SearchPanel();
        this.categoriesBox = new JComboBox<Category>() {
            private ItemListener listener = e -> {
                if (e.getStateChange() == 1) {
                    ModpackScene.this.prepareRightPanel();
                }
            };
            
            @Override
            public void setModel(final ComboBoxModel<Category> aModel) {
                this.removeItemListener(this.listener);
                super.setModel(aModel);
                this.addItemListener(this.listener);
            }
        };
        this.modpacksPanel = new GameEntityRightPanel(this.localmodpacks, this.search, this.categoriesBox, GameType.MODPACK);
        this.modsPanel = new GameEntityRightPanel(this.localmodpacks, this.search, this.categoriesBox, GameType.MOD);
        this.resourcePackPanel = new GameEntityRightPanel(this.localmodpacks, this.search, this.categoriesBox, GameType.RESOURCEPACK);
        this.mapsPanel = new GameEntityRightPanel(this.localmodpacks, this.search, this.categoriesBox, GameType.MAP);
        this.injector = TLauncher.getInjector();
        this.layoutCenterPanel = new JPanel(new CardLayout(0, 0));
        this.manager = (ModpackManager)this.injector.getInstance((Class)ModpackManager.class);
        this.localmodpacks.setPreferredSize(new Dimension(200, 30));
        final ExtendedPanel buttons = new ExtendedPanel() {
            @Override
            protected void paintComponent(final Graphics g0) {
                g0.drawImage(ImageCache.getBufferedImage("modpack-top2-background.png"), 0, 0, null);
            }
        };
        buttons.setLayout(new FlowLayout(0, 0, 0));
        final ExtendedPanel center = new ExtendedPanel();
        this.entitiesPanel = new ExtendedPanel(new CardLayout(0, 0));
        this.panel.setSize(ModpackScene.SIZE);
        final SpringLayout spring = new SpringLayout();
        this.panel.setLayout(spring);
        this.panel.setOpaque(true);
        center.setLayout(new BorderLayout());
        center.setOpaque(true);
        center.setBackground(Color.WHITE);
        final LocalizableLabel nameModpackLabel = new LocalizableLabel("modpack.button.modpack");
        nameModpackLabel.setHorizontalAlignment(0);
        nameModpackLabel.setForeground(Color.WHITE);
        final LocalizableLabel categoryLabel = new LocalizableLabel("modpack.filter.label");
        categoryLabel.setHorizontalAlignment(0);
        categoryLabel.setForeground(Color.WHITE);
        categoryLabel.setPreferredSize(new Dimension(98, 55));
        final JButton create = new UpdaterFullButton(new Color(54, 153, 208), new Color(30, 136, 195), "modpack.create.button", "create-modpack.png");
        create.setIconTextGap(16);
        final JLabel sortLabel = new LocalizableLabel("modpack.sort.label");
        final JPanel wrapper = new ExtendedPanel(new FlowLayout(1, 0, 0));
        wrapper.setBorder(BorderFactory.createEmptyBorder(7, 0, 8, 0));
        (this.sortBox = new JComboBox<UserCategory>(UserCategory.values())).setUI(new ModpackComboBoxUI() {
            @Override
            public void paintCurrentValue(final Graphics g, final Rectangle bounds, final boolean hasFocus) {
                g.setColor(ModpackScene$3.BACKGROUND_BOX);
                g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
                final String name = Localizable.get("modpack." + ((UserCategory)ModpackScene.this.sortBox.getSelectedItem()).name().toLowerCase());
                this.paintText(g, bounds, name);
            }
        });
        wrapper.add(this.sortBox);
        this.sortBox.setBorder(BorderFactory.createLineBorder(ModpackComboxRenderer.LINE, 1));
        this.sortBox.setRenderer(new UserCategoryListRenderer());
        this.categoriesBox.setPreferredSize(new Dimension(192, 40));
        final CategoryListRenderer categoryListRenderer = new CategoryListRenderer();
        categoryListRenderer.setBackground(new Color(59, 165, 225));
        this.categoriesBox.setRenderer(categoryListRenderer);
        this.categoriesBox.setBorder(BorderFactory.createLineBorder(ModpackComboxRenderer.LINE, 1));
        final Color categoryBackground = new Color(59, 165, 225);
        this.categoriesBox.setUI(new ModpackComboBoxUI() {
            @Override
            public void paintCurrentValue(final Graphics g, final Rectangle bounds, final boolean hasFocus) {
                g.setColor(categoryBackground);
                g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
                final Object ob = ModpackScene.this.categoriesBox.getSelectedItem();
                if (ob == null) {
                    return;
                }
                final String name = Localizable.get("modpack." + ((Category)ob).name().toLowerCase());
                this.paintText(g, bounds, name);
            }
            
            @Override
            protected JButton createArrowButton() {
                final JButton b = super.createArrowButton();
                b.setBackground(categoryBackground);
                return b;
            }
        });
        final GameRadioTextButton mods = new GameRadioTextButton("modpack.button.mod");
        this.modpacks = new GameRadioTextButton("modpack.button.modpack");
        final GameRadioTextButton resourcepacks = new GameRadioTextButton("modpack.button.resourcepack");
        final GameRadioTextButton maps = new GameRadioTextButton("modpack.button.map");
        this.group.add(mods);
        this.group.add(this.modpacks);
        this.group.add(resourcepacks);
        this.group.add(maps);
        final ExtendedButton settings = new ImageButton("settings-modpack.png");
        settings.setPreferredSize(new Dimension(66, 55));
        buttons.add(settings);
        buttons.add(Box.createHorizontalStrut(29));
        buttons.add(this.modpacks, Box.createHorizontalStrut(20));
        buttons.add(mods, Box.createHorizontalStrut(20));
        buttons.add(resourcepacks, Box.createHorizontalStrut(20));
        buttons.add(maps, Box.createHorizontalStrut(80));
        buttons.add(categoryLabel);
        buttons.add(Box.createHorizontalStrut(6));
        buttons.add(this.categoriesBox);
        this.layoutCenterPanel.add(this.entitiesPanel, GameType.NOT_MODPACK.toString());
        final CardLayout emptyRightLayout = new CardLayout();
        final JPanel emptyRightPanel = new ExtendedPanel(emptyRightLayout);
        emptyRightPanel.add(new EmptyRightView(GameType.MODPACK), ModpackScene.EMPTY);
        emptyRightPanel.add(this.modpacksPanel, ModpackScene.NOT_EMPTY);
        this.layoutCenterPanel.add(createScrollWrapper(emptyRightPanel), GameType.MODPACK.toString());
        this.entitiesPanel.add(new GameCenterPanel(this.modLeftPanel, this.modsPanel, GameType.MOD), GameType.MOD.toString());
        this.entitiesPanel.add(new GameCenterPanel(this.resourceLeftPanel, this.resourcePackPanel, GameType.RESOURCEPACK), GameType.RESOURCEPACK.toString());
        this.entitiesPanel.add(new GameCenterPanel(this.mapLeftPanel, this.mapsPanel, GameType.MAP), GameType.MAP.toString());
        center.add(this.layoutCenterPanel, "Center");
        final JPanel upPanel = new JPanel(new FlowLayout(0, 0, 0));
        upPanel.setBackground(new Color(60, 170, 232));
        final BackPanel back = new UniverseBackPanel(new Color(54, 153, 208));
        back.setPreferredSize(new Dimension(66, 55));
        nameModpackLabel.setPreferredSize(new Dimension(114, 55));
        this.localmodpacks.setPreferredSize(new Dimension(172, 36));
        create.setPreferredSize(new Dimension(132, 36));
        this.search.setPreferredSize(new Dimension(194, 55));
        final JPanel gup1 = new JPanel();
        gup1.setPreferredSize(new Dimension(1, 55));
        final Color line = new Color(92, 190, 246);
        gup1.setBackground(line);
        final JPanel gup2 = new JPanel();
        gup2.setPreferredSize(new Dimension(1, 55));
        gup2.setBackground(line);
        sortLabel.setPreferredSize(new Dimension(125, 55));
        sortLabel.setHorizontalAlignment(0);
        this.sortBox.setPreferredSize(new Dimension(192, 40));
        wrapper.setPreferredSize(new Dimension(192, 55));
        upPanel.add(back);
        upPanel.add(nameModpackLabel);
        upPanel.add(this.localmodpacks);
        upPanel.add(Box.createHorizontalStrut(16));
        upPanel.add(create);
        upPanel.add(Box.createHorizontalStrut(34));
        upPanel.add(gup1);
        upPanel.add(this.search);
        upPanel.add(gup2);
        upPanel.add(sortLabel);
        upPanel.add(wrapper);
        spring.putConstraint("West", upPanel, 0, "West", this.panel);
        spring.putConstraint("East", upPanel, 0, "East", this.panel);
        spring.putConstraint("North", upPanel, 0, "North", this.panel);
        spring.putConstraint("South", upPanel, 55, "North", this.panel);
        this.panel.add(upPanel);
        spring.putConstraint("West", buttons, 0, "West", this.panel);
        spring.putConstraint("East", buttons, 0, "East", this.panel);
        spring.putConstraint("North", buttons, 0, "South", upPanel);
        spring.putConstraint("South", buttons, 55, "South", upPanel);
        this.panel.add(buttons);
        spring.putConstraint("West", center, 0, "West", this.panel);
        spring.putConstraint("East", center, 0, "East", this.panel);
        spring.putConstraint("North", center, 110, "North", this.panel);
        spring.putConstraint("South", center, 0, "South", this.panel);
        this.panel.add(center);
        this.add(this.panel);
        SwingUtil.changeFontFamily(this.localmodpacks, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(this.search, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(this.sortBox, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(this.categoriesBox, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(nameModpackLabel, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(create, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(sortLabel, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        SwingUtil.changeFontFamily(mods, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(this.modpacks, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(resourcepacks, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(maps, FontTL.ROBOTO_REGULAR, 14);
        SwingUtil.changeFontFamily(categoryLabel, FontTL.ROBOTO_MEDIUM, 14, Color.WHITE);
        final List<GameVersionDTO> list;
        create.addActionListener(e -> {
            list = this.manager.getInfoMod().getGameVersions();
            if (list.isEmpty()) {
                Alert.showLocMessage("modpack.internet.update");
                return;
            }
            else {
                new ModpackCreation(this.getMainPane().getRootFrame(), list).setVisible(true);
                return;
            }
        });
        create.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
            }
        });
        this.modpacks.addActionListener(e -> {
            this.current = GameType.MODPACK;
            this.categoriesBox.setModel(new DefaultComboBoxModel<Category>(Category.getCategories(this.current)));
            this.prepareRightPanel();
            return;
        });
        mods.addActionListener(e -> {
            this.current = GameType.MOD;
            this.categoriesBox.setModel(new DefaultComboBoxModel<Category>(Category.getCategories(this.current)));
            this.prepareRightPanel();
            return;
        });
        resourcepacks.addActionListener(e -> {
            this.current = GameType.RESOURCEPACK;
            this.categoriesBox.setModel(new DefaultComboBoxModel<Category>(Category.getCategories(this.current)));
            this.prepareRightPanel();
            return;
        });
        maps.addActionListener(e -> {
            this.current = GameType.MAP;
            this.categoriesBox.setModel(new DefaultComboBoxModel<Category>(Category.getCategories(this.current)));
            this.prepareRightPanel();
            return;
        });
        this.sortBox.addItemListener(e -> {
            if (e.getStateChange() == 1) {
                this.setSorters();
                this.getRightPanelByType(this.current).filterRightPanel(this.current);
            }
            return;
        });
        this.localmodpacks.addItemListener(e -> {
            if (e.getStateChange() == 1) {
                if (this.current != GameType.MODPACK) {
                    this.prepareRightPanel();
                }
                TLauncher.getInstance().getConfiguration().set("modpack.combobox.index", this.localmodpacks.getSelectedIndex());
            }
            return;
        });
        settings.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    final JPopupMenu menu = new ModpackPopup(ModpackScene.this.localmodpacks);
                    menu.show(settings, e.getX(), e.getY());
                }
            }
        });
        final ActionListener modpackListener = e -> SwingUtilities.invokeLater(new Runnable() {
            final /* synthetic */ GameRadioTextButton val$mods;
            final /* synthetic */ ExtendedPanel val$buttons;
            
            @Override
            public void run() {
                this.val$mods.repaint();
                final Point point = MouseInfo.getPointerInfo().getLocation();
                SwingUtilities.convertPointFromScreen(point, ModpackScene.this.localmodpacks);
                if (point.getX() < ModpackScene.this.localmodpacks.getWidth() && point.getX() > ModpackScene.this.localmodpacks.getWidth() - 40 && ModpackScene.this.localmodpacks.getSelectedIndex() > 0) {
                    final ModpackConfigFrame modpackConfigFrame = new ModpackConfigFrame(TLauncher.getInstance().getFrame(), ModpackScene.this.localmodpacks.getSelectedValue());
                    modpackConfigFrame.setVisible(true);
                    modpackConfigFrame.addComponentListener(new ComponentAdapter() {
                        @Override
                        public void componentHidden(final ComponentEvent e) {
                            Runnable.this.val$buttons.repaint();
                        }
                    });
                }
            }
        });
        this.localmodpacks.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(final PopupMenuEvent e) {
                ModpackScene.this.localmodpacks.addActionListener(modpackListener);
            }
            
            @Override
            public void popupMenuWillBecomeInvisible(final PopupMenuEvent e) {
                ModpackScene.this.localmodpacks.removeActionListener(modpackListener);
            }
            
            @Override
            public void popupMenuCanceled(final PopupMenuEvent e) {
                ModpackScene.this.localmodpacks.removeActionListener(modpackListener);
            }
        });
        this.manager.addGameListener(GameType.MOD, this.modLeftPanel);
        this.manager.addGameListener(GameType.MOD, this.modsPanel);
        this.manager.addGameListener(GameType.RESOURCEPACK, this.resourceLeftPanel);
        this.manager.addGameListener(GameType.RESOURCEPACK, this.resourcePackPanel);
        this.manager.addGameListener(GameType.MAP, this.mapLeftPanel);
        this.manager.addGameListener(GameType.MAP, this.mapsPanel);
        this.manager.addGameListener(GameType.MODPACK, this.localmodpacks);
        this.manager.addGameListener(GameType.MODPACK, this.modpacksPanel);
    }
    
    public static ScrollPane createScrollWrapper(final JComponent panel) {
        final ScrollPane pane = new ScrollPane(panel, ScrollPane.ScrollBarPolicy.AS_NEEDED, ScrollPane.ScrollBarPolicy.NEVER);
        pane.getVerticalScrollBar().setUnitIncrement(30);
        pane.getVerticalScrollBar().setPreferredSize(new Dimension(13, 0));
        pane.setPreferredSize(new Dimension(200, 100));
        pane.getVerticalScrollBar().setUI(new ModpackScrollBarUI());
        pane.setBorder(BorderFactory.createEmptyBorder());
        return pane;
    }
    
    private void setSorters() {
        for (final GameType type : ModpackScene.GAME_TYPES) {
            final GameEntityRightPanel panel = this.getRightPanelByType(type);
            final TableRowSorter<RightTableModel> sorter = new TableRowSorter<RightTableModel>((RightTableModel)panel.getModel());
            sorter.setComparator(0, new UserCategoryComparator((UserCategory)this.sortBox.getSelectedItem()));
            final List<RowSorter.SortKey> key = new ArrayList<RowSorter.SortKey>();
            key.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
            sorter.setSortKeys(key);
            panel.setRowSorter((RowSorter<? extends TableModel>)sorter);
            sorter.sort();
        }
        this.getRightPanelByType(this.current).editCellAt(0, 0);
    }
    
    private void createTextField(final LocalizableTextField search, final DeferredDocumentListener listener) {
        search.getDocument().addDocumentListener(listener);
        search.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(final FocusEvent e) {
                listener.start();
            }
            
            @Override
            public void focusLost(final FocusEvent e) {
                listener.stop();
            }
        });
    }
    
    public void prepareView(final InfoMod infoMod, final List<CompleteVersion> modpackVersions) {
        final GameType[] game_TYPES;
        int length;
        int i = 0;
        GameType type;
        SwingUtilities.invokeLater(() -> {
            U.debug("prepare view modpack");
            this.prepareLocalModpack(modpackVersions);
            U.debug("prepared local modpack");
            game_TYPES = ModpackScene.GAME_TYPES;
            for (length = game_TYPES.length; i < length; ++i) {
                type = game_TYPES[i];
                this.getRightPanelByType(type).addElements(infoMod.getByType(type), type);
            }
            this.setSorters();
            this.setDefault();
            U.debug("fill view by game entities ");
            U.gc();
        });
    }
    
    private void changeEntityView(final GameType gameType) {
        if (this.last == GameType.MODPACK && gameType != GameType.MODPACK) {
            ((CardLayout)this.layoutCenterPanel.getLayout()).show(this.layoutCenterPanel, GameType.NOT_MODPACK.toString());
        }
        else if (gameType == GameType.MODPACK) {
            ((CardLayout)this.layoutCenterPanel.getLayout()).show(this.layoutCenterPanel, GameType.MODPACK.toString());
            this.last = gameType;
            return;
        }
        this.last = this.current;
        ((CardLayout)this.entitiesPanel.getLayout()).show(this.entitiesPanel, gameType.toString());
    }
    
    private GameEntityRightPanel getRightPanelByType(final GameType type) {
        switch (type) {
            case MOD: {
                return this.modsPanel;
            }
            case MAP: {
                return this.mapsPanel;
            }
            case RESOURCEPACK: {
                return this.resourcePackPanel;
            }
            case MODPACK: {
                return this.modpacksPanel;
            }
            default: {
                throw new NullPointerException();
            }
        }
    }
    
    private void fillLeftPanel() {
        if (this.current == GameType.MODPACK) {
            return;
        }
        if (this.localmodpacks.getSelectedIndex() < 1) {
            this.getLeftPanelByType(this.current).cleanLeftPanel();
            this.getLeftPanelByType(this.current).fireCounterChanged();
        }
        else {
            this.manager.checkFolderSubGameEntity(this.localmodpacks.getSelectedValue(), this.current);
            final ModpackVersionDTO version = (ModpackVersionDTO)this.localmodpacks.getSelectedValue().getModpack().getVersion();
            this.getLeftPanelByType(this.current).addElements((List<? extends SubModpackDTO>)version.getByType(this.current), this.current);
        }
        this.getLeftPanelByType(this.current).revalidate();
        this.getLeftPanelByType(this.current).repaint();
    }
    
    private GameEntityLeftPanel getLeftPanelByType(final GameType type) {
        switch (type) {
            case MOD: {
                return this.modLeftPanel;
            }
            case MAP: {
                return this.mapLeftPanel;
            }
            case RESOURCEPACK: {
                return this.resourceLeftPanel;
            }
            default: {
                throw new NullPointerException();
            }
        }
    }
    
    @Override
    public void onResize() {
        super.onResize();
        this.panel.setLocation(this.getWidth() / 2 - this.panel.getWidth() / 2, (this.getHeight() - LoginForm.LOGIN_SIZE.height) / 2 - this.panel.getHeight() / 2);
    }
    
    public void prepareLocalModpack(final List<CompleteVersion> modpackVersions) {
        final int index = TLauncher.getInstance().getConfiguration().getInteger("modpack.combobox.index");
        this.localmodpacks.removeAllItems();
        for (final CompleteVersion v : modpackVersions) {
            this.localmodpacks.addItem(v);
        }
        if (this.localmodpacks.getModel().getSize() > index) {
            this.localmodpacks.setSelectedIndex(index);
        }
    }
    
    @Override
    public void setShown(final boolean shown, final boolean animate) {
        if (shown) {
            this.localmodpacks.addItemListener(this.modpackBoxListener);
        }
        else {
            this.localmodpacks.removeItemListener(this.modpackBoxListener);
        }
        super.setShown(shown, animate);
    }
    
    public void setDefault() {
        this.modpacks.setSelected(true);
        this.current = GameType.MODPACK;
        this.last = GameType.MODPACK;
        this.categoriesBox.setModel(new DefaultComboBoxModel<Category>(Category.getCategories(this.current)));
        this.prepareRightPanel();
    }
    
    @Override
    public void setShown(final boolean shown) {
        super.setShown(shown);
    }
    
    public CompleteVersion getSelectedCompleteVersion() {
        return this.localmodpacks.getSelectedValue();
    }
    
    public boolean isSelectedCompleteVersion() {
        return this.localmodpacks.getSelectedIndex() > 0;
    }
    
    public CompleteVersion getCompleteVersion(final ModpackDTO dto, final VersionDTO versionDTO) {
        return this.localmodpacks.findByModpack(dto, versionDTO);
    }
    
    private void prepareRightPanel() {
        this.getRightPanelByType(this.current).filterRightPanel(this.current);
        this.fillLeftPanel();
        this.changeEntityView(this.current);
    }
    
    public GameType getCurrent() {
        return this.current;
    }
    
    @Override
    public void onMinecraftPrepare() {
    }
    
    @Override
    public void onMinecraftAbort() {
    }
    
    @Override
    public void onMinecraftLaunch() {
    }
    
    @Override
    public void onMinecraftClose() {
        if (this.current != GameType.MODPACK) {
            this.fillLeftPanel();
        }
    }
    
    @Override
    public void onMinecraftError(final Throwable e) {
    }
    
    @Override
    public void onMinecraftKnownError(final MinecraftException e) {
    }
    
    @Override
    public void onMinecraftCrash(final Crash crash) {
    }
    
    static {
        SIZE = new Dimension(MainPane.SIZE.width, 585);
        UP_BACKGROUND_PANEL_COLOR = new Color(60, 170, 252);
        BACKGROUND_RIGHT_UNDER_PANEL = new Color(241, 241, 241);
        GAME_TYPES = new GameType[] { GameType.MODPACK, GameType.MOD, GameType.RESOURCEPACK, GameType.MAP };
        ModpackScene.LEFT_WIDTH = 234;
        ModpackScene.RIGHT_WIDTH = ModpackScene.SIZE.width - ModpackScene.LEFT_WIDTH;
        ModpackScene.EMPTY = "EMPTY";
        ModpackScene.NOT_EMPTY = "NOT_EMPTY";
    }
    
    public enum UserCategory
    {
        POPULATE_MONTH, 
        FAVORITE, 
        NAME, 
        POPULATE_ALL_TIME, 
        DATE;
    }
    
    private class EmptyRightView extends ExtendedPanel
    {
        LocalizableHTMLLabel jLabel;
        private GameType gameType;
        
        public EmptyRightView(final GameType gameType) {
            this.gameType = gameType;
            this.setLayout(new BorderLayout());
            (this.jLabel = new LocalizableHTMLLabel("") {
                @Override
                public void updateLocale() {
                    EmptyRightView.this.updateText();
                }
            }).setHorizontalAlignment(0);
            this.jLabel.setAlignmentY(0.0f);
            SwingUtil.setFontSize(this.jLabel, 18.0f, 1);
            this.add(this.jLabel, "Center");
            this.updateText();
        }
        
        public void updateText() {
            final String value = Localizable.get("modpack.criteria.not.found." + this.gameType);
            String additional = "";
            if (!this.gameType.equals(GameType.MODPACK) && ModpackScene.this.localmodpacks.getSelectedIndex() > 0) {
                additional = Localizable.get("modpack.search.without.modpack", Localizable.get("modpack.local.box.default"));
            }
            final String text = value + additional;
            this.jLabel.setText(String.format("<div WIDTH=%d><center>%s</center></div>", 600, text));
        }
    }
    
    class GameCenterPanel extends ExtendedPanel
    {
        Color color241;
        private String COLLAPSE;
        private String NOT_COLLAPSE;
        
        public GameCenterPanel(final GameEntityLeftPanel leftEntityPanel, final GameEntityRightPanel rightPanel, final GameType gameType) {
            this.color241 = new Color(241, 241, 241);
            this.COLLAPSE = "0";
            this.NOT_COLLAPSE = "1";
            this.setLayout(new BorderLayout(0, 0));
            final ShadowButton collapse = new ShadowButton(this.color241, ColorUtil.COLOR_215, "modpack.element.collapse", "left-array-collapse.png");
            collapse.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 0));
            collapse.setPreferredSize(new Dimension(ModpackScene.LEFT_WIDTH, 39));
            collapse.setIconTextGap(15);
            collapse.setForeground(Color.BLACK);
            final JButton unCollapse = new ImageUdaterButton(new Color(200, 200, 200), new Color(200, 200, 200), "un-collapse-arrow.png", "un-collapse-arrow-up.png");
            final JPanel leftPanel = new JPanel(new BorderLayout());
            leftPanel.setBackground(Color.WHITE);
            final JPanel search = new JPanel(new BorderLayout());
            search.setBackground(Color.WHITE);
            final LocalizableTextField field = new LocalizableTextField("modpack.search.textfield." + gameType.toString()) {
                @Override
                protected void onFocusLost() {
                    super.onFocusLost();
                    this.setForeground(Color.BLACK);
                }
                
                @Override
                protected void onFocusGained() {
                    super.onFocusGained();
                    this.setForeground(Color.BLACK);
                }
                
                @Override
                public void updateLocale() {
                    super.updateLocale();
                    this.setForeground(Color.BLACK);
                }
            };
            final JLabel searchLabel = new JLabel(ImageCache.getNativeIcon("search-left-panel.png"));
            searchLabel.setPreferredSize(new Dimension(38, 38));
            search.setPreferredSize(new Dimension(0, 38));
            field.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 7));
            search.add(field, "Center");
            search.add(searchLabel, "West");
            leftPanel.add(search, "North");
            final CardLayout emptyLayout = new CardLayout();
            final JPanel emptyPanel = new ExtendedPanel(emptyLayout);
            emptyPanel.add(new EmptyGameEntityView(gameType), ModpackScene.EMPTY);
            final JScrollPane pane = ModpackScene.createScrollWrapper(leftEntityPanel);
            pane.setPreferredSize(new Dimension(ModpackScene.LEFT_WIDTH, this.getPreferredSize().height));
            emptyPanel.add(pane, ModpackScene.NOT_EMPTY);
            final CardLayout cardLayout;
            final Container container;
            leftEntityPanel.addCounterListener(currentCounter -> {
                if (currentCounter == 0) {
                    cardLayout.show(container, ModpackScene.EMPTY);
                }
                else {
                    cardLayout.show(container, ModpackScene.NOT_EMPTY);
                }
                return;
            });
            leftEntityPanel.fireCounterChanged();
            leftPanel.add(emptyPanel, "Center");
            leftPanel.add(collapse, "South");
            final CardLayout card = new CardLayout();
            final JPanel leftCenterPanel = new JPanel(card);
            leftCenterPanel.setPreferredSize(new Dimension(ModpackScene.LEFT_WIDTH, 0));
            leftCenterPanel.add(leftPanel, this.NOT_COLLAPSE);
            leftCenterPanel.add(unCollapse, this.COLLAPSE);
            this.add(leftCenterPanel, "West");
            SwingUtil.changeFontFamily(field, FontTL.ROBOTO_REGULAR, 14, new Color(16, 16, 16));
            SwingUtil.changeFontFamily(collapse, FontTL.ROBOTO_BOLD, 14);
            final CardLayout emptyRightLayout = new CardLayout();
            final JPanel emptyRightPanel = new ExtendedPanel(emptyRightLayout);
            final EmptyRightView emptyRightView = new EmptyRightView(gameType);
            emptyRightPanel.add(emptyRightView, ModpackScene.EMPTY);
            emptyRightPanel.add(rightPanel, ModpackScene.NOT_EMPTY);
            this.add(ModpackScene.createScrollWrapper(emptyRightPanel), "Center");
            card.show(leftCenterPanel, this.NOT_COLLAPSE);
            List<? extends GameEntityDTO> list;
            final ExtendedTextField extendedTextField;
            String text;
            final BaseModpackFilter baseModpackFilter;
            List<GameEntityDTO> res;
            final DeferredDocumentListener listener = new DeferredDocumentListener(500, e -> {
                if (ModpackScene.this.localmodpacks.getSelectedIndex() > 0) {
                    list = ((ModpackVersionDTO)ModpackScene.this.localmodpacks.getSelectedValue().getModpack().getVersion()).getByType(gameType);
                    text = extendedTextField.getValue();
                    if (text == null || text.isEmpty()) {
                        leftEntityPanel.addElements((List<? extends SubModpackDTO>)list, gameType);
                    }
                    else {
                        new BaseModpackFilter((Filter<GameEntityDTO>[])new Filter[] { new NameFilter(text) });
                        res = baseModpackFilter.findAll(list);
                        leftEntityPanel.addElements((List<? extends SubModpackDTO>)res, gameType);
                    }
                    leftEntityPanel.revalidate();
                    leftEntityPanel.repaint();
                }
                return;
            }, false);
            collapse.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        card.show(leftCenterPanel, GameCenterPanel.this.COLLAPSE);
                        leftCenterPanel.setPreferredSize(new Dimension(30, 0));
                        rightPanel.setPreferredSize(new Dimension(ModpackScene.SIZE.width - 30, rightPanel.getHeight()));
                    }
                }
            });
            ModpackScene.this.createTextField(field, listener);
            final CardLayout cardLayout2;
            final JComponent component;
            unCollapse.addActionListener(e -> {
                cardLayout2.show(component, this.NOT_COLLAPSE);
                component.setPreferredSize(new Dimension(ModpackScene.LEFT_WIDTH, 0));
                rightPanel.setPreferredSize(new Dimension(ModpackScene.RIGHT_WIDTH, rightPanel.getHeight()));
                rightPanel.revalidate();
                rightPanel.repaint();
                return;
            });
            final EmptyRightView emptyRightView2;
            ModpackScene.this.localmodpacks.addItemListener(e -> {
                if (e.getStateChange() == 1) {
                    if (ModpackScene.this.current != GameType.MODPACK) {
                        ModpackScene.this.prepareRightPanel();
                    }
                    emptyRightView2.updateText();
                }
            });
        }
        
        private class EmptyGameEntityView extends ExtendedPanel implements LocalizableComponent
        {
            private JLabel notHaveSumEntities;
            private JLabel intallThem;
            private GameType gameType;
            
            public EmptyGameEntityView(final GameType gameType) {
                this.gameType = gameType;
                this.setLayout(new FlowLayout(0, 0, 0));
                final JLabel image = new JLabel((Icon)ImageCache.getIcon("empty-left-panel.png", 225, 165));
                image.setHorizontalAlignment(0);
                this.notHaveSumEntities = new LocalizableLabel("modpack.left.empty.mod.label." + gameType.toString());
                this.intallThem = new LocalizableLabel("modpack.left.fast.install");
                this.notHaveSumEntities.setHorizontalAlignment(0);
                this.intallThem.setHorizontalAlignment(0);
                this.notHaveSumEntities.setVerticalAlignment(3);
                this.intallThem.setVerticalAlignment(1);
                this.notHaveSumEntities.setPreferredSize(new Dimension(ModpackScene.LEFT_WIDTH, 40));
                this.intallThem.setPreferredSize(new Dimension(ModpackScene.LEFT_WIDTH, 40));
                SwingUtil.changeFontFamily(this.notHaveSumEntities, FontTL.ROBOTO_BOLD, 14);
                SwingUtil.changeFontFamily(this.intallThem, FontTL.ROBOTO_BOLD, 14);
                this.setBorder(BorderFactory.createEmptyBorder(80, 0, 0, 0));
                this.add(image);
                this.add(this.notHaveSumEntities);
                this.add(this.intallThem);
            }
            
            @Override
            public void updateLocale() {
                this.notHaveSumEntities.setText("modpack.left.empty.mod.label." + this.gameType.toString());
                this.intallThem.setText("modpack.left.fast.install");
            }
        }
    }
    
    class GameLeftElement extends ExtendedPanel implements UpdateGameListener
    {
        private final JLabel name;
        private final StateModpackElementButton clickButton;
        private final Dimension DEFAULT_SIZE;
        private final Color MOUSE_UNDER_BACKGROUND;
        private final Color BACKGROUND_COLOR;
        private SubModpackDTO entity;
        private GameType type;
        private JLabel leftLabel;
        
        public GameLeftElement(final SubModpackDTO e, final GameType type) {
            this.name = new JLabel();
            this.DEFAULT_SIZE = new Dimension(ModpackScene.LEFT_WIDTH, 39);
            this.MOUSE_UNDER_BACKGROUND = new Color(242, 242, 242);
            this.BACKGROUND_COLOR = new Color(235, 235, 235);
            this.type = type;
            this.setPreferredSize(this.DEFAULT_SIZE);
            this.setMaximumSize(this.DEFAULT_SIZE);
            this.setMinimumSize(this.DEFAULT_SIZE);
            this.setLayout(new BorderLayout());
            this.entity = e;
            final SpringLayout layout = new SpringLayout();
            final JPanel wrapperEast = new ExtendedPanel(layout);
            wrapperEast.setPreferredSize(new Dimension(67, 39));
            this.clickButton = new StateModpackElementButton(e, type);
            if (this.entity.isUserInstall()) {
                this.leftLabel = new JLabel((Icon)ImageCache.getIcon("modpack-element-left-hanlde.png"));
            }
            else {
                this.leftLabel = new JLabel((Icon)ImageCache.getIcon("modpack-element-left.png"));
            }
            layout.putConstraint("West", this.clickButton, 0, "West", wrapperEast);
            layout.putConstraint("East", this.clickButton, -16, "East", wrapperEast);
            layout.putConstraint("North", this.clickButton, 10, "North", wrapperEast);
            layout.putConstraint("South", this.clickButton, -9, "South", wrapperEast);
            wrapperEast.add(this.clickButton);
            this.leftLabel.setPreferredSize(new Dimension(39, 29));
            this.add(this.leftLabel, "West");
            final Border nameBorder = BorderFactory.createEmptyBorder(10, 1, 10, 1);
            this.name.setBorder(nameBorder);
            this.add(this.name, "Center");
            if (GameType.MAP != type) {
                this.add(wrapperEast, "East");
            }
            this.initGameEntity();
            this.setOpaque(true);
            SwingUtil.changeFontFamily(this.name, FontTL.ROBOTO_BOLD, 14, ColorUtil.COLOR_16);
            final MouseAdapter click = new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent e) {
                    // 
                    // This method could not be decompiled.
                    // 
                    // Original Bytecode:
                    // 
                    //     1: invokestatic    javax/swing/SwingUtilities.isRightMouseButton:(Ljava/awt/event/MouseEvent;)Z
                    //     4: ifeq            65
                    //     7: new             Lorg/tlauncher/tlauncher/ui/menu/ModpackPopup;
                    //    10: dup            
                    //    11: invokespecial   org/tlauncher/tlauncher/ui/menu/ModpackPopup.<init>:()V
                    //    14: astore_2        /* popupMenu */
                    //    15: new             Lorg/tlauncher/tlauncher/ui/menu/ModpackPopup$ModpackMenuItem;
                    //    18: dup            
                    //    19: ldc             "modpack.popup.delete"
                    //    21: invokespecial   org/tlauncher/tlauncher/ui/menu/ModpackPopup$ModpackMenuItem.<init>:(Ljava/lang/String;)V
                    //    24: astore_3        /* delete */
                    //    25: aload_2         /* popupMenu */
                    //    26: aload_3         /* delete */
                    //    27: invokevirtual   javax/swing/JPopupMenu.add:(Ljavax/swing/JMenuItem;)Ljavax/swing/JMenuItem;
                    //    30: pop            
                    //    31: aload_3         /* delete */
                    //    32: aload_0         /* this */
                    //    33: aload_2         /* popupMenu */
                    //    34: aload_0         /* this */
                    //    35: getfield        org/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement$1.val$type:Lorg/tlauncher/modpack/domain/client/share/GameType;
                    //    38: invokedynamic   BootstrapMethod #0, actionPerformed:(Lorg/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement$1;Ljavax/swing/JPopupMenu;Lorg/tlauncher/modpack/domain/client/share/GameType;)Ljava/awt/event/ActionListener;
                    //    43: invokevirtual   javax/swing/JMenuItem.addActionListener:(Ljava/awt/event/ActionListener;)V
                    //    46: aload_2         /* popupMenu */
                    //    47: aload_1         /* e */
                    //    48: invokevirtual   java/awt/event/MouseEvent.getComponent:()Ljava/awt/Component;
                    //    51: aload_1         /* e */
                    //    52: invokevirtual   java/awt/event/MouseEvent.getX:()I
                    //    55: aload_1         /* e */
                    //    56: invokevirtual   java/awt/event/MouseEvent.getY:()I
                    //    59: invokevirtual   javax/swing/JPopupMenu.show:(Ljava/awt/Component;II)V
                    //    62: goto            103
                    //    65: aload_0         /* this */
                    //    66: getfield        org/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement$1.this$1:Lorg/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement;
                    //    69: invokestatic    org/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement.access$600:(Lorg/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement;)Lorg/tlauncher/modpack/domain/client/SubModpackDTO;
                    //    72: invokevirtual   org/tlauncher/modpack/domain/client/SubModpackDTO.isUserInstall:()Z
                    //    75: ifeq            79
                    //    78: return         
                    //    79: aload_0         /* this */
                    //    80: getfield        org/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement$1.this$1:Lorg/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement;
                    //    83: getfield        org/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement.this$0:Lorg/tlauncher/tlauncher/ui/scenes/ModpackScene;
                    //    86: invokestatic    org/tlauncher/tlauncher/ui/scenes/ModpackScene.access$700:(Lorg/tlauncher/tlauncher/ui/scenes/ModpackScene;)Lorg/tlauncher/tlauncher/managers/ModpackManager;
                    //    89: aload_0         /* this */
                    //    90: getfield        org/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement$1.this$1:Lorg/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement;
                    //    93: invokestatic    org/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement.access$600:(Lorg/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement;)Lorg/tlauncher/modpack/domain/client/SubModpackDTO;
                    //    96: aload_0         /* this */
                    //    97: getfield        org/tlauncher/tlauncher/ui/scenes/ModpackScene$GameLeftElement$1.val$type:Lorg/tlauncher/modpack/domain/client/share/GameType;
                    //   100: invokevirtual   org/tlauncher/tlauncher/managers/ModpackManager.showFullGameEntity:(Lorg/tlauncher/modpack/domain/client/GameEntityDTO;Lorg/tlauncher/modpack/domain/client/share/GameType;)V
                    //   103: return         
                    //    StackMapTable: 00 03 FB 00 41 0D 17
                    // 
                    // The error that occurred was:
                    // 
                    // java.lang.IllegalStateException: Could not infer any expression.
                    //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:374)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:96)
                    //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
                    //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1164)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1009)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:713)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:549)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:576)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                    //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
                    //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
                    //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
                    // 
                    throw new IllegalStateException("An error occurred while decompiling this method.");
                }
            };
            final MouseAdapter backgroundListener = new MouseAdapter() {
                @Override
                public void mouseEntered(final MouseEvent e) {
                    GameLeftElement.this.setBackground(GameLeftElement.this.MOUSE_UNDER_BACKGROUND);
                }
                
                @Override
                public void mouseExited(final MouseEvent e) {
                    GameLeftElement.this.setBackground(GameLeftElement.this.BACKGROUND_COLOR);
                }
            };
            final AtomicInteger padding = new AtomicInteger();
            final int width;
            final AtomicInteger atomicInteger;
            final Timer timer = new Timer(30, e12 -> {
                width = this.name.getWidth();
                if (atomicInteger.get() + width < 0) {
                    atomicInteger.set(width - 20);
                }
                this.name.setBorder(new EmptyBorder(0, atomicInteger.getAndDecrement(), 0, 0));
                return;
            });
            this.name.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(final MouseEvent e) {
                    padding.set(0);
                    if (GameLeftElement.this.name.getPreferredSize().width > 120) {
                        timer.restart();
                    }
                }
                
                @Override
                public void mouseExited(final MouseEvent e) {
                    timer.stop();
                    GameLeftElement.this.name.setBorder(nameBorder);
                }
            });
            this.addMouseListenerOriginally(click);
            this.name.addMouseListener(click);
            this.leftLabel.addMouseListener(click);
            this.addMouseListener(backgroundListener);
            this.addMouseListenerOriginally(backgroundListener);
        }
        
        @Override
        public void initGameEntity() {
            this.name.setText(this.entity.getName());
            if (this.type != GameType.MAP) {
                this.clickButton.setState(this.entity.getStateGameElement());
            }
        }
        
        @Override
        public void processingActivation() {
        }
        
        @Override
        public void processingInstall() {
        }
        
        private GameLeftElement find(final GameEntityDTO e) {
            for (final Component c : Arrays.asList(this.getComponents())) {
                if (c instanceof GameLeftElement) {
                    final GameLeftElement el = (GameLeftElement)c;
                    if (el.getEntity().getId().equals(e.getId())) {
                        return el;
                    }
                    continue;
                }
            }
            return null;
        }
        
        public SubModpackDTO getEntity() {
            return this.entity;
        }
        
        @Override
        protected void paintComponent(final Graphics g) {
            super.paintComponent(g);
            final Rectangle rec = this.getVisibleRect();
            SwingUtil.paintShadowLine(rec, g, this.getBackground().getRed(), 12);
        }
    }
    
    public class SearchPanel extends JPanel
    {
        private final LocalizableTextField field;
        
        public SearchPanel() {
            this.setOpaque(false);
            this.setPreferredSize(new Dimension(194, 55));
            final SpringLayout spring = new SpringLayout();
            this.setLayout(spring);
            (this.field = new LocalizableTextField("modpack.search.text") {
                public void setBackColor() {
                    if (!OS.is(OS.LINUX)) {
                        this.setForeground(Color.WHITE);
                    }
                }
                
                @Override
                protected void onFocusLost() {
                    super.onFocusLost();
                    this.setBackColor();
                }
                
                @Override
                protected void onFocusGained() {
                    super.onFocusGained();
                    this.setBackColor();
                }
                
                @Override
                public void updateLocale() {
                    super.updateLocale();
                    this.setBackColor();
                }
            }).setOpaque(false);
            this.field.setBorder(BorderFactory.createEmptyBorder());
            this.field.setCaretColor(Color.WHITE);
            if (!OS.is(OS.LINUX)) {
                SwingUtil.changeFontFamily(this.field, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
            }
            spring.putConstraint("West", this.field, 17, "West", this);
            spring.putConstraint("East", this.field, -50, "East", this);
            spring.putConstraint("North", this.field, 17, "North", this);
            spring.putConstraint("South", this.field, -20, "South", this);
            this.add(this.field);
            final Color lineYellow = new Color(254, 254, 168);
            final JPanel linePanel = new JPanel();
            linePanel.setBackground(lineYellow);
            spring.putConstraint("West", linePanel, 17, "West", this);
            spring.putConstraint("East", linePanel, -17, "East", this);
            spring.putConstraint("North", linePanel, -12, "South", this);
            spring.putConstraint("South", linePanel, -11, "South", this);
            this.add(linePanel);
            final JLabel image = new JLabel(ImageCache.getNativeIcon("search-modpack-element.png"));
            spring.putConstraint("West", image, -35, "East", this);
            spring.putConstraint("East", image, -19, "East", this);
            spring.putConstraint("North", image, 18, "North", this);
            spring.putConstraint("South", image, -21, "South", this);
            this.add(image);
            final DeferredDocumentListener listener = new DeferredDocumentListener(500, e -> {
                if (ModpackScene.this.isVisible()) {
                    ModpackScene.this.getRightPanelByType(ModpackScene.this.current).filterRightPanel(ModpackScene.this.current);
                }
                return;
            }, false);
            ModpackScene.this.createTextField(this.field, listener);
        }
        
        public boolean isNotEmpty() {
            return this.field.getValue() != null && !this.field.getValue().isEmpty();
        }
        
        public String getSearchLine() {
            return this.field.getValue();
        }
    }
    
    class UserCategoryComparator implements Comparator<GameEntityDTO>
    {
        private UserCategory category;
        
        public UserCategoryComparator(final UserCategory category) {
            this.category = category;
        }
        
        @Override
        public int compare(final GameEntityDTO o1, final GameEntityDTO o2) {
            switch (this.category) {
                case NAME: {
                    return o1.getName().compareTo(o2.getName());
                }
                case FAVORITE: {
                    if (o1.isPopulateStatus()) {
                        if (o2.isPopulateStatus()) {
                            return 0;
                        }
                        return -1;
                    }
                    else {
                        if (o2.isPopulateStatus()) {
                            return 1;
                        }
                        return 0;
                    }
                    break;
                }
                case POPULATE_ALL_TIME: {
                    return o2.getDownloadALL().compareTo(o1.getDownloadALL());
                }
                case POPULATE_MONTH: {
                    return o2.getDownloadMonth().compareTo(o1.getDownloadMonth());
                }
                case DATE: {
                    return o2.getVersions().get(0).getUpdateDate().compareTo(o1.getVersions().get(0).getUpdateDate());
                }
                default: {
                    return 0;
                }
            }
        }
    }
    
    class GameEntityLeftPanel extends GameEntityPanel
    {
        private List<ElementCounterListener> observer;
        
        public GameEntityLeftPanel() {
            this.setLayout(new BoxLayout(this, 1));
            this.observer = new ArrayList<ElementCounterListener>();
        }
        
        public void addElements(final List<? extends SubModpackDTO> list, final GameType type) {
            this.cleanLeftPanel();
            for (final SubModpackDTO e : list) {
                this.add(new GameLeftElement(e, type));
            }
            this.fireCounterChanged();
        }
        
        protected void cleanLeftPanel() {
            for (final Component c : Arrays.asList(this.getComponents())) {
                if (c instanceof GameLeftElement) {
                    this.remove(c);
                }
            }
        }
        
        @Override
        public void activationStarted(final GameEntityDTO e) {
            for (final Component c : Arrays.asList(this.getComponents())) {
                if (c instanceof GameLeftElement) {
                    ((GameLeftElement)c).processingActivation();
                }
            }
        }
        
        @Override
        public void removeEntity(final GameEntityDTO e) {
            final GameLeftElement elem = this.find(e);
            if (elem != null) {
                this.remove(elem);
                this.revalidate();
                this.repaint();
            }
            this.fireCounterChanged();
        }
        
        @Override
        public void activation(final GameEntityDTO e) {
            for (final Component c : Arrays.asList(this.getComponents())) {
                if (c instanceof GameLeftElement) {
                    ((GameLeftElement)c).initGameEntity();
                }
            }
        }
        
        @Override
        public void activationError(final GameEntityDTO e, final Throwable t) {
            for (final Component c : Arrays.asList(this.getComponents())) {
                if (c instanceof GameLeftElement) {
                    ((GameLeftElement)c).initGameEntity();
                }
            }
        }
        
        private GameLeftElement find(final GameEntityDTO e) {
            for (final Component c : Arrays.asList(this.getComponents())) {
                if (c instanceof GameLeftElement) {
                    final GameLeftElement el = (GameLeftElement)c;
                    if (el.getEntity().getId().equals(e.getId())) {
                        return el;
                    }
                    continue;
                }
            }
            return null;
        }
        
        @Override
        public void installEntity(final GameEntityDTO e, final GameType type) {
            this.add(new GameLeftElement((SubModpackDTO)e, type));
            this.revalidate();
            this.repaint();
            this.fireCounterChanged();
        }
        
        public void fireCounterChanged() {
            for (final ElementCounterListener el : this.observer) {
                el.changeCounter(this.getComponentCount());
            }
        }
        
        public void addCounterListener(final ElementCounterListener l) {
            this.observer.add(l);
        }
    }
    
    interface ElementCounterListener
    {
        void changeCounter(final int p0);
    }
}
