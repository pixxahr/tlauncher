// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import javax.swing.JLayeredPane;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.menu.PopupMenuView;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import org.tlauncher.tlauncher.ui.settings.SettingsPanel;
import org.tlauncher.tlauncher.ui.login.LoginFormHelper;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import org.tlauncher.tlauncher.ui.SideNotifier;
import java.awt.Dimension;

public class DefaultScene extends PseudoScene
{
    public static final Dimension SETTINGS_SIZE;
    public static final int EDGE = 7;
    public final SideNotifier notifier;
    public final LoginForm loginForm;
    public final LoginFormHelper loginFormHelper;
    public final SettingsPanel settingsForm;
    private SidePanel sidePanel;
    private ExtendedPanel sidePanelComp;
    private PopupMenuView popupMenuView;
    
    public DefaultScene(final MainPane main) {
        super(main);
        this.notifier = main.notifier;
        (this.settingsForm = new SettingsPanel(this)).setSize(DefaultScene.SETTINGS_SIZE);
        this.settingsForm.setVisible(false);
        this.add(this.settingsForm);
        this.loginForm = new LoginForm(this);
        this.add(this.popupMenuView = new PopupMenuView(this));
        this.add(this.loginForm);
        this.add(this.loginFormHelper = new LoginFormHelper(this));
    }
    
    public ExtendedPanel getSidePanelComp() {
        return this.sidePanelComp;
    }
    
    public void setSidePanelComp(final ExtendedPanel sidePanelComp) {
        this.sidePanelComp = sidePanelComp;
    }
    
    public PopupMenuView getPopupMenuView() {
        return this.popupMenuView;
    }
    
    @Override
    public void onResize() {
        if (this.parent == null) {
            return;
        }
        this.setBounds(0, 0, this.parent.getWidth(), this.parent.getHeight());
        this.updateCoords();
        this.loginFormHelper.onResize();
    }
    
    private void updateCoords() {
        final int w = this.getWidth();
        final int h = this.getHeight();
        final int hw = w / 2;
        final int y = h - this.loginForm.getHeight();
        this.loginForm.setLocation(0, y);
        if (this.sidePanel != null) {
            this.sidePanelComp.setLocation(hw - this.sidePanelComp.getWidth() / 2, 7);
        }
    }
    
    public SidePanel getSidePanel() {
        return this.sidePanel;
    }
    
    public void setSidePanel(final SidePanel side) {
        if (this.sidePanel == side) {
            return;
        }
        final boolean noSidePanel = side == null;
        if (this.sidePanelComp != null) {
            this.sidePanelComp.setVisible(false);
        }
        this.sidePanel = side;
        this.sidePanelComp = (noSidePanel ? null : this.getSidePanelComp(side));
        if (!noSidePanel) {
            this.sidePanelComp.setVisible(true);
        }
        this.getMainPane().browser.setBrowserContentShown("settings", side == null);
        this.updateCoords();
    }
    
    public void toggleSidePanel(SidePanel side) {
        if (this.sidePanel == side) {
            side = null;
        }
        this.setSidePanel(side);
    }
    
    public ExtendedPanel getSidePanelComp(final SidePanel side) {
        if (side == null) {
            throw new NullPointerException("side");
        }
        switch (side) {
            case SETTINGS: {
                return this.settingsForm;
            }
            default: {
                throw new RuntimeException("unknown side:" + side);
            }
        }
    }
    
    static {
        SETTINGS_SIZE = new Dimension(700, 550);
    }
    
    public enum SidePanel
    {
        SETTINGS;
        
        public final boolean requiresShow;
        
        private SidePanel(final boolean requiresShow) {
            this.requiresShow = requiresShow;
        }
        
        private SidePanel() {
            this(false);
        }
    }
}
