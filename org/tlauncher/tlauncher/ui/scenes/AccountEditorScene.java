// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import org.tlauncher.tlauncher.ui.accounts.helper.HelperState;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.Container;
import javax.swing.BoxLayout;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.server.BackPanel;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.swing.FlexibleEditorPanel;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import org.tlauncher.tlauncher.ui.accounts.helper.AccountEditorHelper;
import org.tlauncher.tlauncher.ui.accounts.AccountTip;
import org.tlauncher.tlauncher.ui.accounts.AccountHandler;
import org.tlauncher.tlauncher.ui.accounts.AccountList;
import org.tlauncher.tlauncher.ui.accounts.AccountEditor;

public class AccountEditorScene extends PseudoScene
{
    private static final long serialVersionUID = -151325577614420989L;
    private final int WIDTH = 502;
    private final int HEIGHT = 315;
    public final AccountEditor editor;
    public final AccountList list;
    public final AccountHandler handler;
    public final AccountTip tip;
    public final AccountEditorHelper helper;
    public final ExtendedPanel panel;
    public static final Color BACKGROUND_ACCOUNT_COLOR;
    private FlexibleEditorPanel flex;
    
    public AccountEditorScene(final MainPane main) {
        super(main);
        this.panel = new ExtendedPanel(new BorderLayout(0, 0));
        this.flex = new FlexibleEditorPanel("text/html", "auth.tip.tlauncher", 477);
        final ExtendedPanel middlePanel = new ExtendedPanel();
        final BackPanel backPanel = new BackPanel("account.config", new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    AccountEditorScene.this.handler.exitEditor();
                    TLauncher.getInstance().getProfileManager().refresh();
                }
            }
        }, ImageCache.getIcon("back-arrow.png"));
        this.panel.setOpaque(true);
        this.panel.setSize(502, 315);
        this.flex.setPreferredSize(new Dimension(486, 99));
        this.flex.setMargin(new Insets(20, 20, 22, 16));
        backPanel.setPreferredSize(new Dimension(502, 25));
        this.panel.setBackground(AccountEditorScene.BACKGROUND_ACCOUNT_COLOR);
        middlePanel.setLayout(new BoxLayout(middlePanel, 0));
        middlePanel.setPreferredSize(new Dimension(502, 191));
        middlePanel.setInsets(new Insets(20, 20, 0, 16));
        final JPanel separator = new ExtendedPanel(new FlowLayout(0, 0, 0));
        final JPanel gap = new JPanel(new FlowLayout(1, 0, 0));
        gap.setPreferredSize(new Dimension(1, 170));
        gap.setBackground(new Color(172, 171, 170));
        separator.setPreferredSize(new Dimension(41, 171));
        separator.setBorder(BorderFactory.createEmptyBorder(1, 0, 0, 0));
        separator.add(Box.createHorizontalStrut(20));
        separator.add(gap);
        separator.add(Box.createHorizontalStrut(20));
        (this.editor = new AccountEditor(this, this.flex)).setOpaque(true);
        this.list = new AccountList(this);
        middlePanel.add(this.editor);
        middlePanel.add(separator);
        middlePanel.add(this.list);
        this.panel.add(backPanel, "North");
        this.panel.add(middlePanel, "Center");
        this.panel.add(this.flex, "South");
        this.add(this.panel);
        this.handler = new AccountHandler(this);
        (this.tip = new AccountTip(this)).setAccountType(Account.AccountType.TLAUNCHER);
        this.editor.setSelectedAccountType(Account.AccountType.TLAUNCHER);
        this.handler.notifyEmpty();
        this.addComponentListener(new ComponentListener() {
            @Override
            public void componentShown(final ComponentEvent e) {
                if (AccountEditorScene.this.list.model.getSize() == 0) {
                    AccountEditorScene.this.handler.addAccount();
                }
            }
            
            @Override
            public void componentResized(final ComponentEvent e) {
            }
            
            @Override
            public void componentMoved(final ComponentEvent e) {
            }
            
            @Override
            public void componentHidden(final ComponentEvent e) {
            }
        });
        this.tip.setVisible(true);
        this.add(this.helper = new AccountEditorHelper(this));
        this.helper.setState(HelperState.NONE);
    }
    
    public void setShownAccountHelper(final boolean shown, final boolean animate) {
        super.setShown(shown, animate);
        if (!shown || !this.list.model.isEmpty()) {
            this.helper.setState(HelperState.NONE);
        }
    }
    
    @Override
    public void onResize() {
        super.onResize();
        final int hw = this.getWidth() / 2;
        final int hh = this.getHeight() / 2;
        this.panel.setLocation(hw - this.panel.getWidth() / 2, hh - this.panel.getHeight() / 2);
    }
    
    static {
        BACKGROUND_ACCOUNT_COLOR = new Color(248, 246, 244);
    }
}
