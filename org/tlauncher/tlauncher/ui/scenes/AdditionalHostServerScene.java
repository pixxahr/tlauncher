// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.scenes;

import java.awt.event.MouseEvent;
import javax.swing.event.MouseInputAdapter;
import java.awt.BorderLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.util.Date;
import org.tlauncher.tlauncher.entity.hot.AdditionalHotServer;
import java.util.List;
import org.tlauncher.tlauncher.updater.client.Banner;
import org.tlauncher.tlauncher.entity.hot.HotBanner;
import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.util.U;
import javax.swing.SwingUtilities;
import org.tlauncher.util.OS;
import java.util.Objects;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.JPanel;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import java.awt.Color;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.button.RoundImageButton;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.JLayeredPane;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.BackgroundPanel;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.MainPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTable;
import org.tlauncher.tlauncher.managers.popup.menu.HotServerManager;
import org.tlauncher.tlauncher.ui.menu.PopupMenuView;

public class AdditionalHostServerScene extends PseudoScene
{
    private static final long serialVersionUID = 8975208936840346013L;
    private final PopupMenuView popupMenuView;
    private final HotServerManager hotServerManager;
    private final Integer rowHeight;
    private boolean loadOnce;
    private JTable table;
    private JButton back;
    private JLabel serverTitle;
    
    public AdditionalHostServerScene(final MainPane mp) {
        super(mp);
        this.rowHeight = 48;
        this.hotServerManager = (HotServerManager)TLauncher.getInjector().getInstance((Class)HotServerManager.class);
        final JPanel panel = new BackgroundPanel("hot/hot-servers-background.png");
        panel.setSize(MainPane.SIZE.width, MainPane.SIZE.height);
        panel.setLocation(0, 0);
        this.add(panel);
        this.popupMenuView = new PopupMenuView(this);
        (this.back = new RoundImageButton(ImageCache.getImage("hot/back.png"), ImageCache.getImage("hot/back-active.png"))).setBorder(BorderFactory.createEmptyBorder());
        this.serverTitle = new LocalizableLabel("server.page.title");
        (this.table = new JTable()).setRowHeight(this.rowHeight);
        this.table.setBorder(BorderFactory.createEmptyBorder());
        this.table.setOpaque(false);
        this.table.setShowGrid(false);
        this.table.getColumnModel().setColumnSelectionAllowed(true);
        this.table.setIntercellSpacing(new Dimension(0, 0));
        SwingUtil.changeFontFamily(this.serverTitle, FontTL.ROBOTO_MEDIUM, 20);
        this.serverTitle.setForeground(Color.WHITE);
        this.add(this.back);
        this.add(this.serverTitle);
        this.add(this.table);
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(final ComponentEvent e) {
                AdditionalHostServerScene.this.prepare();
            }
        });
        this.back.addActionListener(e -> mp.setScene(mp.defaultScene));
        final RollOverListener lst = new RollOverListener(this.table);
        this.table.addMouseMotionListener(lst);
        this.table.addMouseListener(lst);
        this.popupMenuView.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentHidden(final ComponentEvent e) {
                AdditionalHostServerScene.this.table.clearSelection();
            }
        });
    }
    
    public void prepare() {
        this.prepareRemoteServerData(this.hotServerManager.getAdditionalHotServers().getList());
        if (this.loadOnce) {
            return;
        }
        this.loadOnce = true;
        final int rows = this.hotServerManager.getAdditionalHotServers().getList().size();
        this.table.setSize(672, rows * this.rowHeight);
        this.back.setSize(new Dimension(187, 30));
        this.serverTitle.setSize(new Dimension(200, 24));
        this.table.setLocation(187, (MainPane.SIZE.height - rows * this.rowHeight) / 2);
        this.back.setLocation(200, this.table.getLocation().y - 32);
        this.serverTitle.setLocation(630, this.table.getLocation().y - 30);
        this.table.setModel(new TableModel(this.hotServerManager.getAdditionalHotServers().getList()));
        this.table.getColumnModel().getColumn(0).setPreferredWidth(632);
        this.table.getColumnModel().getColumn(1).setPreferredWidth(40);
        this.table.getColumnModel().getColumn(0).setCellRenderer(new AdditionalServerRenderer());
        this.table.getColumnModel().getColumn(1).setCellRenderer(new Column1Renderer());
        HotBanner up;
        RoundImageButton upButton;
        final RoundImageButton comp;
        final Banner banner;
        HotBanner down;
        RoundImageButton downButton;
        final RoundImageButton comp2;
        final Banner banner2;
        AsyncThread.execute(() -> {
            try {
                up = this.hotServerManager.getAdditionalHotServers().getUpBanner();
                if (Objects.nonNull(up)) {
                    upButton = new RoundImageButton(up.getImage(), up.getMouseOnImage());
                    SwingUtilities.invokeLater(() -> {
                        comp.setSize(comp.getImageSize());
                        this.add(comp);
                        comp.setLocation(851, this.table.getLocation().y + 1);
                        comp.addActionListener(e -> OS.openLink(banner.getClickLink()));
                        return;
                    });
                }
                down = this.hotServerManager.getAdditionalHotServers().getDownBanner();
                if (Objects.nonNull(down)) {
                    downButton = new RoundImageButton(down.getImage(), down.getMouseOnImage());
                    SwingUtilities.invokeLater(() -> {
                        this.add(comp2);
                        comp2.setSize(comp2.getImageSize());
                        comp2.setLocation(20, this.table.getLocation().y + this.table.getSize().height - comp2.getPreferredSize().height);
                        comp2.addActionListener(e -> OS.openLink(banner2.getClickLink()));
                        return;
                    });
                }
            }
            catch (RuntimeException e) {
                U.log(e);
            }
            SwingUtilities.invokeLater(() -> this.add(this.popupMenuView));
        });
    }
    
    private void prepareRemoteServerData(final List<AdditionalHotServer> list) {
        for (int i = 0; i < list.size(); ++i) {
            final int finalI = i;
            final int n;
            final AdditionalHotServer s;
            AsyncThread.execute(() -> {
                s = list.get(n);
                try {
                    if (Objects.nonNull(s.getUpdated())) {
                        if (s.getUpdated().after(new Date())) {
                            return;
                        }
                    }
                    this.hotServerManager.fillServer(s);
                    ((TableModel)this.table.getModel()).fireTableCellUpdated(n, 0);
                }
                catch (Throwable e) {
                    U.log(s, e);
                }
                return;
            });
        }
    }
    
    public PopupMenuView getPopupMenuView() {
        return this.popupMenuView;
    }
    
    private class TableModel extends AbstractTableModel
    {
        private List<AdditionalHotServer> list;
        
        TableModel(final List<AdditionalHotServer> list) {
            this.list = list;
        }
        
        @Override
        public int getRowCount() {
            return this.list.size();
        }
        
        @Override
        public int getColumnCount() {
            return 2;
        }
        
        @Override
        public Object getValueAt(final int row, final int column) {
            if (column == 0) {
                return this.list.get(row);
            }
            return this.list.get(row).getTMonitoringLink();
        }
    }
    
    private class AdditionalServerRenderer extends DefaultTableCellRenderer
    {
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
            final AdditionalHotServer s = (AdditionalHotServer)value;
            int gap = 0;
            JPanel panel;
            if (isSelected) {
                panel = new BackgroundPanel(String.format("hot/%s-hot-servers-active.png", s.getImageNumber()));
                gap = 1;
            }
            else {
                panel = new BackgroundPanel(String.format("hot/%s-hot-servers.png", s.getImageNumber()));
            }
            panel.setBorder(BorderFactory.createEmptyBorder());
            final JLabel shortDesc = new JLabel(s.getShortDescription());
            final JLabel desc = new JLabel(s.getAddDescription());
            final JLabel onlineTitle = new LocalizableLabel("server.page.online");
            final JLabel versionTitle = new LocalizableLabel("version.release");
            final JLabel online = new JLabel((s.getOnline() == -1) ? "???" : (s.getOnline() + "/" + s.getMax()));
            final JLabel startVersion = new JLabel(s.getVersionDescription());
            final SpringLayout spring = new SpringLayout();
            panel.setLayout(spring);
            if (!isSelected) {
                JLabel image;
                if (Objects.nonNull(s.getImage())) {
                    image = new JLabel(new ImageIcon(s.getImage().getScaledInstance(44, 44, 4)));
                }
                else {
                    image = new JLabel((Icon)ImageCache.getIcon("hot/tl-icon.png"));
                }
                spring.putConstraint("West", image, 1, "West", panel);
                spring.putConstraint("East", image, 45, "West", panel);
                spring.putConstraint("North", image, 1, "North", panel);
                spring.putConstraint("South", image, -1, "South", panel);
                panel.add(image);
            }
            spring.putConstraint("West", shortDesc, -564, "East", panel);
            spring.putConstraint("East", shortDesc, -144, "East", panel);
            spring.putConstraint("North", shortDesc, gap, "North", panel);
            spring.putConstraint("South", shortDesc, gap + 24, "North", panel);
            panel.add(shortDesc);
            spring.putConstraint("West", desc, -564, "East", panel);
            spring.putConstraint("East", desc, -144, "East", panel);
            spring.putConstraint("North", desc, gap + 26, "North", panel);
            spring.putConstraint("South", desc, -gap - 2, "South", panel);
            panel.add(desc);
            spring.putConstraint("West", onlineTitle, -142, "East", panel);
            spring.putConstraint("East", onlineTitle, -62, "East", panel);
            spring.putConstraint("North", onlineTitle, gap, "North", panel);
            spring.putConstraint("South", onlineTitle, gap + 24, "North", panel);
            panel.add(onlineTitle);
            spring.putConstraint("West", online, -142, "East", panel);
            spring.putConstraint("East", online, -62, "East", panel);
            spring.putConstraint("North", online, gap + 26, "North", panel);
            spring.putConstraint("South", online, gap + 46, "North", panel);
            panel.add(online);
            spring.putConstraint("West", versionTitle, -60, "East", panel);
            spring.putConstraint("East", versionTitle, 0, "East", panel);
            spring.putConstraint("North", versionTitle, gap, "North", panel);
            spring.putConstraint("South", versionTitle, gap + 24, "North", panel);
            panel.add(versionTitle);
            spring.putConstraint("West", startVersion, -60, "East", panel);
            spring.putConstraint("East", startVersion, 0, "East", panel);
            spring.putConstraint("North", startVersion, gap + 26, "North", panel);
            spring.putConstraint("South", startVersion, gap + 46, "North", panel);
            panel.add(startVersion);
            onlineTitle.setHorizontalAlignment(0);
            versionTitle.setHorizontalAlignment(0);
            online.setHorizontalAlignment(0);
            startVersion.setHorizontalAlignment(0);
            SwingUtil.changeFontFamily(shortDesc, FontTL.ROBOTO_BOLD, 16);
            SwingUtil.changeFontFamily(desc, FontTL.ROBOTO_REGULAR, 12);
            SwingUtil.changeFontFamily(onlineTitle, FontTL.ROBOTO_BOLD, 12);
            SwingUtil.changeFontFamily(versionTitle, FontTL.ROBOTO_BOLD, 12);
            SwingUtil.changeFontFamily(online, FontTL.ROBOTO_MEDIUM, 12);
            SwingUtil.changeFontFamily(startVersion, FontTL.ROBOTO_MEDIUM, 12);
            shortDesc.setForeground(Color.WHITE);
            desc.setForeground(Color.WHITE);
            onlineTitle.setForeground(Color.WHITE);
            versionTitle.setForeground(Color.WHITE);
            online.setForeground(Color.WHITE);
            startVersion.setForeground(Color.WHITE);
            if (isSelected) {
                return panel;
            }
            final JPanel jPanel = new JPanel(new BorderLayout(0, 0));
            jPanel.setBorder(BorderFactory.createEmptyBorder(1, 13, 1, 0));
            jPanel.add(panel, "Center");
            jPanel.setOpaque(false);
            return jPanel;
        }
    }
    
    private class Column1Renderer implements TableCellRenderer
    {
        private Component get(final Object value, final boolean active) {
            if (Objects.isNull(value)) {
                return new JLabel();
            }
            JLabel label;
            if (active) {
                label = new JLabel((Icon)ImageCache.getIcon("hot/hot-server-site-active.png"));
            }
            else {
                label = new JLabel((Icon)ImageCache.getIcon("hot/hot-server-site.png"));
            }
            return label;
        }
        
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
            return this.get(value, isSelected);
        }
    }
    
    private class RollOverListener extends MouseInputAdapter
    {
        JTable table;
        
        RollOverListener(final JTable table) {
            this.table = table;
        }
        
        @Override
        public void mouseExited(final MouseEvent e) {
            if (!AdditionalHostServerScene.this.popupMenuView.isVisible()) {
                this.table.clearSelection();
            }
        }
        
        @Override
        public void mouseMoved(final MouseEvent e) {
            final int row = this.table.rowAtPoint(e.getPoint());
            final int column = this.table.columnAtPoint(e.getPoint());
            if (this.table.getSelectedRow() != row || this.table.getSelectedColumn() != column) {
                this.table.changeSelection(row, column, false, false);
            }
            if (row == -1 && this.table.getSelectedRow() > -1) {
                this.table.clearSelection();
            }
        }
        
        @Override
        public void mouseClicked(final MouseEvent e) {
            final int row = this.table.rowAtPoint(e.getPoint());
            final int col = this.table.columnAtPoint(e.getPoint());
            if (row >= 0 && col >= 0) {
                final Object value = this.table.getModel().getValueAt(row, col);
                if (col == 1) {
                    if (Objects.nonNull(value)) {
                        OS.openLink((String)value);
                    }
                }
                else if (e.getPoint().getX() < 490.0) {
                    final AdditionalHotServer s = (AdditionalHotServer)value;
                    AdditionalHostServerScene.this.hotServerManager.processingEvent(s.getServerId());
                }
            }
        }
    }
}
