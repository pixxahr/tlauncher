// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.server;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.Component;
import javax.swing.BorderFactory;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.images.ImageIcon;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JPanel;

public class BackPanel extends JPanel
{
    private static final long serialVersionUID = 1L;
    public static final Color BACKGROUND_COLOR;
    private final JLabel backLabel;
    
    public BackPanel(final String titleName, final MouseListener listener, final ImageIcon icon) {
        this.backLabel = new JLabel((Icon)icon);
        this.setLayout(new BorderLayout(0, 0));
        this.setBackground(BackPanel.BACKGROUND_COLOR);
        this.backLabel.setPreferredSize(new Dimension(65, 25));
        this.backLabel.setBackground(new Color(46, 131, 177));
        this.backLabel.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
        this.backLabel.addMouseListener(listener);
        this.add(this.backLabel, "West");
        final LocalizableLabel label = new LocalizableLabel(titleName);
        label.setFont(label.getFont().deriveFont(1, 16.0f));
        label.setForeground(Color.WHITE);
        label.setHorizontalAlignment(0);
        this.add(label, "Center");
        this.backLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                BackPanel.this.backLabel.setOpaque(true);
                BackPanel.this.backLabel.repaint();
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                BackPanel.this.backLabel.setOpaque(false);
                BackPanel.this.backLabel.repaint();
            }
        });
    }
    
    public void addBackListener(final MouseListener listener) {
        this.backLabel.addMouseListener(listener);
    }
    
    static {
        BACKGROUND_COLOR = new Color(60, 170, 232);
    }
}
