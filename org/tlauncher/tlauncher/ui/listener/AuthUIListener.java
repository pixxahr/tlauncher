// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.listener;

import java.io.IOException;
import org.tlauncher.tlauncher.ui.MainPane;
import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.exceptions.auth.NotCorrectTokenOrIdException;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.exceptions.auth.AuthenticatorException;
import org.tlauncher.tlauncher.minecraft.auth.Authenticator;
import org.tlauncher.tlauncher.minecraft.auth.AuthenticatorListener;

public class AuthUIListener implements AuthenticatorListener
{
    private final AuthenticatorListener listener;
    
    public AuthUIListener(final AuthenticatorListener listener) {
        this.listener = listener;
    }
    
    @Override
    public void onAuthPassing(final Authenticator auth) {
        if (this.listener == null) {
            return;
        }
        this.listener.onAuthPassing(auth);
    }
    
    @Override
    public void onAuthPassingError(final Authenticator auth, final Throwable e) {
        this.showError(e);
        if (this.listener != null) {
            this.listener.onAuthPassingError(auth, e);
        }
    }
    
    private void showError(final Throwable e) {
        String description = "unknown";
        if (e instanceof AuthenticatorException) {
            final AuthenticatorException ae = (AuthenticatorException)e;
            if (ae.getLangpath() != null) {
                description = ae.getLangpath();
            }
        }
        Alert.showErrorHtml("auth.error.title", "auth.error." + description);
        if (e instanceof NotCorrectTokenOrIdException) {
            final MainPane m;
            SwingUtilities.invokeLater(() -> {
                m = TLauncher.getInstance().getFrame().mp;
                m.openAccountEditor();
            });
        }
    }
    
    @Override
    public void onAuthPassed(final Authenticator auth) {
        if (this.listener != null) {
            this.listener.onAuthPassed(auth);
        }
        this.saveProfiles();
    }
    
    public void saveProfiles() {
        try {
            TLauncher.getInstance().getProfileManager().saveProfiles();
        }
        catch (IOException e) {
            Alert.showLocError("auth.profiles.save-error");
        }
    }
}
