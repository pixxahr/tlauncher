// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.listener;

import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;
import org.apache.commons.io.FileUtils;
import org.tlauncher.util.MinecraftUtil;
import java.util.Optional;
import java.util.Map;
import net.minecraft.launcher.versions.CompleteVersion;
import java.io.IOException;
import net.minecraft.launcher.versions.Rule;
import java.util.Arrays;
import net.minecraft.launcher.versions.json.Argument;
import net.minecraft.launcher.versions.json.ArgumentType;
import java.util.Objects;
import org.tlauncher.util.StringUtil;
import java.util.List;
import org.tlauncher.util.FileUtil;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;
import java.io.File;
import org.tlauncher.tlauncher.minecraft.launcher.assitent.LanguageAssistance;
import java.util.Iterator;
import org.tlauncher.tlauncher.configuration.Configuration;
import net.minecraft.launcher.Http;
import com.google.common.base.Strings;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.U;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.minecraft.crash.CrashSignatureContainer;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.minecraft.crash.Crash;
import org.tlauncher.tlauncher.configuration.enums.ActionOnLaunch;
import org.tlauncher.tlauncher.configuration.LangConfiguration;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListener;

public class MinecraftUIListener implements MinecraftListener
{
    private final TLauncher t;
    private final LangConfiguration lang;
    
    public MinecraftUIListener(final TLauncher tlauncher) {
        this.t = tlauncher;
        this.lang = this.t.getLang();
    }
    
    @Override
    public void onMinecraftPrepare() {
    }
    
    @Override
    public void onMinecraftAbort() {
    }
    
    @Override
    public void onMinecraftLaunch() {
        if (!this.t.getConfiguration().getActionOnLaunch().equals(ActionOnLaunch.NOTHING)) {
            this.t.hide();
        }
    }
    
    @Override
    public void onMinecraftClose() {
        if (!this.t.getLauncher().isLaunchAssist()) {
            return;
        }
        this.t.show();
    }
    
    @Override
    public void onMinecraftCrash(final Crash crash) {
        if (!this.t.getLauncher().isLaunchAssist()) {
            this.t.show();
        }
        final Configuration c = this.t.getConfiguration();
        final String p = "crash.";
        final String title = Localizable.get(p + "title");
        final String report = crash.getFile();
        final String skipAccountProperty = "skip.account.property";
        if (!crash.isRecognized()) {
            this.showPossibleSolutions();
        }
        else {
            for (final CrashSignatureContainer.CrashSignature sign : crash.getSignatures()) {
                final String path = sign.getPath();
                String message = p + path;
                if ("not proper gpu1".equalsIgnoreCase(sign.getName()) && !c.getBoolean("fixed.gpu.jre.error") && OS.is(OS.WINDOWS)) {
                    c.set("fixed.gpu.jre.error", true);
                    U.log("set new jre");
                }
                else if (c.getBoolean("fixed.gpu.jre.error") && "Bad video drivers".equalsIgnoreCase(sign.getName())) {
                    Alert.showLocMessageWithoutTitle("crash.opengl.windows10.error");
                }
                if (sign.getName().equalsIgnoreCase("thread creation problem")) {
                    if (!this.replaceJVMParam()) {
                        this.showPossibleSolutions();
                        return;
                    }
                }
                else {
                    if (sign.getName().equalsIgnoreCase("Not reserve ram") && this.t.getConfiguration().getInteger("minecraft.memory.ram2") == OS.Arch.PREFERRED_MEMORY) {
                        this.showProperMemoryMessage();
                        continue;
                    }
                    if (sign.getName().equalsIgnoreCase("Not reserve ram") || sign.getName().equalsIgnoreCase("jetty ram problem")) {
                        if (Alert.showLocQuestion(title, message)) {
                            this.t.getConfiguration().set("minecraft.memory.ram2", OS.Arch.PREFERRED_MEMORY, true);
                        }
                        return;
                    }
                    if (sign.getName().equalsIgnoreCase("gson lenient") && !this.t.getConfiguration().getBoolean(skipAccountProperty)) {
                        this.t.getConfiguration().set(skipAccountProperty, true);
                    }
                    else {
                        if (sign.getName().equalsIgnoreCase("Bad video drivers")) {
                            String gpuLink = "";
                            String gpuName = this.t.getConfiguration().get("gpu.info");
                            String cpuLink = "";
                            String cpuName = this.t.getConfiguration().get("process.info");
                            int i = 2;
                            if (!Strings.isNullOrEmpty(gpuName)) {
                                gpuName = String.join(" ", gpuName, OS.NAME, OS.VERSION, Localizable.get("crash.opengl.download.driver"));
                                gpuLink = Http.get("https://www.google.com/search", "q", gpuName);
                                gpuLink = String.format("<br>%s)%s <a href='%s'>%s</a>", i++, Localizable.get("crash.opengl.install.gpu"), gpuLink, Localizable.get("click.me"));
                            }
                            if (!Strings.isNullOrEmpty(cpuName)) {
                                cpuName = String.join(" ", cpuName, OS.NAME, OS.VERSION, Localizable.get("crash.opengl.download.driver"));
                                cpuLink = Http.get("https://www.google.com/search", "q", cpuName);
                                cpuLink = String.format("<br>%s)%s <a href='%s'>%s</a>", i++, Localizable.get("crash.opengl.install.cpu"), cpuLink, Localizable.get("click.me"));
                            }
                            message = String.format(Localizable.get(message), gpuLink, cpuLink, Localizable.get("crash.opengl.help"));
                            Alert.showErrorHtml("", message);
                            return;
                        }
                        if ("vbo fix".equals(sign.getName()) && !this.fixProblem()) {
                            this.showPossibleSolutions();
                            return;
                        }
                        if (("OutOfMemory error".equals(sign.getName()) || "PermGen error".equals(sign.getName())) && "system.32x.not.proper".equals(c.get("memory.problem.message"))) {
                            message = Localizable.get(message) + Localizable.get("crash.outofmemory.32bit");
                        }
                        else if ("problem g1gc".equals(sign.getName()) && !this.t.getConfiguration().getBoolean("not.proper.g1gc")) {
                            this.t.getConfiguration().set("not.proper.g1gc", true);
                            Alert.showLocMessageWithoutTitle(message);
                            return;
                        }
                    }
                }
                Alert.showLocMessage(title, message, report);
            }
        }
    }
    
    private boolean fixProblem() {
        boolean canFixed = false;
        final File options = new File(this.t.getLauncher().getRunningMinecraftDir(), LanguageAssistance.OPTIONS);
        if (options.exists()) {
            try {
                final List<String> lines = Files.readAllLines(options.toPath(), StandardCharsets.UTF_8);
                int indexVsync = -1;
                int indexVbo = -1;
                for (int i = 0; i < lines.size(); ++i) {
                    if (lines.get(i).contains("enableVsync:")) {
                        indexVsync = i;
                    }
                    if (lines.get(i).contains("useVbo:")) {
                        indexVbo = i;
                    }
                }
                final String vbo = "useVbo:false";
                final String vsync = "enableVsync:true";
                if (indexVbo == -1) {
                    lines.add(vbo);
                    canFixed = true;
                }
                else if (lines.get(indexVbo).endsWith("true")) {
                    lines.remove(indexVbo);
                    lines.add(indexVbo, vbo);
                    canFixed = true;
                }
                if (indexVsync == -1) {
                    lines.add(vsync);
                    canFixed = true;
                }
                else if (lines.get(indexVsync).endsWith("false")) {
                    lines.remove(indexVsync);
                    lines.add(indexVsync, vsync);
                    canFixed = true;
                }
                if (canFixed) {
                    FileUtil.writeFile(options, String.join(System.lineSeparator(), lines));
                }
            }
            catch (Throwable t) {
                U.log(t);
                return false;
            }
        }
        return canFixed;
    }
    
    private void showProperMemoryMessage() {
        String recommendedMemory = "";
        String link;
        if (TLauncher.getInstance().getConfiguration().isUSSRLocale()) {
            link = StringUtil.getLink("https://www.dmosk.ru/polezno.php?review=memory-notfull");
        }
        else {
            link = StringUtil.getLink("https://www.howtogeek.com/131632/hardware-upgrade-why-windows-cant-see-all-your-ram") + "<br>" + StringUtil.getLink("https://windowsreport.com/windows-10-isnt-using-all-ram");
        }
        if (OS.Arch.TOTAL_RAM_MB < 1600) {
            recommendedMemory = Localizable.get("crash.java.not.enough.memory.solve.additional.low.memory");
        }
        Alert.showErrorHtml(String.format(Localizable.get("crash.java.not.enough.memory.solve"), OS.Arch.TOTAL_RAM_MB, recommendedMemory, link), 500);
    }
    
    private boolean replaceJVMParam() {
        try {
            final CompleteVersion version = this.t.getVersionManager().getLocalList().getCompleteVersion(this.t.getLauncher().getVersion().getID());
            final Map<ArgumentType, List<Argument>> args = version.getArguments();
            if (Objects.nonNull(args) && Objects.nonNull(args.get(ArgumentType.JVM))) {
                final List<Argument> list = args.get(ArgumentType.JVM);
                for (int i = 0; i < list.size(); ++i) {
                    final Optional<String> xss1024 = Arrays.stream(list.get(i).getValues()).filter(e -> e.equalsIgnoreCase("-Xss1M")).findAny();
                    final Optional<String> xss1025 = Arrays.stream(list.get(i).getValues()).filter(e -> e.equalsIgnoreCase("-Xss512K")).findAny();
                    if (xss1024.isPresent()) {
                        final int ram = OS.Arch.PREFERRED_MEMORY - 256;
                        if (ram > 512) {
                            this.t.getConfiguration().set("minecraft.memory.ram2", ram, false);
                        }
                        list.remove(i);
                        list.add(i, new Argument(new String[] { "-Xss512K" }, null));
                        return true;
                    }
                    if (xss1025.isPresent()) {
                        list.remove(i);
                        list.add(i, new Argument(new String[] { "-Xss256K" }, null));
                        return true;
                    }
                }
                this.t.getVersionManager().getLocalList().saveVersion(version);
            }
        }
        catch (IOException e2) {
            U.log(new Object[0]);
        }
        return false;
    }
    
    @Override
    public void onMinecraftError(final Throwable e) {
        this.showPossibleSolutions();
    }
    
    public void showPossibleSolutions() {
        final MinecraftLauncher launcher = this.t.getLauncher();
        if (launcher.getVersion().isModpack()) {
            Alert.showErrorHtml("launcher.error.modpack.solutions", 400);
        }
        else {
            final File mods = new File(MinecraftUtil.getWorkingDirectory(), "mods");
            String modsLine = "";
            if (mods.exists() && !FileUtils.listFiles(mods, new String[] { "jar", "zip" }, true).isEmpty()) {
                modsLine = Localizable.get("launcher.error.standard.version.point.mod");
            }
            Alert.showErrorHtml(Localizable.get("launcher.error.standard.version.solutions", modsLine, Localizable.get("crash.opengl.help")), 400);
        }
    }
    
    @Override
    public void onMinecraftKnownError(final MinecraftException e) {
        Alert.showError(this.lang.get("launcher.error.title"), this.lang.get("launcher.error." + e.getLangPath(), (Object[])e.getLangVars()), e.getCause());
    }
}
