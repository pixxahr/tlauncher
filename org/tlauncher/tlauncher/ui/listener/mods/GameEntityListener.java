// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.listener.mods;

import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;

public interface GameEntityListener
{
    void activationStarted(final GameEntityDTO p0);
    
    void activation(final GameEntityDTO p0);
    
    void activationError(final GameEntityDTO p0, final Throwable p1);
    
    void processingStarted(final GameEntityDTO p0, final VersionDTO p1);
    
    void installEntity(final GameEntityDTO p0, final GameType p1);
    
    void installEntity(final CompleteVersion p0);
    
    void removeEntity(final GameEntityDTO p0);
    
    void removeCompleteVersion(final CompleteVersion p0);
    
    void installError(final GameEntityDTO p0, final VersionDTO p1, final Throwable p2);
    
    void populateStatus(final GameEntityDTO p0, final GameType p1, final boolean p2);
    
    void updateVersion(final CompleteVersion p0, final CompleteVersion p1);
}
