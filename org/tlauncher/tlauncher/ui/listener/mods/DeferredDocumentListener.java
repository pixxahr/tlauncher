// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.listener.mods;

import javax.swing.event.DocumentEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.event.DocumentListener;

public class DeferredDocumentListener implements DocumentListener
{
    private final Timer timer;
    
    public DeferredDocumentListener(final int timeOut, final ActionListener listener, final boolean repeats) {
        (this.timer = new Timer(timeOut, listener)).setRepeats(repeats);
    }
    
    public void start() {
        this.timer.start();
    }
    
    public void stop() {
        this.timer.stop();
    }
    
    @Override
    public void insertUpdate(final DocumentEvent e) {
        this.timer.restart();
    }
    
    @Override
    public void removeUpdate(final DocumentEvent e) {
        this.timer.restart();
    }
    
    @Override
    public void changedUpdate(final DocumentEvent e) {
        this.timer.restart();
    }
}
