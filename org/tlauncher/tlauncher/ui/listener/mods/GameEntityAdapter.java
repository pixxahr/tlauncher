// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.listener.mods;

import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;

public class GameEntityAdapter implements GameEntityListener
{
    @Override
    public void activationStarted(final GameEntityDTO e) {
    }
    
    @Override
    public void activation(final GameEntityDTO e) {
    }
    
    @Override
    public void activationError(final GameEntityDTO e, final Throwable t) {
    }
    
    @Override
    public void processingStarted(final GameEntityDTO e, final VersionDTO version) {
    }
    
    @Override
    public void installEntity(final GameEntityDTO e, final GameType type) {
    }
    
    @Override
    public void installEntity(final CompleteVersion e) {
    }
    
    @Override
    public void removeEntity(final GameEntityDTO e) {
    }
    
    @Override
    public void removeCompleteVersion(final CompleteVersion e) {
    }
    
    @Override
    public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
    }
    
    @Override
    public void populateStatus(final GameEntityDTO status, final GameType type, final boolean state) {
    }
    
    @Override
    public void updateVersion(final CompleteVersion v, final CompleteVersion newVersion) {
    }
}
