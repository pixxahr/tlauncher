// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.listener.mods;

import org.tlauncher.tlauncher.ui.login.VersionComboBox;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import net.minecraft.launcher.versions.CompleteVersion;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class ModpackBoxListener implements ItemListener
{
    @Override
    public void itemStateChanged(final ItemEvent e) {
        if (1 == e.getStateChange()) {
            final CompleteVersion completeVersion = ((ModpackComboBox)e.getSource()).getSelectedValue();
            final VersionComboBox versions = TLauncher.getInstance().getFrame().mp.defaultScene.loginForm.versions;
            for (int i = 0; i < versions.getModel().getSize(); ++i) {
                if (versions.getModel().getElementAt(i).getID().equals(completeVersion.getID())) {
                    versions.getModel().setSelectedItem(versions.getModel().getElementAt(i));
                }
            }
        }
    }
}
