// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.listener;

import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.block.Blocker;
import java.net.URISyntaxException;
import org.tlauncher.util.OS;
import java.net.URI;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.updater.client.Update;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.updater.client.UpdateListener;

public class UpdateUIListener implements UpdateListener
{
    private final TLauncher t;
    private final Update u;
    
    public UpdateUIListener(final Update u) {
        if (u == null) {
            throw new NullPointerException();
        }
        this.t = TLauncher.getInstance();
        (this.u = u).addListener(this);
    }
    
    public void push() {
        this.block();
        this.u.download(true);
    }
    
    @Override
    public void onUpdateError(final Update u, final Throwable e) {
        if (Alert.showLocQuestion("updater.error.title", "updater.download-error", e)) {
            openUpdateLink(u.getlastDownloadedLink());
        }
        this.unblock();
    }
    
    @Override
    public void onUpdateDownloading(final Update u) {
    }
    
    @Override
    public void onUpdateDownloadError(final Update u, final Throwable e) {
        this.onUpdateError(u, e);
    }
    
    @Override
    public void onUpdateReady(final Update u) {
        onUpdateReady(u, false);
    }
    
    private static void onUpdateReady(final Update u, final boolean showChangeLog) {
        Alert.showLocWarning("updater.downloaded", (Object)(showChangeLog ? u.getDescription() : null));
        u.apply();
    }
    
    @Override
    public void onUpdateApplying(final Update u) {
    }
    
    @Override
    public void onUpdateApplyError(final Update u, final Throwable e) {
        if (Alert.showLocQuestion("updater.save-error", e)) {
            openUpdateLink(u.getlastDownloadedLink());
        }
        this.unblock();
    }
    
    public static boolean openUpdateLink(final String link) {
        try {
            if (OS.openLink(new URI(link), false)) {
                return true;
            }
        }
        catch (URISyntaxException ex) {}
        Alert.showLocError("updater.found.cannotopen", link);
        return false;
    }
    
    private void block() {
        Blocker.block(this.t.getFrame().mp, "updater");
    }
    
    private void unblock() {
        Blocker.unblock(this.t.getFrame().mp, "updater");
    }
}
