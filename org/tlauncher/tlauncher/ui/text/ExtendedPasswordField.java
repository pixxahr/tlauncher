// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.text;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import org.tlauncher.tlauncher.ui.center.CenterPanel;
import org.tlauncher.tlauncher.ui.center.CenterPanelTheme;
import javax.swing.JPasswordField;

public class ExtendedPasswordField extends JPasswordField
{
    private static final long serialVersionUID = 3175896797135831502L;
    private static final String DEFAULT_PLACEHOLDER = "password";
    private CenterPanelTheme theme;
    private String placeholder;
    
    private ExtendedPasswordField(final CenterPanel panel, final String placeholder) {
        this.theme = ((panel == null) ? CenterPanel.defaultTheme : panel.getTheme());
        this.placeholder = ((placeholder == null) ? "password" : placeholder);
        this.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(final FocusEvent e) {
                ExtendedPasswordField.this.onFocusGained();
            }
            
            @Override
            public void focusLost(final FocusEvent e) {
                ExtendedPasswordField.this.onFocusLost();
            }
        });
        this.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                ExtendedPasswordField.this.onChange();
            }
            
            @Override
            public void removeUpdate(final DocumentEvent e) {
                ExtendedPasswordField.this.onChange();
            }
            
            @Override
            public void changedUpdate(final DocumentEvent e) {
                ExtendedPasswordField.this.onChange();
            }
        });
        this.setText(null);
    }
    
    public ExtendedPasswordField() {
        this(null, null);
    }
    
    private String getValueOf(final String value) {
        if (value == null || value.isEmpty() || value.equals(this.placeholder)) {
            return null;
        }
        return value;
    }
    
    @Deprecated
    @Override
    public String getText() {
        return super.getText();
    }
    
    @Override
    public char[] getPassword() {
        final String value = this.getValue();
        if (value == null) {
            return new char[0];
        }
        return value.toCharArray();
    }
    
    public boolean hasPassword() {
        return this.getValue() != null;
    }
    
    private String getValue() {
        return this.getValueOf(this.getText());
    }
    
    @Override
    public void setText(final String text) {
        final String value = this.getValueOf(text);
        if (value == null) {
            this.setPlaceholder();
        }
        else {
            this.setForeground(this.theme.getFocus());
            super.setText(value);
        }
    }
    
    private void setPlaceholder() {
        this.setForeground(this.theme.getFocusLost());
        super.setText(this.placeholder);
    }
    
    private void setEmpty() {
        this.setForeground(this.theme.getFocus());
        super.setText("");
    }
    
    void updateStyle() {
        this.setForeground((this.getValue() == null) ? this.theme.getFocusLost() : this.theme.getFocus());
    }
    
    public String getPlaceholder() {
        return this.placeholder;
    }
    
    public void setPlaceholder(final String placeholder) {
        this.placeholder = ((placeholder == null) ? "password" : placeholder);
        if (this.getValue() == null) {
            this.setPlaceholder();
        }
    }
    
    public CenterPanelTheme getTheme() {
        return this.theme;
    }
    
    public void setTheme(CenterPanelTheme theme) {
        if (theme == null) {
            theme = CenterPanel.defaultTheme;
        }
        this.theme = theme;
        this.updateStyle();
    }
    
    protected void onFocusGained() {
        if (this.getValue() == null) {
            this.setEmpty();
        }
    }
    
    protected void onFocusLost() {
        if (this.getValue() == null) {
            this.setPlaceholder();
        }
    }
    
    protected void onChange() {
    }
}
