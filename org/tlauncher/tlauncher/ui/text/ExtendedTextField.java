// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.text;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import org.tlauncher.tlauncher.ui.center.CenterPanel;
import org.tlauncher.tlauncher.ui.center.CenterPanelTheme;
import javax.swing.JTextField;

public class ExtendedTextField extends JTextField
{
    private static final long serialVersionUID = -1963422246993419362L;
    private CenterPanelTheme theme;
    private String placeholder;
    private String oldPlaceholder;
    
    protected ExtendedTextField(final CenterPanel panel, final String placeholder, final String value) {
        this.theme = ((panel == null) ? CenterPanel.defaultTheme : panel.getTheme());
        this.placeholder = placeholder;
        this.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(final FocusEvent e) {
                ExtendedTextField.this.onFocusGained();
            }
            
            @Override
            public void focusLost(final FocusEvent e) {
                ExtendedTextField.this.onFocusLost();
            }
        });
        this.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                ExtendedTextField.this.onChange();
            }
            
            @Override
            public void removeUpdate(final DocumentEvent e) {
                ExtendedTextField.this.onChange();
            }
            
            @Override
            public void changedUpdate(final DocumentEvent e) {
                ExtendedTextField.this.onChange();
            }
        });
        this.setValue(value);
    }
    
    public ExtendedTextField(final String placeholder, final String value) {
        this(null, placeholder, value);
    }
    
    public ExtendedTextField(final String placeholder) {
        this(null, placeholder, null);
    }
    
    @Deprecated
    @Override
    public String getText() {
        return super.getText();
    }
    
    private String getValueOf(final String value) {
        if (value == null || value.isEmpty() || value.equals(this.placeholder) || value.equals(this.oldPlaceholder)) {
            return null;
        }
        return value;
    }
    
    public String getValue() {
        return this.getValueOf(this.getText());
    }
    
    @Override
    public void setText(final String text) {
        final String value = this.getValueOf(text);
        if (value == null) {
            this.setPlaceholder();
        }
        else {
            this.setForeground(this.theme.getFocus());
            this.setRawText(value);
        }
    }
    
    private void setPlaceholder() {
        this.setForeground(this.theme.getFocusLost());
        this.setRawText(this.placeholder);
    }
    
    private void setEmpty() {
        this.setForeground(this.theme.getFocus());
        this.setRawText("");
    }
    
    protected void updateStyle() {
        this.setForeground((this.getValue() == null) ? this.theme.getFocusLost() : this.theme.getFocus());
    }
    
    public void setValue(final Object obj) {
        this.setText((obj == null) ? null : obj.toString());
    }
    
    protected void setValue(final String s) {
        this.setText(s);
    }
    
    protected void setRawText(final String s) {
        super.setText(s);
        super.setCaretPosition(0);
    }
    
    public String getPlaceholder() {
        return this.placeholder;
    }
    
    protected void setPlaceholder(final String placeholder) {
        this.oldPlaceholder = this.placeholder;
        this.placeholder = placeholder;
        if (this.getValue() == null) {
            this.setPlaceholder();
        }
    }
    
    public CenterPanelTheme getTheme() {
        return this.theme;
    }
    
    protected void setTheme(CenterPanelTheme theme) {
        if (theme == null) {
            theme = CenterPanel.defaultTheme;
        }
        this.theme = theme;
        this.updateStyle();
    }
    
    protected void onFocusGained() {
        if (this.getValue() == null) {
            this.setEmpty();
        }
    }
    
    protected void onFocusLost() {
        if (this.getValue() == null) {
            this.setPlaceholder();
        }
    }
    
    protected void onChange() {
    }
}
