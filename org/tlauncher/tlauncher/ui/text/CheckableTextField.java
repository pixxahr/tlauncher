// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.text;

import org.tlauncher.tlauncher.ui.center.CenterPanel;

public abstract class CheckableTextField extends ExtendedTextField
{
    private static final long serialVersionUID = 2835507963141686372L;
    private CenterPanel parent;
    
    protected CheckableTextField(final CenterPanel panel, final String placeholder, final String value) {
        super(panel, placeholder, value);
        this.parent = panel;
    }
    
    public CheckableTextField(final String placeholder, final String value) {
        this(null, placeholder, value);
    }
    
    public CheckableTextField(final String placeholder) {
        this(null, placeholder, null);
    }
    
    public CheckableTextField(final CenterPanel panel) {
        this(panel, null, null);
    }
    
    boolean check() {
        final String text = this.getValue();
        final String result = this.check(text);
        if (result == null) {
            return this.setValid();
        }
        return this.setInvalid(result);
    }
    
    public boolean setInvalid(final String reason) {
        this.setBackground(this.getTheme().getFailure());
        this.setForeground(this.getTheme().getFocus());
        if (this.parent != null) {
            this.parent.setError(reason);
        }
        return false;
    }
    
    public boolean setValid() {
        this.setBackground(this.getTheme().getBackground());
        this.setForeground(this.getTheme().getFocus());
        if (this.parent != null) {
            this.parent.setError(null);
        }
        return true;
    }
    
    @Override
    protected void updateStyle() {
        super.updateStyle();
        this.check();
    }
    
    @Override
    protected void onChange() {
        this.check();
    }
    
    protected abstract String check(final String p0);
}
