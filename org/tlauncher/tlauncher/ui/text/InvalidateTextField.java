// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.text;

import org.tlauncher.tlauncher.ui.center.CenterPanel;

public class InvalidateTextField extends CheckableTextField
{
    private static final long serialVersionUID = -4076362911409776688L;
    
    protected InvalidateTextField(final CenterPanel panel, final String placeholder, final String value) {
        super(panel, placeholder, value);
    }
    
    public InvalidateTextField(final CenterPanel panel) {
        this(panel, null, null);
    }
    
    @Override
    protected String check(final String text) {
        return null;
    }
}
