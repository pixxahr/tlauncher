// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

public interface StringConverter<T>
{
    T fromString(final String p0);
    
    String toString(final T p0);
    
    String toValue(final T p0);
    
    Class<T> getObjectClass();
}
