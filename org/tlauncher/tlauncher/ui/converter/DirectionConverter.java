// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

import org.tlauncher.util.Reflect;
import org.tlauncher.util.Direction;
import org.tlauncher.tlauncher.ui.loc.LocalizableStringConverter;

public class DirectionConverter extends LocalizableStringConverter<Direction>
{
    public DirectionConverter() {
        super("settings.direction");
    }
    
    @Override
    public Direction fromString(final String from) {
        return Reflect.parseEnum(Direction.class, from);
    }
    
    @Override
    public String toValue(final Direction from) {
        if (from == null) {
            return null;
        }
        return from.toString().toLowerCase();
    }
    
    @Override
    public Class<Direction> getObjectClass() {
        return Direction.class;
    }
    
    @Override
    protected String toPath(final Direction from) {
        return this.toValue(from);
    }
}
