// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.managers.ProfileManager;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.tlauncher.ui.loc.LocalizableStringConverter;

public class AccountConverter extends LocalizableStringConverter<Account>
{
    private final ProfileManager pm;
    
    public AccountConverter(final ProfileManager pm) {
        super(null);
        if (pm == null) {
            throw new NullPointerException();
        }
        this.pm = pm;
    }
    
    @Override
    public String toString(final Account from) {
        if (from == null) {
            return Localizable.get("account.empty");
        }
        if (from.getUsername() == null) {
            return null;
        }
        return from.getUsername();
    }
    
    @Override
    public Account fromString(final String from) {
        return this.pm.getAuthDatabase().getByUsername(from);
    }
    
    @Override
    public String toValue(final Account from) {
        if (from == null || from.getUsername() == null) {
            return null;
        }
        return from.getUsername();
    }
    
    public String toPath(final Account from) {
        return null;
    }
    
    @Override
    public Class<Account> getObjectClass() {
        return Account.class;
    }
}
