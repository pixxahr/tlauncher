// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import org.tlauncher.tlauncher.ui.loc.LocalizableStringConverter;

public class ConnectionQualityConverter extends LocalizableStringConverter<ConnectionQuality>
{
    public ConnectionQualityConverter() {
        super("settings.connection");
    }
    
    @Override
    public ConnectionQuality fromString(final String from) {
        return ConnectionQuality.get(from);
    }
    
    @Override
    public String toValue(final ConnectionQuality from) {
        return from.toString();
    }
    
    public String toPath(final ConnectionQuality from) {
        return from.toString();
    }
    
    @Override
    public Class<ConnectionQuality> getObjectClass() {
        return ConnectionQuality.class;
    }
}
