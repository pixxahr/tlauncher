// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

import net.minecraft.launcher.versions.ReleaseType;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.managers.VersionManager;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.ui.loc.LocalizableStringConverter;

public class VersionConverter extends LocalizableStringConverter<VersionSyncInfo>
{
    private static final VersionSyncInfo LOADING;
    private static final VersionSyncInfo EMPTY;
    private final VersionManager vm;
    
    public VersionConverter(final VersionManager vm) {
        super(null);
        if (vm == null) {
            throw new NullPointerException();
        }
        this.vm = vm;
    }
    
    @Override
    public String toString(final VersionSyncInfo from) {
        if (from == null) {
            return null;
        }
        if (from.equals(VersionConverter.LOADING)) {
            return Localizable.get("versions.loading");
        }
        if (from.equals(VersionConverter.EMPTY)) {
            return Localizable.get("versions.notfound.tip");
        }
        final String id = from.getID();
        final ReleaseType type = from.getLatestVersion().getReleaseType();
        if (type == null || type.equals(ReleaseType.UNKNOWN)) {
            return id;
        }
        final String typeF = type.toString().toLowerCase();
        final String formatted = Localizable.get().nget("version." + typeF, id);
        if (formatted == null) {
            return id;
        }
        return formatted;
    }
    
    @Override
    public VersionSyncInfo fromString(final String from) {
        return this.vm.getVersionSyncInfo(from);
    }
    
    @Override
    public String toValue(final VersionSyncInfo from) {
        return null;
    }
    
    public String toPath(final VersionSyncInfo from) {
        return null;
    }
    
    @Override
    public Class<VersionSyncInfo> getObjectClass() {
        return VersionSyncInfo.class;
    }
    
    static {
        LOADING = VersionSyncInfo.createEmpty();
        EMPTY = VersionSyncInfo.createEmpty();
    }
}
