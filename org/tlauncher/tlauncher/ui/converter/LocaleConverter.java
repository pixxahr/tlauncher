// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Locale;

public class LocaleConverter implements StringConverter<Locale>
{
    @Override
    public String toString(final Locale from) {
        if (from == null) {
            return null;
        }
        if (from.equals(Locale.ENGLISH)) {
            TLauncher.getInstance();
            return TLauncher.getInnerSettings().get("converter.value" + from.toString());
        }
        TLauncher.getInstance();
        return TLauncher.getInnerSettings().get("converter.value." + from.toString());
    }
    
    @Override
    public Locale fromString(final String from) {
        return Configuration.getLocaleOf(from);
    }
    
    @Override
    public String toValue(final Locale from) {
        if (from == null) {
            return null;
        }
        return from.toString();
    }
    
    @Override
    public Class<Locale> getObjectClass() {
        return Locale.class;
    }
}
