// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter.dummy;

import org.tlauncher.tlauncher.ui.converter.StringConverter;

public abstract class DummyConverter<T> implements StringConverter<T>
{
    private static DummyConverter<Object>[] converters;
    
    public static DummyConverter<Object>[] getConverters() {
        if (DummyConverter.converters == null) {
            DummyConverter.converters = (DummyConverter<Object>[])new DummyConverter[] { new DummyStringConverter(), new DummyIntegerConverter(), new DummyDoubleConverter(), new DummyLongConverter(), new DummyDateConverter() };
        }
        return DummyConverter.converters;
    }
    
    @Override
    public T fromString(final String from) {
        return this.fromDummyString(from);
    }
    
    @Override
    public String toString(final T from) {
        return this.toValue(from);
    }
    
    @Override
    public String toValue(final T from) {
        return this.toDummyValue(from);
    }
    
    public abstract T fromDummyString(final String p0) throws RuntimeException;
    
    public abstract String toDummyValue(final T p0) throws RuntimeException;
}
