// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter.dummy;

public class DummyIntegerConverter extends DummyConverter<Integer>
{
    @Override
    public Integer fromDummyString(final String from) throws RuntimeException {
        return Integer.parseInt(from);
    }
    
    @Override
    public String toDummyValue(final Integer value) throws RuntimeException {
        return value.toString();
    }
    
    @Override
    public Class<Integer> getObjectClass() {
        return Integer.class;
    }
}
