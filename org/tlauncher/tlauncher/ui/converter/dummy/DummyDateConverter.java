// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter.dummy;

import net.minecraft.launcher.versions.json.DateTypeAdapter;
import java.util.Date;

public class DummyDateConverter extends DummyConverter<Date>
{
    private final DateTypeAdapter dateAdapter;
    
    public DummyDateConverter() {
        this.dateAdapter = new DateTypeAdapter();
    }
    
    @Override
    public Date fromDummyString(final String from) throws RuntimeException {
        return this.dateAdapter.toDate(from);
    }
    
    @Override
    public String toDummyValue(final Date value) throws RuntimeException {
        return this.dateAdapter.toString(value);
    }
    
    @Override
    public Class<Date> getObjectClass() {
        return Date.class;
    }
}
