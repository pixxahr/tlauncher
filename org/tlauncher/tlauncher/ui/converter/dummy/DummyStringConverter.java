// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter.dummy;

public class DummyStringConverter extends DummyConverter<String>
{
    @Override
    public String fromDummyString(final String from) throws RuntimeException {
        return from;
    }
    
    @Override
    public String toDummyValue(final String value) throws RuntimeException {
        return value;
    }
    
    @Override
    public Class<String> getObjectClass() {
        return String.class;
    }
}
