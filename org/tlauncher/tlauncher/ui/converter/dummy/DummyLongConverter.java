// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter.dummy;

public class DummyLongConverter extends DummyConverter<Long>
{
    @Override
    public Long fromDummyString(final String from) throws RuntimeException {
        return Long.parseLong(from);
    }
    
    @Override
    public String toDummyValue(final Long value) throws RuntimeException {
        return value.toString();
    }
    
    @Override
    public Class<Long> getObjectClass() {
        return Long.class;
    }
}
