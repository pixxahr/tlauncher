// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter.dummy;

public class DummyDoubleConverter extends DummyConverter<Double>
{
    @Override
    public Double fromDummyString(final String from) throws RuntimeException {
        return Double.parseDouble(from);
    }
    
    @Override
    public String toDummyValue(final Double value) throws RuntimeException {
        return value.toString();
    }
    
    @Override
    public Class<Double> getObjectClass() {
        return Double.class;
    }
}
