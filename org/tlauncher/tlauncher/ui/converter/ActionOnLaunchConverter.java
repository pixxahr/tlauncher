// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

import org.tlauncher.tlauncher.configuration.enums.ActionOnLaunch;
import org.tlauncher.tlauncher.ui.loc.LocalizableStringConverter;

public class ActionOnLaunchConverter extends LocalizableStringConverter<ActionOnLaunch>
{
    public ActionOnLaunchConverter() {
        super("settings.launch-action");
    }
    
    @Override
    public ActionOnLaunch fromString(final String from) {
        return ActionOnLaunch.get(from);
    }
    
    @Override
    public String toValue(final ActionOnLaunch from) {
        return from.toString();
    }
    
    public String toPath(final ActionOnLaunch from) {
        return from.toString();
    }
    
    @Override
    public Class<ActionOnLaunch> getObjectClass() {
        return ActionOnLaunch.class;
    }
}
