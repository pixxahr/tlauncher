// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

import net.minecraft.launcher.versions.ReleaseType;
import org.tlauncher.tlauncher.ui.loc.LocalizableStringConverter;

public class ReleaseTypeConverter extends LocalizableStringConverter<ReleaseType>
{
    public ReleaseTypeConverter() {
        super("version.description");
    }
    
    @Override
    public ReleaseType fromString(final String from) {
        if (from == null) {
            return ReleaseType.UNKNOWN;
        }
        for (final ReleaseType type : ReleaseType.values()) {
            if (type.toString().equals(from)) {
                return type;
            }
        }
        return null;
    }
    
    @Override
    public String toValue(final ReleaseType from) {
        if (from == null) {
            return ReleaseType.UNKNOWN.toString();
        }
        return from.toString();
    }
    
    @Override
    protected String toPath(final ReleaseType from) {
        if (from == null) {
            return ReleaseType.UNKNOWN.toString();
        }
        return this.toValue(from);
    }
    
    @Override
    public Class<ReleaseType> getObjectClass() {
        return ReleaseType.class;
    }
}
