// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.converter;

import org.tlauncher.tlauncher.configuration.enums.ConsoleType;
import org.tlauncher.tlauncher.ui.loc.LocalizableStringConverter;

public class ConsoleTypeConverter extends LocalizableStringConverter<ConsoleType>
{
    public ConsoleTypeConverter() {
        super("settings.console");
    }
    
    @Override
    public ConsoleType fromString(final String from) {
        return ConsoleType.get(from);
    }
    
    @Override
    public String toValue(final ConsoleType from) {
        if (from == null) {
            return null;
        }
        return from.toString();
    }
    
    public String toPath(final ConsoleType from) {
        if (from == null) {
            return null;
        }
        return from.toString();
    }
    
    @Override
    public Class<ConsoleType> getObjectClass() {
        return ConsoleType.class;
    }
}
