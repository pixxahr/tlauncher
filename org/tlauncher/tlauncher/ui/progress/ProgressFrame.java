// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.progress;

import com.google.inject.Inject;
import javax.swing.plaf.ProgressBarUI;
import org.tlauncher.tlauncher.ui.ui.PreloaderProgressUI;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import java.awt.Component;
import com.google.inject.assistedinject.Assisted;
import java.awt.Color;
import javax.swing.JProgressBar;
import java.awt.Font;
import javax.swing.JFrame;

public class ProgressFrame extends JFrame
{
    private static final int WIDTH = 240;
    private static final int HEIGHT = 99;
    private Font font;
    private String version_info;
    private JProgressBar progressBar;
    public final Color VERSION_BACKGROUND;
    
    @Inject
    public ProgressFrame(@Assisted("info") final String info) {
        this.font = new Font("Verdana", 0, 10);
        this.VERSION_BACKGROUND = new Color(40, 134, 187);
        this.getContentPane().setForeground(Color.LIGHT_GRAY);
        this.setTitle("TLauncher");
        this.setSize(240, 99);
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);
        this.setResizable(false);
        this.setBackground(Color.LIGHT_GRAY);
        this.setDefaultCloseOperation(3);
        this.version_info = info;
        final SpringLayout springLayout = new SpringLayout();
        this.getContentPane().setLayout(springLayout);
        this.getContentPane().setPreferredSize(new Dimension(240, 99));
        final JLabel version = new JLabel(this.version_info);
        version.setHorizontalAlignment(0);
        version.setForeground(Color.WHITE);
        version.setFont(this.font);
        version.setOpaque(true);
        version.setBackground(this.VERSION_BACKGROUND);
        springLayout.putConstraint("North", version, 0, "North", this.getContentPane());
        springLayout.putConstraint("West", version, -58, "East", this.getContentPane());
        springLayout.putConstraint("South", version, 16, "North", this.getContentPane());
        springLayout.putConstraint("East", version, 0, "East", this.getContentPane());
        this.getContentPane().add(version);
        final JLabel backgroundImage = new JLabel();
        backgroundImage.setIcon(ImageCache.getNativeIcon("tlauncher.png"));
        springLayout.putConstraint("North", backgroundImage, 0, "North", this.getContentPane());
        springLayout.putConstraint("West", backgroundImage, 0, "West", this.getContentPane());
        springLayout.putConstraint("South", backgroundImage, 75, "North", this.getContentPane());
        springLayout.putConstraint("East", backgroundImage, 244, "West", this.getContentPane());
        this.getContentPane().add(backgroundImage);
        (this.progressBar = new JProgressBar()).setIndeterminate(true);
        this.progressBar.setBorder(BorderFactory.createEmptyBorder());
        this.progressBar.setUI(new PreloaderProgressUI(ImageCache.getBufferedImage("bottom-bar.png"), ImageCache.getBufferedImage("up-progress-bar.png")));
        springLayout.putConstraint("North", this.progressBar, 0, "South", backgroundImage);
        springLayout.putConstraint("West", this.progressBar, 0, "West", this.getContentPane());
        springLayout.putConstraint("South", this.progressBar, 0, "South", this.getContentPane());
        springLayout.putConstraint("East", this.progressBar, 4, "East", this.getContentPane());
        this.getContentPane().add(this.progressBar);
        this.pack();
        this.setVisible(true);
    }
}
