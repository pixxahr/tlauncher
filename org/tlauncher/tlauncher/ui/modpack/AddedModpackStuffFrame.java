// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.event.ActionEvent;
import javax.swing.text.JTextComponent;
import javax.swing.JButton;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import org.tlauncher.util.TlauncherUtil;
import java.awt.Component;
import javax.swing.border.Border;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.border.CompoundBorder;
import javax.swing.BorderFactory;
import org.tlauncher.util.ColorUtil;
import javax.swing.JTextField;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import org.tlauncher.tlauncher.ui.swing.HtmlTextPane;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.JComponent;
import java.awt.Color;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.JFrame;
import org.tlauncher.tlauncher.controller.AddedModpackStuffController;
import java.awt.Dimension;

public class AddedModpackStuffFrame extends TemlateModpackFrame
{
    private static final Dimension DEFAULT_SIZE;
    private final AddedModpackStuffController controller;
    
    public AddedModpackStuffFrame(final JFrame parent) {
        super(parent, "modpack.added.request", AddedModpackStuffFrame.DEFAULT_SIZE);
        this.controller = (AddedModpackStuffController)TLauncher.getInjector().getInstance((Class)AddedModpackStuffController.class);
        final SpringLayout spring = new SpringLayout();
        final JPanel panel = new JPanel(spring);
        panel.setBackground(Color.WHITE);
        this.addCenter(panel);
        final HtmlTextPane message = HtmlTextPane.get(String.format("<div><center>%s</center></div>", Localizable.get("modpack.added.request.message")));
        message.setOpaque(false);
        final JButton send = new UpdaterButton(AddedModpackStuffFrame.BLUE_COLOR, AddedModpackStuffFrame.BLUE_COLOR_UNDER, "log.form.send");
        final JTextField link = new JTextField();
        final CompoundBorder border = new CompoundBorder(BorderFactory.createLineBorder(ColorUtil.COLOR_233, 1), BorderFactory.createEmptyBorder(0, 21, 0, 0));
        final CompoundBorder redBorder = new CompoundBorder(BorderFactory.createLineBorder(Color.RED, 1), BorderFactory.createEmptyBorder(0, 21, 0, 0));
        SwingUtil.changeFontFamily(message, FontTL.ROBOTO_REGULAR, 15, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(send, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        link.setBorder(border);
        spring.putConstraint("West", message, 29, "West", panel);
        spring.putConstraint("East", message, -27, "East", panel);
        spring.putConstraint("North", message, 40, "North", panel);
        spring.putConstraint("South", message, 170, "North", panel);
        panel.add(message);
        spring.putConstraint("West", link, 29, "West", panel);
        spring.putConstraint("East", link, -27, "East", panel);
        spring.putConstraint("North", link, 20, "South", message);
        spring.putConstraint("South", link, 66, "South", message);
        panel.add(link);
        spring.putConstraint("West", send, 175, "West", panel);
        spring.putConstraint("East", send, -174, "East", panel);
        spring.putConstraint("North", send, 20, "South", link);
        spring.putConstraint("South", send, 60, "South", link);
        panel.add(send);
        final JTextComponent textComponent;
        final int code;
        final Border border2;
        send.addActionListener(e -> {
            code = TlauncherUtil.hostAvailabilityCheck(textComponent.getText());
            if (code == 404 || code > 500) {
                textComponent.setBorder(border2);
                return;
            }
            else {
                this.setVisible(false);
                this.controller.send(textComponent.getText());
                return;
            }
        });
        link.addFocusListener(new FocusListener() {
            @Override
            public void focusLost(final FocusEvent e) {
            }
            
            @Override
            public void focusGained(final FocusEvent e) {
                link.setBorder(border);
            }
        });
    }
    
    static {
        DEFAULT_SIZE = new Dimension(572, 382);
    }
}
