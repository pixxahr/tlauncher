// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import javax.swing.Box;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.JLayeredPane;
import org.tlauncher.util.OS;
import javax.swing.plaf.SliderUI;
import org.tlauncher.tlauncher.ui.ui.ModpackSliderUI;
import java.awt.Dimension;
import javax.swing.JSlider;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class SliderModpackPanel extends ExtendedPanel
{
    private final Color color179;
    private Color downBackground;
    private int downHeight;
    private Color color233;
    JSlider slider;
    
    public SliderModpackPanel(final Dimension dimension) {
        this.color179 = new Color(179, 179, 179);
        this.downBackground = new Color(244, 252, 255);
        this.downHeight = 40;
        this.color233 = new Color(233, 233, 233);
        this.setPreferredSize(dimension);
        (this.slider = new JSlider()).setOpaque(false);
        this.slider.setUI(new ModpackSliderUI(this.slider));
        this.slider.setMinimum(512);
        this.slider.setMaximum(OS.Arch.MAX_MEMORY);
        this.slider.setMajorTickSpacing((OS.Arch.MAX_MEMORY - 512) / 10);
        this.slider.setSnapToTicks(true);
        this.slider.setPaintLabels(false);
        this.slider.setValue(this.slider.getMaximum() * 2 / 3);
        final JLayeredPane layeredPane = new JLayeredPane();
        final JPanel downPanel = new JPanel(new FlowLayout(0));
        downPanel.add(Box.createVerticalStrut(215));
        final JLabel recomended = new LocalizableLabel("modpack.config.recommended");
        recomended.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, this.color233));
        recomended.setHorizontalAlignment(0);
        recomended.setPreferredSize(new Dimension(131, this.downHeight));
        final JLabel highLevel = new LocalizableLabel("modpack.config.high.level");
        highLevel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, this.color233));
        highLevel.setHorizontalAlignment(0);
        highLevel.setPreferredSize(new Dimension(171, 20));
        SwingUtil.changeFontFamily(this.slider, FontTL.ROBOTO_BOLD, 14, Color.BLACK);
        SwingUtil.changeFontFamily(highLevel, FontTL.ROBOTO_REGULAR, 12, this.color179);
        SwingUtil.changeFontFamily(recomended, FontTL.ROBOTO_REGULAR, 12, this.color179);
        downPanel.setBackground(this.downBackground);
        layeredPane.setPreferredSize(dimension);
        layeredPane.add(this.slider, new Integer(2));
        layeredPane.add(highLevel, new Integer(1));
        layeredPane.add(recomended, new Integer(1));
        layeredPane.add(downPanel, new Integer(0));
        recomended.setBounds(225, 27, 130, 39);
        highLevel.setBounds(356, 27, 171, 39);
        final int border = 8;
        downPanel.setBounds(border + 3, dimension.height - this.downHeight - 12, dimension.width - border * 2 - 3, this.downHeight);
        this.slider.setBounds(0, 0, dimension.width, dimension.height - 20);
        this.add(layeredPane);
    }
    
    public int getValue() {
        return this.slider.getValue();
    }
    
    public void setValue(final int value) {
        this.slider.setValue(value);
    }
}
