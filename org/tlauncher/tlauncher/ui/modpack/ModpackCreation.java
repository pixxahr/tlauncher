// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.event.ActionEvent;
import java.awt.Container;
import java.util.Iterator;
import javax.swing.AbstractButton;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import java.util.Objects;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import org.tlauncher.util.U;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import com.google.common.collect.Lists;
import org.tlauncher.tlauncher.managers.ModpackManager;
import java.awt.Component;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import org.tlauncher.tlauncher.ui.ui.CreationModpackForgeComboboxUI;
import org.tlauncher.tlauncher.ui.swing.renderer.CreationModpackForgeComboboxRenderer;
import org.tlauncher.modpack.domain.client.share.ForgeStringComparator;
import javax.swing.plaf.ComboBoxUI;
import org.tlauncher.tlauncher.ui.ui.CreationModpackGameVersionComboboxUI;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.swing.renderer.CreationModpackGameVersionComboboxRenderer;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.loc.UpdaterFullButton;
import org.tlauncher.tlauncher.ui.editor.EditorCheckBox;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.util.Comparator;
import java.util.List;
import javax.swing.JFrame;
import org.tlauncher.tlauncher.configuration.Configuration;
import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import org.tlauncher.modpack.domain.client.GameVersionDTO;
import javax.swing.JComboBox;
import org.tlauncher.tlauncher.ui.loc.LocalizableTextField;

public class ModpackCreation extends TemlateModpackFrame
{
    private final LocalizableTextField versionName;
    private final JComboBox<GameVersionDTO> gameVersion;
    private final JComboBox<String> forges;
    private JButton create;
    private JCheckBox box;
    private JButton cancel;
    private static final Dimension maxSize;
    private static final Dimension minSize;
    private JLabel question;
    private Configuration c;
    
    public ModpackCreation(final JFrame parent, final List<GameVersionDTO> list) {
        super(parent, "modpack.creation.modpack", new Dimension(572, 479));
        list.sort(new GameVersionDTOComporator());
        this.question = new JLabel(ImageCache.getNativeIcon("qestion-option-panel.png"));
        this.c = TLauncher.getInstance().getConfiguration();
        this.question.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                ModpackCreation.this.setVisible(false);
                Alert.showLocMessage(ModpackCreation.this.c.get("memory.problem.message"));
                ModpackCreation.this.setVisible(true);
            }
        });
        final SpringLayout spring = new SpringLayout();
        final JPanel panel = new JPanel(spring);
        panel.setBorder(new EmptyBorder(0, 0, 33, 0));
        this.addCenter(panel);
        panel.setBackground(Color.WHITE);
        final JLabel nameLabel = new JLabel(Localizable.get("modpack.creation.name"));
        nameLabel.setForeground(ColorUtil.COLOR_149);
        final JLabel selectedMemory = new LocalizableLabel("settings.java.memory.label");
        final JLabel memorySettings = new LocalizableLabel("modpack.config.memory.title");
        final JLabel optifineLabel = new LocalizableLabel("modpack.config.system.label");
        (this.box = new EditorCheckBox("modpack.config.memory.box")).setIconTextGap(14);
        final EditorCheckBox optifineBox = new EditorCheckBox("modpack.config.skin.use");
        optifineBox.setIconTextGap(14);
        optifineBox.setSelected(true);
        final JLabel versionGameLabel = new JLabel(Localizable.get("modpack.table.pack.element.version") + ":");
        versionGameLabel.setForeground(ColorUtil.COLOR_149);
        final JLabel forgesLabel = new JLabel(Localizable.get("modpack.creation.forge"));
        forgesLabel.setForeground(ColorUtil.COLOR_149);
        (this.create = new UpdaterFullButton(ModpackCreation.COLOR_0_174_239, ColorUtil.BLUE_MODPACK_BUTTON_UP, "modpack.create.button", "modpack-creation.button.png")).setBorder(new EmptyBorder(0, 29, 0, 0));
        this.create.setIconTextGap(42);
        (this.cancel = new UpdaterFullButton(new Color(208, 43, 43), new Color(180, 39, 39), "loginform.enter.cancel", "modpack-cancel-button.png")).setBorder(new EmptyBorder(0, 29, 0, 0));
        this.cancel.setIconTextGap(42);
        (this.gameVersion = new JComboBox<GameVersionDTO>(list.toArray(new GameVersionDTO[0]))).setForeground(ColorUtil.COLOR_25);
        this.gameVersion.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, ColorUtil.COLOR_149));
        this.gameVersion.setRenderer(new CreationModpackGameVersionComboboxRenderer());
        this.gameVersion.setUI(new CreationModpackGameVersionComboboxUI());
        if (list.size() > 0) {
            list.get(0).getForgeVersions().sort(new ForgeStringComparator());
            this.forges = new JComboBox<String>(list.get(0).getForgeVersions().toArray(new String[0]));
        }
        else {
            this.forges = new JComboBox<String>();
        }
        this.forges.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, ColorUtil.COLOR_149));
        this.forges.setRenderer(new CreationModpackForgeComboboxRenderer());
        this.forges.setUI(new CreationModpackForgeComboboxUI());
        (this.versionName = new LocalizableTextField("modpack.creation.input.name") {
            @Override
            protected void onFocusLost() {
                super.onFocusLost();
                if (super.getValue() == null) {
                    ModpackCreation.this.versionName.setForeground(ColorUtil.COLOR_202);
                }
            }
            
            @Override
            protected void onFocusGained() {
                super.onFocusGained();
                ModpackCreation.this.versionName.setForeground(ColorUtil.COLOR_25);
            }
        }).setHorizontalAlignment(0);
        this.versionName.setBorder(BorderFactory.createLineBorder(ColorUtil.COLOR_149, 1));
        final SliderModpackPanel slider = new SliderModpackPanel(new Dimension(534, 80));
        SwingUtil.changeFontFamily(nameLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_149);
        SwingUtil.changeFontFamily(forgesLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_149);
        SwingUtil.changeFontFamily(versionGameLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_149);
        SwingUtil.changeFontFamily(memorySettings, FontTL.ROBOTO_BOLD, 14, Color.BLACK);
        SwingUtil.changeFontFamily(selectedMemory, FontTL.ROBOTO_BOLD, 14, Color.BLACK);
        SwingUtil.changeFontFamily(optifineLabel, FontTL.ROBOTO_BOLD, 14, Color.BLACK);
        SwingUtil.changeFontFamily(optifineBox, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(this.create, FontTL.ROBOTO_BOLD, 12, Color.WHITE);
        SwingUtil.changeFontFamily(this.box, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(this.gameVersion, FontTL.ROBOTO_BOLD, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(this.forges, FontTL.ROBOTO_BOLD, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(this.cancel, FontTL.ROBOTO_BOLD, 12, Color.WHITE);
        SwingUtil.changeFontFamily(this.versionName, FontTL.ROBOTO_BOLD, 18, ColorUtil.COLOR_202);
        final int height = 18;
        spring.putConstraint("West", nameLabel, 253, "West", panel);
        spring.putConstraint("East", nameLabel, 353, "West", panel);
        spring.putConstraint("North", nameLabel, 23, "North", panel);
        spring.putConstraint("South", nameLabel, 23 + height, "North", panel);
        panel.add(nameLabel);
        spring.putConstraint("West", this.versionName, 29, "West", panel);
        spring.putConstraint("East", this.versionName, -27, "East", panel);
        spring.putConstraint("North", this.versionName, 2, "South", nameLabel);
        spring.putConstraint("South", this.versionName, 46, "South", nameLabel);
        panel.add(this.versionName);
        spring.putConstraint("West", versionGameLabel, 30, "West", panel);
        spring.putConstraint("East", versionGameLabel, 267, "West", panel);
        spring.putConstraint("North", versionGameLabel, 18, "South", this.versionName);
        spring.putConstraint("South", versionGameLabel, 18 + height, "South", this.versionName);
        panel.add(versionGameLabel);
        spring.putConstraint("West", forgesLabel, 308, "West", panel);
        spring.putConstraint("East", forgesLabel, 545, "West", panel);
        spring.putConstraint("North", forgesLabel, 18, "South", this.versionName);
        spring.putConstraint("South", forgesLabel, 18 + height, "South", this.versionName);
        panel.add(forgesLabel);
        spring.putConstraint("West", this.gameVersion, 29, "West", panel);
        spring.putConstraint("East", this.gameVersion, 267, "West", panel);
        spring.putConstraint("North", this.gameVersion, 5, "South", forgesLabel);
        spring.putConstraint("South", this.gameVersion, 49, "South", forgesLabel);
        panel.add(this.gameVersion);
        spring.putConstraint("West", this.forges, 307, "West", panel);
        spring.putConstraint("East", this.forges, 545, "West", panel);
        spring.putConstraint("North", this.forges, 5, "South", forgesLabel);
        spring.putConstraint("South", this.forges, 49, "South", forgesLabel);
        panel.add(this.forges);
        spring.putConstraint("West", memorySettings, 29, "West", panel);
        spring.putConstraint("East", memorySettings, 229, "West", panel);
        spring.putConstraint("North", memorySettings, 15, "South", this.forges);
        spring.putConstraint("South", memorySettings, 32, "South", this.forges);
        panel.add(memorySettings);
        spring.putConstraint("West", this.box, 179, "West", panel);
        spring.putConstraint("East", this.box, 529, "West", panel);
        spring.putConstraint("North", this.box, 15, "South", this.forges);
        spring.putConstraint("South", this.box, 32, "South", this.forges);
        panel.add(this.box);
        spring.putConstraint("West", optifineLabel, 29, "West", panel);
        spring.putConstraint("East", optifineLabel, 229, "West", this.forges);
        spring.putConstraint("North", optifineLabel, 10, "South", memorySettings);
        spring.putConstraint("South", optifineLabel, 27, "South", memorySettings);
        panel.add(optifineLabel);
        spring.putConstraint("West", optifineBox, 179, "West", panel);
        spring.putConstraint("East", optifineBox, 529, "West", panel);
        spring.putConstraint("North", optifineBox, 10, "South", memorySettings);
        spring.putConstraint("South", optifineBox, 27, "South", memorySettings);
        panel.add(optifineBox);
        spring.putConstraint("West", this.create, 29, "West", panel);
        spring.putConstraint("East", this.create, 267, "West", panel);
        spring.putConstraint("North", this.create, -43, "South", panel);
        spring.putConstraint("South", this.create, 0, "South", panel);
        panel.add(this.create);
        spring.putConstraint("West", this.cancel, 307, "West", panel);
        spring.putConstraint("East", this.cancel, 545, "West", panel);
        spring.putConstraint("North", this.cancel, -43, "South", panel);
        spring.putConstraint("South", this.cancel, 0, "South", panel);
        panel.add(this.cancel);
        final String name;
        ModpackManager modpackManager;
        ModpackDTO modpackDTO;
        ModpackVersionDTO v;
        final SliderModpackPanel sliderModpackPanel;
        final AbstractButton abstractButton;
        this.create.addActionListener(e -> {
            name = this.versionName.getValue();
            if (name == null) {
                this.versionName.setBorder(BorderFactory.createLineBorder(new Color(255, 62, 62), 1));
                return;
            }
            else {
                modpackManager = (ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class);
                if (!modpackManager.checkNameVersion(Lists.newArrayList((Object[])new String[] { name }))) {
                    this.setVisible(false);
                    Alert.showLocWarning("modpack.config.memory.message");
                    this.setVisible(true);
                    return;
                }
                else {
                    modpackDTO = new ModpackDTO();
                    modpackDTO.setId(-U.n());
                    v = new ModpackVersionDTO();
                    v.setId(-U.n() - 1L);
                    modpackDTO.setName(name);
                    v.setGameVersion(Objects.requireNonNull(this.gameVersion.getSelectedItem()).getName());
                    v.setName("1.0");
                    v.setForgeVersion((String)this.forges.getSelectedItem());
                    modpackDTO.setVersion(v);
                    if (!this.box.isSelected()) {
                        modpackDTO.setModpackMemory(true);
                        modpackDTO.setMemory(sliderModpackPanel.getValue());
                    }
                    modpackManager.createModpack(name, modpackDTO, abstractButton.isSelected());
                    this.setVisible(false);
                    return;
                }
            }
        });
        this.gameVersion.addItemListener(new ItemListener() {
            private ItemListener l;
            
            @Override
            public void itemStateChanged(final ItemEvent e) {
                if (e.getStateChange() == 1) {
                    ModpackCreation.this.forges.removeAllItems();
                    final GameVersionDTO g = (GameVersionDTO)e.getItem();
                    g.getForgeVersions().sort(new ForgeStringComparator());
                    final GameVersionDTO g1_7_10 = new GameVersionDTO();
                    g1_7_10.setName("1.7.10");
                    if (this.l != null) {
                        optifineBox.removeItemListener(this.l);
                    }
                    if (new GameVersionDTOComporator().compare(g1_7_10, g) < 0) {
                        optifineBox.setSelected(false);
                        final AbstractButton val$optifineBox;
                        this.l = (i -> {
                            val$optifineBox = optifineBox;
                            if (i.getStateChange() == 1) {
                                ModpackCreation.this.setVisible(false);
                                Alert.showLocMessage("modpack.config.skin.install.message");
                                val$optifineBox.setSelected(false);
                                ModpackCreation.this.setVisible(true);
                            }
                            return;
                        });
                        optifineBox.addItemListener(this.l);
                    }
                    else {
                        optifineBox.setSelected(true);
                    }
                    for (final String s : g.getForgeVersions()) {
                        ModpackCreation.this.forges.addItem(s);
                    }
                }
            }
        });
        final SpringLayout springLayout;
        final Component component;
        final Container container;
        final JLabel c;
        this.box.addItemListener(e -> {
            if (e.getStateChange() == 2) {
                this.setCenter(ModpackCreation.maxSize);
                springLayout.putConstraint("West", component, 12, "West", container);
                springLayout.putConstraint("East", component, -13, "East", container);
                springLayout.putConstraint("North", component, 259, "North", container);
                springLayout.putConstraint("South", component, 339, "South", container);
                container.add(component);
                springLayout.putConstraint("West", c, 29, "West", container);
                springLayout.putConstraint("East", c, 29 + SwingUtil.getWidthText(c, c.getText()) + 5, "West", container);
                springLayout.putConstraint("North", c, 245, "North", container);
                springLayout.putConstraint("South", c, 263, "North", container);
                container.add(c);
                springLayout.putConstraint("West", this.question, 2, "East", c);
                springLayout.putConstraint("East", this.question, 25, "East", c);
                springLayout.putConstraint("North", this.question, -153, "South", container);
                springLayout.putConstraint("South", this.question, -133, "South", container);
                container.add(this.question);
                this.setCenter(ModpackCreation.maxSize);
                if (!this.c.isExist("memory.problem.message")) {
                    this.question.setVisible(false);
                }
            }
            else {
                container.remove(c);
                container.remove(component);
                container.remove(this.question);
                this.setCenter(ModpackCreation.minSize);
            }
            return;
        });
        this.cancel.addActionListener(e -> this.setVisible(false));
        this.box.setSelected(true);
    }
    
    static {
        maxSize = new Dimension(572, 479);
        minSize = new Dimension(572, 374);
    }
    
    public static class GameVersionDTOComporator implements Comparator<GameVersionDTO>
    {
        private ForgeStringComparator comparator;
        
        public GameVersionDTOComporator() {
            this.comparator = new ForgeStringComparator();
        }
        
        @Override
        public int compare(final GameVersionDTO o1, final GameVersionDTO o2) {
            return this.comparator.compare(o1.getName(), o2.getName());
        }
    }
}
