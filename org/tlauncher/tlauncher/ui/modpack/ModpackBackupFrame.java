// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.BorderLayout;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import java.awt.event.ItemEvent;
import java.awt.event.ActionEvent;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import java.util.Iterator;
import javax.swing.ListModel;
import javax.swing.DefaultListModel;
import org.tlauncher.tlauncher.ui.swing.GameRadioTextButton;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;
import org.tlauncher.exceptions.ParseModPackException;
import java.util.ArrayList;
import java.io.File;
import java.util.Date;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.Container;
import javax.swing.AbstractButton;
import javax.swing.border.Border;
import org.tlauncher.tlauncher.ui.swing.renderer.ModpackComboxRenderer;
import javax.swing.plaf.ComboBoxUI;
import java.awt.Rectangle;
import java.awt.Graphics;
import org.tlauncher.tlauncher.ui.ui.ModpackComboBoxUI;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.swing.renderer.UserCategoryListRenderer;
import javax.swing.ListCellRenderer;
import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.BorderFactory;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.JLabel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ListSelectionModel;
import java.util.List;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import javax.swing.DefaultListSelectionModel;
import javax.swing.plaf.ScrollBarUI;
import org.tlauncher.tlauncher.ui.scenes.ModpackScene;
import java.awt.Component;
import javax.swing.JScrollPane;
import org.tlauncher.tlauncher.ui.ui.ModpackScrollBarUI;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.tlauncher.ui.swing.GameInstallRadioButton;
import org.tlauncher.tlauncher.ui.explorer.filters.CommonFilter;
import org.tlauncher.tlauncher.ui.explorer.filters.FilesAndExtentionFilter;
import java.io.IOException;
import org.tlauncher.util.U;
import org.tlauncher.util.FileUtil;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.managers.ModpackManager;
import java.awt.LayoutManager;
import java.awt.CardLayout;
import javax.swing.SpringLayout;
import javax.swing.JFrame;
import org.tlauncher.modpack.domain.client.share.GameType;
import net.minecraft.launcher.versions.CompleteVersion;
import javax.swing.JComboBox;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.explorer.FileChooser;
import javax.swing.ButtonGroup;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import java.awt.Dimension;
import java.text.SimpleDateFormat;

public class ModpackBackupFrame extends TemlateModpackFrame
{
    public static final SimpleDateFormat format;
    private static final String RESTORER = "RESTORER";
    private static final String BACKUP = "BACKUP";
    private static final String RECOVER_FOLDER = "tl-backups";
    private static final Dimension DEFAULT_SIZE;
    private final ModpackComboBox localModpack;
    private ButtonGroup group;
    FileChooser explorerRecovery;
    private Color listColor;
    private JPanel subEntityPanel;
    final JList<GameEntityDTO> entitiesList;
    JComboBox<CompleteVersion> modpackBox;
    final JComboBox<GameType> modpackElementType;
    private CompleteVersion selectedVersion;
    
    public ModpackBackupFrame(final JFrame parent, final ModpackComboBox localModpack) {
        super(parent, "modpack.backup.title", ModpackBackupFrame.DEFAULT_SIZE);
        this.group = new ButtonGroup();
        this.listColor = new Color(237, 249, 255);
        this.selectedVersion = null;
        this.localModpack = localModpack;
        final SpringLayout springRecoverer = new SpringLayout();
        final SpringLayout springBackup = new SpringLayout();
        final SpringLayout springPanel = new SpringLayout();
        final CardLayout cardLayout = new CardLayout();
        final JPanel panel = new JPanel(springPanel);
        final JPanel cardPanel = new JPanel(cardLayout);
        final ModpackManager manager = (ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class);
        final JPanel restoredPanel = new JPanel(springRecoverer);
        final JPanel backupModpackPanel = new JPanel(springBackup);
        backupModpackPanel.setBackground(Color.WHITE);
        this.addCenter(panel);
        try {
            FileUtil.createFolder(FileUtil.getRelative("tl-backups").toFile());
        }
        catch (IOException e2) {
            U.log(e2);
        }
        (this.explorerRecovery = (FileChooser)TLauncher.getInjector().getInstance((Class)FileChooser.class)).setCurrentDirectory(FileUtil.getRelative("tl-backups").toFile());
        this.explorerRecovery.setMultiSelectionEnabled(false);
        this.explorerRecovery.setFileFilter(new FilesAndExtentionFilter("zip, rar", new String[] { "zip", "rar" }));
        this.modpackBox = new JComboBox<CompleteVersion>();
        this.entitiesList = new JList<GameEntityDTO>();
        final GameRadioTextButton restoredBackup = new GameInstallRadioButton("modpack.backup.button.restore");
        final GameRadioTextButton backupModpack = new GameInstallRadioButton("modpack.backup.button.backup");
        final UpdaterButton doButton = new UpdaterButton(ModpackBackupFrame.BLUE_COLOR, ColorUtil.BLUE_MODPACK_BUTTON_UP, "modpack.backup.button.do");
        final UpdaterButton chooseFile = new UpdaterButton(ColorUtil.COLOR_215, "explorer.title");
        final LocalizableLabel description = new LocalizableLabel("modpack.backup.message.info");
        final LocalizableLabel informationBold = new LocalizableLabel("modpack.backup.info.label.0");
        final LocalizableButton restoreButton = new UpdaterButton(ModpackBackupFrame.BLUE_COLOR, ColorUtil.BLUE_MODPACK_BUTTON_UP, "modpack.backup.down.button.restore");
        final ModpackScrollBarUI barUI = new ModpackScrollBarUI() {
            @Override
            protected Dimension getMinimumThumbSize() {
                return new Dimension(10, 40);
            }
            
            @Override
            public Dimension getMaximumSize(final JComponent c) {
                final Dimension dim = super.getMaximumSize(c);
                dim.setSize(10.0, dim.getHeight());
                return dim;
            }
            
            @Override
            public Dimension getPreferredSize(final JComponent c) {
                final Dimension dim = super.getPreferredSize(c);
                dim.setSize(10.0, dim.getHeight());
                return dim;
            }
        };
        barUI.setGapThubm(5);
        final CardLayout cardLayout2 = new CardLayout();
        (this.subEntityPanel = new JPanel(cardLayout2)).setBackground(this.listColor);
        final JScrollPane scroller = new JScrollPane(this.entitiesList, 20, 31);
        this.subEntityPanel.add(scroller, ModpackScene.NOT_EMPTY);
        this.subEntityPanel.add(new EmptyView(GameType.MOD, "modpack.table.empty."), ModpackScene.EMPTY + GameType.MOD.toString());
        this.subEntityPanel.add(new EmptyView(GameType.RESOURCEPACK, "modpack.table.empty."), ModpackScene.EMPTY + GameType.RESOURCEPACK);
        this.subEntityPanel.add(new EmptyView(GameType.MAP, "modpack.table.empty."), ModpackScene.EMPTY + GameType.MAP);
        this.subEntityPanel.add(new EmptyView(GameType.MAP, "modpack.backup.all.elements."), "" + GameType.MODPACK + GameType.MAP);
        this.subEntityPanel.add(new EmptyView(GameType.MOD, "modpack.backup.all.elements."), "" + GameType.MODPACK + GameType.MOD);
        this.subEntityPanel.add(new EmptyView(GameType.RESOURCEPACK, "modpack.backup.all.elements."), "" + GameType.MODPACK + GameType.RESOURCEPACK);
        scroller.getVerticalScrollBar().setUI(barUI);
        scroller.getVerticalScrollBar().setBackground(this.listColor);
        this.entitiesList.setSelectionModel(new DefaultListSelectionModel() {
            private int i0 = -1;
            private int i1 = -1;
            
            @Override
            public void setSelectionInterval(final int index0, final int index1) {
                if (this.i0 == index0 && this.i1 == index1) {
                    if (this.getValueIsAdjusting()) {
                        this.setValueIsAdjusting(false);
                        this.setSelection(index0, index1);
                    }
                }
                else {
                    this.i0 = index0;
                    this.i1 = index1;
                    this.setValueIsAdjusting(false);
                    this.setSelection(index0, index1);
                }
            }
            
            private void setSelection(final int index0, final int index1) {
                final ModpackVersionDTO v = (ModpackVersionDTO)ModpackBackupFrame.this.selectedVersion.getModpack().getVersion();
                if (super.isSelectedIndex(index0)) {
                    v.getByType((GameType)ModpackBackupFrame.this.modpackElementType.getSelectedItem()).remove(ModpackBackupFrame.this.entitiesList.getModel().getElementAt(index0));
                    super.removeSelectionInterval(index0, index1);
                }
                else {
                    final GameEntityDTO en = ModpackBackupFrame.this.entitiesList.getModel().getElementAt(index0);
                    final List<GameEntityDTO> list = (List<GameEntityDTO>)v.getByType((GameType)ModpackBackupFrame.this.modpackElementType.getSelectedItem());
                    list.add(en);
                    super.addSelectionInterval(index0, index1);
                }
            }
        });
        this.entitiesList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
                final GameEntityDTO entity = (GameEntityDTO)value;
                final JLabel label = new JLabel(entity.getName());
                SwingUtil.changeFontFamily(label, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
                label.setHorizontalTextPosition(4);
                label.setBorder(BorderFactory.createEmptyBorder(0, 25, 0, 0));
                if (isSelected) {
                    label.setIcon((Icon)ImageCache.getIcon("settings-check-box-on.png"));
                    label.setIconTextGap(15);
                }
                else {
                    label.setIconTextGap(14);
                    label.setIcon((Icon)ImageCache.getIcon("settings-check-box-off.png"));
                }
                label.setPreferredSize(new Dimension(0, 30));
                label.setOpaque(false);
                return label;
            }
        });
        this.modpackElementType = new JComboBox<GameType>(new GameType[] { GameType.MOD, GameType.RESOURCEPACK, GameType.MAP });
        this.modpackBox.setRenderer(new UserCategoryListRenderer() {
            @Override
            public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
                return this.createElement(index, isSelected, ((CompleteVersion)value).getID());
            }
        });
        this.modpackElementType.setRenderer(new UserCategoryListRenderer() {
            @Override
            public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
                return this.createElement(index, isSelected, Localizable.get("modpack.button." + value.toString()));
            }
        });
        this.modpackElementType.setUI(new ModpackComboBoxUI() {
            @Override
            public void paintCurrentValue(final Graphics g, final Rectangle bounds, final boolean hasFocus) {
                this.paintBackground(g, bounds);
                this.paintText(g, bounds, Localizable.get("modpack.button." + ModpackBackupFrame.this.modpackElementType.getSelectedItem().toString()));
            }
        });
        this.modpackBox.setUI(new ModpackComboBoxUI() {
            @Override
            public void paintCurrentValue(final Graphics g, final Rectangle bounds, final boolean hasFocus) {
                this.paintBackground(g, bounds);
                this.paintText(g, bounds, ((CompleteVersion)ModpackBackupFrame.this.modpackBox.getSelectedItem()).getID());
            }
        });
        this.modpackBox.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, ModpackComboxRenderer.LINE));
        this.modpackElementType.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, ModpackComboxRenderer.LINE));
        restoredBackup.setActionCommand("RESTORER");
        backupModpack.setActionCommand("BACKUP");
        description.setHorizontalAlignment(0);
        description.setVerticalAlignment(1);
        informationBold.setHorizontalAlignment(0);
        this.group.add(restoredBackup);
        this.group.add(backupModpack);
        SwingUtil.changeFontFamily(restoredBackup, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(backupModpack, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(chooseFile, FontTL.ROBOTO_REGULAR, 12, Color.BLACK);
        SwingUtil.changeFontFamily(description, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(doButton, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        SwingUtil.changeFontFamily(restoreButton, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        SwingUtil.changeFontFamily(informationBold, FontTL.ROBOTO_BOLD, 14, Color.BLACK);
        SwingUtil.changeFontFamily(this.modpackBox, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        SwingUtil.changeFontFamily(this.modpackElementType, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        SwingUtil.changeFontFamily(this.entitiesList, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        description.setBackground(ColorUtil.COLOR_244);
        informationBold.setBackground(ColorUtil.COLOR_244);
        restoredPanel.setBackground(Color.WHITE);
        this.entitiesList.setBackground(this.listColor);
        description.setOpaque(true);
        informationBold.setOpaque(true);
        springPanel.putConstraint("West", restoredBackup, 0, "West", panel);
        springPanel.putConstraint("East", restoredBackup, 286, "West", panel);
        springPanel.putConstraint("North", restoredBackup, 0, "North", panel);
        springPanel.putConstraint("South", restoredBackup, 58, "North", panel);
        panel.add(restoredBackup);
        springPanel.putConstraint("West", backupModpack, 286, "West", panel);
        springPanel.putConstraint("East", backupModpack, 572, "West", panel);
        springPanel.putConstraint("North", backupModpack, 0, "North", panel);
        springPanel.putConstraint("South", backupModpack, 58, "North", panel);
        panel.add(backupModpack);
        springPanel.putConstraint("West", cardPanel, 0, "West", panel);
        springPanel.putConstraint("East", cardPanel, 0, "East", panel);
        springPanel.putConstraint("North", cardPanel, 58, "North", panel);
        springPanel.putConstraint("South", cardPanel, 0, "South", panel);
        panel.add(cardPanel);
        cardPanel.add("RESTORER", restoredPanel);
        cardPanel.add("BACKUP", backupModpackPanel);
        springRecoverer.putConstraint("West", chooseFile, 179, "West", restoredPanel);
        springRecoverer.putConstraint("East", chooseFile, -177, "East", restoredPanel);
        springRecoverer.putConstraint("North", chooseFile, 39, "North", restoredPanel);
        springRecoverer.putConstraint("South", chooseFile, 77, "North", restoredPanel);
        restoredPanel.add(chooseFile);
        springRecoverer.putConstraint("West", informationBold, 0, "West", restoredPanel);
        springRecoverer.putConstraint("East", informationBold, 0, "East", restoredPanel);
        springRecoverer.putConstraint("North", informationBold, 113, "North", restoredPanel);
        springRecoverer.putConstraint("South", informationBold, 148, "North", restoredPanel);
        restoredPanel.add(informationBold);
        springRecoverer.putConstraint("West", description, 0, "West", restoredPanel);
        springRecoverer.putConstraint("East", description, 0, "East", restoredPanel);
        springRecoverer.putConstraint("North", description, 145, "North", restoredPanel);
        springRecoverer.putConstraint("South", description, 276, "North", restoredPanel);
        restoredPanel.add(description);
        springRecoverer.putConstraint("West", restoreButton, 205, "West", restoredPanel);
        springRecoverer.putConstraint("East", restoreButton, 368, "West", restoredPanel);
        springRecoverer.putConstraint("North", restoreButton, -68, "South", restoredPanel);
        springRecoverer.putConstraint("South", restoreButton, -29, "South", restoredPanel);
        restoredPanel.add(restoreButton);
        springBackup.putConstraint("West", this.modpackBox, 129, "West", backupModpackPanel);
        springBackup.putConstraint("East", this.modpackBox, -127, "East", backupModpackPanel);
        springBackup.putConstraint("North", this.modpackBox, 21, "North", backupModpackPanel);
        springBackup.putConstraint("South", this.modpackBox, 61, "North", backupModpackPanel);
        backupModpackPanel.add(this.modpackBox);
        springBackup.putConstraint("West", this.modpackElementType, 129, "West", backupModpackPanel);
        springBackup.putConstraint("East", this.modpackElementType, -127, "East", backupModpackPanel);
        springBackup.putConstraint("North", this.modpackElementType, 81, "North", backupModpackPanel);
        springBackup.putConstraint("South", this.modpackElementType, 121, "North", backupModpackPanel);
        backupModpackPanel.add(this.modpackElementType);
        springBackup.putConstraint("West", this.subEntityPanel, 129, "West", backupModpackPanel);
        springBackup.putConstraint("East", this.subEntityPanel, -127, "East", backupModpackPanel);
        springBackup.putConstraint("North", this.subEntityPanel, 121, "North", backupModpackPanel);
        springBackup.putConstraint("South", this.subEntityPanel, 271, "North", backupModpackPanel);
        backupModpackPanel.add(this.subEntityPanel);
        springBackup.putConstraint("West", doButton, 175, "West", backupModpackPanel);
        springBackup.putConstraint("East", doButton, 398, "West", backupModpackPanel);
        springBackup.putConstraint("North", doButton, -62, "South", backupModpackPanel);
        springBackup.putConstraint("South", doButton, -20, "South", backupModpackPanel);
        backupModpackPanel.add(doButton);
        restoredBackup.addActionListener(e -> cardLayout.show(cardPanel, e.getActionCommand()));
        final AbstractButton abstractButton;
        CompleteVersion defaultVersion;
        int i;
        final CardLayout cardLayout3;
        final Container container;
        backupModpack.addActionListener(e -> {
            if (localModpack.getItemCount() < 2) {
                this.setVisible(false);
                Alert.showLocMessage("modpack.backup.init");
                this.setVisible(true);
                abstractButton.setSelected(true);
                return;
            }
            else {
                defaultVersion = new CompleteVersion();
                defaultVersion.setID(Localizable.get("modpack.backup.modpack.box"));
                this.modpackBox.removeAllItems();
                this.modpackBox.addItem(defaultVersion);
                for (i = 1; i < localModpack.getItemCount(); ++i) {
                    this.modpackBox.addItem(localModpack.getItemAt(i));
                }
                this.modpackBox.setSelectedIndex(0);
                cardLayout3.show(container, e.getActionCommand());
                return;
            }
        });
        this.modpackBox.addItemListener(e -> {
            if (1 == e.getStateChange()) {
                if (this.modpackBox.getSelectedIndex() != 0) {
                    this.selectedVersion = ((CompleteVersion)this.modpackBox.getSelectedItem()).fullCopy(new CompleteVersion());
                }
                else {
                    this.selectedVersion = null;
                }
                this.prepareEntityList();
            }
            return;
        });
        this.modpackElementType.addItemListener(e -> {
            if (1 == e.getStateChange()) {
                this.prepareEntityList();
            }
            return;
        });
        final FileChooser f;
        String s;
        final String name;
        final String name2;
        final FilesAndExtentionFilter fileFilter;
        final Object o;
        final int res;
        final UpdaterButton updaterButton;
        ArrayList<CompleteVersion> list;
        int j;
        final ModpackManager modpackManager;
        doButton.addActionListener(e -> {
            f = (FileChooser)TLauncher.getInjector().getInstance((Class)FileChooser.class);
            if (this.selectedVersion == null) {
                s = "modpacks-" + ModpackBackupFrame.format.format(new Date());
            }
            else {
                s = this.selectedVersion.getID() + " " + ModpackBackupFrame.format.format(new Date());
            }
            name = s;
            name2 = name + ".zip";
            new FilesAndExtentionFilter("zip format", new String[] { "zip" });
            ((FileChooser)o).setFileFilter(fileFilter);
            f.setDialogTitle(Localizable.get("console.save.popup"));
            f.setSelectedFile(new File(FileUtil.getRelative("tl-backups").toFile(), name2));
            f.setMultiSelectionEnabled(false);
            this.setAlwaysOnTop(false);
            res = f.showSaveDialog(this);
            if (res != 0) {
                return;
            }
            else {
                this.setAlwaysOnTop(true);
                updaterButton.setText("modpack.install.process");
                updaterButton.setEnabled(false);
                list = new ArrayList<CompleteVersion>();
                if (this.selectedVersion == null) {
                    for (j = 1; j < this.modpackBox.getItemCount(); ++j) {
                        list.add(this.modpackBox.getItemAt(j).fullCopy(new CompleteVersion()));
                    }
                }
                else {
                    list.add(this.selectedVersion);
                }
                modpackManager.backupModPack(list, f.getSelectedFile(), new HandleListener() {
                    final /* synthetic */ UpdaterButton val$doButton;
                    
                    @Override
                    public void processError(final Exception e) {
                        this.val$doButton.setEnabled(true);
                        this.val$doButton.setText("modpack.backup.button.do");
                        ModpackBackupFrame.this.showWarning("modpack.backup.files.error");
                    }
                    
                    @Override
                    public void installedSuccess(final List<String> modpackNames) {
                    }
                    
                    @Override
                    public void operationSuccess() {
                        this.val$doButton.setEnabled(true);
                        this.val$doButton.setText("modpack.backup.button.do");
                        ModpackBackupFrame.this.setVisible(false);
                        Alert.showLocMessageWithoutTitle("modpack.backup.files.do");
                        ModpackBackupFrame.this.setVisible(true);
                    }
                });
                return;
            }
        });
        final LocalizableButton localizableButton;
        File f2;
        final ModpackManager modpackManager2;
        List<String> list2;
        final LocalizableLabel localizableLabel;
        chooseFile.addActionListener(e -> {
            this.setAlwaysOnTop(false);
            if (this.explorerRecovery.showDialog(this) == 0) {
                localizableButton.setText("explorer.backup.file.chosen");
                f2 = this.explorerRecovery.getSelectedFile();
                try {
                    list2 = modpackManager2.analizeArchiver(f2);
                    localizableLabel.setText(this.buildDescription(list2));
                }
                catch (ParseModPackException e3) {
                    this.showWarning("modpack.install.files.error");
                    U.log(e3);
                }
            }
            this.setAlwaysOnTop(true);
            return;
        });
        final File f3;
        final LocalizableButton localizableButton2;
        final ModpackManager modpackManager3;
        final LocalizableLabel localizableLabel2;
        restoreButton.addActionListener(e -> {
            f3 = this.explorerRecovery.getSelectedFile();
            if (f3 == null || f3.isDirectory()) {
                this.showWarning("explorer.error.choose.file");
                return;
            }
            else {
                try {
                    localizableButton2.setEnabled(false);
                    localizableButton2.setText("modpack.install.process");
                    modpackManager3.installModPack(f3, new HandleListener() {
                        final /* synthetic */ LocalizableButton val$restoreButton;
                        final /* synthetic */ LocalizableLabel val$description;
                        
                        @Override
                        public void processError(final Exception e) {
                            this.val$restoreButton.setEnabled(true);
                            ModpackBackupFrame.this.showWarning("modpack.install.files.error");
                        }
                        
                        @Override
                        public void operationSuccess() {
                        }
                        
                        @Override
                        public void installedSuccess(final List<String> modpackNames) {
                            this.val$restoreButton.setText("modpack.backup.down.button.restore");
                            this.val$restoreButton.setEnabled(true);
                            this.val$description.setText(ModpackBackupFrame.this.buildDescription(modpackNames));
                            ModpackBackupFrame.this.setVisible(false);
                            Alert.showLocMessage("modpack.install.files.installed");
                        }
                    });
                }
                catch (Exception e4) {
                    U.log(e4);
                    this.showWarning("modpack.install.files.error");
                }
                return;
            }
        });
        restoredBackup.setSelected(true);
        cardLayout.show(cardPanel, "RESTORER");
    }
    
    private void showWarning(final String value) {
        this.setVisible(false);
        Alert.showLocWarning(value);
        this.setVisible(true);
    }
    
    private void prepareEntityList() {
        final DefaultListModel<GameEntityDTO> listModel = new DefaultListModel<GameEntityDTO>();
        this.entitiesList.setModel(listModel);
        if (this.selectedVersion != null) {
            final ModpackVersionDTO version = (ModpackVersionDTO)this.selectedVersion.getModpack().getVersion();
            for (final GameEntityDTO en : version.getByType((GameType)this.modpackElementType.getSelectedItem())) {
                listModel.addElement(en);
            }
            this.entitiesList.setEnabled(true);
            if (this.entitiesList.getModel().getSize() == 0) {
                ((CardLayout)this.subEntityPanel.getLayout()).show(this.subEntityPanel, "" + ModpackScene.EMPTY + this.modpackElementType.getSelectedItem());
            }
            else {
                ((CardLayout)this.subEntityPanel.getLayout()).show(this.subEntityPanel, ModpackScene.NOT_EMPTY);
            }
        }
        else if (this.containsAnyElement((GameType)this.modpackElementType.getModel().getSelectedItem())) {
            ((CardLayout)this.subEntityPanel.getLayout()).show(this.subEntityPanel, "" + GameType.MODPACK + this.modpackElementType.getSelectedItem());
        }
        else {
            ((CardLayout)this.subEntityPanel.getLayout()).show(this.subEntityPanel, ModpackScene.EMPTY + this.modpackElementType.getSelectedItem());
        }
        final int[] array = new int[listModel.getSize()];
        for (int i = 0; i < listModel.getSize(); ++i) {
            array[i] = i;
        }
        this.entitiesList.setSelectedIndices(array);
    }
    
    private String buildDescription(final List<String> list) {
        final StringBuilder builder = new StringBuilder();
        builder.append("<html>");
        builder.append("<p style='text-align: center; margin-top:5'>");
        for (final String aList : list) {
            builder.append(aList).append("<br>");
        }
        builder.append("</p></html>");
        return builder.toString();
    }
    
    private boolean containsAnyElement(final GameType type) {
        for (final ModpackDTO modpackDTO : this.localModpack.getModpacks()) {
            if (((ModpackVersionDTO)modpackDTO.getVersion()).getByType(type).size() > 0) {
                return true;
            }
        }
        return false;
    }
    
    static {
        format = new SimpleDateFormat("dd-MM-YYYY-HH_mm_ss");
        DEFAULT_SIZE = new Dimension(572, 470);
    }
    
    private class EmptyView extends ExtendedPanel
    {
        public EmptyView(final GameType gameType, final String nameLoc) {
            this.setLayout(new BorderLayout());
            final JLabel jLabel = new LocalizableLabel(nameLoc + gameType);
            jLabel.setHorizontalAlignment(0);
            jLabel.setAlignmentY(0.0f);
            SwingUtil.changeFontFamily(jLabel, FontTL.ROBOTO_BOLD, 14);
            jLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 6, 0));
            this.add(jLabel, "Center");
            this.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, ColorUtil.COLOR_149));
        }
    }
    
    public interface HandleListener
    {
        void operationSuccess();
        
        void processError(final Exception p0);
        
        void installedSuccess(final List<String> p0);
    }
}
