// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import org.tlauncher.tlauncher.ui.swing.ImageButton;
import javax.swing.JComponent;
import java.awt.Graphics;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import java.util.concurrent.CompletableFuture;
import org.tlauncher.util.U;
import javax.swing.Icon;
import java.awt.Image;
import java.net.URL;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import javax.swing.ImageIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.event.MouseListener;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.BorderFactory;
import javax.swing.JLayeredPane;
import org.tlauncher.util.OS;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;

public class BigPictureObserver extends TemlateModpackFrame
{
    private Color backgroundPanel;
    private static final int heightPanel = 586;
    private static final int width = 1050;
    int currentPicture;
    final JLabel picture;
    private Integer[] array;
    
    public BigPictureObserver(final JFrame parent, final String title, final Integer[] array, final int i) {
        super(parent, title, new Dimension(1050, 586), OS.is(OS.LINUX));
        this.backgroundPanel = new Color(188, 188, 188);
        this.picture = new JLabel();
        this.currentPicture = i;
        this.array = array;
        final JLayeredPane layeredPane = new JLayeredPane();
        final Button previousPicture = new Button("big-picture-previous-arrow.png", "previous-arrow-under.png");
        final Button nextPicture = new Button("big-picture-next-arrow.png", "next-arrow-under.png");
        previousPicture.setPreferredSize(new Dimension(46, 155));
        nextPicture.setPreferredSize(new Dimension(46, 155));
        layeredPane.setBackground(this.backgroundPanel);
        this.picture.setBorder(BorderFactory.createLineBorder(Color.white, 5));
        final JLabel close = new JLabel(ImageCache.getNativeIcon("picture-exit.png"));
        close.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                close.setIcon(ImageCache.getNativeIcon("picture-exit-on.png"));
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                close.setIcon(ImageCache.getNativeIcon("picture-exit.png"));
            }
            
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    BigPictureObserver.this.setVisible(false);
                }
            }
        });
        layeredPane.add(previousPicture, 1);
        layeredPane.add(nextPicture, 1);
        layeredPane.add(this.picture, 1);
        layeredPane.add(close, 0);
        layeredPane.setSize(new Dimension(1050, 586));
        previousPicture.setBounds(10, 266, 19, 33);
        nextPicture.setBounds(1021, 266, 19, 33);
        this.putOnPanel(close);
        this.add(layeredPane);
        nextPicture.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if (BigPictureObserver.this.currentPicture < array.length - 1) {
                    final BigPictureObserver this$0 = BigPictureObserver.this;
                    ++this$0.currentPicture;
                    BigPictureObserver.this.putOnPanel(close);
                }
            }
        });
        previousPicture.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if (BigPictureObserver.this.currentPicture > 0) {
                    final BigPictureObserver this$0 = BigPictureObserver.this;
                    --this$0.currentPicture;
                    BigPictureObserver.this.putOnPanel(close);
                }
            }
        });
    }
    
    private void putOnPanel(final JLabel close) {
        JLabel picture;
        final ImageIcon icon;
        final Dimension pictureSize;
        CompletableFuture.runAsync(() -> {
            try {
                picture = this.picture;
                new ImageIcon(ImageCache.loadImage(new URL(ModpackUtil.getPictureURL(this.array[this.currentPicture], "_max_full"))));
                picture.setIcon(icon);
            }
            catch (Exception e) {
                U.log(e);
            }
        }).thenAccept(e -> {
            pictureSize = this.picture.getPreferredSize();
            this.picture.setBounds((1050 - pictureSize.width) / 2, (586 - pictureSize.height) / 2, pictureSize.width, pictureSize.height);
            close.setBounds((1050 - pictureSize.width) / 2 + pictureSize.width - 21, (586 - pictureSize.height) / 2 - 21, 42, 42);
        });
    }
    
    private class Button extends ImageUdaterButton
    {
        public Button(final String s, final String s1) {
            super(s);
            this.setOpaque(false);
            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(final MouseEvent e) {
                    ImageButton.this.setImage(ImageButton.loadImage(s1));
                }
                
                @Override
                public void mouseExited(final MouseEvent e) {
                    ImageButton.this.setImage(ImageButton.loadImage(s));
                }
            });
        }
        
        @Override
        public void paintComponent(final Graphics g) {
            this.paintPicture(g, this, this.getBounds());
        }
    }
}
