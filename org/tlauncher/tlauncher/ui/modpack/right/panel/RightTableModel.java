// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.right.panel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.tlauncher.modpack.domain.client.GameEntityDTO;

public class RightTableModel<T extends GameEntityDTO> extends AbstractTableModel
{
    private List<T> data;
    
    public RightTableModel(final List<T> data) {
        this.data = data;
    }
    
    @Override
    public int getRowCount() {
        return this.data.size();
    }
    
    @Override
    public int getColumnCount() {
        return 1;
    }
    
    @Override
    public T getValueAt(final int rowIndex, final int columnIndex) {
        return this.data.get(rowIndex);
    }
    
    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        return true;
    }
    
    @Override
    public Class<?> getColumnClass(final int columnIndex) {
        return GameEntityDTO.class;
    }
}
