// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.right.panel;

import javax.swing.AbstractCellEditor;
import java.awt.event.MouseListener;
import java.awt.Color;
import java.awt.Rectangle;
import org.tlauncher.util.SwingUtil;
import java.awt.Graphics;
import javax.swing.JLabel;
import org.tlauncher.tlauncher.ui.loc.modpack.GameRightButton;
import org.tlauncher.tlauncher.ui.listener.mods.UpdateGameListener;
import org.tlauncher.tlauncher.ui.scenes.CompleteSubEntityScene;
import net.minecraft.launcher.versions.CompleteVersion;
import java.awt.Container;
import javax.swing.table.TableRowSorter;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import org.tlauncher.tlauncher.ui.modpack.filter.CategoryFilter;
import org.tlauncher.tlauncher.ui.modpack.filter.WordFilter;
import org.tlauncher.tlauncher.ui.modpack.filter.NameFilter;
import org.tlauncher.tlauncher.ui.modpack.filter.ModpackSceneFilter;
import org.tlauncher.tlauncher.ui.modpack.filter.Filter;
import javax.swing.RowFilter;
import java.util.concurrent.atomic.AtomicBoolean;
import java.awt.CardLayout;
import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Iterator;
import java.awt.Component;
import java.util.Arrays;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import javax.swing.table.TableModel;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.AWTEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableCellEditor;
import java.awt.Dimension;
import org.tlauncher.util.ColorUtil;
import java.util.ArrayList;
import java.util.List;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.share.Category;
import javax.swing.JComboBox;
import org.tlauncher.tlauncher.ui.scenes.ModpackScene;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityListener;
import javax.swing.JTable;

public class GameEntityRightPanel extends JTable implements GameEntityListener
{
    private final ModpackComboBox localmodpacks;
    private final ModpackScene.SearchPanel search;
    private final JComboBox<Category> categoriesBox;
    private final GameType type;
    private static final int HEIGHT_RIGHT_ELEMENT = 159;
    private List<Long> changeableElements;
    
    public GameEntityRightPanel(final ModpackComboBox localmodpacks, final ModpackScene.SearchPanel search, final JComboBox<Category> categoriesBox, final GameType type) {
        this.changeableElements = new ArrayList<Long>();
        this.search = search;
        this.localmodpacks = localmodpacks;
        this.categoriesBox = categoriesBox;
        this.type = type;
        this.setBackground(ColorUtil.COLOR_233);
        this.setRowHeight(159);
        this.setColumnSelectionAllowed(true);
        this.setRowSelectionAllowed(true);
        this.setShowGrid(false);
        this.setIntercellSpacing(new Dimension(0, 0));
        this.setDefaultEditor(GameRightElement.class, new RightRenderer());
        this.setDefaultRenderer(GameRightElement.class, new RightRenderer());
        final MouseAdapter mouse = new MouseAdapter() {
            int current = -1;
            
            @Override
            public void mouseMoved(final MouseEvent e) {
                this.check(e);
            }
            
            @Override
            public void mouseWheelMoved(final MouseWheelEvent e) {
                GameEntityRightPanel.this.getParent().dispatchEvent(e);
                this.check(e);
            }
            
            private void check(final MouseEvent e) {
                final int i = GameEntityRightPanel.this.rowAtPoint(e.getPoint());
                if (i != -1 && i != this.current) {
                    GameEntityRightPanel.this.editCellAt(i, 0);
                    this.current = i;
                }
            }
        };
        this.addMouseWheelListener(mouse);
        this.addMouseMotionListener(mouse);
    }
    
    public void addElements(final List<? extends GameEntityDTO> list, final GameType type) {
        final RightTableModel model = new RightTableModel((List<T>)list);
        this.setModel(model);
    }
    
    @Override
    public void processingStarted(final GameEntityDTO e, final VersionDTO version) {
        this.changeableElements.add(e.getId());
        for (final Component c : Arrays.asList(this.getComponents())) {
            if (c instanceof GameRightElement) {
                final GameRightElement el = (GameRightElement)c;
                if (!el.getEntity().getId().equals(e.getId())) {
                    continue;
                }
                el.processingInstall();
            }
        }
    }
    
    @Override
    public void installEntity(final GameEntityDTO e, final GameType type) {
        this.changeableElements.remove(e.getId());
        this.filterRightPanel(TLauncher.getInstance().getFrame().mp.modpackScene.getCurrent());
        this.updateRow();
    }
    
    @Override
    public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
        this.changeableElements.remove(e.getId());
        final GameRightElement elem = this.find(e);
        if (elem != null) {
            elem.modpackActButton.reset();
        }
    }
    
    private GameRightElement find(final GameEntityDTO e) {
        for (final Component c : Arrays.asList(this.getComponents())) {
            if (c instanceof GameRightElement) {
                final GameRightElement el = (GameRightElement)c;
                if (el.getEntity().getId().equals(e.getId())) {
                    return el;
                }
                continue;
            }
        }
        return null;
    }
    
    @Override
    public void populateStatus(final GameEntityDTO e, final GameType type, final boolean state) {
        final GameRightElement elem = this.find(e);
        if (elem != null) {
            elem.getStatusStarButton().setStatus(state);
        }
    }
    
    @Override
    public void removeEntity(final GameEntityDTO e) {
        this.changeableElements.remove(e.getId());
        this.filterRightPanel(TLauncher.getInstance().getFrame().mp.modpackScene.getCurrent());
        this.updateRow();
    }
    
    void updateRow() {
        final int row = this.getEditingRow();
        if (row >= 0) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    GameEntityRightPanel.this.editCellAt(row, 0);
                }
            });
        }
    }
    
    public void filterRightPanel(final GameType current) {
        final Container container = this.getParent();
        final CardLayout cardLayout = (CardLayout)container.getLayout();
        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        final RowFilter<RightTableModel, Integer> filter = new RowFilter<RightTableModel, Integer>() {
            @Override
            public boolean include(final Entry<? extends RightTableModel, ? extends Integer> entry) {
                final GameEntityDTO dto = ((RightTableModel)entry.getModel()).getValueAt((int)entry.getIdentifier(), 0);
                final BaseModpackFilter<GameEntityDTO> gameEntityFilterI = new ModpackSceneFilter(GameEntityRightPanel.this.localmodpacks, current, (Filter<GameEntityDTO>[])new Filter[0]);
                if (GameEntityRightPanel.this.search.isNotEmpty()) {
                    gameEntityFilterI.addFilter(new NameFilter(GameEntityRightPanel.this.search.getSearchLine()));
                    gameEntityFilterI.addFilter(new WordFilter(GameEntityRightPanel.this.search.getSearchLine()));
                }
                gameEntityFilterI.addFilter(new CategoryFilter((Category)GameEntityRightPanel.this.categoriesBox.getSelectedItem()));
                if (gameEntityFilterI.isProper(dto)) {
                    if (!atomicBoolean.get()) {
                        atomicBoolean.set(true);
                    }
                    return true;
                }
                return false;
            }
        };
        final TableRowSorter sorter = (TableRowSorter)this.getRowSorter();
        sorter.setRowFilter(filter);
        sorter.sort();
        if (atomicBoolean.get()) {
            cardLayout.show(container, ModpackScene.NOT_EMPTY);
        }
        else {
            cardLayout.show(container, ModpackScene.EMPTY);
        }
    }
    
    @Override
    public void installEntity(final CompleteVersion e) {
    }
    
    @Override
    public void activationStarted(final GameEntityDTO e) {
    }
    
    @Override
    public void activation(final GameEntityDTO e) {
    }
    
    @Override
    public void activationError(final GameEntityDTO e, final Throwable t) {
    }
    
    @Override
    public void updateVersion(final CompleteVersion v, final CompleteVersion newVersion) {
    }
    
    @Override
    public void removeCompleteVersion(final CompleteVersion e) {
    }
    
    @Override
    public Class<?> getColumnClass(final int column) {
        return GameRightElement.class;
    }
    
    public class GameRightElement extends CompleteSubEntityScene.DescriptionGamePanel implements UpdateGameListener
    {
        private GameEntityDTO entity;
        private int row;
        private GameRightButton modpackActButton;
        
        public GameEntityDTO getEntity() {
            return this.entity;
        }
        
        public GameRightElement(final GameEntityDTO entity, final GameType type, final int row) {
            super(entity, type);
            this.row = row;
            this.description.setVisible(true);
            this.entity = entity;
            this.modpackActButton = new GameRightButton(entity, type, GameEntityRightPanel.this.localmodpacks) {
                @Override
                public void updateRow() {
                    GameEntityRightPanel.this.repaint();
                }
            };
            final JLabel shadow = new JLabel() {
                @Override
                protected void paintComponent(final Graphics g) {
                    final Rectangle rec = this.getBounds();
                    SwingUtil.paintShadowLine(rec, g, this.getParent().getBackground().getRed() - 14, 14);
                }
            };
            shadow.setBackground(Color.green);
            this.descriptionLayout.putConstraint("West", shadow, 0, "West", this);
            this.descriptionLayout.putConstraint("East", shadow, 0, "East", this);
            this.descriptionLayout.putConstraint("North", shadow, 0, "North", this);
            this.descriptionLayout.putConstraint("South", shadow, 14, "North", this);
            this.add(shadow);
            this.modpackActButton.initButton();
            this.imagePanel.addMoapckActButton(this.modpackActButton);
            this.descriptionLayout.putConstraint("West", this.imagePanel, 27, "West", this);
            this.descriptionLayout.putConstraint("East", this.imagePanel, 138, "West", this);
            final MouseBackgroundListener adapter = new MouseBackgroundListener(entity, type);
            this.addMouseListener(adapter);
            for (final Component comp : this.getComponents()) {
                comp.removeMouseListener(adapter);
            }
            this.description.addMouseListener(adapter);
        }
        
        @Override
        public void processingActivation() {
        }
        
        @Override
        public void processingInstall() {
            this.modpackActButton.setTypeButton("PROCESSING");
            ((RightTableModel)GameEntityRightPanel.this.getModel()).fireTableCellUpdated(this.row, 0);
        }
        
        @Override
        public void initGameEntity() {
        }
    }
    
    private class RightRenderer extends AbstractCellEditor implements TableCellRenderer, TableCellEditor
    {
        public RightRenderer() {
        }
        
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
            final GameRightElement el = new GameRightElement((GameEntityDTO)value, GameEntityRightPanel.this.type, row);
            if (GameEntityRightPanel.this.changeableElements.contains(((GameEntityDTO)value).getId())) {
                el.modpackActButton.setTypeButton("PROCESSING");
            }
            el.setBackground(ColorUtil.COLOR_247);
            return el;
        }
        
        @Override
        public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected, final int row, final int column) {
            final GameRightElement el = new GameRightElement((GameEntityDTO)value, GameEntityRightPanel.this.type, row);
            if (GameEntityRightPanel.this.changeableElements.contains(((GameEntityDTO)value).getId())) {
                el.modpackActButton.setTypeButton("PROCESSING");
            }
            el.setBackground(Color.WHITE);
            return el;
        }
        
        @Override
        public Object getCellEditorValue() {
            return null;
        }
    }
}
