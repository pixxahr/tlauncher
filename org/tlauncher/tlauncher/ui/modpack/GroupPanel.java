// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.Component;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;

public class GroupPanel extends ShadowPanel
{
    private ButtonGroup group;
    
    public GroupPanel(final int colorStarted) {
        super(colorStarted);
        this.group = new ButtonGroup();
    }
    
    public void addInGroup(final AbstractButton button) {
        this.group.add(button);
        this.add(button);
    }
}
