// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.event.ItemEvent;
import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;
import javax.swing.JButton;
import org.tlauncher.tlauncher.ui.swing.GameRadioTextButton;
import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.util.U;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.Component;
import java.awt.Color;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.AbstractButton;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.tlauncher.ui.swing.GameInstallRadioButton;
import org.tlauncher.tlauncher.ui.explorer.filters.CommonFilter;
import org.tlauncher.tlauncher.ui.explorer.filters.FilesAndExtentionFilter;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.explorer.FileChooser;
import javax.swing.JComponent;
import javax.swing.ButtonGroup;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import net.minecraft.launcher.versions.CompleteVersion;
import javax.swing.JFrame;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.io.File;
import java.awt.Dimension;
import org.tlauncher.modpack.domain.client.share.GameType;

public class HandleInstallModpackElementFrame extends TemlateModpackFrame
{
    private GameType type;
    private static final Dimension DEFAULT_SIZE;
    private File[] files;
    private LocalizableLabel installToModpack;
    
    public HandleInstallModpackElementFrame(final JFrame parent, final CompleteVersion version) {
        super(parent, "modpack.install.handle.title", HandleInstallModpackElementFrame.DEFAULT_SIZE);
        this.type = GameType.MOD;
        final SpringLayout spring = new SpringLayout();
        final JPanel panel = new JPanel(spring);
        final ButtonGroup group = new ButtonGroup();
        this.addCenter(panel);
        final FileChooser chooser = (FileChooser)TLauncher.getInjector().getInstance((Class)FileChooser.class);
        chooser.setMultiSelectionEnabled(true);
        chooser.setFileFilter(new FilesAndExtentionFilter("zip,rar,jar", new String[] { "zip", "rar", "jar" }));
        final GameRadioTextButton mods = new GameInstallRadioButton("modpack.button.mod");
        final GameRadioTextButton texturePacks = new GameInstallRadioButton("modpack.button.resourcepack");
        final GameRadioTextButton maps = new GameInstallRadioButton("modpack.button.map");
        final UpdaterButton chooseFiles = new UpdaterButton(ColorUtil.COLOR_215, "modpack.explorer.files");
        this.installToModpack = new LocalizableLabel();
        final LocalizableLabel warningLabel = new LocalizableLabel("modpack.install.handle.warning");
        final JButton install = new UpdaterButton(HandleInstallModpackElementFrame.BLUE_COLOR, HandleInstallModpackElementFrame.BLUE_COLOR_UNDER, "loginform.enter.install");
        this.installToModpack.setHorizontalAlignment(0);
        warningLabel.setHorizontalAlignment(0);
        group.add(mods);
        group.add(texturePacks);
        group.add(maps);
        SwingUtil.changeFontFamily(mods, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(texturePacks, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(maps, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(chooseFiles, FontTL.ROBOTO_MEDIUM, 12, ColorUtil.get(96));
        SwingUtil.changeFontFamily(this.installToModpack, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(warningLabel, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(install, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        warningLabel.setBackground(ColorUtil.COLOR_244);
        panel.setBackground(Color.WHITE);
        warningLabel.setOpaque(true);
        spring.putConstraint("West", mods, 0, "West", panel);
        spring.putConstraint("East", mods, 190, "West", panel);
        spring.putConstraint("North", mods, 0, "North", panel);
        spring.putConstraint("South", mods, 58, "North", panel);
        panel.add(mods);
        spring.putConstraint("West", texturePacks, 191, "West", panel);
        spring.putConstraint("East", texturePacks, 381, "West", panel);
        spring.putConstraint("North", texturePacks, 0, "North", panel);
        spring.putConstraint("South", texturePacks, 58, "North", panel);
        panel.add(texturePacks);
        spring.putConstraint("West", maps, 382, "West", panel);
        spring.putConstraint("East", maps, 0, "East", panel);
        spring.putConstraint("North", maps, 0, "North", panel);
        spring.putConstraint("South", maps, 58, "North", panel);
        panel.add(maps);
        spring.putConstraint("West", chooseFiles, 179, "West", panel);
        spring.putConstraint("East", chooseFiles, -177, "East", panel);
        spring.putConstraint("North", chooseFiles, 97, "North", panel);
        spring.putConstraint("South", chooseFiles, 135, "North", panel);
        panel.add(chooseFiles);
        spring.putConstraint("West", this.installToModpack, 29, "West", panel);
        spring.putConstraint("East", this.installToModpack, -27, "East", panel);
        spring.putConstraint("North", this.installToModpack, 156, "North", panel);
        spring.putConstraint("South", this.installToModpack, 182, "North", panel);
        panel.add(this.installToModpack);
        spring.putConstraint("West", warningLabel, 0, "West", panel);
        spring.putConstraint("East", warningLabel, 0, "East", panel);
        spring.putConstraint("North", warningLabel, 208, "North", panel);
        spring.putConstraint("South", warningLabel, 326, "North", panel);
        panel.add(warningLabel);
        spring.putConstraint("West", install, 205, "West", panel);
        spring.putConstraint("East", install, 368, "West", panel);
        spring.putConstraint("North", install, -68, "South", panel);
        spring.putConstraint("South", install, -29, "South", panel);
        panel.add(install);
        final LocalizableButton localizableButton;
        mods.addItemListener(e -> {
            if (1 == e.getStateChange()) {
                localizableButton.setText(Localizable.get("modpack.explorer.files"));
                this.files = null;
                this.type = GameType.MOD;
                this.updateInfoLabel(version);
            }
            return;
        });
        final LocalizableButton localizableButton2;
        maps.addItemListener(e -> {
            if (1 == e.getStateChange()) {
                localizableButton2.setText(Localizable.get("modpack.explorer.files"));
                this.files = null;
                this.type = GameType.MAP;
                this.updateInfoLabel(version);
            }
            return;
        });
        final LocalizableButton localizableButton3;
        texturePacks.addItemListener(e -> {
            if (1 == e.getStateChange()) {
                localizableButton3.setText(Localizable.get("modpack.explorer.files"));
                this.files = null;
                this.type = GameType.RESOURCEPACK;
                this.updateInfoLabel(version);
            }
            return;
        });
        final LocalizableButton localizableButton4;
        final FileChooser fileChooser;
        int result;
        chooseFiles.addActionListener(e -> {
            localizableButton4.setBorder(BorderFactory.createEmptyBorder());
            try {
                result = fileChooser.showDialog(this, Localizable.get("modpack.explorer.files"));
                if (result == 0) {
                    this.files = fileChooser.getSelectedFiles();
                    localizableButton4.setText(Localizable.get("explorer.backup.file.chosen"));
                }
            }
            catch (NullPointerException ex) {
                U.log(ex);
            }
            return;
        });
        final JComponent component;
        install.addActionListener(e -> {
            if (this.files == null) {
                component.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
                return;
            }
            else {
                AsyncThread.execute(() -> ((ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class)).installHandleEntity(this.files, version, this.type, new HandleListener() {
                    @Override
                    public void processError(final Exception e1) {
                        HandleInstallModpackElementFrame.this.setVisible(false);
                        Alert.showLocError("modpack.install.files.error");
                        HandleInstallModpackElementFrame.this.setVisible(true);
                    }
                    
                    @Override
                    public void installedSuccess() {
                        HandleInstallModpackElementFrame.this.setVisible(false);
                        Alert.showLocMessage("modpack.install.files.installed");
                        HandleInstallModpackElementFrame.this.setVisible(true);
                    }
                }));
                return;
            }
        });
        mods.setSelected(true);
    }
    
    private void updateInfoLabel(final CompleteVersion version) {
        this.installToModpack.setText(Localizable.get("modpack.install.handle." + this.type.toString().toLowerCase()).replace("modpack.name", version.getModpack().getName()));
    }
    
    static {
        DEFAULT_SIZE = new Dimension(572, 470);
    }
    
    public interface HandleListener
    {
        void installedSuccess();
        
        void processError(final Exception p0);
    }
}
