// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.Rectangle;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class ShadowPanel extends ExtendedPanel
{
    private int colorStarted;
    
    public ShadowPanel(final int colorStarted) {
        this.colorStarted = colorStarted;
    }
    
    @Override
    protected void paintComponent(final Graphics g0) {
        super.paintComponent(g0);
        final Rectangle rec = this.getVisibleRect();
        int y = rec.y;
        int i = this.colorStarted;
        final Graphics2D g = (Graphics2D)g0;
        while (y < rec.height + rec.y) {
            g.setColor(new Color(i, i, i));
            if (i != 255) {
                ++i;
            }
            g.drawLine(rec.x, y, rec.x + rec.width, y);
            ++y;
        }
    }
}
