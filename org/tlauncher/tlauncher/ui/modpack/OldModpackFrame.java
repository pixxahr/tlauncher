// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.event.ActionEvent;
import javax.swing.text.JTextComponent;
import javax.swing.JTextArea;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.util.Map;
import net.minecraft.launcher.Http;
import java.util.HashMap;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.util.TlauncherUtil;
import java.awt.Component;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.BorderFactory;
import org.tlauncher.util.ColorUtil;
import javax.swing.JTextField;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import org.tlauncher.tlauncher.ui.swing.TextWrapperLabel;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.JComponent;
import java.awt.Color;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import java.awt.Dimension;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import javax.swing.JFrame;

public class OldModpackFrame extends TemlateModpackFrame
{
    public OldModpackFrame(final JFrame parent, final GameEntityDTO entity, final GameType type) {
        super(parent, "modpack.request.update.title", new Dimension(500, 320));
        final SpringLayout spring = new SpringLayout();
        final JPanel panel = new JPanel(spring);
        panel.setBackground(Color.WHITE);
        this.addCenter(panel);
        final JTextArea question = new TextWrapperLabel(Localizable.get("modpack.element.old.question"));
        final UpdaterButton send = new UpdaterButton(OldModpackFrame.BLUE_COLOR, OldModpackFrame.BLUE_COLOR_UNDER, "log.form.send");
        final JTextField link = new JTextField();
        final CompoundBorder border = new CompoundBorder(BorderFactory.createLineBorder(ColorUtil.COLOR_233, 1), BorderFactory.createEmptyBorder(0, 21, 0, 0));
        final CompoundBorder redBorder = new CompoundBorder(BorderFactory.createLineBorder(Color.RED, 1), BorderFactory.createEmptyBorder(0, 21, 0, 0));
        link.setBorder(border);
        SwingUtil.changeFontFamily(link, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(question, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(send, FontTL.ROBOTO_REGULAR, 14, Color.WHITE);
        spring.putConstraint("West", question, 34, "West", panel);
        spring.putConstraint("East", question, -34, "East", panel);
        spring.putConstraint("North", question, 51, "North", panel);
        spring.putConstraint("South", question, 130, "North", panel);
        panel.add(question);
        spring.putConstraint("West", link, 34, "West", panel);
        spring.putConstraint("East", link, -34, "East", panel);
        spring.putConstraint("North", link, 20, "South", question);
        spring.putConstraint("South", link, 66, "South", question);
        panel.add(link);
        spring.putConstraint("West", send, 133, "West", panel);
        spring.putConstraint("East", send, -133, "East", panel);
        spring.putConstraint("North", send, 20, "South", link);
        spring.putConstraint("South", send, 59, "South", link);
        panel.add(send);
        final JTextComponent textComponent;
        final int code;
        final Border border2;
        String subUrl;
        final JTextField textField;
        String url;
        send.addActionListener(e -> {
            code = TlauncherUtil.hostAvailabilityCheck(textComponent.getText());
            if (code == 404 || code > 500) {
                textComponent.setBorder(border2);
                return;
            }
            else {
                this.setVisible(false);
                AsyncThread.execute(() -> {
                    try {
                        subUrl = TLauncher.getInnerSettings().get("modpack.operation.url") + ModpackManager.ModpackServerCommand.UPDATE.toString().toLowerCase() + "/" + entity.getId();
                        url = Http.get(subUrl, new HashMap<String, Object>() {
                            final /* synthetic */ JTextField val$link;
                            
                            {
                                ((HashMap<String, String>)this).put("url", this.val$link.getText());
                            }
                        });
                        Http.performPost(Http.constantURL(url), "", "application/json");
                        Alert.showLocMessage("modpack.send.success");
                    }
                    catch (Throwable throwable) {
                        Alert.showMonologError(Localizable.get().get("modpack.error.send.unsuccess"), 0);
                        U.log(throwable);
                    }
                });
                return;
            }
        });
        link.addFocusListener(new FocusListener() {
            @Override
            public void focusLost(final FocusEvent e) {
            }
            
            @Override
            public void focusGained(final FocusEvent e) {
                link.setBorder(border);
            }
        });
    }
}
