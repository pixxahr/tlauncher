// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.event.ActionEvent;
import javax.swing.ListModel;
import javax.swing.DefaultListModel;
import java.util.Iterator;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.concurrent.Executor;
import org.tlauncher.util.async.AsyncThread;
import org.apache.commons.io.IOUtils;
import java.net.URL;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import java.util.Arrays;
import java.awt.event.ComponentListener;
import java.util.concurrent.CompletableFuture;
import javax.swing.SwingUtilities;
import java.awt.event.ComponentEvent;
import javax.swing.JLabel;
import java.awt.event.ComponentAdapter;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.Component;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.swing.renderer.PictureListRenderer;
import org.tlauncher.tlauncher.ui.scenes.ModpackScene;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import java.awt.Color;
import javax.swing.BorderFactory;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JButton;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class PicturePanel extends ExtendedPanel
{
    private JButton previousPicture;
    private JButton nextPicture;
    private JList<ImageIcon> list;
    private final int WIDHT_BUTTON = 64;
    private int current;
    private Dimension buttonSize;
    private List<ImageIcon> cache;
    
    public PicturePanel(final Integer[] array) {
        this.buttonSize = new Dimension(64, 155);
        this.cache = new ArrayList<ImageIcon>();
        this.setPreferredSize(new Dimension(1050, 318));
        this.setLayout(new BorderLayout(0, 0));
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.setOpaque(false);
        this.previousPicture = new ImageUdaterButton(Color.WHITE, Color.WHITE, "previous-arrow.png", "previous-arrow-under.png");
        this.nextPicture = new ImageUdaterButton(Color.WHITE, Color.WHITE, "next-arrow.png", "next-arrow-under.png");
        (this.list = new JList<ImageIcon>()).setPreferredSize(new Dimension(ModpackScene.SIZE.width - 128, 190));
        if (array.length == 1) {
            this.list.setBorder(BorderFactory.createEmptyBorder(64, 304, 0, 0));
        }
        else {
            this.list.setBorder(BorderFactory.createEmptyBorder(64, 0, 0, 0));
        }
        this.list.setOpaque(false);
        this.list.setLayoutOrientation(2);
        this.list.setVisibleRowCount(1);
        this.nextPicture.setPreferredSize(this.buttonSize);
        this.previousPicture.setPreferredSize(this.buttonSize);
        this.list.setCellRenderer(new PictureListRenderer());
        this.list.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (PicturePanel.this.list.getModel().getSize() > 0 && PicturePanel.this.current != -1) {
                    final BigPictureObserver pictureObserver = new BigPictureObserver(TLauncher.getInstance().getFrame(), "", array, PicturePanel.this.current + PicturePanel.this.list.locationToIndex(e.getPoint()));
                    pictureObserver.setVisible(true);
                }
            }
        });
        this.previousPicture.setOpaque(false);
        this.nextPicture.setOpaque(false);
        if (array.length > 3) {
            this.add(this.previousPicture, "West");
            this.add(this.nextPicture, "East");
        }
        else {
            this.setBorder(BorderFactory.createEmptyBorder(0, 64, 0, 64));
        }
        this.nextPicture.addActionListener(e -> this.updateNext());
        this.previousPicture.addActionListener(e -> this.updatePrevious());
        if (array.length == 0) {
            final JLabel l = new LocalizableLabel("modpack.complete.picture.empty");
            l.setHorizontalAlignment(0);
            l.setVerticalAlignment(0);
            SwingUtil.setFontSize(l, 16.0f);
            this.add(l, "Center");
        }
        else {
            final JLabel downloadingLabel = new LocalizableLabel("loginform.loading");
            downloadingLabel.setHorizontalAlignment(0);
            downloadingLabel.setVerticalAlignment(0);
            SwingUtil.setFontSize(downloadingLabel, 16.0f);
            this.add(downloadingLabel, "Center");
            this.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentShown(final ComponentEvent c) {
                    final Integer[] val$array;
                    final Object val$downloadingLabel;
                    final Component component;
                    CompletableFuture.runAsync(() -> {
                        val$array = array;
                        val$downloadingLabel = downloadingLabel;
                        if (PicturePanel.this.cache.isEmpty()) {
                            PicturePanel.this.loadImages(val$array);
                            SwingUtilities.invokeLater(() -> {
                                PicturePanel.this.updateData();
                                PicturePanel.this.remove(component);
                                PicturePanel.this.add(PicturePanel.this.list, "Center");
                                PicturePanel.this.revalidate();
                                PicturePanel.this.repaint();
                            });
                        }
                    });
                }
            });
        }
    }
    
    private void loadImages(final Integer[] array) {
        final Object o;
        final List<CompletableFuture<ImageIcon>> futures = Arrays.stream(array).map(e -> ModpackUtil.getPictureURL(e, "_small_full")).map(s -> CompletableFuture.supplyAsync(() -> {
            try {
                U.debug(s);
                new ImageIcon(IOUtils.toByteArray(new URL(s)));
                return o;
            }
            catch (Exception e2) {
                U.log(e2);
                return null;
            }
        }, AsyncThread.getService())).collect((Collector<? super Object, ?, List<CompletableFuture<ImageIcon>>>)Collectors.toList());
        try {
            CompletableFuture.allOf((CompletableFuture<?>[])futures.toArray(new CompletableFuture[futures.size()])).get();
            for (final CompletableFuture<ImageIcon> f : futures) {
                if (f.get() != null) {
                    this.cache.add(f.get());
                }
            }
        }
        catch (Exception e3) {
            U.log(e3);
        }
    }
    
    private void updateNext() {
        final int enable = this.cache.size() - 3 - this.current;
        if (enable > 0) {
            ++this.current;
            this.updateData();
        }
    }
    
    private void updatePrevious() {
        if (this.current > 0) {
            --this.current;
            this.updateData();
        }
    }
    
    private void updateData() {
        final DefaultListModel<ImageIcon> page = new DefaultListModel<ImageIcon>();
        for (int i = this.current, j = 0; i < this.cache.size() && j < 3; ++i, ++j) {
            page.addElement(this.cache.get(i));
        }
        this.list.setModel(page);
        this.nextPicture.setPreferredSize(this.buttonSize);
        this.previousPicture.setPreferredSize(this.buttonSize);
        this.repaint();
    }
}
