// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.event.ItemEvent;
import java.awt.event.ActionEvent;
import javax.swing.AbstractButton;
import java.awt.Component;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.JComponent;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import org.tlauncher.tlauncher.ui.loc.UpdaterFullButton;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import org.tlauncher.modpack.domain.client.GameVersionDTO;
import org.tlauncher.tlauncher.ui.editor.EditorCheckBox;
import javax.swing.plaf.ComboBoxUI;
import org.tlauncher.tlauncher.ui.ui.CreationModpackForgeComboboxUI;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.swing.renderer.CreationModpackForgeComboboxRenderer;
import javax.swing.border.Border;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import javax.swing.border.CompoundBorder;
import javax.swing.BorderFactory;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.rmo.TLauncher;
import net.minecraft.launcher.versions.CompleteVersion;
import javax.swing.JFrame;
import java.awt.Color;
import org.tlauncher.tlauncher.configuration.Configuration;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import org.tlauncher.tlauncher.controller.ModpackConfig;

public class ModpackConfigFrame extends TemlateModpackFrame
{
    private ModpackConfig controller;
    private JCheckBox box;
    private JTextField modpackName;
    private JComboBox<String> forgeVersion;
    private SliderModpackPanel slider;
    private JButton save;
    private JButton open;
    private JButton remove;
    private static final Dimension maxSize;
    private static final Dimension minSize;
    private final JPanel panel;
    private final JLabel selectedMemory;
    private JLabel question;
    private Configuration c;
    private Color colorButton;
    
    public ModpackConfigFrame(final JFrame parent, final CompleteVersion version) {
        super(parent, "modpack.config.title", ModpackConfigFrame.minSize);
        this.colorButton = new Color(0, 174, 239);
        this.controller = (ModpackConfig)TLauncher.getInjector().getInstance((Class)ModpackConfig.class);
        this.question = new JLabel(ImageCache.getNativeIcon("qestion-option-panel.png"));
        this.c = TLauncher.getInstance().getConfiguration();
        this.question.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                ModpackConfigFrame.this.setVisible(false);
                Alert.showLocMessage(ModpackConfigFrame.this.c.get("memory.problem.message"));
                ModpackConfigFrame.this.setVisible(true);
            }
        });
        final JLabel memorySettings = new LocalizableLabel("modpack.config.memory.title");
        final JLabel optifineLabel = new LocalizableLabel("modpack.config.system.label");
        final Border empty = BorderFactory.createLineBorder(ColorUtil.COLOR_149, 1);
        final CompoundBorder border = new CompoundBorder(empty, BorderFactory.createEmptyBorder(0, 15, 0, 0));
        final JLabel nameLabel = new LocalizableLabel("modpack.creation.name");
        final JLabel forgeVersionLabel = new LocalizableLabel("modpack.version");
        final JLabel gameVersion = new LocalizableLabel("modpack.config.game.version");
        gameVersion.setVerticalAlignment(0);
        this.selectedMemory = new LocalizableLabel("settings.java.memory.label");
        final String currentForge = ((ModpackVersionDTO)version.getModpack().getVersion()).getForgeVersion();
        final String currentGameVersion = ((ModpackVersionDTO)version.getModpack().getVersion()).getGameVersion();
        final GameVersionDTO gameVersionDTO = this.controller.findGameVersion(currentGameVersion);
        if (gameVersionDTO == null) {
            this.forgeVersion = new JComboBox<String>(new String[] { currentForge });
        }
        else {
            this.forgeVersion = new JComboBox<String>(gameVersionDTO.getForgeVersions().toArray(new String[0]));
        }
        this.forgeVersion.setSelectedItem(currentForge);
        this.forgeVersion.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, ColorUtil.COLOR_149));
        this.forgeVersion.setRenderer(new CreationModpackForgeComboboxRenderer());
        this.forgeVersion.setUI(new CreationModpackForgeComboboxUI());
        (this.box = new EditorCheckBox("modpack.config.memory.box")).setIconTextGap(14);
        final EditorCheckBox skinCheckBox = new EditorCheckBox("modpack.config.skin.use");
        skinCheckBox.setIconTextGap(14);
        final GameVersionDTO g1_7_10 = new GameVersionDTO();
        g1_7_10.setName("1.7.10");
        if (ModpackUtil.useSkinMod(version)) {
            skinCheckBox.setSelected(true);
        }
        else if (gameVersionDTO == null) {
            skinCheckBox.setSelected(false);
            final AbstractButton abstractButton;
            skinCheckBox.addItemListener(e -> {
                if (e.getStateChange() == 1) {
                    this.setVisible(false);
                    Alert.showLocMessage("modpack.internet.update");
                    abstractButton.setSelected(false);
                    this.setVisible(true);
                }
                return;
            });
        }
        else if (new ModpackCreation.GameVersionDTOComporator().compare(g1_7_10, gameVersionDTO) == -1) {
            skinCheckBox.setSelected(false);
            final AbstractButton abstractButton2;
            skinCheckBox.addItemListener(e -> {
                if (e.getStateChange() == 1) {
                    this.setVisible(false);
                    Alert.showLocMessage("modpack.config.skin.install.message");
                    abstractButton2.setSelected(false);
                    this.setVisible(true);
                }
                return;
            });
        }
        (this.modpackName = new JTextField(version.getID())).setBorder(border);
        this.modpackName.setForeground(ColorUtil.COLOR_25);
        final JLabel gameVersionValue = new JLabel(currentGameVersion);
        gameVersionValue.setBorder(border);
        this.slider = new SliderModpackPanel(new Dimension(534, 80));
        if (version.getModpack().isModpackMemory()) {
            this.slider.setValue(version.getModpack().getMemory());
        }
        else {
            this.slider.setValue(TLauncher.getInstance().getConfiguration().getInteger("minecraft.memory.ram2"));
        }
        (this.save = new UpdaterFullButton(this.colorButton, ModpackConfigFrame.BLUE_COLOR_UNDER, "settings.save", "save-modpack.png")).setBorder(BorderFactory.createEmptyBorder(0, 14, 0, 0));
        this.save.setIconTextGap(19);
        (this.open = new UpdaterFullButton(this.colorButton, ModpackConfigFrame.BLUE_COLOR_UNDER, "modpack.open.folder", "open-modpack.png")).setIconTextGap(10);
        (this.remove = new UpdaterFullButton(new Color(208, 43, 43), new Color(180, 39, 39), "modpack.popup.delete", "modpack-dustbin.png")).setBorder(BorderFactory.createEmptyBorder(0, 19, 0, 0));
        this.remove.setIconTextGap(15);
        final SpringLayout spring = new SpringLayout();
        (this.panel = new JPanel(spring)).setBackground(Color.WHITE);
        this.panel.setBorder(BorderFactory.createEmptyBorder(20, 0, 21, 0));
        this.addCenter(this.panel);
        SwingUtil.changeFontFamily(nameLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_149);
        SwingUtil.changeFontFamily(gameVersion, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_149);
        SwingUtil.changeFontFamily(gameVersionValue, FontTL.ROBOTO_REGULAR, 16, ColorUtil.COLOR_149);
        SwingUtil.changeFontFamily(this.modpackName, FontTL.ROBOTO_REGULAR, 16, ColorUtil.COLOR_25);
        SwingUtil.changeFontFamily(forgeVersionLabel, FontTL.ROBOTO_REGULAR, 14, ColorUtil.COLOR_149);
        SwingUtil.changeFontFamily(this.forgeVersion, FontTL.ROBOTO_REGULAR, 16, ColorUtil.COLOR_149);
        SwingUtil.changeFontFamily(this.box, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(skinCheckBox, FontTL.ROBOTO_REGULAR, 14, Color.BLACK);
        SwingUtil.changeFontFamily(memorySettings, FontTL.ROBOTO_BOLD, 14, Color.BLACK);
        SwingUtil.changeFontFamily(optifineLabel, FontTL.ROBOTO_BOLD, 14, Color.BLACK);
        SwingUtil.changeFontFamily(this.selectedMemory, FontTL.ROBOTO_BOLD, 14, Color.BLACK);
        SwingUtil.changeFontFamily(this.save, FontTL.ROBOTO_REGULAR, 12, Color.WHITE);
        SwingUtil.changeFontFamily(this.open, FontTL.ROBOTO_REGULAR, 12, Color.WHITE);
        SwingUtil.changeFontFamily(this.remove, FontTL.ROBOTO_REGULAR, 12, Color.WHITE);
        spring.putConstraint("West", nameLabel, 30, "West", this.panel);
        spring.putConstraint("East", nameLabel, 267, "West", this.panel);
        spring.putConstraint("North", nameLabel, -3, "North", this.panel);
        spring.putConstraint("South", nameLabel, 15, "North", this.panel);
        this.panel.add(nameLabel);
        spring.putConstraint("West", this.modpackName, 29, "West", this.panel);
        spring.putConstraint("East", this.modpackName, -27, "East", this.panel);
        spring.putConstraint("North", this.modpackName, 19, "North", this.panel);
        spring.putConstraint("South", this.modpackName, 63, "North", this.panel);
        this.panel.add(this.modpackName);
        spring.putConstraint("West", gameVersion, 30, "West", this.panel);
        spring.putConstraint("East", gameVersion, 129, "West", this.panel);
        spring.putConstraint("North", gameVersion, 10, "South", this.modpackName);
        spring.putConstraint("South", gameVersion, 28, "South", this.modpackName);
        this.panel.add(gameVersion);
        spring.putConstraint("West", gameVersionValue, 30, "West", this.panel);
        spring.putConstraint("East", gameVersionValue, 269, "West", this.panel);
        spring.putConstraint("North", gameVersionValue, 4, "South", gameVersion);
        spring.putConstraint("South", gameVersionValue, 48, "South", gameVersion);
        this.panel.add(gameVersionValue);
        spring.putConstraint("West", forgeVersionLabel, 307, "West", this.panel);
        spring.putConstraint("East", forgeVersionLabel, -27, "East", this.panel);
        spring.putConstraint("North", forgeVersionLabel, 10, "South", this.modpackName);
        spring.putConstraint("South", forgeVersionLabel, 28, "South", this.modpackName);
        this.panel.add(forgeVersionLabel);
        spring.putConstraint("West", this.forgeVersion, 307, "West", this.panel);
        spring.putConstraint("East", this.forgeVersion, -27, "East", this.panel);
        spring.putConstraint("North", this.forgeVersion, 4, "South", forgeVersionLabel);
        spring.putConstraint("South", this.forgeVersion, 48, "South", forgeVersionLabel);
        this.panel.add(this.forgeVersion);
        spring.putConstraint("West", optifineLabel, 29, "West", this.panel);
        spring.putConstraint("East", optifineLabel, 169, "East", this.panel);
        spring.putConstraint("North", optifineLabel, 15, "South", this.forgeVersion);
        spring.putConstraint("South", optifineLabel, 33, "South", this.forgeVersion);
        this.panel.add(optifineLabel);
        spring.putConstraint("West", skinCheckBox, 179, "West", this.panel);
        spring.putConstraint("East", skinCheckBox, -27, "East", this.panel);
        spring.putConstraint("North", skinCheckBox, 15, "South", this.forgeVersion);
        spring.putConstraint("South", skinCheckBox, 33, "South", this.forgeVersion);
        this.panel.add(skinCheckBox);
        spring.putConstraint("West", memorySettings, 29, "West", this.panel);
        spring.putConstraint("East", memorySettings, 169, "East", this.panel);
        spring.putConstraint("North", memorySettings, 10, "South", skinCheckBox);
        spring.putConstraint("South", memorySettings, 33, "South", skinCheckBox);
        this.panel.add(memorySettings);
        spring.putConstraint("West", this.box, 179, "West", this.panel);
        spring.putConstraint("East", this.box, -27, "East", this.panel);
        spring.putConstraint("North", this.box, 10, "South", skinCheckBox);
        spring.putConstraint("South", this.box, 33, "South", skinCheckBox);
        this.panel.add(this.box);
        spring.putConstraint("West", this.save, 29, "West", this.panel);
        spring.putConstraint("East", this.save, 168, "West", this.panel);
        spring.putConstraint("North", this.save, -43, "South", this.panel);
        spring.putConstraint("South", this.save, 0, "South", this.panel);
        this.panel.add(this.save);
        spring.putConstraint("West", this.open, 185, "West", this.panel);
        spring.putConstraint("East", this.open, 390, "West", this.panel);
        spring.putConstraint("North", this.open, -43, "South", this.panel);
        spring.putConstraint("South", this.open, 0, "South", this.panel);
        this.panel.add(this.open);
        spring.putConstraint("West", this.remove, 406, "West", this.panel);
        spring.putConstraint("East", this.remove, -27, "East", this.panel);
        spring.putConstraint("North", this.remove, -43, "South", this.panel);
        spring.putConstraint("South", this.remove, 0, "South", this.panel);
        this.panel.add(this.remove);
        this.box.addItemListener(e -> this.updateState(spring));
        this.box.setSelected(!version.getModpack().isModpackMemory());
        final AbstractButton abstractButton3;
        this.save.addActionListener(e -> {
            if (!this.box.isSelected()) {
                version.getModpack().setMemory(this.slider.getValue());
            }
            version.getModpack().setModpackMemory(!this.box.isSelected());
            this.controller.save(version, this.modpackName.getText(), abstractButton3.isSelected(), (String)this.forgeVersion.getSelectedItem());
            this.setVisible(false);
            return;
        });
        this.open.addActionListener(e -> {
            this.controller.open(version);
            this.setVisible(false);
            return;
        });
        this.remove.addActionListener(e -> {
            this.controller.remove(version);
            this.setVisible(false);
            return;
        });
        this.updateState(spring);
    }
    
    private void updateState(final SpringLayout spring) {
        if (this.box.isSelected()) {
            this.panel.remove(this.selectedMemory);
            this.panel.remove(this.slider);
            this.panel.remove(this.question);
            this.setCenter(ModpackConfigFrame.minSize);
        }
        else {
            spring.putConstraint("West", this.slider, 12, "West", this.panel);
            spring.putConstraint("East", this.slider, -13, "East", this.panel);
            spring.putConstraint("North", this.slider, -130, "South", this.panel);
            spring.putConstraint("South", this.slider, -60, "South", this.panel);
            this.panel.add(this.slider);
            spring.putConstraint("West", this.selectedMemory, 29, "West", this.panel);
            spring.putConstraint("East", this.selectedMemory, 29 + SwingUtil.getWidthText(this.selectedMemory, this.selectedMemory.getText()) + 5, "West", this.panel);
            spring.putConstraint("North", this.selectedMemory, -150, "South", this.panel);
            spring.putConstraint("South", this.selectedMemory, -132, "South", this.panel);
            this.panel.add(this.selectedMemory);
            spring.putConstraint("West", this.question, 2, "East", this.selectedMemory);
            spring.putConstraint("East", this.question, 25, "East", this.selectedMemory);
            spring.putConstraint("North", this.question, -151, "South", this.panel);
            spring.putConstraint("South", this.question, -131, "South", this.panel);
            this.panel.add(this.question);
            this.setCenter(ModpackConfigFrame.maxSize);
            if (!this.c.isExist("memory.problem.message")) {
                this.question.setVisible(false);
            }
        }
        this.panel.revalidate();
        this.panel.repaint();
    }
    
    static {
        maxSize = new Dimension(572, 451);
        minSize = new Dimension(572, 350);
    }
}
