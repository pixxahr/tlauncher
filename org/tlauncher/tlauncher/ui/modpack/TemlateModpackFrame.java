// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack;

import java.awt.Point;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.MainPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.util.swing.FontTL;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import java.awt.FlowLayout;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.event.MouseListener;
import java.awt.BorderLayout;
import java.awt.LayoutManager;
import java.awt.Frame;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.Color;
import javax.swing.JDialog;

public class TemlateModpackFrame extends JDialog
{
    protected int upGap;
    public static final Color BLUE_COLOR;
    public static final Color BLUE_COLOR_UNDER;
    public static final Color COLOR_0_174_239;
    public static final int LEFT_BORDER = 29;
    public static final int RIGHT_BORDER = 27;
    protected LocalizableLabel label;
    protected final JPanel baseContainer;
    private final JPanel paneContainer;
    
    public TemlateModpackFrame(final JFrame parent, final String title, final Dimension size) {
        this(parent, title, size, false);
    }
    
    public TemlateModpackFrame(final JFrame parent, final String title, final Dimension size, final boolean noTransparentImage) {
        super(parent);
        this.upGap = 92;
        this.baseContainer = new JPanel(null);
        this.paneContainer = new JPanel((LayoutManager)new BorderLayout()) {
            @Override
            public synchronized void addMouseListener(final MouseListener l) {
            }
        };
        this.setUndecorated(true);
        this.setTitle(Localizable.get(title));
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        if (!noTransparentImage) {
            this.setBackground(new Color(0, 0, 0, 50));
            this.baseContainer.setOpaque(false);
        }
        this.baseContainer.add(this.paneContainer);
        this.add(this.baseContainer);
        final JPanel panel = new JPanel(new FlowLayout(0, 0, 0));
        panel.setPreferredSize(new Dimension(size.width, 47));
        panel.setBackground(TemlateModpackFrame.BLUE_COLOR);
        final JButton close = new ImageUdaterButton(TemlateModpackFrame.BLUE_COLOR, "close-modpack.png");
        close.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                close.setBackground(new Color(60, 145, 193));
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                close.setBackground(TemlateModpackFrame.BLUE_COLOR);
            }
        });
        SwingUtil.changeFontFamily(this.label = new LocalizableLabel(title), FontTL.ROBOTO_REGULAR, 18, ColorUtil.COLOR_248);
        this.label.setBorder(new EmptyBorder(0, 40, 0, 0));
        this.label.setHorizontalTextPosition(0);
        this.label.setHorizontalAlignment(0);
        this.label.setPreferredSize(new Dimension(size.width - 41, 47));
        close.setPreferredSize(new Dimension(41, 47));
        panel.add(this.label);
        panel.add(close);
        this.paneContainer.add(panel, "North");
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                TemlateModpackFrame.this.setVisible(false);
            }
        });
        final Point point = parent.getContentPane().getLocationOnScreen();
        this.setBounds(point.x, point.y, MainPane.SIZE.width, MainPane.SIZE.height);
        this.setCenter(size);
        if (!TLauncher.DEBUG) {
            this.setAlwaysOnTop(true);
        }
    }
    
    public void addCenter(final JComponent comp) {
        this.paneContainer.add(comp, "Center");
    }
    
    public void setCenter(final Dimension size) {
        final Point point = this.baseContainer.getLocation();
        this.paneContainer.setBounds(point.x + (MainPane.SIZE.width - size.width) / 2, point.y + this.upGap, size.width, size.height);
        this.paneContainer.revalidate();
        this.paneContainer.repaint();
    }
    
    @Override
    public void setVisible(final boolean b) {
        if (b) {
            this.getParent().setEnabled(false);
        }
        else {
            this.getParent().setEnabled(true);
        }
        super.setVisible(b);
    }
    
    static {
        BLUE_COLOR = new Color(69, 168, 223);
        BLUE_COLOR_UNDER = new Color(2, 161, 221);
        COLOR_0_174_239 = new Color(0, 174, 239);
    }
}
