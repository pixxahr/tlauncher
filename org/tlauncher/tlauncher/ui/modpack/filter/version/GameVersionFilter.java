// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter.version;

import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;

public class GameVersionFilter extends VersionFilter
{
    public GameVersionFilter(final GameEntityDTO entityDTO, final GameType gameType, final ModpackDTO modpackDTO) {
        super(entityDTO, gameType, modpackDTO);
    }
    
    @Override
    public boolean isProper(final VersionDTO versionDTO) {
        return versionDTO.getGameVersions() == null || versionDTO.getGameVersions().contains(((ModpackVersionDTO)this.modpackDTO.getVersion()).getGameVersion());
    }
}
