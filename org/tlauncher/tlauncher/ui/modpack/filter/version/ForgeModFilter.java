// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter.version;

import java.util.Comparator;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import org.tlauncher.modpack.domain.client.version.ModVersionDTO;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;

public class ForgeModFilter extends VersionFilter
{
    public ForgeModFilter(final GameEntityDTO entityDTO, final GameType gameType, final ModpackDTO modpackDTO) {
        super(entityDTO, gameType, modpackDTO);
    }
    
    @Override
    public boolean isProper(final VersionDTO versionDTO) {
        if (this.gameType != GameType.MOD && versionDTO instanceof ModVersionDTO) {
            final ModVersionDTO dto = (ModVersionDTO)versionDTO;
            final ForgeComparator f = new ForgeComparator();
            final String version = ((ModpackVersionDTO)this.modpackDTO.getVersion()).getForgeVersion();
            if (dto.getDownForge() != null && f.compare(dto.getDownForge(), version) > 0) {
                return false;
            }
            if (dto.getUpForge() != null && f.compare(version, dto.getUpForge()) == 1) {
                return false;
            }
        }
        return true;
    }
    
    class ForgeComparator implements Comparator<String>
    {
        @Override
        public int compare(final String o1, final String o2) {
            final String[] versions1 = o1.split("-")[1].split("\\.");
            final String[] versions2 = o2.split("-")[1].split("\\.");
            for (int i = 0; i < versions1.length; ++i) {
                final int res = versions1[i].compareTo(versions2[i]);
                if (res != 0) {
                    return res;
                }
            }
            return 0;
        }
    }
}
