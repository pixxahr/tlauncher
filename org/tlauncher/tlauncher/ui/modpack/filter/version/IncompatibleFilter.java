// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter.version;

import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import java.util.HashSet;
import org.tlauncher.modpack.domain.client.share.DependencyType;
import org.tlauncher.modpack.domain.client.GameEntityDependencyDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import java.util.Set;
import org.tlauncher.tlauncher.ui.modpack.filter.GameEntityFilter;

public class IncompatibleFilter implements GameEntityFilter
{
    private Set<Long> modpackIncompatible;
    private Set<Long> modapckIds;
    
    public IncompatibleFilter(final Set<Long> modpackIncompatible, final Set<Long> modapckIds) {
        this.modpackIncompatible = modpackIncompatible;
        this.modapckIds = modapckIds;
    }
    
    @Override
    public boolean isProper(final GameEntityDTO entity) {
        if (this.modpackIncompatible.contains(entity.getId())) {
            return false;
        }
        if (entity.getDependencies() != null) {
            for (final GameEntityDependencyDTO d : entity.getDependencies()) {
                if (d.getDependencyType() == DependencyType.REQUIRED && this.modpackIncompatible.contains(d.getId())) {
                    return false;
                }
            }
        }
        final Set<Long> set = new HashSet<Long>();
        ModpackUtil.extractIncompatible(entity, set);
        return Collections.disjoint(this.modapckIds, set);
    }
}
