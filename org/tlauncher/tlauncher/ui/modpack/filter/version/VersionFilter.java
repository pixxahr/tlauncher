// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter.version;

import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.tlauncher.ui.modpack.filter.Filter;

public abstract class VersionFilter implements Filter<VersionDTO>
{
    protected GameEntityDTO entityDTO;
    protected GameType gameType;
    protected ModpackDTO modpackDTO;
    
    public VersionFilter(final GameEntityDTO entityDTO, final GameType gameType, final ModpackDTO modpackDTO) {
        this.entityDTO = entityDTO;
        this.gameType = gameType;
        this.modpackDTO = modpackDTO;
    }
    
    @Override
    public abstract boolean isProper(final VersionDTO p0);
}
