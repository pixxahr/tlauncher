// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter;

import org.tlauncher.tlauncher.ui.modpack.filter.version.ForgeModFilter;
import org.tlauncher.tlauncher.ui.modpack.filter.version.GameVersionFilter;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import java.util.Iterator;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class BaseModpackFilter<T>
{
    private final List<Filter<T>> filters;
    
    public BaseModpackFilter(final Filter<T>... filters) {
        (this.filters = new ArrayList<Filter<T>>()).addAll(Arrays.asList(filters));
    }
    
    public boolean isProper(final T entity) {
        for (final Filter<T> filter : this.filters) {
            if (!filter.isProper(entity)) {
                return false;
            }
        }
        return true;
    }
    
    public void addFilter(final Filter<T> filter) {
        this.filters.add(filter);
    }
    
    public List<T> findAll(final List<? extends T> gameEntities) {
        final List<T> res = new ArrayList<T>();
        for (final T g : gameEntities) {
            if (this.isProper(g)) {
                res.add(g);
            }
        }
        return res;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "{filters=" + this.filters + '}';
    }
    
    public static BaseModpackFilter<VersionDTO> getBaseModpackStandardFilters(final GameEntityDTO entity, final GameType type, final ModpackComboBox modpackComboBox) {
        final BaseModpackFilter<VersionDTO> filter = new BaseModpackFilter<VersionDTO>((Filter<VersionDTO>[])new Filter[0]);
        if (modpackComboBox.getSelectedIndex() > 0 && type != GameType.MODPACK) {
            final ModpackDTO modpackDTO = modpackComboBox.getSelectedValue().getModpack();
            return getBaseModpackStandardFilters(entity, type, modpackDTO);
        }
        return filter;
    }
    
    public static BaseModpackFilter<VersionDTO> getBaseModpackStandardFilters(final GameEntityDTO entity, final GameType type, final ModpackDTO modpackDTO) {
        final BaseModpackFilter<VersionDTO> filter = new BaseModpackFilter<VersionDTO>((Filter<VersionDTO>[])new Filter[0]);
        filter.addFilter(new GameVersionFilter(entity, type, modpackDTO));
        filter.addFilter(new ForgeModFilter(entity, type, modpackDTO));
        return filter;
    }
}
