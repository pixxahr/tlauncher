// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter;

import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.modpack.domain.client.share.GameType;

public class ListGameEntityImpl implements GameEntityFilter
{
    private GameType gameType;
    private ModpackDTO modpackDTO;
    
    public ListGameEntityImpl(final GameType gameType, final ModpackDTO modpackDTO) {
        this.gameType = gameType;
        this.modpackDTO = modpackDTO;
    }
    
    @Override
    public boolean isProper(final GameEntityDTO gameEntity) {
        for (final VersionDTO v : gameEntity.getVersions()) {
            if (v.getGameVersions() != null && this.modpackDTO.getVersion().getGameVersions() != null && Collections.disjoint(v.getGameVersions(), this.modpackDTO.getVersion().getGameVersions())) {
                return true;
            }
        }
        return true;
    }
}
