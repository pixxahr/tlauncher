// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter;

import org.tlauncher.modpack.domain.client.version.VersionDTO;
import java.util.List;
import java.util.Iterator;
import java.util.Set;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.tlauncher.ui.modpack.filter.version.IncompatibleFilter;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import java.util.HashSet;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.modpack.domain.client.GameEntityDTO;

public class ModpackSceneFilter extends BaseModpackFilter<GameEntityDTO>
{
    private ModpackComboBox modpackComboBox;
    private GameType type;
    
    public ModpackSceneFilter(final ModpackComboBox modpackComboBox, final GameType type, final Filter<GameEntityDTO>... filters) {
        super(filters);
        this.modpackComboBox = modpackComboBox;
        this.type = type;
        if (modpackComboBox.getSelectedIndex() > 0 && type != GameType.MODPACK) {
            final ModpackDTO m = ((CompleteVersion)modpackComboBox.getSelectedItem()).getModpack();
            final ListGameEntityImpl subElementVersionFilter = new ListGameEntityImpl(type, m);
            final Set<Long> incompatible = new HashSet<Long>();
            ModpackUtil.extractIncompatible(m, incompatible);
            for (final GameType t : GameType.getSubEntities()) {
                for (final GameEntityDTO e : ((ModpackVersionDTO)m.getVersion()).getByType(t)) {
                    ModpackUtil.extractIncompatible(e, incompatible);
                }
            }
            final IncompatibleFilter filter = new IncompatibleFilter(incompatible, ModpackUtil.getAllModpackIds(m));
            this.addFilter(filter);
            this.addFilter(subElementVersionFilter);
        }
    }
    
    @Override
    public boolean isProper(final GameEntityDTO entity) {
        if (!super.isProper(entity)) {
            return false;
        }
        if (this.type != GameType.MODPACK && this.modpackComboBox.getSelectedIndex() > 0) {
            final BaseModpackFilter<VersionDTO> filter = BaseModpackFilter.getBaseModpackStandardFilters(entity, this.type, this.modpackComboBox);
            if (filter.findAll(entity.getVersions()).size() == 0) {
                return false;
            }
        }
        return true;
    }
}
