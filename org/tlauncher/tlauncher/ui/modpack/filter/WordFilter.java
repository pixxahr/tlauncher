// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter;

import org.tlauncher.modpack.domain.client.GameEntityDTO;

public class WordFilter extends NameFilter
{
    private final String word;
    
    public WordFilter(final String word) {
        super(word);
        this.word = word;
    }
    
    @Override
    public boolean isProper(final GameEntityDTO gameEntity) {
        return super.isProper(gameEntity) || gameEntity.getShortDescription().toLowerCase().contains(this.word.toLowerCase());
    }
}
