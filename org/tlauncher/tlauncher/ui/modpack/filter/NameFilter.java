// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter;

import org.tlauncher.modpack.domain.client.GameEntityDTO;

public class NameFilter implements GameEntityFilter
{
    private String name;
    
    public NameFilter(final String name) {
        this.name = name;
    }
    
    @Override
    public boolean isProper(final GameEntityDTO gameEntity) {
        return gameEntity.getName().toLowerCase().contains(this.name.toLowerCase());
    }
}
