// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter;

import org.tlauncher.modpack.domain.client.GameEntityDTO;

public interface GameEntityFilter extends Filter<GameEntityDTO>
{
    boolean isProper(final GameEntityDTO p0);
}
