// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.modpack.filter;

import java.util.Iterator;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.share.Category;

public class CategoryFilter implements GameEntityFilter
{
    private Category category;
    
    public CategoryFilter(final Category category) {
        this.category = category;
    }
    
    @Override
    public boolean isProper(final GameEntityDTO gameEntity) {
        if (this.category == Category.ALL) {
            return true;
        }
        for (final Category c : gameEntity.getCategories()) {
            if (c == this.category) {
                return true;
            }
        }
        return false;
    }
}
