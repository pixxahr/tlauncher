// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.animate;

import java.awt.Component;

public class Animator
{
    private static final int DEFAULT_TICK = 20;
    
    public static void move(final Component comp, final int destX, final int destY, final int tick) {
        comp.setLocation(destX, destY);
    }
    
    public static void move(final Component comp, final int destX, final int destY) {
        move(comp, destX, destY, 20);
    }
}
