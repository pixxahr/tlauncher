// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.button;

import org.tlauncher.tlauncher.ui.listener.BlockClickListener;
import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.event.MouseListener;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.tlauncher.managers.ModpackManager;
import javax.swing.JLabel;

public class StatusStarButton extends JLabel
{
    private boolean status;
    private ModpackManager manager;
    
    public StatusStarButton(final GameEntityDTO entityDTO, final GameType type) {
        this.manager = (ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class);
        this.setStatus(this.manager.getStatusModpackElement().contains(entityDTO.getId()));
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (StatusStarButton.this.status) {
                        StatusStarButton.this.manager.removeStatusElement(entityDTO, type);
                    }
                    else {
                        StatusStarButton.this.manager.addStatusElement(entityDTO, type);
                    }
                }
            }
        });
    }
    
    public void setStatus(final boolean status) {
        this.status = status;
        this.setIcon((Icon)ImageCache.getIcon("star-" + status + ".png"));
    }
    
    @Override
    public synchronized void addMouseListener(final MouseListener l) {
        if (l instanceof BlockClickListener) {
            return;
        }
        super.addMouseListener(l);
    }
}
