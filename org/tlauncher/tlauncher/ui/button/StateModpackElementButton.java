// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.button;

import java.awt.Image;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.event.MouseListener;
import java.util.List;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.SubModpackDTO;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.modpack.domain.client.share.StateGameElement;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;

public class StateModpackElementButton extends ImageUdaterButton
{
    private StateGameElement state;
    private ModpackManager manager;
    
    public StateModpackElementButton(final SubModpackDTO entity, final GameType type) {
        super(buildImage(entity.getStateGameElement()));
        this.manager = (ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class);
        if (this.state != StateGameElement.BLOCK) {
            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        final List<GameEntityDTO> list = StateModpackElementButton.this.manager.findDependenciesFromGameEntityDTO(entity);
                        final StringBuilder b = ModpackUtil.buildMessage(list);
                        if (list.isEmpty()) {
                            StateModpackElementButton.this.manager.changeModpackElementState(entity, type);
                        }
                        else if (Alert.showQuestion("", Localizable.get("modpack.left.element.remove.question", entity.getName(), b.toString()))) {
                            StateModpackElementButton.this.manager.changeModpackElementState(entity, type);
                        }
                    }
                }
            });
        }
    }
    
    public void setState(final StateGameElement state) {
        this.setImage(ImageCache.getImage(buildImage(state)));
        this.state = state;
    }
    
    private static String buildImage(final StateGameElement state) {
        return (state == null) ? (StateGameElement.ACTIVE + "-element-left.png") : (state + "-element-left.png");
    }
}
