// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.button;

import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import java.awt.Graphics2D;
import com.google.common.base.Strings;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.util.U;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.BorderFactory;
import java.awt.image.BufferedImage;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;

public class RoundImageButton extends LocalizableButton
{
    private BufferedImage current;
    
    public RoundImageButton(final BufferedImage image, final BufferedImage mouseUnderImage) {
        super("");
        this.current = image;
        this.setOpaque(false);
        this.setBorder(BorderFactory.createEmptyBorder());
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                RoundImageButton.this.current = mouseUnderImage;
                RoundImageButton.this.repaint();
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                RoundImageButton.this.current = image;
                RoundImageButton.this.repaint();
            }
        });
    }
    
    public RoundImageButton(final String image, final String mouseUnderUrl) {
        this(ImageCache.loadImage(U.makeURL(image), true), ImageCache.loadImage(U.makeURL(mouseUnderUrl), true));
    }
    
    public Dimension getImageSize() {
        return new Dimension(this.current.getWidth(), this.current.getHeight());
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        g.drawImage(this.current, 0, 0, null);
        final String text = this.getText();
        if (!Strings.isNullOrEmpty(text)) {
            SwingUtil.paintText((Graphics2D)g, this, text);
        }
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(this.current.getWidth(), this.current.getHeight());
    }
}
