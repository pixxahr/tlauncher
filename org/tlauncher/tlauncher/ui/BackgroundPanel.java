// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui;

import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.Graphics;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class BackgroundPanel extends JPanel
{
    private final BufferedImage image;
    
    public BackgroundPanel(final String name) {
        this.image = ImageCache.get(name);
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        g.drawImage(this.image, 0, 0, null);
    }
}
