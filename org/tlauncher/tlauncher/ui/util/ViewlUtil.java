// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.util;

import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.text.JTextComponent;
import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.JFrame;

public class ViewlUtil
{
    public static final int minCountCharater = 12;
    
    public static Dimension calculateSizeReview(final JFrame parent) {
        final Dimension dimension = parent.getSize();
        dimension.setSize(dimension.getWidth() * 2.0 / 3.0, dimension.getHeight() * 4.0 / 5.0 + 10.0);
        return dimension;
    }
    
    public static JPanel createBoxYPanel(final JLabel label, final JTextComponent component) {
        final JPanel p = new JPanel();
        p.setAlignmentX(0.0f);
        final BoxLayout layout = new BoxLayout(p, 1);
        p.setLayout(layout);
        label.setAlignmentX(0.0f);
        component.setAlignmentY(0.0f);
        p.add(label);
        p.add(component);
        return p;
    }
    
    public static String addSpaces(final String line, final String serverName) {
        final int addedSpaces = 12 - serverName.length();
        if (addedSpaces <= 0) {
            return line;
        }
        final StringBuilder builder = new StringBuilder(line);
        for (int i = 0; i < addedSpaces; ++i) {
            builder.append(' ');
        }
        return builder.toString();
    }
}
