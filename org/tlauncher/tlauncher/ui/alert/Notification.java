// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.alert;

import java.awt.event.ActionEvent;
import java.awt.Component;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.JCheckBox;
import org.tlauncher.tlauncher.ui.swing.FlexibleEditorPanel;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class Notification extends JPanel
{
    private static final int WIDTH = 500;
    public static final String MEMORY_NOTIFICATION = "memory.notification.off";
    
    public Notification(final String message, final String saveKey) {
        final BoxLayout box = new BoxLayout(this, 1);
        this.setLayout(box);
        final FlexibleEditorPanel label = new FlexibleEditorPanel("text/html", Localizable.get(message), 500);
        final JCheckBox notificationState = new JCheckBox(Localizable.get("skin.notification.state"));
        notificationState.addActionListener(e -> {
            if (notificationState.isSelected()) {
                TLauncher.getInstance().getConfiguration().set(saveKey, "true", true);
            }
            else {
                TLauncher.getInstance().getConfiguration().set(saveKey, "false", true);
            }
            return;
        });
        this.add(label);
        final JPanel panel = new JPanel();
        panel.add(notificationState);
        this.add(panel);
    }
}
