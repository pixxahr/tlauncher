// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.alert;

import org.tlauncher.tlauncher.ui.swing.ScrollPane;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.swing.TextPopup;
import javax.swing.JTextArea;
import org.tlauncher.util.U;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.swing.editor.EditorPane;
import org.tlauncher.util.StringUtil;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import java.awt.Dimension;
import javax.swing.JPanel;

class AlertPanel extends JPanel
{
    private static final int MAX_CHARS = 80;
    private static final int MAX_WIDTH = 500;
    private static final int MAX_HEIGHT = 300;
    private static final Dimension MAX_SIZE;
    
    AlertPanel(final String rawMessage, final Object rawTextarea) {
        this.setLayout(new BoxLayout(this, 1));
        String message;
        if (rawMessage == null) {
            message = null;
        }
        else {
            message = StringUtil.wrap("<html>" + rawMessage + "</html>", 80);
        }
        final EditorPane label = new EditorPane("text/html", message);
        label.setAlignmentX(0.0f);
        label.setFocusable(false);
        this.add(label);
        if (rawTextarea == null) {
            return;
        }
        final String textarea = U.toLog(rawTextarea);
        final JTextArea area = new JTextArea(textarea);
        area.addMouseListener(new TextPopup());
        area.setFont(this.getFont());
        area.setEditable(false);
        final ScrollPane scroll = new ScrollPane(area, true);
        scroll.setAlignmentX(0.0f);
        scroll.setVBPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        final int textAreaHeight = StringUtil.countLines(textarea) * this.getFontMetrics(this.getFont()).getHeight();
        if (textAreaHeight > 300) {
            scroll.setPreferredSize(AlertPanel.MAX_SIZE);
        }
        this.add(scroll);
    }
    
    static {
        MAX_SIZE = new Dimension(500, 300);
    }
}
