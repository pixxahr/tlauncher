// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.alert;

import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.Container;
import java.util.Arrays;
import java.awt.Frame;
import javax.swing.JDialog;
import javax.swing.JButton;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.ui.swing.HtmlTextPane;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.loc.LocalizableCheckbox;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import javax.swing.Icon;
import java.awt.Component;
import javax.swing.JOptionPane;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.util.U;
import javax.swing.JFrame;
import java.awt.Color;

public class Alert
{
    private static final Color YES;
    private static final Color NO;
    private static final JFrame frame;
    private static final String PREFIX = "TLauncher : ";
    private static final String MISSING_TITLE = "MISSING TITLE";
    private static final String MISSING_MESSAGE = "MISSING MESSAGE";
    private static final String MISSING_QUESTION = "MISSING QUESTION";
    private static String DEFAULT_TITLE;
    private static String DEFAULT_MESSAGE;
    
    public static void showError(final String title, final String message, final Object textarea) {
        if (textarea instanceof Throwable) {
            U.log("Showing error:", textarea);
            final Throwable throwable = (Throwable)textarea;
            final String messageThrowable = throwable.getMessage();
            showMonolog(0, title, message, throwable.getClass().getName() + ": " + messageThrowable);
            return;
        }
        showMonolog(0, title, message, textarea);
    }
    
    public static void showError(final String title, final String message) {
        showError(title, message, null);
    }
    
    public static void showError(final String message, final Object textarea) {
        showError(Alert.DEFAULT_TITLE, message, textarea);
    }
    
    public static void showError(final Object textarea, final boolean exit) {
        showError(Alert.DEFAULT_TITLE, Alert.DEFAULT_MESSAGE, textarea);
        if (exit) {
            System.exit(-1);
        }
    }
    
    public static void showError(final Object textarea) {
        showError(textarea, false);
    }
    
    public static void showLocError(final String titlePath, final String messagePath, final Object textarea) {
        showError(getLoc(titlePath, "MISSING TITLE"), getLoc(messagePath, "MISSING MESSAGE"), textarea);
    }
    
    public static void showLocError(final String path, final Object textarea) {
        showError(getLoc(path + ".title", "MISSING TITLE"), getLoc(path, "MISSING MESSAGE"), textarea);
    }
    
    public static void showLocError(final String path) {
        showLocError(path, null);
    }
    
    public static void showMessage(final String title, final String message, final Object textarea) {
        showMonolog(1, title, message, textarea);
    }
    
    public static void showMessage(final String title, final String message) {
        showMessage(title, message, null);
    }
    
    public static void showLocMessage(final String titlePath, final String messagePath, final Object textarea) {
        showMessage(getLoc(titlePath, "MISSING TITLE"), getLoc(messagePath, "MISSING MESSAGE"), textarea);
    }
    
    public static void showLocMessage(final String path, final Object textarea) {
        showMessage(getLoc(path + ".title", "MISSING TITLE"), getLoc(path, "MISSING MESSAGE"), textarea);
    }
    
    public static void showLocMessageWithoutTitle(final String path) {
        showMessage(getLoc("", "MISSING TITLE"), getLoc(path, "MISSING MESSAGE"));
    }
    
    public static void showLocMessage(final String path) {
        showLocMessage(path, null);
    }
    
    public static void showWarning(final String title, final String message, final Object textarea) {
        showMonolog(2, title, message, textarea);
    }
    
    public static void showWarning(final String title, final String message) {
        showWarning(title, message, null);
    }
    
    public static void showLocWarning(final String titlePath, final String messagePath, final Object textarea) {
        showWarning(getLoc(titlePath, "MISSING TITLE"), getLoc(messagePath, "MISSING MESSAGE"), textarea);
    }
    
    private static void showLocWarning(final String titlePath, final String messagePath) {
        showLocWarning(titlePath, messagePath, null);
    }
    
    public static void showLocWarning(final String path, final Object textarea) {
        showWarning(getLoc(path + ".title", "MISSING TITLE"), getLoc(path, "MISSING MESSAGE"), textarea);
    }
    
    public static void showLocWarning(final String path) {
        showLocWarning(path, (Object)null);
    }
    
    public static boolean showQuestion(final String title, final String question, final Object textarea) {
        return showConfirmDialog(0, 3, title, question, textarea) == 0;
    }
    
    public static boolean showQuestion(final String title, final String question) {
        return showQuestion(title, question, null);
    }
    
    public static boolean showLocQuestion(final String titlePath, final String questionPath, final Object textarea) {
        return showQuestion(getLoc(titlePath, "MISSING TITLE"), getLoc(questionPath, "MISSING QUESTION"), textarea);
    }
    
    public static boolean showLocQuestion(final String titlePath, final String questionPath) {
        return showQuestion(getLoc(titlePath, titlePath), getLoc(questionPath, questionPath), null);
    }
    
    public static boolean showLocQuestion(final String path, final Object textarea) {
        return showQuestion(getLoc(path + ".title", "MISSING TITLE"), getLoc(path, "MISSING QUESTION"), textarea);
    }
    
    public static boolean showLocQuestion(final String path) {
        return showLocQuestion(path, (Object)null);
    }
    
    private static void showMonolog(final int messageType, final String title, final String message, final Object textarea) {
        String button = "ui.ok";
        if (!Localizable.exists()) {
            button = "OK";
        }
        final UpdaterButton ok = new UpdaterButton(UpdaterButton.GREEN_COLOR, button);
        addListener(ok, Alert.YES);
        ok.setForeground(Color.WHITE);
        JOptionPane.showOptionDialog(Alert.frame, new AlertPanel(message, textarea), getTitle(title), 1, messageType, null, new Object[] { ok }, 0);
    }
    
    public static int showErrorMessage(final String title, final String message, final String button1Text, final String button2Text) {
        final UpdaterButton button1 = new UpdaterButton(UpdaterButton.GREEN_COLOR, button1Text);
        final UpdaterButton button2 = new UpdaterButton(UpdaterButton.ORRANGE_COLOR, button2Text);
        addListener(button1, Alert.YES);
        addListener(button2, Alert.NO);
        button1.setForeground(Color.WHITE);
        SwingUtil.setFontSize(button1, 13.0f);
        SwingUtil.setFontSize(button2, 13.0f);
        button2.setForeground(Color.WHITE);
        return JOptionPane.showOptionDialog(Alert.frame, Localizable.get(message), Localizable.get(title), 0, 0, null, new Object[] { button1, button2 }, 0);
    }
    
    public static void showCustomMonolog(final String title, final Object textarea) {
        final UpdaterButton ok = new UpdaterButton(UpdaterButton.GREEN_COLOR, "ui.ok");
        addListener(ok, Alert.YES);
        ok.setForeground(Color.WHITE);
        JOptionPane.showOptionDialog(Alert.frame, textarea, getTitle(title), 0, -1, null, new Object[] { ok }, 0);
    }
    
    public static int showConfirmDialog(final int optionType, final int messageType, final String title, final String message, final Object textarea) {
        final UpdaterButton yes = new UpdaterButton(UpdaterButton.GREEN_COLOR, "ui.yes");
        yes.setForeground(Color.WHITE);
        final UpdaterButton no = new UpdaterButton(UpdaterButton.ORRANGE_COLOR, "ui.no");
        no.setForeground(Color.WHITE);
        addListener(yes, Alert.YES);
        addListener(no, Alert.NO);
        return JOptionPane.showOptionDialog(Alert.frame, new AlertPanel(message, textarea), getTitle(title), 0, messageType, null, new Object[] { yes, no }, 0);
    }
    
    public static void prepareLocal() {
        Alert.DEFAULT_TITLE = getLoc("alert.error.title", Alert.DEFAULT_TITLE);
        Alert.DEFAULT_MESSAGE = getLoc("alert.error.message", Alert.DEFAULT_MESSAGE);
    }
    
    private static String getTitle(final String title) {
        return "TLauncher : " + ((title == null) ? "MISSING TITLE" : title);
    }
    
    private static String getLoc(final String path, final String fallbackMessage) {
        final String result = Localizable.get(path);
        return (result == null) ? fallbackMessage : result;
    }
    
    public static void showMonologError(final String message, final int number) {
        JOptionPane.showMessageDialog(null, new AlertPanel(message, null), "", number, null);
    }
    
    protected static JOptionPane getOptionPane(final JComponent parent) {
        JOptionPane pane;
        if (!(parent instanceof JOptionPane)) {
            pane = getOptionPane((JComponent)parent.getParent());
        }
        else {
            pane = (JOptionPane)parent;
        }
        return pane;
    }
    
    public static void addListener(final UpdaterButton changes, final Color color) {
        final JOptionPane pane;
        changes.addActionListener(e -> {
            pane = getOptionPane((JComponent)e.getSource());
            pane.setValue(changes);
            return;
        });
        changes.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                changes.setBackground(color);
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                changes.setBackground(changes.getBackgroundColor());
            }
        });
    }
    
    public static boolean showWarningMessageWithCheckBox(final String title, final String message, final int width) {
        return showWarningMessageWithCheckBox(title, message, width, "skin.notification.state");
    }
    
    public static boolean showWarningMessageWithCheckBox(final String title, final String message, final int width, final String buttonText) {
        final UpdaterButton ok = new UpdaterButton(UpdaterButton.GREEN_COLOR, "ui.ok");
        addListener(ok, Alert.YES);
        final LocalizableCheckbox b = new LocalizableCheckbox(buttonText, LocalizableCheckbox.PANEL_TYPE.SETTINGS);
        b.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        b.setHorizontalAlignment(0);
        b.setIconTextGap(10);
        b.setState(false);
        final JPanel panel = new JPanel(new BorderLayout(0, 10));
        final HtmlTextPane pane = HtmlTextPane.get(Localizable.get(message), width);
        panel.add(pane, "Center");
        panel.add(b, "South");
        ok.setForeground(Color.WHITE);
        JOptionPane.showOptionDialog(Alert.frame, panel, " " + Localizable.get(title), 0, 1, (Icon)ImageCache.getIcon("warning.png"), new Object[] { ok }, null);
        return b.getState();
    }
    
    public static void showHtmlMessage(final String title, final String message, final int type, final int width) {
        String textValue;
        if (Localizable.exists()) {
            textValue = Localizable.get("ui.ok");
        }
        else {
            textValue = "OK";
        }
        final UpdaterButton ok = new UpdaterButton(UpdaterButton.GREEN_COLOR, textValue);
        addListener(ok, Alert.YES);
        ok.setForeground(Color.WHITE);
        JOptionPane.showOptionDialog(Alert.frame, HtmlTextPane.get(Localizable.get(message), width), Localizable.get(title), 0, type, null, new Object[] { ok }, null);
    }
    
    public static void showErrorHtml(final String message, final int width) {
        showHtmlMessage("", message, 0, width);
    }
    
    public static void showErrorHtml(final String title, final String message) {
        showHtmlMessage(title, message, 0, 500);
    }
    
    public static void showMessage(final String title, final JPanel content, final JButton[] buttons) {
        final JDialog jDialog = new JDialog(Alert.frame, title);
        Arrays.stream(buttons).forEach(e -> e.addActionListener(a -> jDialog.setVisible((boolean)(0 != 0))));
        jDialog.setAlwaysOnTop(true);
        jDialog.setResizable(false);
        jDialog.setContentPane(content);
        jDialog.setModal(true);
        jDialog.pack();
        jDialog.setLocationRelativeTo(null);
        jDialog.setVisible(true);
    }
    
    public static boolean showWarningMessageWithCheckBox1(final String title, final String message, final int width, final String buttonText) {
        final JButton ok = new JButton("OK");
        final JOptionPane pane;
        final Object value;
        ok.addActionListener(e -> {
            pane = getOptionPane((JComponent)e.getSource());
            pane.setValue(value);
            return;
        });
        final JCheckBox b = new JCheckBox(buttonText);
        b.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        b.setHorizontalAlignment(0);
        b.setIconTextGap(10);
        b.getModel().setSelected(false);
        final JPanel panel = new JPanel(new BorderLayout(0, 10));
        final HtmlTextPane pane2 = HtmlTextPane.get(Localizable.get(message), width);
        panel.add(pane2, "Center");
        panel.add(b, "South");
        ok.setForeground(Color.WHITE);
        JOptionPane.showOptionDialog(null, panel, " " + Localizable.get(title), 0, 1, null, new Object[] { ok }, null);
        return b.getModel().isSelected();
    }
    
    static {
        YES = new Color(88, 159, 42);
        NO = new Color(204, 118, 47);
        frame = new JFrame();
        Alert.DEFAULT_TITLE = "An error occurred";
        Alert.DEFAULT_MESSAGE = "An unexpected error occurred";
        Alert.frame.setAlwaysOnTop(true);
    }
}
