// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.util.Iterator;
import java.util.Collection;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;

public class SimpleComboBoxModel<E> extends DefaultComboBoxModel<E>
{
    private static final long serialVersionUID = 5950434966721171811L;
    protected Vector<E> objects;
    protected Object selectedObject;
    
    public SimpleComboBoxModel() {
        this.objects = new Vector<E>();
    }
    
    public SimpleComboBoxModel(final E[] items) {
        this.objects = new Vector<E>(items.length);
        for (int i = 0, c = items.length; i < c; ++i) {
            this.objects.addElement(items[i]);
        }
        if (this.getSize() > 0) {
            this.selectedObject = this.getElementAt(0);
        }
    }
    
    public SimpleComboBoxModel(final Vector<E> v) {
        this.objects = v;
        if (this.getSize() > 0) {
            this.selectedObject = this.getElementAt(0);
        }
    }
    
    @Override
    public void setSelectedItem(final Object anObject) {
        if ((this.selectedObject != null && !this.selectedObject.equals(anObject)) || (this.selectedObject == null && anObject != null)) {
            this.selectedObject = anObject;
            this.fireContentsChanged(this, -1, -1);
        }
    }
    
    @Override
    public Object getSelectedItem() {
        return this.selectedObject;
    }
    
    @Override
    public int getSize() {
        return this.objects.size();
    }
    
    @Override
    public E getElementAt(final int index) {
        if (index >= 0 && index < this.objects.size()) {
            return this.objects.elementAt(index);
        }
        return null;
    }
    
    @Override
    public int getIndexOf(final Object anObject) {
        return this.objects.indexOf(anObject);
    }
    
    @Override
    public void addElement(final E anObject) {
        this.objects.addElement(anObject);
        final int size = this.objects.size();
        final int index = this.objects.size() - 1;
        this.fireIntervalAdded(this, index, index);
        if (size == 1 && this.selectedObject == null && anObject != null) {
            this.setSelectedItem(anObject);
        }
    }
    
    public void addElements(final Collection<E> list) {
        if (list.size() == 0) {
            return;
        }
        final int size = list.size();
        final int index0 = this.objects.size();
        final int index2 = index0 + size - 1;
        this.objects.addAll((Collection<? extends E>)list);
        this.fireIntervalAdded(this, index0, index2);
        if (this.selectedObject == null) {
            for (final E elem : list) {
                if (elem == null) {
                    continue;
                }
                this.setSelectedItem(elem);
                break;
            }
        }
    }
    
    @Override
    public void insertElementAt(final E anObject, final int index) {
        this.objects.insertElementAt(anObject, index);
        this.fireIntervalAdded(this, index, index);
    }
    
    @Override
    public void removeElementAt(final int index) {
        if (this.getElementAt(index) == this.selectedObject) {
            if (index == 0) {
                this.setSelectedItem((this.getSize() == 1) ? null : this.getElementAt(index + 1));
            }
            else {
                this.setSelectedItem(this.getElementAt(index - 1));
            }
        }
        this.objects.removeElementAt(index);
        this.fireIntervalRemoved(this, index, index);
    }
    
    @Override
    public void removeElement(final Object anObject) {
        final int index = this.objects.indexOf(anObject);
        if (index != -1) {
            this.removeElementAt(index);
        }
    }
    
    @Override
    public void removeAllElements() {
        final int size = this.objects.size();
        if (size > 0) {
            final int firstIndex = 0;
            final int lastIndex = size - 1;
            this.objects.removeAllElements();
            this.selectedObject = null;
            this.fireIntervalRemoved(this, firstIndex, lastIndex);
        }
        else {
            this.selectedObject = null;
        }
    }
}
