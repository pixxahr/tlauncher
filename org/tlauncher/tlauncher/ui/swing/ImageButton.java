// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.FontMetrics;
import java.awt.Composite;
import java.awt.AlphaComposite;
import java.awt.image.ImageObserver;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedButton;

public class ImageButton extends ExtendedButton
{
    private static final long serialVersionUID = 1L;
    protected Image image;
    protected ImageRotation rotation;
    private int margin;
    private boolean pressed;
    
    protected ImageButton() {
        this.rotation = ImageRotation.CENTER;
        this.margin = 4;
        this.initListeners();
    }
    
    public ImageButton(final String label, final Image image, final ImageRotation rotation, final int margin) {
        super(label);
        this.rotation = ImageRotation.CENTER;
        this.margin = 4;
        this.image = image;
        this.rotation = rotation;
        this.margin = margin;
        this.initImage();
        this.initListeners();
    }
    
    public ImageButton(final String label, final Image image, final ImageRotation rotation) {
        this(label, image, rotation, 4);
    }
    
    public ImageButton(final String label, final Image image) {
        this(label, image, ImageRotation.CENTER);
    }
    
    public ImageButton(final Image image) {
        this(null, image);
    }
    
    public ImageButton(final String imagepath) {
        this(null, loadImage(imagepath));
    }
    
    public ImageButton(final String label, final String imagepath, final ImageRotation rotation, final int margin) {
        this(label, loadImage(imagepath), rotation, margin);
    }
    
    public ImageButton(final String label, final String imagepath, final ImageRotation rotation) {
        this(label, loadImage(imagepath), rotation);
    }
    
    public ImageButton(final String label, final String imagepath) {
        this(label, loadImage(imagepath));
    }
    
    public Image getImage() {
        return this.image;
    }
    
    public void setImage(final Image image) {
        this.image = image;
        this.initImage();
        this.repaint();
    }
    
    public ImageRotation getRotation() {
        return this.rotation;
    }
    
    public int getImageMargin() {
        return this.margin;
    }
    
    @Override
    public void update(final Graphics g) {
        super.update(g);
        this.paint(g);
    }
    
    public void paintComponent(final Graphics g0) {
        if (this.image == null) {
            return;
        }
        final Graphics2D g = (Graphics2D)g0;
        final String text = this.getText();
        final boolean drawtext = text != null && text.length() > 0;
        final FontMetrics fm = g.getFontMetrics();
        final float opacity = this.isEnabled() ? 1.0f : 0.5f;
        final int width = this.getWidth();
        final int height = this.getHeight();
        int rmargin = this.margin;
        final int offset = this.pressed ? 1 : 0;
        final int iwidth = this.image.getWidth(null);
        final int iheight = this.image.getHeight(null);
        int ix = 0;
        final int iy = height / 2 - iheight / 2;
        int twidth;
        if (drawtext) {
            twidth = fm.stringWidth(text);
        }
        else {
            rmargin = (twidth = 0);
        }
        switch (this.rotation) {
            case LEFT: {
                ix = width / 2 - twidth / 2 - iwidth - rmargin;
                break;
            }
            case CENTER: {
                ix = width / 2 - iwidth / 2;
                break;
            }
            case RIGHT: {
                ix = width / 2 + twidth / 2 + rmargin;
                break;
            }
            default: {
                throw new IllegalStateException("Unknown rotation!");
            }
        }
        final Composite c = g.getComposite();
        g.setComposite(AlphaComposite.getInstance(3, opacity));
        g.drawImage(this.image, ix + offset, iy + offset, null);
        g.setComposite(c);
        this.pressed = false;
    }
    
    protected static Image loadImage(final String path) {
        return ImageCache.getImage(path);
    }
    
    protected void initImage() {
        if (this.image == null) {
            return;
        }
        this.setPreferredSize(new Dimension(this.image.getWidth(null) + 10, this.image.getHeight(null) + 10));
    }
    
    private void initListeners() {
        this.initImage();
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(final MouseEvent e) {
            }
            
            @Override
            public void mouseEntered(final MouseEvent e) {
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
            }
            
            @Override
            public void mousePressed(final MouseEvent e) {
                ImageButton.this.pressed = true;
            }
            
            @Override
            public void mouseReleased(final MouseEvent e) {
            }
        });
        this.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(final KeyEvent e) {
                if (e.getKeyCode() != 32) {
                    return;
                }
                ImageButton.this.pressed = true;
            }
            
            @Override
            public void keyReleased(final KeyEvent e) {
                ImageButton.this.pressed = false;
            }
            
            @Override
            public void keyTyped(final KeyEvent e) {
            }
        });
    }
    
    public enum ImageRotation
    {
        LEFT, 
        CENTER, 
        RIGHT;
    }
}
