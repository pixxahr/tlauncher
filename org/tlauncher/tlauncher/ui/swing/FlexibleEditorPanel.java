// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.net.URL;
import org.tlauncher.util.OS;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.EditorKit;
import java.awt.Insets;
import org.tlauncher.tlauncher.ui.swing.editor.ExtendedHTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.Font;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import javax.swing.JEditorPane;

public class FlexibleEditorPanel extends JEditorPane implements LocalizableComponent
{
    public FlexibleEditorPanel(Font font, final int width) {
        if (font != null) {
            this.setFont(font);
        }
        else {
            font = this.getFont();
        }
        final StyleSheet css = new StyleSheet();
        css.importStyleSheet(this.getClass().getResource("styles.css"));
        css.addRule("body { font-family: " + font.getFamily() + ";width:" + width + "; font-size: " + font.getSize() + "pt; } " + "a { text-decoration: underline; }");
        final ExtendedHTMLEditorKit html = new ExtendedHTMLEditorKit();
        html.setStyleSheet(css);
        this.getDocument().putProperty("IgnoreCharsetDirective", Boolean.TRUE);
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setEditorKit(html);
        this.setEditable(false);
        this.setOpaque(false);
        this.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(final HyperlinkEvent e) {
                if (!e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                    return;
                }
                final URL url = e.getURL();
                if (url == null) {
                    return;
                }
                OS.openLink(url);
            }
        });
    }
    
    public FlexibleEditorPanel(final int width) {
        this(new LocalizableLabel().getFont(), width);
    }
    
    public FlexibleEditorPanel(final String type, final String text, final int width) {
        this(width);
        this.setContentType(type);
        this.setText(Localizable.get(text));
    }
    
    @Override
    public void updateLocale() {
        this.setText(Localizable.get("auth.tip.tlauncher"));
    }
}
