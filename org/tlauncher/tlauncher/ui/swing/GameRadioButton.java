// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Dimension;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.LocalizableRadioButton;

public class GameRadioButton extends LocalizableRadioButton
{
    private Color selected;
    private Color under;
    private boolean mouseUnder;
    
    public GameRadioButton(final String string) {
        super(string);
        this.selected = new Color(60, 170, 232);
        this.under = new Color(255, 202, 41);
        this.mouseUnder = false;
        this.setPreferredSize(new Dimension(149, 52));
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                GameRadioButton.this.mouseUnder = true;
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                GameRadioButton.this.mouseUnder = false;
            }
        });
        this.setForeground(Color.BLACK);
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        final Rectangle rec = this.getVisibleRect();
        int i = 242;
        int y = rec.y;
        final Graphics2D g2 = (Graphics2D)g;
        while (y < rec.height + rec.y) {
            g2.setColor(new Color(i, i, i));
            if (i != 255) {
                ++i;
            }
            g2.drawLine(rec.x, y, rec.x + rec.width, y);
            ++y;
        }
        if (this.isSelected()) {
            y = rec.y + rec.height - 3;
            g2.setColor(this.selected);
            while (y < rec.height + rec.y) {
                g2.drawLine(rec.x, y, rec.x + rec.width, y);
                ++y;
            }
        }
        else if (this.mouseUnder) {
            y = rec.y + rec.height - 3;
            g2.setColor(this.under);
            while (y < rec.height + rec.y) {
                g2.drawLine(rec.x, y, rec.x + rec.width, y);
                ++y;
            }
        }
        this.paintText(g2, rec);
    }
    
    protected void paintText(final Graphics2D g, final Rectangle textRect) {
        g.setColor(this.getForeground());
        final String text = this.getText();
        final Graphics2D g2d = g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        final FontMetrics fm = g2d.getFontMetrics();
        final Rectangle2D r = fm.getStringBounds(text, g2d);
        g.setFont(this.getFont());
        final int x = (this.getWidth() - (int)r.getWidth()) / 2;
        final int y = (this.getHeight() - (int)r.getHeight()) / 2 + fm.getAscent();
        g2d.drawString(text, x, y);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    }
}
