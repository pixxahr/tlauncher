// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.image.ImageObserver;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.LocalizableRadioButton;

public class GameRadioTextButton extends LocalizableRadioButton
{
    protected Color defaultColor;
    private Color under;
    private boolean mouseUnder;
    
    public GameRadioTextButton(final String string) {
        super(string);
        this.defaultColor = new Color(60, 170, 232);
        this.under = new Color(255, 202, 41);
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                GameRadioTextButton.this.mouseUnder = true;
                GameRadioTextButton.this.repaint();
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                GameRadioTextButton.this.mouseUnder = false;
                GameRadioTextButton.this.repaint();
            }
        });
        this.setForeground(Color.WHITE);
        this.setPreferredSize(new Dimension(129, 55));
    }
    
    @Override
    protected void paintComponent(final Graphics gr) {
        super.paintComponent(gr);
        final Rectangle rec = this.getVisibleRect();
        final Graphics2D g2 = (Graphics2D)gr;
        this.paintBackground(g2, rec);
        this.paintText(g2, rec);
    }
    
    protected void paintBackground(final Graphics2D g2, final Rectangle rec) {
        int y = rec.y;
        g2.drawImage(ImageCache.getBufferedImage("modpack-radio-button-background.png"), 0, 0, null);
        if (this.isSelected() || this.mouseUnder) {
            y = rec.y + rec.height - 9;
            g2.setColor(this.under);
            while (y < rec.height + rec.y) {
                g2.drawLine(rec.x, y, rec.x + rec.width, y);
                ++y;
            }
        }
        else {
            y = rec.y + rec.height - 9;
            g2.setColor(this.defaultColor);
            while (y < rec.height + rec.y) {
                g2.drawLine(rec.x, y, rec.x + rec.width, y);
                ++y;
            }
        }
    }
    
    protected void paintText(final Graphics2D g, final Rectangle textRect) {
        g.setColor(this.getForeground());
        final String text = this.getText();
        final Graphics2D g2d = g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        final FontMetrics fm = g2d.getFontMetrics();
        final Rectangle2D r = fm.getStringBounds(text, g2d);
        g.setFont(this.getFont());
        final int x = (this.getWidth() - (int)r.getWidth()) / 2;
        final int y = (this.getHeight() - (int)r.getHeight()) / 2 + fm.getAscent();
        g2d.drawString(text, x, y);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    }
}
