// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import javax.swing.border.Border;
import java.awt.Component;
import javax.swing.JScrollPane;

public class ScrollPane extends JScrollPane
{
    private static final boolean DEFAULT_BORDER = false;
    
    public ScrollPane(final Component view, final ScrollBarPolicy vertical, final ScrollBarPolicy horizontal, final boolean border) {
        super(view);
        this.setOpaque(false);
        this.getViewport().setOpaque(false);
        if (!border) {
            this.setBorder(null);
        }
        this.setVBPolicy(vertical);
        this.setHBPolicy(horizontal);
    }
    
    public ScrollPane(final Component view, final ScrollBarPolicy vertical, final ScrollBarPolicy horizontal) {
        this(view, vertical, horizontal, false);
    }
    
    public ScrollPane(final Component view, final ScrollBarPolicy generalPolicy, final boolean border) {
        this(view, generalPolicy, generalPolicy, border);
    }
    
    public ScrollPane(final Component view, final ScrollBarPolicy generalPolicy) {
        this(view, generalPolicy, generalPolicy);
    }
    
    public ScrollPane(final Component view, final boolean border) {
        this(view, ScrollBarPolicy.AS_NEEDED, border);
    }
    
    public ScrollPane(final Component view) {
        this(view, ScrollBarPolicy.AS_NEEDED);
    }
    
    public void setVerticalScrollBarPolicy(final ScrollBarPolicy policy) {
        int i_policy = 0;
        switch (policy) {
            case ALWAYS: {
                i_policy = 22;
                break;
            }
            case AS_NEEDED: {
                i_policy = 20;
                break;
            }
            case NEVER: {
                i_policy = 21;
                break;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
        super.setVerticalScrollBarPolicy(i_policy);
    }
    
    public void setHorizontalScrollBarPolicy(final ScrollBarPolicy policy) {
        int i_policy = 0;
        switch (policy) {
            case ALWAYS: {
                i_policy = 32;
                break;
            }
            case AS_NEEDED: {
                i_policy = 30;
                break;
            }
            case NEVER: {
                i_policy = 31;
                break;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
        super.setHorizontalScrollBarPolicy(i_policy);
    }
    
    public void setVBPolicy(final ScrollBarPolicy policy) {
        this.setVerticalScrollBarPolicy(policy);
    }
    
    public void setHBPolicy(final ScrollBarPolicy policy) {
        this.setHorizontalScrollBarPolicy(policy);
    }
    
    public enum ScrollBarPolicy
    {
        ALWAYS, 
        AS_NEEDED, 
        NEVER;
    }
}
