// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.util;

public enum Orientation
{
    TOP(1), 
    LEFT(2), 
    BOTTOM(3), 
    RIGHT(4), 
    CENTER(0);
    
    private final int swingAlias;
    
    private Orientation(final int swingAlias) {
        this.swingAlias = swingAlias;
    }
    
    public int getSwingAlias() {
        return this.swingAlias;
    }
    
    public static Orientation fromSwingConstant(final int orientation) {
        for (final Orientation current : values()) {
            if (orientation == current.getSwingAlias()) {
                return current;
            }
        }
        return null;
    }
}
