// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.util;

import java.io.File;

public interface FileGetter
{
    File getFile();
}
