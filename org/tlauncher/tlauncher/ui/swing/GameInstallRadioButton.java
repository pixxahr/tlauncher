// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.Color;

public class GameInstallRadioButton extends GameRadioTextButton
{
    public GameInstallRadioButton(final String string) {
        super(string);
        this.defaultColor = Color.WHITE;
    }
    
    @Override
    protected void paintBackground(final Graphics2D g2, final Rectangle rec) {
        if (this.getModel().isSelected()) {
            g2.setColor(this.defaultColor);
        }
        else {
            g2.setColor(this.getBackground());
        }
        g2.fillRect(rec.x, rec.y, rec.width, rec.height);
    }
}
