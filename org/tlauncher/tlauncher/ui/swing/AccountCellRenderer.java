// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JList;
import java.awt.Color;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import javax.swing.ListCellRenderer;

public class AccountCellRenderer implements ListCellRenderer<Account>
{
    public static final Account EMPTY;
    public static final Account MANAGE;
    private static final Icon MANAGE_ICON;
    private static final Icon MOJANG_USER_ICON;
    private static final Icon TLAUNCHER_USER_ICON;
    private final DefaultListCellRenderer defaultRenderer;
    private AccountCellType type;
    private static final Color FOREGROUND_EDITOR;
    public static final Color DARK_COLOR_TEXT;
    public static final Color OVER_ITEM;
    
    public AccountCellRenderer(final AccountCellType type) {
        if (type == null) {
            throw new NullPointerException("CellType cannot be NULL!");
        }
        this.defaultRenderer = new DefaultListCellRenderer();
        this.type = type;
    }
    
    public AccountCellRenderer() {
        this(AccountCellType.PREVIEW);
    }
    
    public AccountCellType getType() {
        return this.type;
    }
    
    public void setType(final AccountCellType type) {
        if (type == null) {
            throw new NullPointerException("CellType cannot be NULL!");
        }
        this.type = type;
    }
    
    @Override
    public Component getListCellRendererComponent(final JList<? extends Account> list, final Account value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        final JLabel renderer = (JLabel)this.defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (isSelected) {
            renderer.setBackground(AccountCellRenderer.OVER_ITEM);
        }
        else {
            renderer.setBackground(Color.white);
        }
        renderer.setForeground(AccountCellRenderer.DARK_COLOR_TEXT);
        renderer.setAlignmentY(0.5f);
        renderer.setIconTextGap(7);
        if (value == null || value.equals(AccountCellRenderer.EMPTY)) {
            renderer.setText(Localizable.get("account.empty"));
        }
        else if (value.equals(AccountCellRenderer.MANAGE)) {
            renderer.setText(Localizable.get("account.manage"));
            renderer.setIcon(AccountCellRenderer.MANAGE_ICON);
        }
        else {
            Icon icon = null;
            switch (value.getType()) {
                case TLAUNCHER: {
                    icon = AccountCellRenderer.TLAUNCHER_USER_ICON;
                    break;
                }
                case MOJANG: {
                    icon = AccountCellRenderer.MOJANG_USER_ICON;
                    break;
                }
            }
            if (icon != null) {
                renderer.setIcon(icon);
                renderer.setFont(renderer.getFont().deriveFont(1));
            }
            switch (this.type) {
                case EDITOR: {
                    this.configEditLabel(renderer, isSelected);
                    if (!value.hasUsername()) {
                        renderer.setText(Localizable.get("account.creating"));
                        break;
                    }
                    renderer.setText(value.getUsername());
                    break;
                }
                default: {
                    this.configEditLabel(renderer, isSelected);
                    renderer.setText(value.getDisplayName());
                    break;
                }
            }
        }
        renderer.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 0));
        renderer.setOpaque(true);
        return renderer;
    }
    
    private void configEditLabel(final JLabel renderer, final boolean isSelected) {
        renderer.setFont(renderer.getFont().deriveFont(0, 12.0f));
        renderer.setForeground(AccountCellRenderer.FOREGROUND_EDITOR);
        renderer.setBorder(BorderFactory.createEmptyBorder(1, 0, 1, 0));
        if (isSelected) {
            renderer.setBackground(new Color(235, 235, 235));
        }
    }
    
    static {
        EMPTY = Account.randomAccount();
        MANAGE = Account.randomAccount();
        MANAGE_ICON = (Icon)ImageCache.getIcon("gear.png");
        MOJANG_USER_ICON = (Icon)ImageCache.getIcon("mojang-user.png");
        TLAUNCHER_USER_ICON = (Icon)ImageCache.getIcon("tlauncher-user.png");
        FOREGROUND_EDITOR = new Color(74, 74, 73);
        DARK_COLOR_TEXT = new Color(77, 77, 77);
        OVER_ITEM = new Color(235, 235, 235);
    }
    
    public enum AccountCellType
    {
        PREVIEW, 
        EDITOR;
    }
}
