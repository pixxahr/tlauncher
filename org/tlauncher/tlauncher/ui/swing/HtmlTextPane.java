// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import javax.swing.BorderFactory;
import java.awt.Component;
import org.launcher.resource.TlauncherResource;
import javax.swing.text.html.StyleSheet;
import javax.swing.JScrollPane;
import java.net.URL;
import org.tlauncher.util.OS;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.Insets;
import javax.swing.JEditorPane;

public class HtmlTextPane extends JEditorPane
{
    private static final HtmlTextPane HTML_TEXT_PANE;
    private static final HtmlTextPane HTML_TEXT_PANE_WIDTH;
    
    private HtmlTextPane(final String type, final String text) {
        super(type, text);
        this.getDocument().putProperty("IgnoreCharsetDirective", Boolean.TRUE);
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setEditable(false);
        this.setOpaque(false);
        final HTMLEditorKit kit = new HTMLEditorKit();
        this.setEditorKit(kit);
        URL url;
        this.addHyperlinkListener(e -> {
            if (!(!e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED))) {
                url = e.getURL();
                if (url != null) {
                    OS.openLink(url);
                }
            }
        });
    }
    
    public static HtmlTextPane get(final String text) {
        HtmlTextPane.HTML_TEXT_PANE.setText(text);
        return HtmlTextPane.HTML_TEXT_PANE;
    }
    
    public static HtmlTextPane get(final String text, final int width) {
        final HTMLEditorKit kit = (HTMLEditorKit)HtmlTextPane.HTML_TEXT_PANE_WIDTH.getEditorKit();
        kit.getStyleSheet().addRule(String.format("body {width:%spx;}", width));
        kit.getStyleSheet().addRule("a { text-decoration: underline; color: #147de0;}");
        HtmlTextPane.HTML_TEXT_PANE_WIDTH.setText(text);
        return HtmlTextPane.HTML_TEXT_PANE_WIDTH;
    }
    
    public static JScrollPane createNew(final String text, final int width) {
        final HtmlTextPane pane = new HtmlTextPane("text/html", "");
        pane.setText(text);
        final HTMLEditorKit kit = (HTMLEditorKit)pane.getEditorKit();
        final StyleSheet ss = new StyleSheet();
        ss.importStyleSheet(TlauncherResource.getResource("updater.css"));
        kit.getStyleSheet().addStyleSheet(ss);
        return wrap(pane);
    }
    
    private static JScrollPane wrap(final HtmlTextPane pane) {
        final JScrollPane jScrollPane = new JScrollPane(pane, 21, 31);
        jScrollPane.getViewport().setOpaque(false);
        jScrollPane.setOpaque(false);
        jScrollPane.setBorder(BorderFactory.createEmptyBorder());
        return jScrollPane;
    }
    
    static {
        HTML_TEXT_PANE = new HtmlTextPane("text/html", "");
        HTML_TEXT_PANE_WIDTH = new HtmlTextPane("text/html", "");
    }
}
