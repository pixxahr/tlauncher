// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import javax.swing.BorderFactory;
import javax.swing.text.Highlighter;
import javax.swing.JTextArea;

public class TextWrapperLabel extends JTextArea
{
    public TextWrapperLabel(final String text) {
        super(text);
        this.setLineWrap(true);
        this.setWrapStyleWord(true);
        this.setEditable(false);
        this.setHighlighter(null);
        this.setOpaque(false);
        this.setBorder(BorderFactory.createEmptyBorder());
    }
}
