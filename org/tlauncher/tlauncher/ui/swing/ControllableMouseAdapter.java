// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ControllableMouseAdapter implements MouseListener
{
    private MouseEventHandler click;
    private MouseEventHandler press;
    private MouseEventHandler release;
    private MouseEventHandler enter;
    private MouseEventHandler exit;
    
    public MouseEventHandler getOnClick() {
        return this.click;
    }
    
    public ControllableMouseAdapter setOnClick(final MouseEventHandler handler) {
        this.click = handler;
        return this;
    }
    
    public MouseEventHandler getOnPress() {
        return this.press;
    }
    
    public ControllableMouseAdapter setOnPress(final MouseEventHandler handler) {
        this.press = handler;
        return this;
    }
    
    public MouseEventHandler getOnRelease() {
        return this.release;
    }
    
    public ControllableMouseAdapter setOnRelease(final MouseEventHandler handler) {
        this.release = handler;
        return this;
    }
    
    public MouseEventHandler getOnEnter() {
        return this.enter;
    }
    
    public ControllableMouseAdapter setOnEnter(final MouseEventHandler handler) {
        this.enter = handler;
        return this;
    }
    
    public MouseEventHandler getOnExit() {
        return this.exit;
    }
    
    public ControllableMouseAdapter setOnExit(final MouseEventHandler handler) {
        this.exit = handler;
        return this;
    }
    
    @Override
    public final void mouseClicked(final MouseEvent e) {
        if (this.click != null) {
            this.click.handleEvent(e);
        }
    }
    
    @Override
    public final void mousePressed(final MouseEvent e) {
        if (this.press != null) {
            this.press.handleEvent(e);
        }
    }
    
    @Override
    public final void mouseReleased(final MouseEvent e) {
        if (this.release != null) {
            this.release.handleEvent(e);
        }
    }
    
    @Override
    public final void mouseEntered(final MouseEvent e) {
        if (this.enter != null) {
            this.enter.handleEvent(e);
        }
    }
    
    @Override
    public final void mouseExited(final MouseEvent e) {
        if (this.exit != null) {
            this.exit.handleEvent(e);
        }
    }
    
    public interface MouseEventHandler
    {
        void handleEvent(final MouseEvent p0);
    }
}
