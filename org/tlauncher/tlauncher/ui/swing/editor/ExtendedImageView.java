// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.editor;

import javax.swing.text.GlyphView;
import javax.swing.text.Segment;
import javax.swing.text.html.InlineView;
import javax.swing.text.Document;
import javax.swing.text.AbstractDocument;
import javax.swing.SwingUtilities;
import java.net.URL;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.util.Dictionary;
import org.tlauncher.util.U;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import javax.xml.bind.DatatypeConverter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.Highlighter;
import javax.swing.text.LayeredHighlighter;
import javax.swing.text.JTextComponent;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.text.ViewFactory;
import javax.swing.event.DocumentEvent;
import javax.swing.text.StyledDocument;
import java.awt.Shape;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;
import javax.swing.GrayFilter;
import javax.swing.UIManager;
import javax.swing.Icon;
import javax.swing.text.html.HTML;
import javax.swing.text.Element;
import java.awt.image.ImageObserver;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Container;
import java.awt.Image;
import javax.swing.text.AttributeSet;
import javax.swing.text.View;

public class ExtendedImageView extends View
{
    private static String base64s;
    private static String base64e;
    private static boolean sIsInc;
    private static int sIncRate;
    private static final String PENDING_IMAGE = "html.pendingImage";
    private static final String MISSING_IMAGE = "html.missingImage";
    private static final String IMAGE_CACHE_PROPERTY = "imageCache";
    private static final int DEFAULT_WIDTH = 38;
    private static final int DEFAULT_HEIGHT = 38;
    private static final int LOADING_FLAG = 1;
    private static final int LINK_FLAG = 2;
    private static final int WIDTH_FLAG = 4;
    private static final int HEIGHT_FLAG = 8;
    private static final int RELOAD_FLAG = 16;
    private static final int RELOAD_IMAGE_FLAG = 32;
    private static final int SYNC_LOAD_FLAG = 64;
    private AttributeSet attr;
    private Image image;
    private Image disabledImage;
    private int width;
    private int height;
    private int state;
    private Container container;
    private Rectangle fBounds;
    private Color borderColor;
    private short borderSize;
    private short leftInset;
    private short rightInset;
    private short topInset;
    private short bottomInset;
    private ImageObserver imageObserver;
    private View altView;
    private float vAlign;
    
    public ExtendedImageView(final Element elem) {
        super(elem);
        this.fBounds = new Rectangle();
        this.imageObserver = new ImageHandler();
        this.state = 48;
    }
    
    public String getAltText() {
        return (String)this.getElement().getAttributes().getAttribute(HTML.Attribute.ALT);
    }
    
    public String getImageSource() {
        return (String)this.getElement().getAttributes().getAttribute(HTML.Attribute.SRC);
    }
    
    public Icon getNoImageIcon() {
        return (Icon)UIManager.getLookAndFeelDefaults().get("html.missingImage");
    }
    
    public Icon getLoadingImageIcon() {
        return (Icon)UIManager.getLookAndFeelDefaults().get("html.pendingImage");
    }
    
    public Image getImage() {
        this.sync();
        return this.image;
    }
    
    private Image getImage(final boolean enabled) {
        Image img = this.getImage();
        if (!enabled) {
            if (this.disabledImage == null) {
                this.disabledImage = GrayFilter.createDisabledImage(img);
            }
            img = this.disabledImage;
        }
        return img;
    }
    
    public void setLoadsSynchronously(final boolean newValue) {
        synchronized (this) {
            if (newValue) {
                this.state |= 0x40;
            }
            else {
                this.state = ((this.state | 0x40) ^ 0x40);
            }
        }
    }
    
    public boolean getLoadsSynchronously() {
        return (this.state & 0x40) != 0x0;
    }
    
    protected StyleSheet getStyleSheet() {
        final HTMLDocument doc = (HTMLDocument)this.getDocument();
        return doc.getStyleSheet();
    }
    
    @Override
    public AttributeSet getAttributes() {
        this.sync();
        return this.attr;
    }
    
    @Override
    public String getToolTipText(final float x, final float y, final Shape allocation) {
        return this.getAltText();
    }
    
    protected void setPropertiesFromAttributes() {
        final StyleSheet sheet = this.getStyleSheet();
        this.attr = sheet.getViewAttributes(this);
        this.borderSize = (short)this.getIntAttr(HTML.Attribute.BORDER, 0);
        final short n = (short)(this.getIntAttr(HTML.Attribute.HSPACE, 0) + this.borderSize);
        this.rightInset = n;
        this.leftInset = n;
        final short n2 = (short)(this.getIntAttr(HTML.Attribute.VSPACE, 0) + this.borderSize);
        this.bottomInset = n2;
        this.topInset = n2;
        this.borderColor = ((StyledDocument)this.getDocument()).getForeground(this.getAttributes());
        final AttributeSet attr = this.getElement().getAttributes();
        Object alignment = attr.getAttribute(HTML.Attribute.ALIGN);
        this.vAlign = 1.0f;
        if (alignment != null) {
            alignment = alignment.toString();
            if ("top".equals(alignment)) {
                this.vAlign = 0.0f;
            }
            else if ("middle".equals(alignment)) {
                this.vAlign = 0.5f;
            }
        }
        final AttributeSet anchorAttr = (AttributeSet)attr.getAttribute(HTML.Tag.A);
        if (anchorAttr != null && anchorAttr.isDefined(HTML.Attribute.HREF)) {
            synchronized (this) {
                this.state |= 0x2;
            }
        }
        else {
            synchronized (this) {
                this.state = ((this.state | 0x2) ^ 0x2);
            }
        }
    }
    
    @Override
    public void setParent(final View parent) {
        final View oldParent = this.getParent();
        super.setParent(parent);
        this.container = ((parent != null) ? this.getContainer() : null);
        if (oldParent != parent) {
            synchronized (this) {
                this.state |= 0x10;
            }
        }
    }
    
    @Override
    public void changedUpdate(final DocumentEvent e, final Shape a, final ViewFactory f) {
        super.changedUpdate(e, a, f);
        synchronized (this) {
            this.state |= 0x30;
        }
        this.preferenceChanged(null, true, true);
    }
    
    @Override
    public void paint(final Graphics g, final Shape a) {
        this.sync();
        final Rectangle rect = (Rectangle)((a instanceof Rectangle) ? a : a.getBounds());
        final Rectangle clip = g.getClipBounds();
        this.fBounds.setBounds(rect);
        this.paintHighlights(g, a);
        this.paintBorder(g, rect);
        if (clip != null) {
            g.clipRect(rect.x + this.leftInset, rect.y + this.topInset, rect.width - this.leftInset - this.rightInset, rect.height - this.topInset - this.bottomInset);
        }
        final Container host = this.getContainer();
        final Image img = this.getImage(host == null || host.isEnabled());
        if (img != null) {
            if (!this.hasPixels(img)) {
                final Icon icon = this.getLoadingImageIcon();
                if (icon != null) {
                    icon.paintIcon(host, g, rect.x + this.leftInset, rect.y + this.topInset);
                }
            }
            else {
                g.drawImage(img, rect.x + this.leftInset, rect.y + this.topInset, this.width, this.height, this.imageObserver);
            }
        }
        else {
            final Icon icon = this.getNoImageIcon();
            if (icon != null) {
                icon.paintIcon(host, g, rect.x + this.leftInset, rect.y + this.topInset);
            }
            final View view = this.getAltView();
            if (view != null && ((this.state & 0x4) == 0x0 || this.width > 38)) {
                final Rectangle altRect = new Rectangle(rect.x + this.leftInset + 38, rect.y + this.topInset, rect.width - this.leftInset - this.rightInset - 38, rect.height - this.topInset - this.bottomInset);
                view.paint(g, altRect);
            }
        }
        if (clip != null) {
            g.setClip(clip.x, clip.y, clip.width, clip.height);
        }
    }
    
    private void paintHighlights(final Graphics g, final Shape shape) {
        if (this.container instanceof JTextComponent) {
            final JTextComponent tc = (JTextComponent)this.container;
            final Highlighter h = tc.getHighlighter();
            if (h instanceof LayeredHighlighter) {
                ((LayeredHighlighter)h).paintLayeredHighlights(g, this.getStartOffset(), this.getEndOffset(), shape, tc, this);
            }
        }
    }
    
    private void paintBorder(final Graphics g, final Rectangle rect) {
        final Color color = this.borderColor;
        if ((this.borderSize > 0 || this.image == null) && color != null) {
            final int xOffset = this.leftInset - this.borderSize;
            final int yOffset = this.topInset - this.borderSize;
            g.setColor(color);
            for (int n = (this.image == null) ? 1 : this.borderSize, counter = 0; counter < n; ++counter) {
                g.drawRect(rect.x + xOffset + counter, rect.y + yOffset + counter, rect.width - counter - counter - xOffset - xOffset - 1, rect.height - counter - counter - yOffset - yOffset - 1);
            }
        }
    }
    
    @Override
    public float getPreferredSpan(final int axis) {
        this.sync();
        if (axis == 0 && (this.state & 0x4) == 0x4) {
            this.getPreferredSpanFromAltView(axis);
            return (float)(this.width + this.leftInset + this.rightInset);
        }
        if (axis == 1 && (this.state & 0x8) == 0x8) {
            this.getPreferredSpanFromAltView(axis);
            return (float)(this.height + this.topInset + this.bottomInset);
        }
        final Image image = this.getImage();
        if (image != null) {
            switch (axis) {
                case 0: {
                    return (float)(this.width + this.leftInset + this.rightInset);
                }
                case 1: {
                    return (float)(this.height + this.topInset + this.bottomInset);
                }
                default: {
                    throw new IllegalArgumentException("Invalid axis: " + axis);
                }
            }
        }
        else {
            final View view = this.getAltView();
            float retValue = 0.0f;
            if (view != null) {
                retValue = view.getPreferredSpan(axis);
            }
            switch (axis) {
                case 0: {
                    return retValue + (this.width + this.leftInset + this.rightInset);
                }
                case 1: {
                    return retValue + (this.height + this.topInset + this.bottomInset);
                }
                default: {
                    throw new IllegalArgumentException("Invalid axis: " + axis);
                }
            }
        }
    }
    
    @Override
    public float getAlignment(final int axis) {
        switch (axis) {
            case 1: {
                return this.vAlign;
            }
            default: {
                return super.getAlignment(axis);
            }
        }
    }
    
    @Override
    public Shape modelToView(final int pos, final Shape a, final Position.Bias b) throws BadLocationException {
        final int p0 = this.getStartOffset();
        final int p2 = this.getEndOffset();
        if (pos >= p0 && pos <= p2) {
            final Rectangle r = a.getBounds();
            if (pos == p2) {
                final Rectangle rectangle = r;
                rectangle.x += r.width;
            }
            r.width = 0;
            return r;
        }
        return null;
    }
    
    @Override
    public int viewToModel(final float x, final float y, final Shape a, final Position.Bias[] bias) {
        final Rectangle alloc = (Rectangle)a;
        if (x < alloc.x + alloc.width) {
            bias[0] = Position.Bias.Forward;
            return this.getStartOffset();
        }
        bias[0] = Position.Bias.Backward;
        return this.getEndOffset();
    }
    
    @Override
    public void setSize(final float width, final float height) {
        this.sync();
        if (this.getImage() == null) {
            final View view = this.getAltView();
            if (view != null) {
                view.setSize(Math.max(0.0f, width - (38 + this.leftInset + this.rightInset)), Math.max(0.0f, height - (this.topInset + this.bottomInset)));
            }
        }
    }
    
    private boolean hasPixels(final Image image) {
        return image != null && image.getHeight(this.imageObserver) > 0 && image.getWidth(this.imageObserver) > 0;
    }
    
    private float getPreferredSpanFromAltView(final int axis) {
        if (this.getImage() == null) {
            final View view = this.getAltView();
            if (view != null) {
                return view.getPreferredSpan(axis);
            }
        }
        return 0.0f;
    }
    
    private void repaint(final long delay) {
        if (this.container != null && this.fBounds != null) {
            this.container.repaint(delay, this.fBounds.x, this.fBounds.y, this.fBounds.width, this.fBounds.height);
        }
    }
    
    private int getIntAttr(final HTML.Attribute name, final int deflt) {
        final AttributeSet attr = this.getElement().getAttributes();
        if (attr.isDefined(name)) {
            final String val = (String)attr.getAttribute(name);
            int i;
            if (val == null) {
                i = deflt;
            }
            else {
                try {
                    i = Math.max(0, Integer.parseInt(val));
                }
                catch (NumberFormatException x) {
                    i = deflt;
                }
            }
            return i;
        }
        return deflt;
    }
    
    private void sync() {
        int s = this.state;
        if ((s & 0x20) != 0x0) {
            this.refreshImage();
        }
        s = this.state;
        if ((s & 0x10) != 0x0) {
            synchronized (this) {
                this.state = ((this.state | 0x10) ^ 0x10);
            }
            this.setPropertiesFromAttributes();
        }
    }
    
    private void refreshImage() {
        synchronized (this) {
            this.state = ((this.state | 0x1 | 0x20 | 0x4 | 0x8) ^ 0x2C);
            this.image = null;
            final int n = 0;
            this.height = n;
            this.width = n;
        }
        try {
            this.loadImage();
            this.updateImageSize();
        }
        finally {
            synchronized (this) {
                this.state = ((this.state | 0x1) ^ 0x1);
            }
        }
    }
    
    private void loadImage() {
        try {
            this.image = this.loadNewImage();
        }
        catch (Exception e) {
            this.image = null;
            e.printStackTrace();
        }
    }
    
    private Image loadNewImage() throws Exception {
        final String source = this.getImageSource();
        if (source == null) {
            return null;
        }
        if (source.startsWith(ExtendedImageView.base64s)) {
            int startPoint = ExtendedImageView.base64s.length();
            final String imageType = source.substring(startPoint, startPoint + 4);
            if (imageType.startsWith("png") || imageType.startsWith("jpg")) {
                startPoint += 3;
            }
            else {
                if (!imageType.equals("jpeg")) {
                    return null;
                }
                startPoint += 4;
            }
            if (!source.substring(startPoint, startPoint + ExtendedImageView.base64e.length()).equals(ExtendedImageView.base64e)) {
                return null;
            }
            startPoint += ExtendedImageView.base64e.length();
            final byte[] bytes = DatatypeConverter.parseBase64Binary(source.substring(startPoint));
            return ImageIO.read(new ByteArrayInputStream(bytes));
        }
        else {
            final URL src = U.makeURL(source);
            if (src == null) {
                return null;
            }
            Image newImage = null;
            final Dictionary<?, ?> cache = (Dictionary<?, ?>)this.getDocument().getProperty("imageCache");
            if (cache != null) {
                newImage = (Image)cache.get(src);
            }
            else {
                newImage = Toolkit.getDefaultToolkit().createImage(src);
                if (newImage != null && this.getLoadsSynchronously()) {
                    final ImageIcon ii = new ImageIcon();
                    ii.setImage(newImage);
                }
            }
            return newImage;
        }
    }
    
    private void updateImageSize() {
        int newWidth = 0;
        int newHeight = 0;
        int newState = 0;
        final Image newImage = this.getImage();
        if (newImage != null) {
            newWidth = this.getIntAttr(HTML.Attribute.WIDTH, -1);
            if (newWidth > 0) {
                newState |= 0x4;
            }
            newHeight = this.getIntAttr(HTML.Attribute.HEIGHT, -1);
            if (newHeight > 0) {
                newState |= 0x8;
            }
            if (newWidth <= 0) {
                newWidth = newImage.getWidth(this.imageObserver);
                if (newWidth <= 0) {
                    newWidth = 38;
                }
            }
            if (newHeight <= 0) {
                newHeight = newImage.getHeight(this.imageObserver);
                if (newHeight <= 0) {
                    newHeight = 38;
                }
            }
            if ((newState & 0xC) != 0x0) {
                Toolkit.getDefaultToolkit().prepareImage(newImage, newWidth, newHeight, this.imageObserver);
            }
            else {
                Toolkit.getDefaultToolkit().prepareImage(newImage, -1, -1, this.imageObserver);
            }
            boolean createText = false;
            synchronized (this) {
                if (this.image != null) {
                    if ((newState & 0x4) == 0x4 || this.width == 0) {
                        this.width = newWidth;
                    }
                    if ((newState & 0x8) == 0x8 || this.height == 0) {
                        this.height = newHeight;
                    }
                }
                else {
                    createText = true;
                    if ((newState & 0x4) == 0x4) {
                        this.width = newWidth;
                    }
                    if ((newState & 0x8) == 0x8) {
                        this.height = newHeight;
                    }
                }
                this.state |= newState;
                this.state = ((this.state | 0x1) ^ 0x1);
            }
            if (createText) {
                this.updateAltTextView();
            }
        }
        else {
            final int n = 38;
            this.height = n;
            this.width = n;
            this.updateAltTextView();
        }
    }
    
    private void updateAltTextView() {
        final String text = this.getAltText();
        if (text != null) {
            final ImageLabelView newView = new ImageLabelView(this.getElement(), text);
            synchronized (this) {
                this.altView = newView;
            }
        }
    }
    
    private View getAltView() {
        final View view;
        synchronized (this) {
            view = this.altView;
        }
        if (view != null && view.getParent() == null) {
            view.setParent(this.getParent());
        }
        return view;
    }
    
    private void safePreferenceChanged() {
        if (SwingUtilities.isEventDispatchThread()) {
            final Document doc = this.getDocument();
            if (doc instanceof AbstractDocument) {
                ((AbstractDocument)doc).readLock();
            }
            this.preferenceChanged(null, true, true);
            if (doc instanceof AbstractDocument) {
                ((AbstractDocument)doc).readUnlock();
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ExtendedImageView.this.safePreferenceChanged();
                }
            });
        }
    }
    
    static {
        ExtendedImageView.base64s = "data:image/";
        ExtendedImageView.base64e = ";base64,";
        ExtendedImageView.sIsInc = false;
        ExtendedImageView.sIncRate = 100;
    }
    
    private class ImageHandler implements ImageObserver
    {
        @Override
        public boolean imageUpdate(final Image img, final int flags, final int x, final int y, final int newWidth, final int newHeight) {
            if ((img != ExtendedImageView.this.image && img != ExtendedImageView.this.disabledImage) || ExtendedImageView.this.image == null || ExtendedImageView.this.getParent() == null) {
                return false;
            }
            if ((flags & 0xC0) != 0x0) {
                ExtendedImageView.this.repaint(0L);
                synchronized (ExtendedImageView.this) {
                    if (ExtendedImageView.this.image == img) {
                        ExtendedImageView.this.image = null;
                        if ((ExtendedImageView.this.state & 0x4) != 0x4) {
                            ExtendedImageView.this.width = 38;
                        }
                        if ((ExtendedImageView.this.state & 0x8) != 0x8) {
                            ExtendedImageView.this.height = 38;
                        }
                    }
                    else {
                        ExtendedImageView.this.disabledImage = null;
                    }
                    if ((ExtendedImageView.this.state & 0x1) == 0x1) {
                        return false;
                    }
                }
                ExtendedImageView.this.updateAltTextView();
                ExtendedImageView.this.safePreferenceChanged();
                return false;
            }
            if (ExtendedImageView.this.image == img) {
                short changed = 0;
                if ((flags & 0x2) != 0x0 && !ExtendedImageView.this.getElement().getAttributes().isDefined(HTML.Attribute.HEIGHT)) {
                    changed |= 0x1;
                }
                if ((flags & 0x1) != 0x0 && !ExtendedImageView.this.getElement().getAttributes().isDefined(HTML.Attribute.WIDTH)) {
                    changed |= 0x2;
                }
                synchronized (ExtendedImageView.this) {
                    if ((changed & 0x1) == 0x1 && (ExtendedImageView.this.state & 0x4) == 0x0) {
                        ExtendedImageView.this.width = newWidth;
                    }
                    if ((changed & 0x2) == 0x2 && (ExtendedImageView.this.state & 0x8) == 0x0) {
                        ExtendedImageView.this.height = newHeight;
                    }
                    if ((ExtendedImageView.this.state & 0x1) == 0x1) {
                        return true;
                    }
                }
                if (changed != 0) {
                    ExtendedImageView.this.safePreferenceChanged();
                    return true;
                }
            }
            if ((flags & 0x30) != 0x0) {
                ExtendedImageView.this.repaint(0L);
            }
            else if ((flags & 0x8) != 0x0 && ExtendedImageView.sIsInc) {
                ExtendedImageView.this.repaint(ExtendedImageView.sIncRate);
            }
            return (flags & 0x20) == 0x0;
        }
    }
    
    private class ImageLabelView extends InlineView
    {
        private Segment segment;
        private Color fg;
        
        ImageLabelView(final Element e, final String text) {
            super(e);
            this.reset(text);
        }
        
        public void reset(final String text) {
            this.segment = new Segment(text.toCharArray(), 0, text.length());
        }
        
        @Override
        public void paint(final Graphics g, final Shape a) {
            final GlyphPainter painter = this.getGlyphPainter();
            if (painter != null) {
                g.setColor(this.getForeground());
                painter.paint(this, g, a, this.getStartOffset(), this.getEndOffset());
            }
        }
        
        @Override
        public Segment getText(final int p0, final int p1) {
            if (p0 < 0 || p1 > this.segment.array.length) {
                throw new RuntimeException("ImageLabelView: Stale view");
            }
            this.segment.offset = p0;
            this.segment.count = p1 - p0;
            return this.segment;
        }
        
        @Override
        public int getStartOffset() {
            return 0;
        }
        
        @Override
        public int getEndOffset() {
            return this.segment.array.length;
        }
        
        @Override
        public View breakView(final int axis, final int p0, final float pos, final float len) {
            return this;
        }
        
        @Override
        public Color getForeground() {
            final View parent;
            if (this.fg == null && (parent = this.getParent()) != null) {
                final Document doc = this.getDocument();
                final AttributeSet attr = parent.getAttributes();
                if (attr != null && doc instanceof StyledDocument) {
                    this.fg = ((StyledDocument)doc).getForeground(attr);
                }
            }
            return this.fg;
        }
    }
}
