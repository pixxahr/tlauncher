// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.editor;

import java.io.IOException;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.net.URL;
import org.tlauncher.util.OS;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.EditorKit;
import java.awt.Insets;
import javax.swing.text.html.StyleSheet;
import java.awt.Font;
import javax.swing.JEditorPane;

public class EditorPane extends JEditorPane
{
    private static final long serialVersionUID = -2857352867725574106L;
    
    public EditorPane(Font font) {
        if (font != null) {
            this.setFont(font);
        }
        else {
            font = this.getFont();
        }
        final StyleSheet css = new StyleSheet();
        css.importStyleSheet(this.getClass().getResource("styles.css"));
        css.addRule("body { font-family: " + font.getFamily() + "; font-size: " + font.getSize() + "pt; } " + "a { text-decoration: underline; }");
        final ExtendedHTMLEditorKit html = new ExtendedHTMLEditorKit();
        html.setStyleSheet(css);
        this.getDocument().putProperty("IgnoreCharsetDirective", Boolean.TRUE);
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setEditorKit(html);
        this.setEditable(false);
        this.setOpaque(false);
        this.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(final HyperlinkEvent e) {
                if (!e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                    return;
                }
                final URL url = e.getURL();
                if (url == null) {
                    return;
                }
                OS.openLink(url);
            }
        });
    }
    
    public EditorPane() {
        this(new LocalizableLabel().getFont());
    }
    
    public EditorPane(final URL initialPage) throws IOException {
        this();
        this.setPage(initialPage);
    }
    
    public EditorPane(final String url) throws IOException {
        this();
        this.setPage(url);
    }
    
    public EditorPane(final String type, final String text) {
        this();
        this.setContentType(type);
        this.setText(text);
    }
}
