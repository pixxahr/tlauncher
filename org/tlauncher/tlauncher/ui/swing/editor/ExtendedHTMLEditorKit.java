// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.editor;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import javax.swing.text.View;
import org.tlauncher.util.OS;
import java.net.URISyntaxException;
import java.net.URI;
import javax.swing.text.html.HTMLDocument;
import java.awt.event.MouseEvent;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.Element;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JEditorPane;
import javax.swing.text.ViewFactory;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.StringSelection;
import java.awt.Toolkit;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Cursor;
import javax.swing.text.html.HTMLEditorKit;

public class ExtendedHTMLEditorKit extends HTMLEditorKit
{
    protected static final ExtendedHTMLFactory extendedFactory;
    public static final HyperlinkProcessor defaultHyperlinkProcessor;
    protected final ExtendedLinkController linkController;
    private HyperlinkProcessor hlProc;
    private boolean processPopup;
    private static final Cursor HAND;
    private String popupHref;
    private final JPopupMenu popup;
    
    public ExtendedHTMLEditorKit() {
        this.linkController = new ExtendedLinkController();
        this.hlProc = ExtendedHTMLEditorKit.defaultHyperlinkProcessor;
        this.processPopup = true;
        this.popup = new JPopupMenu();
        final LocalizableMenuItem open = new LocalizableMenuItem("browser.hyperlink.popup.open");
        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ExtendedHTMLEditorKit.this.hlProc.process(ExtendedHTMLEditorKit.this.popupHref);
            }
        });
        this.popup.add(open);
        final LocalizableMenuItem copy = new LocalizableMenuItem("browser.hyperlink.popup.copy");
        copy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(ExtendedHTMLEditorKit.this.popupHref), null);
            }
        });
        this.popup.add(copy);
        final LocalizableMenuItem show = new LocalizableMenuItem("browser.hyperlink.popup.show");
        show.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                Alert.showLocMessage("browser.hyperlink.popup.show.alert", ExtendedHTMLEditorKit.this.popupHref);
            }
        });
        this.popup.add(show);
    }
    
    @Override
    public ViewFactory getViewFactory() {
        return ExtendedHTMLEditorKit.extendedFactory;
    }
    
    @Override
    public void install(final JEditorPane pane) {
        super.install(pane);
        for (final MouseListener listener : pane.getMouseListeners()) {
            if (listener instanceof LinkController) {
                pane.removeMouseListener(listener);
                pane.removeMouseMotionListener((MouseMotionListener)listener);
                pane.addMouseListener(this.linkController);
                pane.addMouseMotionListener(this.linkController);
            }
        }
    }
    
    public final boolean getProcessPopup() {
        return this.processPopup;
    }
    
    public final void setProcessPopup(final boolean process) {
        this.processPopup = process;
    }
    
    private static HTML.Tag getTag(final Element elem) {
        final AttributeSet attrs = elem.getAttributes();
        final Object elementName = attrs.getAttribute("$ename");
        final Object o = (elementName != null) ? null : attrs.getAttribute(StyleConstants.NameAttribute);
        return (o instanceof HTML.Tag) ? ((HTML.Tag)o) : null;
    }
    
    private static String getAnchorHref(final MouseEvent e) {
        final JEditorPane editor = (JEditorPane)e.getSource();
        if (!(editor.getDocument() instanceof HTMLDocument)) {
            return null;
        }
        final HTMLDocument hdoc = (HTMLDocument)editor.getDocument();
        final Element elem = hdoc.getCharacterElement(editor.viewToModel(e.getPoint()));
        final HTML.Tag tag = getTag(elem);
        if (tag == HTML.Tag.CONTENT) {
            final Object anchorAttr = elem.getAttributes().getAttribute(HTML.Tag.A);
            if (anchorAttr != null && anchorAttr instanceof AttributeSet) {
                final AttributeSet anchor = (AttributeSet)anchorAttr;
                final Object hrefObject = anchor.getAttribute(HTML.Attribute.HREF);
                if (hrefObject != null && hrefObject instanceof String) {
                    return (String)hrefObject;
                }
            }
        }
        return null;
    }
    
    static {
        extendedFactory = new ExtendedHTMLFactory();
        defaultHyperlinkProcessor = new HyperlinkProcessor() {
            @Override
            public void process(final String link) {
                if (link == null) {
                    return;
                }
                URI uri;
                try {
                    uri = new URI(link);
                }
                catch (URISyntaxException e) {
                    Alert.showLocError("browser.hyperlink.createScrollWrapper.error", e);
                    return;
                }
                OS.openLink(uri);
            }
        };
        HAND = Cursor.getPredefinedCursor(12);
    }
    
    public static class ExtendedHTMLFactory extends HTMLFactory
    {
        @Override
        public View create(final Element elem) {
            final HTML.Tag kind = getTag(elem);
            if (kind == HTML.Tag.IMG) {
                return new ExtendedImageView(elem);
            }
            return super.create(elem);
        }
    }
    
    public class ExtendedLinkController extends MouseAdapter
    {
        @Override
        public void mouseClicked(final MouseEvent e) {
            final JEditorPane editor = (JEditorPane)e.getSource();
            if (!editor.isEnabled() && !editor.isDisplayable()) {
                return;
            }
            final String href = getAnchorHref(e);
            if (href == null) {
                return;
            }
            switch (e.getButton()) {
                case 3: {
                    if (ExtendedHTMLEditorKit.this.processPopup) {
                        ExtendedHTMLEditorKit.this.popupHref = href;
                        ExtendedHTMLEditorKit.this.popup.show(editor, e.getX(), e.getY());
                        break;
                    }
                    break;
                }
                default: {
                    ExtendedHTMLEditorKit.this.hlProc.process(href);
                    break;
                }
            }
        }
        
        @Override
        public void mouseMoved(final MouseEvent e) {
            final JEditorPane editor = (JEditorPane)e.getSource();
            if (!editor.isEnabled() && !editor.isDisplayable()) {
                return;
            }
            editor.setCursor((getAnchorHref(e) == null) ? Cursor.getDefaultCursor() : ExtendedHTMLEditorKit.HAND);
        }
        
        @Override
        public void mouseExited(final MouseEvent e) {
            final JEditorPane editor = (JEditorPane)e.getSource();
            if (!editor.isEnabled() && !editor.isDisplayable()) {
                return;
            }
            editor.setCursor(Cursor.getDefaultCursor());
        }
    }
}
