// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.beans.PropertyChangeListener;
import javax.swing.Action;

public abstract class EmptyAction implements Action
{
    protected boolean enabled;
    
    public EmptyAction() {
        this.enabled = true;
    }
    
    @Override
    public Object getValue(final String key) {
        return null;
    }
    
    @Override
    public void putValue(final String key, final Object value) {
    }
    
    @Override
    public void setEnabled(final boolean b) {
        this.enabled = b;
    }
    
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
    
    @Override
    public void addPropertyChangeListener(final PropertyChangeListener listener) {
    }
    
    @Override
    public void removePropertyChangeListener(final PropertyChangeListener listener) {
    }
}
