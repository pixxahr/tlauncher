// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.util.Collection;
import java.util.Vector;
import javax.swing.AbstractListModel;

public class SimpleListModel<E> extends AbstractListModel<E>
{
    private static final long serialVersionUID = 727845864028652893L;
    protected final Vector<E> vector;
    
    public SimpleListModel() {
        this.vector = new Vector<E>();
    }
    
    @Override
    public int getSize() {
        return this.vector.size();
    }
    
    @Override
    public E getElementAt(final int index) {
        if (index < 0 || index >= this.getSize()) {
            return null;
        }
        return this.vector.get(index);
    }
    
    public void add(final E elem) {
        final int index = this.vector.size();
        this.vector.add(elem);
        this.fireIntervalAdded(this, index, index);
    }
    
    public boolean remove(final E elem) {
        final int index = this.indexOf(elem);
        final boolean rv = this.vector.removeElement(elem);
        if (index >= 0) {
            this.fireIntervalRemoved(this, index, index);
        }
        return rv;
    }
    
    public void addAll(final Collection<E> elem) {
        final int size = elem.size();
        if (size == 0) {
            return;
        }
        final int index0 = this.vector.size();
        final int index2 = index0 + size - 1;
        this.vector.addAll((Collection<? extends E>)elem);
        this.fireIntervalAdded(this, index0, index2);
    }
    
    public void clear() {
        final int index1 = this.vector.size() - 1;
        this.vector.clear();
        if (index1 >= 0) {
            this.fireIntervalRemoved(this, 0, index1);
        }
    }
    
    public boolean isEmpty() {
        return this.vector.isEmpty();
    }
    
    public boolean contains(final E elem) {
        return this.vector.contains(elem);
    }
    
    public int indexOf(final E elem) {
        return this.vector.indexOf(elem);
    }
    
    public int indexOf(final E elem, final int index) {
        return this.vector.indexOf(elem, index);
    }
    
    public E elementAt(final int index) {
        return this.vector.elementAt(index);
    }
    
    @Override
    public String toString() {
        return this.vector.toString();
    }
}
