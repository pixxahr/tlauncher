// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JList;
import org.tlauncher.tlauncher.ui.converter.StringConverter;
import java.awt.Color;
import javax.swing.DefaultListCellRenderer;

public class DefaultConverterCellRenderer<T> extends ConverterCellRenderer<T>
{
    private final DefaultListCellRenderer defaultRenderer;
    public static final Color DARK_COLOR_TEXT;
    public static final Color OVER_ITEM;
    
    public DefaultConverterCellRenderer(final StringConverter<T> converter) {
        super(converter);
        this.defaultRenderer = new DefaultListCellRenderer();
    }
    
    @Override
    public Component getListCellRendererComponent(final JList<? extends T> list, final T value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        final JLabel renderer = (JLabel)this.defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (isSelected) {
            renderer.setBackground(DefaultConverterCellRenderer.OVER_ITEM);
        }
        else {
            renderer.setBackground(Color.white);
        }
        renderer.setForeground(DefaultConverterCellRenderer.DARK_COLOR_TEXT);
        renderer.setOpaque(true);
        renderer.setText(this.converter.toString(value));
        renderer.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 0));
        return renderer;
    }
    
    static {
        DARK_COLOR_TEXT = new Color(77, 77, 77);
        OVER_ITEM = new Color(235, 235, 235);
    }
}
