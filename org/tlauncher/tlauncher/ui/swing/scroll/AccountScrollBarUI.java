// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.scroll;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class AccountScrollBarUI extends BasicScrollBarUI
{
    protected int heightThubm;
    protected Color trackColor;
    protected Color thumbColor;
    
    public AccountScrollBarUI() {
        this.heightThubm = 46;
        this.trackColor = new Color(225, 234, 238);
        this.thumbColor = new Color(205, 223, 233);
    }
    
    @Override
    protected void paintTrack(final Graphics g, final JComponent c, final Rectangle trackBounds) {
        final Graphics2D g2 = (Graphics2D)g;
        final Rectangle rec = g2.getClipBounds();
        g2.setColor(this.trackColor);
        g2.fillRect(rec.x, rec.y, rec.width, rec.height);
    }
    
    @Override
    protected void paintThumb(final Graphics g, final JComponent c, final Rectangle thumbBounds) {
        final Graphics2D g2 = (Graphics2D)g;
        final Rectangle rec = thumbBounds;
        g2.setColor(this.thumbColor);
        g2.fillRect(rec.x, rec.y, rec.width, rec.height);
    }
    
    @Override
    protected Dimension getMinimumThumbSize() {
        return new Dimension(7, 46);
    }
    
    @Override
    protected JButton createDecreaseButton(final int orientation) {
        return this.createZeroButton();
    }
    
    @Override
    protected JButton createIncreaseButton(final int orientation) {
        return this.createZeroButton();
    }
    
    private JButton createZeroButton() {
        final JButton jbutton = new JButton();
        jbutton.setPreferredSize(new Dimension(0, 0));
        jbutton.setMinimumSize(new Dimension(0, 0));
        jbutton.setMaximumSize(new Dimension(0, 0));
        return jbutton;
    }
}
