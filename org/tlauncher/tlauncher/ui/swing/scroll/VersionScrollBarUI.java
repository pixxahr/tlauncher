// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.scroll;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class VersionScrollBarUI extends BasicScrollBarUI
{
    protected int heightThubm;
    protected int gapThubm;
    protected Color lineColor;
    protected Color trackColor;
    protected Color thumbColor;
    
    public VersionScrollBarUI() {
        this.heightThubm = 16;
        this.gapThubm = 6;
        this.lineColor = new Color(255, 255, 255);
        this.trackColor = new Color(30, 134, 187);
        this.thumbColor = new Color(191, 219, 235);
    }
    
    public int getHeightThubm() {
        return this.heightThubm;
    }
    
    public void setHeightThubm(final int heightThubm) {
        this.heightThubm = heightThubm;
    }
    
    @Override
    protected void paintTrack(final Graphics g, final JComponent c, final Rectangle trackBounds) {
        final Graphics2D g2 = (Graphics2D)g;
        final Rectangle rec = g2.getClipBounds();
        g2.setColor(this.trackColor);
        g2.fillRect(rec.x, rec.y, rec.width, rec.height);
    }
    
    @Override
    protected void paintThumb(final Graphics g, final JComponent c, final Rectangle thumbBounds) {
        final Graphics2D g2 = (Graphics2D)g;
        final Rectangle rec = thumbBounds;
        g2.setColor(this.thumbColor);
        g2.fillRect(rec.x, rec.y, rec.width, rec.height);
        final int width = rec.width - rec.width / 3;
        final int startX = rec.x + rec.width / 6 + 1;
        for (int startY = rec.y + rec.height / 2 - this.heightThubm / 2, i = 0; i < 4; ++i, startY += this.gapThubm) {
            this.drawLines(g2, startX, startY, width);
        }
    }
    
    private void drawLines(final Graphics2D g2, final int startX, final int startY, final int width) {
        g2.setColor(Color.WHITE);
        g2.drawLine(startX, startY, startX + width - 1, startY);
        g2.setColor(new Color(190, 190, 190));
        g2.drawLine(startX, startY + 1, startX + width - 1, startY + 1);
    }
    
    @Override
    protected Dimension getMinimumThumbSize() {
        return new Dimension(10, 80);
    }
    
    @Override
    protected JButton createDecreaseButton(final int orientation) {
        return this.createZeroButton();
    }
    
    @Override
    protected JButton createIncreaseButton(final int orientation) {
        return this.createZeroButton();
    }
    
    private JButton createZeroButton() {
        final JButton jbutton = new JButton();
        jbutton.setPreferredSize(new Dimension(0, 0));
        jbutton.setMinimumSize(new Dimension(0, 0));
        jbutton.setMaximumSize(new Dimension(0, 0));
        return jbutton;
    }
    
    public int getGapThubm() {
        return this.gapThubm;
    }
    
    public void setGapThubm(final int gapThubm) {
        this.gapThubm = gapThubm;
    }
}
