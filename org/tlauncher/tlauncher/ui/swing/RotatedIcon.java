// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.awt.geom.AffineTransform;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Component;
import javax.swing.Icon;

public class RotatedIcon implements Icon
{
    private Icon icon;
    private Rotate rotate;
    private double angle;
    
    public RotatedIcon(final Icon icon) {
        this(icon, Rotate.UP);
    }
    
    private RotatedIcon(final Icon icon, final Rotate rotate) {
        this.icon = icon;
        this.rotate = rotate;
    }
    
    public RotatedIcon(final Icon icon, final double angle) {
        this(icon, Rotate.ABOUT_CENTER);
        this.angle = angle;
    }
    
    public Icon getIcon() {
        return this.icon;
    }
    
    public Rotate getRotate() {
        return this.rotate;
    }
    
    public double getAngle() {
        return this.angle;
    }
    
    @Override
    public int getIconWidth() {
        if (this.rotate == Rotate.ABOUT_CENTER) {
            final double radians = Math.toRadians(this.angle);
            final double sin = Math.abs(Math.sin(radians));
            final double cos = Math.abs(Math.cos(radians));
            final int width = (int)Math.floor(this.icon.getIconWidth() * cos + this.icon.getIconHeight() * sin);
            return width;
        }
        if (this.rotate == Rotate.UPSIDE_DOWN) {
            return this.icon.getIconWidth();
        }
        return this.icon.getIconHeight();
    }
    
    @Override
    public int getIconHeight() {
        if (this.rotate == Rotate.ABOUT_CENTER) {
            final double radians = Math.toRadians(this.angle);
            final double sin = Math.abs(Math.sin(radians));
            final double cos = Math.abs(Math.cos(radians));
            final int height = (int)Math.floor(this.icon.getIconHeight() * cos + this.icon.getIconWidth() * sin);
            return height;
        }
        if (this.rotate == Rotate.UPSIDE_DOWN) {
            return this.icon.getIconHeight();
        }
        return this.icon.getIconWidth();
    }
    
    @Override
    public void paintIcon(final Component c, final Graphics g, final int x, final int y) {
        final Graphics2D g2 = (Graphics2D)g.create();
        final int cWidth = this.icon.getIconWidth() / 2;
        final int cHeight = this.icon.getIconHeight() / 2;
        final int xAdjustment = (this.icon.getIconWidth() % 2 == 0) ? 0 : -1;
        final int yAdjustment = (this.icon.getIconHeight() % 2 == 0) ? 0 : -1;
        if (this.rotate == Rotate.DOWN) {
            g2.translate(x + cHeight, y + cWidth);
            g2.rotate(Math.toRadians(90.0));
            this.icon.paintIcon(c, g2, -cWidth, yAdjustment - cHeight);
        }
        else if (this.rotate == Rotate.UP) {
            g2.translate(x + cHeight, y + cWidth);
            g2.rotate(Math.toRadians(-90.0));
            this.icon.paintIcon(c, g2, xAdjustment - cWidth, -cHeight);
        }
        else if (this.rotate == Rotate.UPSIDE_DOWN) {
            g2.translate(x + cWidth, y + cHeight);
            g2.rotate(Math.toRadians(180.0));
            this.icon.paintIcon(c, g2, xAdjustment - cWidth, yAdjustment - cHeight);
        }
        else if (this.rotate == Rotate.ABOUT_CENTER) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            final AffineTransform original = g2.getTransform();
            final AffineTransform at = new AffineTransform();
            at.concatenate(original);
            at.translate((this.getIconWidth() - this.icon.getIconWidth()) / 2, (this.getIconHeight() - this.icon.getIconHeight()) / 2);
            at.rotate(Math.toRadians(this.angle), x + cWidth, y + cHeight);
            g2.setTransform(at);
            this.icon.paintIcon(c, g2, x, y);
            g2.setTransform(original);
        }
    }
    
    public enum Rotate
    {
        DOWN, 
        UP, 
        UPSIDE_DOWN, 
        ABOUT_CENTER;
    }
}
