// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.border;

import java.awt.Insets;
import java.awt.Graphics;
import java.awt.Component;
import java.awt.Color;
import javax.swing.border.MatteBorder;

public class VersionBorder extends MatteBorder
{
    public static final Color SEPARATOR_COLOR;
    
    public VersionBorder(final int top, final int left, final int bottom, final int right, final Color matteColor) {
        super(top, left, bottom, right, matteColor);
    }
    
    @Override
    public void paintBorder(final Component c, final Graphics g, final int x, final int y, final int width, final int height) {
        final Insets insets = this.getBorderInsets(c);
        final Color oldColor = g.getColor();
        g.translate(x, y);
        if (this.tileIcon != null) {
            this.color = ((this.tileIcon.getIconWidth() == -1) ? Color.gray : null);
        }
        if (this.color != null) {
            g.setColor(VersionBorder.SEPARATOR_COLOR);
            g.drawLine(0, height - 1, width, height - 1);
        }
        else if (this.tileIcon != null) {
            final int tileW = this.tileIcon.getIconWidth();
            final int tileH = this.tileIcon.getIconHeight();
            this.paintEdge(c, g, 0, 0, width - insets.right, insets.top, tileW, tileH);
            this.paintEdge(c, g, 0, insets.top, insets.left, height - insets.top, tileW, tileH);
            this.paintEdge(c, g, insets.left, height - insets.bottom, width - insets.left, insets.bottom, tileW, tileH);
            this.paintEdge(c, g, width - insets.right, 0, insets.right, height - insets.bottom, tileW, tileH);
        }
        g.translate(-x, -y);
        g.setColor(oldColor);
    }
    
    private void paintEdge(final Component c, Graphics g, int x, int y, final int width, final int height, final int tileW, final int tileH) {
        g = g.create(x, y, width, height);
        final int sY = -(y % tileH);
        for (x = -(x % tileW); x < width; x += tileW) {
            for (y = sY; y < height; y += tileH) {
                this.tileIcon.paintIcon(c, g, x, y);
            }
        }
        g.dispose();
    }
    
    static {
        SEPARATOR_COLOR = new Color(220, 220, 220);
    }
}
