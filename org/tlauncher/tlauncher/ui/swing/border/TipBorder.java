// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.border;

import java.awt.Insets;
import java.awt.Graphics;
import java.awt.Component;
import java.awt.Color;
import javax.swing.border.MatteBorder;

public class TipBorder extends MatteBorder
{
    private final BORDER_POS pos;
    
    public static TipBorder createInstance(final int width, final BORDER_POS pos, final Color matteColor) {
        switch (pos) {
            case UP: {
                return new TipBorder(width, 0, 0, 0, matteColor, pos);
            }
            case RIGHT: {
                return new TipBorder(0, 0, 0, width, matteColor, pos);
            }
            case BOTTOM: {
                return new TipBorder(0, 0, width, 0, matteColor, pos);
            }
            case LEFT: {
                return new TipBorder(0, width, 0, 0, matteColor, pos);
            }
            default: {
                throw new IllegalArgumentException(pos.toString());
            }
        }
    }
    
    private TipBorder(final int top, final int left, final int bottom, final int right, final Color matteColor, final BORDER_POS pos) {
        super(top, left, bottom, right, matteColor);
        this.pos = pos;
    }
    
    @Override
    public void paintBorder(final Component c, final Graphics g, final int x, final int y, final int width, final int height) {
        final Insets insets = this.getBorderInsets(c);
        final Color oldColor = g.getColor();
        g.translate(x, y);
        if (this.tileIcon != null) {
            this.color = ((this.tileIcon.getIconWidth() == -1) ? Color.gray : null);
        }
        if (this.color != null) {
            g.setColor(this.color);
            int[] xT = new int[0];
            int[] yT = new int[0];
            switch (this.pos) {
                case BOTTOM: {
                    final int bottom = insets.bottom;
                    xT = new int[] { width - 2 * bottom, width - 2 * bottom + bottom / 2, width - 2 * bottom + bottom };
                    yT = new int[] { height - bottom, height, height - bottom };
                    break;
                }
                case UP: {
                    final int top = insets.top;
                    xT = new int[] { width - 2 * top, width - 2 * top + top / 2, width - 2 * top + top };
                    yT = new int[] { 0 + top, 0, 0 + top };
                }
                case LEFT: {}
            }
            g.fillPolygon(xT, yT, 3);
            g.translate(-x, -y);
            g.setColor(oldColor);
        }
    }
    
    public enum BORDER_POS
    {
        UP, 
        BOTTOM, 
        LEFT, 
        RIGHT;
    }
}
