// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.box;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.util.ArrayList;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import java.util.List;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.swing.plaf.ScrollBarUI;
import java.awt.Component;
import javax.swing.JComponent;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.ui.ModpackScrollBarUI;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import org.tlauncher.tlauncher.ui.ui.ModpackComboBoxUI;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.swing.renderer.ModpackComboxRenderer;
import javax.swing.ComboBoxModel;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.ui.listener.mods.GameEntityListener;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedComboBox;

public class ModpackComboBox extends ExtendedComboBox<CompleteVersion> implements GameEntityListener, LocalizableComponent
{
    public ModpackComboBox() {
        this.setModel(new LocalModapackBoxModel());
        this.setRenderer(new ModpackComboxRenderer());
        this.setUI(new ModpackComboBoxUI() {
            @Override
            protected ComboPopup createPopup() {
                final BasicComboPopup basic = new BasicComboPopup(this.comboBox) {
                    @Override
                    protected JScrollPane createScroller() {
                        final ModpackScrollBarUI barUI = new ModpackScrollBarUI() {
                            @Override
                            protected Dimension getMinimumThumbSize() {
                                return new Dimension(10, 40);
                            }
                            
                            @Override
                            public Dimension getPreferredSize(final JComponent c) {
                                final Dimension dim = super.getPreferredSize(c);
                                dim.setSize(8.0, dim.getHeight());
                                return dim;
                            }
                        };
                        barUI.setGapThubm(5);
                        final JScrollPane scroller = new JScrollPane(this.list, 20, 31);
                        scroller.getVerticalScrollBar().setUI(barUI);
                        scroller.setBackground(ModpackComboBoxUI.BACKGROUND_BOX);
                        return scroller;
                    }
                };
                basic.setMaximumSize(new Dimension(172, 149));
                basic.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, ModpackComboxRenderer.LINE));
                return basic;
            }
        });
        this.setBorder(BorderFactory.createLineBorder(ModpackComboxRenderer.LINE, 1));
    }
    
    @Override
    public void activationStarted(final GameEntityDTO e) {
    }
    
    @Override
    public void activation(final GameEntityDTO e) {
    }
    
    @Override
    public void activationError(final GameEntityDTO e, final Throwable t) {
    }
    
    @Override
    public void processingStarted(final GameEntityDTO e, final VersionDTO version) {
    }
    
    @Override
    public void installEntity(final GameEntityDTO e, final GameType type) {
    }
    
    @Override
    public void installEntity(final CompleteVersion e) {
        this.addItem(e);
        this.setSelectedItem(e);
    }
    
    @Override
    public void removeEntity(final GameEntityDTO e) {
    }
    
    @Override
    public void installError(final GameEntityDTO e, final VersionDTO v, final Throwable t) {
    }
    
    @Override
    public void populateStatus(final GameEntityDTO status, final GameType type, final boolean state) {
    }
    
    @Override
    public void updateVersion(final CompleteVersion v, final CompleteVersion newVersion) {
        this.repaint();
    }
    
    public List<ModpackDTO> getModpacks() {
        final int size = this.getModel().getSize();
        final List<ModpackDTO> list = new ArrayList<ModpackDTO>();
        for (int i = 1; i < size; ++i) {
            list.add(this.getModel().getElementAt(i).getModpack());
        }
        return list;
    }
    
    public CompleteVersion findByModpack(final ModpackDTO modpackDTO, final VersionDTO versionDTO) {
        for (int size = this.getModel().getSize(), i = 1; i < size; ++i) {
            final ModpackDTO m = this.getModel().getElementAt(i).getModpack();
            if (m.getId().equals(modpackDTO.getId()) && m.getVersion().getId().equals(versionDTO.getId())) {
                return this.getModel().getElementAt(i);
            }
        }
        return null;
    }
    
    @Override
    public void removeCompleteVersion(final CompleteVersion e) {
        final ComboBoxModel<CompleteVersion> model = this.getModel();
        for (int i = 1; i < model.getSize(); ++i) {
            if (model.getElementAt(i).getID().equals(e.getID())) {
                this.removeItemAt(i);
                break;
            }
        }
    }
    
    @Override
    public void updateLocale() {
        this.getModel().getElementAt(0).setID(Localizable.get("modpack.local.box.default"));
    }
}
