// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.box;

import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import net.minecraft.launcher.versions.CompleteVersion;
import javax.swing.DefaultComboBoxModel;

class LocalModapackBoxModel extends DefaultComboBoxModel<CompleteVersion>
{
    private final CompleteVersion DEFAULT;
    
    public LocalModapackBoxModel() {
        (this.DEFAULT = new CompleteVersion()).setID(Localizable.get("modpack.local.box.default"));
        final ModpackDTO d = new ModpackDTO();
        d.setId(0L);
        d.setName("");
        this.DEFAULT.setModpackDTO(d);
        this.addElement(this.DEFAULT);
        this.setSelectedItem(this.DEFAULT);
    }
    
    @Override
    public void removeAllElements() {
        super.removeAllElements();
        this.addElement(this.DEFAULT);
    }
    
    @Override
    public void removeElement(final Object anObject) {
        if (anObject == this.DEFAULT) {
            return;
        }
        super.removeElement(anObject);
    }
    
    @Override
    public void removeElementAt(final int index) {
        if (index == 0) {
            return;
        }
        super.removeElementAt(index);
    }
}
