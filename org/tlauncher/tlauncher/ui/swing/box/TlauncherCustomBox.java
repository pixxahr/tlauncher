// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.box;

import javax.swing.BorderFactory;
import javax.swing.plaf.ComboBoxUI;
import org.tlauncher.tlauncher.ui.ui.TlauncherBasicComboBoxUI;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

public class TlauncherCustomBox<T> extends JComboBox<T>
{
    public TlauncherCustomBox() {
        this.init();
    }
    
    public TlauncherCustomBox(final ComboBoxModel<T> aModel) {
        super(aModel);
        this.init();
    }
    
    public TlauncherCustomBox(final T[] items) {
        super(items);
        this.setModel(new DefaultComboBoxModel<T>(items));
        this.init();
    }
    
    protected void init() {
        this.setUI(new TlauncherBasicComboBoxUI());
        this.setBorder(BorderFactory.createEmptyBorder());
    }
}
