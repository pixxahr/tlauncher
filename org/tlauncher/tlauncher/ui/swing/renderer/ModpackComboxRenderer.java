// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.renderer;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import java.awt.Rectangle;
import java.awt.Graphics;
import javax.swing.JLabel;
import net.minecraft.launcher.versions.CompleteVersion;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JList;
import java.awt.Color;
import javax.swing.DefaultListCellRenderer;

public class ModpackComboxRenderer extends DefaultListCellRenderer
{
    public static final Color LINE;
    protected int GUP_LEFT;
    protected Color backgroundBox;
    
    public ModpackComboxRenderer() {
        this.GUP_LEFT = 18;
        this.backgroundBox = new Color(63, 177, 241);
    }
    
    @Override
    public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        if (value == null) {
            return null;
        }
        final JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(172, 30));
        p.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, ModpackComboxRenderer.LINE));
        p.setBackground(this.backgroundBox);
        JLabel label = new JLabel(((CompleteVersion)value).getID()) {
            @Override
            protected void paintComponent(final Graphics g) {
                super.paintComponent(g);
                if (isSelected && index > -1) {
                    final Rectangle bounds;
                    final Rectangle r = bounds = this.getBounds();
                    --bounds.height;
                    g.setColor(new Color(121, 201, 245));
                    g.drawLine(ModpackComboxRenderer.this.GUP_LEFT, r.height - this.getFontMetrics(this.getFont()).getDescent(), this.getFontMetrics(this.getFont()).stringWidth(this.getText()) + ModpackComboxRenderer.this.GUP_LEFT, r.height - this.getFontMetrics(this.getFont()).getDescent());
                    g.setColor(new Color(121, 211, 247));
                    g.drawLine(ModpackComboxRenderer.this.GUP_LEFT, r.height - this.getFontMetrics(this.getFont()).getDescent(), this.getFontMetrics(this.getFont()).stringWidth(this.getText()) + ModpackComboxRenderer.this.GUP_LEFT, r.height - this.getFontMetrics(this.getFont()).getDescent());
                }
            }
        };
        label.setForeground(Color.WHITE);
        label.setBorder(BorderFactory.createEmptyBorder(0, this.GUP_LEFT, 0, 0));
        SwingUtil.changeFontFamily(label, FontTL.ROBOTO_REGULAR, 14);
        p.add(label, "Center");
        if (index > 0) {
            label = new JLabel(ImageCache.getNativeIcon("config-modpack.png"));
            label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 12));
            SwingUtil.changeFontFamily(label, FontTL.ROBOTO_REGULAR, 14);
            p.add(label, "East");
        }
        if (index == 0 && list.getModel().getSize() < 2) {
            label.setText(Localizable.get("modpack.local.box.default.list"));
        }
        return p;
    }
    
    @Override
    public void setBackground(final Color color) {
        this.backgroundBox = color;
    }
    
    static {
        LINE = new Color(67, 187, 255);
    }
}
