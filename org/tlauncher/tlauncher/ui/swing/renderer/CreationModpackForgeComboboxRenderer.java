// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.renderer;

import javax.swing.BorderFactory;
import java.awt.Dimension;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JList;
import java.awt.Color;
import javax.swing.DefaultListCellRenderer;

public class CreationModpackForgeComboboxRenderer extends DefaultListCellRenderer
{
    public static final Color LINE;
    static final int GUP_LEFT = 13;
    public static final Color TEXT_COLOR;
    
    @Override
    public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        final JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (label == null) {
            return null;
        }
        SwingUtil.changeFontFamily(label, FontTL.ROBOTO_MEDIUM, 14, ColorUtil.COLOR_25);
        label.setText((String)value);
        label.setPreferredSize(new Dimension(238, 44));
        label.setOpaque(true);
        label.setBackground(Color.WHITE);
        label.setForeground(CreationModpackForgeComboboxRenderer.TEXT_COLOR);
        if (isSelected && index != -1) {
            label.setBackground(new Color(235, 235, 235));
        }
        else {
            label.setBackground(Color.white);
        }
        label.setBorder(BorderFactory.createEmptyBorder(0, 13, 0, 0));
        return label;
    }
    
    static {
        LINE = new Color(149, 149, 149);
        TEXT_COLOR = new Color(25, 25, 25);
    }
}
