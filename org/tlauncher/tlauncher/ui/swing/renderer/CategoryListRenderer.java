// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.renderer;

import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.modpack.domain.client.share.Category;
import java.awt.Component;
import javax.swing.JList;

public class CategoryListRenderer extends UserCategoryListRenderer
{
    @Override
    public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        if (value == null) {
            return null;
        }
        final String text = Localizable.get("modpack." + ((Category)value).name().toLowerCase());
        final JComponent component = (JComponent)this.createElement(index, isSelected, text);
        if (Category.getSubCategories().contains(value)) {
            final CompoundBorder border = new CompoundBorder(component.getBorder(), BorderFactory.createEmptyBorder(0, 15, 0, 0));
            component.setBorder(border);
        }
        return component;
    }
}
