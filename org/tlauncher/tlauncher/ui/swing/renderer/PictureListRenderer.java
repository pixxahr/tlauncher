// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.renderer;

import javax.swing.BorderFactory;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.DefaultListCellRenderer;

public class PictureListRenderer extends DefaultListCellRenderer
{
    @Override
    public Component getListCellRendererComponent(final JList list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        final int gup = 20;
        final int width = 294;
        final JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        label.setIcon((Icon)value);
        label.setPreferredSize(new Dimension(width + gup, 190));
        label.setText("");
        label.setOpaque(false);
        if (index == 2) {
            label.setPreferredSize(new Dimension(width, 190));
        }
        else {
            label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, gup));
        }
        return label;
    }
}
