// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.renderer;

import javax.swing.border.Border;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.scenes.ModpackScene;
import java.awt.Component;
import javax.swing.JList;

public class UserCategoryListRenderer extends ModpackComboxRenderer
{
    @Override
    public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        if (value == null) {
            return null;
        }
        final String text = Localizable.get("modpack." + ((ModpackScene.UserCategory)value).name().toLowerCase());
        return this.createElement(index, isSelected, text);
    }
    
    protected Component createElement(final int index, final boolean isSelected, final String text) {
        final JLabel label = new JLabel(text) {
            @Override
            protected void paintComponent(final Graphics g) {
                super.paintComponent(g);
                if (isSelected && index > -1) {
                    final Rectangle clipBounds;
                    final Rectangle r = clipBounds = g.getClipBounds();
                    --clipBounds.height;
                    g.setColor(new Color(121, 201, 245));
                    g.drawLine(UserCategoryListRenderer.this.GUP_LEFT, r.height - this.getFontMetrics(this.getFont()).getDescent(), this.getFontMetrics(this.getFont()).stringWidth(this.getText()) + UserCategoryListRenderer.this.GUP_LEFT, r.height - this.getFontMetrics(this.getFont()).getDescent());
                    g.setColor(new Color(121, 211, 247));
                    g.drawLine(UserCategoryListRenderer.this.GUP_LEFT, r.height - this.getFontMetrics(this.getFont()).getDescent(), this.getFontMetrics(this.getFont()).stringWidth(this.getText()) + UserCategoryListRenderer.this.GUP_LEFT, r.height - this.getFontMetrics(this.getFont()).getDescent());
                }
            }
        };
        label.setForeground(Color.WHITE);
        SwingUtil.changeFontFamily(label, FontTL.ROBOTO_REGULAR, 14);
        label.setBorder(BorderFactory.createEmptyBorder(0, this.GUP_LEFT, 0, 0));
        final JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(172, 30));
        p.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, UserCategoryListRenderer.LINE));
        p.setBackground(this.backgroundBox);
        p.add(label, "Center");
        return p;
    }
}
