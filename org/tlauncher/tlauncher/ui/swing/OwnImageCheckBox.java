// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import org.tlauncher.tlauncher.ui.images.ImageIcon;
import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.JCheckBox;

public class OwnImageCheckBox extends JCheckBox
{
    public OwnImageCheckBox(final String text, final String onText, final String offText) {
        super(text);
        final ImageIcon on = ImageCache.getIcon(onText);
        final ImageIcon off = ImageCache.getIcon(offText);
        this.setSelectedIcon((Icon)on);
        this.setDisabledSelectedIcon((Icon)on);
        this.setPressedIcon((Icon)on);
        this.setIcon((Icon)off);
        this.setDisabledIcon((Icon)off);
        this.setOpaque(false);
        this.setFocusable(false);
    }
}
