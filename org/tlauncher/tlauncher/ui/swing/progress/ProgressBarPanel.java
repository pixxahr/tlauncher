// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.progress;

import org.tlauncher.tlauncher.minecraft.crash.Crash;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.downloader.Downloader;
import javax.swing.Box;
import java.awt.Component;
import java.awt.Container;
import javax.swing.BoxLayout;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import org.tlauncher.tlauncher.ui.progress.login.LauncherProgress;
import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.progress.login.LauncherProgressListener;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftExtendedListener;
import org.tlauncher.tlauncher.downloader.DownloaderListener;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import javax.swing.JLayeredPane;

public class ProgressBarPanel extends JLayeredPane implements LocalizableComponent, DownloaderListener, MinecraftExtendedListener, LauncherProgressListener
{
    public static final Dimension SIZE;
    private JProgressBar bar;
    private JLabel percentLabel;
    private JLabel centerLabel;
    private JLabel speedLabel;
    private JLabel fileLabel;
    private boolean downloadingStart;
    private ExtendedPanel upperPanel;
    
    public ProgressBarPanel(final Icon speed, final Icon file, final LauncherProgress bar) {
        this.bar = bar;
        this.setVisible(false);
        this.upperPanel = new ExtendedPanel(new BorderLayout(0, 0));
        this.speedLabel = this.createLabel(speed);
        this.fileLabel = this.createLabel(file);
        this.percentLabel = this.createLabel(null);
        (this.centerLabel = this.createLabel(null)).setHorizontalAlignment(0);
        this.percentLabel.setFont(this.percentLabel.getFont().deriveFont(1));
        this.upperPanel.setPreferredSize(ProgressBarPanel.SIZE);
        this.upperPanel.setInsets(0, 20, 0, 20);
        this.setPreferredSize(ProgressBarPanel.SIZE);
        final ExtendedPanel leftPanel = new ExtendedPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, 0));
        leftPanel.add(this.fileLabel);
        leftPanel.add(Box.createHorizontalStrut(15));
        leftPanel.add(this.speedLabel);
        this.upperPanel.add(this.centerLabel, "Center");
        this.upperPanel.add(this.percentLabel, "East");
        this.upperPanel.add(leftPanel, "West");
        this.add(bar, 1);
        this.add(this.upperPanel, 0);
        bar.setBounds(0, 0, ProgressBarPanel.SIZE.width, ProgressBarPanel.SIZE.height);
        this.upperPanel.setBounds(0, 0, ProgressBarPanel.SIZE.width, ProgressBarPanel.SIZE.height);
    }
    
    private JLabel createLabel(final Icon icon) {
        final JLabel jLabel = new JLabel();
        jLabel.setFont(jLabel.getFont().deriveFont(0, 12.0f));
        if (icon != null) {
            jLabel.setIcon(icon);
        }
        jLabel.setHorizontalTextPosition(4);
        return jLabel;
    }
    
    private void startProgress() {
        this.setVisible(true);
    }
    
    private void stopProgress() {
        this.setVisible(false);
        this.fileLabel.setVisible(false);
        this.speedLabel.setVisible(false);
        this.centerLabel.setVisible(false);
        this.bar.setValue(0);
        this.downloadingStart = true;
    }
    
    @Override
    public void updateLocale() {
    }
    
    @Override
    public void onDownloaderStart(final Downloader d, final int files) {
        this.startProgress();
        this.updateStateDownloading(d);
    }
    
    @Override
    public void onDownloaderAbort(final Downloader d) {
        this.stopProgress();
    }
    
    @Override
    public void onDownloaderProgress(final Downloader d, final double progress, final double speed) {
        this.updateStateDownloading(d);
    }
    
    @Override
    public void onDownloaderFileComplete(final Downloader d, final Downloadable file) {
        this.updateStateDownloading(d);
    }
    
    private void updateStateDownloading(final Downloader d) {
        if (d.getProgress() > 0.0) {
            final int dprogress = (int)(d.getProgress() * 100.0);
            if (this.bar.getValue() > dprogress) {
                return;
            }
            this.bar.setValue(dprogress);
            StringBuilder b = new StringBuilder();
            b.append(d.getRemaining()).append(" ").append(Localizable.get("progress.bar.panel.file"));
            this.fileLabel.setText(b.toString());
            b = new StringBuilder();
            b.append((int)d.getSpeed()).append(" ").append(Localizable.get("progress.bar.panel.speed"));
            this.speedLabel.setText(b.toString());
            this.percentLabel.setText(dprogress + "%");
            if (this.downloadingStart) {
                this.centerLabel.setVisible(false);
                this.fileLabel.setVisible(true);
                this.speedLabel.setVisible(true);
                this.downloadingStart = false;
            }
        }
    }
    
    @Override
    public void onDownloaderComplete(final Downloader d) {
        this.stopProgress();
    }
    
    @Override
    public void onMinecraftPrepare() {
        this.clean();
        this.startProgress();
        this.centerLabel.setText(Localizable.get("progress.bar.panel.init"));
        this.centerLabel.setVisible(true);
        this.downloadingStart = true;
    }
    
    @Override
    public void onMinecraftAbort() {
        this.stopProgress();
    }
    
    @Override
    public void onMinecraftClose() {
    }
    
    @Override
    public void onMinecraftError(final Throwable e) {
        this.stopProgress();
    }
    
    @Override
    public void onMinecraftKnownError(final MinecraftException e) {
        this.stopProgress();
    }
    
    @Override
    public void onMinecraftCrash(final Crash crash) {
    }
    
    @Override
    public void onMinecraftCollecting() {
    }
    
    @Override
    public void onMinecraftComparingAssets() {
    }
    
    @Override
    public void onMinecraftDownloading() {
    }
    
    @Override
    public void onMinecraftReconstructingAssets() {
    }
    
    @Override
    public void onMinecraftUnpackingNatives() {
    }
    
    @Override
    public void onMinecraftDeletingEntries() {
    }
    
    @Override
    public void onMinecraftConstructing() {
    }
    
    @Override
    public void onMinecraftLaunch() {
        this.stopProgress();
    }
    
    @Override
    public void onMinecraftPostLaunch() {
    }
    
    private void clean() {
        this.speedLabel.setVisible(false);
        this.fileLabel.setVisible(false);
        this.centerLabel.setVisible(false);
        this.speedLabel.setText("");
        this.fileLabel.setText("");
        this.percentLabel.setText("");
        this.bar.setValue(0);
    }
    
    public JProgressBar getBar() {
        return this.bar;
    }
    
    public void setBar(final JProgressBar bar) {
        this.bar = bar;
    }
    
    @Override
    public void repaintPanel() {
        this.upperPanel.repaint();
    }
    
    static {
        SIZE = new Dimension(1050, 24);
    }
}
