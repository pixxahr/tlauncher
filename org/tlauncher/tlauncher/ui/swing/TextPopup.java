// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.datatransfer.DataFlavor;
import java.awt.Toolkit;
import org.apache.commons.lang3.StringUtils;
import javax.swing.JPopupMenu;
import javax.swing.text.JTextComponent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

public class TextPopup extends MouseAdapter
{
    @Override
    public void mouseClicked(final MouseEvent e) {
        if (e.getModifiers() != 4) {
            return;
        }
        final Object source = e.getSource();
        if (!(source instanceof JTextComponent)) {
            return;
        }
        final JPopupMenu popup = this.getPopup(e, (JTextComponent)source);
        if (popup == null) {
            return;
        }
        popup.show(e.getComponent(), e.getX(), e.getY() - popup.getSize().height);
    }
    
    protected JPopupMenu getPopup(final MouseEvent e, final JTextComponent comp) {
        if (!comp.isEnabled()) {
            return null;
        }
        final boolean isEditable = comp.isEditable();
        final boolean isSelected = comp.getSelectedText() != null;
        final boolean hasValue = StringUtils.isNotEmpty((CharSequence)comp.getText());
        final boolean pasteAvailable = isEditable && Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null).isDataFlavorSupported(DataFlavor.stringFlavor);
        final JPopupMenu menu = new JPopupMenu();
        final Action cut = isEditable ? selectAction(comp, "cut-to-clipboard", "cut") : null;
        final Action copy = selectAction(comp, "copy-to-clipboard", "copy");
        final Action paste = pasteAvailable ? selectAction(comp, "paste-from-clipboard", "paste") : null;
        final Action selectAll = hasValue ? selectAction(comp, "select-all", "selectAll") : null;
        Action copyAll;
        if (selectAll != null && copy != null) {
            copyAll = new EmptyAction() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    selectAll.actionPerformed(e);
                    copy.actionPerformed(e);
                    comp.setSelectionStart(comp.getSelectionEnd());
                }
            };
        }
        else {
            copyAll = null;
        }
        if (cut != null) {
            menu.add(cut).setText(Localizable.get("popup.cut"));
        }
        if (isSelected && copy != null) {
            menu.add(copy).setText(Localizable.get("popup.copy"));
        }
        if (paste != null) {
            menu.add(paste).setText(Localizable.get("popup.paste"));
        }
        if (selectAll != null) {
            if (menu.getComponentCount() > 0 && !(menu.getComponent(menu.getComponentCount() - 1) instanceof JPopupMenu.Separator)) {
                menu.addSeparator();
            }
            menu.add(selectAll).setText(Localizable.get("popup.selectall"));
        }
        if (copyAll != null) {
            menu.add(copyAll).setText(Localizable.get("popup.copyall"));
        }
        if (menu.getComponentCount() == 0) {
            return null;
        }
        if (menu.getComponent(0) instanceof JPopupMenu.Separator) {
            menu.remove(0);
        }
        if (menu.getComponent(menu.getComponentCount() - 1) instanceof JPopupMenu.Separator) {
            menu.remove(menu.getComponentCount() - 1);
        }
        return menu;
    }
    
    protected static Action selectAction(final JTextComponent comp, final String general, final String fallback) {
        Action action = comp.getActionMap().get(general);
        if (action == null) {
            action = comp.getActionMap().get(fallback);
        }
        return action;
    }
}
