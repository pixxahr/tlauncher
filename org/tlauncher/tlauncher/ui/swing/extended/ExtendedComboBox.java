// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import org.tlauncher.tlauncher.ui.swing.DefaultConverterCellRenderer;
import org.tlauncher.util.Reflect;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.TLauncherFrame;
import javax.swing.ComboBoxModel;
import org.tlauncher.tlauncher.ui.swing.SimpleComboBoxModel;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.converter.StringConverter;
import org.tlauncher.tlauncher.ui.swing.box.TlauncherCustomBox;

public class ExtendedComboBox<T> extends TlauncherCustomBox<T>
{
    private static final long serialVersionUID = -4509947341182373649L;
    private StringConverter<T> converter;
    
    public ExtendedComboBox(final ListCellRenderer<T> renderer) {
        this.setModel(new SimpleComboBoxModel<T>());
        this.setRenderer(renderer);
        this.setOpaque(false);
        this.setFont(this.getFont().deriveFont(TLauncherFrame.fontSize));
        Reflect.cast(this.getEditor().getEditorComponent(), JComponent.class).setOpaque(false);
    }
    
    public ExtendedComboBox(final StringConverter<T> converter) {
        this((ListCellRenderer)new DefaultConverterCellRenderer(converter));
        this.converter = converter;
    }
    
    public ExtendedComboBox() {
        this((ListCellRenderer)null);
    }
    
    public SimpleComboBoxModel<T> getSimpleModel() {
        return (SimpleComboBoxModel<T>)(SimpleComboBoxModel)this.getModel();
    }
    
    public T getValueAt(final int i) {
        final Object value = this.getItemAt(i);
        return this.returnAs(value);
    }
    
    public T getSelectedValue() {
        final Object selected = this.getSelectedItem();
        return this.returnAs(selected);
    }
    
    public void setSelectedValue(final T value) {
        this.setSelectedItem(value);
    }
    
    public void setSelectedValue(final String string) {
        final T value = this.convert(string);
        if (value == null) {
            return;
        }
        this.setSelectedValue(value);
    }
    
    public StringConverter<T> getConverter() {
        return this.converter;
    }
    
    public void setConverter(final StringConverter<T> converter) {
        this.converter = converter;
    }
    
    protected String convert(final T obj) {
        final T from = this.returnAs(obj);
        if (this.converter != null) {
            return this.converter.toValue(from);
        }
        return (from == null) ? null : from.toString();
    }
    
    protected T convert(final String from) {
        if (this.converter == null) {
            return null;
        }
        return this.converter.fromString(from);
    }
    
    private T returnAs(final Object obj) {
        try {
            return (T)obj;
        }
        catch (ClassCastException ce) {
            return null;
        }
    }
}
