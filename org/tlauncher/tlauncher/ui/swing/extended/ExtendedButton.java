// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.TLauncherFrame;
import javax.swing.Action;
import javax.swing.Icon;
import java.awt.Color;
import javax.swing.JButton;

public class ExtendedButton extends JButton
{
    private static final long serialVersionUID = -2009736184875993130L;
    public static final Color ORRANGE_COLOR;
    public static final Color GREEN_COLOR;
    public static final Color DARD_GREEN_COLOR;
    public static final Color GRAY_COLOR;
    
    protected ExtendedButton() {
        this.init();
    }
    
    public ExtendedButton(final Icon icon) {
        super(icon);
        this.init();
    }
    
    protected ExtendedButton(final String text) {
        super(text);
        this.init();
    }
    
    public ExtendedButton(final Action a) {
        super(a);
        this.init();
    }
    
    public ExtendedButton(final String text, final Icon icon) {
        super(text, icon);
        this.init();
    }
    
    private void init() {
        this.setFont(this.getFont().deriveFont(TLauncherFrame.fontSize));
        this.setOpaque(false);
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final Component parent = this.findRootParent(ExtendedButton.this.getParent());
                if (parent == null) {
                    return;
                }
                parent.requestFocusInWindow();
            }
            
            private Component findRootParent(final Component comp) {
                if (comp == null) {
                    return null;
                }
                if (comp.getParent() == null) {
                    return comp;
                }
                return this.findRootParent(comp.getParent());
            }
        });
    }
    
    static {
        ORRANGE_COLOR = new Color(235, 132, 46);
        GREEN_COLOR = new Color(107, 202, 45);
        DARD_GREEN_COLOR = new Color(113, 169, 76);
        GRAY_COLOR = new Color(176, 177, 173);
    }
}
