// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import org.tlauncher.util.Reflect;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;

public class VPanel extends ExtendedPanel
{
    private VPanel(final boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        this.setLayout(new BoxLayout(this, 3));
    }
    
    public VPanel() {
        this(true);
    }
    
    @Override
    public BoxLayout getLayout() {
        return (BoxLayout)super.getLayout();
    }
    
    @Override
    public void setLayout(final LayoutManager mgr) {
        if (!(mgr instanceof BoxLayout)) {
            return;
        }
        final int axis = Reflect.cast(mgr, BoxLayout.class).getAxis();
        if (axis == 3 || axis == 1) {
            super.setLayout(mgr);
            return;
        }
        throw new IllegalArgumentException("Illegal BoxLayout axis!");
    }
}
