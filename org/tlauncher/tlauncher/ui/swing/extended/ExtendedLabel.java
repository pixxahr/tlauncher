// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Graphics;
import org.tlauncher.tlauncher.ui.TLauncherFrame;
import javax.swing.Icon;
import java.awt.AlphaComposite;
import javax.swing.JLabel;

public class ExtendedLabel extends JLabel
{
    private static final AlphaComposite disabledAlphaComposite;
    
    public ExtendedLabel(final String text, final Icon icon, final int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
        this.setFont(this.getFont().deriveFont(TLauncherFrame.fontSize));
        this.setOpaque(false);
    }
    
    public ExtendedLabel(final String text, final int horizontalAlignment) {
        this(text, null, horizontalAlignment);
    }
    
    public ExtendedLabel(final String text) {
        this(text, null, 10);
    }
    
    public ExtendedLabel(final Icon image, final int horizontalAlignment) {
        this(null, image, horizontalAlignment);
    }
    
    public ExtendedLabel(final Icon image) {
        this(null, image, 0);
    }
    
    public ExtendedLabel() {
        this(null, null, 10);
    }
    
    public void paintComponent(final Graphics g0) {
        if (this.isEnabled()) {
            super.paintComponent(g0);
            return;
        }
        final Graphics2D g = (Graphics2D)g0;
        final Composite oldComposite = g.getComposite();
        g.setComposite(ExtendedLabel.disabledAlphaComposite);
        super.paintComponent(g);
        g.setComposite(oldComposite);
    }
    
    static {
        disabledAlphaComposite = AlphaComposite.getInstance(3, 0.5f);
    }
}
