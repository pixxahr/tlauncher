// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import java.awt.Point;
import org.tlauncher.tlauncher.ui.swing.util.IntegerArrayGetter;
import java.awt.event.ComponentEvent;
import java.awt.Component;
import java.awt.event.ComponentListener;

public abstract class ExtendedComponentListener implements ComponentListener
{
    private final Component comp;
    private final QuickParameterListenerThread resizeListener;
    private final QuickParameterListenerThread moveListener;
    private ComponentEvent lastResizeEvent;
    private ComponentEvent lastMoveEvent;
    
    public ExtendedComponentListener(final Component component, final int tick) {
        if (component == null) {
            throw new NullPointerException();
        }
        this.comp = component;
        this.resizeListener = new QuickParameterListenerThread(new IntegerArrayGetter() {
            @Override
            public int[] getIntegerArray() {
                return new int[] { ExtendedComponentListener.this.comp.getWidth(), ExtendedComponentListener.this.comp.getHeight() };
            }
        }, new Runnable() {
            @Override
            public void run() {
                ExtendedComponentListener.this.onComponentResized(ExtendedComponentListener.this.lastResizeEvent);
            }
        }, tick);
        this.moveListener = new QuickParameterListenerThread(new IntegerArrayGetter() {
            @Override
            public int[] getIntegerArray() {
                final Point location = ExtendedComponentListener.this.comp.getLocation();
                return new int[] { location.x, location.y };
            }
        }, new Runnable() {
            @Override
            public void run() {
                ExtendedComponentListener.this.onComponentMoved(ExtendedComponentListener.this.lastMoveEvent);
            }
        }, tick);
    }
    
    public ExtendedComponentListener(final Component component) {
        this(component, 500);
    }
    
    @Override
    public final void componentResized(final ComponentEvent e) {
        this.onComponentResizing(e);
        this.resizeListener.startListening();
    }
    
    @Override
    public final void componentMoved(final ComponentEvent e) {
        this.onComponentMoving(e);
        this.moveListener.startListening();
    }
    
    public boolean isListening() {
        return this.resizeListener.isIterating() || this.moveListener.isIterating();
    }
    
    public abstract void onComponentResizing(final ComponentEvent p0);
    
    public abstract void onComponentResized(final ComponentEvent p0);
    
    public abstract void onComponentMoving(final ComponentEvent p0);
    
    public abstract void onComponentMoved(final ComponentEvent p0);
}
