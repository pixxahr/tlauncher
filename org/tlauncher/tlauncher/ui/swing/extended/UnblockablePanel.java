// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import java.awt.LayoutManager;
import org.tlauncher.tlauncher.ui.block.Unblockable;

public class UnblockablePanel extends ExtendedPanel implements Unblockable
{
    private static final long serialVersionUID = -5273727580864479391L;
    
    public UnblockablePanel(final LayoutManager layout, final boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
    }
    
    public UnblockablePanel(final LayoutManager layout) {
        super(layout);
    }
    
    public UnblockablePanel(final boolean isDoubleBuffered) {
        super(isDoubleBuffered);
    }
    
    public UnblockablePanel() {
    }
}
