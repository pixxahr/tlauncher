// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.util.Iterator;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.awt.LayoutManager;
import java.awt.AlphaComposite;
import java.awt.Insets;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JPanel;

public class ExtendedPanel extends JPanel
{
    private final List<MouseListener> mouseListeners;
    private Insets insets;
    private float opacity;
    private AlphaComposite aComp;
    
    public ExtendedPanel(final LayoutManager layout, final boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        this.opacity = 1.0f;
        this.mouseListeners = new ArrayList<MouseListener>();
        this.setOpaque(false);
    }
    
    public ExtendedPanel(final LayoutManager layout) {
        this(layout, true);
    }
    
    public ExtendedPanel(final boolean isDoubleBuffered) {
        this(new FlowLayout(), isDoubleBuffered);
    }
    
    public ExtendedPanel() {
        this(true);
    }
    
    public float getOpacity() {
        return this.opacity;
    }
    
    public void setOpacity(final float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("opacity must be in [0;1]");
        }
        this.opacity = f;
        this.aComp = AlphaComposite.getInstance(3, f);
        this.repaint();
    }
    
    @Override
    public Insets getInsets() {
        return (this.insets == null) ? super.getInsets() : this.insets;
    }
    
    public void setInsets(final Insets insets) {
        this.insets = insets;
    }
    
    @Override
    public Component add(final Component comp) {
        super.add(comp);
        if (comp == null) {
            return null;
        }
        final MouseListener[] compareListeners = comp.getMouseListeners();
        for (MouseListener add : this.mouseListeners) {
            final MouseListener listener = add;
            for (final MouseListener compareListener : compareListeners) {
                if (listener.equals(compareListener)) {
                    add = null;
                    break;
                }
            }
            if (add == null) {
                continue;
            }
            comp.addMouseListener(add);
        }
        return comp;
    }
    
    public void add(final Component... components) {
        if (components == null) {
            throw new NullPointerException();
        }
        for (final Component comp : components) {
            this.add(comp);
        }
    }
    
    public void add(final Component component0, final Component component1) {
        this.add(new Component[] { component0, component1 });
    }
    
    @Override
    public synchronized void addMouseListener(final MouseListener listener) {
        if (listener == null) {
            return;
        }
        this.mouseListeners.add(listener);
        for (final Component comp : this.getComponents()) {
            comp.addMouseListener(listener);
        }
    }
    
    public synchronized void addMouseListenerOriginally(final MouseListener listener) {
        super.addMouseListener(listener);
    }
    
    @Override
    public synchronized void removeMouseListener(final MouseListener listener) {
        if (listener == null) {
            return;
        }
        this.mouseListeners.remove(listener);
        for (final Component comp : this.getComponents()) {
            comp.removeMouseListener(listener);
        }
    }
    
    protected synchronized void removeMouseListenerOriginally(final MouseListener listener) {
        super.removeMouseListener(listener);
    }
    
    public boolean contains(final Component comp) {
        if (comp == null) {
            return false;
        }
        for (final Component c : this.getComponents()) {
            if (comp.equals(c)) {
                return true;
            }
        }
        return false;
    }
    
    public Insets setInsets(final int top, final int left, final int bottom, final int right) {
        final Insets insets = new Insets(top, left, bottom, right);
        this.setInsets(insets);
        return insets;
    }
    
    @Override
    protected void paintComponent(final Graphics g0) {
        if (this.opacity == 1.0f) {
            super.paintComponent(g0);
            return;
        }
        final Graphics2D g = (Graphics2D)g0;
        g.setComposite(this.aComp);
        super.paintComponent(g0);
    }
}
