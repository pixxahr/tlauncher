// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import javax.swing.Icon;

public class HTMLLabel extends ExtendedLabel
{
    private static final long serialVersionUID = -509864367525835474L;
    
    public HTMLLabel(final String text, final Icon icon, final int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }
    
    public HTMLLabel(final String text, final int horizontalAlignment) {
        super(text, horizontalAlignment);
    }
    
    public HTMLLabel(final String text) {
        super(text);
    }
    
    public HTMLLabel(final Icon image, final int horizontalAlignment) {
        super(image, horizontalAlignment);
    }
    
    public HTMLLabel(final Icon image) {
        super(image);
    }
    
    public HTMLLabel() {
    }
    
    @Override
    public void setText(final String text) {
        super.setText("<html>" + ((text == null) ? null : text.replace("\n", "<br/>")) + "</html>");
    }
}
