// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.BorderLayout;

public class BorderPanel extends ExtendedPanel
{
    private static final long serialVersionUID = -7641580330557833990L;
    
    private BorderPanel(BorderLayout layout, final boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        if (layout == null) {
            layout = new BorderLayout();
        }
        this.setLayout(layout);
    }
    
    public BorderPanel() {
        this(null, true);
    }
    
    public BorderPanel(final int hgap, final int vgap) {
        this();
        this.setHgap(hgap);
        this.setVgap(vgap);
    }
    
    @Override
    public BorderLayout getLayout() {
        return (BorderLayout)super.getLayout();
    }
    
    @Override
    public void setLayout(final LayoutManager mgr) {
        if (mgr instanceof BorderLayout) {
            super.setLayout(mgr);
        }
    }
    
    public int getHgap() {
        return this.getLayout().getHgap();
    }
    
    public void setHgap(final int hgap) {
        this.getLayout().setHgap(hgap);
    }
    
    public int getVgap() {
        return this.getLayout().getVgap();
    }
    
    public void setVgap(final int vgap) {
        this.getLayout().setVgap(vgap);
    }
    
    public void setNorth(final Component comp) {
        this.add(comp, "North");
    }
    
    public void setEast(final Component comp) {
        this.add(comp, "East");
    }
    
    public void setSouth(final Component comp) {
        this.add(comp, "South");
    }
    
    public void setWest(final Component comp) {
        this.add(comp, "West");
    }
    
    public void setCenter(final Component comp) {
        this.add(comp, "Center");
    }
}
