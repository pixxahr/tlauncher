// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import javax.swing.plaf.ComponentUI;
import org.tlauncher.tlauncher.ui.swing.util.Orientation;
import javax.swing.JTabbedPane;

public class TabbedPane extends JTabbedPane
{
    public TabbedPane(final Orientation tabLocation, final TabLayout layout) {
        this.setTabLocation((tabLocation == null) ? Orientation.TOP : tabLocation);
        this.setTabLayout((layout == null) ? TabLayout.SCROLL : layout);
    }
    
    public TabbedPane() {
        this(null, null);
    }
    
    public ExtendedUI getExtendedUI() {
        final ComponentUI ui = this.getUI();
        if (ui instanceof ExtendedUI) {
            return (ExtendedUI)ui;
        }
        return null;
    }
    
    public void setTabLocation(final Orientation direction) {
        if (direction == null) {
            throw new NullPointerException();
        }
        this.setTabPlacement(direction.getSwingAlias());
    }
    
    public TabLayout getTabLayout() {
        return TabLayout.fromSwingConstant(this.getTabLayoutPolicy());
    }
    
    public void setTabLayout(final TabLayout layout) {
        if (layout == null) {
            throw new NullPointerException();
        }
        this.setTabLayoutPolicy(layout.getSwingAlias());
    }
    
    public enum TabLayout
    {
        WRAP(0), 
        SCROLL(1);
        
        private final int swingAlias;
        
        private TabLayout(final int swingAlias) {
            this.swingAlias = swingAlias;
        }
        
        public int getSwingAlias() {
            return this.swingAlias;
        }
        
        public static TabLayout fromSwingConstant(final int orientation) {
            for (final TabLayout current : values()) {
                if (orientation == current.getSwingAlias()) {
                    return current;
                }
            }
            return null;
        }
    }
}
