// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import java.awt.event.ComponentEvent;
import java.awt.Component;

public class ExtendedComponentAdapter extends ExtendedComponentListener
{
    public ExtendedComponentAdapter(final Component component, final int tick) {
        super(component, tick);
    }
    
    public ExtendedComponentAdapter(final Component component) {
        super(component);
    }
    
    @Override
    public void componentShown(final ComponentEvent e) {
    }
    
    @Override
    public void componentHidden(final ComponentEvent e) {
    }
    
    @Override
    public void onComponentResizing(final ComponentEvent e) {
    }
    
    @Override
    public void onComponentResized(final ComponentEvent e) {
    }
    
    @Override
    public void onComponentMoving(final ComponentEvent e) {
    }
    
    @Override
    public void onComponentMoved(final ComponentEvent e) {
    }
}
