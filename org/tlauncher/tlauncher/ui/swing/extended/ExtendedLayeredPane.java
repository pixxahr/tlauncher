// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.swing.ResizeableComponent;
import org.tlauncher.tlauncher.ui.block.BlockableLayeredPane;

public abstract class ExtendedLayeredPane extends BlockableLayeredPane implements ResizeableComponent
{
    private static final long serialVersionUID = -1L;
    private Integer LAYER_COUNT;
    protected final JComponent parent;
    
    protected ExtendedLayeredPane() {
        this.LAYER_COUNT = 0;
        this.parent = null;
    }
    
    protected ExtendedLayeredPane(final JComponent parent) {
        this.LAYER_COUNT = 0;
        this.parent = parent;
        if (parent == null) {
            return;
        }
        parent.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(final ComponentEvent e) {
                ExtendedLayeredPane.this.onResize();
            }
            
            @Override
            public void componentMoved(final ComponentEvent e) {
            }
            
            @Override
            public void componentShown(final ComponentEvent e) {
                ExtendedLayeredPane.this.onResize();
            }
            
            @Override
            public void componentHidden(final ComponentEvent e) {
            }
        });
    }
    
    @Override
    public Component add(final Component comp) {
        final Integer layer_COUNT = this.LAYER_COUNT;
        ++this.LAYER_COUNT;
        super.add(comp, layer_COUNT);
        return comp;
    }
    
    public void add(final Component... components) {
        if (components == null) {
            throw new NullPointerException();
        }
        for (final Component comp : components) {
            this.add(comp);
        }
    }
    
    @Override
    public void onResize() {
        if (this.parent == null) {
            return;
        }
        this.setSize(this.parent.getWidth(), this.parent.getHeight());
        for (final Component comp : this.getComponents()) {
            if (comp instanceof ResizeableComponent) {
                ((ResizeableComponent)comp).onResize();
            }
        }
    }
}
