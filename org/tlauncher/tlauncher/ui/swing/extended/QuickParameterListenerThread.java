// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.extended;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.swing.util.IntegerArrayGetter;
import org.tlauncher.util.async.LoopedThread;

public class QuickParameterListenerThread extends LoopedThread
{
    public static final int DEFAULT_TICK = 500;
    private final IntegerArrayGetter paramGetter;
    private final Runnable runnable;
    private final int tick;
    
    QuickParameterListenerThread(final IntegerArrayGetter getter, final Runnable run, final int tick) {
        super("QuickParameterListenerThread");
        if (getter == null) {
            throw new NullPointerException("Getter is NULL!");
        }
        if (run == null) {
            throw new NullPointerException("Runnable is NULL!");
        }
        if (tick < 0) {
            throw new IllegalArgumentException("Tick must be positive!");
        }
        this.paramGetter = getter;
        this.runnable = run;
        this.tick = tick;
        this.setPriority(1);
        this.startAndWait();
    }
    
    QuickParameterListenerThread(final IntegerArrayGetter getter, final Runnable run) {
        this(getter, run, 500);
    }
    
    void startListening() {
        this.iterate();
    }
    
    @Override
    protected void iterateOnce() {
        int[] initial = this.paramGetter.getIntegerArray();
        int i = 0;
        boolean equal;
        do {
            this.sleep();
            final int[] newvalue = this.paramGetter.getIntegerArray();
            equal = true;
            for (i = 0; i < initial.length; ++i) {
                if (initial[i] != newvalue[i]) {
                    equal = false;
                }
            }
            initial = newvalue;
        } while (!equal);
        this.runnable.run();
    }
    
    private void sleep() {
        U.sleepFor(this.tick);
    }
}
