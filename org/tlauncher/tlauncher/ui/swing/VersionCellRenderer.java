// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import org.tlauncher.tlauncher.ui.images.ImageCache;
import net.minecraft.launcher.versions.ReleaseType;
import net.minecraft.launcher.updater.LatestVersionSyncInfo;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Objects;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.Icon;
import java.awt.Color;
import javax.swing.DefaultListCellRenderer;
import net.minecraft.launcher.updater.VersionSyncInfo;
import javax.swing.ListCellRenderer;

public class VersionCellRenderer implements ListCellRenderer<VersionSyncInfo>
{
    public static final VersionSyncInfo LOADING;
    public static final VersionSyncInfo EMPTY;
    private final DefaultListCellRenderer defaultRenderer;
    private final int averageColor;
    public static final Color DARK_COLOR_TEXT;
    public static final Color OVER_ITEM;
    private static final Icon TLAUNCHER_ICON;
    
    public VersionCellRenderer() {
        this.defaultRenderer = new DefaultListCellRenderer();
        this.averageColor = new Color(128, 128, 128, 255).getRGB();
    }
    
    @Override
    public Component getListCellRendererComponent(final JList<? extends VersionSyncInfo> list, final VersionSyncInfo value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        final JLabel mainText = (JLabel)this.defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        mainText.setAlignmentY(0.5f);
        mainText.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 0));
        mainText.setOpaque(true);
        if (isSelected) {
            mainText.setBackground(VersionCellRenderer.OVER_ITEM);
        }
        else {
            mainText.setBackground(Color.white);
        }
        mainText.setForeground(VersionCellRenderer.DARK_COLOR_TEXT);
        if (value == null) {
            mainText.setText("(null)");
            return mainText;
        }
        if (value.equals(VersionCellRenderer.LOADING)) {
            mainText.setText(Localizable.get("versions.loading"));
        }
        else if (value.equals(VersionCellRenderer.EMPTY)) {
            mainText.setText(Localizable.get("versions.notfound.tip"));
        }
        else {
            mainText.setText(getLabelFor(value));
            if (!value.isInstalled()) {
                mainText.setBackground(U.shiftColor(mainText.getBackground(), (mainText.getBackground().getRGB() < this.averageColor) ? 32 : -32));
            }
        }
        if (Objects.nonNull(value.getAvailableVersion())) {
            final boolean skin = TLauncher.getInstance().getTLauncherManager().useTlauncherAuthlib(value.getAvailableVersion());
            if (skin) {
                mainText.setIcon(VersionCellRenderer.TLAUNCHER_ICON);
            }
        }
        return mainText;
    }
    
    public static String getLabelFor(final VersionSyncInfo value) {
        final LatestVersionSyncInfo asLatest = (value instanceof LatestVersionSyncInfo) ? ((LatestVersionSyncInfo)value) : null;
        final ReleaseType type = value.getAvailableVersion().getReleaseType();
        String id;
        String label;
        if (asLatest == null) {
            id = value.getID();
            label = "version." + type;
        }
        else {
            id = asLatest.getVersionID();
            label = "version.latest." + type;
        }
        label = Localizable.nget(label);
        if (type != null) {
            switch (type) {
                case OLD_ALPHA: {
                    id = (id.startsWith("a") ? id.substring(1) : id);
                    break;
                }
                case OLD_BETA: {
                    id = id.substring(1);
                    break;
                }
            }
        }
        final String text = (label != null) ? (label + " " + id) : id;
        return text;
    }
    
    public boolean getShowTLauncherVersions() {
        return false;
    }
    
    static {
        LOADING = VersionSyncInfo.createEmpty();
        EMPTY = VersionSyncInfo.createEmpty();
        DARK_COLOR_TEXT = new Color(77, 77, 77);
        OVER_ITEM = new Color(235, 235, 235);
        TLAUNCHER_ICON = (Icon)ImageCache.getIcon("tlauncher-user.png");
    }
}
