// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import org.tlauncher.tlauncher.ui.converter.StringConverter;
import javax.swing.ListCellRenderer;

public abstract class ConverterCellRenderer<T> implements ListCellRenderer<T>
{
    protected final StringConverter<T> converter;
    
    ConverterCellRenderer(final StringConverter<T> converter) {
        if (converter == null) {
            throw new NullPointerException();
        }
        this.converter = converter;
    }
    
    public StringConverter<T> getConverter() {
        return this.converter;
    }
}
