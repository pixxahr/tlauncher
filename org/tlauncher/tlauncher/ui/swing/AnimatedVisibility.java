// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

public interface AnimatedVisibility
{
    void setShown(final boolean p0);
    
    void setShown(final boolean p0, final boolean p1);
}
