// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.notification.skin;

import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.JCheckBox;
import java.awt.FlowLayout;
import org.tlauncher.tlauncher.ui.swing.editor.EditorPane;
import org.tlauncher.tlauncher.ui.swing.FlexibleEditorPanel;
import java.awt.Font;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.GridLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import javax.swing.JPanel;

public class SkinNotification extends JPanel
{
    private static final int WIDTH = 400;
    
    public SkinNotification() {
        this.setLayout(new BorderLayout(0, 0));
        final JPanel panel = new JPanel();
        this.add(panel, "West");
        panel.setLayout(new GridLayout(1, 0, 0, 0));
        final JLabel lblNewLabel = new JLabel((Icon)ImageCache.getIcon("notification-picture.png"));
        lblNewLabel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
        panel.add(lblNewLabel);
        final JPanel panel_1 = new JPanel();
        this.add(panel_1, "Center");
        panel_1.setLayout(new BoxLayout(panel_1, 1));
        final JPanel titlePanel = new JPanel();
        panel_1.add(titlePanel);
        final JLabel title = new JLabel(Localizable.get("skin.notification.up.title"));
        final Font font = title.getFont();
        title.setFont(new Font(font.getName(), font.getStyle(), 16));
        titlePanel.add(title);
        final JPanel commonInformation = new JPanel();
        panel_1.add(commonInformation);
        final FlexibleEditorPanel commonInformationLabel = new FlexibleEditorPanel("text/html", Localizable.get("skin.notification.common.message"), 400);
        commonInformation.add(commonInformationLabel);
        final JPanel TlIconPanel = new JPanel();
        panel_1.add(TlIconPanel);
        final String textImage = Localizable.get("skin.notification.image.explanation").replaceFirst("TlIcon", ImageCache.getRes("tlauncher-user.png").toExternalForm());
        final FlexibleEditorPanel flexibleEditorPanel = new FlexibleEditorPanel("text/html", textImage, 400);
        TlIconPanel.add(flexibleEditorPanel);
        final JPanel detailInformation = new JPanel();
        panel_1.add(detailInformation);
        final EditorPane detailSkinLink = new EditorPane("text/html", Localizable.get("skin.notification.link.message"));
        detailInformation.add(detailSkinLink);
        final JPanel boxPanel = new JPanel();
        panel_1.add(boxPanel);
        boxPanel.setLayout(new FlowLayout(1, 5, 5));
        final JCheckBox notificationState = new JCheckBox(Localizable.get("skin.notification.state"));
        boxPanel.add(notificationState);
        notificationState.addActionListener(e -> {
            if (notificationState.isSelected()) {
                TLauncher.getInstance().getConfiguration().set("skin.notification.off", "true");
            }
        });
    }
    
    public static void showMessage() {
        final SkinNotification notification = new SkinNotification();
        Alert.showCustomMonolog(Localizable.get("skin.notification.title"), notification);
    }
}
