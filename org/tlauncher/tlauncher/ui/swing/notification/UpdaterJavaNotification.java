// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing.notification;

import java.io.IOException;
import java.util.Locale;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import java.util.ArrayList;
import org.tlauncher.tlauncher.ui.swing.FlexibleEditorPanel;
import org.tlauncher.util.OS;
import java.awt.GridLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import org.tlauncher.tlauncher.configuration.LangConfiguration;
import java.awt.Dimension;
import javax.swing.JPanel;

public class UpdaterJavaNotification extends JPanel
{
    private final Dimension DIMENSION;
    
    public UpdaterJavaNotification(final LangConfiguration configuration) {
        this.DIMENSION = new Dimension(500, 100);
        this.setLayout(new BoxLayout(this, 1));
        final JPanel messagePanel = new JPanel();
        this.add(messagePanel);
        messagePanel.setLayout(new GridLayout(0, 1, 0, 0));
        final String userMessage = OS.is(OS.WINDOWS) ? configuration.get("updater.java.notification.message") : configuration.get("updater.java.notification.message.special");
        final FlexibleEditorPanel message = new FlexibleEditorPanel("text/html", userMessage, this.DIMENSION.width);
        messagePanel.add(message);
    }
    
    public static int showUpdaterJavaNotfication(final LangConfiguration configuration) {
        final List<Object> buttons = new ArrayList<Object>(3);
        if (OS.is(OS.WINDOWS)) {
            buttons.add(configuration.get("updater.java.install.button"));
        }
        buttons.add(configuration.get("updater.java.myself.button"));
        buttons.add(configuration.get("updater.java.reminder.button"));
        final JFrame f = new JFrame();
        f.setAlwaysOnTop(true);
        int res = JOptionPane.showOptionDialog(f, new UpdaterJavaNotification(configuration), configuration.get("updater.java.notification.title"), -1, 2, null, buttons.toArray(), null);
        if (!OS.is(OS.WINDOWS) && res > -1) {
            ++res;
        }
        return res;
    }
    
    public static void main(final String[] args) throws IOException {
        final LangConfiguration langConfiguration = new LangConfiguration(new Locale[] { new Locale("ru", "Ru") }, new Locale("ru", "Ru"), "/lang/tlauncher/");
        showUpdaterJavaNotfication(langConfiguration);
    }
}
