// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.util.SwingUtil;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JFrame;

public class TemplateTLauncherFrame extends JFrame
{
    protected JPanel contentPane;
    public static final int MAX_WIDTH_ELEMENT = 500;
    protected JFrame parent;
    
    public TemplateTLauncherFrame(final JFrame parent, final String title) {
        this.setBounds(100, 100, 400, 366);
        this.parent = parent;
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(false);
        this.setResizable(false);
        SwingUtil.setFavicons(this);
        this.setTitle(Localizable.get(title));
        parent.setEnabled(false);
    }
    
    @Override
    public void setVisible(final boolean b) {
        this.parent.setEnabled(!b);
        super.setVisible(b);
    }
}
