// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.awt.Dimension;
import org.tlauncher.util.U;
import java.awt.Composite;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.image.ImageObserver;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class ImagePanel extends ExtendedPanel
{
    private static final long serialVersionUID = 1L;
    public static final float DEFAULT_ACTIVE_OPACITY = 1.0f;
    public static final float DEFAULT_NON_ACTIVE_OPACITY = 0.75f;
    protected final Object animationLock;
    private Image originalImage;
    private Image image;
    private float activeOpacity;
    private float nonActiveOpacity;
    private boolean antiAlias;
    private int timeFrame;
    private float opacity;
    private boolean hover;
    private boolean shown;
    private boolean animating;
    
    public ImagePanel(final String image, final float activeOpacity, final float nonActiveOpacity, final boolean shown) {
        this(ImageCache.getImage(image), activeOpacity, nonActiveOpacity, shown);
    }
    
    public ImagePanel(final String image) {
        this(image, 1.0f, 0.75f, true);
    }
    
    public ImagePanel(final Image image, final float activeOpacity, final float nonActiveOpacity, final boolean shown) {
        this.animationLock = new Object();
        this.setImage(image);
        this.setActiveOpacity(activeOpacity);
        this.setNonActiveOpacity(nonActiveOpacity);
        this.shown = shown;
        this.opacity = (shown ? nonActiveOpacity : 0.0f);
        this.timeFrame = 10;
        this.setBackground(new Color(0, 0, 0, 0));
        this.addMouseListenerOriginally(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                ImagePanel.this.onClick();
            }
            
            @Override
            public void mouseEntered(final MouseEvent e) {
                ImagePanel.this.onMouseEntered();
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                ImagePanel.this.onMouseExited();
            }
        });
    }
    
    public void setImage(final Image image, final boolean resetSize) {
        synchronized (this.animationLock) {
            this.originalImage = image;
            this.image = image;
            if (resetSize && image != null) {
                this.setSize(image.getWidth(null), image.getHeight(null));
            }
        }
    }
    
    protected void setImage(final Image image) {
        this.setImage(image, true);
    }
    
    protected void setActiveOpacity(final float opacity) {
        if (opacity > 1.0f || opacity < 0.0f) {
            throw new IllegalArgumentException("Invalid opacity! Condition: 0.0F <= opacity (got: " + opacity + ") <= 1.0F");
        }
        this.activeOpacity = opacity;
    }
    
    protected void setNonActiveOpacity(final float opacity) {
        if (opacity > 1.0f || opacity < 0.0f) {
            throw new IllegalArgumentException("Invalid opacity! Condition: 0.0F <= opacity (got: " + opacity + ") <= 1.0F");
        }
        this.nonActiveOpacity = opacity;
    }
    
    public void paintComponent(final Graphics g0) {
        if (this.image == null) {
            return;
        }
        final Graphics2D g = (Graphics2D)g0;
        final Composite oldComp = g.getComposite();
        g.setComposite(AlphaComposite.getInstance(3, this.opacity));
        g.drawImage(this.image, 0, 0, this.getWidth(), this.getHeight(), null);
        g.setComposite(oldComp);
    }
    
    @Override
    public void show() {
        if (this.shown) {
            return;
        }
        this.shown = true;
        synchronized (this.animationLock) {
            this.setVisible(this.animating = true);
            this.opacity = 0.0f;
            final float selectedOpacity = this.hover ? this.activeOpacity : this.nonActiveOpacity;
            while (this.opacity < selectedOpacity) {
                this.opacity += 0.01f;
                if (this.opacity > selectedOpacity) {
                    this.opacity = selectedOpacity;
                }
                this.repaint();
                U.sleepFor(this.timeFrame);
            }
            this.animating = false;
        }
    }
    
    @Override
    public void hide() {
        if (!this.shown) {
            return;
        }
        this.shown = false;
        synchronized (this.animationLock) {
            this.animating = true;
            while (this.opacity > 0.0f) {
                this.opacity -= 0.01f;
                if (this.opacity < 0.0f) {
                    this.opacity = 0.0f;
                }
                this.repaint();
                U.sleepFor(this.timeFrame);
            }
            this.setVisible(false);
            this.animating = false;
        }
    }
    
    public void setPreferredSize() {
        if (this.image == null) {
            return;
        }
        this.setPreferredSize(new Dimension(this.image.getWidth(null), this.image.getHeight(null)));
    }
    
    protected boolean onClick() {
        return this.shown;
    }
    
    protected void onMouseEntered() {
        this.hover = true;
        if (this.animating || !this.shown) {
            return;
        }
        this.opacity = this.activeOpacity;
        this.repaint();
    }
    
    protected void onMouseExited() {
        this.hover = false;
        if (this.animating || !this.shown) {
            return;
        }
        this.opacity = this.nonActiveOpacity;
        this.repaint();
    }
}
