// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.swing;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public abstract class CheckBoxListener implements ItemListener
{
    @Override
    public void itemStateChanged(final ItemEvent e) {
        this.itemStateChanged(e.getStateChange() == 1);
    }
    
    public abstract void itemStateChanged(final boolean p0);
}
