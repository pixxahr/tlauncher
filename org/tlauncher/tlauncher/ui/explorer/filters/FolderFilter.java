// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.explorer.filters;

import java.io.File;

public class FolderFilter extends CommonFilter
{
    @Override
    public boolean accept(final File pathname) {
        return pathname.isDirectory();
    }
    
    @Override
    public boolean accept(final File dir, final String name) {
        return new File(dir, name).isDirectory();
    }
    
    @Override
    public String getDescription() {
        return "";
    }
}
