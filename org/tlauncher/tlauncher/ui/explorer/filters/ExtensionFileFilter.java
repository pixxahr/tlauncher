// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.explorer.filters;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.util.FileUtil;
import java.io.File;
import javax.swing.filechooser.FileFilter;

public class ExtensionFileFilter extends FileFilter
{
    private final String extension;
    private final boolean acceptNull;
    
    public ExtensionFileFilter(final String extension, final boolean acceptNullExtension) {
        if (extension == null) {
            throw new NullPointerException("Extension is NULL!");
        }
        if (extension.isEmpty()) {
            throw new IllegalArgumentException("Extension is empty!");
        }
        this.extension = extension;
        this.acceptNull = acceptNullExtension;
    }
    
    public ExtensionFileFilter(final String extension) {
        this(extension, true);
    }
    
    public String getExtension() {
        return this.extension;
    }
    
    public boolean acceptsNull() {
        return this.acceptNull;
    }
    
    @Override
    public boolean accept(final File f) {
        final String currentExtension = FileUtil.getExtension(f);
        return (this.acceptNull && currentExtension == null) || this.extension.equals(currentExtension);
    }
    
    @Override
    public String getDescription() {
        return Localizable.get("explorer.extension.format", this.extension.toUpperCase());
    }
}
