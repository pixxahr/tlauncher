// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.explorer.filters;

import java.io.File;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FilesAndExtentionFilter extends FileFilter
{
    private FileNameExtensionFilter extensionFilter;
    private String s1;
    
    public FilesAndExtentionFilter(final String s1, final String... s2) {
        this.s1 = s1;
        this.extensionFilter = new FileNameExtensionFilter(s1, s2);
    }
    
    @Override
    public boolean accept(final File f) {
        return f.isDirectory() || (super.accept(f) && this.extensionFilter.accept(f));
    }
    
    @Override
    public boolean accept(final File dir, final String name) {
        return new File(dir, name).isDirectory() || (super.accept(dir, name) && this.extensionFilter.accept(new File(dir, name)));
    }
    
    @Override
    public String getDescription() {
        return this.s1;
    }
}
