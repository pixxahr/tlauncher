// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.explorer.filters;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.util.FileUtil;
import java.io.File;
import java.util.regex.Pattern;
import javax.swing.filechooser.FileFilter;

public class ImageFileFilter extends FileFilter
{
    public static final Pattern extensionPattern;
    
    @Override
    public boolean accept(final File f) {
        final String extension = FileUtil.getExtension(f);
        return extension == null || ImageFileFilter.extensionPattern.matcher(extension).matches();
    }
    
    @Override
    public String getDescription() {
        return Localizable.get("explorer.type.image");
    }
    
    static {
        extensionPattern = Pattern.compile("^(?:jp(?:e|)g|png)$", 2);
    }
}
