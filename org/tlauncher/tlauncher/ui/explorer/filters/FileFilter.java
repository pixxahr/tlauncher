// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.explorer.filters;

import java.io.File;

public class FileFilter extends CommonFilter
{
    @Override
    public boolean accept(final File dir, final String name) {
        return new File(dir, name).isFile();
    }
    
    @Override
    public boolean accept(final File f) {
        return f.isFile();
    }
    
    @Override
    public String getDescription() {
        return "";
    }
}
