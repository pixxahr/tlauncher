// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.explorer;

import java.io.File;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.explorer.filters.CommonFilter;

public interface FileChooser
{
    void setFileFilter(final CommonFilter p0);
    
    int showSaveDialog(final Component p0);
    
    File getSelectedFile();
    
    void setSelectedFile(final File p0);
    
    void setCurrentDirectory(final File p0);
    
    int showDialog(final Component p0);
    
    File[] getSelectedFiles();
    
    void setMultiSelectionEnabled(final boolean p0);
    
    int showDialog(final Component p0, final String p1);
    
    void setDialogTitle(final String p0);
    
    void setFileSelectionMode(final int p0);
}
