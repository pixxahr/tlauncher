// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.explorer;

import java.util.Objects;
import java.io.FilenameFilter;
import java.awt.Dialog;
import javax.swing.JDialog;
import java.awt.Frame;
import javax.swing.JFrame;
import java.awt.Component;
import java.io.File;
import java.awt.FileDialog;
import org.tlauncher.tlauncher.ui.explorer.filters.CommonFilter;

public class FileWrapper implements FileChooser
{
    private CommonFilter filter;
    private FileDialog dialog;
    private File directory;
    private File selectedFile;
    private boolean multiSelectionMode;
    private String title;
    
    public FileWrapper() {
        this.multiSelectionMode = false;
    }
    
    private FileDialog create(final Component component) {
        if (component instanceof JFrame) {
            this.dialog = new FileDialog((Frame)component);
        }
        else if (component instanceof JDialog) {
            this.dialog = new FileDialog((Dialog)component);
        }
        else {
            this.dialog = new FileDialog(new JFrame());
        }
        this.dialog.setAlwaysOnTop(true);
        this.dialog.setFilenameFilter(this.filter);
        if (Objects.nonNull(this.selectedFile)) {
            this.dialog.setFile(this.selectedFile.getAbsolutePath());
        }
        if (Objects.nonNull(this.directory)) {
            this.dialog.setDirectory(this.directory.getAbsolutePath());
        }
        this.dialog.setMultipleMode(this.multiSelectionMode);
        if (Objects.nonNull(this.title)) {
            this.dialog.setTitle(this.title);
        }
        return this.dialog;
    }
    
    @Override
    public int showSaveDialog(final Component component) {
        (this.dialog = this.create(component)).setMode(1);
        this.dialog.show();
        if (Objects.nonNull(this.dialog.getFile())) {
            return 0;
        }
        return 1;
    }
    
    @Override
    public File getSelectedFile() {
        return new File(this.dialog.getDirectory(), this.dialog.getFile());
    }
    
    @Override
    public void setSelectedFile(final File file) {
        this.selectedFile = file;
    }
    
    @Override
    public void setCurrentDirectory(final File file) {
        this.directory = file;
    }
    
    @Override
    public int showDialog(final Component parent) {
        (this.dialog = this.create(parent)).setMode(0);
        this.dialog.show();
        if (Objects.nonNull(this.dialog.getFile())) {
            return 0;
        }
        return 1;
    }
    
    @Override
    public File[] getSelectedFiles() {
        return this.dialog.getFiles();
    }
    
    @Override
    public void setFileFilter(final CommonFilter filter) {
        this.filter = filter;
    }
    
    @Override
    public void setMultiSelectionEnabled(final boolean b) {
        this.multiSelectionMode = b;
    }
    
    @Override
    public int showDialog(final Component component, final String s) {
        this.setDialogTitle(s);
        return this.showDialog(component);
    }
    
    @Override
    public void setDialogTitle(final String s) {
        this.title = s;
    }
    
    @Override
    public void setFileSelectionMode(final int mode) {
    }
}
