// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.explorer;

import javax.swing.UIManager;
import java.awt.Component;
import java.io.File;
import javax.swing.filechooser.FileFilter;
import org.tlauncher.tlauncher.ui.explorer.filters.CommonFilter;
import javax.swing.JFileChooser;

public class FileExplorer extends JFileChooser implements FileChooser
{
    private static final long serialVersionUID = 3826379908958645663L;
    
    @Override
    public void setFileFilter(final CommonFilter filter) {
        super.setFileFilter(filter);
    }
    
    @Override
    public void setCurrentDirectory(File dir) {
        if (dir == null) {
            dir = this.getFileSystemView().getDefaultDirectory();
        }
        super.setCurrentDirectory(dir);
    }
    
    @Override
    public int showDialog(final Component parent) {
        return this.showDialog(parent, UIManager.getString("FileChooser.directoryOpenButtonText"));
    }
    
    @Override
    public File[] getSelectedFiles() {
        final File[] selectedFiles = super.getSelectedFiles();
        if (selectedFiles.length > 0) {
            return selectedFiles;
        }
        final File selectedFile = super.getSelectedFile();
        if (selectedFile == null) {
            return null;
        }
        return new File[] { selectedFile };
    }
    
    @Override
    public void setFileSelectionMode(final int mode) {
        super.setFileSelectionMode(mode);
    }
}
