// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui;

import org.tlauncher.util.SwingUtil;
import java.awt.Point;
import org.tlauncher.tlauncher.ui.login.LoginForm;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import org.tlauncher.tlauncher.downloader.DownloaderListener;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.plaf.ProgressBarUI;
import org.tlauncher.tlauncher.ui.ui.FancyProgressBar;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.progress.login.LauncherProgress;
import java.awt.Component;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.swing.progress.ProgressBarPanel;
import org.tlauncher.tlauncher.ui.scenes.PseudoScene;
import org.tlauncher.tlauncher.ui.scenes.AccountEditorScene;
import org.tlauncher.tlauncher.ui.scenes.AdditionalHostServerScene;
import org.tlauncher.tlauncher.ui.scenes.ModpackEnitityScene;
import org.tlauncher.tlauncher.ui.scenes.CompleteSubEntityScene;
import org.tlauncher.tlauncher.ui.scenes.ModpackScene;
import org.tlauncher.tlauncher.ui.scenes.SettingsScene;
import org.tlauncher.tlauncher.ui.scenes.VersionManagerScene;
import org.tlauncher.tlauncher.ui.scenes.DefaultScene;
import org.tlauncher.tlauncher.ui.browser.BrowserHolder;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedLayeredPane;

public class MainPane extends ExtendedLayeredPane
{
    public static final Dimension SIZE;
    public final BrowserHolder browser;
    public final DefaultScene defaultScene;
    public final VersionManagerScene versionManager;
    public final SettingsScene settingsScene;
    public final ModpackScene modpackScene;
    public final CompleteSubEntityScene completeSubEntityScene;
    public final ModpackEnitityScene modpackEnitityScene;
    public final AdditionalHostServerScene additionalHostServerScene;
    public final SideNotifier notifier;
    public final AccountEditorScene accountEditor;
    private final TLauncherFrame rootFrame;
    private PseudoScene scene;
    private ProgressBarPanel barPanel;
    
    MainPane(final TLauncherFrame frame) {
        super(null);
        this.setPreferredSize(MainPane.SIZE);
        this.setMaximumSize(MainPane.SIZE);
        this.rootFrame = frame;
        (this.browser = BrowserHolder.getInstance()).setPane(this);
        this.add(this.browser);
        (this.notifier = new SideNotifier()).setLocation(10, 10);
        this.notifier.setSize(32, 32);
        this.add(this.notifier);
        this.add(this.defaultScene = new DefaultScene(this));
        this.add(this.versionManager = new VersionManagerScene(this));
        this.add(this.modpackScene = new ModpackScene(this));
        this.add(this.completeSubEntityScene = new CompleteSubEntityScene(this));
        this.add(this.modpackEnitityScene = new ModpackEnitityScene(this));
        this.add(this.additionalHostServerScene = new AdditionalHostServerScene(this));
        final LauncherProgress bar = new LauncherProgress();
        bar.setBorder(BorderFactory.createEmptyBorder());
        bar.setUI(new FancyProgressBar(ImageCache.getBufferedImage("login-progress-bar.png")));
        bar.setOpaque(false);
        this.barPanel = new ProgressBarPanel(ImageCache.getNativeIcon("speed-icon.png"), ImageCache.getNativeIcon("files-icon.png"), bar);
        TLauncher.getInstance().getDownloader().addListener(this.barPanel);
        this.add(this.barPanel);
        this.add(this.accountEditor = new AccountEditorScene(this));
        this.add(this.settingsScene = new SettingsScene(this));
        this.setScene(this.defaultScene, false);
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(final ComponentEvent e) {
                MainPane.this.onResize();
            }
        });
    }
    
    public PseudoScene getScene() {
        return this.scene;
    }
    
    public void setScene(final PseudoScene scene) {
        this.setScene(scene, true);
    }
    
    private void setScene(final PseudoScene newscene, final boolean animate) {
        if (newscene == null) {
            throw new NullPointerException();
        }
        if (newscene.equals(this.scene)) {
            return;
        }
        for (final Component comp : this.getComponents()) {
            if (newscene != this.additionalHostServerScene) {
                if (!comp.equals(newscene) && comp instanceof PseudoScene && comp != this.defaultScene) {
                    ((PseudoScene)comp).setShown(false, animate);
                }
            }
            else if (!comp.equals(newscene) && comp instanceof PseudoScene) {
                ((PseudoScene)comp).setShown(false, animate);
            }
        }
        (this.scene = newscene).setShown(true);
        this.browser.setBrowserContentShown("scene", this.scene.equals(this.defaultScene));
    }
    
    public void openDefaultScene() {
        this.setScene(this.defaultScene);
    }
    
    public TLauncherFrame getRootFrame() {
        return this.rootFrame;
    }
    
    public ProgressBarPanel getProgress() {
        return this.barPanel;
    }
    
    @Override
    public void onResize() {
        this.browser.onResize();
        this.barPanel.setBounds(0, this.getHeight() - ProgressBarPanel.SIZE.height - LoginForm.LOGIN_SIZE.height, ProgressBarPanel.SIZE.width, ProgressBarPanel.SIZE.height);
    }
    
    public Point getLocationOf(final Component comp) {
        return SwingUtil.getRelativeLocation(this, comp);
    }
    
    public void openAccountEditor() {
        this.setScene(this.accountEditor);
    }
    
    static {
        SIZE = new Dimension(1050, 655);
    }
}
