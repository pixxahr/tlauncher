// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.updater;

import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.ui.alert.Alert;
import javax.swing.JButton;
import org.tlauncher.tlauncher.controller.UpdaterFormController;
import java.util.Iterator;
import javax.swing.JScrollPane;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import org.tlauncher.tlauncher.ui.swing.OwnImageCheckBox;
import org.tlauncher.tlauncher.updater.client.PointOffer;
import org.tlauncher.tlauncher.updater.client.Offer;
import java.net.MalformedURLException;
import java.awt.event.MouseListener;
import org.tlauncher.util.OS;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.Icon;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.images.ImageIcon;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.net.URL;
import org.tlauncher.tlauncher.updater.client.Banner;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import org.tlauncher.tlauncher.ui.swing.HtmlTextPane;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.JLabel;
import java.util.ArrayList;
import org.tlauncher.tlauncher.ui.loc.RoundUpdaterButton;
import java.awt.Color;
import org.tlauncher.tlauncher.updater.client.Update;
import javax.swing.JCheckBox;
import java.util.List;
import org.tlauncher.tlauncher.ui.swing.ImagePanel;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import java.awt.Dimension;
import javax.swing.JPanel;

public class UpdaterMessageView extends JPanel
{
    private static final Dimension SIZE;
    private final UpdaterButton ok;
    private UpdaterButton updater;
    private UpdaterButton laterUpdater;
    private JPanel down;
    private ImagePanel imageTop;
    private int result;
    private List<JCheckBox> checkBoxList;
    
    public UpdaterMessageView(final Update update, final int messageType, final String lang, final boolean isAdmin) {
        this.ok = new RoundUpdaterButton(Color.WHITE, new Color(107, 202, 45), new Color(91, 174, 37), "launcher.update.updater.button");
        this.updater = new RoundUpdaterButton(Color.WHITE, new Color(107, 202, 45), new Color(91, 174, 37), "launcher.update.updater.button");
        this.laterUpdater = new RoundUpdaterButton(Color.WHITE, new Color(235, 132, 46), new Color(200, 112, 38), "launcher.update.later.button");
        this.down = new JPanel();
        this.checkBoxList = new ArrayList<JCheckBox>();
        this.setPreferredSize(UpdaterMessageView.SIZE);
        String image;
        if (messageType == 2) {
            image = "offer.png";
        }
        else if (messageType == 1) {
            image = "banner.png";
        }
        else {
            image = "without-banner-offer.png";
        }
        this.imageTop = new ImagePanel(image, 1.0f, 1.0f, true);
        final JLabel tlauncher = new JLabel("TLAUNCHER " + update.getVersion());
        final JScrollPane message = HtmlTextPane.createNew(Localizable.get(update.isMandatory() ? "launcher.update.message.mandatory" : "launcher.update.message.optional"), 246);
        final JScrollPane changes = HtmlTextPane.createNew(update.getDescription(), 246);
        final JScrollPane notice = HtmlTextPane.createNew(Localizable.get("updater.notice"), 700);
        final SpringLayout spring = new SpringLayout();
        final SpringLayout topSpring = new SpringLayout();
        final SpringLayout downSpring = new SpringLayout();
        this.setLayout(spring);
        this.imageTop.setLayout(topSpring);
        this.down.setLayout(downSpring);
        spring.putConstraint("West", this.imageTop, 0, "West", this);
        spring.putConstraint("East", this.imageTop, 0, "East", this);
        spring.putConstraint("North", this.imageTop, 0, "North", this);
        spring.putConstraint("South", this.imageTop, 453, "North", this);
        this.add(this.imageTop);
        spring.putConstraint("West", this.down, 0, "West", this);
        spring.putConstraint("East", this.down, 0, "East", this);
        spring.putConstraint("North", this.down, 453, "North", this);
        spring.putConstraint("South", this.down, 600, "North", this);
        this.add(this.down);
        topSpring.putConstraint("West", tlauncher, 40, "West", this.imageTop);
        topSpring.putConstraint("East", tlauncher, 286, "West", this.imageTop);
        topSpring.putConstraint("North", tlauncher, 37, "North", this.imageTop);
        topSpring.putConstraint("South", tlauncher, 67, "North", this.imageTop);
        this.imageTop.add(tlauncher);
        topSpring.putConstraint("West", message, 40, "West", this.imageTop);
        topSpring.putConstraint("East", message, 286, "West", this.imageTop);
        topSpring.putConstraint("North", message, 60, "North", this.imageTop);
        topSpring.putConstraint("South", message, 160, "North", this.imageTop);
        this.imageTop.add(message);
        topSpring.putConstraint("West", changes, 40, "West", this.imageTop);
        topSpring.putConstraint("East", changes, 286, "West", this.imageTop);
        topSpring.putConstraint("North", changes, 144, "North", this.imageTop);
        topSpring.putConstraint("South", changes, -40, "South", this.imageTop);
        this.imageTop.add(changes);
        downSpring.putConstraint("West", notice, 40, "West", this.down);
        downSpring.putConstraint("East", notice, 0, "East", this.down);
        downSpring.putConstraint("North", notice, 5, "North", this.down);
        downSpring.putConstraint("South", notice, 65, "North", this.down);
        this.down.add(notice);
        if (update.isMandatory()) {
            downSpring.putConstraint("West", this.ok, 365, "West", this.down);
            downSpring.putConstraint("East", this.ok, 535, "West", this.down);
            downSpring.putConstraint("North", this.ok, 86, "North", this.down);
            downSpring.putConstraint("South", this.ok, 123, "North", this.down);
            this.down.add(this.ok);
        }
        else {
            downSpring.putConstraint("West", this.updater, 273, "West", this.down);
            downSpring.putConstraint("East", this.updater, 443, "West", this.down);
            downSpring.putConstraint("North", this.updater, 86, "North", this.down);
            downSpring.putConstraint("South", this.updater, 123, "North", this.down);
            this.down.add(this.updater);
            downSpring.putConstraint("West", this.laterUpdater, 454, "West", this.down);
            downSpring.putConstraint("East", this.laterUpdater, 624, "West", this.down);
            downSpring.putConstraint("North", this.laterUpdater, 86, "North", this.down);
            downSpring.putConstraint("South", this.laterUpdater, 123, "North", this.down);
            this.down.add(this.laterUpdater);
        }
        if (messageType == 1) {
            final Banner banner = update.getBanners().get(lang).get(0);
            JLabel imagePanel = null;
            try {
                imagePanel = new JLabel((Icon)new ImageIcon((Image)ImageCache.loadImage(new URL(banner.getImage()))));
                imagePanel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(final MouseEvent e) {
                        OS.openLink(banner.getClickLink());
                    }
                });
            }
            catch (MalformedURLException ex) {}
            topSpring.putConstraint("West", imagePanel, 326, "West", this.imageTop);
            topSpring.putConstraint("East", imagePanel, 0, "East", this.imageTop);
            topSpring.putConstraint("North", imagePanel, 0, "North", this.imageTop);
            topSpring.putConstraint("South", imagePanel, 453, "North", this.imageTop);
            this.imageTop.add(imagePanel);
        }
        else if (messageType == 2) {
            final Offer offer = update.getOffers().get(0);
            final JScrollPane pane = HtmlTextPane.createNew(offer.getTopText().get(lang), 574);
            topSpring.putConstraint("West", pane, 326, "West", this.imageTop);
            topSpring.putConstraint("East", pane, 0, "East", this.imageTop);
            topSpring.putConstraint("North", pane, 0, "North", this.imageTop);
            topSpring.putConstraint("South", pane, offer.getStartCheckboxSouth(), "North", this.imageTop);
            this.imageTop.add(pane);
            int start = offer.getStartCheckboxSouth();
            for (final PointOffer p : offer.getCheckBoxes()) {
                final JCheckBox checkBox = new OwnImageCheckBox(p.getTexts().get(lang), "updater-checkbox-on.png", "updater-checkbox-off.png");
                if (isAdmin) {
                    checkBox.setSelected(p.isActive());
                }
                else {
                    checkBox.setSelected(false);
                }
                checkBox.setIconTextGap(18);
                checkBox.setActionCommand(p.getName());
                checkBox.setVerticalAlignment(0);
                SwingUtil.changeFontFamily(checkBox, FontTL.CALIBRI, 16);
                topSpring.putConstraint("West", checkBox, 378, "West", this.imageTop);
                topSpring.putConstraint("East", checkBox, 0, "East", this.imageTop);
                topSpring.putConstraint("North", checkBox, start, "North", this.imageTop);
                topSpring.putConstraint("South", checkBox, start + 39, "North", this.imageTop);
                start += 39;
                this.checkBoxList.add(checkBox);
                this.imageTop.add(checkBox);
            }
            final JScrollPane downDescription = HtmlTextPane.createNew(offer.getDownText().get(lang), 574);
            topSpring.putConstraint("West", downDescription, 328, "West", this.imageTop);
            topSpring.putConstraint("East", downDescription, 0, "East", this.imageTop);
            topSpring.putConstraint("North", downDescription, 320, "North", this.imageTop);
            topSpring.putConstraint("South", downDescription, 0, "South", this.imageTop);
            this.imageTop.add(downDescription);
        }
        this.down.setBackground(Color.WHITE);
        SwingUtil.changeFontFamily(tlauncher, FontTL.CALIBRI_BOLD, 30);
        SwingUtil.changeFontFamily(this.updater, FontTL.ROBOTO_REGULAR, 13);
        SwingUtil.changeFontFamily(this.ok, FontTL.ROBOTO_REGULAR, 13);
        SwingUtil.changeFontFamily(this.laterUpdater, FontTL.ROBOTO_REGULAR, 13);
        tlauncher.setHorizontalTextPosition(2);
        this.updater.addActionListener(e -> this.result = 1);
        this.laterUpdater.addActionListener(e -> this.result = 0);
        this.ok.addActionListener(e -> this.result = 1);
    }
    
    public UpdaterFormController.UserResult showMessage() {
        this.result = -1;
        Alert.showMessage("  " + Localizable.get("launcher.update.title"), this, new JButton[] { this.updater, this.laterUpdater, this.ok });
        final UpdaterFormController.UserResult res = new UpdaterFormController.UserResult();
        res.setUserChooser(this.result);
        final StringBuilder builder = new StringBuilder();
        for (final JCheckBox box : this.checkBoxList) {
            if (box.isSelected()) {
                if (builder.length() > 0) {
                    builder.append("+");
                }
                builder.append(box.getActionCommand());
                res.setSelectedAnyCheckBox(true);
            }
        }
        res.setOfferArgs(builder.toString());
        return res;
    }
    
    static {
        SIZE = new Dimension(900, 600);
    }
}
