// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.updater;

import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.downloader.Downloader;
import org.tlauncher.tlauncher.controller.UpdaterFormController;
import org.tlauncher.tlauncher.updater.client.Updater;
import org.tlauncher.tlauncher.ui.listener.UpdateUIListener;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.nio.file.Files;
import org.tlauncher.util.FileUtil;
import org.tlauncher.tlauncher.updater.client.Update;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import org.tlauncher.util.U;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Dimension;
import org.tlauncher.tlauncher.updater.client.AutoUpdater;
import org.tlauncher.tlauncher.ui.swing.ImagePanel;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import javax.swing.JPanel;
import java.awt.Color;
import org.tlauncher.tlauncher.updater.client.UpdateListener;
import org.tlauncher.tlauncher.updater.client.UpdaterListener;
import org.tlauncher.tlauncher.downloader.DownloaderListener;
import javax.swing.JFrame;

public class AutoUpdaterFrame extends JFrame implements DownloaderListener, UpdaterListener, UpdateListener
{
    private static final long serialVersionUID = -1184260781662212096L;
    private static final int ANIMATION_TICK = 1;
    private static final double OPACITY_STEP = 0.005;
    private final AutoUpdaterFrame instance;
    private final Color border;
    private final JPanel titlepan;
    private final JPanel pan;
    private final LocalizableLabel label;
    private final ImagePanel hide;
    private final ImagePanel skip;
    private final Object animationLock;
    private boolean closed;
    private boolean canSkip;
    
    public AutoUpdaterFrame(final AutoUpdater updater) {
        this.border = new Color(255, 255, 255, 255);
        this.instance = this;
        this.animationLock = new Object();
        this.setPreferredSize(new Dimension(350, 60));
        this.setResizable(false);
        this.setUndecorated(true);
        this.setBackground(new Color(0, 0, 0, 0));
        this.setDefaultCloseOperation(3);
        (this.pan = new JPanel() {
            private static final long serialVersionUID = -8469500310564854471L;
            protected Insets insets = new Insets(5, 10, 10, 10);
            protected Color background = new Color(255, 255, 255, 220);
            
            public void paintComponent(final Graphics g0) {
                final Graphics2D g = (Graphics2D)g0;
                final int arc = 16;
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g.setColor(this.background);
                g.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), arc, arc);
                g.setColor(AutoUpdaterFrame.this.border);
                for (int x = 1; x < 2; ++x) {
                    g.drawRoundRect(x - 1, x - 1, this.getWidth() - 2 * x + 1, this.getHeight() - 2 * x + 1, arc, arc);
                }
                Color shadow = U.shiftAlpha(Color.gray, -200);
                int x2 = 2;
                while (true) {
                    shadow = U.shiftAlpha(shadow, -8);
                    if (shadow.getAlpha() == 0) {
                        break;
                    }
                    g.setColor(shadow);
                    g.drawRoundRect(x2 - 1, x2 - 1, this.getWidth() - 2 * x2 + 1, this.getHeight() - 2 * x2 + 1, arc - 2 * x2 + 1, arc - 2 * x2 + 1);
                    ++x2;
                }
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                super.paintComponent(g0);
            }
            
            @Override
            public Insets getInsets() {
                return this.insets;
            }
        }).setOpaque(false);
        this.pan.setLayout(new BorderLayout());
        this.add(this.pan);
        (this.titlepan = new JPanel()).setOpaque(false);
        this.titlepan.setLayout(new FlowLayout(2));
        (this.hide = new ImagePanel("hide.png", 0.75f, 0.5f, false) {
            private static final long serialVersionUID = 513294577418505533L;
            
            @Override
            protected boolean onClick() {
                if (!super.onClick()) {
                    return false;
                }
                AutoUpdaterFrame.this.instance.setExtendedState(1);
                return true;
            }
        }).setToolTipText(Localizable.get("autoupdater.buttons.hide"));
        this.titlepan.add(this.hide);
        this.skip = new ImagePanel("skip.png", 0.75f, 0.5f, false) {
            private static final long serialVersionUID = 513294577418505533L;
            
            @Override
            protected boolean onClick() {
                if (!super.onClick() || !AutoUpdaterFrame.this.canSkip) {
                    return false;
                }
                AutoUpdaterFrame.this.handleClose();
                return true;
            }
        };
        this.pan.add("East", this.titlepan);
        (this.label = new LocalizableLabel("autoupdater.preparing")).setOpaque(false);
        this.pan.add("West", this.label);
        this.setCanSkip(true);
        updater.getLauncher().getDownloader().addListener(this);
        this.pack();
        this.setLocationRelativeTo(null);
        this.requestFocusInWindow();
    }
    
    private void hideButtons() {
        this.skip.hide();
    }
    
    private void closeFrame() {
        if (this.closed) {
            this.dispose();
        }
        else {
            float opacity = 1.0f;
            synchronized (this.animationLock) {
                try {
                    this.setOpacity(opacity);
                }
                catch (Throwable t) {}
                while (opacity > 0.0f) {
                    opacity -= (float)0.005;
                    if (opacity < 0.0f) {
                        opacity = 0.0f;
                    }
                    try {
                        this.setOpacity(opacity);
                    }
                    catch (Throwable t2) {}
                    U.sleepFor(1L);
                }
                this.dispose();
            }
        }
        this.closed = true;
    }
    
    public boolean canSkip() {
        return this.canSkip;
    }
    
    private void setCanSkip(final boolean b) {
        if (!(this.canSkip = b)) {
            this.hideButtons();
        }
    }
    
    private void handleClose() {
        this.label.setText("autoupdater.opening", TLauncher.getVersion());
        this.closeFrame();
    }
    
    public boolean isClosed() {
        return this.closed;
    }
    
    @Override
    public void onUpdateError(final Update u, final Throwable e) {
        if (this.closed) {
            return;
        }
        if (!Files.isWritable(FileUtil.getRunningJar().getParentFile().toPath())) {
            if (Alert.showErrorMessage("updater.error.title", "updater.access.denied.message", "updater.button.download.handle", "review.button.close") == 0) {
                OS.openLink(OS.is(OS.WINDOWS) ? u.getExeLinks().get(0) : u.getJarLinks().get(0));
            }
            TLauncher.kill();
        }
        else if (Alert.showLocQuestion("updater.error.title", "updater.download-error", e)) {
            UpdateUIListener.openUpdateLink(u.getlastDownloadedLink());
        }
        this.handleClose();
    }
    
    @Override
    public void onUpdateDownloading(final Update u) {
        this.label.setText("autoupdater.downloading", u.getVersion());
    }
    
    @Override
    public void onUpdateDownloadError(final Update u, final Throwable e) {
        this.onUpdateError(u, e);
    }
    
    @Override
    public void onUpdateReady(final Update u) {
        this.setCanSkip(true);
    }
    
    @Override
    public void onUpdateApplying(final Update u) {
    }
    
    @Override
    public void onUpdateApplyError(final Update u, final Throwable e) {
        if (Alert.showLocQuestion("updater.save-error", e)) {
            UpdateUIListener.openUpdateLink(u.getlastDownloadedLink());
        }
    }
    
    @Override
    public void onUpdaterRequesting(final Updater u) {
        this.label.setText("autoupdater.requesting");
    }
    
    @Override
    public void onUpdaterErrored(final Updater.SearchFailed failed) {
        this.handleClose();
    }
    
    @Override
    public void onUpdaterSucceeded(final Updater.SearchSucceeded succeeded) {
        final Update update = succeeded.getResponse();
        if (!FileUtil.checkFreeSpace(FileUtil.getRunningJar(), FileUtil.SIZE_100) && update.isApplicable()) {
            this.showSpaceMessage(update);
            update.setFreeSpaceEnough(false);
        }
        if (update.isApplicable()) {
            final UpdaterFormController controller = new UpdaterFormController(update, TLauncher.getInstance().getConfiguration());
            update.setUserChoose(controller.getResult());
        }
        if (update.isApplicable()) {
            update.addListener(this);
        }
        else {
            this.label.setText("autoupdater.opening");
            this.handleClose();
        }
    }
    
    private void showSpaceMessage(final Update update) {
        Alert.showMessage(Localizable.get("launcher.update.title"), Localizable.get("launcher.update.no.space").replace("disk", FileUtil.getRunningJar().toPath().getRoot().toString()));
    }
    
    @Override
    public void onDownloaderStart(final Downloader d, final int files) {
        this.setCanSkip(false);
    }
    
    @Override
    public void onDownloaderAbort(final Downloader d) {
    }
    
    @Override
    public void onDownloaderProgress(final Downloader d, final double progress, final double speed) {
    }
    
    @Override
    public void onDownloaderFileComplete(final Downloader d, final Downloadable file) {
    }
    
    @Override
    public void onDownloaderComplete(final Downloader d) {
    }
}
