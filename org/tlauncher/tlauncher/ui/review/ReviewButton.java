// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.review;

import org.tlauncher.tlauncher.ui.block.Unblockable;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;

public class ReviewButton extends LocalizableButton implements Unblockable
{
    private static final long serialVersionUID = -8398005391201344228L;
    
    public ReviewButton(final String name) {
        super(name);
    }
}
