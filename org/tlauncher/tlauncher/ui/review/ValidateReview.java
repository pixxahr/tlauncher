// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.review;

import java.util.regex.Pattern;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.entity.Review;

public class ValidateReview
{
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    public static boolean validate(final Review review) {
        if (review.getDescription().isEmpty()) {
            Alert.showWarning("", Localizable.get().get("review.message.fill") + " " + Localizable.get().get("review.message.description"));
            return false;
        }
        if (review.getDescription().equals(Localizable.get("review.description"))) {
            Alert.showWarning("", Localizable.get().get("review.message.fill") + " " + Localizable.get().get("review.message.description"));
            return false;
        }
        final String mail = review.getMailReview();
        if (mail.isEmpty()) {
            Alert.showLocWarning("review.message.email.invalid");
            return false;
        }
        if (mail.startsWith("https://vk.com") || mail.startsWith("http://vk.com") || mail.startsWith("vk.com")) {
            return true;
        }
        if (!Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(review.getMailReview()).matches()) {
            Alert.showWarning("", Localizable.get().get("review.message.email.invalid"));
            return false;
        }
        return true;
    }
}
