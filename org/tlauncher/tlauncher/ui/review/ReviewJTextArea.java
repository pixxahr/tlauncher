// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.review;

import javax.swing.text.BadLocationException;
import javax.swing.text.AttributeSet;
import javax.swing.text.PlainDocument;
import javax.swing.text.Document;
import javax.swing.JTextArea;

public class ReviewJTextArea extends JTextArea
{
    public ReviewJTextArea() {
    }
    
    public ReviewJTextArea(final int rows, final int column) {
        super(rows, column);
    }
    
    @Override
    protected final Document createDefaultModel() {
        return new PlainDocument() {
            @Override
            public void insertString(final int offs, final String str, final AttributeSet a) throws BadLocationException {
                if (ReviewJTextArea.this.getText().length() < 1500) {
                    super.insertString(offs, str, a);
                }
            }
        };
    }
}
