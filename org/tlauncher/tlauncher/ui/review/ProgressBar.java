// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.review;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.JFrame;

public class ProgressBar extends JFrame
{
    private JProgressBar progress;
    private JFrame parent;
    
    public ProgressBar(final JFrame parent, final JButton cancel) {
        this.parent = parent;
        (this.progress = new JProgressBar()).setPreferredSize(new Dimension(180, 20));
        this.progress.setIndeterminate(true);
        this.setUndecorated(true);
        this.setSize(180, 75);
        this.add(this.progress, "Center");
        this.add(cancel, "South");
        parent.setGlassPane(new JComponent() {
            @Override
            protected void paintComponent(final Graphics g) {
                g.setColor(new Color(0, 0, 0, 50));
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
                super.paintComponent(g);
            }
        });
        this.pack();
    }
    
    @Override
    public void setVisible(final boolean b) {
        if (b) {
            this.setLocation(this.parent.getX() + this.parent.getWidth() / 2 - this.getWidth() / 2, this.parent.getY() + this.parent.getHeight() / 2 - this.getHeight() / 2);
            this.parent.setEnabled(false);
        }
        else {
            this.parent.setEnabled(true);
        }
        this.parent.getGlassPane().setVisible(b);
        super.setVisible(b);
    }
}
