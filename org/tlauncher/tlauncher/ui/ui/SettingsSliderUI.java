// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.ui;

import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.Dimension;
import javax.swing.JSlider;
import java.awt.Color;
import javax.swing.plaf.basic.BasicSliderUI;

public class SettingsSliderUI extends BasicSliderUI
{
    public static Color TRACK_LEFT;
    public static Color TRACK_RIGHT;
    public static Color THUMB_COLOR;
    public static Color SERIFS;
    private final JSlider jSlider;
    
    public SettingsSliderUI(final JSlider b) {
        super(b);
        this.uninstallListeners(this.jSlider = b);
    }
    
    @Override
    protected Dimension getThumbSize() {
        final Dimension size = new Dimension();
        size.width = 19;
        size.height = 19;
        return size;
    }
    
    @Override
    public void paintTrack(final Graphics g) {
        final Rectangle trackBounds = this.trackRect;
        final int cy = trackBounds.height / 2 - 2;
        final int cw = trackBounds.width;
        g.translate(trackBounds.x, trackBounds.y + cy);
        g.setColor(SettingsSliderUI.TRACK_LEFT);
        g.fillRect(0, 0, this.thumbRect.x, 2);
        g.setColor(SettingsSliderUI.TRACK_RIGHT);
        g.fillRect(this.thumbRect.x, 0, this.trackRect.width - this.thumbRect.x, 2);
        final int major = this.jSlider.getMajorTickSpacing();
        final int width = this.jSlider.getMaximum() - this.jSlider.getMinimum();
        final int count = width / major;
        g.setColor(SettingsSliderUI.SERIFS);
        for (int i = 0; i < count; ++i) {
            g.fillRect(i * cw / count, 0, 2, 2);
        }
        if (count == 0) {
            g.fillRect(0, 0, 2, 2);
        }
        else {
            g.fillRect(count * cw / count, 0, 2, 2);
        }
        g.translate(-trackBounds.x, -(trackBounds.y + cy));
    }
    
    @Override
    protected void paintHorizontalLabel(final Graphics g, final int value, final Component label) {
        label.setForeground(new Color(96, 96, 96));
        super.paintHorizontalLabel(g, value, label);
    }
    
    @Override
    public void paintThumb(final Graphics g) {
        final Rectangle knobBounds = this.thumbRect;
        final Graphics2D graphics2d = (Graphics2D)g;
        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(SettingsSliderUI.THUMB_COLOR);
        g.fillOval(knobBounds.x, knobBounds.y, knobBounds.width, knobBounds.height);
        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    }
    
    @Override
    public void paintFocus(final Graphics g) {
    }
    
    static {
        SettingsSliderUI.TRACK_LEFT = new Color(0, 174, 239);
        SettingsSliderUI.TRACK_RIGHT = new Color(181, 181, 181);
        SettingsSliderUI.THUMB_COLOR = new Color(0, 174, 239);
        SettingsSliderUI.SERIFS = new Color(37, 37, 37);
    }
}
