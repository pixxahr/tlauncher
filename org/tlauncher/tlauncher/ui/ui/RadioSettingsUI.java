// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.ui;

import java.awt.image.ImageObserver;
import java.awt.Color;
import org.tlauncher.util.SwingUtil;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JRadioButton;
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.plaf.basic.BasicButtonUI;

public class RadioSettingsUI extends BasicButtonUI
{
    Image backgroundImage;
    
    public RadioSettingsUI(final Image image) {
        this.backgroundImage = image;
    }
    
    @Override
    public void paint(final Graphics g, final JComponent c) {
        final JRadioButton button = (JRadioButton)c;
        final Rectangle rec = c.getVisibleRect();
        this.paintBackground(g, rec, button.isSelected());
        this.paintText(g, rec, button);
    }
    
    private void paintText(final Graphics g, final Rectangle rec, final JRadioButton comp) {
        final Graphics2D g2d = (Graphics2D)g;
        SwingUtil.paintText(g2d, comp, comp.getText());
    }
    
    private void paintBackground(final Graphics g, final Rectangle rec, final boolean state) {
        if (state) {
            g.setColor(Color.WHITE);
            g.fillRect(rec.x, rec.y, (int)rec.getWidth(), (int)rec.getHeight());
        }
        else {
            g.drawImage(this.backgroundImage, rec.x, rec.y, null);
        }
    }
}
