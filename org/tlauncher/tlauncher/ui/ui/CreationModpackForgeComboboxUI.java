// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.ui;

import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Graphics;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.swing.plaf.ScrollBarUI;
import java.awt.Component;
import javax.swing.JComponent;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import java.awt.event.ActionListener;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class CreationModpackForgeComboboxUI extends BasicComboBoxUI
{
    @Override
    protected JButton createArrowButton() {
        final ImageUdaterButton button = new ImageUdaterButton(Color.WHITE, "gray-combobox-array.png");
        for (final ActionListener l : button.getActionListeners()) {
            button.removeActionListener(l);
        }
        button.setModelPressedColor(ColorUtil.COLOR_195);
        return button;
    }
    
    @Override
    protected ComboPopup createPopup() {
        final BasicComboPopup basic = new BasicComboPopup(this.comboBox) {
            @Override
            protected JScrollPane createScroller() {
                final ModpackScrollBarUI barUI = new ModpackScrollBarUI() {
                    @Override
                    protected Dimension getMinimumThumbSize() {
                        return new Dimension(10, 40);
                    }
                    
                    @Override
                    public Dimension getMaximumSize(final JComponent c) {
                        final Dimension dim = super.getMaximumSize(c);
                        dim.setSize(10.0, dim.getHeight());
                        return dim;
                    }
                    
                    @Override
                    public Dimension getPreferredSize(final JComponent c) {
                        final Dimension dim = super.getPreferredSize(c);
                        dim.setSize(13.0, dim.getHeight());
                        return dim;
                    }
                };
                barUI.setGapThubm(5);
                final JScrollPane scroller = new JScrollPane(this.list, 20, 31);
                scroller.getVerticalScrollBar().setUI(barUI);
                return scroller;
            }
        };
        basic.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, new Color(149, 149, 149)));
        return basic;
    }
    
    @Override
    public void paintCurrentValue(final Graphics g, final Rectangle bounds, final boolean hasFocus) {
        super.paintCurrentValue(g, bounds, hasFocus);
        g.setColor(Color.WHITE);
        g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
        this.paintText(g, bounds, (String)this.comboBox.getSelectedItem());
    }
    
    protected void paintText(final Graphics g, final Rectangle textRect, final String text) {
        if (text == null) {
            return;
        }
        final Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        final FontMetrics fm = g2d.getFontMetrics();
        final Rectangle2D r = fm.getStringBounds(text, g2d);
        g.setFont(this.comboBox.getFont());
        g.setColor(new Color(25, 25, 25));
        final int x = 14;
        final int y = (textRect.height - (int)r.getHeight()) / 2 + fm.getAscent();
        g2d.drawString(text, x, y);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    }
}
