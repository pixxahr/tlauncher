// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.ui;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.Component;
import java.util.Map;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class ModpackScrollBarUI extends BasicScrollBarUI
{
    protected int heightThubm;
    protected int gapThubm;
    int arc;
    protected Color trackColor;
    protected Color thumbColor;
    private static final int gup = 2;
    
    public ModpackScrollBarUI() {
        this.heightThubm = 16;
        this.gapThubm = 6;
        this.arc = 10;
        this.trackColor = new Color(215, 215, 215);
        this.thumbColor = new Color(182, 182, 182);
    }
    
    public int getHeightThubm() {
        return this.heightThubm;
    }
    
    public void setHeightThubm(final int heightThubm) {
        this.heightThubm = heightThubm;
    }
    
    @Override
    protected void paintTrack(final Graphics g, final JComponent c, final Rectangle trackBounds) {
        final Graphics2D g2 = (Graphics2D)g.create();
        final RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHints(qualityHints);
        final Component parent = c.getParent();
        final Rectangle rec = this.trackRect;
        if (parent != null) {
            final Color bg = parent.getBackground();
            g2.setColor(bg);
            g2.fillRect(rec.x, rec.y, rec.width, rec.height);
        }
        g2.setColor(this.trackColor);
        g2.fillRoundRect(rec.x, rec.y + 2, rec.width, rec.height - 4, this.arc, this.arc);
        g2.dispose();
    }
    
    @Override
    protected void paintThumb(final Graphics g, final JComponent c, final Rectangle thumbBounds) {
        final Graphics2D g2 = (Graphics2D)g.create();
        final RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHints(qualityHints);
        final Rectangle rec = thumbBounds;
        g2.setColor(this.thumbColor);
        g2.fillRoundRect(rec.x, rec.y + 2, rec.width, rec.height - 4, this.arc, this.arc);
        g2.dispose();
    }
    
    @Override
    protected Dimension getMinimumThumbSize() {
        return new Dimension(10, 80);
    }
    
    @Override
    protected JButton createDecreaseButton(final int orientation) {
        return this.createZeroButton();
    }
    
    @Override
    protected JButton createIncreaseButton(final int orientation) {
        return this.createZeroButton();
    }
    
    private JButton createZeroButton() {
        final JButton jbutton = new JButton();
        jbutton.setPreferredSize(new Dimension(0, 0));
        return jbutton;
    }
    
    public int getGapThubm() {
        return this.gapThubm;
    }
    
    public void setGapThubm(final int gapThubm) {
        this.gapThubm = gapThubm;
    }
}
