// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.ui;

import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.Color;
import javax.swing.plaf.basic.BasicProgressBarUI;

public class FancyProgressBar extends BasicProgressBarUI
{
    public final Color border;
    public final Color bottomBorderLine;
    public final Color REST_COLOR;
    public static final int PROGRESS_HEIGHT = 24;
    BufferedImage image;
    
    public FancyProgressBar(final BufferedImage bufferedImage) {
        this.border = new Color(156, 155, 155);
        this.bottomBorderLine = new Color(146, 154, 140);
        this.REST_COLOR = new Color(200, 203, 199);
        this.image = bufferedImage;
    }
    
    @Override
    protected void paintDeterminate(final Graphics g, final JComponent c) {
        final Graphics2D g2d = (Graphics2D)g.create();
        final Rectangle rec = this.progressBar.getVisibleRect();
        final double complete = this.progressBar.getPercentComplete();
        final int width = this.progressBar.getWidth();
        final int height = this.progressBar.getHeight();
        final int completeWidth = (int)(complete * width);
        g2d.setColor(this.REST_COLOR);
        g2d.fillRect(rec.x + completeWidth, rec.y, width, height);
        g2d.setColor(this.border);
        g2d.drawLine(completeWidth, 0, rec.width, 0);
        g2d.drawLine(completeWidth, rec.height - 1, rec.width, rec.height - 1);
        g2d.drawLine(rec.x + rec.width - 1, rec.y, rec.x + rec.width - 1, rec.y + rec.height);
        g2d.setColor(this.bottomBorderLine);
        g2d.drawLine(completeWidth, rec.height - 1, rec.width, rec.height - 1);
        if (completeWidth > 0) {
            g2d.drawImage(this.image.getSubimage(0, 0, completeWidth, 24), rec.x, rec.y, completeWidth, rec.height + 1, null);
        }
    }
}
