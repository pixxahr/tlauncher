// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.ui;

import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import net.minecraft.launcher.versions.CompleteVersion;
import java.awt.Rectangle;
import java.awt.Graphics;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.swing.renderer.ModpackComboxRenderer;
import javax.swing.plaf.ScrollBarUI;
import java.awt.Component;
import javax.swing.JComponent;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import java.awt.event.ActionListener;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class ModpackComboBoxUI extends BasicComboBoxUI
{
    protected int GUP_LEFT;
    public static final Color BACKGROUND_BOX;
    protected boolean centerText;
    
    public ModpackComboBoxUI() {
        this.GUP_LEFT = 20;
        this.centerText = false;
    }
    
    @Override
    protected JButton createArrowButton() {
        final ImageUdaterButton button = new ImageUdaterButton(ModpackComboBoxUI.BACKGROUND_BOX, "white-arrow-down.png");
        for (final ActionListener l : button.getActionListeners()) {
            button.removeActionListener(l);
        }
        button.setBackground(ModpackComboBoxUI.BACKGROUND_BOX);
        this.comboBox.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(final PopupMenuEvent e) {
                button.setImage(ImageCache.getBufferedImage("white-arrow-up.png"));
            }
            
            @Override
            public void popupMenuWillBecomeInvisible(final PopupMenuEvent e) {
                button.setImage(ImageCache.getBufferedImage("white-arrow-down.png"));
            }
            
            @Override
            public void popupMenuCanceled(final PopupMenuEvent e) {
            }
        });
        return button;
    }
    
    @Override
    protected ComboPopup createPopup() {
        final BasicComboPopup basic = new BasicComboPopup(this.comboBox) {
            @Override
            protected JScrollPane createScroller() {
                final ModpackScrollBarUI barUI = new ModpackScrollBarUI() {
                    @Override
                    protected Dimension getMinimumThumbSize() {
                        return new Dimension(10, 40);
                    }
                    
                    @Override
                    public Dimension getMaximumSize(final JComponent c) {
                        final Dimension dim = super.getMaximumSize(c);
                        dim.setSize(10.0, dim.getHeight());
                        return dim;
                    }
                    
                    @Override
                    public Dimension getPreferredSize(final JComponent c) {
                        final Dimension dim = super.getPreferredSize(c);
                        dim.setSize(13.0, dim.getHeight());
                        return dim;
                    }
                };
                barUI.setGapThubm(5);
                final JScrollPane scroller = new JScrollPane(this.list, 20, 31);
                scroller.getVerticalScrollBar().setUI(barUI);
                scroller.setBackground(ModpackComboBoxUI.BACKGROUND_BOX);
                return scroller;
            }
        };
        basic.setMaximumSize(new Dimension(172, 149));
        basic.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, ModpackComboxRenderer.LINE));
        return basic;
    }
    
    @Override
    public void paintCurrentValue(final Graphics g, final Rectangle bounds, final boolean hasFocus) {
        super.paintCurrentValue(g, bounds, hasFocus);
        this.paintBackground(g, bounds);
        this.paintText(g, bounds, ((CompleteVersion)this.comboBox.getSelectedItem()).getID());
    }
    
    protected void paintBackground(final Graphics g, final Rectangle bounds) {
        g.setColor(ModpackComboBoxUI.BACKGROUND_BOX);
        g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
    }
    
    protected void paintText(final Graphics g, final Rectangle textRect, final String text) {
        final Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setFont(this.comboBox.getFont());
        final FontMetrics fm = g2d.getFontMetrics();
        final Rectangle2D r = fm.getStringBounds(text, g2d);
        g.setColor(Color.WHITE);
        final int x = this.GUP_LEFT;
        final int y = (textRect.height - (int)r.getHeight()) / 2 + fm.getAscent();
        g2d.drawString(text, x, y);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    }
    
    static {
        BACKGROUND_BOX = new Color(63, 177, 241);
    }
}
