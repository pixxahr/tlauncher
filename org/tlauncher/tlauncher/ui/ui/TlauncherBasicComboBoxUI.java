// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.ui;

import javax.swing.BorderFactory;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.JComponent;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.swing.scroll.VersionScrollBarUI;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.ListCellRenderer;
import java.awt.Container;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.JList;
import java.awt.Rectangle;
import java.awt.Graphics;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class TlauncherBasicComboBoxUI extends BasicComboBoxUI
{
    @Override
    public void paintCurrentValue(final Graphics g, final Rectangle bounds, final boolean hasFocus) {
        final ListCellRenderer renderer = this.comboBox.getRenderer();
        Component c;
        if (hasFocus && !this.isPopupVisible(this.comboBox)) {
            c = renderer.getListCellRendererComponent(this.listBox, this.comboBox.getSelectedItem(), -1, true, false);
        }
        else {
            c = renderer.getListCellRendererComponent(this.listBox, this.comboBox.getSelectedItem(), -1, false, false);
            c.setBackground(UIManager.getColor("ComboBox.background"));
        }
        c.setFont(this.comboBox.getFont());
        if (hasFocus && !this.isPopupVisible(this.comboBox)) {
            c.setForeground(Color.BLACK);
            c.setBackground(Color.WHITE);
        }
        else if (this.comboBox.isEnabled()) {
            c.setForeground(this.comboBox.getForeground());
            c.setBackground(this.comboBox.getBackground());
        }
        else {
            c.setForeground(UIManager.getColor("ComboBox.disabledForeground"));
            c.setBackground(UIManager.getColor("ComboBox.disabledBackground"));
        }
        boolean shouldValidate = false;
        if (c instanceof JPanel) {
            shouldValidate = true;
        }
        int x = bounds.x;
        int y = bounds.y;
        int w = bounds.width;
        int h = bounds.height;
        if (this.padding != null) {
            x = bounds.x + this.padding.left;
            y = bounds.y + this.padding.top;
            w = bounds.width - (this.padding.left + this.padding.right);
            h = bounds.height - (this.padding.top + this.padding.bottom);
        }
        this.currentValuePane.paintComponent(g, c, this.comboBox, x, y, w, h, shouldValidate);
    }
    
    @Override
    protected JButton createArrowButton() {
        final JButton button = new ImageUdaterButton(Color.white, "black-arrow.png") {};
        for (final ActionListener l : button.getActionListeners()) {
            button.removeActionListener(l);
        }
        return button;
    }
    
    @Override
    protected ComboPopup createPopup() {
        final BasicComboPopup basic = new BasicComboPopup(this.comboBox) {
            @Override
            protected JScrollPane createScroller() {
                final VersionScrollBarUI barUI = new VersionScrollBarUI() {
                    @Override
                    protected Dimension getMinimumThumbSize() {
                        return new Dimension(10, 40);
                    }
                    
                    @Override
                    public Dimension getMaximumSize(final JComponent c) {
                        final Dimension dim = super.getMaximumSize(c);
                        dim.setSize(10.0, dim.getHeight());
                        return dim;
                    }
                    
                    @Override
                    public Dimension getPreferredSize(final JComponent c) {
                        final Dimension dim = super.getPreferredSize(c);
                        dim.setSize(13.0, dim.getHeight());
                        return dim;
                    }
                };
                barUI.setGapThubm(5);
                final JScrollPane scroller = new JScrollPane(this.list, 20, 31);
                scroller.getVerticalScrollBar().setUI(barUI);
                return scroller;
            }
        };
        basic.setBorder(BorderFactory.createEmptyBorder());
        return basic;
    }
}
