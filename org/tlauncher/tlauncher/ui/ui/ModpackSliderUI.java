// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.ui;

import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JSlider;

public class ModpackSliderUI extends SettingsSliderUI
{
    public ModpackSliderUI(final JSlider b) {
        super(b);
    }
    
    @Override
    public void paintThumb(final Graphics g) {
        super.paintThumb(g);
        g.setColor(Color.BLACK);
        g.setFont(this.slider.getFont());
        final Rectangle knobBounds = this.thumbRect;
        final Graphics2D graphics2d = (Graphics2D)g;
        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (9999 < this.slider.getValue()) {
            graphics2d.drawString("" + this.slider.getValue(), knobBounds.x - 20, knobBounds.y - 8);
        }
        else if (9999 > this.slider.getValue() && 1000 <= this.slider.getValue()) {
            graphics2d.drawString("" + this.slider.getValue(), knobBounds.x - 10, knobBounds.y - 8);
        }
        else {
            graphics2d.drawString("" + this.slider.getValue(), knobBounds.x, knobBounds.y - 8);
        }
        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    }
}
