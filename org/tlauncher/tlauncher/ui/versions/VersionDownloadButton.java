// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.Image;
import org.tlauncher.tlauncher.managers.VersionManager;
import java.util.Iterator;
import org.tlauncher.tlauncher.downloader.AbortedDownloadException;
import java.io.IOException;
import java.util.List;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.managers.VersionSyncInfoContainer;
import java.util.ArrayList;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import net.minecraft.launcher.updater.VersionSyncInfo;
import java.awt.Component;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;
import javax.swing.JPopupMenu;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.block.Unblockable;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;

public class VersionDownloadButton extends ImageUdaterButton implements VersionHandlerListener, Unblockable
{
    private static final String SELECTION_BLOCK = "selection";
    private static final String PREFIX = "version.manager.downloader.";
    private static final String WARNING = "version.manager.downloader.warning.";
    private static final String WARNING_TITLE = "version.manager.downloader.warning.title";
    private static final String WARNING_FORCE = "version.manager.downloader.warning.force.";
    private static final String ERROR = "version.manager.downloader.error.";
    private static final String ERROR_TITLE = "version.manager.downloader.error.title";
    private static final String INFO = "version.manager.downloader.info.";
    private static final String INFO_TITLE = "version.manager.downloader.info.title";
    private static final String MENU = "version.manager.downloader.menu.";
    final VersionHandler handler;
    final Blockable blockable;
    private final JPopupMenu menu;
    private final LocalizableMenuItem ordinary;
    private final LocalizableMenuItem force;
    private ButtonState state;
    private boolean downloading;
    private boolean aborted;
    boolean forceDownload;
    
    VersionDownloadButton(final VersionList list) {
        super(Color.WHITE);
        this.handler = list.handler;
        this.blockable = new Blockable() {
            @Override
            public void block(final Object reason) {
                VersionDownloadButton.this.setEnabled(false);
            }
            
            @Override
            public void unblock(final Object reason) {
                VersionDownloadButton.this.setEnabled(true);
            }
        };
        this.menu = new JPopupMenu();
        (this.ordinary = new LocalizableMenuItem("version.manager.downloader.menu.ordinary")).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                VersionDownloadButton.this.forceDownload = false;
                VersionDownloadButton.this.onDownloadCalled();
            }
        });
        this.menu.add(this.ordinary);
        (this.force = new LocalizableMenuItem("version.manager.downloader.menu.force")).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
            }
        });
        this.menu.add(this.force);
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                VersionDownloadButton.this.onPressed();
            }
        });
        this.setState(ButtonState.DOWNLOAD);
        this.handler.addListener(this);
    }
    
    void setState(final ButtonState state) {
        if (state == null) {
            throw new NullPointerException();
        }
        this.state = state;
        this.setImage(state.image);
    }
    
    void onPressed() {
        switch (this.state) {
            case DOWNLOAD: {
                this.forceDownload = true;
                this.onDownloadCalled();
                break;
            }
            case STOP: {
                this.onStopCalled();
                break;
            }
        }
    }
    
    void onDownloadPressed() {
        this.menu.show(this, 0, this.getHeight());
    }
    
    void onDownloadCalled() {
        if (this.state != ButtonState.DOWNLOAD) {
            throw new IllegalStateException();
        }
        this.handler.thread.startThread.iterate();
    }
    
    void onStopCalled() {
        if (this.state != ButtonState.STOP) {
            throw new IllegalStateException();
        }
        this.handler.thread.stopThread.iterate();
    }
    
    void startDownload() {
        this.aborted = false;
        final List<VersionSyncInfo> list = this.handler.getSelectedList();
        if (list == null || list.isEmpty()) {
            return;
        }
        int countLocal = 0;
        VersionSyncInfo local = null;
        for (final VersionSyncInfo version : list) {
            if (this.forceDownload) {
                if (!version.hasRemote()) {
                    Alert.showError(Localizable.get("version.manager.downloader.error.title"), Localizable.get("version.manager.downloader.error.local", version.getID()));
                    return;
                }
                if (!version.isUpToDate() || !version.isInstalled()) {
                    continue;
                }
                ++countLocal;
                local = version;
            }
        }
        if (countLocal > 0) {
            final String title = Localizable.get("version.manager.downloader.warning.title");
            String suffix;
            Object var;
            if (countLocal == 1) {
                suffix = "single";
                var = local.getID();
            }
            else {
                suffix = "multiply";
                var = countLocal;
            }
            if (!Alert.showQuestion(title, Localizable.get("version.manager.downloader.warning.force." + suffix, var))) {
                return;
            }
        }
        final List<VersionSyncInfoContainer> containers = new ArrayList<VersionSyncInfoContainer>();
        final VersionManager manager = TLauncher.getInstance().getVersionManager();
        try {
            this.downloading = true;
            for (final VersionSyncInfo version2 : list) {
                try {
                    version2.resolveCompleteVersion(manager, this.forceDownload);
                    final VersionSyncInfoContainer container = manager.downloadVersion(version2, false, this.forceDownload);
                    if (this.aborted) {
                        return;
                    }
                    if (container.getList().isEmpty()) {
                        continue;
                    }
                    containers.add(container);
                }
                catch (Exception e) {
                    Alert.showError(Localizable.get("version.manager.downloader.error.title"), Localizable.get("version.manager.downloader.error.getting", version2.getID()), e);
                    return;
                }
            }
            if (containers.isEmpty()) {
                Alert.showMessage(Localizable.get("version.manager.downloader.info.title"), Localizable.get("version.manager.downloader.info.no-needed"));
                return;
            }
            if (containers.size() > 1) {
                DownloadableContainer.removeDuplicates(containers);
            }
            if (this.aborted) {
                return;
            }
            for (final DownloadableContainer c : containers) {
                this.handler.downloader.add(c);
            }
            this.handler.downloading = list;
            this.handler.onVersionDownload(list);
            this.handler.downloader.startDownloadAndWait();
        }
        finally {
            this.downloading = false;
        }
        this.handler.downloading.clear();
        for (final VersionSyncInfoContainer container2 : containers) {
            final List<Throwable> errors = container2.getErrors();
            final VersionSyncInfo version3 = container2.getVersion();
            if (errors.isEmpty()) {
                try {
                    manager.getLocalList().saveVersion(version3.getCompleteVersion(this.forceDownload));
                    continue;
                }
                catch (IOException e2) {
                    Alert.showError(Localizable.get("version.manager.downloader.error.title"), Localizable.get("version.manager.downloader.error.saving", version3.getID()), e2);
                    return;
                }
            }
            if (!(errors.get(0) instanceof AbortedDownloadException)) {
                Alert.showError(Localizable.get("version.manager.downloader.error.title"), Localizable.get("version.manager.downloader.error.downloading", version3.getID()), errors);
            }
        }
        this.handler.refresh();
    }
    
    void stopDownload() {
        this.aborted = true;
        if (this.downloading) {
            this.handler.downloader.stopDownloadAndWait();
        }
    }
    
    @Override
    public void onVersionRefreshing(final VersionManager vm) {
    }
    
    @Override
    public void onVersionRefreshed(final VersionManager vm) {
    }
    
    @Override
    public void onVersionSelected(final List<VersionSyncInfo> versions) {
        if (!this.downloading) {
            this.blockable.unblock("selection");
        }
    }
    
    @Override
    public void onVersionDeselected() {
        if (!this.downloading) {
            this.blockable.block("selection");
        }
    }
    
    @Override
    public void onVersionDownload(final List<VersionSyncInfo> list) {
    }
    
    public enum ButtonState
    {
        DOWNLOAD("down.png"), 
        STOP("cancel.png");
        
        final Image image;
        
        private ButtonState(final String image) {
            this.image = ImageCache.getImage(image);
        }
    }
}
