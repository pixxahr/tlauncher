// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.util.async.LoopedThread;

public class VersionHandlerThread
{
    public final String START_DOWNLOAD = "start-download";
    public final String STOP_DOWNLOAD = "stop-download";
    public final String DELETE_BLOCK = "deleting";
    private final VersionHandler handler;
    final StartDownloadThread startThread;
    final StopDownloadThread stopThread;
    final VersionDeleteThread deleteThread;
    
    VersionHandlerThread(final VersionHandler handler) {
        this.handler = handler;
        this.startThread = new StartDownloadThread(this);
        this.stopThread = new StopDownloadThread(this);
        this.deleteThread = new VersionDeleteThread(this);
    }
    
    class StartDownloadThread extends LoopedThread
    {
        private final VersionHandler handler;
        private final VersionDownloadButton button;
        
        StartDownloadThread(final VersionHandlerThread parent) {
            super("StartDownloadThread");
            this.handler = parent.handler;
            this.button = this.handler.list.download;
            this.startAndWait();
        }
        
        @Override
        protected void iterateOnce() {
            this.button.setState(VersionDownloadButton.ButtonState.STOP);
            Blocker.block(this.handler, "start-download");
            this.button.startDownload();
            Blocker.unblock(this.handler, "start-download");
            this.button.setState(VersionDownloadButton.ButtonState.DOWNLOAD);
        }
    }
    
    class StopDownloadThread extends LoopedThread
    {
        private final VersionHandler handler;
        private final VersionDownloadButton button;
        
        StopDownloadThread(final VersionHandlerThread parent) {
            super("StopDownloadThread");
            this.handler = parent.handler;
            this.button = this.handler.list.download;
            this.startAndWait();
        }
        
        @Override
        protected void iterateOnce() {
            Blocker.block(this.button.blockable, "stop-download");
            while (!this.handler.downloader.isThreadLocked()) {
                U.sleepFor(1000L);
            }
            this.button.stopDownload();
        }
    }
    
    class VersionDeleteThread extends LoopedThread
    {
        private final VersionHandler handler;
        private final VersionRemoveButton button;
        
        VersionDeleteThread(final VersionHandlerThread parent) {
            super("VersionDeleteThread");
            this.handler = parent.handler;
            this.button = this.handler.list.remove;
            this.startAndWait();
        }
        
        @Override
        protected void iterateOnce() {
            Blocker.block(this.handler, "deleting");
            this.button.delete();
            Blocker.unblock(this.handler, "deleting");
        }
    }
}
