// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import java.util.Iterator;
import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.border.Border;
import org.tlauncher.tlauncher.ui.swing.border.VersionBorder;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.JLabel;
import java.awt.Component;
import net.minecraft.launcher.updater.VersionSyncInfo;
import javax.swing.JList;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.images.ImageIcon;
import org.tlauncher.tlauncher.ui.swing.VersionCellRenderer;

public class VersionListCellRenderer extends VersionCellRenderer
{
    private final VersionHandler handler;
    private final ImageIcon downloading;
    private static final Color FOREGROUND_COLOR;
    private static final Color BACKGROUND_COLOR;
    
    VersionListCellRenderer(final VersionList list) {
        this.handler = list.handler;
        this.downloading = ImageCache.getIcon("down.png", 16, 16);
    }
    
    @Override
    public Component getListCellRendererComponent(final JList<? extends VersionSyncInfo> list, final VersionSyncInfo value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        if (value == null) {
            return null;
        }
        final JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (!isSelected) {
            if (value.isInstalled()) {
                label.setBackground(Color.WHITE);
            }
            else {
                label.setBackground(VersionListCellRenderer.BACKGROUND_COLOR);
            }
        }
        SwingUtil.changeFontFamily(label, FontTL.ROBOTO_BOLD, 14, VersionListCellRenderer.FOREGROUND_COLOR);
        label.setBorder(new VersionBorder(10, 20, 10, 0, VersionBorder.SEPARATOR_COLOR));
        if (value.isInstalled() && !value.isUpToDate()) {
            label.setText(label.getText() + ' ' + Localizable.get("version.list.needsupdate"));
        }
        if (this.handler.downloading != null) {
            final Iterator<VersionSyncInfo> iterator = this.handler.downloading.iterator();
            if (iterator.hasNext()) {
                final VersionSyncInfo compare = iterator.next();
                final ImageIcon icon = compare.equals(value) ? this.downloading : null;
                label.setIcon((Icon)icon);
                label.setDisabledIcon((Icon)icon);
            }
        }
        return label;
    }
    
    static {
        FOREGROUND_COLOR = new Color(76, 75, 74);
        BACKGROUND_COLOR = new Color(235, 235, 235);
    }
}
