// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import java.util.Iterator;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.tlauncher.managers.VersionManagerListener;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Collections;
import java.util.ArrayList;
import net.minecraft.launcher.updater.VersionFilter;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.downloader.Downloader;
import org.tlauncher.tlauncher.managers.VersionManager;
import org.tlauncher.tlauncher.ui.scenes.VersionManagerScene;
import java.util.List;
import org.tlauncher.tlauncher.ui.block.Blockable;

public class VersionHandler implements Blockable, VersionHandlerListener
{
    static final int ELEM_WIDTH = 500;
    public static final String REFRESH_BLOCK = "refresh";
    public static final String SINGLE_SELECTION_BLOCK = "single-select";
    public static final String START_DOWNLOAD = "start-download";
    public static final String STOP_DOWNLOAD = "stop-download";
    public static final String DELETE_BLOCK = "deleting";
    private final List<VersionHandlerListener> listeners;
    private final VersionHandler instance;
    public final VersionManagerScene scene;
    final VersionHandlerThread thread;
    public final VersionList list;
    final VersionManager vm;
    final Downloader downloader;
    List<VersionSyncInfo> selected;
    List<VersionSyncInfo> downloading;
    VersionFilter filter;
    
    public VersionHandler(final VersionManagerScene scene) {
        this.instance = this;
        this.scene = scene;
        this.listeners = Collections.synchronizedList(new ArrayList<VersionHandlerListener>());
        this.downloading = Collections.synchronizedList(new ArrayList<VersionSyncInfo>());
        final TLauncher launcher = TLauncher.getInstance();
        this.vm = launcher.getVersionManager();
        this.downloader = launcher.getDownloader();
        this.list = new VersionList(this);
        this.thread = new VersionHandlerThread(this);
        this.vm.addListener(new VersionManagerListener() {
            @Override
            public void onVersionsRefreshing(final VersionManager manager) {
                VersionHandler.this.instance.onVersionRefreshing(manager);
            }
            
            @Override
            public void onVersionsRefreshed(final VersionManager manager) {
                VersionHandler.this.instance.onVersionRefreshed(manager);
            }
            
            @Override
            public void onVersionsRefreshingFailed(final VersionManager manager) {
                this.onVersionsRefreshed(manager);
            }
        });
        this.onVersionDeselected();
    }
    
    void addListener(final VersionHandlerListener listener) {
        this.listeners.add(listener);
    }
    
    void update() {
        if (this.selected != null) {
            this.onVersionSelected(this.selected);
        }
    }
    
    void refresh() {
        this.vm.startRefresh(true);
    }
    
    void asyncRefresh() {
        this.vm.asyncRefresh();
    }
    
    public void stopRefresh() {
        this.vm.stopRefresh();
    }
    
    void exitEditor() {
        this.list.deselect();
        this.scene.getMainPane().openDefaultScene();
    }
    
    VersionSyncInfo getSelected() {
        return (this.selected == null || this.selected.size() != 1) ? null : this.selected.get(0);
    }
    
    List<VersionSyncInfo> getSelectedList() {
        return this.selected;
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.block(reason, this.list, this.scene.getMainPane().defaultScene);
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblock(reason, this.list, this.scene.getMainPane().defaultScene);
    }
    
    @Override
    public void onVersionRefreshing(final VersionManager vm) {
        Blocker.block(this.instance, "refresh");
        for (final VersionHandlerListener listener : this.listeners) {
            listener.onVersionRefreshing(vm);
        }
    }
    
    @Override
    public void onVersionRefreshed(final VersionManager vm) {
        Blocker.unblock(this.instance, "refresh");
        for (final VersionHandlerListener listener : this.listeners) {
            listener.onVersionRefreshed(vm);
        }
    }
    
    @Override
    public void onVersionSelected(final List<VersionSyncInfo> version) {
        this.selected = version;
        if (version == null || version.isEmpty() || version.get(0).getID() == null) {
            this.onVersionDeselected();
        }
        else {
            for (final VersionHandlerListener listener : this.listeners) {
                listener.onVersionSelected(version);
            }
        }
    }
    
    @Override
    public void onVersionDeselected() {
        this.selected = null;
        for (final VersionHandlerListener listener : this.listeners) {
            listener.onVersionDeselected();
        }
    }
    
    @Override
    public void onVersionDownload(final List<VersionSyncInfo> list) {
        this.downloading = list;
        for (final VersionHandlerListener listener : this.listeners) {
            listener.onVersionDownload(list);
        }
    }
}
