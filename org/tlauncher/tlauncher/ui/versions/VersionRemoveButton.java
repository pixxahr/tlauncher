// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.tlauncher.managers.VersionManager;
import java.util.Iterator;
import java.util.List;
import net.minecraft.launcher.updater.LocalVersionList;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import net.minecraft.launcher.updater.VersionSyncInfo;
import java.util.ArrayList;
import java.awt.Component;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;
import javax.swing.JPopupMenu;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;

public class VersionRemoveButton extends ImageUdaterButton implements VersionHandlerListener, Blockable
{
    private static final long serialVersionUID = 427368162418879141L;
    private static final String ILLEGAL_SELECTION_BLOCK = "illegal-selection";
    private static final String PREFIX = "version.manager.delete.";
    private static final String ERROR = "version.manager.delete.error.";
    private static final String ERROR_TITLE = "version.manager.delete.error.title";
    private static final String MENU = "version.manager.delete.menu.";
    private final VersionHandler handler;
    private final JPopupMenu menu;
    private final LocalizableMenuItem onlyJar;
    private final LocalizableMenuItem withLibraries;
    private boolean libraries;
    
    VersionRemoveButton(final VersionList list) {
        super(Color.WHITE, "delete-version.png");
        (this.handler = list.handler).addListener(this);
        this.menu = new JPopupMenu();
        (this.onlyJar = new LocalizableMenuItem("version.manager.delete.menu.jar")).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                VersionRemoveButton.this.onChosen(false);
            }
        });
        this.menu.add(this.onlyJar);
        (this.withLibraries = new LocalizableMenuItem("version.manager.delete.menu.libraries")).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                VersionRemoveButton.this.onChosen(true);
            }
        });
        this.menu.add(this.withLibraries);
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                VersionRemoveButton.this.onChosen(false);
            }
        });
    }
    
    void onPressed() {
        this.menu.show(this, 0, this.getHeight());
    }
    
    void onChosen(final boolean removeLibraries) {
        this.libraries = removeLibraries;
        this.handler.thread.deleteThread.iterate();
    }
    
    void delete() {
        if (this.handler.selected != null) {
            final LocalVersionList localList = this.handler.vm.getLocalList();
            final List<Throwable> errors = new ArrayList<Throwable>();
            for (final VersionSyncInfo version : this.handler.selected) {
                if (version.isInstalled()) {
                    try {
                        localList.deleteVersion(version.getID(), this.libraries);
                    }
                    catch (Throwable e) {
                        errors.add(e);
                    }
                }
            }
            if (!errors.isEmpty()) {
                final String title = Localizable.get("version.manager.delete.error.title");
                final String message = Localizable.get("version.manager.delete.error." + ((errors.size() == 1) ? "single" : "multiply"), errors);
                Alert.showError(title, message);
            }
        }
        this.handler.refresh();
    }
    
    @Override
    public void onVersionRefreshing(final VersionManager vm) {
    }
    
    @Override
    public void onVersionRefreshed(final VersionManager vm) {
    }
    
    @Override
    public void onVersionSelected(final List<VersionSyncInfo> versions) {
        boolean onlyRemote = true;
        for (final VersionSyncInfo version : versions) {
            if (version.isInstalled()) {
                onlyRemote = false;
                break;
            }
        }
        Blocker.setBlocked(this, "illegal-selection", onlyRemote);
    }
    
    @Override
    public void onVersionDeselected() {
        Blocker.block(this, "illegal-selection");
    }
    
    @Override
    public void onVersionDownload(final List<VersionSyncInfo> list) {
    }
    
    @Override
    public void block(final Object reason) {
        this.setEnabled(false);
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
}
