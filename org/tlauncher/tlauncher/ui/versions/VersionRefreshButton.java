// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.Image;
import net.minecraft.launcher.updater.VersionSyncInfo;
import java.util.List;
import org.tlauncher.tlauncher.managers.VersionManager;
import java.awt.Component;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;
import javax.swing.JPopupMenu;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;

public class VersionRefreshButton extends ImageUdaterButton implements VersionHandlerListener, Blockable
{
    private static final long serialVersionUID = -7148657244927244061L;
    private static final String PREFIX = "version.manager.refresher.";
    private static final String MENU = "version.manager.refresher.menu.";
    final VersionHandler handler;
    private final JPopupMenu menu;
    private final LocalizableMenuItem local;
    private final LocalizableMenuItem remote;
    private ButtonState state;
    
    VersionRefreshButton(final VersionList list) {
        super(VersionRefreshButton.GREEN_COLOR);
        this.handler = list.handler;
        this.menu = new JPopupMenu();
        (this.local = new LocalizableMenuItem("version.manager.refresher.menu.local")).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                VersionRefreshButton.this.handler.refresh();
            }
        });
        this.menu.add(this.local);
        (this.remote = new LocalizableMenuItem("version.manager.refresher.menu.remote")).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                VersionRefreshButton.this.handler.asyncRefresh();
            }
        });
        this.menu.add(this.remote);
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                VersionRefreshButton.this.handler.asyncRefresh();
            }
        });
        this.setState(ButtonState.REFRESH);
        this.handler.addListener(this);
    }
    
    void onPressed() {
        switch (this.state) {
            case CANCEL: {
                this.handler.stopRefresh();
                break;
            }
            case REFRESH: {
                this.menu.show(this, 0, this.getHeight());
                break;
            }
        }
    }
    
    private void setState(final ButtonState state) {
        if (state == null) {
            throw new NullPointerException();
        }
        this.state = state;
        this.setImage(state.image);
    }
    
    @Override
    public void onVersionRefreshing(final VersionManager vm) {
        this.setState(ButtonState.CANCEL);
    }
    
    @Override
    public void onVersionRefreshed(final VersionManager vm) {
        this.setState(ButtonState.REFRESH);
    }
    
    @Override
    public void onVersionSelected(final List<VersionSyncInfo> versions) {
    }
    
    @Override
    public void onVersionDeselected() {
    }
    
    @Override
    public void onVersionDownload(final List<VersionSyncInfo> list) {
    }
    
    @Override
    public void block(final Object reason) {
        if (!reason.equals("refresh")) {
            this.setEnabled(false);
        }
    }
    
    @Override
    public void unblock(final Object reason) {
        this.setEnabled(true);
    }
    
    enum ButtonState
    {
        REFRESH("refresh.png"), 
        CANCEL("cancel.png");
        
        final Image image;
        
        private ButtonState(final String image) {
            this.image = ImageCache.getImage(image);
        }
    }
}
