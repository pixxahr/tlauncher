// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.swing.extended.HTMLLabel;
import org.tlauncher.tlauncher.ui.swing.ResizeableComponent;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.ui.center.CenterPanel;

public class VersionTipPanel extends CenterPanel implements LocalizableComponent, ResizeableComponent
{
    private final HTMLLabel tip;
    
    VersionTipPanel(final VersionHandler handler) {
        super(CenterPanel.tipTheme, CenterPanel.squareInsets);
        this.add(this.tip = new HTMLLabel());
        this.tip.addPropertyChangeListener("html", new PropertyChangeListener() {
            @Override
            public void propertyChange(final PropertyChangeEvent evt) {
                final Object o = evt.getNewValue();
                if (o == null || !(o instanceof View)) {
                    return;
                }
                final View view = (View)o;
                BasicHTML.getHTMLBaseline(view, 500 - VersionTipPanel.this.getHorizontalInsets(), 0);
            }
        });
        this.updateLocale();
    }
    
    @Override
    public void updateLocale() {
        this.tip.setText("");
        String text = Localizable.get("version.list.tip");
        if (text == null) {
            return;
        }
        text = text.replace("{Ctrl}", OS.OSX.isCurrent() ? "Command" : "Ctrl");
        this.tip.setText(text);
        this.onResize();
    }
    
    @Override
    public void onResize() {
        this.setSize(500, this.tip.getHeight() + this.getVerticalInsets());
    }
    
    private int getVerticalInsets() {
        return this.getInsets().top + this.getInsets().bottom;
    }
    
    private int getHorizontalInsets() {
        return this.getInsets().left + this.getInsets().right;
    }
}
