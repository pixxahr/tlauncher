// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.tlauncher.ui.swing.VersionCellRenderer;
import java.util.Collection;
import org.tlauncher.tlauncher.managers.VersionManager;
import java.util.List;
import java.awt.LayoutManager;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import java.awt.GridLayout;
import javax.swing.plaf.ScrollBarUI;
import org.tlauncher.tlauncher.ui.swing.scroll.VersionScrollBarUI;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.swing.ScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import java.awt.Component;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.server.BackPanel;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.ui.swing.extended.BorderPanel;
import org.tlauncher.tlauncher.ui.swing.ImageButton;
import javax.swing.JList;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.ui.swing.SimpleListModel;
import org.tlauncher.tlauncher.ui.center.CenterPanel;

public class VersionList extends CenterPanel implements VersionHandlerListener
{
    private static final long serialVersionUID = -7192156096621636270L;
    final VersionHandler handler;
    public final SimpleListModel<VersionSyncInfo> model;
    public final JList<VersionSyncInfo> list;
    VersionDownloadButton download;
    VersionRemoveButton remove;
    public final ImageButton refresh;
    
    VersionList(final VersionHandler h) {
        super(VersionList.squareInsets);
        this.handler = h;
        final BorderPanel panel = new BorderPanel(0, 0);
        final BackPanel backPanel = new BackPanel("version.manager.list", new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    VersionList.this.handler.exitEditor();
                }
            }
        }, ImageCache.getIcon("back-arrow.png"));
        panel.setNorth(backPanel);
        this.model = new SimpleListModel<VersionSyncInfo>();
        (this.list = new JList<VersionSyncInfo>(this.model)).setCellRenderer(new VersionListCellRenderer(this));
        this.list.setSelectionMode(2);
        this.list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent e) {
                VersionList.this.handler.onVersionSelected(VersionList.this.list.getSelectedValuesList());
            }
        });
        final ScrollPane pane = new ScrollPane(this.list, ScrollPane.ScrollBarPolicy.AS_NEEDED, ScrollPane.ScrollBarPolicy.NEVER);
        pane.getVerticalScrollBar().setPreferredSize(new Dimension(10, 0));
        pane.getVerticalScrollBar().setUI(new VersionScrollBarUI());
        panel.setCenter(pane);
        final ExtendedPanel buttons = new ExtendedPanel(new GridLayout(0, 3));
        buttons.setInsets(0, -1, 0, -1);
        buttons.add(this.refresh = new VersionRefreshButton(this));
        buttons.add(this.download = new VersionDownloadButton(this));
        buttons.add(this.remove = new VersionRemoveButton(this));
        panel.setSouth(buttons);
        this.add(panel);
        this.handler.addListener(this);
        this.setSize(500, 400);
    }
    
    void select(final List<VersionSyncInfo> list) {
        if (list == null) {
            return;
        }
        final int size = list.size();
        final int[] indexes = new int[list.size()];
        for (int i = 0; i < size; ++i) {
            indexes[i] = this.model.indexOf(list.get(i));
        }
        this.list.setSelectedIndices(indexes);
    }
    
    void deselect() {
        this.list.clearSelection();
    }
    
    void refreshFrom(final VersionManager manager) {
        this.setRefresh(false);
        final List<VersionSyncInfo> list = manager.getVersions(this.handler.filter, false);
        this.model.addAll(list);
    }
    
    void setRefresh(final boolean refresh) {
        this.model.clear();
        if (refresh) {
            this.model.add(VersionCellRenderer.LOADING);
        }
    }
    
    @Override
    public void block(final Object reason) {
        Blocker.blockComponents(reason, this.list, this.refresh, this.remove);
    }
    
    @Override
    public void unblock(final Object reason) {
        Blocker.unblockComponents(reason, this.list, this.refresh, this.remove);
    }
    
    @Override
    public void onVersionRefreshing(final VersionManager vm) {
        this.setRefresh(true);
    }
    
    @Override
    public void onVersionRefreshed(final VersionManager vm) {
        this.refreshFrom(vm);
    }
    
    @Override
    public void onVersionSelected(final List<VersionSyncInfo> version) {
    }
    
    @Override
    public void onVersionDeselected() {
    }
    
    @Override
    public void onVersionDownload(final List<VersionSyncInfo> list) {
        this.select(list);
    }
}
