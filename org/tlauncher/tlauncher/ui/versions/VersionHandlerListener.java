// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.versions;

import net.minecraft.launcher.updater.VersionSyncInfo;
import java.util.List;
import org.tlauncher.tlauncher.managers.VersionManager;

public interface VersionHandlerListener
{
    void onVersionRefreshing(final VersionManager p0);
    
    void onVersionRefreshed(final VersionManager p0);
    
    void onVersionSelected(final List<VersionSyncInfo> p0);
    
    void onVersionDeselected();
    
    void onVersionDownload(final List<VersionSyncInfo> p0);
}
