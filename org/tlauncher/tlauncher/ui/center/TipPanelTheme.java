// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

import java.awt.Color;

public class TipPanelTheme extends DefaultCenterPanelTheme
{
    private final Color borderColor;
    
    public TipPanelTheme() {
        this.borderColor = this.failureColor;
    }
    
    @Override
    public Color getBorder() {
        return this.borderColor;
    }
}
