// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

public interface CenterPanelThemed
{
    CenterPanelTheme getTheme();
    
    void setTheme(final CenterPanelTheme p0);
}
