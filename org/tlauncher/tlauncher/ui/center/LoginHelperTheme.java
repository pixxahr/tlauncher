// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

import java.awt.Color;

public class LoginHelperTheme extends TipPanelTheme
{
    private final Color borderColor;
    private final Color BACKROUND_COLOR;
    private final Color FOREGROUND_COLOR;
    
    public LoginHelperTheme() {
        this.borderColor = new Color(96, 204, 240);
        this.BACKROUND_COLOR = new Color(96, 204, 240);
        this.FOREGROUND_COLOR = Color.WHITE;
    }
    
    @Override
    public Color getBorder() {
        return this.borderColor;
    }
    
    @Override
    public Color getBackground() {
        return this.BACKROUND_COLOR;
    }
    
    public Color getForeground() {
        return this.FOREGROUND_COLOR;
    }
}
