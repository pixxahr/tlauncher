// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

import java.awt.Color;

public abstract class CenterPanelTheme
{
    public abstract Color getBackground();
    
    public abstract Color getPanelBackground();
    
    public abstract Color getFocus();
    
    public abstract Color getFocusLost();
    
    public abstract Color getSuccess();
    
    public abstract Color getFailure();
    
    public abstract Color getBorder();
    
    public abstract Color getDelPanel();
}
