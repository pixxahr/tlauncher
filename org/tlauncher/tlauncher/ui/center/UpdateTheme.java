// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

import java.awt.Color;

public class UpdateTheme extends DefaultCenterPanelTheme
{
    private final Color backGroundColorServer;
    
    public UpdateTheme() {
        this.backGroundColorServer = new Color(252, 247, 244);
    }
    
    @Override
    public Color getPanelBackground() {
        return this.backGroundColorServer;
    }
    
    @Override
    public Color getBorder() {
        return this.backGroundColorServer;
    }
}
