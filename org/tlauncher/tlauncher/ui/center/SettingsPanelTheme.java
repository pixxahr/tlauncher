// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

import java.awt.Color;

public class SettingsPanelTheme extends DefaultCenterPanelTheme
{
    protected final Color panelBackgroundColor;
    protected final Color borderColor;
    protected final Color delPanelColor;
    
    public SettingsPanelTheme() {
        this.panelBackgroundColor = new Color(246, 244, 243);
        this.borderColor = new Color(172, 172, 172, 255);
        this.delPanelColor = new Color(50, 80, 190, 255);
    }
    
    @Override
    public Color getPanelBackground() {
        return this.panelBackgroundColor;
    }
    
    @Override
    public Color getBorder() {
        return this.borderColor;
    }
    
    @Override
    public Color getDelPanel() {
        return this.delPanelColor;
    }
}
