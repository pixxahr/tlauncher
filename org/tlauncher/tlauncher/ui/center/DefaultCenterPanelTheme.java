// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

import java.awt.Color;

public class DefaultCenterPanelTheme extends CenterPanelTheme
{
    protected final Color backgroundColor;
    protected final Color panelBackgroundColor;
    protected final Color focusColor;
    protected final Color focusLostColor;
    protected final Color successColor;
    protected final Color failureColor;
    protected final Color borderColor;
    protected final Color delPanelColor;
    
    public DefaultCenterPanelTheme() {
        this.backgroundColor = new Color(255, 255, 255, 255);
        this.panelBackgroundColor = new Color(255, 255, 255, 128);
        this.focusColor = new Color(0, 0, 0, 255);
        this.focusLostColor = new Color(128, 128, 128, 255);
        this.successColor = new Color(78, 196, 78, 255);
        this.failureColor = Color.getHSBColor(0.0f, 0.3f, 1.0f);
        this.borderColor = new Color(28, 128, 28, 255);
        this.delPanelColor = this.successColor;
    }
    
    @Override
    public Color getBackground() {
        return this.backgroundColor;
    }
    
    @Override
    public Color getPanelBackground() {
        return this.panelBackgroundColor;
    }
    
    @Override
    public Color getFocus() {
        return this.focusColor;
    }
    
    @Override
    public Color getFocusLost() {
        return this.focusLostColor;
    }
    
    @Override
    public Color getSuccess() {
        return this.successColor;
    }
    
    @Override
    public Color getFailure() {
        return this.failureColor;
    }
    
    @Override
    public Color getBorder() {
        return this.borderColor;
    }
    
    @Override
    public Color getDelPanel() {
        return this.delPanelColor;
    }
}
