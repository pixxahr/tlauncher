// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

import java.awt.Color;

public class LoadingPanelTheme extends DefaultCenterPanelTheme
{
    protected final Color panelBackgroundColor;
    
    public LoadingPanelTheme() {
        this.panelBackgroundColor = new Color(255, 255, 255, 168);
    }
    
    @Override
    public Color getPanelBackground() {
        return this.panelBackgroundColor;
    }
}
