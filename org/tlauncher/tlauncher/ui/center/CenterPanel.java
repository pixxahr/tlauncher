// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.center;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.swing.extended.UnblockablePanel;
import java.awt.GridLayout;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.swing.Del;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.BoxLayout;
import org.tlauncher.tlauncher.configuration.LangConfiguration;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import java.awt.Insets;
import org.tlauncher.tlauncher.ui.block.BlockablePanel;

public class CenterPanel extends BlockablePanel
{
    private static final long serialVersionUID = -1975869198322761508L;
    public static final CenterPanelTheme defaultTheme;
    public static final CenterPanelTheme tipTheme;
    public static final CenterPanelTheme loadingTheme;
    public static final CenterPanelTheme settingsTheme;
    public static final CenterPanelTheme updateTheme;
    public static final Insets defaultInsets;
    public static final Insets squareInsets;
    public static final Insets ACCOUNT_LOGIN_INSETS;
    public static final Insets accountInsets;
    public static final Insets VERSION_LOGIN_INSETS;
    public static final Insets PLAY_INSETS;
    public static final Insets SERVER_SQUARE_INSETS;
    public static final Insets smallSquareInsets;
    public static final Insets smallSquareNoTopInsets;
    public static final Insets noInsets;
    public static final Insets ISETS_20;
    public static final Insets MIDDLE_PANEL;
    protected static final int ARC_SIZE = 24;
    private final Insets insets;
    private final CenterPanelTheme theme;
    protected final ExtendedPanel messagePanel;
    protected final LocalizableLabel messageLabel;
    public final TLauncher tlauncher;
    public final Configuration global;
    public final LangConfiguration lang;
    
    public CenterPanel() {
        this(null, null);
    }
    
    public CenterPanel(final Insets insets) {
        this(null, insets);
    }
    
    public CenterPanel(final CenterPanelTheme theme) {
        this(theme, null);
    }
    
    public CenterPanel(CenterPanelTheme theme, Insets insets) {
        this.tlauncher = TLauncher.getInstance();
        this.global = this.tlauncher.getConfiguration();
        this.lang = this.tlauncher.getLang();
        theme = (this.theme = ((theme == null) ? CenterPanel.defaultTheme : theme));
        insets = (this.insets = ((insets == null) ? CenterPanel.defaultInsets : insets));
        this.setLayout(new BoxLayout(this, 3));
        this.setBackground(theme.getPanelBackground());
        (this.messageLabel = new LocalizableLabel("  ")).setFont(this.getFont().deriveFont(1));
        this.messageLabel.setVerticalAlignment(0);
        this.messageLabel.setHorizontalTextPosition(0);
        this.messageLabel.setAlignmentX(0.5f);
        (this.messagePanel = new ExtendedPanel()).setLayout(new BoxLayout(this.messagePanel, 1));
        this.messagePanel.setAlignmentX(0.5f);
        this.messagePanel.setInsets(new Insets(3, 0, 3, 0));
        this.messagePanel.add(this.messageLabel);
    }
    
    public CenterPanelTheme getTheme() {
        return this.theme;
    }
    
    @Override
    public Insets getInsets() {
        return this.insets;
    }
    
    protected Del del(final int aligment) {
        return new Del(1, aligment, this.theme.getBorder());
    }
    
    protected Del del(final int aligment, final int width, final int height) {
        return new Del(1, aligment, width, height, this.theme.getBorder());
    }
    
    public void defocus() {
        this.requestFocusInWindow();
    }
    
    public boolean setError(final String message) {
        this.messageLabel.setForeground(this.theme.getFailure());
        this.messageLabel.setText((message == null || message.length() == 0) ? " " : message);
        return false;
    }
    
    protected boolean setMessage(final String message, final Object... vars) {
        this.messageLabel.setForeground(this.theme.getFocus());
        this.messageLabel.setText((message == null || message.length() == 0) ? " " : message, vars);
        return true;
    }
    
    protected boolean setMessage(final String message) {
        return this.setMessage(message, Localizable.EMPTY_VARS);
    }
    
    public static BlockablePanel sepPan(final LayoutManager manager, final Component... components) {
        final BlockablePanel panel = new BlockablePanel(manager) {
            private static final long serialVersionUID = 1L;
            
            @Override
            public Insets getInsets() {
                return CenterPanel.noInsets;
            }
        };
        panel.add(components);
        return panel;
    }
    
    public static BlockablePanel sepPan(final Component... components) {
        return sepPan(new GridLayout(0, 1), components);
    }
    
    public static UnblockablePanel uSepPan(final LayoutManager manager, final Component... components) {
        final UnblockablePanel panel = new UnblockablePanel(manager) {
            private static final long serialVersionUID = 1L;
            
            @Override
            public Insets getInsets() {
                return CenterPanel.noInsets;
            }
        };
        panel.add(components);
        return panel;
    }
    
    public static UnblockablePanel uSepPan(final Component... components) {
        return uSepPan(new GridLayout(0, 1), components);
    }
    
    protected void log(final Object... o) {
        U.log("[" + this.getClass().getSimpleName() + "]", o);
    }
    
    static {
        defaultTheme = new DefaultCenterPanelTheme();
        tipTheme = new TipPanelTheme();
        loadingTheme = new LoadingPanelTheme();
        settingsTheme = new SettingsPanelTheme();
        updateTheme = new UpdateTheme();
        defaultInsets = new Insets(5, 24, 18, 24);
        squareInsets = new Insets(11, 15, 11, 15);
        ACCOUNT_LOGIN_INSETS = new Insets(11, 19, 11, 0);
        accountInsets = new Insets(0, 20, 1, 10);
        VERSION_LOGIN_INSETS = new Insets(10, 20, 10, 0);
        PLAY_INSETS = new Insets(11, 0, 11, 0);
        SERVER_SQUARE_INSETS = new Insets(1, 10, 10, 10);
        smallSquareInsets = new Insets(7, 7, 7, 7);
        smallSquareNoTopInsets = new Insets(5, 15, 5, 15);
        noInsets = new Insets(0, 0, 0, 0);
        ISETS_20 = new Insets(20, 20, 20, 20);
        MIDDLE_PANEL = new Insets(20, 20, 5, 20);
    }
}
