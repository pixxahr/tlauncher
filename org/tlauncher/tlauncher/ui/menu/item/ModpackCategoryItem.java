// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.menu.item;

import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JComponent;
import org.tlauncher.util.ColorUtil;
import java.awt.Graphics;
import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;

public class ModpackCategoryItem extends LocalizableMenuItem
{
    public ModpackCategoryItem(final String path) {
        super(path);
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        final Rectangle r = this.getVisibleRect();
        g.setColor(ColorUtil.BLUE_MODPACK);
        g.fillRect(r.x, r.y, r.width, r.height);
        this.paintText(g, this, r, this.getText());
    }
    
    protected void paintText(final Graphics g, final JComponent c, final Rectangle r, final String text) {
        final Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        final FontMetrics fm = g2d.getFontMetrics();
        final Rectangle2D textRect = fm.getStringBounds(text, g2d);
        g.setFont(this.getFont());
        g.setColor(Color.WHITE);
        final int x = (int)(r.getWidth() - textRect.getWidth()) / 2;
        final int y = (int)(r.getHeight() - (int)textRect.getHeight()) / 2 + fm.getAscent() - 1;
        g2d.drawString(text, x, y);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    }
}
