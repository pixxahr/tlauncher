// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.menu;

import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Graphics;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.JList;
import java.awt.event.ActionEvent;
import javax.swing.SwingUtilities;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import org.tlauncher.tlauncher.ui.util.ViewlUtil;
import org.tlauncher.util.U;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.MouseInfo;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.tlauncher.tlauncher.managers.popup.menu.HotServerManager;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import java.awt.Component;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.swing.VersionCellRenderer;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.swing.box.TlauncherCustomBox;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import javax.swing.JLayeredPane;
import java.awt.Point;
import javax.swing.DefaultListCellRenderer;
import net.minecraft.launcher.updater.VersionSyncInfo;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.Icon;
import java.awt.Color;
import javax.swing.JPanel;

public class PopupMenuView extends JPanel
{
    private static final int MAX_SIZE_VERSION = 30;
    private static final Color BACKGROUND_TITLE;
    private static final Icon TLAUNCHER_USER_ICON;
    private final JLabel title;
    private final JComboBox<VersionSyncInfo> box;
    private final JLabel versionLabel;
    private final DefaultListCellRenderer defaultRenderer;
    private Point point;
    private JLayeredPane defaultScene;
    private PopupUpdaterButton start;
    private PopupUpdaterButton copy;
    private PopupUpdaterButton favorite;
    
    public PopupMenuView(final JLayeredPane defaultScene) {
        this.defaultRenderer = new DefaultListCellRenderer();
        this.setVisible(false);
        this.defaultScene = defaultScene;
        final SpringLayout springLayout = new SpringLayout();
        this.setLayout(springLayout);
        this.setSize(290, 150);
        this.setBackground(Color.WHITE);
        (this.versionLabel = new JLabel(Localizable.get().get("menu.version"))).setForeground(ColorUtil.COLOR_77);
        final JLabel renderer;
        boolean skin;
        (this.box = new TlauncherCustomBox<VersionSyncInfo>()).setRenderer((list, value, index, isSelected, cellHasFocus) -> {
            renderer = (JLabel)this.defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value == null) {
                return null;
            }
            else {
                skin = TLauncher.getInstance().getTLauncherManager().useTlauncherAuthlib(value.getAvailableVersion());
                if (skin) {
                    renderer.setIcon(PopupMenuView.TLAUNCHER_USER_ICON);
                }
                renderer.setText(trimId(VersionCellRenderer.getLabelFor(value)));
                renderer.setAlignmentY(0.5f);
                renderer.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 0));
                renderer.setOpaque(true);
                if (isSelected) {
                    renderer.setBackground(ColorUtil.COLOR_213);
                }
                else {
                    renderer.setBackground(Color.white);
                }
                renderer.setForeground(ColorUtil.COLOR_77);
                return renderer;
            }
        });
        this.box.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(149, 149, 149)));
        (this.title = new JLabel()).setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        springLayout.putConstraint("North", this.title, 0, "North", this);
        springLayout.putConstraint("West", this.title, 0, "West", this);
        springLayout.putConstraint("South", this.title, 30, "North", this);
        springLayout.putConstraint("East", this.title, 0, "East", this);
        this.add(this.title);
        this.title.setOpaque(true);
        this.title.setForeground(Color.WHITE);
        this.title.setBackground(PopupMenuView.BACKGROUND_TITLE);
        SwingUtil.setFontSize(this.title, 13.0f);
        (this.start = new PopupUpdaterButton(Color.white, Localizable.get().get("menu.start"))).setBorder(BorderFactory.createEmptyBorder());
        springLayout.putConstraint("North", this.start, 30, "North", this);
        springLayout.putConstraint("West", this.start, 0, "West", this);
        springLayout.putConstraint("South", this.start, 60, "North", this);
        springLayout.putConstraint("East", this.start, 0, "East", this);
        this.add(this.start);
        (this.copy = new PopupUpdaterButton(Color.white, Localizable.get().get("menu.copy"))).setBorder(BorderFactory.createEmptyBorder());
        springLayout.putConstraint("North", this.copy, 60, "North", this);
        springLayout.putConstraint("West", this.copy, 0, "West", this);
        springLayout.putConstraint("South", this.copy, 90, "North", this);
        springLayout.putConstraint("East", this.copy, 0, "East", this);
        this.add(this.copy);
        (this.favorite = new PopupUpdaterButton(Color.white, Localizable.get().get("menu.favorite"))).setBorder(BorderFactory.createEmptyBorder());
        springLayout.putConstraint("North", this.favorite, 90, "North", this);
        springLayout.putConstraint("West", this.favorite, 0, "West", this);
        springLayout.putConstraint("South", this.favorite, 120, "North", this);
        springLayout.putConstraint("East", this.favorite, 0, "East", this);
        this.add(this.favorite);
        springLayout.putConstraint("North", this.versionLabel, 122, "North", this);
        springLayout.putConstraint("West", this.versionLabel, 10, "West", this);
        springLayout.putConstraint("South", this.versionLabel, 146, "North", this);
        springLayout.putConstraint("East", this.versionLabel, 90, "West", this);
        this.add(this.versionLabel);
        springLayout.putConstraint("North", this.box, 122, "North", this);
        springLayout.putConstraint("West", this.box, 90, "West", this);
        springLayout.putConstraint("South", this.box, 146, "North", this);
        springLayout.putConstraint("East", this.box, -10, "East", this);
        this.add(this.box);
        final HotServerManager manager = (HotServerManager)TLauncher.getInjector().getInstance((Class)HotServerManager.class);
        this.favorite.addActionListener(e -> manager.addServerToList(true, (VersionSyncInfo)this.box.getSelectedItem()));
        this.copy.addActionListener(e -> manager.copyAddress());
        this.start.addActionListener(e -> {
            manager.launchGame((VersionSyncInfo)this.box.getSelectedItem());
            this.setVisible(false);
            return;
        });
        this.box.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(final PopupMenuEvent e) {
                PopupMenuView.this.point = MouseInfo.getPointerInfo().getLocation();
            }
            
            @Override
            public void popupMenuWillBecomeInvisible(final PopupMenuEvent e) {
            }
            
            @Override
            public void popupMenuCanceled(final PopupMenuEvent e) {
            }
        });
        this.box.addActionListener(e -> {
            try {
                new Robot().mouseMove(this.point.x, this.point.y);
            }
            catch (AWTException e1) {
                this.log(e1);
            }
            return;
        });
        final MouseAdapter m = new MouseAdapter() {
            @Override
            public void mouseExited(final MouseEvent e) {
                if (PopupMenuView.this.isShowing() && !PopupMenuView.this.mouseIsOverDisplayPanel(PopupMenuView.this)) {
                    PopupMenuView.this.setVisible(false);
                }
            }
        };
        this.addMouseListener(m);
        this.start.addMouseListener(m);
        this.copy.addMouseListener(m);
        this.favorite.addMouseListener(m);
        this.box.addMouseListener(m);
        this.box.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(final PopupMenuEvent e) {
            }
            
            @Override
            public void popupMenuWillBecomeInvisible(final PopupMenuEvent e) {
                if (PopupMenuView.this.isShowing() && !PopupMenuView.this.mouseIsOverDisplayPanel(PopupMenuView.this)) {
                    PopupMenuView.this.setVisible(false);
                }
            }
            
            @Override
            public void popupMenuCanceled(final PopupMenuEvent e) {
            }
        });
    }
    
    private boolean mouseIsOverDisplayPanel(final Component component) {
        final Point c = component.getLocationOnScreen();
        final Point m = MouseInfo.getPointerInfo().getLocation();
        return m.x >= c.x && m.x < c.x + component.getWidth() - 1 && m.y >= c.y && m.y < c.y + component.getHeight();
    }
    
    private static String trimId(final String version) {
        if (version.length() > 30) {
            return version.substring(0, 30);
        }
        return version;
    }
    
    private void log(final Object e) {
        U.log("[PopupMenuView] ", e);
    }
    
    public void showSelectedModel(final PopupMenuModel model) {
        final String v;
        final Point p;
        final Point p2;
        SwingUtilities.invokeLater(() -> {
            v = ViewlUtil.addSpaces(Localizable.get().get("menu.chooseVersion") + " " + model.getName(), model.getName());
            this.title.setText(v);
            this.start.setText(Localizable.get().get("menu.start"));
            this.copy.setText(Localizable.get().get("menu.copy"));
            this.favorite.setText(Localizable.get().get("menu.favorite"));
            this.versionLabel.setText(Localizable.get().get("menu.version"));
            this.box.setModel(new DefaultComboBoxModel<VersionSyncInfo>(model.getServers().toArray(new VersionSyncInfo[0])));
            p = MouseInfo.getPointerInfo().getLocation();
            if (p.getY() > 680.0) {
                p.y = 680;
            }
            p2 = new Point(p.x - 35, p.y - 30);
            SwingUtilities.convertPointFromScreen(p2, this.defaultScene);
            this.setLocation(p2);
            this.setVisible(true);
        });
    }
    
    static {
        BACKGROUND_TITLE = new Color(60, 170, 232);
        TLAUNCHER_USER_ICON = (Icon)ImageCache.getIcon("tlauncher-user.png");
    }
    
    private class PopupUpdaterButton extends UpdaterButton
    {
        PopupUpdaterButton(final Color color, final String value) {
            this.setText(value);
            this.setOpaque(true);
            this.setBackground(color);
            this.setForeground(ColorUtil.COLOR_77);
            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(final MouseEvent e) {
                    PopupUpdaterButton.this.setBackground(ColorUtil.COLOR_235);
                }
                
                @Override
                public void mouseExited(final MouseEvent e) {
                    PopupUpdaterButton.this.setBackground(Color.WHITE);
                }
            });
        }
        
        @Override
        protected void paintText(final Graphics g, final JComponent c, final Rectangle textRect, final String text) {
            final Graphics2D g2d = (Graphics2D)g;
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            final FontMetrics fm = g2d.getFontMetrics();
            final Rectangle2D r = fm.getStringBounds(text, g2d);
            g.setFont(this.getFont());
            final int x = 10;
            final int y = (this.getHeight() - (int)r.getHeight()) / 2 + fm.getAscent();
            g2d.drawString(text, x, y);
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        }
    }
}
