// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.menu;

import java.util.Objects;
import org.tlauncher.tlauncher.entity.ServerInfo;
import net.minecraft.launcher.updater.VersionSyncInfo;
import java.util.List;

public class PopupMenuModel
{
    private List<VersionSyncInfo> servers;
    private ServerInfo info;
    private boolean mainPage;
    private VersionSyncInfo selected;
    
    public ServerInfo getInfo() {
        return this.info;
    }
    
    public PopupMenuModel(final List<VersionSyncInfo> servers, final ServerInfo info, final boolean mainPage) {
        this.info = info;
        this.servers = servers;
        this.mainPage = mainPage;
    }
    
    public String getName() {
        return this.getAddress().split(":")[0];
    }
    
    public String getAddress() {
        return this.info.getAddress();
    }
    
    public String getResolvedAddress() {
        if (Objects.nonNull(this.info.getRedirectAddress())) {
            return this.info.getRedirectAddress();
        }
        return this.getAddress();
    }
    
    public String getServerId() {
        return this.info.getServerId();
    }
    
    public List<VersionSyncInfo> getServers() {
        return this.servers;
    }
    
    public boolean isMainPage() {
        return this.mainPage;
    }
    
    public VersionSyncInfo getSelected() {
        return this.selected;
    }
    
    public void setServers(final List<VersionSyncInfo> servers) {
        this.servers = servers;
    }
    
    public void setInfo(final ServerInfo info) {
        this.info = info;
    }
    
    public void setMainPage(final boolean mainPage) {
        this.mainPage = mainPage;
    }
    
    public void setSelected(final VersionSyncInfo selected) {
        this.selected = selected;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof PopupMenuModel)) {
            return false;
        }
        final PopupMenuModel other = (PopupMenuModel)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$servers = this.getServers();
        final Object other$servers = other.getServers();
        Label_0065: {
            if (this$servers == null) {
                if (other$servers == null) {
                    break Label_0065;
                }
            }
            else if (this$servers.equals(other$servers)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$info = this.getInfo();
        final Object other$info = other.getInfo();
        Label_0102: {
            if (this$info == null) {
                if (other$info == null) {
                    break Label_0102;
                }
            }
            else if (this$info.equals(other$info)) {
                break Label_0102;
            }
            return false;
        }
        if (this.isMainPage() != other.isMainPage()) {
            return false;
        }
        final Object this$selected = this.getSelected();
        final Object other$selected = other.getSelected();
        if (this$selected == null) {
            if (other$selected == null) {
                return true;
            }
        }
        else if (this$selected.equals(other$selected)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof PopupMenuModel;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $servers = this.getServers();
        result = result * 59 + (($servers == null) ? 43 : $servers.hashCode());
        final Object $info = this.getInfo();
        result = result * 59 + (($info == null) ? 43 : $info.hashCode());
        result = result * 59 + (this.isMainPage() ? 79 : 97);
        final Object $selected = this.getSelected();
        result = result * 59 + (($selected == null) ? 43 : $selected.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "PopupMenuModel(servers=" + this.getServers() + ", info=" + this.getInfo() + ", mainPage=" + this.isMainPage() + ", selected=" + this.getSelected() + ")";
    }
}
