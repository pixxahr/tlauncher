// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.menu;

import javax.swing.JMenuItem;
import java.awt.Color;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import javax.swing.BorderFactory;
import org.tlauncher.tlauncher.ui.swing.renderer.ModpackComboxRenderer;
import javax.swing.JLabel;
import org.tlauncher.modpack.domain.client.share.Category;
import javax.swing.JPopupMenu;

public class ModpackCategoryPopupMenu extends JPopupMenu
{
    public ModpackCategoryPopupMenu(final Category c, final JLabel label) {
        this.setBorder(BorderFactory.createLineBorder(ModpackComboxRenderer.LINE));
        final ModpackPopup.ModpackMenuItem localizableMenuItem = new ModpackPopup.ModpackMenuItem("modpack." + c.name().toLowerCase());
        SwingUtil.changeFontFamily(localizableMenuItem, FontTL.ROBOTO_REGULAR, 12);
        localizableMenuItem.setBackground(Color.WHITE);
        this.add(localizableMenuItem);
    }
}
