// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.menu;

import javax.swing.plaf.MenuItemUI;
import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.plaf.basic.BasicMenuItemUI;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.ColorUtil;
import org.tlauncher.util.swing.FontTL;
import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;
import java.awt.Rectangle;
import java.awt.Graphics;
import org.tlauncher.tlauncher.ui.modpack.ModpackBackupFrame;
import org.tlauncher.tlauncher.ui.modpack.HandleInstallModpackElementFrame;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.tlauncher.ui.alert.Alert;
import javax.swing.JFrame;
import org.tlauncher.tlauncher.ui.modpack.AddedModpackStuffFrame;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.JMenuItem;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.JPopupMenu;

public class ModpackPopup extends JPopupMenu
{
    public static Color BACKGROUND_ITEM;
    public static Color LINE;
    
    public ModpackPopup() {
        this.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, ModpackPopup.LINE));
    }
    
    public ModpackPopup(final ModpackComboBox localmodpacks) {
        this();
        final ModpackMenuItem notFound = new ModpackMenuItem("modpack.backup.not.found");
        final ModpackMenuItem hand = new ModpackMenuItem("modpack.backup.install.hand");
        final ModpackMenuItem backup = new ModpackMenuItem("modpack.backup.settings");
        this.add(backup);
        this.add(notFound);
        this.add(hand);
        this.setPreferredSize(new Dimension(this.getPreferredSize().width, 97));
        notFound.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final AddedModpackStuffFrame addedModpackStuffView = new AddedModpackStuffFrame(TLauncher.getInstance().getFrame());
                addedModpackStuffView.setVisible(true);
            }
        });
        hand.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if (localmodpacks.getSelectedIndex() == 0) {
                    Alert.showLocMessage("modpack.select.modpack");
                    return;
                }
                final HandleInstallModpackElementFrame frame = new HandleInstallModpackElementFrame(TLauncher.getInstance().getFrame(), localmodpacks.getSelectedValue());
                frame.setVisible(true);
            }
        });
        backup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final ModpackBackupFrame frame = new ModpackBackupFrame(TLauncher.getInstance().getFrame(), localmodpacks);
                frame.setVisible(true);
            }
        });
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        final Rectangle rec = this.getVisibleRect();
        g.setColor(ModpackPopup.LINE);
        g.fillRect(rec.x, rec.y, rec.width, rec.height);
    }
    
    static {
        ModpackPopup.BACKGROUND_ITEM = new Color(240, 240, 240);
        ModpackPopup.LINE = new Color(204, 204, 204);
    }
    
    public static class ModpackMenuItem extends LocalizableMenuItem
    {
        int item_gup;
        
        public ModpackMenuItem(final String name) {
            super(name);
            this.item_gup = 20;
            this.setBackground(ModpackPopup.BACKGROUND_ITEM);
            SwingUtil.changeFontFamily(this, FontTL.ROBOTO_REGULAR, 12, ColorUtil.COLOR_16);
            this.setUI(new BasicMenuItemUI() {
                @Override
                protected void paintMenuItem(final Graphics g, final JComponent c, final Icon checkIcon, final Icon arrowIcon, final Color background, final Color foreground, final int gup) {
                    final Rectangle rec = c.getVisibleRect();
                    final ButtonModel model = ModpackMenuItem.this.getModel();
                    if (model.isPressed()) {
                        g.setColor(ColorUtil.COLOR_195);
                    }
                    else if (model.isArmed()) {
                        g.setColor(ColorUtil.COLOR_215);
                    }
                    else {
                        g.setColor(ModpackMenuItem.this.getBackground());
                    }
                    g.fillRect(rec.x, rec.y, rec.width, rec.height);
                    g.setColor(ModpackPopup.LINE);
                    ModpackMenuItem.this.paintBorder(g);
                    this.paintText(g, c, rec, ModpackMenuItem.this.getText());
                }
                
                protected void paintText(final Graphics g, final JComponent c, final Rectangle textRect, final String text) {
                    final Graphics2D g2d = (Graphics2D)g;
                    g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    final FontMetrics fm = g2d.getFontMetrics();
                    final Rectangle2D r = fm.getStringBounds(text, g2d);
                    g.setFont(ModpackMenuItem.this.getFont());
                    g.setColor(Color.BLACK);
                    final int x = ModpackMenuItem.this.item_gup;
                    final int y = (textRect.height - (int)r.getHeight()) / 2 + fm.getAscent();
                    g2d.drawString(text, x, y);
                    g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                }
            });
        }
        
        @Override
        public void paint(final Graphics g) {
            this.ui.paint(g, this);
        }
        
        @Override
        protected void paintBorder(final Graphics g) {
            final Rectangle rec = this.getVisibleRect();
            g.setColor(ModpackPopup.LINE);
            g.drawLine(rec.x, rec.y + rec.height - 1, rec.x + rec.width, rec.y + rec.height - 1);
        }
    }
}
