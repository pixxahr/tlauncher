// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.background;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.background.slide.SlideBackground;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedLayeredPane;

public class BackgroundHolder extends ExtendedLayeredPane
{
    private static final long serialVersionUID = 8722087129402330131L;
    public final MainPane pane;
    private Background currentBackground;
    public final BackgroundCover cover;
    public final SlideBackground SLIDE_BACKGROUND;
    
    public BackgroundHolder(final MainPane parent) {
        super(parent);
        this.pane = parent;
        this.cover = new BackgroundCover(this);
        this.SLIDE_BACKGROUND = new SlideBackground(this);
        this.add(this.cover, (Object)Integer.MAX_VALUE);
    }
    
    public Background getBackgroundPane() {
        return this.currentBackground;
    }
    
    public void setBackground(final Background background, final boolean animate) {
        if (background == null) {
            throw new NullPointerException();
        }
        Color coverColor = background.getCoverColor();
        if (coverColor == null) {
            coverColor = Color.black;
        }
        this.cover.setColor(coverColor, animate);
        this.cover.makeCover(animate);
        if (this.currentBackground != null) {
            this.remove(this.currentBackground);
        }
        this.add(this.currentBackground = background);
        this.cover.removeCover(animate);
    }
    
    public void showBackground() {
        this.cover.removeCover();
    }
    
    public void hideBackground() {
        this.cover.makeCover();
    }
    
    public void startBackground() {
        if (this.currentBackground == null) {
            return;
        }
        if (this.currentBackground instanceof AnimatedBackground) {
            ((AnimatedBackground)this.currentBackground).startBackground();
        }
    }
    
    public void suspendBackground() {
        if (this.currentBackground == null) {
            return;
        }
        if (this.currentBackground instanceof AnimatedBackground) {
            ((AnimatedBackground)this.currentBackground).suspendBackground();
        }
    }
    
    public void stopBackground() {
        if (this.currentBackground == null) {
            return;
        }
        if (this.currentBackground instanceof AnimatedBackground) {
            ((AnimatedBackground)this.currentBackground).stopBackground();
        }
    }
}
