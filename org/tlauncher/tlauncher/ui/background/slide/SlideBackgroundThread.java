// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.background.slide;

import org.tlauncher.tlauncher.ui.explorer.filters.ImageFileFilter;
import java.io.IOException;
import org.tlauncher.util.FileUtil;
import java.io.File;
import java.awt.Image;
import org.tlauncher.util.U;
import java.net.URL;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.util.regex.Pattern;
import org.tlauncher.util.async.LoopedThread;

public class SlideBackgroundThread extends LoopedThread
{
    private static final Pattern extensionPattern;
    private static final String defaultImageName = "plains.jpg";
    private final SlideBackground background;
    final Slide defaultSlide;
    private Slide currentSlide;
    
    SlideBackgroundThread(final SlideBackground background) {
        super("SlideBackgroundThread");
        this.background = background;
        this.defaultSlide = new Slide(ImageCache.getRes("plains.jpg"));
        this.startAndWait();
    }
    
    public SlideBackground getBackground() {
        return this.background;
    }
    
    public Slide getSlide() {
        return this.currentSlide;
    }
    
    public synchronized void refreshSlide(final boolean animate) {
        final String path = TLauncher.getInstance().getConfiguration().get("gui.background");
        final URL url = this.getImageURL(path);
        final Slide slide = (url == null) ? this.defaultSlide : new Slide(url);
        this.setSlide(slide, animate);
    }
    
    public void asyncRefreshSlide() {
        this.iterate();
    }
    
    public synchronized void setSlide(Slide slide, final boolean animate) {
        if (slide == null) {
            throw new NullPointerException();
        }
        if (slide.equals(this.currentSlide)) {
            return;
        }
        Image image = slide.getImage();
        if (image == null) {
            slide = this.defaultSlide;
            image = slide.getImage();
        }
        this.currentSlide = slide;
        if (image == null) {
            this.log("Default image is NULL. Check accessibility to the JAR file of TLauncher.");
            return;
        }
        this.background.holder.cover.makeCover(animate);
        this.background.setImage(image);
        U.sleepFor(500L);
        this.background.holder.cover.removeCover(animate);
    }
    
    @Override
    protected void iterateOnce() {
        this.refreshSlide(true);
    }
    
    private URL getImageURL(final String path) {
        this.log("Trying to resolve path:", path);
        if (path == null) {
            this.log("Na NULL i suda NULL.");
            return null;
        }
        final URL asURL = U.makeURL(path);
        if (asURL != null) {
            this.log("Path resolved as an URL:", asURL);
            return asURL;
        }
        final File asFile = new File(path);
        if (asFile.isFile()) {
            final String absPath = asFile.getAbsolutePath();
            this.log("Path resolved as a file:", absPath);
            final String ext = FileUtil.getExtension(asFile);
            if (ext == null || !SlideBackgroundThread.extensionPattern.matcher(ext).matches()) {
                this.log("This file doesn't seem to be an image. It should have JPG or PNG format.");
                return null;
            }
            try {
                return asFile.toURI().toURL();
            }
            catch (IOException e) {
                this.log("Cannot covert this file into URL.", e);
                return null;
            }
        }
        this.log("Cannot resolve this path.");
        return null;
    }
    
    protected void log(final Object... w) {
        U.log("[" + this.getClass().getSimpleName() + "]", w);
    }
    
    static {
        extensionPattern = ImageFileFilter.extensionPattern;
    }
}
