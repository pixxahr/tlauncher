// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.background.slide;

import java.awt.Graphics;
import java.awt.image.ImageObserver;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.Component;
import java.awt.Color;
import java.awt.Image;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedComponentAdapter;
import org.tlauncher.tlauncher.ui.background.BackgroundHolder;
import org.tlauncher.tlauncher.ui.background.Background;

public class SlideBackground extends Background
{
    private static final long serialVersionUID = -4479685866688951989L;
    private final SlideBackgroundThread thread;
    final BackgroundHolder holder;
    final ExtendedComponentAdapter listener;
    private Image oImage;
    private int oImageWidth;
    private int oImageHeight;
    private Image vImage;
    private int vImageWidth;
    private int vImageHeight;
    
    public SlideBackground(final BackgroundHolder holder) {
        super(holder, Color.black);
        this.holder = holder;
        (this.thread = new SlideBackgroundThread(this)).setSlide(this.thread.defaultSlide, false);
        this.thread.refreshSlide(false);
        this.addComponentListener(this.listener = new ExtendedComponentAdapter(this, 1000) {
            @Override
            public void onComponentResized(final ComponentEvent e) {
                SlideBackground.this.updateImage();
                SlideBackground.this.repaint();
            }
        });
    }
    
    public SlideBackgroundThread getThread() {
        return this.thread;
    }
    
    public Image getImage() {
        return this.oImage;
    }
    
    public void setImage(final Image image) {
        if (image == null) {
            throw new NullPointerException();
        }
        this.oImage = image;
        this.oImageWidth = image.getWidth(null);
        this.oImageHeight = image.getHeight(null);
        this.updateImage();
    }
    
    private void updateImage() {
        final double windowWidth = this.getWidth();
        final double windowHeight = this.getHeight();
        final double ratio = Math.min(this.oImageWidth / windowWidth, this.oImageHeight / windowHeight);
        double width;
        double height;
        if (ratio < 1.0) {
            width = this.oImageWidth;
            height = this.oImageHeight;
        }
        else {
            width = this.oImageWidth / ratio;
            height = this.oImageHeight / ratio;
        }
        this.vImageWidth = (int)width;
        this.vImageHeight = (int)height;
        if (this.vImageWidth == 0 || this.vImageHeight == 0) {
            this.vImage = null;
        }
        else if (this.oImageWidth == this.vImageWidth && this.oImageHeight == this.vImageHeight) {
            this.vImage = this.oImage;
        }
        else {
            this.vImage = this.oImage.getScaledInstance(this.vImageWidth, this.vImageHeight, 4);
        }
    }
    
    @Override
    public void paintBackground(final Graphics g) {
        if (this.vImage == null) {
            this.updateImage();
        }
        if (this.vImage == null) {
            return;
        }
        final double windowWidth = this.getWidth();
        final double windowHeight = this.getHeight();
        final double ratio = Math.min(this.vImageWidth / windowWidth, this.vImageHeight / windowHeight);
        final double width = this.vImageWidth / ratio;
        final double height = this.vImageHeight / ratio;
        final double x = (windowWidth - width) / 2.0;
        final double y = (windowHeight - height) / 2.0;
        g.drawImage(this.vImage, (int)x, (int)y, (int)width, (int)height, null);
    }
}
