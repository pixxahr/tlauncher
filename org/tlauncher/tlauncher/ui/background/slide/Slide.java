// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.background.slide;

import org.tlauncher.util.U;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import org.tlauncher.util.Reflect;
import java.awt.Image;
import java.net.URL;

public class Slide
{
    private final URL url;
    private Image image;
    
    public Slide(final URL url) {
        if (url == null) {
            throw new NullPointerException();
        }
        this.url = url;
        if (this.isLocal()) {
            this.load();
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        final Slide slide = Reflect.cast(o, Slide.class);
        return slide != null && this.url.equals(slide.url);
    }
    
    public URL getURL() {
        return this.url;
    }
    
    public boolean isLocal() {
        return this.url.getProtocol().equals("file");
    }
    
    public Image getImage() {
        if (this.image == null) {
            this.load();
        }
        return this.image;
    }
    
    private void load() {
        this.log("Loading from:", this.url);
        BufferedImage tempImage = null;
        try {
            tempImage = ImageIO.read(this.url);
        }
        catch (Throwable e) {
            this.log("Cannot load slide!", e);
            return;
        }
        if (tempImage == null) {
            this.log("Image seems to be corrupted.");
            return;
        }
        this.log("Loaded successfully!");
        this.image = tempImage;
    }
    
    protected void log(final Object... w) {
        U.log("[" + this.getClass().getSimpleName() + "]", w);
    }
}
