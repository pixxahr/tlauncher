// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.background;

import java.awt.Graphics;
import javax.swing.JComponent;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedLayeredPane;

public abstract class Background extends ExtendedLayeredPane
{
    private static final long serialVersionUID = -1353975966057230209L;
    protected Color coverColor;
    
    public Background(final BackgroundHolder holder, final Color coverColor) {
        super(holder);
        this.coverColor = coverColor;
    }
    
    public Color getCoverColor() {
        return this.coverColor;
    }
    
    @Override
    public final void paint(final Graphics g) {
        this.paintBackground(g);
        super.paint(g);
    }
    
    public abstract void paintBackground(final Graphics p0);
}
