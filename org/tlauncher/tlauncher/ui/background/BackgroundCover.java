// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.background;

import java.awt.Graphics;
import org.tlauncher.util.U;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.ResizeableComponent;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class BackgroundCover extends ExtendedPanel implements ResizeableComponent
{
    private static final long serialVersionUID = -1801217638400760969L;
    private static final double opacityStep = 0.01;
    private static final int timeFrame = 5;
    private final BackgroundHolder parent;
    private final Object animationLock;
    private double opacity;
    private Color opacityColor;
    private Color color;
    
    BackgroundCover(final BackgroundHolder parent, final Color opacityColor, final double opacity) {
        if (parent == null) {
            throw new NullPointerException();
        }
        this.parent = parent;
        this.setColor(opacityColor, false);
        this.setBgOpacity(opacity, false);
        this.animationLock = new Object();
    }
    
    BackgroundCover(final BackgroundHolder parent) {
        this(parent, Color.white, 0.0);
    }
    
    public void makeCover(final boolean animate) {
        synchronized (this.animationLock) {
            if (animate) {
                while (this.opacity < 1.0) {
                    this.setBgOpacity(this.opacity + 0.01, true);
                    U.sleepFor(5L);
                }
            }
            this.setBgOpacity(1.0, true);
        }
    }
    
    public void makeCover() {
        this.makeCover(true);
    }
    
    public void removeCover(final boolean animate) {
        synchronized (this.animationLock) {
            if (animate) {
                while (this.opacity > 0.0) {
                    this.setBgOpacity(this.opacity - 0.01, true);
                    U.sleepFor(5L);
                }
            }
            this.setBgOpacity(0.0, true);
        }
    }
    
    public void removeCover() {
        this.removeCover(true);
    }
    
    public boolean isCovered() {
        return this.opacity == 1.0;
    }
    
    public void toggleCover(final boolean animate) {
        if (this.isCovered()) {
            this.removeCover(animate);
        }
        else {
            this.makeCover(animate);
        }
    }
    
    @Override
    public void paint(final Graphics g) {
        g.setColor(this.opacityColor);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
    }
    
    public double getBgOpacity() {
        return this.opacity;
    }
    
    public void setBgOpacity(double opacity, final boolean repaint) {
        if (opacity < 0.0) {
            opacity = 0.0;
        }
        else if (opacity > 1.0) {
            opacity = 1.0;
        }
        this.opacity = opacity;
        this.opacityColor = new Color(this.color.getRed(), this.color.getGreen(), this.color.getBlue(), (int)(255.0 * opacity));
        if (repaint) {
            this.repaint();
        }
    }
    
    public Color getColor() {
        return this.color;
    }
    
    public void setColor(final Color color, final boolean repaint) {
        if (color == null) {
            throw new NullPointerException();
        }
        this.color = color;
        if (repaint) {
            this.repaint();
        }
    }
    
    @Override
    public void onResize() {
        this.setSize(this.parent.getWidth(), this.parent.getHeight());
    }
}
