// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import java.awt.Component;
import org.tlauncher.util.Reflect;
import java.awt.Container;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.configuration.LangConfiguration;

public class Localizable
{
    public static final Object[] EMPTY_VARS;
    public static final LocalizableFilter defaultFilter;
    private static LangConfiguration lang;
    
    public static void setLang(final LangConfiguration l) {
        Localizable.lang = l;
    }
    
    public static LangConfiguration get() {
        return Localizable.lang;
    }
    
    public static boolean exists() {
        return Localizable.lang != null;
    }
    
    public static String get(final String path) {
        return (Localizable.lang != null) ? Localizable.lang.get(path) : path;
    }
    
    public static String get(final String path, final Object... vars) {
        return (Localizable.lang != null) ? Localizable.lang.get(path, vars) : (path + " {" + U.toLog(vars) + "}");
    }
    
    public static String nget(final String path) {
        return (Localizable.lang != null) ? Localizable.lang.nget(path) : null;
    }
    
    public static String[] checkVariables(final Object[] check) {
        if (check == null) {
            throw new NullPointerException();
        }
        final String[] string = new String[check.length];
        for (int i = 0; i < check.length; ++i) {
            if (check[i] == null) {
                throw new NullPointerException("Variable at index " + i + " is NULL!");
            }
            string[i] = check[i].toString();
        }
        return string;
    }
    
    public static void updateContainer(final Container container, final LocalizableFilter filter) {
        for (final Component c : container.getComponents()) {
            final LocalizableComponent asLocalizable = Reflect.cast(c, LocalizableComponent.class);
            if (asLocalizable != null && filter.localize(c)) {
                asLocalizable.updateLocale();
            }
            if (c instanceof Container) {
                updateContainer((Container)c, filter);
            }
        }
    }
    
    public static void updateContainer(final Container container) {
        updateContainer(container, Localizable.defaultFilter);
    }
    
    static {
        EMPTY_VARS = new Object[0];
        defaultFilter = new LocalizableFilter() {
            @Override
            public boolean localize(final Component comp) {
                return true;
            }
        };
    }
    
    public interface LocalizableFilter
    {
        boolean localize(final Component p0);
    }
}
