// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import javax.swing.ButtonModel;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.Color;

public class UpdaterButton extends LocalizableButton
{
    protected Color unEnableColor;
    private Color backgroundColor;
    
    public UpdaterButton(final Color color, final String value) {
        this.backgroundColor = color;
        this.setText(value);
        this.setOpaque(true);
        this.setBackground(color);
    }
    
    public UpdaterButton(final Color color, final Color UnEnableColor, final String value) {
        this(color, value);
        this.unEnableColor = UnEnableColor;
    }
    
    public UpdaterButton(final Color color, final Color unEnableColor, final Color foreground, final String value) {
        this(color, unEnableColor, value);
        this.setForeground(foreground);
    }
    
    public UpdaterButton(final Color color) {
        this.backgroundColor = color;
        this.setText(null);
        this.setContentAreaFilled(false);
        this.setOpaque(true);
        this.setBackground(this.backgroundColor);
    }
    
    public UpdaterButton() {
        this.setText(null);
        this.setContentAreaFilled(false);
        this.setOpaque(true);
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        final Rectangle rec = this.getVisibleRect();
        g.setColor(this.getBackground());
        g.fillRect(rec.x, rec.y, rec.width, rec.height);
        final String text = this.getText();
        final ButtonModel buttonModel = this.getModel();
        final Color colorText = this.getForeground();
        if (buttonModel.isRollover() && this.unEnableColor != null && this.model.isEnabled()) {
            g.setColor(this.unEnableColor);
        }
        g.fillRect(rec.x, rec.y, rec.width, rec.height);
        g.setColor(colorText);
        final JComponent component = this;
        if (text != null) {
            this.paintText(g, component, rec, text);
        }
    }
    
    protected void paintText(final Graphics g, final JComponent c, final Rectangle textRect, final String text) {
        final Graphics2D g2d = (Graphics2D)g;
        g2d.setFont(this.getFont());
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        final FontMetrics fm = g2d.getFontMetrics();
        final Rectangle2D r = fm.getStringBounds(text, g2d);
        final int x = (this.getWidth() - (int)r.getWidth()) / 2;
        final int y = (this.getHeight() - (int)r.getHeight()) / 2 + fm.getAscent();
        g2d.drawString(text, x, y);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    }
    
    public Color getBackgroundColor() {
        return this.backgroundColor;
    }
}
