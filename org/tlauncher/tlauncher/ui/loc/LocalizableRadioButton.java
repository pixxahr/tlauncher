// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.event.ItemListener;
import javax.swing.JRadioButton;

public class LocalizableRadioButton extends JRadioButton implements LocalizableComponent
{
    private static final long serialVersionUID = 1L;
    private String path;
    
    public LocalizableRadioButton() {
        this.init();
    }
    
    public LocalizableRadioButton(final String path) {
        this.init();
        this.setLabel(path);
    }
    
    @Deprecated
    @Override
    public void setLabel(final String path) {
        this.setText(path);
    }
    
    @Override
    public void setText(final String path) {
        this.path = path;
        super.setText((Localizable.get() == null) ? path : Localizable.get().get(path));
    }
    
    public String getLangPath() {
        return this.path;
    }
    
    public void addListener(final ItemListener l) {
        super.getModel().addItemListener(l);
    }
    
    public void removeListener(final ItemListener l) {
        super.getModel().removeItemListener(l);
    }
    
    @Override
    public void updateLocale() {
        this.setLabel(this.path);
    }
    
    private void init() {
        this.setOpaque(false);
        this.setIcon(ImageCache.getNativeIcon("radio-button-off.png"));
        this.setSelectedIcon(ImageCache.getNativeIcon("radio-button-on.png"));
        this.setDisabledIcon(ImageCache.getNativeIcon("radio-button-off.png"));
        this.setDisabledSelectedIcon(ImageCache.getNativeIcon("radio-button-on.png"));
        this.setPressedIcon(ImageCache.getNativeIcon("radio-button-on.png"));
    }
}
