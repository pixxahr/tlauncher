// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import javax.swing.JComponent;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Color;

public class RoundUpdaterButton extends UpdaterButton
{
    public static Color TEXT_COLOR;
    int ARC_SIZE;
    int i;
    
    public RoundUpdaterButton(final Color colorText, final Color background, final Color mouseUnder, final String value) {
        super(background, value);
        this.ARC_SIZE = 10;
        this.i = 0;
        this.setOpaque(false);
        RoundUpdaterButton.TEXT_COLOR = colorText;
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                RoundUpdaterButton.this.setBackground(mouseUnder);
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                RoundUpdaterButton.this.setBackground(background);
            }
        });
    }
    
    @Override
    protected void paintComponent(final Graphics g0) {
        final int x = 0;
        final Graphics2D g = (Graphics2D)g0;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(this.getBackground());
        g.fillRoundRect(x, x, this.getWidth(), this.getHeight(), this.ARC_SIZE, this.ARC_SIZE);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g0.setColor(RoundUpdaterButton.TEXT_COLOR);
        this.paintText(g0, this, this.getVisibleRect(), this.getText());
    }
}
