// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import org.tlauncher.tlauncher.ui.TLauncherFrame;
import org.tlauncher.tlauncher.ui.center.CenterPanel;
import org.tlauncher.tlauncher.ui.text.ExtendedTextField;

public class LocalizableTextField extends ExtendedTextField implements LocalizableComponent
{
    private static final long serialVersionUID = 359096767189321072L;
    protected String placeholderPath;
    protected String[] variables;
    
    public LocalizableTextField(final CenterPanel panel, final String placeholderPath, final String value) {
        super(panel, null, value);
        this.setValue(value);
        this.setPlaceholder(placeholderPath);
        this.setFont(this.getFont().deriveFont(TLauncherFrame.fontSize));
    }
    
    public LocalizableTextField(final CenterPanel panel, final String placeholderPath) {
        this(panel, placeholderPath, null);
    }
    
    public LocalizableTextField(final String placeholderPath) {
        this(null, placeholderPath, null);
    }
    
    public LocalizableTextField() {
        this(null, null, null);
    }
    
    public void setPlaceholder(final String placeholderPath, final Object... vars) {
        this.placeholderPath = placeholderPath;
        this.variables = Localizable.checkVariables(vars);
        String value = Localizable.get(placeholderPath);
        for (int i = 0; i < this.variables.length; ++i) {
            value = value.replace("%" + i, this.variables[i]);
        }
        super.setPlaceholder(value);
    }
    
    public void setPlaceholder(final String placeholderPath) {
        this.setPlaceholder(placeholderPath, Localizable.EMPTY_VARS);
    }
    
    public String getPlaceholderPath() {
        return this.placeholderPath;
    }
    
    @Override
    public void updateLocale() {
        this.setPlaceholder(this.placeholderPath, (Object[])this.variables);
    }
}
