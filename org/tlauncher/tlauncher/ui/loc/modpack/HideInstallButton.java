// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc.modpack;

import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.UpdaterFullButton;

public abstract class HideInstallButton extends UpdaterFullButton
{
    public static final Color MOUSE_UNDER;
    public static final Color DEFAULT_BACKGROUND;
    protected ModpackComboBox localmodpacks;
    protected GameType type;
    protected GameEntityDTO entity;
    
    public HideInstallButton(final String text, final String image, final ModpackComboBox localmodpacks, final GameType type, final GameEntityDTO entity) {
        super(HideInstallButton.DEFAULT_BACKGROUND, text, image);
        this.localmodpacks = localmodpacks;
        this.type = type;
        this.entity = entity;
    }
    
    public HideInstallButton(final ModpackComboBox localmodpacks, final GameType type, final GameEntityDTO entity, final String text, final boolean remote, final String image) {
        super(HideInstallButton.DEFAULT_BACKGROUND, "loginform.enter.install", image);
        this.localmodpacks = localmodpacks;
        this.type = type;
        this.entity = entity;
    }
    
    public abstract void init();
    
    static {
        MOUSE_UNDER = new Color(95, 198, 255);
        DEFAULT_BACKGROUND = new Color(63, 186, 255);
    }
}
