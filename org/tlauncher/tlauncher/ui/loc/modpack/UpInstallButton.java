// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc.modpack;

import org.tlauncher.modpack.domain.client.ModpackDTO;
import java.util.Iterator;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import java.util.List;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Color;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.modpack.filter.Filter;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.tlauncher.ui.modpack.filter.ModpackSceneFilter;

public class UpInstallButton extends HideInstallButton
{
    private ModpackSceneFilter filter;
    private ModpackManager manager;
    
    public UpInstallButton(final GameEntityDTO entity, final GameType type, final ModpackComboBox localmodpacks) {
        super(Localizable.get("loginform.enter.install").toUpperCase(), "up-install.png", localmodpacks, type, entity);
        this.filter = new ModpackSceneFilter(localmodpacks, type, (Filter<GameEntityDTO>[])new Filter[0]);
        this.manager = (ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class);
        this.setBackground(UpInstallButton.DEFAULT_BACKGROUND);
        this.setHorizontalTextPosition(4);
        this.setForeground(Color.WHITE);
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                UpInstallButton.this.setBackground(HideInstallButton.MOUSE_UNDER);
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                UpInstallButton.this.setBackground(HideInstallButton.DEFAULT_BACKGROUND);
            }
            
            @Override
            public void mousePressed(final MouseEvent e) {
                UpInstallButton.this.setVisible(false);
                final BaseModpackFilter<VersionDTO> filter = BaseModpackFilter.getBaseModpackStandardFilters(entity, type, localmodpacks);
                UpInstallButton.this.manager.installEntity(entity, (VersionDTO)ModpackUtil.sortByDate(filter.findAll(entity.getVersions())).get(0), type, true);
            }
        });
        this.init();
    }
    
    @Override
    public void init() {
        switch (this.type) {
            case MODPACK: {
                this.modpackInit();
                break;
            }
            default: {
                if (this.localmodpacks.getSelectedIndex() > 0) {
                    for (final GameEntityDTO m : ((ModpackVersionDTO)this.localmodpacks.getSelectedValue().getModpack().getVersion()).getByType(this.type)) {
                        this.setVisible(false);
                        if (this.entity.getId().equals(m.getId())) {
                            return;
                        }
                    }
                    this.setVisible(true);
                    break;
                }
                break;
            }
        }
    }
    
    private void modpackInit() {
        final List<ModpackDTO> list = this.localmodpacks.getModpacks();
        final ModpackDTO modpackDTO = (ModpackDTO)this.entity;
        for (final ModpackDTO m : list) {
            if (modpackDTO.getId().equals(m.getId())) {
                this.setVisible(false);
                return;
            }
        }
        this.setVisible(true);
    }
    
    public void installEntity(final GameEntityDTO e, final GameType type) {
        if (this.entity.getId().equals(e.getId())) {
            this.setVisible(false);
        }
    }
    
    public void removeEntity(final GameEntityDTO e) {
        this.init();
    }
}
