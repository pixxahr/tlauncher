// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc.modpack;

import org.tlauncher.tlauncher.ui.listener.BlockClickListener;
import java.awt.event.MouseListener;
import java.util.Iterator;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import java.util.List;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.managers.ModpackManager;
import javax.swing.JComponent;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;

public abstract class GameRightButton extends ModpackActButton
{
    public GameRightButton(final GameEntityDTO entity, final GameType type, final ModpackComboBox localmodpacks) {
        super(entity, type, localmodpacks);
        this.installButton = new Button("loginform.enter.install");
        this.removeButton = new Button("modpack.remove.button");
        this.processButton = new Button("modpack.process.button");
        this.add(this.installButton, "INSTALL");
        this.add(this.removeButton, "REMOVE");
        this.add(this.processButton, "PROCESSING");
        SwingUtil.changeFontFamily(this.installButton, FontTL.ROBOTO_REGULAR, 12);
        SwingUtil.changeFontFamily(this.removeButton, FontTL.ROBOTO_REGULAR, 12);
        SwingUtil.changeFontFamily(this.processButton, FontTL.ROBOTO_REGULAR, 12);
        this.initButton();
        final ModpackManager manager = (ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class);
        this.removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                manager.removeEntity(GameRightButton.this.getEntity(), entity.getVersion(), GameRightButton.this.getType());
            }
        });
        this.installButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final BaseModpackFilter<VersionDTO> filter = BaseModpackFilter.getBaseModpackStandardFilters(entity, type, localmodpacks);
                manager.installEntity(GameRightButton.this.entity, (VersionDTO)ModpackUtil.sortByDate(filter.findAll(entity.getVersions())).get(0), GameRightButton.this.getType(), true);
            }
        });
    }
    
    @Override
    public void initButton() {
        if (this.type == GameType.MODPACK) {
            for (final GameEntityDTO e : this.getSelectedModpackData()) {
                if (this.entity.getId().equals(e.getId()) && this.type == GameType.MODPACK) {
                    this.setVisible(false);
                    return;
                }
            }
            this.setVisible(true);
        }
        for (final GameEntityDTO e : this.getSelectedModpackData()) {
            if (this.entity.getId().equals(e.getId())) {
                if (this.type == GameType.MODPACK) {
                    this.setVisible(false);
                    return;
                }
                this.setTypeButton("REMOVE");
                return;
            }
        }
        this.setTypeButton("INSTALL");
    }
    
    @Override
    public synchronized void addMouseListener(final MouseListener l) {
        if (l instanceof BlockClickListener) {
            return;
        }
        super.addMouseListener(l);
    }
    
    public abstract void updateRow();
}
