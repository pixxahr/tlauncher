// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc.modpack;

import javax.swing.ButtonModel;
import org.tlauncher.util.SwingUtil;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.loc.UpdaterFullButton;

public class ShadowButton extends UpdaterFullButton
{
    public ShadowButton(final Color color, final Color mouseUnder, final String value, final String image) {
        super(color, mouseUnder, value, image);
    }
    
    @Override
    protected void paintBackground(final Rectangle rec, final Graphics g) {
        final ButtonModel buttonModel = this.getModel();
        if (buttonModel.isRollover()) {
            g.setColor(this.unEnableColor);
        }
        else {
            g.setColor(this.backgroundColor);
        }
        SwingUtil.paintShadowLine(rec, g, this.getBackground().getRed(), 12);
    }
}
