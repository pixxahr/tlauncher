// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc.modpack;

import java.util.Iterator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.version.VersionDTO;

public class ModpackTableInstallButton extends TableActButton
{
    private VersionDTO selectedVersion;
    
    public ModpackTableInstallButton(final GameEntityDTO entity, final GameType type, final ModpackComboBox localmodpacks) {
        super(entity, type, localmodpacks);
        this.filter = BaseModpackFilter.getBaseModpackStandardFilters(entity, type, localmodpacks);
        this.installButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ModpackTableInstallButton.this.manager.installEntity(ModpackTableInstallButton.this.getEntity(), ModpackTableInstallButton.this.selectedVersion, type, true);
            }
        });
    }
    
    @Override
    public void initButton() {
        for (final GameEntityDTO e : this.getSelectedModpackData()) {
            if (this.entity.getId().equals(e.getId())) {
                this.selectedVersion = e.getVersion();
                this.setTypeButton("REMOVE");
                return;
            }
        }
        for (final VersionDTO versionDTO : this.entity.getVersions()) {
            if (this.filter.isProper(versionDTO)) {
                this.selectedVersion = versionDTO;
            }
        }
        if (this.selectedVersion == null) {
            this.setTypeButton("DENIED");
            return;
        }
        this.setTypeButton("INSTALL");
    }
}
