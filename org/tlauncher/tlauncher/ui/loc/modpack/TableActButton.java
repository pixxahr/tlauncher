// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc.modpack;

import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import java.awt.Color;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import javax.swing.JButton;

public abstract class TableActButton extends ModpackActButton
{
    protected JButton deniedButton;
    protected BaseModpackFilter<VersionDTO> filter;
    protected ModpackManager manager;
    
    public TableActButton(final GameEntityDTO entity, final GameType type, final ModpackComboBox localmodpacks) {
        super(entity, type, localmodpacks);
        this.manager = (ModpackManager)TLauncher.getInjector().getInstance((Class)ModpackManager.class);
        final Color color = new Color(0, 0, 0, 0);
        this.installButton = new ImageUdaterButton(color, "install-game-element.png");
        this.removeButton = new ImageUdaterButton(color, "delete-game-element.png");
        this.processButton = new ImageUdaterButton(color, "refresh.png");
        this.deniedButton = new ImageUdaterButton(color, "modpack-element-denied.png");
        this.add(this.installButton, "INSTALL");
        this.add(this.removeButton, "REMOVE");
        this.add(this.processButton, "PROCESSING");
        this.add(this.deniedButton, "DENIED");
        this.deniedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                Alert.showLocMessageWithoutTitle("modpack.table.denied.button");
            }
        });
    }
    
    @Override
    public abstract void initButton();
}
