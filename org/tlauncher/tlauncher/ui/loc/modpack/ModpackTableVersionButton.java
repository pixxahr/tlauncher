// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc.modpack;

import java.util.Iterator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.modpack.filter.BaseModpackFilter;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.version.VersionDTO;

public class ModpackTableVersionButton extends TableActButton
{
    private VersionDTO versionDTO;
    
    public ModpackTableVersionButton(final GameEntityDTO entity, final GameType type, final ModpackComboBox localmodpacks, final VersionDTO versionDTO, final BaseModpackFilter<VersionDTO> filter) {
        super(entity, type, localmodpacks);
        this.filter = filter;
        this.versionDTO = versionDTO;
        this.initButton();
        this.installButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ModpackTableVersionButton.this.manager.installEntity(entity, versionDTO, type, true);
            }
        });
        this.removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ModpackTableVersionButton.this.manager.removeEntity(entity, versionDTO, type, false);
            }
        });
    }
    
    @Override
    public void initButton() {
        if (this.findLocal() != null) {
            this.setTypeButton("REMOVE");
            return;
        }
        if (this.type == GameType.MODPACK) {
            this.setTypeButton("INSTALL");
            return;
        }
        if (this.filter.isProper(this.versionDTO)) {
            this.setTypeButton("INSTALL");
        }
        else {
            this.setTypeButton("DENIED");
        }
    }
    
    private GameEntityDTO findLocal() {
        for (final GameEntityDTO e : this.getSelectedModpackData()) {
            if (this.entity.getId().equals(e.getId()) && e.getVersion().getId().equals(this.versionDTO.getId())) {
                return e;
            }
        }
        return null;
    }
}
