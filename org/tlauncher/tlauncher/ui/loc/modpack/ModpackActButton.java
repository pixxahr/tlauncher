// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc.modpack;

import java.awt.Container;
import java.util.ArrayList;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import java.util.List;
import java.awt.LayoutManager;
import java.awt.CardLayout;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.tlauncher.ui.swing.box.ModpackComboBox;
import javax.swing.JButton;
import org.tlauncher.tlauncher.ui.listener.BlockClickListener;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public abstract class ModpackActButton extends ExtendedPanel implements BlockClickListener
{
    public static final String INSTALL = "INSTALL";
    public static final String REMOVE = "REMOVE";
    public static final String PROCESSING = "PROCESSING";
    public static final String DENIED_OPERATION = "DENIED";
    protected JButton installButton;
    protected JButton removeButton;
    protected JButton processButton;
    protected String last;
    protected ModpackComboBox localmodpacks;
    protected GameType type;
    protected GameEntityDTO entity;
    
    public ModpackActButton(final GameEntityDTO entity, final GameType type, final ModpackComboBox localmodpacks) {
        this.localmodpacks = localmodpacks;
        this.type = type;
        this.entity = entity;
        this.setLayout(new CardLayout(0, 0));
    }
    
    protected List<? extends GameEntityDTO> getSelectedModpackData() {
        if (this.localmodpacks.getSelectedIndex() > 0 && this.type != GameType.MODPACK) {
            return ((ModpackVersionDTO)this.localmodpacks.getSelectedValue().getModpack().getVersion()).getByType(this.type);
        }
        if (this.type == GameType.MODPACK) {
            return this.localmodpacks.getModpacks();
        }
        return new ArrayList<GameEntityDTO>();
    }
    
    public void setTypeButton(final String name) {
        if (!name.equals("PROCESSING")) {
            this.last = name;
        }
        ((CardLayout)this.getLayout()).show(this, name);
    }
    
    public abstract void initButton();
    
    public void reset() {
        this.setTypeButton(this.last);
    }
    
    public GameEntityDTO getEntity() {
        return this.entity;
    }
    
    public GameType getType() {
        return this.type;
    }
}
