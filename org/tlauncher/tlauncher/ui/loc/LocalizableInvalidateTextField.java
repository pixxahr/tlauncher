// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import org.tlauncher.tlauncher.ui.center.CenterPanel;
import org.tlauncher.tlauncher.ui.text.InvalidateTextField;

public class LocalizableInvalidateTextField extends InvalidateTextField implements LocalizableComponent
{
    private static final long serialVersionUID = -3999545292427982797L;
    private String placeholderPath;
    
    private LocalizableInvalidateTextField(final CenterPanel panel, final String placeholderPath, final String value) {
        super(panel, null, value);
        this.placeholderPath = placeholderPath;
        this.setValue(value);
    }
    
    protected LocalizableInvalidateTextField(final String placeholderPath) {
        this(null, placeholderPath, null);
    }
    
    public void setPlaceholder(final String placeholderPath) {
        this.placeholderPath = placeholderPath;
        super.setPlaceholder((Localizable.get() == null) ? placeholderPath : Localizable.get().get(placeholderPath));
    }
    
    public String getPlaceholderPath() {
        return this.placeholderPath;
    }
    
    @Override
    public void updateLocale() {
        this.setPlaceholder(this.placeholderPath);
    }
}
