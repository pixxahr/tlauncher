// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import org.tlauncher.tlauncher.ui.swing.TransparentButton;

public class LocalizableTransparentButton extends TransparentButton implements LocalizableComponent
{
    private static final long serialVersionUID = -1357535949476677157L;
    private String path;
    private String[] variables;
    
    public LocalizableTransparentButton(final String path, final Object... vars) {
        this.setOpaque(false);
        this.setText(path, vars);
    }
    
    void setText(final String path, final Object... vars) {
        this.path = path;
        this.variables = Localizable.checkVariables(vars);
        String value = Localizable.get(path);
        for (int i = 0; i < this.variables.length; ++i) {
            value = value.replace("%" + i, this.variables[i]);
        }
        super.setText(value);
    }
    
    @Override
    public void setText(final String path) {
        this.setText(path, Localizable.EMPTY_VARS);
    }
    
    @Override
    public void updateLocale() {
        this.setText(this.path, (Object[])this.variables);
    }
}
