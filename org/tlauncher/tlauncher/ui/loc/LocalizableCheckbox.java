// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import javax.swing.Icon;
import org.tlauncher.tlauncher.ui.TLauncherFrame;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.event.ItemListener;
import javax.swing.JCheckBox;

public class LocalizableCheckbox extends JCheckBox implements LocalizableComponent
{
    private static final long serialVersionUID = 1L;
    private String path;
    
    public LocalizableCheckbox(final String path) {
        this.init(PANEL_TYPE.LOGIN);
        this.setLabel(path);
    }
    
    public LocalizableCheckbox(final String path, final boolean state) {
        super("", state);
        this.init(PANEL_TYPE.LOGIN);
        this.setText(path);
    }
    
    public LocalizableCheckbox(final String path2, final PANEL_TYPE settings) {
        this.setText(path2);
        this.init(settings);
    }
    
    @Deprecated
    @Override
    public void setLabel(final String path) {
        this.setText(path);
    }
    
    @Override
    public void setText(final String path) {
        this.path = path;
        super.setText((Localizable.get() == null) ? path : Localizable.get().get(path));
    }
    
    public String getLangPath() {
        return this.path;
    }
    
    public boolean getState() {
        return super.getModel().isSelected();
    }
    
    public void setState(final boolean state) {
        super.getModel().setSelected(state);
    }
    
    public void addListener(final ItemListener l) {
        super.getModel().addItemListener(l);
    }
    
    public void removeListener(final ItemListener l) {
        super.getModel().removeItemListener(l);
    }
    
    @Override
    public void updateLocale() {
        this.setLabel(this.path);
    }
    
    protected void init(final PANEL_TYPE panel) {
        Icon off = null;
        Icon on = null;
        switch (panel) {
            case SETTINGS: {
                on = ImageCache.getNativeIcon("settings-check-box-on.png");
                off = ImageCache.getNativeIcon("settings-check-box-off.png");
                break;
            }
            case LOGIN: {
                on = ImageCache.getNativeIcon("checkbox-on.png");
                off = ImageCache.getNativeIcon("checkbox-off.png");
                break;
            }
        }
        this.setFont(this.getFont().deriveFont(TLauncherFrame.fontSize));
        this.setOpaque(false);
        this.setIcon(off);
        this.setSelectedIcon(on);
        this.setDisabledIcon(off);
        this.setDisabledSelectedIcon(on);
        this.setPressedIcon(on);
    }
    
    public enum PANEL_TYPE
    {
        SETTINGS, 
        LOGIN;
    }
}
