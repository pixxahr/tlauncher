// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JMenuItem;

public class LocalizableMenuItem extends JMenuItem implements LocalizableComponent
{
    private static final long serialVersionUID = 1364363532569997394L;
    private static List<LocalizableMenuItem> items;
    private String path;
    private String[] variables;
    
    public LocalizableMenuItem(final String path, final Object... vars) {
        LocalizableMenuItem.items.add(this);
        this.setText(path, vars);
    }
    
    public LocalizableMenuItem(final String path) {
        this(path, Localizable.EMPTY_VARS);
    }
    
    public void setText(final String path, final Object... vars) {
        this.path = path;
        this.variables = Localizable.checkVariables(vars);
        String value = Localizable.get(path);
        for (int i = 0; i < this.variables.length; ++i) {
            value = value.replace("%" + i, this.variables[i]);
        }
        super.setText(value);
    }
    
    @Override
    public void setText(final String path) {
        this.setText(path, Localizable.EMPTY_VARS);
    }
    
    public void setVariables(final Object... vars) {
        this.setText(this.path, vars);
    }
    
    @Override
    public void updateLocale() {
        this.setText(this.path, (Object[])this.variables);
    }
    
    public static void updateLocales() {
        for (final LocalizableMenuItem item : LocalizableMenuItem.items) {
            if (item == null) {
                continue;
            }
            item.updateLocale();
        }
    }
    
    static {
        LocalizableMenuItem.items = Collections.synchronizedList(new ArrayList<LocalizableMenuItem>());
    }
}
