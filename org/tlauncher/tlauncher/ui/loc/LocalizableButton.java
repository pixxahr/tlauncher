// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import org.tlauncher.tlauncher.ui.swing.extended.ExtendedButton;

public class LocalizableButton extends ExtendedButton implements LocalizableComponent
{
    private static final long serialVersionUID = 1073130908385613323L;
    private String path;
    private String[] variables;
    
    protected LocalizableButton() {
    }
    
    public LocalizableButton(final String path) {
        this();
        this.setText(path);
    }
    
    public LocalizableButton(final String path, final Object... vars) {
        this();
        this.setText(path, vars);
    }
    
    public void setText(final String path, final Object... vars) {
        this.path = path;
        this.variables = Localizable.checkVariables(vars);
        String value = Localizable.get(path);
        for (int i = 0; i < this.variables.length; ++i) {
            value = value.replace("%" + i, this.variables[i]);
        }
        super.setText(value);
    }
    
    @Override
    public void setText(final String path) {
        this.setText(path, Localizable.EMPTY_VARS);
    }
    
    @Override
    public void updateLocale() {
        this.setText(this.path, (Object[])this.variables);
    }
}
