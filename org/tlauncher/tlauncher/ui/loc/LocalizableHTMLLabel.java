// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

public class LocalizableHTMLLabel extends LocalizableLabel
{
    public LocalizableHTMLLabel(final String path, final Object... vars) {
        super(path, vars);
    }
    
    public LocalizableHTMLLabel(final String path) {
        this(path, Localizable.EMPTY_VARS);
    }
    
    public LocalizableHTMLLabel() {
        this((String)null);
    }
    
    public LocalizableHTMLLabel(final int horizontalAlignment) {
        this((String)null);
        this.setHorizontalAlignment(horizontalAlignment);
    }
    
    @Override
    public void setText(final String path, final Object... vars) {
        this.path = path;
        this.variables = Localizable.checkVariables(vars);
        String value = Localizable.get(path);
        if (value != null) {
            value = "<html>" + value.replace("\n", "<br/>") + "</html>";
            for (int i = 0; i < this.variables.length; ++i) {
                value = value.replace("%" + i, this.variables[i]);
            }
        }
        this.setRawText(value);
    }
}
