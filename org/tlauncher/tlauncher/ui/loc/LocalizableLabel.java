// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import org.tlauncher.tlauncher.ui.TLauncherFrame;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedLabel;

public class LocalizableLabel extends ExtendedLabel implements LocalizableComponent
{
    private static final long serialVersionUID = 7628068160047735335L;
    protected String path;
    protected String[] variables;
    
    public LocalizableLabel(final String path, final Object... vars) {
        this.setText(path, vars);
        this.setFont(this.getFont().deriveFont(TLauncherFrame.fontSize));
    }
    
    public LocalizableLabel(final String path) {
        this(path, Localizable.EMPTY_VARS);
    }
    
    public LocalizableLabel() {
        this((String)null);
    }
    
    public LocalizableLabel(final int horizontalAlignment) {
        this((String)null);
        this.setHorizontalAlignment(horizontalAlignment);
    }
    
    public void setText(final String path, final Object... vars) {
        this.path = path;
        this.variables = Localizable.checkVariables(vars);
        String value = Localizable.get(path);
        for (int i = 0; i < this.variables.length; ++i) {
            value = value.replace("%" + i, this.variables[i]);
        }
        this.setRawText(value);
    }
    
    protected void setRawText(final String value) {
        super.setText(value);
    }
    
    @Override
    public void setText(final String path) {
        this.setText(path, Localizable.EMPTY_VARS);
    }
    
    @Override
    public void updateLocale() {
        this.setText(this.path, (Object[])this.variables);
    }
}
