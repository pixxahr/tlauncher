// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import javax.swing.ButtonModel;
import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.image.ImageObserver;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.awt.Image;
import java.awt.Color;

public class UpdaterFullButton extends UpdaterButton
{
    protected Color unEnableColor;
    protected Color backgroundColor;
    private Image image;
    private Image imageUp;
    
    public UpdaterFullButton(final Color color, final String value, final String image) {
        super(color, value);
        this.backgroundColor = color;
        this.image = ImageCache.getImage(image);
        this.setForeground(Color.WHITE);
        this.setHorizontalTextPosition(4);
    }
    
    public UpdaterFullButton(final Color color, final Color mouseUnder, final String value, final String image) {
        super(color, value);
        this.setHorizontalTextPosition(4);
        this.backgroundColor = color;
        this.image = ImageCache.getImage(image);
        this.setForeground(Color.WHITE);
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                UpdaterFullButton.this.setBackground(mouseUnder);
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                UpdaterFullButton.this.setBackground(UpdaterFullButton.this.backgroundColor);
            }
        });
    }
    
    public UpdaterFullButton(final Color color, final Color mouseUnder, final String value, final String image, final String imageUp) {
        super(color, value);
        this.setHorizontalTextPosition(4);
        this.backgroundColor = color;
        this.image = ImageCache.getImage(image);
        this.imageUp = ImageCache.getImage(imageUp);
        this.setForeground(Color.WHITE);
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                UpdaterFullButton.this.setBackground(mouseUnder);
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                UpdaterFullButton.this.setBackground(UpdaterFullButton.this.backgroundColor);
            }
        });
    }
    
    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        final Rectangle rec = this.getVisibleRect();
        final String text = this.getText();
        final JComponent component = this;
        this.paintBackground(rec, g);
        this.paintText(g, component, rec, text);
        this.paintPicture(g, component, rec, this.getModel().isRollover());
    }
    
    protected void paintPicture(final Graphics g, final JComponent c, final Rectangle rect, final boolean rollover) {
        if (this.image == null) {
            return;
        }
        if (this.getHorizontalTextPosition() == 4) {
            final Graphics2D g2d = (Graphics2D)g;
            final int x = this.getInsets().left;
            final int y = (this.getHeight() - this.image.getHeight(null)) / 2;
            if (rollover && this.imageUp != null) {
                g2d.drawImage(this.imageUp, x, y, null);
            }
            else {
                g2d.drawImage(this.image, x, y, null);
            }
        }
    }
    
    @Override
    protected void paintText(final Graphics g, final JComponent c, final Rectangle textRect, final String text) {
        if (text == null) {
            return;
        }
        if (this.getHorizontalTextPosition() == 4) {
            g.setColor(this.getForeground());
            final Graphics2D g2d = (Graphics2D)g;
            g2d.setFont(this.getFont());
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            final FontMetrics fm = g2d.getFontMetrics();
            final Rectangle2D r = fm.getStringBounds(text, g2d);
            final int x = this.getInsets().left + this.image.getWidth(null) + this.getIconTextGap();
            final int y = (this.getHeight() - (int)r.getHeight()) / 2 + fm.getAscent() - 1;
            g2d.drawString(text, x, y);
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        }
    }
    
    protected void paintBackground(final Rectangle rec, final Graphics g) {
        final ButtonModel buttonModel = this.getModel();
        g.setColor(this.getBackground());
        if (buttonModel.isPressed()) {
            g.setColor(this.getBackground());
            g.fillRect(rec.x, rec.y, rec.width, rec.height);
        }
        else if (!buttonModel.isEnabled()) {
            if (this.unEnableColor == null) {
                g.setColor(this.getForeground().darker());
            }
            else {
                g.setColor(this.unEnableColor);
            }
        }
        else if (buttonModel.isRollover()) {
            g.setColor(this.unEnableColor);
        }
        g.fillRect(rec.x, rec.y, rec.width, rec.height);
    }
}
