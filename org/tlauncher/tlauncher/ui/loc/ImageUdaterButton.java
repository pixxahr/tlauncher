// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import javax.swing.ButtonModel;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.ImageButton;

public class ImageUdaterButton extends ImageButton
{
    public final Color backroundColor;
    public final String defaultImage;
    private Color modelPressedColor;
    
    public ImageUdaterButton(final Color color, final String image) {
        super(image);
        this.defaultImage = image;
        this.setBackground(this.backroundColor = color);
    }
    
    public ImageUdaterButton(final Color color) {
        this.backroundColor = color;
        this.defaultImage = null;
        this.setContentAreaFilled(false);
        this.setOpaque(true);
        this.setBackground(color);
    }
    
    public ImageUdaterButton(final String image) {
        super(image);
        this.defaultImage = image;
        this.backroundColor = Color.BLACK;
    }
    
    public ImageUdaterButton(final Color color, final Color color1, final String s, final String s1) {
        this(color, s);
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                if (ImageUdaterButton.this.getModel().isEnabled()) {
                    ImageButton.this.setImage(ImageButton.loadImage(s1));
                    JComponent.this.setBackground(color1);
                }
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                ImageButton.this.setImage(ImageButton.loadImage(s));
                JComponent.this.setBackground(color);
            }
        });
    }
    
    public ImageUdaterButton(final Color color, final Color color1, final String s) {
        this(color, s);
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(final MouseEvent e) {
                if (ImageUdaterButton.this.getModel().isEnabled()) {
                    JComponent.this.setBackground(color1);
                }
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                JComponent.this.setBackground(color);
            }
        });
    }
    
    @Override
    public void paintComponent(final Graphics g) {
        final Rectangle rec = this.getVisibleRect();
        g.setColor(this.getBackground());
        final ButtonModel buttonModel = this.getModel();
        if (buttonModel.isPressed() && buttonModel.isEnabled() && this.modelPressedColor != null) {
            g.setColor(this.modelPressedColor);
        }
        g.fillRect(rec.x, rec.y, rec.width, rec.height);
        final JComponent component = this;
        if (this.image != null) {
            this.paintPicture(g, component, rec);
        }
    }
    
    protected void paintPicture(final Graphics g, final JComponent c, final Rectangle rect) {
        final Graphics2D g2d = (Graphics2D)g;
        final int x = (this.getWidth() - this.image.getWidth(null)) / 2;
        final int y = (this.getHeight() - this.image.getHeight(null)) / 2;
        g2d.drawImage(this.image, x, y, null);
    }
    
    public String getDefaultImage() {
        return this.defaultImage;
    }
    
    public Color getBackroundColor() {
        return this.backroundColor;
    }
    
    public void setModelPressedColor(final Color modelPressedColor) {
        this.modelPressedColor = modelPressedColor;
    }
}
