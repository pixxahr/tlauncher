// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import org.tlauncher.tlauncher.ui.center.CenterPanel;
import org.tlauncher.tlauncher.ui.text.CheckableTextField;

public abstract class LocalizableCheckableTextField extends CheckableTextField implements LocalizableComponent
{
    private static final long serialVersionUID = 1L;
    private String placeholderPath;
    
    private LocalizableCheckableTextField(final CenterPanel panel, final String placeholderPath, final String value) {
        super(panel, null, null);
        this.placeholderPath = placeholderPath;
        this.setValue(value);
    }
    
    public LocalizableCheckableTextField(final CenterPanel panel, final String placeholderPath) {
        this(panel, placeholderPath, null);
    }
    
    public LocalizableCheckableTextField(final String placeholderPath, final String value) {
        this(null, placeholderPath, value);
    }
    
    public LocalizableCheckableTextField(final String placeholderPath) {
        this(null, placeholderPath, null);
    }
    
    public void setPlaceholder(final String placeholderPath) {
        this.placeholderPath = placeholderPath;
        super.setPlaceholder((Localizable.get() == null) ? placeholderPath : Localizable.get().get(placeholderPath));
    }
    
    public String getPlaceholderPath() {
        return this.placeholderPath;
    }
    
    @Override
    public void updateLocale() {
        this.setPlaceholder(this.placeholderPath);
    }
}
