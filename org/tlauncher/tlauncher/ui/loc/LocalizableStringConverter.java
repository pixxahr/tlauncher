// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.loc;

import org.tlauncher.tlauncher.ui.converter.StringConverter;

public abstract class LocalizableStringConverter<T> implements StringConverter<T>
{
    private final String prefix;
    
    public LocalizableStringConverter(final String prefix) {
        this.prefix = prefix;
    }
    
    @Override
    public String toString(final T from) {
        return Localizable.get(this.getPath(from));
    }
    
    String getPath(final T from) {
        final String prefix = this.getPrefix();
        if (prefix == null || prefix.isEmpty()) {
            return this.toPath(from);
        }
        final String path = this.toPath(from);
        return prefix + "." + path;
    }
    
    String getPrefix() {
        return this.prefix;
    }
    
    protected abstract String toPath(final T p0);
}
