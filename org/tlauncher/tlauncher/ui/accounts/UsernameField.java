// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.accounts;

import org.tlauncher.tlauncher.ui.center.CenterPanel;
import org.tlauncher.tlauncher.ui.loc.LocalizableTextField;

public class UsernameField extends LocalizableTextField
{
    private static final long serialVersionUID = -5813187607562947592L;
    private UsernameState state;
    String username;
    
    public UsernameField(final CenterPanel pan, final UsernameState state) {
        super(pan, "account.username");
        this.setState(state);
    }
    
    public UsernameState getState() {
        return this.state;
    }
    
    public void setState(final UsernameState state) {
        if (state == null) {
            throw new NullPointerException();
        }
        this.state = state;
        this.setPlaceholder(state.placeholder);
    }
    
    public enum UsernameState
    {
        USERNAME("account.username"), 
        EMAIL_LOGIN("account.email"), 
        EMAIL("account.email.restrict");
        
        private final String placeholder;
        
        private UsernameState(final String placeholder) {
            this.placeholder = placeholder;
        }
        
        public String getPlaceholder() {
            return this.placeholder;
        }
    }
}
