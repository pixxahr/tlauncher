// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.accounts;

import java.awt.event.ItemListener;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.swing.CheckBoxListener;
import javax.swing.AbstractButton;
import org.tlauncher.tlauncher.ui.loc.LocalizableRadioButton;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.text.ExtendedPasswordField;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JComponent;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.SpringLayout;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.alert.Alert;
import javax.swing.SwingUtilities;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.BorderFactory;
import javax.swing.border.EmptyBorder;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.Color;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.editor.EditorCheckBox;
import org.tlauncher.tlauncher.ui.swing.FlexibleEditorPanel;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import java.util.LinkedHashMap;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import javax.swing.ButtonGroup;
import org.tlauncher.tlauncher.ui.scenes.AccountEditorScene;
import org.tlauncher.tlauncher.ui.center.CenterPanel;

public class AccountEditor extends CenterPanel
{
    private static final String passlock = "passlock";
    private final AccountEditorScene scene;
    public final UsernameField username;
    public final BlockablePasswordField password;
    public final ButtonGroup authGroup;
    public final AuthTypeRadio mojangAuth;
    public final AuthTypeRadio tlauncherAuth;
    public final UpdaterButton save;
    public final LinkedHashMap<Account.AccountType, AuthTypeRadio> radioMap;
    private final Configuration configuration;
    private final FlexibleEditorPanel flex;
    public final EditorCheckBox skinCheckBox;
    public static final Dimension SIZE;
    public static final Color FIELD_COLOR;
    
    public AccountEditor(final AccountEditorScene sc, final FlexibleEditorPanel flex) {
        super(AccountEditor.noInsets);
        this.configuration = TLauncher.getInstance().getConfiguration();
        this.scene = sc;
        this.flex = flex;
        final ActionListener enterHandler = e -> {
            this.defocus();
            this.scene.handler.saveEditor();
            return;
        };
        this.username = new UsernameField(this, UsernameField.UsernameState.EMAIL);
        final Border empty = new EmptyBorder(0, 9, 0, 0);
        final CompoundBorder border = new CompoundBorder(BorderFactory.createLineBorder(AccountEditor.FIELD_COLOR, 1), empty);
        this.username.setBorder(border);
        this.username.addActionListener(enterHandler);
        this.username.setForeground(AccountEditor.FIELD_COLOR);
        (this.password = new BlockablePasswordField()).addActionListener(enterHandler);
        this.password.setBorder(border);
        this.password.setEnabled(false);
        this.password.setForeground(AccountEditor.FIELD_COLOR);
        this.authGroup = new ButtonGroup();
        this.radioMap = new LinkedHashMap<Account.AccountType, AuthTypeRadio>();
        (this.mojangAuth = new AuthTypeRadio(Account.AccountType.MOJANG)).setFont(this.mojangAuth.getFont().deriveFont(1));
        (this.tlauncherAuth = new AuthTypeRadio(Account.AccountType.TLAUNCHER)).setFont(this.tlauncherAuth.getFont().deriveFont(1));
        (this.save = new UpdaterButton(UpdaterButton.GREEN_COLOR, "account.save")).setFont(this.save.getFont().deriveFont(1, 16.0f));
        this.save.setForeground(Color.WHITE);
        this.save.addActionListener(enterHandler);
        (this.skinCheckBox = new EditorCheckBox("skin.status.checkbox") {
            @Override
            public void block(final Object reason) {
            }
            
            @Override
            public void unblock(final Object reason) {
            }
        }).setState(this.configuration.getBoolean("skin.status.checkbox.state"));
        this.skinCheckBox.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(final MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (!AccountEditor.this.skinCheckBox.getState()) {
                        if (Alert.showLocQuestion("skin.status.title", "skin.status.message")) {
                            AccountEditor.this.configuration.set("skin.status.checkbox.state", false, true);
                        }
                        else {
                            AccountEditor.this.skinCheckBox.setState(true);
                        }
                    }
                    else {
                        Alert.showMessage(AccountEditor.this.lang.get("skin.checkbox.switch.on.title"), AccountEditor.this.lang.get("skin.checkbox.switch.on.message"));
                        AccountEditor.this.configuration.set("skin.status.checkbox.state", true, true);
                    }
                }
            }
        });
        this.setPreferredSize(new Dimension(214, 171));
        final SpringLayout springLayout = new SpringLayout();
        this.setLayout(springLayout);
        final JComponent panel = this.username;
        springLayout.putConstraint("North", panel, 0, "North", this);
        springLayout.putConstraint("South", panel, 20, "North", panel);
        springLayout.putConstraint("West", panel, 0, "West", this);
        springLayout.putConstraint("East", panel, 214, "West", this);
        this.add(panel);
        final JComponent panel_1 = this.tlauncherAuth;
        springLayout.putConstraint("North", panel_1, 12, "South", panel);
        springLayout.putConstraint("South", panel_1, 15, "North", panel_1);
        springLayout.putConstraint("West", panel_1, -4, "West", panel);
        springLayout.putConstraint("East", panel_1, 8, "East", panel);
        this.add(panel_1);
        final JComponent panel_2 = this.mojangAuth;
        springLayout.putConstraint("North", panel_2, 12, "South", panel_1);
        springLayout.putConstraint("South", panel_2, 15, "North", panel_2);
        springLayout.putConstraint("West", panel_2, -4, "West", panel);
        springLayout.putConstraint("East", panel_2, 0, "East", panel);
        this.add(panel_2);
        final JComponent panel_3 = this.password;
        springLayout.putConstraint("North", panel_3, 12, "South", panel_2);
        springLayout.putConstraint("South", panel_3, 19, "North", panel_3);
        springLayout.putConstraint("West", panel_3, 0, "West", panel);
        springLayout.putConstraint("East", panel_3, 0, "East", panel);
        this.add(panel_3);
        final JComponent panel_4 = this.skinCheckBox;
        springLayout.putConstraint("North", panel_4, 12, "South", panel_3);
        springLayout.putConstraint("South", panel_4, 15, "North", panel_4);
        springLayout.putConstraint("West", panel_4, -4, "West", panel);
        springLayout.putConstraint("East", panel_4, 0, "East", panel);
        this.add(panel_4);
        final JComponent panel_5 = this.save;
        springLayout.putConstraint("North", panel_5, 13, "South", panel_4);
        springLayout.putConstraint("South", panel_5, 26, "North", panel_5);
        springLayout.putConstraint("West", panel_5, 0, "West", panel);
        springLayout.putConstraint("East", panel_5, 0, "East", panel);
        this.add(panel_5);
    }
    
    public Account.AccountType getSelectedAccountType() {
        for (final Map.Entry<Account.AccountType, AuthTypeRadio> en : this.radioMap.entrySet()) {
            if (en.getValue().isSelected()) {
                return en.getKey();
            }
        }
        return Account.AccountType.TLAUNCHER;
    }
    
    public void setSelectedAccountType(final Account.AccountType type) {
        final AuthTypeRadio selectable = this.radioMap.get(type);
        if (selectable != null) {
            selectable.setSelected(true);
        }
    }
    
    public void fill(final Account account) {
        this.setSelectedAccountType(account.getType());
        this.username.setText(account.getUsername());
        this.password.setText(null);
    }
    
    public void clear() {
        this.setSelectedAccountType(null);
        this.username.setText(null);
        this.password.setText(null);
    }
    
    public Account get() {
        final Account account = new Account();
        account.setUsername(this.username.getValue());
        final Account.AccountType type = this.getSelectedAccountType();
        switch (type) {
            case TLAUNCHER:
            case MOJANG: {
                if (this.password.hasPassword()) {
                    account.setPassword(new String(this.password.getPassword()));
                    break;
                }
                break;
            }
        }
        account.setType(type);
        return account;
    }
    
    @Override
    public void block(final Object reason) {
        super.block(reason);
        if (!reason.equals("empty")) {}
    }
    
    @Override
    public void unblock(final Object reason) {
        super.unblock(reason);
        if (!reason.equals("empty")) {}
    }
    
    static {
        SIZE = new Dimension(214, 171);
        FIELD_COLOR = new Color(149, 149, 149);
    }
    
    private class BlockablePasswordField extends ExtendedPasswordField implements Blockable
    {
        @Override
        public void block(final Object reason) {
            this.setEnabled(false);
        }
        
        @Override
        public void unblock(final Object reason) {
            this.setEnabled(true);
        }
    }
    
    public class AuthTypeRadio extends LocalizableRadioButton
    {
        private final Account.AccountType type;
        
        private AuthTypeRadio(final Account.AccountType type) {
            super("account.auth." + type.toString().toLowerCase());
            AccountEditor.this.radioMap.put(type, this);
            AccountEditor.this.authGroup.add(this);
            this.type = type;
            this.setFocusable(false);
            final boolean free = type == Account.AccountType.FREE;
            this.addItemListener(new CheckBoxListener() {
                @Override
                public void itemStateChanged(boolean newstate) {
                    if (newstate && !AccountEditor.this.password.hasPassword()) {
                        AccountEditor.this.password.setText(null);
                    }
                    if (newstate) {
                        AccountEditor.this.scene.tip.setAccountType(type);
                        if (AuthTypeRadio.this.type == Account.AccountType.TLAUNCHER) {
                            AccountEditor.this.flex.setText(Localizable.get("auth.tip.tlauncher"));
                        }
                        else {
                            AccountEditor.this.flex.setText(Localizable.get("auth.tip.mojang"));
                        }
                    }
                    newstate &= free;
                    Blocker.setBlocked(AccountEditor.this.password, "passlock", newstate);
                    AccountEditor.this.username.setState(newstate ? UsernameField.UsernameState.USERNAME : UsernameField.UsernameState.EMAIL_LOGIN);
                    if (AccountEditor.this.mojangAuth.isSelected()) {
                        AccountEditor.this.username.setState(UsernameField.UsernameState.EMAIL);
                    }
                    AccountEditor.this.defocus();
                }
            });
        }
        
        public Account.AccountType getAccountType() {
            return this.type;
        }
    }
}
