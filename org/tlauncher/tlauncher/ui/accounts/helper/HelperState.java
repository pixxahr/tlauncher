// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.accounts.helper;

import org.tlauncher.tlauncher.ui.loc.LocalizableMenuItem;

public enum HelperState
{
    PREMIUM, 
    FREE, 
    HELP(false), 
    NONE;
    
    public final LocalizableMenuItem item;
    public final boolean showInList;
    
    private HelperState() {
        this(true);
    }
    
    private HelperState(final boolean showInList) {
        this.item = new LocalizableMenuItem("auth.helper." + this.toString());
        this.showInList = showInList;
    }
    
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
