// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.accounts.helper;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.center.CenterPanelTheme;
import org.tlauncher.tlauncher.ui.center.LoginHelperTheme;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import org.tlauncher.tlauncher.ui.center.CenterPanel;

class HelperTip extends CenterPanel
{
    public final String name;
    public final LocalizableLabel label;
    public final Component component;
    public final Component parent;
    public final byte alignment;
    public final HelperState[] states;
    private static final LoginHelperTheme HelperTipTheme;
    
    HelperTip(final String name, final Component component, final Component parent, final byte alignment, final HelperState... states) {
        super(HelperTip.HelperTipTheme, HelperTip.smallSquareInsets);
        if (name == null) {
            throw new NullPointerException("Name is NULL");
        }
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name is empty");
        }
        if (component == null) {
            throw new NullPointerException("Component is NULL");
        }
        if (parent == null) {
            throw new NullPointerException("Parent is NULL");
        }
        if (alignment > 3) {
            throw new IllegalArgumentException("Unknown alignment");
        }
        if (states == null) {
            throw new NullPointerException("State array is NULL");
        }
        this.name = name;
        this.component = component;
        this.parent = parent;
        this.alignment = alignment;
        this.label = new LocalizableLabel();
        this.states = states;
        this.add(this.label);
        this.setBackground(U.shiftAlpha(this.getTheme().getBackground(), 255));
    }
    
    static {
        HelperTipTheme = new LoginHelperTheme();
    }
}
