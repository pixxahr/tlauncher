// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.accounts.helper;

import java.awt.Insets;
import java.awt.FontMetrics;
import java.awt.Point;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.Component;
import javax.swing.JComponent;
import org.tlauncher.tlauncher.ui.scenes.AccountEditorScene;
import org.tlauncher.tlauncher.ui.accounts.AccountHandler;
import org.tlauncher.tlauncher.ui.MainPane;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedLayeredPane;

public class AccountEditorHelper extends ExtendedLayeredPane
{
    static final int MARGIN = 5;
    static final byte LEFT = 0;
    static final byte UP = 1;
    static final byte RIGHT = 2;
    static final byte DOWN = 3;
    private final MainPane pane;
    private final AccountHandler handler;
    private final HelperTip[] tips;
    
    public AccountEditorHelper(final AccountEditorScene scene) {
        super(scene);
        this.handler = scene.handler;
        this.pane = scene.getMainPane();
        this.add((Component[])(this.tips = new HelperTip[] { new HelperTip("account", this.handler.editor, this.handler.editor, (byte)1, new HelperState[] { HelperState.PREMIUM, HelperState.FREE }), new HelperTip("username", this.handler.editor.username, this.handler.editor, (byte)0, new HelperState[] { HelperState.PREMIUM, HelperState.FREE }), new HelperTip("password", this.handler.editor.password, this.handler.editor, (byte)0, new HelperState[] { HelperState.PREMIUM }), new HelperTip("button", this.handler.editor.save, this.handler.editor, (byte)0, new HelperState[] { HelperState.PREMIUM, HelperState.FREE }) }));
    }
    
    public void setState(final HelperState state) {
        if (state == null) {
            throw new NullPointerException();
        }
        for (final HelperState st : HelperState.values()) {
            st.item.setEnabled(!st.equals(state));
        }
        if (state == HelperState.NONE) {
            for (final HelperTip step : this.tips) {
                step.setVisible(false);
            }
            return;
        }
        for (final HelperTip step : this.tips) {
            final LocalizableLabel l = step.label;
            l.setText("auth.helper." + state.toString() + "." + step.name);
            final Component c = step.component;
            final int cWidth = c.getWidth();
            final int cHeight = c.getHeight();
            final Point cp = this.pane.getLocationOf(c);
            final Component p = step.parent;
            final int pWidth = p.getWidth();
            final int pHeight = p.getHeight();
            final Point pp = this.pane.getLocationOf(p);
            final FontMetrics fm = l.getFontMetrics(l.getFont());
            final Insets i = step.getInsets();
            final int height = i.top + i.bottom + fm.getHeight();
            final int width = i.left + i.right + fm.stringWidth(l.getText());
            int x = 0;
            int y = 0;
            switch (step.alignment) {
                case 0: {
                    x = pp.x - 5 - width;
                    y = cp.y + cHeight / 2 - height / 2;
                    break;
                }
                case 1: {
                    x = cp.x + cWidth / 2 - width / 2;
                    y = pp.y - 5 - height;
                    break;
                }
                case 2: {
                    x = pp.x + pWidth + 5;
                    y = cp.y + cHeight / 2 - height / 2;
                    break;
                }
                case 3: {
                    x = cp.x + cWidth / 2 - width / 2;
                    y = pp.y + pHeight + 5;
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Unknown alignment");
                }
            }
            if (x < 0) {
                x = 0;
            }
            else if (x + width > this.getWidth()) {
                x = this.getWidth() - width;
            }
            if (y < 0) {
                y = 0;
            }
            else if (y + height > this.getHeight()) {
                y = this.getHeight() - height;
            }
            step.setVisible(true);
            step.setBounds(x, y, width, height);
        }
    }
    
    @Override
    public void onResize() {
        super.onResize();
    }
}
