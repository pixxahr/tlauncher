// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.accounts;

import org.tlauncher.tlauncher.ui.block.Unblockable;
import org.tlauncher.tlauncher.ui.swing.ImageButton;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import javax.swing.JPanel;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.minecraft.auth.AuthenticatorDatabase;
import org.tlauncher.tlauncher.managers.ProfileManager;
import org.tlauncher.tlauncher.managers.ProfileManagerListener;
import javax.swing.Box;
import org.tlauncher.tlauncher.ui.loc.ImageUdaterButton;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedButton;
import java.awt.FlowLayout;
import javax.swing.border.Border;
import javax.swing.plaf.ScrollBarUI;
import org.tlauncher.tlauncher.ui.swing.scroll.AccountScrollBarUI;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import org.tlauncher.tlauncher.ui.swing.AccountCellRenderer;
import javax.swing.ListModel;
import javax.swing.BorderFactory;
import java.awt.Component;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.util.swing.FontTL;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.LayoutManager;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.loc.UpdaterButton;
import javax.swing.JList;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import javax.swing.DefaultListModel;
import org.tlauncher.tlauncher.ui.scenes.AccountEditorScene;
import org.tlauncher.tlauncher.ui.center.CenterPanel;

public class AccountList extends CenterPanel
{
    private static final long serialVersionUID = 3280495266368287215L;
    private final AccountEditorScene scene;
    public final DefaultListModel<Account> model;
    public final JList<Account> list;
    public final UpdaterButton add;
    public final UpdaterButton remove;
    public static final Dimension SIZE;
    
    public AccountList(final AccountEditorScene sc) {
        super(AccountList.noInsets);
        this.setPreferredSize(AccountList.SIZE);
        this.scene = sc;
        final JPanel panel = new ExtendedPanel(new BorderLayout(0, 0));
        final LocalizableLabel label = new LocalizableLabel("account.list");
        SwingUtil.changeFontFamily(label, FontTL.ROBOTO_BOLD);
        label.setFont(label.getFont().deriveFont(14));
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 13, 0));
        label.setHorizontalAlignment(2);
        label.setVerticalAlignment(1);
        this.model = new DefaultListModel<Account>();
        (this.list = new JList<Account>(this.model)).setCellRenderer(new AccountCellRenderer(AccountCellRenderer.AccountCellType.EDITOR));
        this.list.setSelectionMode(0);
        this.list.setBackground(AccountEditorScene.BACKGROUND_ACCOUNT_COLOR);
        final Account account;
        this.list.addListSelectionListener(e -> {
            account = this.list.getSelectedValue();
            this.scene.handler.refreshEditor(account);
            return;
        });
        final JScrollPane scroll = new JScrollPane(this.list);
        scroll.getVerticalScrollBar().setUI(new AccountScrollBarUI());
        scroll.setBorder(null);
        scroll.setHorizontalScrollBarPolicy(31);
        scroll.setVerticalScrollBarPolicy(20);
        final JPanel buttons = new ExtendedPanel();
        buttons.setLayout(new FlowLayout(0, 0, 0));
        buttons.setPreferredSize(new Dimension(AccountList.SIZE.width, 26));
        (this.add = new UpdaterButton(ExtendedButton.ORRANGE_COLOR, "account.list.add")).setFont(this.add.getFont().deriveFont(1, 16.0f));
        this.add.setForeground(Color.WHITE);
        this.add.setPreferredSize(new Dimension(100, 26));
        this.add.addActionListener(e -> {
            this.scene.handler.addAccount();
            this.defocus();
            return;
        });
        (this.remove = new UpdaterButton(ImageUdaterButton.ORRANGE_COLOR, "account.list.remove")).setPreferredSize(new Dimension(100, 26));
        this.remove.setFont(this.remove.getFont().deriveFont(1, 16.0f));
        this.remove.setForeground(Color.WHITE);
        this.remove.addActionListener(e -> {
            this.scene.handler.removeAccount();
            this.defocus();
            return;
        });
        buttons.add(this.add);
        buttons.add(Box.createHorizontalStrut(11));
        buttons.add(this.remove);
        panel.add("South", buttons);
        panel.add("Center", scroll);
        panel.add("North", label);
        this.add(panel);
        final ProfileManagerListener listener = new ProfileManagerListener() {
            @Override
            public void onProfilesRefreshed(final ProfileManager pm) {
                AccountList.this.refreshFrom(pm.getAuthDatabase());
            }
            
            @Override
            public void onProfileManagerChanged(final ProfileManager pm) {
                AccountList.this.refreshFrom(pm.getAuthDatabase());
            }
            
            @Override
            public void onAccountsRefreshed(final AuthenticatorDatabase db) {
                AccountList.this.refreshFrom(db);
            }
        };
        TLauncher.getInstance().getProfileManager().addListener(listener);
    }
    
    public void refreshFrom(final AuthenticatorDatabase db) {
        this.model.clear();
        for (final Account account : db.getAccounts()) {
            this.model.addElement(account);
        }
        if (this.model.isEmpty()) {
            this.scene.handler.notifyEmpty();
        }
    }
    
    static {
        SIZE = new Dimension(211, 171);
    }
    
    class UnblockableImageButton extends ImageButton implements Unblockable
    {
        public UnblockableImageButton(final String imagepath) {
            super(imagepath);
        }
    }
}
