// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.accounts;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.block.Blockable;
import org.tlauncher.tlauncher.ui.block.Blocker;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.minecraft.auth.Authenticator;
import org.tlauncher.tlauncher.minecraft.auth.AuthenticatorListener;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.tlauncher.ui.listener.AuthUIListener;
import org.tlauncher.tlauncher.managers.ProfileManager;
import org.tlauncher.tlauncher.ui.scenes.AccountEditorScene;

public class AccountHandler
{
    private static final String fieldConfig = "mojang.account.protection.hide";
    private final AccountEditorScene scene;
    public final AccountList list;
    public final AccountEditor editor;
    private final ProfileManager manager;
    private final AuthUIListener listener;
    private Account lastAccount;
    private Account tempAccount;
    
    public AccountHandler(final AccountEditorScene sc) {
        this.manager = TLauncher.getInstance().getProfileManager();
        this.scene = sc;
        this.list = this.scene.list;
        this.editor = this.scene.editor;
        this.listener = new AuthUIListener(new AuthenticatorListener() {
            @Override
            public void onAuthPassing(final Authenticator auth) {
                AccountHandler.this.block();
            }
            
            @Override
            public void onAuthPassingError(final Authenticator auth, final Throwable e) {
                AccountHandler.this.unblock();
                final int num = AccountHandler.this.list.model.indexOf(AccountHandler.this.lastAccount);
                AccountHandler.this.list.model.remove(num);
                AccountHandler.this.clearEditor();
                AccountHandler.this.list.repaint();
                AccountHandler.this.tempAccount = null;
            }
            
            @Override
            public void onAuthPassed(final Authenticator auth) {
                AccountHandler.this.unblock();
                AccountHandler.this.registerTemp();
                final Configuration c = TLauncher.getInstance().getConfiguration();
                if (auth.getAccount().getType().equals(Account.AccountType.MOJANG) && !c.getBoolean("mojang.account.protection.hide") && Alert.showWarningMessageWithCheckBox(Localizable.get("account.protection.message.title"), Localizable.get("account.protection.message"), 350, Localizable.get("account.message.show.again"))) {
                    c.set("mojang.account.protection.hide", true);
                }
            }
        });
    }
    
    public void selectAccount(final Account acc) {
        if (acc == null) {
            return;
        }
        if (acc.equals(this.list.list.getSelectedValue())) {
            return;
        }
        this.list.list.setSelectedValue(acc, true);
    }
    
    void refreshEditor(final Account account) {
        if (account == null) {
            this.clearEditor();
            return;
        }
        if (account.equals(this.lastAccount)) {
            return;
        }
        this.lastAccount = account;
        Blocker.unblock(this.editor, "empty");
        this.editor.fill(account);
        if (!account.equals(this.tempAccount)) {
            this.scene.getMainPane().defaultScene.loginForm.accountComboBox.setAccount(this.lastAccount);
        }
    }
    
    void clearEditor() {
        this.lastAccount = null;
        this.editor.clear();
        this.notifyEmpty();
    }
    
    void saveEditor() {
        if (this.lastAccount == null) {
            return;
        }
        final Account acc = this.editor.get();
        if (acc.getUsername() == null) {
            Alert.showLocError("auth.error.email.account");
            return;
        }
        this.lastAccount.complete(acc);
        U.log(this.lastAccount.isFree());
        if (!this.lastAccount.isFree()) {
            if (this.lastAccount.getAccessToken() == null && this.lastAccount.getPassword() == null) {
                Alert.showLocError("auth.error.nopass");
                return;
            }
            Authenticator.instanceFor(this.lastAccount).asyncPass(this.listener);
        }
        else {
            this.registerTemp();
            this.listener.saveProfiles();
        }
    }
    
    public void exitEditor() {
        this.scene.getMainPane().openDefaultScene();
        this.listener.saveProfiles();
        this.list.list.clearSelection();
        this.tempAccount = null;
        this.notifyEmpty();
    }
    
    public void addAccount() {
        if (this.tempAccount != null) {
            return;
        }
        for (int i = 0; i < this.list.model.getSize(); ++i) {
            if (this.list.model.getElementAt(i).getUsername() == null) {
                return;
            }
        }
        this.tempAccount = new Account();
        this.list.model.addElement(this.tempAccount);
        this.list.list.setSelectedValue(this.tempAccount, true);
        this.refreshEditor(this.tempAccount);
    }
    
    public void removeAccount() {
        if (this.lastAccount == null || this.list.model.isEmpty()) {
            return;
        }
        final Account selected = this.list.list.getSelectedValue();
        final Account acc = this.lastAccount;
        final int num = this.list.model.indexOf(this.lastAccount) - 1;
        this.list.model.removeElement(selected);
        this.lastAccount = acc;
        if (selected.getUsername() != null) {
            U.log("Removing", this.lastAccount);
            this.manager.getAuthDatabase().unregisterAccount(selected);
            this.listener.saveProfiles();
            this.tempAccount = null;
        }
        else {
            this.tempAccount = null;
            this.clearEditor();
        }
        if (num > -1) {
            this.list.list.setSelectedIndex(num);
        }
    }
    
    void registerTemp() {
        if (this.tempAccount == null) {
            return;
        }
        this.manager.getAuthDatabase().registerAccount(this.tempAccount);
        this.scene.getMainPane().defaultScene.loginForm.accountComboBox.refreshAccounts(this.manager.getAuthDatabase(), this.tempAccount);
        final int num = this.list.model.indexOf(this.tempAccount);
        this.list.list.setSelectedIndex(num);
        this.tempAccount = null;
    }
    
    public void notifyEmpty() {
        if (this.list.list.getSelectedIndex() == -1) {
            Blocker.block(this.editor, "empty");
        }
    }
    
    private void block() {
        Blocker.block("auth", this.editor, this.list);
    }
    
    private void unblock() {
        Blocker.unblock("auth", this.editor, this.list);
    }
}
