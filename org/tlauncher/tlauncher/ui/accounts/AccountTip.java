// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.accounts;

import java.net.URL;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import org.tlauncher.tlauncher.ui.swing.editor.EditorPane;
import org.tlauncher.tlauncher.ui.scenes.AccountEditorScene;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.ui.center.CenterPanel;

public class AccountTip extends CenterPanel implements LocalizableComponent
{
    public static final int WIDTH = 510;
    private final AccountEditorScene scene;
    public final Tip mojangTip;
    public final Tip tlauncherTip;
    private Tip tip;
    private final EditorPane content;
    
    public AccountTip(final AccountEditorScene sc) {
        super(AccountTip.smallSquareInsets);
        this.scene = sc;
        (this.content = new EditorPane()).addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                if (!AccountTip.this.isVisible()) {
                    e.consume();
                }
            }
            
            @Override
            public void mousePressed(final MouseEvent e) {
                if (!AccountTip.this.isVisible()) {
                    e.consume();
                }
            }
            
            @Override
            public void mouseReleased(final MouseEvent e) {
                if (!AccountTip.this.isVisible()) {
                    e.consume();
                }
            }
            
            @Override
            public void mouseEntered(final MouseEvent e) {
                if (!AccountTip.this.isVisible()) {
                    e.consume();
                }
            }
            
            @Override
            public void mouseExited(final MouseEvent e) {
                if (!AccountTip.this.isVisible()) {
                    e.consume();
                }
            }
        });
        this.add(this.content);
        this.mojangTip = new Tip(Account.AccountType.MOJANG, ImageCache.getRes("mojang-user.png"));
        this.tlauncherTip = new Tip(Account.AccountType.TLAUNCHER, ImageCache.getRes("tlauncher-user.png"));
        this.setTip(null);
    }
    
    public void setAccountType(final Account.AccountType type) {
        if (type != null) {
            switch (type) {
                case TLAUNCHER: {
                    this.setTip(this.tlauncherTip);
                    return;
                }
                case FREE: {
                    return;
                }
                case MOJANG: {
                    this.setTip(this.mojangTip);
                    return;
                }
            }
        }
        this.setTip(null);
    }
    
    public Tip getTip() {
        return this.tip;
    }
    
    public void setTip(final Tip tip) {
        this.tip = tip;
        if (tip == null) {
            this.setVisible(false);
            return;
        }
        this.setVisible(true);
        final StringBuilder builder = new StringBuilder();
        builder.append("<table width=\"").append(510 - this.getInsets().left - this.getInsets().right).append("\" height=\"").append(tip.getHeight()).append("\"><tr><td align=\"center\" valign=\"center\">");
        if (tip.image != null) {
            builder.append("<img src=\"").append(tip.image).append("\" /></td><td align=\"center\" valign=\"center\" width=\"100%\">");
        }
        builder.append(Localizable.get(tip.path));
        builder.append("</td></tr></table>");
        this.setContent(builder.toString(), 510, tip.getHeight());
    }
    
    void setContent(final String text, int width, int height) {
        if (width < 1 || height < 1) {
            throw new IllegalArgumentException();
        }
        this.content.setText(text);
        if (OS.CURRENT == OS.LINUX) {
            width *= (int)1.2;
            height *= (int)1.2;
        }
        this.setSize(width, height + this.getInsets().top + this.getInsets().bottom);
    }
    
    @Override
    public void updateLocale() {
        this.setTip(this.tip);
    }
    
    public class Tip
    {
        private final Account.AccountType type;
        private final String path;
        private final URL image;
        
        Tip(final Account.AccountType type, final URL image) {
            this.type = type;
            this.path = "auth.tip." + type.toString().toLowerCase();
            this.image = image;
        }
        
        public int getHeight() {
            return AccountTip.this.tlauncher.getLang().getInteger(this.path + ".height");
        }
    }
}
