// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.console;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Iterator;
import java.awt.Dimension;
import java.awt.Point;
import org.tlauncher.util.async.AsyncThread;
import java.io.IOException;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.pastebin.PasteResult;
import org.tlauncher.util.pastebin.PasteListener;
import org.tlauncher.util.pastebin.Paste;
import org.tlauncher.util.stream.StringStream;
import org.tlauncher.util.StringUtil;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedComponentAdapter;
import java.awt.event.WindowListener;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.configuration.enums.ConsoleType;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;
import org.tlauncher.util.stream.PrintLogger;
import org.tlauncher.util.stream.LinkedStringStream;
import org.tlauncher.tlauncher.configuration.Configuration;
import java.util.List;
import org.tlauncher.util.stream.Logger;

public class Console implements Logger
{
    private static List<ConsoleFrame> frames;
    public final ConsoleFrame frame;
    private final Configuration global;
    private String name;
    private LinkedStringStream stream;
    private PrintLogger logger;
    private CloseAction close;
    private boolean killed;
    MinecraftLauncher launcher;
    
    public Console(final Configuration global, final PrintLogger logger, final String name, final boolean show) {
        this.global = global;
        this.frame = new ConsoleFrame(this);
        this.setName(name);
        Console.frames.add(this.frame);
        this.update();
        this.frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                Console.this.setShown(false);
                global.set("gui.console", ConsoleType.NONE);
            }
            
            @Override
            public void windowClosed(final WindowEvent e) {
                U.log("Console", Console.this.name, "has been disposed.");
            }
        });
        this.frame.addComponentListener(new ExtendedComponentAdapter(this.frame) {
            @Override
            public void componentShown(final ComponentEvent e) {
                Console.this.save(true);
            }
            
            @Override
            public void componentHidden(final ComponentEvent e) {
                Console.this.save(true);
            }
            
            @Override
            public void onComponentResized(final ComponentEvent e) {
                Console.this.save(true);
            }
            
            @Override
            public void onComponentMoved(final ComponentEvent e) {
                Console.this.save(true);
            }
        });
        this.frame.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(final ComponentEvent e) {
                Console.this.save(false);
            }
            
            @Override
            public void componentMoved(final ComponentEvent e) {
                Console.this.save(false);
            }
            
            @Override
            public void componentShown(final ComponentEvent e) {
                Console.this.save(true);
            }
            
            @Override
            public void componentHidden(final ComponentEvent e) {
                Console.this.save(true);
            }
        });
        if (logger == null) {
            this.logger = null;
            (this.stream = new LinkedStringStream()).setLogger(this);
        }
        else {
            this.logger = logger;
            this.stream = logger.getStream();
        }
        if (show) {
            this.show();
        }
        this.stream.flush();
        if (logger != null) {
            logger.setMirror(this);
        }
    }
    
    public Console(final PrintLogger logger, final String name) {
        this(null, logger, name, true);
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
        this.frame.setTitle(name);
    }
    
    public MinecraftLauncher getLauncher() {
        return this.launcher;
    }
    
    public void setLauncher(final MinecraftLauncher launcher) {
        this.launcher = launcher;
        this.frame.bottom.kill.setEnabled(launcher != null);
    }
    
    @Override
    public void log(final String s) {
        if (this.logger != null) {
            this.logger.rawlog(s);
        }
        else {
            this.stream.write(s.toCharArray());
        }
    }
    
    @Override
    public void log(final Object... o) {
        this.log(U.toLog(o));
    }
    
    @Override
    public void rawlog(final String s) {
        if (StringUtil.lastChar(s) == '\n') {
            this.frame.print(s);
        }
        else {
            this.frame.println(s);
        }
    }
    
    public void rawlog(final Object... o) {
        this.rawlog(U.toLog(o));
    }
    
    @Override
    public void rawlog(final char[] c) {
        this.rawlog(new String(c));
    }
    
    public PrintLogger getLogger() {
        return this.logger;
    }
    
    public String getOutput() {
        return this.frame.textarea.getText();
    }
    
    StringStream getStream() {
        return this.stream;
    }
    
    void update() {
        this.check();
        if (this.global == null) {
            return;
        }
        final String prefix = "gui.console.";
        final int width = this.global.getInteger(prefix + "width", 670);
        final int height = this.global.getInteger(prefix + "height", 500);
        final int x = this.global.getInteger(prefix + "x", 0);
        final int y = this.global.getInteger(prefix + "y", 0);
        this.frame.setSize(width, height);
        this.frame.setLocation(x, y);
    }
    
    void save() {
        this.save(false);
    }
    
    void save(final boolean flush) {
        this.check();
        if (this.global == null) {
            return;
        }
        final String prefix = "gui.console.";
        final int[] size = this.getSize();
        final int[] position = this.getPosition();
        this.global.set(prefix + "width", size[0], false);
        this.global.set(prefix + "height", size[1], false);
        this.global.set(prefix + "x", position[0], false);
        this.global.set(prefix + "y", position[1], false);
    }
    
    private void check() {
        if (this.killed) {
            throw new IllegalStateException("Console is already killed!");
        }
    }
    
    public void setShown(final boolean shown) {
        if (shown) {
            this.show();
        }
        else {
            this.hide();
        }
    }
    
    public void show() {
        this.show(true);
    }
    
    public void show(final boolean toFront) {
        this.check();
        this.frame.setVisible(true);
        this.frame.scrollDown();
        if (toFront) {
            this.frame.toFront();
        }
    }
    
    public void hide() {
        this.check();
        this.frame.setVisible(false);
    }
    
    public void clear() {
        this.check();
        this.stream.flush();
        this.frame.clear();
    }
    
    public void kill() {
        this.check();
        this.save();
        this.frame.dispose();
        this.frame.clear();
        Console.frames.remove(this.frame);
        this.killed = true;
    }
    
    public void killIn(final long millis) {
        this.check();
        this.save();
        this.frame.hideIn(millis);
    }
    
    public void sendPaste() {
        AsyncThread.execute(new Runnable() {
            @Override
            public void run() {
                final Paste paste = new Paste();
                paste.addListener(Console.this.frame);
                paste.setTitle(Console.this.frame.getTitle());
                paste.setContent(Console.this.frame.console.getOutput());
                final PasteResult result = paste.paste();
                if (result instanceof PasteResult.PasteUploaded) {
                    final PasteResult.PasteUploaded uploaded = (PasteResult.PasteUploaded)result;
                    if (Alert.showLocQuestion("console.pastebin.sent", uploaded.getURL())) {
                        OS.openLink(uploaded.getURL());
                    }
                }
                else if (result instanceof PasteResult.PasteFailed) {
                    final Throwable error = ((PasteResult.PasteFailed)result).getError();
                    if (error instanceof RuntimeException) {
                        Alert.showLocError("console.pastebin.invalid", error);
                    }
                    else if (error instanceof IOException) {
                        Alert.showLocError("console.pastebin.failed", error);
                    }
                }
            }
        });
    }
    
    public boolean isKilled() {
        this.check();
        return this.killed;
    }
    
    boolean isHidden() {
        return !this.frame.isShowing();
    }
    
    Point getPositionPoint() {
        this.check();
        return this.frame.getLocation();
    }
    
    int[] getPosition() {
        this.check();
        final Point p = this.getPositionPoint();
        return new int[] { p.x, p.y };
    }
    
    Dimension getDimension() {
        this.check();
        return this.frame.getSize();
    }
    
    int[] getSize() {
        this.check();
        final Dimension d = this.getDimension();
        return new int[] { d.width, d.height };
    }
    
    public CloseAction getCloseAction() {
        return this.close;
    }
    
    public void setCloseAction(final CloseAction action) {
        this.close = action;
    }
    
    private void onClose() {
        if (this.close == null) {
            return;
        }
        switch (this.close) {
            case KILL: {
                this.kill();
                break;
            }
        }
    }
    
    public static void updateLocale() {
        for (final ConsoleFrame frame : Console.frames) {
            frame.updateLocale();
        }
    }
    
    static {
        Console.frames = Collections.synchronizedList(new ArrayList<ConsoleFrame>());
    }
    
    public enum CloseAction
    {
        KILL, 
        EXIT;
    }
}
