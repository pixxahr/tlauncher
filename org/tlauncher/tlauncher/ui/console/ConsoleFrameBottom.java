// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.console;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.awt.Dimension;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import org.tlauncher.tlauncher.ui.swing.ImageButton;
import org.tlauncher.tlauncher.ui.loc.LocalizableButton;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.tlauncher.ui.swing.extended.BorderPanel;

public class ConsoleFrameBottom extends BorderPanel implements LocalizableComponent
{
    private final ConsoleFrame frame;
    public final LocalizableButton closeCancelButton;
    public final ImageButton pastebin;
    public final ImageButton kill;
    
    ConsoleFrameBottom(final ConsoleFrame fr) {
        this.frame = fr;
        this.setOpaque(true);
        this.setBackground(Color.darkGray);
        (this.closeCancelButton = new LocalizableButton("console.close.cancel")).setVisible(false);
        this.closeCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if (!ConsoleFrameBottom.this.closeCancelButton.isVisible()) {
                    return;
                }
                ConsoleFrameBottom.this.frame.hiding = false;
                ConsoleFrameBottom.this.closeCancelButton.setVisible(false);
            }
        });
        this.setCenter(this.closeCancelButton);
        this.pastebin = this.newButton("mail-attachment.png", new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ConsoleFrameBottom.this.frame.console.sendPaste();
            }
        });
        (this.kill = this.newButton("process-stop.png", new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ConsoleFrameBottom.this.frame.console.launcher.killProcess();
                ConsoleFrameBottom.this.kill.setEnabled(false);
            }
        })).setEnabled(false);
        this.updateLocale();
        final ExtendedPanel buttonPanel = new ExtendedPanel();
        buttonPanel.add(this.pastebin, this.kill);
        this.setEast(buttonPanel);
    }
    
    private ImageButton newButton(final String path, final ActionListener action) {
        final ImageButton button = new ImageButton(path);
        button.addActionListener(action);
        button.setPreferredSize(new Dimension(32, 32));
        return button;
    }
    
    @Override
    public void updateLocale() {
        this.pastebin.setToolTipText(Localizable.get("console.pastebin"));
        this.kill.setToolTipText(Localizable.get("console.kill"));
    }
}
