// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.console;

import org.tlauncher.tlauncher.ui.swing.ImageButton;
import org.tlauncher.tlauncher.ui.loc.LocalizableCheckbox;
import org.tlauncher.tlauncher.ui.swing.extended.BorderPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.tlauncher.tlauncher.ui.loc.LocalizableInvalidateTextField;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.GroupLayout;
import org.tlauncher.tlauncher.ui.swing.extended.ExtendedPanel;

public class SearchPanel extends ExtendedPanel
{
    final ConsoleFrame cf;
    public final SearchField field;
    public final SearchPrefs prefs;
    public final FindButton find;
    public final KillButton kill;
    private int startIndex;
    private int endIndex;
    private String lastText;
    private boolean lastRegexp;
    
    SearchPanel(final ConsoleFrame cf) {
        this.cf = cf;
        this.field = new SearchField();
        this.prefs = new SearchPrefs();
        this.find = new FindButton();
        this.kill = new KillButton();
        final GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setAutoCreateContainerGaps(true);
        layout.setHorizontalGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.field).addComponent(this.prefs)).addGap(4).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.find, 48, 48, Integer.MAX_VALUE).addComponent(this.kill)));
        layout.linkSize(0, this.find, this.kill);
        layout.setVerticalGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.field).addComponent(this.find, 24, 24, Integer.MAX_VALUE)).addGap(2).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.prefs).addComponent(this.kill)));
        layout.linkSize(1, this.field, this.prefs, this.find, this.kill);
    }
    
    void search() {
    }
    
    private void focus() {
        this.field.requestFocusInWindow();
    }
    
    public class SearchField extends LocalizableInvalidateTextField
    {
        private SearchField() {
            super("console.search.placeholder");
            this.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    SearchPanel.this.search();
                }
            });
        }
    }
    
    public class SearchPrefs extends BorderPanel
    {
        public final LocalizableCheckbox regexp;
        
        private SearchPrefs() {
            (this.regexp = new LocalizableCheckbox("console.search.prefs.regexp")).setFocusable(false);
            SearchPanel.this.field.setFont(this.regexp.getFont());
            this.setWest(this.regexp);
        }
        
        public boolean getUseRegExp() {
            return this.regexp.isSelected();
        }
        
        public void setUseRegExp(final boolean use) {
            this.regexp.setSelected(use);
        }
    }
    
    public class FindButton extends ImageButton
    {
        private FindButton() {
            this.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    SearchPanel.this.search();
                }
            });
        }
    }
    
    public class KillButton extends ImageButton
    {
        private KillButton() {
        }
    }
    
    private class Range
    {
        private int start;
        private int end;
        
        Range(final int start, final int end) {
            this.start = start;
            this.end = end;
        }
        
        boolean isCorrect() {
            return this.start > 0 && this.end > this.start;
        }
    }
}
