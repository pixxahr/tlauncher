// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.console;

import org.tlauncher.tlauncher.ui.center.DefaultCenterPanelTheme;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.ui.center.CenterPanelTheme;
import org.tlauncher.tlauncher.ui.loc.LocalizableInvalidateTextField;

class SearchField extends LocalizableInvalidateTextField
{
    private static final long serialVersionUID = -6453744340240419870L;
    private static final CenterPanelTheme darkTheme;
    
    SearchField(final SearchPanel sp) {
        super("console.search.placeholder");
        if (OS.WINDOWS.isCurrent()) {
            this.setTheme(SearchField.darkTheme);
        }
        this.setText(null);
        this.setCaretColor(Color.white);
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                sp.search();
            }
        });
    }
    
    static {
        darkTheme = new DefaultCenterPanelTheme() {
            public final Color backgroundColor = new Color(0, 0, 0, 255);
            public final Color focusColor = new Color(255, 255, 255, 255);
            public final Color focusLostColor = new Color(128, 128, 128, 255);
            public final Color successColor = this.focusColor;
            
            @Override
            public Color getBackground() {
                return this.backgroundColor;
            }
            
            @Override
            public Color getFocus() {
                return this.focusColor;
            }
            
            @Override
            public Color getFocusLost() {
                return this.focusLostColor;
            }
            
            @Override
            public Color getSuccess() {
                return this.successColor;
            }
        };
    }
}
