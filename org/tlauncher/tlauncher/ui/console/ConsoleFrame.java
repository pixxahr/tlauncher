// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.ui.console;

import java.io.IOException;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.OS;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import org.tlauncher.util.FileUtil;
import java.io.File;
import javax.swing.JPopupMenu;
import javax.swing.text.JTextComponent;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import org.tlauncher.tlauncher.ui.swing.EmptyAction;
import org.tlauncher.tlauncher.ui.explorer.filters.CommonFilter;
import org.tlauncher.tlauncher.ui.explorer.filters.FilesAndExtentionFilter;
import org.tlauncher.tlauncher.rmo.TLauncher;
import javax.swing.Action;
import org.tlauncher.tlauncher.ui.explorer.FileChooser;
import org.tlauncher.tlauncher.ui.swing.TextPopup;
import java.awt.event.AdjustmentEvent;
import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.util.U;
import org.tlauncher.util.pastebin.Paste;
import java.awt.Container;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.AttributeSet;
import javax.swing.BoundedRangeModel;
import org.tlauncher.util.SwingUtil;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import javax.swing.border.Border;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.swing.ScrollPane;
import java.awt.event.MouseListener;
import javax.swing.text.DefaultCaret;
import java.awt.Color;
import java.awt.Font;
import org.tlauncher.tlauncher.ui.loc.LocalizableLabel;
import java.awt.Insets;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import org.tlauncher.tlauncher.ui.loc.LocalizableComponent;
import org.tlauncher.util.pastebin.PasteListener;
import javax.swing.JFrame;

public class ConsoleFrame extends JFrame implements PasteListener, LocalizableComponent
{
    public static final int MIN_WIDTH = 670;
    public static final int MIN_HEIGHT = 500;
    public final Console console;
    public final JTextArea textarea;
    public final JScrollBar vScrollbar;
    public final ConsoleFrameBottom bottom;
    public final ConsoleTextPopup popup;
    private int lastWindowWidth;
    private int scrollBarValue;
    private boolean scrollDown;
    private final Object busy;
    boolean hiding;
    
    ConsoleFrame(final Console console) {
        this.busy = new Object();
        this.console = console;
        (this.textarea = new JTextArea()).setLineWrap(true);
        this.textarea.setEditable(false);
        this.textarea.setAutoscrolls(true);
        this.textarea.setMargin(new Insets(0, 0, 0, 0));
        this.textarea.setFont(new Font("DialogInput", 0, (int)(new LocalizableLabel().getFont().getSize() * 1.2)));
        this.textarea.setForeground(Color.white);
        this.textarea.setCaretColor(Color.white);
        this.textarea.setBackground(Color.black);
        this.textarea.setSelectionColor(Color.gray);
        ((DefaultCaret)this.textarea.getCaret()).setUpdatePolicy(2);
        this.popup = new ConsoleTextPopup();
        this.textarea.addMouseListener(this.popup);
        final ScrollPane scrollPane = new ScrollPane(this.textarea);
        scrollPane.setBorder(null);
        scrollPane.setVBPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.vScrollbar = scrollPane.getVerticalScrollBar();
        final BoundedRangeModel vsbModel = this.vScrollbar.getModel();
        int nv;
        final BoundedRangeModel boundedRangeModel;
        this.vScrollbar.addAdjustmentListener(e -> {
            if (this.getWidth() != this.lastWindowWidth) {
                return;
            }
            else {
                nv = e.getValue();
                if (nv < this.scrollBarValue) {
                    this.scrollDown = false;
                }
                else if (nv == boundedRangeModel.getMaximum() - boundedRangeModel.getExtent()) {
                    this.scrollDown = true;
                }
                this.scrollBarValue = nv;
                return;
            }
        });
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(final ComponentEvent e) {
                ConsoleFrame.this.lastWindowWidth = ConsoleFrame.this.getWidth();
            }
        });
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(scrollPane, "Center");
        this.getContentPane().add(this.bottom = new ConsoleFrameBottom(this), "South");
        SwingUtil.setFavicons(this);
    }
    
    public void println(final String string) {
        this.print(string + '\n');
    }
    
    public void print(final String string) {
        synchronized (this.busy) {
            final Document document = this.textarea.getDocument();
            try {
                document.insertString(document.getLength(), string, null);
            }
            catch (Throwable t) {}
            if (this.scrollDown) {
                this.scrollDown();
            }
        }
    }
    
    public void clear() {
        this.textarea.setText("");
    }
    
    public void scrollDown() {
        SwingUtilities.invokeLater(() -> this.vScrollbar.setValue(this.vScrollbar.getMaximum()));
    }
    
    @Override
    public void updateLocale() {
        Localizable.updateContainer(this);
    }
    
    @Override
    public void pasteUploading(final Paste paste) {
        this.bottom.pastebin.setEnabled(false);
        this.popup.pastebinAction.setEnabled(false);
    }
    
    @Override
    public void pasteDone(final Paste paste) {
        this.bottom.pastebin.setEnabled(true);
        this.popup.pastebinAction.setEnabled(true);
    }
    
    void hideIn(final long millis) {
        this.hiding = true;
        this.bottom.closeCancelButton.setVisible(true);
        this.bottom.closeCancelButton.setText("console.close.cancel", millis / 1000L);
        AsyncThread.execute(new Runnable() {
            long remaining = millis;
            
            @Override
            public void run() {
                ConsoleFrame.this.bottom.closeCancelButton.setText("console.close.cancel", this.remaining / 1000L);
                while (ConsoleFrame.this.hiding && this.remaining > 1999L) {
                    this.remaining -= 1000L;
                    ConsoleFrame.this.bottom.closeCancelButton.setText("console.close.cancel", this.remaining / 1000L);
                    U.sleepFor(1000L);
                }
                if (ConsoleFrame.this.hiding) {
                    ConsoleFrame.this.dispose();
                }
            }
        });
    }
    
    public class ConsoleTextPopup extends TextPopup
    {
        private final FileChooser explorer;
        private final Action saveAllAction;
        private final Action pastebinAction;
        private final Action clearAllAction;
        
        ConsoleTextPopup() {
            (this.explorer = (FileChooser)TLauncher.getInjector().getInstance((Class)FileChooser.class)).setFileFilter(new FilesAndExtentionFilter("log", new String[] { "log" }));
            this.saveAllAction = new EmptyAction() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    ConsoleTextPopup.this.onSavingCalled();
                }
            };
            this.pastebinAction = new EmptyAction() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    ConsoleFrame.this.console.sendPaste();
                }
            };
            this.clearAllAction = new EmptyAction() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    ConsoleTextPopup.this.onClearCalled();
                }
            };
        }
        
        @Override
        protected JPopupMenu getPopup(final MouseEvent e, final JTextComponent comp) {
            final JPopupMenu menu = super.getPopup(e, comp);
            if (menu == null) {
                return null;
            }
            menu.addSeparator();
            menu.add(this.saveAllAction).setText(Localizable.get("console.save.popup"));
            menu.add(this.pastebinAction).setText(Localizable.get("console.pastebin"));
            menu.addSeparator();
            menu.add(this.clearAllAction).setText(Localizable.get("console.clear.popup"));
            return menu;
        }
        
        protected void onSavingCalled() {
            this.explorer.setSelectedFile(new File(ConsoleFrame.this.console.getName() + ".log"));
            final int result = this.explorer.showSaveDialog(ConsoleFrame.this.console.frame);
            if (result != 0) {
                return;
            }
            File file = this.explorer.getSelectedFile();
            if (file == null) {
                U.log("Returned NULL. Damn it!");
                return;
            }
            String path = file.getAbsolutePath();
            if (!path.endsWith(".log")) {
                path += ".log";
            }
            file = new File(path);
            OutputStream output = null;
            try {
                FileUtil.createFile(file);
                final StringReader input = new StringReader(ConsoleFrame.this.console.getOutput());
                output = new BufferedOutputStream(new FileOutputStream(file));
                int current;
                while ((current = input.read()) != -1) {
                    if (current == 10 && OS.WINDOWS.isCurrent()) {
                        output.write(13);
                    }
                    output.write(current);
                }
                output.close();
            }
            catch (Throwable throwable) {
                Alert.showLocError("console.save.error", throwable);
                if (output != null) {
                    try {
                        output.close();
                    }
                    catch (IOException ignored) {
                        ignored.printStackTrace();
                    }
                }
            }
            finally {
                if (output != null) {
                    try {
                        output.close();
                    }
                    catch (IOException ignored2) {
                        ignored2.printStackTrace();
                    }
                }
            }
        }
        
        protected void onClearCalled() {
            ConsoleFrame.this.console.clear();
        }
    }
}
