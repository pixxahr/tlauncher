// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.repository;

import java.util.Objects;
import java.io.IOException;
import java.net.URL;
import net.minecraft.launcher.Http;
import org.tlauncher.util.Time;
import java.util.function.Predicate;
import java.util.Collection;
import java.util.Collections;
import org.tlauncher.util.U;
import java.util.ArrayList;
import java.util.List;

public class Repo
{
    private static final List<String> notShowLogs;
    private final String name;
    private final List<String> repos;
    private int primaryTimeout;
    private int selected;
    private boolean isSelected;
    
    public Repo(final String[] urls, final String name) {
        if (urls == null) {
            throw new NullPointerException("URL array is NULL!");
        }
        this.name = name;
        this.repos = new ArrayList<String>();
        this.primaryTimeout = U.getConnectionTimeout();
        Collections.addAll(this.repos, urls);
    }
    
    private String getUrl0(final String uri) throws IOException {
        final boolean canSelect = this.isSelectable();
        if (!canSelect) {
            return this.getRawUrl(uri);
        }
        final boolean gotError = false;
        final Object lock = new Object();
        IOException e = null;
        int i = 0;
        int attempt = 0;
        while (i < 3) {
            ++i;
            final int timeout = this.primaryTimeout * i;
            int x = 0;
            while (x < this.getCount()) {
                ++attempt;
                final String url = this.getRepo(x);
                if (Repo.notShowLogs.stream().noneMatch((Predicate<? super Object>)url::endsWith)) {
                    this.log("Attempt #" + attempt + "; timeout: " + timeout + " ms; url: " + url);
                }
                Time.start(lock);
                try {
                    final String result = Http.performGet(new URL(url + Http.encode(uri)), timeout, timeout);
                    this.setSelected(x);
                    this.log("Success: Reached the repo in", Time.stop(lock), "ms.");
                    return result;
                }
                catch (IOException e2) {
                    if (Repo.notShowLogs.stream().noneMatch((Predicate<? super Object>)url::endsWith)) {
                        this.log("request to url = " + url + uri);
                    }
                    this.log("Failed: Repo is not reachable!", e2);
                    e = e2;
                    Time.stop(lock);
                    ++x;
                    continue;
                }
                break;
            }
        }
        this.log("Failed: All repos are unreachable.");
        throw Objects.requireNonNull(e);
    }
    
    public String getUrl(final String uri) throws IOException {
        return this.getUrl0(uri);
    }
    
    public String getUrl() throws IOException {
        return this.getUrl0("");
    }
    
    private String getRawUrl(final String uri) throws IOException {
        final String url = this.getSelectedRepo() + Http.encode(uri);
        try {
            return Http.performGet(new URL(url));
        }
        catch (IOException e) {
            this.log("Cannot get raw:", url);
            throw e;
        }
    }
    
    public int getSelected() {
        return this.selected;
    }
    
    public synchronized void selectNext() {
        if (++this.selected >= this.getCount()) {
            this.selected = 0;
        }
    }
    
    public String getSelectedRepo() {
        return this.repos.get(this.selected);
    }
    
    private String getRepo(final int pos) {
        return this.repos.get(pos);
    }
    
    public List<String> getList() {
        return this.repos;
    }
    
    public int getCount() {
        return this.repos.size();
    }
    
    boolean isSelected() {
        return this.isSelected;
    }
    
    void setSelected(final int pos) {
        if (!this.isSelectable()) {
            throw new IllegalStateException();
        }
        this.isSelected = true;
        this.selected = pos;
    }
    
    public boolean isSelectable() {
        return !this.repos.isEmpty();
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    private void log(final Object... obj) {
        U.log("[REPO][" + this.name + "]", obj);
    }
    
    public String getName() {
        return this.name;
    }
    
    static {
        notShowLogs = new ArrayList<String>() {
            {
                this.add("inner_servers.json");
            }
        };
    }
}
