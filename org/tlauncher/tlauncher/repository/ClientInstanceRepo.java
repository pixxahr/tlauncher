// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.repository;

import java.util.ArrayList;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Optional;
import java.util.List;

public class ClientInstanceRepo
{
    public static final Repo LOCAL_VERSION_REPO;
    public static final Repo OFFICIAL_VERSION_REPO;
    public static final Repo EMPTY_REPO;
    public static final Repo EXTRA_VERSION_REPO;
    public static final Repo ASSETS_REPO;
    public static final Repo LIBRARY_REPO;
    public static final Repo SKIN_VERSION_REPO;
    public static final Repo HOT_SERVERS_REPO;
    public static final Repo ADD_HOT_SERVERS_REPO;
    public static final Repo SERVER_LIST_REPO;
    private static final List<Repo> LIST;
    
    public static Repo find(final String name) {
        if (name.isEmpty()) {
            return ClientInstanceRepo.EMPTY_REPO;
        }
        final Optional<Repo> repo = ClientInstanceRepo.LIST.stream().filter(r -> r.getName().equalsIgnoreCase(name)).findFirst();
        if (repo.isPresent()) {
            return repo.get();
        }
        throw new RuntimeException("can't find proper repo " + name);
    }
    
    public static Repo createModpackRepo() {
        return new Repo(TLauncher.getInnerSettings().getArrayShuffle("modpack.repo"), "MODPACK_REPO");
    }
    
    static {
        LOCAL_VERSION_REPO = new Repo(new String[0], "LOCAL_VERSION_REPO");
        OFFICIAL_VERSION_REPO = new Repo(TLauncher.getInnerSettings().getArray("official.repo"), "OFFICIAL_VERSION_REPO");
        EMPTY_REPO = new Repo(TLauncher.getInnerSettings().getArray("empty.repositories"), "EMPTY");
        EXTRA_VERSION_REPO = new Repo(TLauncher.getInnerSettings().getArray("tlauncher.versions.repo"), "EXTRA_VERSION_REPO");
        ASSETS_REPO = new Repo(TLauncher.getInnerSettings().getArray("assets.repo"), "ASSETS_REPO");
        LIBRARY_REPO = new Repo(TLauncher.getInnerSettings().getArray("library.repo"), "LIBRARY_REPO");
        SKIN_VERSION_REPO = new Repo(TLauncher.getInnerSettings().getArray("skin.extra.repo"), "SKIN_VERSION_REPO");
        HOT_SERVERS_REPO = new Repo(TLauncher.getInnerSettings().getArray("hot.servers"), "HOT_SERVERS_REPO");
        ADD_HOT_SERVERS_REPO = new Repo(TLauncher.getInnerSettings().getArray("add.hot.servers"), "ADD_HOT_SERVERS_REPO");
        SERVER_LIST_REPO = new Repo(new String[] { "http://repo.tlauncher.org/update/downloads/configs/inner_servers.json", "https://tlauncher.org/repo/update/downloads/configs/inner_servers.json", "http://advancedrepository.com/update/downloads/configs/inner_servers.json" }, "SERVER_LIST_REPO");
        LIST = new ArrayList<Repo>() {
            {
                this.add(ClientInstanceRepo.LOCAL_VERSION_REPO);
                this.add(ClientInstanceRepo.OFFICIAL_VERSION_REPO);
                this.add(ClientInstanceRepo.EXTRA_VERSION_REPO);
                this.add(ClientInstanceRepo.SKIN_VERSION_REPO);
            }
        };
    }
}
