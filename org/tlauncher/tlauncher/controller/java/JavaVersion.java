// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.controller.java;

import java.util.List;
import org.tlauncher.util.OS;
import java.util.Map;

public class JavaVersion
{
    private Map<OS.Arch, Map<OS, List<String>>> map;
    
    public Map<OS.Arch, Map<OS, List<String>>> getMap() {
        return this.map;
    }
    
    public void setMap(final Map<OS.Arch, Map<OS, List<String>>> map) {
        this.map = map;
    }
    
    public List<String> getProperUrl() throws NotIdentifiedSystem {
        final Map<OS, List<String>> res = this.map.get(OS.Arch.CURRENT);
        if (res == null) {
            throw new NotIdentifiedSystem("coudn't find" + OS.Arch.CURRENT);
        }
        return res.get(OS.CURRENT);
    }
}
