// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.controller.modpack;

import java.io.IOException;
import net.minecraft.launcher.Http;
import java.net.URL;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.configuration.InnerConfiguration;

public class FullGameEntityController
{
    private InnerConfiguration innerConfiguration;
    
    public FullGameEntityController() {
        TLauncher.getInstance();
        this.innerConfiguration = TLauncher.getInnerSettings();
    }
    
    public void getEnDescription(final GameEntityDTO en) throws IOException {
        final String request = this.innerConfiguration.get("modpack.url") + "description/en/" + en.getId();
        final String result = Http.performGet(new URL(request), this.innerConfiguration.getInteger("modpack.update.time.connect"), this.innerConfiguration.getInteger("modpack.update.time.connect"));
        en.setEnDescription(result);
    }
}
