// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.controller;

import java.io.IOException;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.ui.alert.Alert;
import net.minecraft.launcher.Http;
import java.net.URL;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.modpack.domain.client.AddedGameEntityDTO;
import javax.inject.Named;
import javax.inject.Inject;
import com.google.gson.Gson;

public class AddedModpackStuffController
{
    @Inject
    @Named("GsonCompleteVersion")
    private Gson gson;
    
    public void send(final String link) {
        final AddedGameEntityDTO en = new AddedGameEntityDTO();
        en.setUrl(link);
        try {
            final URL url = new URL(TLauncher.getInnerSettings().get("modpack.operation.url") + ModpackManager.ModpackServerCommand.ADD_NEW_GAME_ENTITY.toString().toLowerCase());
            Http.performPost(url, this.gson.toJson(en), "application/json");
            Alert.showLocMessage("modpack.send.success");
        }
        catch (IOException e) {
            Alert.showMonologError(Localizable.get().get("modpack.error.send.unsuccess"), 0);
            U.log(e);
        }
    }
}
