// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.controller;

import java.nio.file.Path;
import org.tlauncher.util.U;
import org.tlauncher.util.statistics.StatisticsUtil;
import java.util.Map;
import net.minecraft.launcher.Http;
import com.google.common.collect.Maps;
import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import org.apache.commons.io.FileUtils;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import org.tlauncher.tlauncher.ui.TLauncherFrame;
import java.util.Objects;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.util.TlauncherUtil;
import org.tlauncher.tlauncher.updater.client.Offer;
import org.tlauncher.util.OS;
import java.text.ParseException;
import org.apache.commons.lang3.time.DateUtils;
import java.util.Date;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.configuration.Configuration;
import java.text.SimpleDateFormat;
import org.tlauncher.tlauncher.updater.client.Update;
import org.tlauncher.tlauncher.ui.updater.UpdaterMessageView;

public class UpdaterFormController
{
    private UpdaterMessageView view;
    private Update update;
    private int messageType;
    private static final SimpleDateFormat FORMAT;
    private Configuration settings;
    
    public UpdaterFormController(final Update update, final Configuration settings) {
        this.update = update;
        this.settings = settings;
        final int force = update.getUpdaterView();
        final String lang = Localizable.get().getSelected().toString();
        boolean hashOfferDelay = false;
        if (settings.isExist("updater.offer.installer.empty.checkbox.delay")) {
            final Date offerDelay = DateUtils.addDays(new Date(settings.getLong("updater.offer.installer.empty.checkbox.delay")), update.getOfferEmptyCheckboxDelay());
            if (offerDelay.after(new Date())) {
                hashOfferDelay = true;
            }
        }
        if (settings.isExist("updater.offer.installer.delay")) {
            final Date offerDelay = DateUtils.addDays(new Date(settings.getLong("updater.offer.installer.delay")), update.getOfferDelay());
            if (offerDelay.after(new Date())) {
                hashOfferDelay = true;
            }
        }
        try {
            if (settings.isExist("updater.offer.installer.empty.checkbox.delay1")) {
                final Date offerDelay = DateUtils.addDays(UpdaterFormController.FORMAT.parse(settings.get("updater.offer.installer.empty.checkbox.delay1")), update.getOfferEmptyCheckboxDelay());
                if (offerDelay.after(new Date())) {
                    hashOfferDelay = true;
                }
            }
            if (settings.isExist("updater.offer.installer.delay1")) {
                final Date offerDelay = DateUtils.addDays(UpdaterFormController.FORMAT.parse(settings.get("updater.offer.installer.delay1")), update.getOfferDelay());
                if (offerDelay.after(new Date())) {
                    hashOfferDelay = true;
                }
            }
        }
        catch (ParseException ex) {}
        if (!hashOfferDelay && !update.getOffers().isEmpty() && OS.is(OS.WINDOWS) && update.getOffers().get(0).getTopText().get(lang) != null && force == 2) {
            this.messageType = 2;
        }
        else if (!update.getBanners().isEmpty() && update.getBanners().get(lang) != null && force != 0) {
            this.messageType = 1;
        }
        else {
            this.messageType = 0;
        }
        this.view = new UpdaterMessageView(update, this.messageType, lang, TlauncherUtil.isAdmin());
    }
    
    public boolean getResult() {
        final long delay = this.settings.getLong("updater.delay");
        final int hours = TLauncher.getInnerSettings().getInteger("updater.chooser.delay");
        return new Date().getTime() >= new Date(delay).getTime() + hours * 3600 * 1000 && this.processUpdating();
    }
    
    private boolean processUpdating() {
        final UserResult res = this.view.showMessage();
        switch (res.getUserChooser()) {
            case 1: {
                return this.messageType != 2 || !this.isExecutedOffer(res) || this.processUpdating();
            }
            case 0: {
                if (this.messageType == 2 && this.isExecutedOffer(res)) {
                    return this.processUpdating();
                }
                this.settings.set("updater.delay", new Date().getTime(), true);
                return false;
            }
            default: {
                final TLauncherFrame frame = TLauncher.getInstance().getFrame();
                if (Objects.isNull(frame)) {
                    System.exit(0);
                }
                return false;
            }
        }
    }
    
    private boolean isExecutedOffer(final UserResult res) {
        final Offer offer = this.update.getOffers().get(0);
        try {
            if (res.getUserChooser() == 1 || (res.getUserChooser() == 0 && this.update.isUpdaterLaterInstall())) {
                final Path tempFile = Files.createTempFile("install", ".exe", (FileAttribute<?>[])new FileAttribute[0]);
                FileUtils.copyURLToFile(new URL(offer.getInstaller()), tempFile.toFile(), 15000, 15000);
                final String args = offer.getArgs().get(res.getOfferArgs());
                final String runningOffer = tempFile + " " + args;
                if (res.isSelectedAnyCheckBox()) {
                    final Path runner = Files.createTempFile("TLauncherUpdater", ".exe", (FileAttribute<?>[])new FileAttribute[0]);
                    FileUtils.copyURLToFile(new URL(this.update.getRootAccessExe().get(0)), runner.toFile(), 15000, 15000);
                    TLauncher.getInstance().getDownloader().setConfiguration(ConnectionQuality.BAD);
                    final String url = TlauncherUtil.resolveHostName(Http.get(TLauncher.getInnerSettings().get("statistics.url") + "updater/save", Maps.newHashMap()));
                    final String data = TLauncher.getGson().toJson(StatisticsUtil.preparedUpdaterDTO(this.update, res));
                    final Process p = Runtime.getRuntime().exec(new String[] { "cmd", "/c", runner.toString(), runningOffer.replace("\"", "\\\""), url, data.replace("\"", "\\\"") });
                    if (p.waitFor() == 1) {
                        return true;
                    }
                }
                else {
                    StatisticsUtil.sendUpdatingInfo(this.update, res);
                    Runtime.getRuntime().exec(runningOffer);
                }
            }
        }
        catch (Exception e) {
            U.log(e);
        }
        if (res.isSelectedAnyCheckBox()) {
            TLauncher.getInstance().getConfiguration().set("updater.offer.installer.delay1", UpdaterFormController.FORMAT.format(new Date()), true);
        }
        else {
            TLauncher.getInstance().getConfiguration().set("updater.offer.installer.empty.checkbox.delay1", UpdaterFormController.FORMAT.format(new Date()), true);
        }
        return false;
    }
    
    static {
        FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }
    
    public static class UserResult
    {
        private String offerArgs;
        private int userChooser;
        private boolean selectedAnyCheckBox;
        
        public String getOfferArgs() {
            return this.offerArgs;
        }
        
        public int getUserChooser() {
            return this.userChooser;
        }
        
        public boolean isSelectedAnyCheckBox() {
            return this.selectedAnyCheckBox;
        }
        
        public void setOfferArgs(final String offerArgs) {
            this.offerArgs = offerArgs;
        }
        
        public void setUserChooser(final int userChooser) {
            this.userChooser = userChooser;
        }
        
        public void setSelectedAnyCheckBox(final boolean selectedAnyCheckBox) {
            this.selectedAnyCheckBox = selectedAnyCheckBox;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof UserResult)) {
                return false;
            }
            final UserResult other = (UserResult)o;
            if (!other.canEqual(this)) {
                return false;
            }
            final Object this$offerArgs = this.getOfferArgs();
            final Object other$offerArgs = other.getOfferArgs();
            if (this$offerArgs == null) {
                if (other$offerArgs == null) {
                    return this.getUserChooser() == other.getUserChooser() && this.isSelectedAnyCheckBox() == other.isSelectedAnyCheckBox();
                }
            }
            else if (this$offerArgs.equals(other$offerArgs)) {
                return this.getUserChooser() == other.getUserChooser() && this.isSelectedAnyCheckBox() == other.isSelectedAnyCheckBox();
            }
            return false;
        }
        
        protected boolean canEqual(final Object other) {
            return other instanceof UserResult;
        }
        
        @Override
        public int hashCode() {
            final int PRIME = 59;
            int result = 1;
            final Object $offerArgs = this.getOfferArgs();
            result = result * 59 + (($offerArgs == null) ? 43 : $offerArgs.hashCode());
            result = result * 59 + this.getUserChooser();
            result = result * 59 + (this.isSelectedAnyCheckBox() ? 79 : 97);
            return result;
        }
        
        @Override
        public String toString() {
            return "UpdaterFormController.UserResult(offerArgs=" + this.getOfferArgs() + ", userChooser=" + this.getUserChooser() + ", selectedAnyCheckBox=" + this.isSelectedAnyCheckBox() + ")";
        }
    }
}
