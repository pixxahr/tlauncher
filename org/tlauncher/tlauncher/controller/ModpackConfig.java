// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.controller;

import java.io.IOException;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.ModDTO;
import org.tlauncher.tlauncher.ui.alert.Alert;
import net.minecraft.launcher.versions.CompleteVersion;
import java.util.Iterator;
import java.util.Comparator;
import org.tlauncher.modpack.domain.client.share.ForgeStringComparator;
import org.tlauncher.modpack.domain.client.GameVersionDTO;
import com.google.inject.Inject;
import org.tlauncher.tlauncher.managers.ModpackManager;

public class ModpackConfig
{
    @Inject
    private ModpackManager manager;
    
    public GameVersionDTO findGameVersion(final String currentGameVersion) {
        for (final GameVersionDTO dto : this.manager.getInfoMod().getGameVersions()) {
            if (dto.getName().equalsIgnoreCase(currentGameVersion) && dto.getForgeVersions().size() > 0) {
                dto.getForgeVersions().sort(new ForgeStringComparator());
                return dto;
            }
        }
        return null;
    }
    
    public void installSkinMod(final CompleteVersion version) {
        if (this.manager.getInfoMod().getMods().isEmpty()) {
            Alert.showLocMessage("modpack.internet.update");
            return;
        }
        final Long id = this.manager.getSkinId(version.getModpack());
        for (final ModDTO m : this.manager.getInfoMod().getMods()) {
            if (m.getId().equals(id)) {
                for (final VersionDTO v : m.getVersions()) {
                    if (v.getGameVersions().contains(((ModpackVersionDTO)version.getModpack().getVersion()).getGameVersion())) {
                        this.manager.installEntity(m, v, GameType.MOD);
                    }
                }
            }
        }
    }
    
    public void save(final CompleteVersion version, final String modpackName, final boolean optifine, final String forge) {
        if (!version.getID().equals(modpackName)) {
            this.manager.renameModpack(version, modpackName);
        }
        else {
            this.manager.resaveVersion(version);
        }
        if (ModpackUtil.useSkinMod(version) && !optifine) {
            final Long id = this.manager.getSkinId(version.getModpack());
            final ModDTO m = new ModDTO();
            m.setId(id);
            this.manager.removeEntity(m, m.getVersion(), GameType.MOD, true);
        }
        else if (!ModpackUtil.useSkinMod(version) && optifine) {
            this.installSkinMod(version);
        }
        if (!((ModpackVersionDTO)version.getModpack().getVersion()).getForgeVersion().equalsIgnoreCase(forge)) {
            try {
                final CompleteVersion v = this.manager.getForgeVersion(forge);
                v.setSkinVersion(version.isSkinVersion());
                v.setID(version.getID());
                v.setModpackDTO(version.getModpack());
                ((ModpackVersionDTO)v.getModpack().getVersion()).setForgeVersion(forge);
                this.manager.resaveVersion(v);
            }
            catch (IOException e) {
                U.log(e);
            }
        }
    }
    
    public void open(final CompleteVersion version) {
        this.manager.openModpackFolder(version);
    }
    
    public void remove(final CompleteVersion version) {
        this.manager.removeEntity(version.getModpack(), version.getModpack().getVersion(), GameType.MODPACK, false);
    }
}
