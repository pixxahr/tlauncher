// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.component;

import org.tlauncher.util.U;
import org.tlauncher.tlauncher.managers.ComponentManager;

public abstract class LauncherComponent
{
    protected final ComponentManager manager;
    
    public LauncherComponent(final ComponentManager manager) throws Exception {
        if (manager == null) {
            throw new NullPointerException();
        }
        this.manager = manager;
    }
    
    protected void log(final Object... w) {
        U.log("[" + this.getClass().getSimpleName() + "]", w);
    }
}
