// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.component;

import org.tlauncher.tlauncher.managers.ComponentManager;
import java.util.concurrent.Semaphore;

public abstract class InterruptibleComponent extends RefreshableComponent
{
    protected final boolean[] refreshList;
    private int lastRefreshID;
    protected final Semaphore semaphore;
    protected boolean lastResult;
    
    protected InterruptibleComponent(final ComponentManager manager) throws Exception {
        this(manager, 64);
    }
    
    private InterruptibleComponent(final ComponentManager manager, final int listSize) throws Exception {
        super(manager);
        this.semaphore = new Semaphore(1);
        if (listSize < 1) {
            throw new IllegalArgumentException("Invalid list size: " + listSize + " < 1");
        }
        this.refreshList = new boolean[listSize];
    }
    
    public final boolean refresh() {
        if (this.semaphore.tryAcquire()) {
            try {
                return this.lastResult = this.refresh(this.nextID());
            }
            finally {
                this.semaphore.release();
            }
        }
        try {
            this.semaphore.acquire();
            return this.lastResult;
        }
        catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
        finally {
            this.semaphore.release();
        }
    }
    
    public synchronized void stopRefresh() {
        for (int i = 0; i < this.refreshList.length; ++i) {
            this.refreshList[i] = false;
        }
    }
    
    protected synchronized int nextID() {
        final int listSize = this.refreshList.length;
        int next = this.lastRefreshID++;
        if (next >= listSize) {
            next = 0;
        }
        return this.lastRefreshID = next;
    }
    
    protected boolean isCancelled(final int refreshID) {
        return !this.refreshList[refreshID];
    }
    
    protected abstract boolean refresh(final int p0);
}
