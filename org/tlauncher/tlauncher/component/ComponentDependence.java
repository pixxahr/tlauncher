// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.component;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Inherited;
import java.lang.annotation.Documented;
import java.lang.annotation.Annotation;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface ComponentDependence {
    Class<?>[] value();
}
