// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.component;

import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.tlauncher.managers.ComponentManager;

public abstract class RefreshableComponent extends LauncherComponent
{
    public RefreshableComponent(final ComponentManager manager) throws Exception {
        super(manager);
    }
    
    public boolean refreshComponent() {
        return this.refresh();
    }
    
    public void asyncRefresh() {
        AsyncThread.execute(this::refresh);
    }
    
    protected abstract boolean refresh();
}
