// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration;

import java.util.Iterator;
import java.util.HashMap;
import java.io.IOException;
import joptsimple.OptionException;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.io.OutputStream;
import joptsimple.OptionSet;
import joptsimple.OptionParser;
import java.util.Map;

public class ArgumentParser
{
    private static Map<String, String> m;
    private static OptionParser parser;
    
    public static OptionParser getParser() {
        return ArgumentParser.parser;
    }
    
    public static OptionSet parseArgs(final String[] args) throws IOException {
        OptionSet set = null;
        try {
            set = ArgumentParser.parser.parse(args);
        }
        catch (OptionException e) {
            e.printStackTrace();
            ArgumentParser.parser.printHelpOn((OutputStream)System.out);
            Alert.showError(e, false);
        }
        return set;
    }
    
    public static Map<String, Object> parse(final OptionSet set) {
        final Map<String, Object> r = new HashMap<String, Object>();
        if (set == null) {
            return r;
        }
        for (final Map.Entry<String, String> a : ArgumentParser.m.entrySet()) {
            String key = a.getKey();
            Object value = null;
            if (key.startsWith("-")) {
                key = key.substring(1);
                value = true;
            }
            if (!set.has(key)) {
                continue;
            }
            if (value == null) {
                value = set.valueOf(key);
            }
            r.put(a.getValue(), value);
        }
        return r;
    }
    
    private static Map<String, String> createLinkMap() {
        final Map<String, String> r = new HashMap<String, String>();
        r.put("directory", "minecraft.gamedir");
        r.put("java-directory", "minecraft.javadir");
        r.put("version", "login.version.game");
        r.put("username", "login.account");
        r.put("usertype", "login.account.type");
        r.put("javaargs", "minecraft.javaargs");
        r.put("margs", "minecraft.args");
        r.put("window", "minecraft.size");
        r.put("background", "gui.background");
        r.put("fullscreen", "minecraft.fullscreen");
        return r;
    }
    
    private static OptionParser createParser() {
        final OptionParser parser = new OptionParser();
        parser.accepts("help", "Shows this help");
        parser.accepts("nogui", "Starts minimal version");
        parser.accepts("directory", "Specifies Minecraft directory").withRequiredArg();
        parser.accepts("java-directory", "Specifies Java directory").withRequiredArg();
        parser.accepts("version", "Specifies version to run").withRequiredArg();
        parser.accepts("username", "Specifies username").withRequiredArg();
        parser.accepts("usertype", "Specifies user type (if multiple with the same name)").withRequiredArg();
        parser.accepts("javaargs", "Specifies JVM arguments").withRequiredArg();
        parser.accepts("margs", "Specifies Minecraft arguments").withRequiredArg();
        parser.accepts("window", "Specifies window size in format: width;height").withRequiredArg();
        parser.accepts("settings", "Specifies path to settings file").withRequiredArg();
        parser.accepts("background", "Specifies background image. URL links, JPEG and PNG formats are supported.").withRequiredArg();
        parser.accepts("fullscreen", "Specifies whether fullscreen mode enabled or not").withRequiredArg();
        return parser;
    }
    
    static {
        ArgumentParser.m = createLinkMap();
        ArgumentParser.parser = createParser();
    }
}
