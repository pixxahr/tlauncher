// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration;

import org.tlauncher.tlauncher.entity.ConfigEnum;
import org.tlauncher.util.OS;
import java.util.Properties;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.util.UUID;
import java.net.Proxy;
import org.tlauncher.util.Reflect;
import org.tlauncher.util.Direction;
import java.util.Iterator;
import net.minecraft.launcher.versions.ReleaseType;
import net.minecraft.launcher.updater.VersionFilter;
import org.tlauncher.util.IntegerArray;
import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import org.tlauncher.tlauncher.configuration.enums.ConsoleType;
import org.tlauncher.tlauncher.configuration.enums.ActionOnLaunch;
import java.util.ArrayList;
import org.tlauncher.util.FileUtil;
import org.tlauncher.util.U;
import org.tlauncher.util.MinecraftUtil;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.io.File;
import java.io.IOException;
import joptsimple.OptionSet;
import java.net.URL;
import java.util.Locale;
import java.util.List;
import java.util.Map;

public class Configuration extends SimpleConfiguration
{
    private ConfigurationDefaults defaults;
    private Map<String, Object> constants;
    private List<Locale> defaultLocales;
    private List<Locale> supportedLocales;
    private boolean firstRun;
    
    private Configuration(final URL url, final OptionSet set) throws IOException {
        super(url);
        this.init(set);
    }
    
    private Configuration(final File file, final OptionSet set) {
        super(file);
        this.init(set);
    }
    
    public static Configuration createConfiguration(final OptionSet set) throws IOException {
        final Object path = (set != null) ? set.valueOf("settings") : null;
        final InnerConfiguration inner = TLauncher.getInnerSettings();
        File file;
        if (path == null) {
            file = MinecraftUtil.getSystemRelatedDirectory(inner.get("settings.new"));
        }
        else {
            file = new File(path.toString());
        }
        final boolean doesntExist = !file.isFile();
        if (doesntExist) {
            U.log("Creating configuration file...");
            FileUtil.createFile(file);
        }
        U.log("Loading configuration from file:", file);
        final Configuration config = new Configuration(file, set);
        config.firstRun = doesntExist;
        return config;
    }
    
    public static Configuration createConfiguration() throws IOException {
        return createConfiguration(null);
    }
    
    public static List<Locale> getDefaultLocales(final SimpleConfiguration simpleConfiguration) {
        final List<Locale> l = new ArrayList<Locale>();
        final String[] split;
        final String[] languages = split = simpleConfiguration.get("languages").split(",");
        for (final String string : split) {
            l.add(getLocaleOf(string));
        }
        return l;
    }
    
    public boolean isFirstRun() {
        return this.firstRun;
    }
    
    public boolean isSaveable(final String key) {
        return !this.constants.containsKey(key);
    }
    
    public Locale getLocale() {
        return getLocaleOf(this.get("locale"));
    }
    
    public Locale[] getLocales() {
        final Locale[] locales = new Locale[this.supportedLocales.size()];
        return this.supportedLocales.toArray(locales);
    }
    
    public ActionOnLaunch getActionOnLaunch() {
        return ActionOnLaunch.get(this.get("minecraft.onlaunch"));
    }
    
    public ConsoleType getConsoleType() {
        return ConsoleType.get(this.get("gui.console"));
    }
    
    public ConnectionQuality getConnectionQuality() {
        return ConnectionQuality.get(this.get("connection"));
    }
    
    public int[] getClientWindowSize() {
        final String plainValue = this.get("minecraft.size");
        final int[] value = new int[2];
        if (plainValue == null) {
            return new int[2];
        }
        try {
            final IntegerArray arr = IntegerArray.parseIntegerArray(plainValue);
            value[0] = arr.get(0);
            value[1] = arr.get(1);
        }
        catch (Exception ex) {}
        return value;
    }
    
    public int[] getLauncherWindowSize() {
        final String plainValue = this.get("gui.size");
        final int[] value = new int[2];
        if (plainValue == null) {
            return new int[2];
        }
        try {
            final IntegerArray arr = IntegerArray.parseIntegerArray(plainValue);
            value[0] = arr.get(0);
            value[1] = arr.get(1);
        }
        catch (Exception ex) {}
        return value;
    }
    
    public int[] getDefaultClientWindowSize() {
        final String plainValue = this.getDefault("minecraft.size");
        return IntegerArray.parseIntegerArray(plainValue).toArray();
    }
    
    public int[] getDefaultLauncherWindowSize() {
        final String plainValue = this.getDefault("gui.size");
        return IntegerArray.parseIntegerArray(plainValue).toArray();
    }
    
    public VersionFilter getVersionFilter() {
        final VersionFilter filter = new VersionFilter();
        for (final ReleaseType type : ReleaseType.getDefinable()) {
            final boolean include = this.getBoolean("minecraft.versions." + type);
            if (!include) {
                filter.exclude(type);
            }
        }
        for (final ReleaseType.SubType type2 : ReleaseType.SubType.values()) {
            final boolean include2 = this.getBoolean("minecraft.versions.sub." + type2);
            if (!include2) {
                filter.exclude(type2);
            }
        }
        return filter;
    }
    
    public Direction getDirection(final String key) {
        return Reflect.parseEnum(Direction.class, this.get(key));
    }
    
    public Proxy getProxy() {
        return Proxy.NO_PROXY;
    }
    
    public UUID getClient() {
        try {
            return UUID.fromString(this.get("client"));
        }
        catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public String getDefault(final String key) {
        return this.getStringOf(this.defaults.get(key));
    }
    
    @Override
    public int getDefaultInteger(final String key) {
        return this.getIntegerOf(this.defaults.get(key), 0);
    }
    
    @Override
    public double getDefaultDouble(final String key) {
        return this.getDoubleOf(this.defaults.get(key), 0.0);
    }
    
    @Override
    public float getDefaultFloat(final String key) {
        return this.getFloatOf(this.defaults.get(key), 0.0f);
    }
    
    @Override
    public long getDefaultLong(final String key) {
        return this.getLongOf(this.defaults.get(key), 0L);
    }
    
    @Override
    public boolean getDefaultBoolean(final String key) {
        return this.getBooleanOf(this.defaults.get(key), false);
    }
    
    @Override
    public void set(final String key, final Object value, final boolean flush) {
        if (this.constants.containsKey(key)) {
            return;
        }
        super.set(key, value, flush);
    }
    
    public void setForcefully(final String key, final Object value, final boolean flush) {
        super.set(key, value, flush);
    }
    
    public void setForcefully(final String key, final Object value) {
        this.setForcefully(key, value, true);
    }
    
    @Override
    public void save() throws IOException {
        if (!this.isSaveable()) {
            throw new UnsupportedOperationException();
        }
        final Properties temp = SimpleConfiguration.copyProperties(this.properties);
        for (final String key : this.constants.keySet()) {
            temp.remove(key);
        }
        final File file = (File)this.input;
        temp.store(new FileOutputStream(file), this.comments);
    }
    
    public File getFile() {
        if (!this.isSaveable()) {
            return null;
        }
        return (File)this.input;
    }
    
    private void init(final OptionSet set) {
        this.comments = " TLauncher configuration file\n Version " + TLauncher.getVersion();
        this.defaults = new ConfigurationDefaults();
        this.set(this.constants = ArgumentParser.parse(set), false);
        this.log("Constant values:", this.constants);
        final int version = ConfigurationDefaults.getVersion();
        if (this.getDouble("settings.version") != version) {
            this.clear();
        }
        this.set("settings.version", version, false);
        for (final Map.Entry<String, Object> curen : this.defaults.getMap().entrySet()) {
            final String key = curen.getKey();
            if (this.constants.containsKey(key)) {
                this.log("Key \"" + key + "\" is unsaveable!");
            }
            else {
                final String value = this.get(key);
                final Object defvalue = curen.getValue();
                if (defvalue == null) {
                    continue;
                }
                try {
                    PlainParser.parse(value, defvalue);
                }
                catch (Exception e2) {
                    this.log("Key \"" + key + "\" is invalid!");
                    this.set(key, defvalue, false);
                }
            }
        }
        this.defaultLocales = getDefaultLocales(TLauncher.getInnerSettings());
        this.supportedLocales = this.defaultLocales;
        Locale selected = getLocaleOf(this.get("locale"));
        if (selected == null) {
            this.log("Selected locale is not supported, rolling back to default one");
            selected = Locale.getDefault();
            if (selected == getLocaleOf("uk_UA")) {
                selected = getLocaleOf("ru_RU");
            }
        }
        selected = findSuitableLocale(selected, this.supportedLocales);
        this.set("locale", selected, false);
        if (this.get("chooser.type.account") == null) {
            this.set("chooser.type.account", false, false);
        }
        if (this.get("skin.status.checkbox.state") == null) {
            this.set("skin.status.checkbox.state", true, false);
        }
        if (this.getInteger("minecraft.memory.ram2") > OS.Arch.MAX_MEMORY) {
            U.log(String.format("decreased memory from  %s to %s", this.getInteger("minecraft.memory.ram2"), OS.Arch.MAX_MEMORY));
            this.set("minecraft.memory.ram2", OS.Arch.MAX_MEMORY);
        }
        this.set(ConfigEnum.UPDATER_LAUNCHER, false);
        if (this.isSaveable()) {
            try {
                this.save();
            }
            catch (IOException e) {
                this.log("Cannot save value!", e);
            }
        }
    }
    
    public static Locale getLocaleOf(final String locale) {
        if (locale == null) {
            return null;
        }
        for (final Locale cur : Locale.getAvailableLocales()) {
            if (cur.toString().equals(locale)) {
                return cur;
            }
        }
        return null;
    }
    
    public static Locale findSuitableLocale(final Locale selected, final List<Locale> supportedLocales) {
        for (final Locale l : supportedLocales) {
            if (l.getLanguage().equals(selected.getLanguage())) {
                return l;
            }
        }
        U.log("Default locale is not supported, rolling back to global default one");
        return Locale.US;
    }
    
    public boolean isExist(final String key) {
        return this.get(key) != null;
    }
}
