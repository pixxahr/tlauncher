// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration;

import java.util.List;
import java.util.Collections;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.InputStream;

public class InnerConfiguration extends SimpleConfiguration
{
    public InnerConfiguration(final InputStream in) throws IOException {
        super(in);
    }
    
    public String getAccessRepoTlauncherOrg(final String key) {
        return this.get(key);
    }
    
    public String[] getArrayAccess(final String key) {
        return this.getAccessRepoTlauncherOrg(key).split(",");
    }
    
    public String[] getArrayShuffle(final String key) {
        final List<String> list = (List<String>)Lists.newArrayList((Object[])this.getArray(key));
        Collections.shuffle(list);
        return list.toArray(new String[0]);
    }
    
    public String[] getArray(final String key) {
        return this.get(key).split(",");
    }
}
