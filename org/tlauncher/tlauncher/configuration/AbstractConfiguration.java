// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration;

import java.io.IOException;
import org.tlauncher.tlauncher.entity.ConfigEnum;

public interface AbstractConfiguration
{
    String get(final String p0);
    
    int getInteger(final String p0);
    
    double getDouble(final String p0);
    
    float getFloat(final String p0);
    
    long getLong(final String p0);
    
    boolean getBoolean(final String p0);
    
    boolean getBoolean(final ConfigEnum p0);
    
    String getDefault(final String p0);
    
    int getDefaultInteger(final String p0);
    
    double getDefaultDouble(final String p0);
    
    float getDefaultFloat(final String p0);
    
    long getDefaultLong(final String p0);
    
    boolean getDefaultBoolean(final String p0);
    
    void set(final String p0, final Object p1);
    
    void set(final ConfigEnum p0, final Object p1);
    
    void clear();
    
    void save() throws IOException;
}
