// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration.enums;

public enum ConnectionQuality
{
    GOOD(2, 5, 3, 45000), 
    NORMAL(5, 10, 2, 45000), 
    BAD(10, 20, 1, 120000);
    
    private final int minTries;
    private final int maxTries;
    private final int maxThreads;
    private final int timeout;
    private final int[] configuration;
    
    private ConnectionQuality(final int minTries, final int maxTries, final int maxThreads, final int timeout) {
        this.minTries = minTries;
        this.maxTries = maxTries;
        this.maxThreads = maxThreads;
        this.timeout = timeout;
        this.configuration = new int[] { minTries, maxTries, maxThreads };
    }
    
    public static boolean parse(final String val) {
        if (val == null) {
            return false;
        }
        for (final ConnectionQuality cur : values()) {
            if (cur.toString().equalsIgnoreCase(val)) {
                return true;
            }
        }
        return false;
    }
    
    public static ConnectionQuality get(final String val) {
        for (final ConnectionQuality cur : values()) {
            if (cur.toString().equalsIgnoreCase(val)) {
                return cur;
            }
        }
        return null;
    }
    
    public int[] getConfiguration() {
        return this.configuration;
    }
    
    public int getMinTries() {
        return this.minTries;
    }
    
    public int getMaxTries() {
        return this.maxTries;
    }
    
    public int getMaxThreads() {
        return this.maxThreads;
    }
    
    public int getTries(final boolean fast) {
        return fast ? this.minTries : this.maxTries;
    }
    
    public int getTimeout() {
        return this.timeout;
    }
    
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    
    public static ConnectionQuality getDefault() {
        return ConnectionQuality.GOOD;
    }
}
