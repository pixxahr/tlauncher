// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration.enums;

public enum ActionOnLaunch
{
    HIDE, 
    EXIT, 
    NOTHING;
    
    public static boolean parse(final String val) {
        if (val == null) {
            return false;
        }
        for (final ActionOnLaunch cur : values()) {
            if (cur.toString().equalsIgnoreCase(val)) {
                return true;
            }
        }
        return false;
    }
    
    public static ActionOnLaunch get(final String val) {
        for (final ActionOnLaunch cur : values()) {
            if (cur.toString().equalsIgnoreCase(val)) {
                return cur;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    
    public static ActionOnLaunch getDefault() {
        return ActionOnLaunch.HIDE;
    }
}
