// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration.enums;

import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public enum ConsoleType
{
    GLOBAL, 
    MINECRAFT, 
    NONE;
    
    public static boolean parse(final String val) {
        if (val == null) {
            return false;
        }
        for (final ConsoleType cur : values()) {
            if (cur.toString().equalsIgnoreCase(val)) {
                return true;
            }
        }
        return false;
    }
    
    public static ConsoleType get(final String val) {
        for (final ConsoleType cur : values()) {
            if (cur.toString().equalsIgnoreCase(val)) {
                return cur;
            }
        }
        throw new NullPointerException("not find console type " + val);
    }
    
    public MinecraftLauncher.ConsoleVisibility getVisibility() {
        return (this == ConsoleType.GLOBAL) ? MinecraftLauncher.ConsoleVisibility.NONE : ((this == ConsoleType.MINECRAFT) ? MinecraftLauncher.ConsoleVisibility.ALWAYS : MinecraftLauncher.ConsoleVisibility.ON_CRASH);
    }
    
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    
    public static ConsoleType getDefault() {
        return ConsoleType.NONE;
    }
}
