// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration;

import java.util.Collections;
import java.util.Iterator;
import java.util.UUID;
import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import org.tlauncher.tlauncher.configuration.enums.ConsoleType;
import org.tlauncher.tlauncher.configuration.enums.ActionOnLaunch;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.updater.client.Notices;
import net.minecraft.launcher.versions.ReleaseType;
import org.tlauncher.util.IntegerArray;
import org.tlauncher.util.MinecraftUtil;
import java.util.HashMap;
import java.util.Map;

class ConfigurationDefaults
{
    private static final int version = 3;
    private final Map<String, Object> d;
    
    ConfigurationDefaults() {
        (this.d = new HashMap<String, Object>()).put("settings.version", 3);
        this.d.put("minecraft.gamedir", MinecraftUtil.getDefaultWorkingDirectory().getAbsolutePath());
        this.d.put("minecraft.size", new IntegerArray(new int[] { 925, 530 }));
        this.d.put("minecraft.fullscreen", false);
        for (final ReleaseType type : ReleaseType.getDefault()) {
            this.d.put("minecraft.versions." + type.name().toLowerCase(), true);
        }
        for (final ReleaseType.SubType type2 : ReleaseType.SubType.getDefault()) {
            this.d.put("minecraft.versions.sub." + type2.name().toLowerCase(), true);
        }
        for (final Notices.NoticeType type3 : Notices.NoticeType.values()) {
            if (type3.isAdvert()) {
                this.d.put("gui.notice." + type3.name().toLowerCase(), true);
            }
        }
        this.d.put("minecraft.memory.ram2", OS.Arch.PREFERRED_MEMORY);
        this.d.put("minecraft.onlaunch", ActionOnLaunch.getDefault());
        this.d.put("gui.console", ConsoleType.getDefault());
        this.d.put("gui.console.width", 720);
        this.d.put("gui.console.height", 500);
        this.d.put("gui.console.x", 30);
        this.d.put("gui.console.y", 30);
        this.d.put("connection", ConnectionQuality.getDefault());
        this.d.put("client", UUID.randomUUID());
        this.d.put("gui.statistics.checkbox", false);
        this.d.put("gui.settings.guard.checkbox", true);
        this.d.put("gui.settings.servers.recommendation", true);
    }
    
    public static int getVersion() {
        return 3;
    }
    
    public Map<String, Object> getMap() {
        return Collections.unmodifiableMap((Map<? extends String, ?>)this.d);
    }
    
    public Object get(final String key) {
        return this.d.get(key);
    }
}
