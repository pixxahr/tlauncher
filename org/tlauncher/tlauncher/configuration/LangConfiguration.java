// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration;

import java.util.Hashtable;
import java.util.Iterator;
import java.io.InputStream;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.io.IOException;
import java.util.Properties;
import java.util.Locale;

public class LangConfiguration extends SimpleConfiguration
{
    private final Locale[] locales;
    private final Properties[] prop;
    private int i;
    
    public LangConfiguration(final Locale[] locales, final Locale select, final String folder) throws IOException {
        if (locales == null) {
            throw new NullPointerException();
        }
        final int size = locales.length;
        this.locales = locales;
        this.prop = new Properties[size];
        for (int i = 0; i < size; ++i) {
            final Locale locale = locales[i];
            if (locale == null) {
                throw new NullPointerException("Locale at #" + i + " is NULL!");
            }
            final String localeName = locale.toString();
            final InputStream stream = this.getClass().getResourceAsStream(folder + localeName);
            if (stream == null) {
                throw new IOException("Cannot find locale file for: " + localeName);
            }
            this.prop[i] = SimpleConfiguration.loadFromStream(stream);
            if (localeName.equals("en_US")) {
                SimpleConfiguration.copyProperties(this.prop[i], this.properties, true);
            }
        }
        int defLocale = -1;
        for (int j = 0; j < size; ++j) {
            if (locales[j].toString().equals("ru_RU")) {
                defLocale = j;
                break;
            }
        }
        if (defLocale != -1) {
            for (final Object key : ((Hashtable<Object, V>)this.prop[defLocale]).keySet()) {
                for (int k = 0; k < size; ++k) {
                    if (k != defLocale) {
                        if (!this.prop[k].containsKey(key) && !TLauncher.DEBUG) {
                            U.log("Locale", locales[k], "doesn't contain key", key);
                        }
                    }
                }
            }
            for (int j = 0; j < size; ++j) {
                if (j != defLocale) {
                    int numberLine = 0;
                    for (final Object key2 : ((Hashtable<Object, V>)this.prop[j]).keySet()) {
                        ++numberLine;
                        if (!this.prop[defLocale].containsKey(key2)) {
                            U.log("Locale", locales[j], "contains redundant key line is " + numberLine + ", key is " + key2);
                        }
                    }
                }
            }
        }
        this.setSelected(select);
    }
    
    public Locale[] getLocales() {
        return this.locales;
    }
    
    public Locale getSelected() {
        return this.locales[this.i];
    }
    
    public void setSelected(final Locale select) {
        if (select == null) {
            throw new NullPointerException();
        }
        for (int i = 0; i < this.locales.length; ++i) {
            if (this.locales[i].equals(select)) {
                this.i = i;
                return;
            }
        }
        throw new IllegalArgumentException("Cannot find Locale:" + select);
    }
    
    public String nget(final String key) {
        if (key == null) {
            return null;
        }
        final String value = this.prop[this.i].getProperty(key);
        if (value == null) {
            return this.getDefault(key);
        }
        return value;
    }
    
    @Override
    public String get(final String key) {
        final String value = this.nget(key);
        if (value == null) {
            return key;
        }
        return value;
    }
    
    public String nget(final String key, final Object... vars) {
        String value = this.get(key);
        if (value == null) {
            return null;
        }
        final String[] variables = checkVariables(vars);
        for (int i = 0; i < variables.length; ++i) {
            value = value.replace("%" + i, variables[i]);
        }
        return value;
    }
    
    public String get(final String key, final Object... vars) {
        final String value = this.nget(key, vars);
        if (value == null) {
            return key;
        }
        return value;
    }
    
    @Override
    public void set(final String key, final Object value) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public String getDefault(final String key) {
        return super.get(key);
    }
    
    private static String[] checkVariables(final Object[] check) {
        if (check == null) {
            throw new NullPointerException();
        }
        if (check.length == 1 && check[0] == null) {
            return new String[0];
        }
        final String[] string = new String[check.length];
        for (int i = 0; i < check.length; ++i) {
            if (check[i] == null) {
                throw new NullPointerException("Variable at index " + i + " is NULL!");
            }
            string[i] = check[i].toString();
        }
        return string;
    }
}
