// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration;

import java.util.Hashtable;
import org.tlauncher.util.U;
import java.io.FileInputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.io.BufferedInputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import org.tlauncher.util.StringUtil;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import org.tlauncher.tlauncher.entity.ConfigEnum;
import java.net.URL;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SimpleConfiguration implements AbstractConfiguration
{
    protected final Properties properties;
    protected Object input;
    protected String comments;
    
    public SimpleConfiguration() {
        this.properties = new Properties();
    }
    
    public SimpleConfiguration(final InputStream stream) throws IOException {
        this();
        loadFromStream(this.properties, stream);
        this.input = stream;
    }
    
    public SimpleConfiguration(final File file) {
        this();
        try {
            loadFromFile(this.properties, file);
        }
        catch (FileNotFoundException e2) {
            this.log(file + " file not exists");
        }
        catch (Exception e) {
            this.log("Error loading config from file:", e);
        }
        this.input = file;
    }
    
    public SimpleConfiguration(final URL url) throws IOException {
        this();
        loadFromURL(this.properties, url);
        this.input = url;
    }
    
    @Override
    public String get(final String key) {
        return this.getStringOf(this.properties.getProperty(key));
    }
    
    protected String getStringOf(final Object obj) {
        String s;
        if (obj == null) {
            s = null;
        }
        else {
            s = obj.toString();
            if (s.isEmpty()) {
                s = null;
            }
        }
        return s;
    }
    
    public void set(final String key, final Object value, final boolean flush) {
        if (key == null) {
            throw new NullPointerException();
        }
        if (value == null) {
            this.properties.remove(key);
        }
        else {
            this.properties.setProperty(key, value.toString());
        }
        if (flush && this.isSaveable()) {
            this.store();
        }
    }
    
    @Override
    public void set(final String key, final Object value) {
        this.set(key, value, true);
    }
    
    @Override
    public void set(final ConfigEnum key, final Object value) {
        this.set(key.name(), value);
    }
    
    public void set(final Map<String, Object> map, final boolean flush) {
        for (final Map.Entry<String, Object> en : map.entrySet()) {
            final String key = en.getKey();
            final Object value = en.getValue();
            if (value == null) {
                this.properties.remove(key);
            }
            else {
                this.properties.setProperty(key, value.toString());
            }
        }
        if (flush && this.isSaveable()) {
            this.store();
        }
    }
    
    public void set(final Map<String, Object> map) {
        this.set(map, false);
    }
    
    public Set<String> getKeys() {
        final Set<String> set = new HashSet<String>();
        for (final Object obj : ((Hashtable<Object, V>)this.properties).keySet()) {
            set.add(this.getStringOf(obj));
        }
        return Collections.unmodifiableSet((Set<? extends String>)set);
    }
    
    @Override
    public String getDefault(final String key) {
        return null;
    }
    
    public int getInteger(final String key, final int def) {
        return this.getIntegerOf(this.get(key), 0);
    }
    
    @Override
    public int getInteger(final String key) {
        return this.getInteger(key, 0);
    }
    
    protected int getIntegerOf(final Object obj, final int def) {
        try {
            return Integer.parseInt(obj.toString());
        }
        catch (Exception e) {
            return def;
        }
    }
    
    @Override
    public double getDouble(final String key) {
        return this.getDoubleOf(this.get(key), 0.0);
    }
    
    protected double getDoubleOf(final Object obj, final double def) {
        try {
            return Double.parseDouble(obj.toString());
        }
        catch (Exception e) {
            return def;
        }
    }
    
    @Override
    public float getFloat(final String key) {
        return this.getFloatOf(this.get(key), 0.0f);
    }
    
    protected float getFloatOf(final Object obj, final float def) {
        try {
            return Float.parseFloat(obj.toString());
        }
        catch (Exception e) {
            return def;
        }
    }
    
    @Override
    public long getLong(final String key) {
        return this.getLongOf(this.get(key), 0L);
    }
    
    protected long getLongOf(final Object obj, final long def) {
        try {
            return Long.parseLong(obj.toString());
        }
        catch (Exception e) {
            return def;
        }
    }
    
    @Override
    public boolean getBoolean(final String key) {
        return this.getBooleanOf(this.get(key), false);
    }
    
    @Override
    public boolean getBoolean(final ConfigEnum key) {
        return this.getBoolean(key.name());
    }
    
    protected boolean getBooleanOf(final Object obj, final boolean def) {
        try {
            return StringUtil.parseBoolean(obj.toString());
        }
        catch (Exception e) {
            return def;
        }
    }
    
    @Override
    public int getDefaultInteger(final String key) {
        return 0;
    }
    
    @Override
    public double getDefaultDouble(final String key) {
        return 0.0;
    }
    
    @Override
    public float getDefaultFloat(final String key) {
        return 0.0f;
    }
    
    @Override
    public long getDefaultLong(final String key) {
        return 0L;
    }
    
    @Override
    public boolean getDefaultBoolean(final String key) {
        return false;
    }
    
    @Override
    public void save() throws IOException {
        if (!this.isSaveable()) {
            throw new UnsupportedOperationException();
        }
        final File file = (File)this.input;
        this.properties.store(new FileOutputStream(file), this.comments);
    }
    
    public void store() {
        try {
            this.save();
        }
        catch (IOException e) {
            this.log("Cannot store values!", e);
        }
    }
    
    @Override
    public void clear() {
        this.properties.clear();
    }
    
    public boolean isSaveable() {
        return this.input instanceof File;
    }
    
    private static void loadFromStream(final Properties properties, final InputStream stream) throws IOException {
        if (stream == null) {
            throw new NullPointerException();
        }
        final Reader reader = new InputStreamReader(new BufferedInputStream(stream), Charset.forName("UTF-8"));
        properties.clear();
        properties.load(reader);
    }
    
    static Properties loadFromStream(final InputStream stream) throws IOException {
        final Properties properties = new Properties();
        loadFromStream(properties, stream);
        return properties;
    }
    
    private static void loadFromFile(final Properties properties, final File file) throws IOException {
        if (file == null) {
            throw new NullPointerException();
        }
        final FileInputStream stream = new FileInputStream(file);
        loadFromStream(properties, stream);
    }
    
    protected static Properties loadFromFile(final File file) throws IOException {
        final Properties properties = new Properties();
        loadFromFile(properties, file);
        return properties;
    }
    
    private static void loadFromURL(final Properties properties, final URL url) throws IOException {
        if (url == null) {
            throw new NullPointerException();
        }
        final InputStream connection = url.openStream();
        loadFromStream(properties, connection);
    }
    
    protected static Properties loadFromURL(final URL url) throws IOException {
        final Properties properties = new Properties();
        loadFromURL(properties, url);
        return properties;
    }
    
    protected static void copyProperties(final Properties src, final Properties dest, final boolean wipe) {
        if (src == null) {
            throw new NullPointerException("src is NULL");
        }
        if (dest == null) {
            throw new NullPointerException("dest is NULL");
        }
        if (wipe) {
            dest.clear();
        }
        for (final Map.Entry<Object, Object> en : src.entrySet()) {
            final String key = (en.getKey() == null) ? null : en.getKey().toString();
            final String value = (en.getKey() == null) ? null : en.getValue().toString();
            dest.setProperty(key, value);
        }
    }
    
    protected static Properties copyProperties(final Properties src) {
        final Properties properties = new Properties();
        copyProperties(src, properties, false);
        return properties;
    }
    
    void log(final Object... o) {
        U.log("[" + this.getClass().getSimpleName() + "]", o);
    }
    
    public boolean isUSSRLocale() {
        final String locale = this.get("locale");
        return "ru_RU".equals(locale) || "uk_UA".equals(locale);
    }
}
