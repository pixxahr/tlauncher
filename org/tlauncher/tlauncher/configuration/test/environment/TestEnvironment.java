// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration.test.environment;

public interface TestEnvironment
{
    public static final String WARMING_MESSAGE = "memory.problem.message";
    
    boolean testEnvironment();
    
    void fix();
}
