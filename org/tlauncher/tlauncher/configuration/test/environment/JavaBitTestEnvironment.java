// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration.test.environment;

import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.util.U;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.configuration.Configuration;

public class JavaBitTestEnvironment implements TestEnvironment
{
    private Configuration c;
    
    public JavaBitTestEnvironment(final Configuration c) {
        this.c = c;
    }
    
    @Override
    public boolean testEnvironment() {
        final String systemType = OS.getSystemInfo("System Type");
        final String processorDesc = OS.getSystemInfo("Processor(s)");
        if (OS.CURRENT == OS.WINDOWS && systemType != null && processorDesc != null && systemType.toLowerCase().contains("86")) {
            U.log("system type" + systemType);
            U.log("Processor(s)" + processorDesc);
            if (processorDesc.toLowerCase().contains("64")) {
                this.c.set("memory.problem.message", "system.32x.not.proper", false);
                U.log("not proper bit system for this processor " + OS.getSystemInfo("Processor(s)"));
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void fix() {
        final String value = "system.bit.message.not.show";
        if (this.c.getBoolean(value)) {
            return;
        }
        final String key;
        SwingUtilities.invokeLater(() -> {
            if (Alert.showWarningMessageWithCheckBox("", Localizable.get("system.32x.not.proper"), 400)) {
                this.c.set(key, true, false);
            }
        });
    }
}
