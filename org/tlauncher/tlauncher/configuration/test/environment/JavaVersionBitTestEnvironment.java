// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.configuration.test.environment;

import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.util.U;
import org.tlauncher.util.OS;
import org.tlauncher.tlauncher.configuration.Configuration;

public class JavaVersionBitTestEnvironment implements TestEnvironment
{
    private Configuration c;
    
    public JavaVersionBitTestEnvironment(final Configuration c) {
        this.c = c;
    }
    
    @Override
    public boolean testEnvironment() {
        final String systemType = OS.getSystemInfo("System Type");
        U.debug("system type" + systemType);
        if (OS.CURRENT == OS.WINDOWS && OS.Arch.CURRENT == OS.Arch.x32 && systemType != null && systemType.toLowerCase().contains("64")) {
            this.c.set("memory.problem.message", "java.version.32x.not.proper", false);
            U.log("not proper java bit for system type " + systemType);
            return false;
        }
        return true;
    }
    
    @Override
    public void fix() {
        final String value = "java.version.message.not.show";
        if (this.c.getBoolean(value)) {
            return;
        }
        final String key;
        SwingUtilities.invokeLater(() -> {
            if (Alert.showWarningMessageWithCheckBox("", "java.version.32x.not.proper", 400)) {
                this.c.set(key, true, false);
            }
        });
    }
}
