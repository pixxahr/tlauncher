// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper;

import org.tlauncher.tlauncher.updater.bootstrapper.model.DownloadedBootInfo;
import java.io.IOException;
import java.util.List;

public interface PreparedEnvironmentComponent
{
    List<String> getLibrariesForRunning() throws IOException;
    
    DownloadedBootInfo validateLibraryAndJava();
    
    void preparedLibrariesAndJava(final DownloadedBootInfo p0);
}
