// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper.model;

import java.util.List;

public class LibraryConfig
{
    private double versionClient;
    private List<DownloadedElement> libraries;
    
    public double getVersionClient() {
        return this.versionClient;
    }
    
    public List<DownloadedElement> getLibraries() {
        return this.libraries;
    }
    
    public void setVersionClient(final double versionClient) {
        this.versionClient = versionClient;
    }
    
    public void setLibraries(final List<DownloadedElement> libraries) {
        this.libraries = libraries;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LibraryConfig)) {
            return false;
        }
        final LibraryConfig other = (LibraryConfig)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (Double.compare(this.getVersionClient(), other.getVersionClient()) != 0) {
            return false;
        }
        final Object this$libraries = this.getLibraries();
        final Object other$libraries = other.getLibraries();
        if (this$libraries == null) {
            if (other$libraries == null) {
                return true;
            }
        }
        else if (this$libraries.equals(other$libraries)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof LibraryConfig;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $versionClient = Double.doubleToLongBits(this.getVersionClient());
        result = result * 59 + (int)($versionClient >>> 32 ^ $versionClient);
        final Object $libraries = this.getLibraries();
        result = result * 59 + (($libraries == null) ? 43 : $libraries.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "LibraryConfig(versionClient=" + this.getVersionClient() + ", libraries=" + this.getLibraries() + ")";
    }
}
