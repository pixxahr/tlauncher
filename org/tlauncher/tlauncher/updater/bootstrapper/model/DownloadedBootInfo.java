// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper.model;

import java.util.List;

public class DownloadedBootInfo
{
    List<DownloadedElement> libraries;
    JavaDownloadedElement element;
    
    public List<DownloadedElement> getLibraries() {
        return this.libraries;
    }
    
    public JavaDownloadedElement getElement() {
        return this.element;
    }
    
    public void setLibraries(final List<DownloadedElement> libraries) {
        this.libraries = libraries;
    }
    
    public void setElement(final JavaDownloadedElement element) {
        this.element = element;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof DownloadedBootInfo)) {
            return false;
        }
        final DownloadedBootInfo other = (DownloadedBootInfo)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$libraries = this.getLibraries();
        final Object other$libraries = other.getLibraries();
        Label_0065: {
            if (this$libraries == null) {
                if (other$libraries == null) {
                    break Label_0065;
                }
            }
            else if (this$libraries.equals(other$libraries)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$element = this.getElement();
        final Object other$element = other.getElement();
        if (this$element == null) {
            if (other$element == null) {
                return true;
            }
        }
        else if (this$element.equals(other$element)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof DownloadedBootInfo;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $libraries = this.getLibraries();
        result = result * 59 + (($libraries == null) ? 43 : $libraries.hashCode());
        final Object $element = this.getElement();
        result = result * 59 + (($element == null) ? 43 : $element.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "DownloadedBootInfo(libraries=" + this.getLibraries() + ", element=" + this.getElement() + ")";
    }
}
