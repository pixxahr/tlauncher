// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper.model;

import java.io.File;

public class JavaDownloadedElement extends DownloadedElement
{
    private String javaFolder;
    
    public boolean existFolder(final File folder) {
        return new File(folder, this.javaFolder).exists();
    }
    
    public String getJavaFolder() {
        return this.javaFolder;
    }
    
    public void setJavaFolder(final String javaFolder) {
        this.javaFolder = javaFolder;
    }
    
    @Override
    public String toString() {
        return "JavaDownloadedElement(javaFolder=" + this.getJavaFolder() + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof JavaDownloadedElement)) {
            return false;
        }
        final JavaDownloadedElement other = (JavaDownloadedElement)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final Object this$javaFolder = this.getJavaFolder();
        final Object other$javaFolder = other.getJavaFolder();
        if (this$javaFolder == null) {
            if (other$javaFolder == null) {
                return true;
            }
        }
        else if (this$javaFolder.equals(other$javaFolder)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof JavaDownloadedElement;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = super.hashCode();
        final Object $javaFolder = this.getJavaFolder();
        result = result * 59 + (($javaFolder == null) ? 43 : $javaFolder.hashCode());
        return result;
    }
}
