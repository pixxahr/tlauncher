// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper.model;

import java.util.Collections;
import org.tlauncher.util.FileUtil;
import java.io.File;
import java.util.List;

public class DownloadedElement
{
    private List<String> url;
    private String shaCode;
    private String storagePath;
    
    public boolean notExistOrValid(final File folder) {
        final File library = new File(folder, this.storagePath);
        if (library.exists() && library.isFile()) {
            final String sha = FileUtil.getChecksum(library, "sha1");
            return !this.getShaCode().equals(sha);
        }
        return true;
    }
    
    public List<String> getUrl() {
        Collections.shuffle(this.url);
        return this.url;
    }
    
    public String getShaCode() {
        return this.shaCode;
    }
    
    public String getStoragePath() {
        return this.storagePath;
    }
    
    public void setUrl(final List<String> url) {
        this.url = url;
    }
    
    public void setShaCode(final String shaCode) {
        this.shaCode = shaCode;
    }
    
    public void setStoragePath(final String storagePath) {
        this.storagePath = storagePath;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof DownloadedElement)) {
            return false;
        }
        final DownloadedElement other = (DownloadedElement)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$url = this.getUrl();
        final Object other$url = other.getUrl();
        Label_0065: {
            if (this$url == null) {
                if (other$url == null) {
                    break Label_0065;
                }
            }
            else if (this$url.equals(other$url)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$shaCode = this.getShaCode();
        final Object other$shaCode = other.getShaCode();
        Label_0102: {
            if (this$shaCode == null) {
                if (other$shaCode == null) {
                    break Label_0102;
                }
            }
            else if (this$shaCode.equals(other$shaCode)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$storagePath = this.getStoragePath();
        final Object other$storagePath = other.getStoragePath();
        if (this$storagePath == null) {
            if (other$storagePath == null) {
                return true;
            }
        }
        else if (this$storagePath.equals(other$storagePath)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof DownloadedElement;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $url = this.getUrl();
        result = result * 59 + (($url == null) ? 43 : $url.hashCode());
        final Object $shaCode = this.getShaCode();
        result = result * 59 + (($shaCode == null) ? 43 : $shaCode.hashCode());
        final Object $storagePath = this.getStoragePath();
        result = result * 59 + (($storagePath == null) ? 43 : $storagePath.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "DownloadedElement(url=" + this.getUrl() + ", shaCode=" + this.getShaCode() + ", storagePath=" + this.getStoragePath() + ")";
    }
}
