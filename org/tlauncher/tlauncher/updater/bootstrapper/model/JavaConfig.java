// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper.model;

import org.tlauncher.util.OS;
import java.util.Map;

public class JavaConfig
{
    Map<OS, Map<OS.Arch, JavaDownloadedElement>> config;
    
    public Map<OS, Map<OS.Arch, JavaDownloadedElement>> getConfig() {
        return this.config;
    }
    
    public void setConfig(final Map<OS, Map<OS.Arch, JavaDownloadedElement>> config) {
        this.config = config;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof JavaConfig)) {
            return false;
        }
        final JavaConfig other = (JavaConfig)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$config = this.getConfig();
        final Object other$config = other.getConfig();
        if (this$config == null) {
            if (other$config == null) {
                return true;
            }
        }
        else if (this$config.equals(other$config)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof JavaConfig;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $config = this.getConfig();
        result = result * 59 + (($config == null) ? 43 : $config.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "JavaConfig(config=" + this.getConfig() + ")";
    }
}
