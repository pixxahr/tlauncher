// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper;

import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.rmo.Bootstrapper;
import org.tlauncher.tlauncher.downloader.DownloadableContainerHandlerAdapter;
import org.tlauncher.tlauncher.repository.Repo;
import java.util.concurrent.TimeUnit;
import org.tlauncher.tlauncher.downloader.DownloadableContainerHandler;
import java.io.IOException;
import org.tlauncher.tlauncher.downloader.RetryDownloadException;
import org.apache.commons.io.FileUtils;
import org.tlauncher.util.U;
import java.nio.file.Files;
import org.tlauncher.util.FileUtil;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;
import java.util.Objects;
import org.tlauncher.tlauncher.updater.bootstrapper.model.JavaDownloadedElement;
import org.tlauncher.util.OS;
import org.tlauncher.util.TlauncherUtil;
import org.tlauncher.tlauncher.updater.bootstrapper.model.DownloadedBootInfo;
import java.util.Iterator;
import org.tlauncher.tlauncher.updater.bootstrapper.model.DownloadedElement;
import java.util.ArrayList;
import java.util.List;
import org.tlauncher.tlauncher.downloader.Downloader;
import java.io.File;
import org.tlauncher.tlauncher.updater.bootstrapper.model.JavaConfig;
import org.tlauncher.tlauncher.updater.bootstrapper.model.LibraryConfig;

public class PreparedEnvironmentComponentImpl implements PreparedEnvironmentComponent
{
    private LibraryConfig config;
    private JavaConfig javaConfig;
    private File workFolder;
    private File jvmsFolder;
    private Downloader downloader;
    private boolean usedProperJFXJVM;
    
    @Override
    public List<String> getLibrariesForRunning() {
        final List<DownloadedElement> list = this.config.getLibraries();
        final List<String> result = new ArrayList<String>();
        for (final DownloadedElement downloadedElement : list) {
            final File f = new File(this.workFolder, downloadedElement.getStoragePath());
            result.add(f.getPath());
        }
        return result;
    }
    
    @Override
    public DownloadedBootInfo validateLibraryAndJava() {
        final List<DownloadedElement> result = new ArrayList<DownloadedElement>();
        final List<DownloadedElement> list = this.config.getLibraries();
        for (final DownloadedElement downloadedElement : list) {
            if (downloadedElement.notExistOrValid(this.workFolder)) {
                result.add(downloadedElement);
            }
        }
        final DownloadedBootInfo el = new DownloadedBootInfo();
        el.setLibraries(result);
        final JavaDownloadedElement javaDownloadedElement = TlauncherUtil.getProperJavaElement(this.javaConfig);
        if ((TlauncherUtil.useX64JavaInsteadX32Java() || !TlauncherUtil.hasCorrectJavaFX() || this.usedProperJFXJVM) && !javaDownloadedElement.existFolder(this.jvmsFolder)) {
            el.setElement(javaDownloadedElement);
            return el;
        }
        if (!"8".equals(OS.getJavaNumber()) && !javaDownloadedElement.existFolder(this.jvmsFolder)) {
            el.setElement(javaDownloadedElement);
        }
        return el;
    }
    
    @Override
    public void preparedLibrariesAndJava(final DownloadedBootInfo info) {
        if (info.getLibraries().isEmpty() && Objects.isNull(info.getElement())) {
            return;
        }
        if (Objects.nonNull(info.getElement())) {
            final DownloadableContainer javaDownloadableContainer = new DownloadableContainer();
            final DownloadableDownloadElement downloadedJava = new DownloadableDownloadElement(info.getElement(), this.jvmsFolder);
            javaDownloadableContainer.add(downloadedJava);
            javaDownloadableContainer.addHandler(new DownloadableHandler(this.jvmsFolder) {
                @Override
                public void onComplete(final DownloadableContainer c, final Downloadable d) throws RetryDownloadException {
                    final DownloadableDownloadElement el = (DownloadableDownloadElement)d;
                    super.onComplete(c, d);
                    final File javaFolder = new File(PreparedEnvironmentComponentImpl.this.jvmsFolder, ((JavaDownloadedElement)el.getElement()).getJavaFolder());
                    final File tempJVM = new File(javaFolder + "_temp");
                    try {
                        if (javaFolder.exists()) {
                            FileUtil.deleteDirectory(javaFolder);
                        }
                        if (tempJVM.exists()) {
                            FileUtil.deleteDirectory(tempJVM);
                        }
                        PreparedEnvironmentComponentImpl.this.unpack(d);
                        final File java = new File(OS.appendBootstrapperJvm(tempJVM.getPath()));
                        if (OS.is(OS.LINUX)) {
                            Files.setPosixFilePermissions(java.toPath(), FileUtil.PERMISSIONS);
                        }
                        U.log(String.format("rename from %s to %s", tempJVM, javaFolder));
                        if (!tempJVM.renameTo(javaFolder)) {
                            FileUtils.copyDirectory(tempJVM, javaFolder);
                        }
                        FileUtil.deleteFile(tempJVM);
                        FileUtil.deleteFile(d.getDestination());
                    }
                    catch (IOException t) {
                        U.log(t);
                        if (javaFolder.exists()) {
                            FileUtil.deleteDirectory(javaFolder);
                        }
                        throw new RetryDownloadException("cannot unpack archive", t);
                    }
                }
            });
            this.downloader.add(javaDownloadableContainer);
        }
        if (!info.getLibraries().isEmpty()) {
            final DownloadableContainer container = new DownloadableContainer();
            for (final DownloadedElement e : info.getLibraries()) {
                container.add(new DownloadableDownloadElement(e, this.workFolder));
            }
            container.addHandler(new DownloadableHandler(this.workFolder));
            this.downloader.add(container);
        }
        this.downloader.startDownloadAndWait();
    }
    
    private void unpack(final Downloadable d) throws IOException {
        if (OS.is(OS.OSX)) {
            try {
                final String[] commands = { "tar", "-xf", d.getDestination().getPath() };
                final ProcessBuilder p = new ProcessBuilder(commands);
                p.directory(this.jvmsFolder);
                final Process process = p.start();
                try {
                    process.waitFor(90L, TimeUnit.SECONDS);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            catch (Throwable e2) {
                TlauncherUtil.showCriticalProblem(e2);
            }
        }
        else {
            FileUtil.unZip(d.getDestination(), this.jvmsFolder, false, false);
        }
    }
    
    public void setDownloader(final Downloader downloader) {
        this.downloader = downloader;
    }
    
    public PreparedEnvironmentComponentImpl(final LibraryConfig config, final JavaConfig javaConfig, final File workFolder, final File jvmsFolder, final Downloader downloader, final boolean usedProperJFXJVM) {
        this.config = config;
        this.javaConfig = javaConfig;
        this.workFolder = workFolder;
        this.jvmsFolder = jvmsFolder;
        this.downloader = downloader;
        this.usedProperJFXJVM = usedProperJFXJVM;
    }
    
    private class DownloadableDownloadElement extends Downloadable
    {
        private DownloadedElement element;
        
        DownloadableDownloadElement(final DownloadedElement element, final File work) {
            super(new Repo(element.getUrl().toArray(new String[0]), "BOOTSTRAP"), "", new File(work, element.getStoragePath()), true, false);
            this.element = element;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof DownloadableDownloadElement)) {
                return false;
            }
            final DownloadableDownloadElement other = (DownloadableDownloadElement)o;
            if (!other.canEqual(this)) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }
            final Object this$element = this.getElement();
            final Object other$element = other.getElement();
            if (this$element == null) {
                if (other$element == null) {
                    return true;
                }
            }
            else if (this$element.equals(other$element)) {
                return true;
            }
            return false;
        }
        
        protected boolean canEqual(final Object other) {
            return other instanceof DownloadableDownloadElement;
        }
        
        @Override
        public int hashCode() {
            final int PRIME = 59;
            int result = super.hashCode();
            final Object $element = this.getElement();
            result = result * 59 + (($element == null) ? 43 : $element.hashCode());
            return result;
        }
        
        public DownloadedElement getElement() {
            return this.element;
        }
        
        public void setElement(final DownloadedElement element) {
            this.element = element;
        }
        
        @Override
        public String toString() {
            return "PreparedEnvironmentComponentImpl.DownloadableDownloadElement(element=" + this.getElement() + ")";
        }
    }
    
    private class DownloadableHandler extends DownloadableContainerHandlerAdapter
    {
        private File downloadFolder;
        
        @Override
        public void onComplete(final DownloadableContainer c, final Downloadable d) throws RetryDownloadException {
            final DownloadableDownloadElement el = (DownloadableDownloadElement)d;
            if (el.getElement().notExistOrValid(this.downloadFolder)) {
                throw new RetryDownloadException("illegal library hash. " + el.getElement());
            }
        }
        
        @Override
        public void onError(final DownloadableContainer c, final Downloadable d, final Throwable e) {
            U.log(e);
            TlauncherUtil.showCriticalProblem(Bootstrapper.langConfiguration.get("updater.download.fail"));
            TLauncher.kill();
        }
        
        public File getDownloadFolder() {
            return this.downloadFolder;
        }
        
        public void setDownloadFolder(final File downloadFolder) {
            this.downloadFolder = downloadFolder;
        }
        
        @Override
        public String toString() {
            return "PreparedEnvironmentComponentImpl.DownloadableHandler(downloadFolder=" + this.getDownloadFolder() + ")";
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof DownloadableHandler)) {
                return false;
            }
            final DownloadableHandler other = (DownloadableHandler)o;
            if (!other.canEqual(this)) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }
            final Object this$downloadFolder = this.getDownloadFolder();
            final Object other$downloadFolder = other.getDownloadFolder();
            if (this$downloadFolder == null) {
                if (other$downloadFolder == null) {
                    return true;
                }
            }
            else if (this$downloadFolder.equals(other$downloadFolder)) {
                return true;
            }
            return false;
        }
        
        protected boolean canEqual(final Object other) {
            return other instanceof DownloadableHandler;
        }
        
        @Override
        public int hashCode() {
            final int PRIME = 59;
            int result = super.hashCode();
            final Object $downloadFolder = this.getDownloadFolder();
            result = result * 59 + (($downloadFolder == null) ? 43 : $downloadFolder.hashCode());
            return result;
        }
        
        public DownloadableHandler(final File downloadFolder) {
            this.downloadFolder = downloadFolder;
        }
    }
}
