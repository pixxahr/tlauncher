// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper;

public class PrepareLauncherException extends Exception
{
    public PrepareLauncherException(final String message) {
        super(message);
    }
    
    public PrepareLauncherException(final Throwable ex) {
        super(ex);
    }
}
