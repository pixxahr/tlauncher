// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper.view;

import org.tlauncher.util.U;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Insets;
import javax.swing.JPanel;

public class UpdaterPanelProgressBar extends JPanel
{
    private static final long serialVersionUID = -8469500310564854471L;
    protected Insets insets;
    protected Color background;
    private final Color border;
    
    public UpdaterPanelProgressBar() {
        this.insets = new Insets(5, 10, 10, 10);
        this.background = new Color(255, 255, 255, 220);
        this.border = new Color(255, 255, 255, 255);
    }
    
    public void paintComponent(final Graphics g0) {
        final Graphics2D g = (Graphics2D)g0;
        final int arc = 16;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(this.background);
        g.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), arc, arc);
        g.setColor(this.border);
        for (int x = 1; x < 2; ++x) {
            g.drawRoundRect(x - 1, x - 1, this.getWidth() - 2 * x + 1, this.getHeight() - 2 * x + 1, arc, arc);
        }
        Color shadow = U.shiftAlpha(Color.gray, -200);
        int x2 = 2;
        while (true) {
            shadow = U.shiftAlpha(shadow, -8);
            if (shadow.getAlpha() == 0) {
                break;
            }
            g.setColor(shadow);
            g.drawRoundRect(x2 - 1, x2 - 1, this.getWidth() - 2 * x2 + 1, this.getHeight() - 2 * x2 + 1, arc - 2 * x2 + 1, arc - 2 * x2 + 1);
            ++x2;
        }
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        super.paintComponent(g0);
    }
    
    @Override
    public Insets getInsets() {
        return this.insets;
    }
}
