// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.bootstrapper.view;

import org.tlauncher.tlauncher.downloader.Downloadable;
import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.downloader.Downloader;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import org.tlauncher.tlauncher.ui.TLauncherFrame;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import javax.swing.JProgressBar;
import org.tlauncher.tlauncher.downloader.DownloaderListener;
import javax.swing.JFrame;

public class DownloadingFrameElement extends JFrame implements DownloaderListener
{
    private JProgressBar bar;
    
    public DownloadingFrameElement(final String upMessage) {
        this.bar = new JProgressBar();
        final JPanel pan = new UpdaterPanelProgressBar();
        pan.setLayout(new BorderLayout());
        pan.setOpaque(false);
        final JLabel label = new JLabel(upMessage);
        label.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        label.setFont(label.getFont().deriveFont(TLauncherFrame.fontSize));
        pan.add(label, "North");
        pan.add(this.bar, "Center");
        pan.add(new JLabel(""), "South");
        this.setSize(new Dimension(350, 60));
        this.setResizable(false);
        this.setUndecorated(true);
        this.setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));
        this.setDefaultCloseOperation(3);
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        this.add(pan);
    }
    
    @Override
    public void onDownloaderStart(final Downloader d, final int files) {
        this.setVisible(true);
    }
    
    @Override
    public void onDownloaderAbort(final Downloader d) {
    }
    
    @Override
    public void onDownloaderProgress(final Downloader d, final double progress, final double speed) {
        final int p;
        SwingUtilities.invokeLater(() -> {
            p = (int)(progress * 100.0);
            this.bar.setValue(p);
            this.bar.repaint();
        });
    }
    
    @Override
    public void onDownloaderFileComplete(final Downloader d, final Downloadable file) {
    }
    
    @Override
    public void onDownloaderComplete(final Downloader d) {
        this.setVisible(false);
        this.dispose();
    }
}
