// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

import org.tlauncher.tlauncher.entity.ConfigEnum;
import org.tlauncher.tlauncher.ui.updater.AutoUpdaterFrame;
import org.tlauncher.tlauncher.rmo.TLauncher;

public class AutoUpdater extends Updater
{
    private final TLauncher t;
    private final AutoUpdaterFrame frame;
    private final UpdaterListener updaterL;
    private final UpdateListener updateL;
    
    public AutoUpdater(final TLauncher tlauncher) {
        this.t = tlauncher;
        this.frame = new AutoUpdaterFrame(this);
        this.addListener(new UpdaterAdapter() {
            @Override
            public void onUpdaterSucceeded(final SearchSucceeded succeeded) {
                if (TLauncher.getVersion() < succeeded.response.getVersion()) {
                    tlauncher.getConfiguration().set(ConfigEnum.UPDATER_LAUNCHER, true);
                }
            }
        });
        this.addListener(this.frame);
        this.updaterL = new UpdaterAdapter() {
            @Override
            public void onUpdaterSucceeded(final SearchSucceeded succeeded) {
                final Update upd = succeeded.getResponse();
                if (upd.isApplicable()) {
                    upd.addListener(AutoUpdater.this.updateL);
                    upd.download();
                }
            }
        };
        this.updateL = new UpdateListener() {
            @Override
            public void onUpdateError(final Update u, final Throwable e) {
            }
            
            @Override
            public void onUpdateDownloading(final Update u) {
            }
            
            @Override
            public void onUpdateDownloadError(final Update u, final Throwable e) {
            }
            
            @Override
            public void onUpdateReady(final Update u) {
                u.apply();
            }
            
            @Override
            public void onUpdateApplying(final Update u) {
            }
            
            @Override
            public void onUpdateApplyError(final Update u, final Throwable e) {
                System.exit(0);
            }
        };
        this.addListener(this.updaterL);
    }
    
    public TLauncher getLauncher() {
        return this.t;
    }
    
    @Override
    protected SearchResult findUpdate0() {
        return super.findUpdate0();
    }
    
    public void cancelUpdate() {
        this.t.getDownloader().stopDownload();
    }
    
    public boolean isClosed() {
        return this.frame.isClosed();
    }
}
