// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

import java.util.Collection;
import java.util.Arrays;
import org.tlauncher.util.async.AsyncThread;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.lang.reflect.Type;
import org.tlauncher.util.gson.serializer.UpdateDeserializer;
import com.google.gson.GsonBuilder;
import java.io.Reader;
import org.apache.commons.io.IOUtils;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.util.U;
import java.net.URL;
import net.minecraft.launcher.Http;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

public class Updater
{
    private Update update;
    private final List<UpdaterListener> listeners;
    
    public Updater() {
        this.listeners = Collections.synchronizedList(new ArrayList<UpdaterListener>());
    }
    
    public Update getUpdate() {
        return this.update;
    }
    
    protected SearchResult findUpdate0() {
        SearchResult result = null;
        this.log("Requesting an update...");
        final List<Throwable> errorList = new ArrayList<Throwable>();
        final String get = "?version=" + Http.encode(String.valueOf(TLauncher.getVersion())) + "&client=" + Http.encode(TLauncher.getInstance().getConfiguration().getClient().toString());
        for (final String updateUrl : this.getUpdateUrlList()) {
            final long startTime = System.currentTimeMillis();
            this.log("Requesting from:", updateUrl);
            String response = null;
            try {
                final URL url = new URL(updateUrl + get);
                this.log("Making request:", url);
                final HttpURLConnection connection = Downloadable.setUp(url.openConnection(U.getProxy()), true);
                connection.setDoOutput(true);
                response = IOUtils.toString(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));
                final Update update = new GsonBuilder().registerTypeAdapter(Update.class, new UpdateDeserializer()).create().fromJson(response, Update.class);
                result = new SearchSucceeded(update);
            }
            catch (Exception e) {
                this.log("Failed to request from:", updateUrl, e);
                if (response != null) {
                    this.log("Response:", response);
                }
                result = null;
                errorList.add(e);
            }
            this.log("Request time:", System.currentTimeMillis() - startTime, "ms");
            if (result == null) {
                continue;
            }
            this.log("Successfully requested from:", updateUrl);
            break;
        }
        return (result == null) ? new SearchFailed(errorList) : result;
    }
    
    private SearchResult findUpdate() {
        try {
            final SearchResult result = this.findUpdate0();
            this.dispatchResult(result);
            return result;
        }
        catch (Exception e) {
            this.log(e);
            return null;
        }
    }
    
    public void asyncFindUpdate() {
        AsyncThread.execute(() -> this.findUpdate());
    }
    
    public void addListener(final UpdaterListener l) {
        this.listeners.add(l);
    }
    
    public void removeListener(final UpdaterListener l) {
        this.listeners.remove(l);
    }
    
    private void dispatchResult(final SearchResult result) {
        requireNotNull(result, "result");
        if (result instanceof SearchSucceeded) {
            synchronized (this.listeners) {
                for (final UpdaterListener l : this.listeners) {
                    l.onUpdaterSucceeded((SearchSucceeded)result);
                }
            }
        }
        else {
            if (!(result instanceof SearchFailed)) {
                throw new IllegalArgumentException("unknown result of " + result.getClass());
            }
            synchronized (this.listeners) {
                for (final UpdaterListener l : this.listeners) {
                    l.onUpdaterErrored((SearchFailed)result);
                }
            }
        }
    }
    
    protected void onUpdaterRequests() {
        synchronized (this.listeners) {
            for (final UpdaterListener l : this.listeners) {
                l.onUpdaterRequesting(this);
            }
        }
    }
    
    protected List<String> getUpdateUrlList() {
        return Arrays.asList(TLauncher.getUpdateRepos());
    }
    
    protected void log(final Object... o) {
        U.log("[Updater]", o);
    }
    
    private static <T> T requireNotNull(final T obj, final String name) {
        if (obj == null) {
            throw new NullPointerException(name);
        }
        return obj;
    }
    
    public abstract class SearchResult
    {
        protected final Update response;
        
        public SearchResult(final Update response) {
            this.response = response;
        }
        
        public final Update getResponse() {
            return this.response;
        }
        
        public final Updater getUpdater() {
            return Updater.this;
        }
        
        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "{response=" + this.response + "}";
        }
    }
    
    public class SearchSucceeded extends SearchResult
    {
        public SearchSucceeded(final Update response) {
            super((Update)requireNotNull(response, "response"));
        }
    }
    
    public class SearchFailed extends SearchResult
    {
        protected final List<Throwable> errorList;
        
        public SearchFailed(final List<Throwable> list) {
            super(null);
            this.errorList = new ArrayList<Throwable>();
            for (final Throwable t : list) {
                if (t == null) {
                    throw new NullPointerException();
                }
            }
            this.errorList.addAll(list);
        }
        
        public final List<Throwable> getCauseList() {
            return this.errorList;
        }
        
        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "{errors=" + this.errorList + "}";
        }
    }
}
