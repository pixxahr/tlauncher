// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

import java.util.Map;

public class PointOffer
{
    private String name;
    private boolean active;
    private Map<String, String> texts;
    
    public String getName() {
        return this.name;
    }
    
    public boolean isActive() {
        return this.active;
    }
    
    public Map<String, String> getTexts() {
        return this.texts;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setActive(final boolean active) {
        this.active = active;
    }
    
    public void setTexts(final Map<String, String> texts) {
        this.texts = texts;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof PointOffer)) {
            return false;
        }
        final PointOffer other = (PointOffer)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        Label_0065: {
            if (this$name == null) {
                if (other$name == null) {
                    break Label_0065;
                }
            }
            else if (this$name.equals(other$name)) {
                break Label_0065;
            }
            return false;
        }
        if (this.isActive() != other.isActive()) {
            return false;
        }
        final Object this$texts = this.getTexts();
        final Object other$texts = other.getTexts();
        if (this$texts == null) {
            if (other$texts == null) {
                return true;
            }
        }
        else if (this$texts.equals(other$texts)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof PointOffer;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $name = this.getName();
        result = result * 59 + (($name == null) ? 43 : $name.hashCode());
        result = result * 59 + (this.isActive() ? 79 : 97);
        final Object $texts = this.getTexts();
        result = result * 59 + (($texts == null) ? 43 : $texts.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "PointOffer(name=" + this.getName() + ", active=" + this.isActive() + ", texts=" + this.getTexts() + ")";
    }
}
