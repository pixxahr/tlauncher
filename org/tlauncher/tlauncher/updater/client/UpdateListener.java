// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

public interface UpdateListener
{
    void onUpdateError(final Update p0, final Throwable p1);
    
    void onUpdateDownloading(final Update p0);
    
    void onUpdateDownloadError(final Update p0, final Throwable p1);
    
    void onUpdateReady(final Update p0);
    
    void onUpdateApplying(final Update p0);
    
    void onUpdateApplyError(final Update p0, final Throwable p1);
}
