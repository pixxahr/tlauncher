// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

import java.util.Map;
import java.util.List;

public class Offer
{
    private String client;
    private String offer;
    private String installer;
    private int startCheckboxSouth;
    private List<PointOffer> checkBoxes;
    private Map<String, String> topText;
    private Map<String, String> downText;
    private Map<String, String> args;
    
    public String getClient() {
        return this.client;
    }
    
    public String getOffer() {
        return this.offer;
    }
    
    public String getInstaller() {
        return this.installer;
    }
    
    public int getStartCheckboxSouth() {
        return this.startCheckboxSouth;
    }
    
    public List<PointOffer> getCheckBoxes() {
        return this.checkBoxes;
    }
    
    public Map<String, String> getTopText() {
        return this.topText;
    }
    
    public Map<String, String> getDownText() {
        return this.downText;
    }
    
    public Map<String, String> getArgs() {
        return this.args;
    }
    
    public void setClient(final String client) {
        this.client = client;
    }
    
    public void setOffer(final String offer) {
        this.offer = offer;
    }
    
    public void setInstaller(final String installer) {
        this.installer = installer;
    }
    
    public void setStartCheckboxSouth(final int startCheckboxSouth) {
        this.startCheckboxSouth = startCheckboxSouth;
    }
    
    public void setCheckBoxes(final List<PointOffer> checkBoxes) {
        this.checkBoxes = checkBoxes;
    }
    
    public void setTopText(final Map<String, String> topText) {
        this.topText = topText;
    }
    
    public void setDownText(final Map<String, String> downText) {
        this.downText = downText;
    }
    
    public void setArgs(final Map<String, String> args) {
        this.args = args;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Offer)) {
            return false;
        }
        final Offer other = (Offer)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$client = this.getClient();
        final Object other$client = other.getClient();
        Label_0065: {
            if (this$client == null) {
                if (other$client == null) {
                    break Label_0065;
                }
            }
            else if (this$client.equals(other$client)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$offer = this.getOffer();
        final Object other$offer = other.getOffer();
        Label_0102: {
            if (this$offer == null) {
                if (other$offer == null) {
                    break Label_0102;
                }
            }
            else if (this$offer.equals(other$offer)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$installer = this.getInstaller();
        final Object other$installer = other.getInstaller();
        Label_0139: {
            if (this$installer == null) {
                if (other$installer == null) {
                    break Label_0139;
                }
            }
            else if (this$installer.equals(other$installer)) {
                break Label_0139;
            }
            return false;
        }
        if (this.getStartCheckboxSouth() != other.getStartCheckboxSouth()) {
            return false;
        }
        final Object this$checkBoxes = this.getCheckBoxes();
        final Object other$checkBoxes = other.getCheckBoxes();
        Label_0189: {
            if (this$checkBoxes == null) {
                if (other$checkBoxes == null) {
                    break Label_0189;
                }
            }
            else if (this$checkBoxes.equals(other$checkBoxes)) {
                break Label_0189;
            }
            return false;
        }
        final Object this$topText = this.getTopText();
        final Object other$topText = other.getTopText();
        Label_0226: {
            if (this$topText == null) {
                if (other$topText == null) {
                    break Label_0226;
                }
            }
            else if (this$topText.equals(other$topText)) {
                break Label_0226;
            }
            return false;
        }
        final Object this$downText = this.getDownText();
        final Object other$downText = other.getDownText();
        Label_0263: {
            if (this$downText == null) {
                if (other$downText == null) {
                    break Label_0263;
                }
            }
            else if (this$downText.equals(other$downText)) {
                break Label_0263;
            }
            return false;
        }
        final Object this$args = this.getArgs();
        final Object other$args = other.getArgs();
        if (this$args == null) {
            if (other$args == null) {
                return true;
            }
        }
        else if (this$args.equals(other$args)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof Offer;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $client = this.getClient();
        result = result * 59 + (($client == null) ? 43 : $client.hashCode());
        final Object $offer = this.getOffer();
        result = result * 59 + (($offer == null) ? 43 : $offer.hashCode());
        final Object $installer = this.getInstaller();
        result = result * 59 + (($installer == null) ? 43 : $installer.hashCode());
        result = result * 59 + this.getStartCheckboxSouth();
        final Object $checkBoxes = this.getCheckBoxes();
        result = result * 59 + (($checkBoxes == null) ? 43 : $checkBoxes.hashCode());
        final Object $topText = this.getTopText();
        result = result * 59 + (($topText == null) ? 43 : $topText.hashCode());
        final Object $downText = this.getDownText();
        result = result * 59 + (($downText == null) ? 43 : $downText.hashCode());
        final Object $args = this.getArgs();
        result = result * 59 + (($args == null) ? 43 : $args.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Offer(client=" + this.getClient() + ", offer=" + this.getOffer() + ", installer=" + this.getInstaller() + ", startCheckboxSouth=" + this.getStartCheckboxSouth() + ", checkBoxes=" + this.getCheckBoxes() + ", topText=" + this.getTopText() + ", downText=" + this.getDownText() + ", args=" + this.getArgs() + ")";
    }
}
