// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

public class Banner
{
    private String image;
    private String clickLink;
    
    public String getImage() {
        return this.image;
    }
    
    public String getClickLink() {
        return this.clickLink;
    }
    
    public void setImage(final String image) {
        this.image = image;
    }
    
    public void setClickLink(final String clickLink) {
        this.clickLink = clickLink;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Banner)) {
            return false;
        }
        final Banner other = (Banner)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$image = this.getImage();
        final Object other$image = other.getImage();
        Label_0065: {
            if (this$image == null) {
                if (other$image == null) {
                    break Label_0065;
                }
            }
            else if (this$image.equals(other$image)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$clickLink = this.getClickLink();
        final Object other$clickLink = other.getClickLink();
        if (this$clickLink == null) {
            if (other$clickLink == null) {
                return true;
            }
        }
        else if (this$clickLink.equals(other$clickLink)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof Banner;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $image = this.getImage();
        result = result * 59 + (($image == null) ? 43 : $image.hashCode());
        final Object $clickLink = this.getClickLink();
        result = result * 59 + (($clickLink == null) ? 43 : $clickLink.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Banner(image=" + this.getImage() + ", clickLink=" + this.getClickLink() + ")";
    }
}
