// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

import com.google.gson.JsonArray;
import java.util.Iterator;
import com.google.gson.JsonObject;
import org.tlauncher.util.Reflect;
import org.tlauncher.util.IntegerArray;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.regex.Pattern;
import com.google.gson.JsonParseException;
import org.tlauncher.util.U;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import com.google.gson.JsonDeserializer;
import org.apache.commons.lang3.StringUtils;
import java.util.Arrays;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import java.net.URL;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Notices
{
    private final Map<String, NoticeList> map;
    private final Map<String, NoticeList> unmodifiable;
    
    public Notices() {
        this.map = new HashMap<String, NoticeList>();
        this.unmodifiable = Collections.unmodifiableMap((Map<? extends String, ? extends NoticeList>)this.map);
    }
    
    public final Map<String, NoticeList> getMap() {
        return this.unmodifiable;
    }
    
    protected final Map<String, NoticeList> map() {
        return this.map;
    }
    
    public final NoticeList getByName(final String name) {
        return this.map.get(name);
    }
    
    protected void add(final NoticeList list) {
        if (list == null) {
            throw new NullPointerException("list");
        }
        this.map.put(list.name, list);
    }
    
    protected void add(final String listName, final Notice notice) {
        if (notice == null) {
            throw new NullPointerException("notice");
        }
        NoticeList list = this.map.get(listName);
        final boolean add = list == null;
        if (add) {
            list = new NoticeList(listName);
        }
        list.add(notice);
        if (add) {
            this.add(list);
        }
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + this.map;
    }
    
    private static String parseImage(final String image) {
        if (image == null) {
            return null;
        }
        if (image.startsWith("data:image")) {
            return image;
        }
        final URL url = ImageCache.getRes(image);
        return (url == null) ? null : url.toString();
    }
    
    public static class NoticeList
    {
        private final String name;
        private final List<Notice> list;
        private final List<Notice> unmodifiable;
        private final Notice[] chances;
        private int totalChance;
        
        public NoticeList(final String name) {
            this.list = new ArrayList<Notice>();
            this.unmodifiable = Collections.unmodifiableList((List<? extends Notice>)this.list);
            this.chances = new Notice[100];
            this.totalChance = 0;
            if (name == null) {
                throw new NullPointerException("name");
            }
            if (name.isEmpty()) {
                throw new IllegalArgumentException("name is empty");
            }
            this.name = name;
        }
        
        public final String getName() {
            return this.name;
        }
        
        public final List<Notice> getList() {
            return this.unmodifiable;
        }
        
        protected final List<Notice> list() {
            return this.list;
        }
        
        public final Notice getRandom() {
            return this.chances[new Random().nextInt(100)];
        }
        
        protected void add(final Notice notice) {
            if (notice == null) {
                throw new NullPointerException();
            }
            if (this.totalChance + notice.chance > 100) {
                throw new IllegalArgumentException("chance overflow: " + (this.totalChance + notice.chance));
            }
            this.list.add(notice);
            Arrays.fill(this.chances, this.totalChance, this.totalChance + notice.chance, notice);
            this.totalChance += notice.chance;
        }
        
        @Override
        public String toString() {
            return this.getClass().getSimpleName() + this.list();
        }
    }
    
    public static class Notice
    {
        private String content;
        private int chance;
        private NoticeType type;
        private int[] size;
        private String image;
        
        public Notice() {
            this.chance = 100;
            this.type = NoticeType.NOTICE;
            this.size = new int[2];
        }
        
        public final int getChance() {
            return this.chance;
        }
        
        public final void setChance(final int chance) {
            if (chance < 1 || chance > 100) {
                throw new IllegalArgumentException("illegal chance: " + chance);
            }
            this.chance = chance;
        }
        
        public final String getContent() {
            return this.content;
        }
        
        public final void setContent(final String content) {
            if (StringUtils.isBlank((CharSequence)content)) {
                throw new IllegalArgumentException("content is empty or is null");
            }
            this.content = content;
        }
        
        public final NoticeType getType() {
            return this.type;
        }
        
        public final void setType(final NoticeType type) {
            this.type = type;
        }
        
        public final int[] getSize() {
            return this.size.clone();
        }
        
        public final void setSize(final int[] size) {
            if (size == null) {
                throw new NullPointerException();
            }
            if (size.length != 2) {
                throw new IllegalArgumentException("illegal length");
            }
            this.setWidth(size[0]);
            this.setHeight(size[1]);
        }
        
        public final int getWidth() {
            return this.size[0];
        }
        
        public final void setWidth(final int width) {
            if (width < 1) {
                throw new IllegalArgumentException("width must be greater than 0");
            }
            this.size[0] = width;
        }
        
        public final int getHeight() {
            return this.size[1];
        }
        
        public final void setHeight(final int height) {
            if (height < 1) {
                throw new IllegalArgumentException("height must be greater than 0");
            }
            this.size[1] = height;
        }
        
        public final String getImage() {
            return this.image;
        }
        
        public final void setImage(final String image) {
            this.image = (StringUtils.isBlank((CharSequence)image) ? null : parseImage(image));
        }
        
        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append(this.getClass().getSimpleName()).append("{").append("size=").append(this.size[0]).append('x').append(this.size[1]).append(';').append("chance=").append(this.chance).append(';').append("content=\"");
            if (this.content.length() < 50) {
                builder.append(this.content);
            }
            else {
                builder.append(this.content.substring(0, 46)).append("...");
            }
            builder.append("\";").append("image=");
            if (this.image != null && this.image.length() > 24) {
                builder.append(this.image.substring(0, 22)).append("...");
            }
            else {
                builder.append(this.image);
            }
            builder.append('}');
            return builder.toString();
        }
    }
    
    public enum NoticeType
    {
        NOTICE(false), 
        WARNING(false), 
        AD_SERVER, 
        AD_YOUTUBE, 
        AD_OTHER;
        
        private final boolean advert;
        
        private NoticeType(final boolean advert) {
            this.advert = advert;
        }
        
        private NoticeType() {
            this(true);
        }
        
        public boolean isAdvert() {
            return this.advert;
        }
    }
    
    public static class Deserializer implements JsonDeserializer<Notices>
    {
        @Override
        public Notices deserialize(final JsonElement root, final Type type, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return this.deserialize0(root);
            }
            catch (Exception e) {
                U.log("Cannot parse notices:", e);
                return new Notices();
            }
        }
        
        private Notices deserialize0(final JsonElement root) throws JsonParseException {
            final Notices notices = new Notices();
            final JsonObject rootObject = root.getAsJsonObject();
            for (final Map.Entry<String, JsonElement> entry : rootObject.entrySet()) {
                final String listName = entry.getKey();
                final JsonArray ntArray = entry.getValue().getAsJsonArray();
                for (final JsonElement elem : ntArray) {
                    final JsonObject ntObj = elem.getAsJsonObject();
                    if (ntObj.has("version")) {
                        final String version = ntObj.get("version").getAsString();
                        final Pattern pattern = Pattern.compile(version);
                        if (!pattern.matcher(String.valueOf(TLauncher.getVersion())).matches()) {
                            continue;
                        }
                    }
                    final Notice notice = new Notice();
                    notice.setContent(ntObj.get("content").getAsString());
                    notice.setSize(IntegerArray.parseIntegerArray(ntObj.get("size").getAsString(), 'x').toArray());
                    if (ntObj.has("chance")) {
                        notice.setChance(ntObj.get("chance").getAsInt());
                    }
                    if (ntObj.has("type")) {
                        notice.setType(Reflect.parseEnum(NoticeType.class, ntObj.get("type").getAsString()));
                    }
                    if (ntObj.has("image")) {
                        notice.setImage(ntObj.get("image").getAsString());
                    }
                    notices.add(listName, notice);
                }
            }
            if (notices.getByName("uk_UA") == null && notices.getByName("ru_RU") != null) {
                for (final Notice notice2 : notices.getByName("ru_RU").getList()) {
                    notices.add("uk_UA", notice2);
                }
            }
            return notices;
        }
    }
}
