// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

public interface UpdaterListener
{
    void onUpdaterRequesting(final Updater p0);
    
    void onUpdaterErrored(final Updater.SearchFailed p0);
    
    void onUpdaterSucceeded(final Updater.SearchSucceeded p0);
}
