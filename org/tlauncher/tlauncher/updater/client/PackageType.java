// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

import org.tlauncher.util.FileUtil;

public enum PackageType
{
    EXE, 
    JAR;
    
    public static final PackageType CURRENT;
    
    public String toLowerCase() {
        return this.name().toLowerCase();
    }
    
    public static boolean isCurrent(final PackageType pt) {
        return pt == PackageType.CURRENT;
    }
    
    static {
        CURRENT = (FileUtil.getRunningJar().toString().endsWith(".exe") ? PackageType.EXE : PackageType.JAR);
    }
}
