// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

public abstract class UpdaterAdapter implements UpdaterListener
{
    @Override
    public void onUpdaterRequesting(final Updater u) {
    }
    
    @Override
    public void onUpdaterErrored(final Updater.SearchFailed failed) {
    }
    
    @Override
    public void onUpdaterSucceeded(final Updater.SearchSucceeded succeeded) {
    }
}
