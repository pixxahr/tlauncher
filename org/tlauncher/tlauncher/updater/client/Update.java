// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.updater.client;

import org.tlauncher.util.U;
import java.util.Iterator;
import org.tlauncher.util.DoubleRunningTimer;
import org.apache.commons.io.FileUtils;
import java.io.OutputStream;
import java.io.InputStream;
import net.minecraft.launcher.process.JavaProcessLauncher;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import org.tlauncher.tlauncher.rmo.Bootstrapper;
import java.util.Random;
import java.net.MalformedURLException;
import org.tlauncher.tlauncher.downloader.DownloadableHandler;
import java.net.URL;
import java.io.File;
import org.tlauncher.util.FileUtil;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Collection;
import java.util.HashMap;
import java.util.Collections;
import java.util.ArrayList;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.downloader.Downloader;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.gson.annotations.Expose;
import java.util.List;

public class Update
{
    protected double version;
    protected double requiredAtLeastFor;
    @Expose(serialize = false, deserialize = false)
    private final List<UpdateListener> listeners;
    protected boolean mandatory;
    protected List<String> jarLinks;
    protected List<String> exeLinks;
    protected List<String> triedToDownload;
    protected AtomicBoolean stateDownloading;
    protected Map<String, String> description;
    private int updaterView;
    private boolean updaterLaterInstall;
    private List<Offer> offers;
    private int offerDelay;
    private int offerEmptyCheckboxDelay;
    private Map<String, List<Banner>> banners;
    private List<String> rootAccessExe;
    @Expose(serialize = false, deserialize = false)
    protected boolean userChoose;
    @Expose(serialize = false, deserialize = false)
    protected boolean freeSpaceEnough;
    @Expose(serialize = false, deserialize = false)
    protected State state;
    @Expose(serialize = false, deserialize = false)
    protected Downloader downloader;
    @Expose(serialize = false, deserialize = false)
    private Downloadable download;
    
    public void setDescription(final Map<String, String> description) {
        this.description = description;
    }
    
    public Update() {
        this.listeners = Collections.synchronizedList(new ArrayList<UpdateListener>());
        this.jarLinks = new ArrayList<String>();
        this.exeLinks = new ArrayList<String>();
        this.triedToDownload = new ArrayList<String>();
        this.description = new HashMap<String, String>();
        this.userChoose = true;
        this.freeSpaceEnough = true;
        this.state = State.NONE;
        this.downloader = this.getDefaultDownloader();
    }
    
    public Update(final double version, final Map<String, String> description, final List<String> jarList, final List<String> exeList) {
        this.listeners = Collections.synchronizedList(new ArrayList<UpdateListener>());
        this.jarLinks = new ArrayList<String>();
        this.exeLinks = new ArrayList<String>();
        this.triedToDownload = new ArrayList<String>();
        this.description = new HashMap<String, String>();
        this.userChoose = true;
        this.freeSpaceEnough = true;
        this.state = State.NONE;
        this.downloader = this.getDefaultDownloader();
        this.version = version;
        if (description != null) {
            this.description.putAll(description);
        }
        if (exeList != null) {
            this.exeLinks.addAll(exeList);
        }
        if (jarList != null) {
            this.jarLinks.addAll(jarList);
        }
    }
    
    public double getVersion() {
        return this.version;
    }
    
    public void setVersion(final double version) {
        this.version = version;
    }
    
    public double getRequiredAtLeastFor() {
        return this.requiredAtLeastFor;
    }
    
    public void setRequiredAtLeastFor(final double version) {
        this.requiredAtLeastFor = version;
    }
    
    public Map<String, String> getDescriptionMap() {
        return this.description;
    }
    
    public State getState() {
        return this.state;
    }
    
    protected void setState(final State newState) {
        if (newState.ordinal() <= this.state.ordinal() && this.state.ordinal() != State.values().length - 1) {
            throw new IllegalStateException("tried to change from " + this.state + " to " + newState);
        }
        this.state = newState;
        this.log("Set state:", newState);
    }
    
    public Downloader getDownloader() {
        return this.downloader;
    }
    
    public void setDownloader(final Downloader downloader) {
        this.downloader = downloader;
    }
    
    public boolean isApplicable() {
        return this.freeSpaceEnough && this.userChoose && TLauncher.getVersion() < this.version;
    }
    
    public boolean isRequired() {
        return this.userChoose && this.requiredAtLeastFor != 0.0 && TLauncher.getVersion() <= this.requiredAtLeastFor;
    }
    
    public String getDescription(final String key) {
        return (this.description == null) ? null : this.description.get(key);
    }
    
    public String getDescription() {
        return this.getDescription(TLauncher.getInstance().getConfiguration().getLocale().toString());
    }
    
    protected void download0(final PackageType packageType, final boolean async) throws Throwable {
        this.setState(State.DOWNLOADING);
        final File destination = new File(FileUtil.getRunningJar().getAbsolutePath() + ".update");
        destination.deleteOnExit();
        this.log("dest", destination);
        this.onUpdateDownloading();
        this.individualUpdate(packageType, async, destination);
    }
    
    private void individualUpdate(final PackageType packageType, final boolean async, final File destination) throws MalformedURLException {
        final String pathServer = this.getLink(packageType);
        final URL url = new URL(pathServer);
        this.log("url:", url);
        (this.download = new Downloadable(url.toExternalForm(), destination)).setInsertUA(true);
        if (this.triedToDownload.size() == this.calculateListSize(packageType)) {
            this.download.addHandler(new DownloadableHandler() {
                @Override
                public void onStart(final Downloadable d) {
                }
                
                @Override
                public void onAbort(final Downloadable d) {
                    Update.this.onUpdateDownloadError(d.getError());
                }
                
                @Override
                public void onComplete(final Downloadable d) {
                    Update.this.onUpdateReady();
                }
                
                @Override
                public void onError(final Downloadable d, final Throwable e) {
                    Update.this.onUpdateDownloadError(e);
                }
            });
        }
        else {
            this.download.addHandler(new DownloadableHandler() {
                @Override
                public void onStart(final Downloadable d) {
                }
                
                @Override
                public void onAbort(final Downloadable d) {
                    Update.this.onUpdateDownloadError(d.getError());
                }
                
                @Override
                public void onComplete(final Downloadable d) {
                    Update.this.onUpdateReady();
                }
                
                @Override
                public void onError(final Downloadable d, final Throwable e) {
                    Update.this.log(e);
                    Update.this.stateDownloading.set(false);
                }
            });
        }
        this.downloader.add(this.download);
        this.stateDownloading = new AtomicBoolean(true);
        if (async) {
            this.downloader.startDownload();
        }
        else {
            this.downloader.startDownloadAndWait();
        }
        if (!this.stateDownloading.get() && this.triedToDownload.size() != this.calculateListSize(packageType)) {
            this.individualUpdate(packageType, async, destination);
        }
    }
    
    private String getLink(final PackageType packageType) {
        switch (packageType) {
            case EXE: {
                return this.findLink(this.exeLinks);
            }
            case JAR: {
                return this.findLink(this.jarLinks);
            }
            default: {
                throw new NullPointerException("incorrect PackageType");
            }
        }
    }
    
    private int calculateListSize(final PackageType packageType) {
        switch (packageType) {
            case EXE: {
                return this.exeLinks.size();
            }
            case JAR: {
                return this.jarLinks.size();
            }
            default: {
                throw new NullPointerException("incorrect PackageType");
            }
        }
    }
    
    private String findLink(final List<String> list) {
        final Random r = new Random();
        String link;
        do {
            link = list.get(r.nextInt(list.size()));
        } while (this.triedToDownload.contains(link));
        this.triedToDownload.add(link);
        return link;
    }
    
    public void download(final PackageType type, final boolean async) {
        try {
            this.download0(type, async);
        }
        catch (Throwable t) {
            this.onUpdateError(t);
        }
    }
    
    public void download(final boolean async) {
        this.download(PackageType.CURRENT, async);
    }
    
    public void download() {
        this.download(false);
    }
    
    public void asyncDownload() {
        this.download(true);
    }
    
    protected void apply0() throws Throwable {
        this.setState(State.APPLYING);
        final JavaProcessLauncher javaProcessLauncher = Bootstrapper.restartLauncher();
        final File replace = FileUtil.getRunningJar();
        final File replaceWith = this.download.getDestination();
        final ProcessBuilder builder = javaProcessLauncher.createProcess();
        this.onUpdateApplying();
        final InputStream in = new FileInputStream(replaceWith);
        final OutputStream out = new FileOutputStream(replace);
        final byte[] buffer = new byte[2048];
        for (int read = in.read(buffer); read > 0; read = in.read(buffer)) {
            out.write(buffer, 0, read);
        }
        try {
            in.close();
        }
        catch (IOException ex) {}
        try {
            out.close();
        }
        catch (IOException ex2) {}
        try {
            builder.start();
        }
        catch (Throwable t) {
            this.log(t);
        }
        TLauncher.kill();
    }
    
    public void apply() {
        try {
            final File file = FileUtil.getRunningJar();
            FileUtils.copyFile(file, new File(file.getParentFile(), "Old-" + file.getName()));
            ((DoubleRunningTimer)TLauncher.getInjector().getInstance((Class)DoubleRunningTimer.class)).clearTimeLabel();
            this.apply0();
        }
        catch (Throwable t) {
            this.onUpdateApplyError(t);
        }
    }
    
    public void addListener(final UpdateListener l) {
        this.listeners.add(l);
    }
    
    public void removeListener(final UpdateListener l) {
        this.listeners.remove(l);
    }
    
    protected void onUpdateError(final Throwable e) {
        this.setState(State.ERRORED);
        synchronized (this.listeners) {
            for (final UpdateListener l : this.listeners) {
                l.onUpdateError(this, e);
            }
        }
    }
    
    protected void onUpdateDownloading() {
        synchronized (this.listeners) {
            for (final UpdateListener l : this.listeners) {
                l.onUpdateDownloading(this);
            }
        }
    }
    
    protected void onUpdateDownloadError(final Throwable e) {
        this.setState(State.ERRORED);
        synchronized (this.listeners) {
            for (final UpdateListener l : this.listeners) {
                l.onUpdateDownloadError(this, e);
            }
        }
    }
    
    protected void onUpdateReady() {
        this.setState(State.READY);
        synchronized (this.listeners) {
            for (final UpdateListener l : this.listeners) {
                l.onUpdateReady(this);
            }
        }
    }
    
    protected void onUpdateApplying() {
        synchronized (this.listeners) {
            for (final UpdateListener l : this.listeners) {
                l.onUpdateApplying(this);
            }
        }
    }
    
    protected void onUpdateApplyError(final Throwable e) {
        this.setState(State.ERRORED);
        synchronized (this.listeners) {
            for (final UpdateListener l : this.listeners) {
                l.onUpdateApplyError(this, e);
            }
        }
    }
    
    protected Downloader getDefaultDownloader() {
        return TLauncher.getInstance().getDownloader();
    }
    
    protected void log(final Object... o) {
        U.log("[Update:" + this.version + "]", o);
    }
    
    public List<String> getJarLinks() {
        return this.jarLinks;
    }
    
    public boolean isMandatory() {
        return this.mandatory;
    }
    
    public void setMandatory(final boolean mandatory) {
        this.mandatory = mandatory;
    }
    
    public boolean isUserChoose() {
        return this.userChoose;
    }
    
    public void setUserChoose(final boolean userChoose) {
        this.userChoose = userChoose;
    }
    
    public String getlastDownloadedLink() {
        if (this.triedToDownload.size() > 0) {
            return this.triedToDownload.get(this.triedToDownload.size() - 1);
        }
        return "";
    }
    
    public boolean isFreeSpaceEnough() {
        return this.freeSpaceEnough;
    }
    
    public void setFreeSpaceEnough(final boolean freeSpaceEnough) {
        this.freeSpaceEnough = freeSpaceEnough;
    }
    
    public void setJarLinks(final List<String> jarLinks) {
        this.jarLinks = jarLinks;
    }
    
    public List<String> getExeLinks() {
        return this.exeLinks;
    }
    
    public void setExeLinks(final List<String> exeLinks) {
        this.exeLinks = exeLinks;
    }
    
    public List<Offer> getOffers() {
        return this.offers;
    }
    
    public void setOffers(final List<Offer> offers) {
        this.offers = offers;
    }
    
    public Map<String, List<Banner>> getBanners() {
        return this.banners;
    }
    
    public int getUpdaterView() {
        return this.updaterView;
    }
    
    public void setBanners(final Map<String, List<Banner>> banners) {
        this.banners = banners;
    }
    
    public void setUpdaterView(final int updaterView) {
        this.updaterView = updaterView;
    }
    
    public boolean isUpdaterLaterInstall() {
        return this.updaterLaterInstall;
    }
    
    public void setUpdaterLaterInstall(final boolean updaterLaterInstall) {
        this.updaterLaterInstall = updaterLaterInstall;
    }
    
    public int getOfferDelay() {
        return this.offerDelay;
    }
    
    public void setOfferDelay(final int offerDelay) {
        this.offerDelay = offerDelay;
    }
    
    public int getOfferEmptyCheckboxDelay() {
        return this.offerEmptyCheckboxDelay;
    }
    
    public void setOfferEmptyCheckboxDelay(final int offerEmptyCheckboxDelay) {
        this.offerEmptyCheckboxDelay = offerEmptyCheckboxDelay;
    }
    
    public List<String> getRootAccessExe() {
        return this.rootAccessExe;
    }
    
    public void setRootAccessExe(final List<String> rootAccessExe) {
        this.rootAccessExe = rootAccessExe;
    }
    
    public enum State
    {
        NONE, 
        DOWNLOADING, 
        READY, 
        APPLYING, 
        ERRORED;
    }
}
