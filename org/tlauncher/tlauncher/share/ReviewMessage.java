// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.share;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class ReviewMessage implements Serializable
{
    private static final long serialVersionUID = 5899496321871739561L;
    private String title;
    private String description;
    private String mailReview;
    private String typeReview;
    private List<String> listNamesOfFiles;
    
    public ReviewMessage(final String title, final String description, final String mailReview, final String typeReview, final List<String> listNamesOfFiles) {
        this.title = title;
        this.description = description;
        this.mailReview = mailReview;
        this.typeReview = typeReview;
        this.listNamesOfFiles = listNamesOfFiles;
    }
    
    public ReviewMessage() {
        this.listNamesOfFiles = new ArrayList<String>();
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public String getMailReview() {
        return this.mailReview;
    }
    
    public void setMailReview(final String mailReview) {
        this.mailReview = mailReview;
    }
    
    public String getTypeReview() {
        return this.typeReview;
    }
    
    public void setTypeReview(final String typeReview) {
        this.typeReview = typeReview;
    }
    
    public List<String> getListNamesOfFiles() {
        return this.listNamesOfFiles;
    }
    
    public void setListNamesOfFiles(final List<String> listNamesOfFiles) {
        this.listNamesOfFiles = listNamesOfFiles;
    }
}
