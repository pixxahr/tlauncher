// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.site.play;

import javax.net.ssl.SSLServerSocketFactory;
import java.net.Socket;
import net.minecraft.launcher.Http;
import com.google.gson.Gson;
import org.tlauncher.tlauncher.entity.server.SiteServer;
import javax.net.ssl.SSLSocket;
import org.tlauncher.util.U;
import javax.net.ssl.SSLServerSocket;
import java.security.SecureRandom;
import javax.net.ssl.TrustManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.KeyManagerFactory;
import java.security.KeyStore;
import org.tlauncher.tlauncher.managers.VersionManager;
import com.google.inject.Inject;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.managers.VersionManagerListener;

public class SitePlay implements VersionManagerListener
{
    private boolean status;
    @Inject
    private TLauncher tLauncher;
    
    public SitePlay() {
        this.status = false;
    }
    
    @Override
    public void onVersionsRefreshing(final VersionManager manager) {
    }
    
    @Override
    public void onVersionsRefreshingFailed(final VersionManager manager) {
        this.init();
    }
    
    @Override
    public void onVersionsRefreshed(final VersionManager manager) {
        this.init();
    }
    
    private synchronized void init() {
        if (this.status) {
            return;
        }
        this.status = true;
        final Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                SitePlay.this.tLauncher;
                final String[] array;
                final String[] ports = array = TLauncher.getInnerSettings().getArray("local.ports.client.play");
                final int length = array.length;
                int i = 0;
                while (i < length) {
                    final String port = array[i];
                    final char[] ksPass = "test123123".toCharArray();
                    final char[] ctPass = "test123123".toCharArray();
                    try {
                        final KeyStore ks = KeyStore.getInstance("JKS");
                        ks.load(TLauncher.class.getResource("/play_game_server").openStream(), ksPass);
                        final KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
                        kmf.init(ks, ctPass);
                        final SSLContext sc = SSLContext.getInstance("TLS");
                        sc.init(kmf.getKeyManagers(), null, null);
                        final SSLServerSocketFactory ssf = sc.getServerSocketFactory();
                        final SSLServerSocket s = (SSLServerSocket)ssf.createServerSocket(Integer.valueOf(port));
                        s.setEnabledCipherSuites(sc.getServerSocketFactory().getSupportedCipherSuites());
                        U.log("run server on ", port);
                        while (true) {
                            final SSLSocket socket = (SSLSocket)s.accept();
                            final SiteServer server = new Gson().fromJson(Http.readBody(socket), SiteServer.class);
                            SitePlay.this.tLauncher.getFrame().setAlwaysOnTop(true);
                            SitePlay.this.tLauncher.getFrame().setAlwaysOnTop(false);
                            SitePlay.this.tLauncher.getFrame().mp.defaultScene.loginForm.startLauncher(server);
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        ++i;
                        continue;
                    }
                    break;
                }
            }
        });
        th.setDaemon(true);
        th.start();
    }
}
