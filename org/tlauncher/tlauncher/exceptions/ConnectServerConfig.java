// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions;

public class ConnectServerConfig extends Exception
{
    private static final long serialVersionUID = 2977918735746519247L;
    
    public ConnectServerConfig(final String name) {
        super(name);
    }
    
    public ConnectServerConfig(final String name, final Throwable er) {
        super(name, er);
    }
}
