// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions;

public class GameEntityNotFound extends Exception
{
    public GameEntityNotFound(final String line) {
        super(line);
    }
}
