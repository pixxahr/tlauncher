// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions;

public class TLauncherException extends RuntimeException
{
    private static final long serialVersionUID = 5812333186574527445L;
    
    public TLauncherException(final String message, final Throwable e) {
        super(message, e);
        e.printStackTrace();
    }
    
    public TLauncherException(final String message) {
        super(message);
    }
}
