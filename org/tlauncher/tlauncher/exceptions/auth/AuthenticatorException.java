// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions.auth;

import org.tlauncher.tlauncher.entity.auth.Response;

public class AuthenticatorException extends Exception
{
    private static final long serialVersionUID = -6773418626800336871L;
    private Response response;
    private String langpath;
    
    public AuthenticatorException(final Throwable cause) {
        super(cause);
    }
    
    public AuthenticatorException(final String message, final String langpath) {
        super(message);
        this.langpath = langpath;
    }
    
    public AuthenticatorException(final Response response, final String langpath) {
        super(response.getErrorMessage());
        this.response = response;
        this.langpath = langpath;
    }
    
    public AuthenticatorException(final String message, final String langpath, final Throwable cause) {
        super(message, cause);
        this.langpath = langpath;
    }
    
    public String getLangpath() {
        return this.langpath;
    }
    
    public Response getResponse() {
        return this.response;
    }
}
