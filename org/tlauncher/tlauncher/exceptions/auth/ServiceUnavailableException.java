// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions.auth;

public class ServiceUnavailableException extends AuthenticatorException
{
    public ServiceUnavailableException(final String message) {
        super(message, "unavailable");
    }
}
