// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions.auth;

public class InvalidCredentialsException extends AuthenticatorException
{
    private static final long serialVersionUID = 7221509839484990453L;
    
    public InvalidCredentialsException() {
        super("Invalid user / password / token", "relogin");
    }
}
