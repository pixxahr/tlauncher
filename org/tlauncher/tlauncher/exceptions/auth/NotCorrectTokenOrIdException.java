// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions.auth;

public class NotCorrectTokenOrIdException extends AuthenticatorException
{
    public NotCorrectTokenOrIdException() {
        super("Invalid client id or token", "authorization");
    }
}
