// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions.auth;

public class UserMigratedException extends AuthenticatorException
{
    private static final long serialVersionUID = 7441756035466353515L;
    
    public UserMigratedException() {
        super("This user has migrated", "migrated");
    }
}
