// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.exceptions.auth;

public class NotCorrectPasswordOrLogingException extends AuthenticatorException
{
    public NotCorrectPasswordOrLogingException() {
        super("Invalid user or password", "relogin");
    }
}
