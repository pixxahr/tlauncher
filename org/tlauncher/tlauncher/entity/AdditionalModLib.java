// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity;

public class AdditionalModLib extends TLauncherLib
{
    private boolean activeCustomUserTexture;
    
    public boolean isActiveCustomUserTexture() {
        return this.activeCustomUserTexture;
    }
    
    public void setActiveCustomUserTexture(final boolean activeCustomUserTexture) {
        this.activeCustomUserTexture = activeCustomUserTexture;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AdditionalModLib)) {
            return false;
        }
        final AdditionalModLib other = (AdditionalModLib)o;
        return other.canEqual(this) && this.isActiveCustomUserTexture() == other.isActiveCustomUserTexture();
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof AdditionalModLib;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * 59 + (this.isActiveCustomUserTexture() ? 79 : 97);
        return result;
    }
    
    @Override
    public String toString() {
        return "AdditionalModLib(super=" + super.toString() + ", activeCustomUserTexture=" + this.isActiveCustomUserTexture() + ")";
    }
}
