// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.auth;

import org.tlauncher.tlauncher.minecraft.auth.Authenticator;
import org.tlauncher.tlauncher.minecraft.auth.GameProfile;

public class RefreshRequest extends Request
{
    private String accessToken;
    private GameProfile selectedProfile;
    private boolean requestUser;
    
    public RefreshRequest(final Authenticator auth) {
        this.requestUser = true;
        this.setClientToken(Authenticator.getClientToken().toString());
        this.setAccessToken(auth.getAccount().getAccessToken());
    }
    
    public String getAccessToken() {
        return this.accessToken;
    }
    
    public GameProfile getSelectedProfile() {
        return this.selectedProfile;
    }
    
    public boolean isRequestUser() {
        return this.requestUser;
    }
    
    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }
    
    public void setSelectedProfile(final GameProfile selectedProfile) {
        this.selectedProfile = selectedProfile;
    }
    
    public void setRequestUser(final boolean requestUser) {
        this.requestUser = requestUser;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof RefreshRequest)) {
            return false;
        }
        final RefreshRequest other = (RefreshRequest)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$accessToken = this.getAccessToken();
        final Object other$accessToken = other.getAccessToken();
        Label_0065: {
            if (this$accessToken == null) {
                if (other$accessToken == null) {
                    break Label_0065;
                }
            }
            else if (this$accessToken.equals(other$accessToken)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$selectedProfile = this.getSelectedProfile();
        final Object other$selectedProfile = other.getSelectedProfile();
        if (this$selectedProfile == null) {
            if (other$selectedProfile == null) {
                return this.isRequestUser() == other.isRequestUser();
            }
        }
        else if (this$selectedProfile.equals(other$selectedProfile)) {
            return this.isRequestUser() == other.isRequestUser();
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof RefreshRequest;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $accessToken = this.getAccessToken();
        result = result * 59 + (($accessToken == null) ? 43 : $accessToken.hashCode());
        final Object $selectedProfile = this.getSelectedProfile();
        result = result * 59 + (($selectedProfile == null) ? 43 : $selectedProfile.hashCode());
        result = result * 59 + (this.isRequestUser() ? 79 : 97);
        return result;
    }
    
    @Override
    public String toString() {
        return "RefreshRequest(accessToken=" + this.getAccessToken() + ", selectedProfile=" + this.getSelectedProfile() + ", requestUser=" + this.isRequestUser() + ")";
    }
}
