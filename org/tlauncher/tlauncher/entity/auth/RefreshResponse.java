// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.auth;

import org.tlauncher.tlauncher.minecraft.auth.User;
import org.tlauncher.tlauncher.minecraft.auth.GameProfile;

public class RefreshResponse extends Response
{
    private String accessToken;
    private String clientToken;
    private GameProfile selectedProfile;
    private User user;
    
    public String getAccessToken() {
        return this.accessToken;
    }
    
    public String getClientToken() {
        return this.clientToken;
    }
    
    public GameProfile getSelectedProfile() {
        return this.selectedProfile;
    }
    
    public User getUser() {
        return this.user;
    }
    
    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }
    
    public void setClientToken(final String clientToken) {
        this.clientToken = clientToken;
    }
    
    public void setSelectedProfile(final GameProfile selectedProfile) {
        this.selectedProfile = selectedProfile;
    }
    
    public void setUser(final User user) {
        this.user = user;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof RefreshResponse)) {
            return false;
        }
        final RefreshResponse other = (RefreshResponse)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$accessToken = this.getAccessToken();
        final Object other$accessToken = other.getAccessToken();
        Label_0065: {
            if (this$accessToken == null) {
                if (other$accessToken == null) {
                    break Label_0065;
                }
            }
            else if (this$accessToken.equals(other$accessToken)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$clientToken = this.getClientToken();
        final Object other$clientToken = other.getClientToken();
        Label_0102: {
            if (this$clientToken == null) {
                if (other$clientToken == null) {
                    break Label_0102;
                }
            }
            else if (this$clientToken.equals(other$clientToken)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$selectedProfile = this.getSelectedProfile();
        final Object other$selectedProfile = other.getSelectedProfile();
        Label_0139: {
            if (this$selectedProfile == null) {
                if (other$selectedProfile == null) {
                    break Label_0139;
                }
            }
            else if (this$selectedProfile.equals(other$selectedProfile)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$user = this.getUser();
        final Object other$user = other.getUser();
        if (this$user == null) {
            if (other$user == null) {
                return true;
            }
        }
        else if (this$user.equals(other$user)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof RefreshResponse;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $accessToken = this.getAccessToken();
        result = result * 59 + (($accessToken == null) ? 43 : $accessToken.hashCode());
        final Object $clientToken = this.getClientToken();
        result = result * 59 + (($clientToken == null) ? 43 : $clientToken.hashCode());
        final Object $selectedProfile = this.getSelectedProfile();
        result = result * 59 + (($selectedProfile == null) ? 43 : $selectedProfile.hashCode());
        final Object $user = this.getUser();
        result = result * 59 + (($user == null) ? 43 : $user.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "RefreshResponse(accessToken=" + this.getAccessToken() + ", clientToken=" + this.getClientToken() + ", selectedProfile=" + this.getSelectedProfile() + ", user=" + this.getUser() + ")";
    }
}
