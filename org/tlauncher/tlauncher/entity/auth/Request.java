// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.auth;

public class Request
{
    private String clientToken;
    
    public String getClientToken() {
        return this.clientToken;
    }
    
    public void setClientToken(final String clientToken) {
        this.clientToken = clientToken;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Request)) {
            return false;
        }
        final Request other = (Request)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$clientToken = this.getClientToken();
        final Object other$clientToken = other.getClientToken();
        if (this$clientToken == null) {
            if (other$clientToken == null) {
                return true;
            }
        }
        else if (this$clientToken.equals(other$clientToken)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof Request;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $clientToken = this.getClientToken();
        result = result * 59 + (($clientToken == null) ? 43 : $clientToken.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Request(clientToken=" + this.getClientToken() + ")";
    }
}
