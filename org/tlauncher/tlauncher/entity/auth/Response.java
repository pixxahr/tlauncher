// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.auth;

public class Response
{
    private String error;
    private String errorMessage;
    private String cause;
    
    public String getError() {
        return this.error;
    }
    
    public String getErrorMessage() {
        return this.errorMessage;
    }
    
    public String getCause() {
        return this.cause;
    }
    
    public void setError(final String error) {
        this.error = error;
    }
    
    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public void setCause(final String cause) {
        this.cause = cause;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Response)) {
            return false;
        }
        final Response other = (Response)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$error = this.getError();
        final Object other$error = other.getError();
        Label_0065: {
            if (this$error == null) {
                if (other$error == null) {
                    break Label_0065;
                }
            }
            else if (this$error.equals(other$error)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$errorMessage = this.getErrorMessage();
        final Object other$errorMessage = other.getErrorMessage();
        Label_0102: {
            if (this$errorMessage == null) {
                if (other$errorMessage == null) {
                    break Label_0102;
                }
            }
            else if (this$errorMessage.equals(other$errorMessage)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$cause = this.getCause();
        final Object other$cause = other.getCause();
        if (this$cause == null) {
            if (other$cause == null) {
                return true;
            }
        }
        else if (this$cause.equals(other$cause)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof Response;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $error = this.getError();
        result = result * 59 + (($error == null) ? 43 : $error.hashCode());
        final Object $errorMessage = this.getErrorMessage();
        result = result * 59 + (($errorMessage == null) ? 43 : $errorMessage.hashCode());
        final Object $cause = this.getCause();
        result = result * 59 + (($cause == null) ? 43 : $cause.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Response(error=" + this.getError() + ", errorMessage=" + this.getErrorMessage() + ", cause=" + this.getCause() + ")";
    }
}
