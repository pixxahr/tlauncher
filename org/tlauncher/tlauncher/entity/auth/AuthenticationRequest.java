// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.auth;

import org.tlauncher.tlauncher.minecraft.auth.Authenticator;
import org.tlauncher.tlauncher.minecraft.auth.Agent;

public class AuthenticationRequest extends Request
{
    private Agent agent;
    private String username;
    private String password;
    
    public AuthenticationRequest(final Authenticator auth) {
        this.agent = Agent.MINECRAFT;
        this.username = auth.getAccount().getUsername();
        this.password = auth.getAccount().getPassword();
        this.setClientToken(Authenticator.getClientToken().toString());
    }
    
    public Agent getAgent() {
        return this.agent;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setAgent(final Agent agent) {
        this.agent = agent;
    }
    
    public void setUsername(final String username) {
        this.username = username;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AuthenticationRequest)) {
            return false;
        }
        final AuthenticationRequest other = (AuthenticationRequest)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$agent = this.getAgent();
        final Object other$agent = other.getAgent();
        Label_0065: {
            if (this$agent == null) {
                if (other$agent == null) {
                    break Label_0065;
                }
            }
            else if (this$agent.equals(other$agent)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$username = this.getUsername();
        final Object other$username = other.getUsername();
        Label_0102: {
            if (this$username == null) {
                if (other$username == null) {
                    break Label_0102;
                }
            }
            else if (this$username.equals(other$username)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$password = this.getPassword();
        final Object other$password = other.getPassword();
        if (this$password == null) {
            if (other$password == null) {
                return true;
            }
        }
        else if (this$password.equals(other$password)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof AuthenticationRequest;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $agent = this.getAgent();
        result = result * 59 + (($agent == null) ? 43 : $agent.hashCode());
        final Object $username = this.getUsername();
        result = result * 59 + (($username == null) ? 43 : $username.hashCode());
        final Object $password = this.getPassword();
        result = result * 59 + (($password == null) ? 43 : $password.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "AuthenticationRequest(agent=" + this.getAgent() + ", username=" + this.getUsername() + ", password=" + this.getPassword() + ")";
    }
}
