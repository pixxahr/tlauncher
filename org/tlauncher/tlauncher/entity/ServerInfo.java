// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity;

import java.util.ArrayList;
import java.util.List;

public class ServerInfo
{
    protected String serverId;
    protected String address;
    protected String minVersion;
    protected List<String> ignoreVersions;
    protected List<String> includeVersions;
    private String redirectAddress;
    
    public ServerInfo() {
        this.ignoreVersions = new ArrayList<String>();
        this.includeVersions = new ArrayList<String>();
    }
    
    public String getServerId() {
        return this.serverId;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public String getMinVersion() {
        return this.minVersion;
    }
    
    public List<String> getIgnoreVersions() {
        return this.ignoreVersions;
    }
    
    public List<String> getIncludeVersions() {
        return this.includeVersions;
    }
    
    public String getRedirectAddress() {
        return this.redirectAddress;
    }
    
    public void setServerId(final String serverId) {
        this.serverId = serverId;
    }
    
    public void setAddress(final String address) {
        this.address = address;
    }
    
    public void setMinVersion(final String minVersion) {
        this.minVersion = minVersion;
    }
    
    public void setIgnoreVersions(final List<String> ignoreVersions) {
        this.ignoreVersions = ignoreVersions;
    }
    
    public void setIncludeVersions(final List<String> includeVersions) {
        this.includeVersions = includeVersions;
    }
    
    public void setRedirectAddress(final String redirectAddress) {
        this.redirectAddress = redirectAddress;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ServerInfo)) {
            return false;
        }
        final ServerInfo other = (ServerInfo)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$serverId = this.getServerId();
        final Object other$serverId = other.getServerId();
        Label_0065: {
            if (this$serverId == null) {
                if (other$serverId == null) {
                    break Label_0065;
                }
            }
            else if (this$serverId.equals(other$serverId)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$address = this.getAddress();
        final Object other$address = other.getAddress();
        Label_0102: {
            if (this$address == null) {
                if (other$address == null) {
                    break Label_0102;
                }
            }
            else if (this$address.equals(other$address)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$minVersion = this.getMinVersion();
        final Object other$minVersion = other.getMinVersion();
        Label_0139: {
            if (this$minVersion == null) {
                if (other$minVersion == null) {
                    break Label_0139;
                }
            }
            else if (this$minVersion.equals(other$minVersion)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$ignoreVersions = this.getIgnoreVersions();
        final Object other$ignoreVersions = other.getIgnoreVersions();
        Label_0176: {
            if (this$ignoreVersions == null) {
                if (other$ignoreVersions == null) {
                    break Label_0176;
                }
            }
            else if (this$ignoreVersions.equals(other$ignoreVersions)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$includeVersions = this.getIncludeVersions();
        final Object other$includeVersions = other.getIncludeVersions();
        Label_0213: {
            if (this$includeVersions == null) {
                if (other$includeVersions == null) {
                    break Label_0213;
                }
            }
            else if (this$includeVersions.equals(other$includeVersions)) {
                break Label_0213;
            }
            return false;
        }
        final Object this$redirectAddress = this.getRedirectAddress();
        final Object other$redirectAddress = other.getRedirectAddress();
        if (this$redirectAddress == null) {
            if (other$redirectAddress == null) {
                return true;
            }
        }
        else if (this$redirectAddress.equals(other$redirectAddress)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof ServerInfo;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $serverId = this.getServerId();
        result = result * 59 + (($serverId == null) ? 43 : $serverId.hashCode());
        final Object $address = this.getAddress();
        result = result * 59 + (($address == null) ? 43 : $address.hashCode());
        final Object $minVersion = this.getMinVersion();
        result = result * 59 + (($minVersion == null) ? 43 : $minVersion.hashCode());
        final Object $ignoreVersions = this.getIgnoreVersions();
        result = result * 59 + (($ignoreVersions == null) ? 43 : $ignoreVersions.hashCode());
        final Object $includeVersions = this.getIncludeVersions();
        result = result * 59 + (($includeVersions == null) ? 43 : $includeVersions.hashCode());
        final Object $redirectAddress = this.getRedirectAddress();
        result = result * 59 + (($redirectAddress == null) ? 43 : $redirectAddress.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "ServerInfo(serverId=" + this.getServerId() + ", address=" + this.getAddress() + ", minVersion=" + this.getMinVersion() + ", ignoreVersions=" + this.getIgnoreVersions() + ", includeVersions=" + this.getIncludeVersions() + ", redirectAddress=" + this.getRedirectAddress() + ")";
    }
}
