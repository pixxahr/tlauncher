// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.server;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import java.util.Date;
import java.util.List;

public class RemoteServer extends Server
{
    private List<Long> removedTime;
    private Integer recoveryServerTime;
    private Integer maxRemovingCountServer;
    private Date addedDate;
    private ServerState state;
    private Set<String> locales;
    private boolean remote;
    
    public RemoteServer() {
        this.recoveryServerTime = 0;
        this.maxRemovingCountServer = 0;
        this.removedTime = new ArrayList<Long>();
        this.locales = new HashSet<String>();
    }
    
    public void initRemote() {
        this.setRemote(true);
        this.addedDate = new Date();
        this.setState(ServerState.ACTIVE);
    }
    
    public Integer getRecoveryServerTime() {
        return this.recoveryServerTime;
    }
    
    public Integer getMaxRemovingCountServer() {
        return this.maxRemovingCountServer;
    }
    
    public List<Long> getRemovedTime() {
        return this.removedTime;
    }
    
    public Date getAddedDate() {
        return this.addedDate;
    }
    
    public ServerState getState() {
        return this.state;
    }
    
    public void setState(final ServerState state) {
        this.state = state;
    }
    
    public boolean isRemote() {
        return this.remote;
    }
    
    public void setRemote(final boolean remote) {
        this.remote = remote;
    }
    
    public Set<String> getLocales() {
        return this.locales;
    }
    
    public enum ServerState
    {
        DEACTIVATED, 
        ACTIVE;
    }
}
