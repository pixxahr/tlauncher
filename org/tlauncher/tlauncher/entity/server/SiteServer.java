// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.server;

public class SiteServer extends RemoteServer
{
    private String version;
    
    public String getVersion() {
        return this.version;
    }
    
    public void setVersion(final String version) {
        this.version = version;
    }
}
