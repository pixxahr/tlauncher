// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.server;

import net.minecraft.common.NBTTagCompound;
import org.tlauncher.exceptions.ParseException;
import org.apache.commons.lang3.StringUtils;

public class Server
{
    private String name;
    private String address;
    private String ip;
    private String port;
    private boolean hideAddress;
    private int acceptTextures;
    
    private static String[] splitAddress(final String address) {
        final String[] array = StringUtils.split(address, ':');
        switch (array.length) {
            case 1: {
                return new String[] { address, null };
            }
            case 2: {
                return new String[] { array[0], array[1] };
            }
            default: {
                throw new ParseException("split incorrectly");
            }
        }
    }
    
    public NBTTagCompound getNBT() {
        final NBTTagCompound compound = new NBTTagCompound();
        compound.setString("name", this.name);
        compound.setString("ip", this.address);
        compound.setBoolean("hideAddress", this.hideAddress);
        if (this.acceptTextures != 0) {
            compound.setBoolean("acceptTextures", this.acceptTextures == 1);
        }
        return compound;
    }
    
    public static Server loadFromNBT(final NBTTagCompound nbt) {
        final Server server = new Server();
        server.setName(nbt.getString("name"));
        server.setAddress(nbt.getString("ip"));
        server.hideAddress = nbt.getBoolean("hideAddress");
        if (nbt.hasKey("acceptTextures")) {
            server.acceptTextures = (nbt.getBoolean("acceptTextures") ? 1 : -1);
        }
        return server;
    }
    
    public void setAddress(final String address) {
        if (address == null) {
            this.ip = null;
            this.port = null;
        }
        else {
            final String[] split = splitAddress(address);
            this.ip = split[0];
            this.port = split[1];
        }
        this.address = address;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public String getIp() {
        return this.ip;
    }
    
    public String getPort() {
        return this.port;
    }
    
    public boolean isHideAddress() {
        return this.hideAddress;
    }
    
    public int getAcceptTextures() {
        return this.acceptTextures;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setIp(final String ip) {
        this.ip = ip;
    }
    
    public void setPort(final String port) {
        this.port = port;
    }
    
    public void setHideAddress(final boolean hideAddress) {
        this.hideAddress = hideAddress;
    }
    
    public void setAcceptTextures(final int acceptTextures) {
        this.acceptTextures = acceptTextures;
    }
    
    @Override
    public String toString() {
        return "Server(name=" + this.getName() + ", address=" + this.getAddress() + ", ip=" + this.getIp() + ", port=" + this.getPort() + ", hideAddress=" + this.isHideAddress() + ", acceptTextures=" + this.getAcceptTextures() + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Server)) {
            return false;
        }
        final Server other = (Server)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$address = this.getAddress();
        final Object other$address = other.getAddress();
        if (this$address == null) {
            if (other$address == null) {
                return true;
            }
        }
        else if (this$address.equals(other$address)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof Server;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $address = this.getAddress();
        result = result * 59 + (($address == null) ? 43 : $address.hashCode());
        return result;
    }
}
