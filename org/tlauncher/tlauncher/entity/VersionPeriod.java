// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity;

import java.util.List;

public class VersionPeriod
{
    private String minVersion;
    private List<String> ignoreVersions;
    private List<String> includeVersions;
    
    public String getMinVersion() {
        return this.minVersion;
    }
    
    public void setMinVersion(final String minVersion) {
        this.minVersion = minVersion;
    }
    
    public List<String> getIgnoreVersions() {
        return this.ignoreVersions;
    }
    
    public void setIgnoreVersions(final List<String> ignoreVersions) {
        this.ignoreVersions = ignoreVersions;
    }
    
    public List<String> getIncludeVersions() {
        return this.includeVersions;
    }
    
    public void setIncludeVersions(final List<String> includeVersions) {
        this.includeVersions = includeVersions;
    }
}
