// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.hot;

import java.util.Date;
import java.awt.image.BufferedImage;
import org.tlauncher.tlauncher.entity.ServerInfo;

public class AdditionalHotServer extends ServerInfo
{
    private String shortDescription;
    private String addDescription;
    private String versionDescription;
    private String tMonitoringLink;
    private BufferedImage image;
    private Integer online;
    private Integer max;
    private Date updated;
    private Integer imageNumber;
    private boolean active;
    
    public AdditionalHotServer() {
        this.online = -1;
        this.max = -1;
    }
    
    public String getShortDescription() {
        return this.shortDescription;
    }
    
    public String getAddDescription() {
        return this.addDescription;
    }
    
    public String getVersionDescription() {
        return this.versionDescription;
    }
    
    public String getTMonitoringLink() {
        return this.tMonitoringLink;
    }
    
    public BufferedImage getImage() {
        return this.image;
    }
    
    public Integer getOnline() {
        return this.online;
    }
    
    public Integer getMax() {
        return this.max;
    }
    
    public Date getUpdated() {
        return this.updated;
    }
    
    public Integer getImageNumber() {
        return this.imageNumber;
    }
    
    public boolean isActive() {
        return this.active;
    }
    
    public void setShortDescription(final String shortDescription) {
        this.shortDescription = shortDescription;
    }
    
    public void setAddDescription(final String addDescription) {
        this.addDescription = addDescription;
    }
    
    public void setVersionDescription(final String versionDescription) {
        this.versionDescription = versionDescription;
    }
    
    public void setTMonitoringLink(final String tMonitoringLink) {
        this.tMonitoringLink = tMonitoringLink;
    }
    
    public void setImage(final BufferedImage image) {
        this.image = image;
    }
    
    public void setOnline(final Integer online) {
        this.online = online;
    }
    
    public void setMax(final Integer max) {
        this.max = max;
    }
    
    public void setUpdated(final Date updated) {
        this.updated = updated;
    }
    
    public void setImageNumber(final Integer imageNumber) {
        this.imageNumber = imageNumber;
    }
    
    public void setActive(final boolean active) {
        this.active = active;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AdditionalHotServer)) {
            return false;
        }
        final AdditionalHotServer other = (AdditionalHotServer)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$shortDescription = this.getShortDescription();
        final Object other$shortDescription = other.getShortDescription();
        Label_0065: {
            if (this$shortDescription == null) {
                if (other$shortDescription == null) {
                    break Label_0065;
                }
            }
            else if (this$shortDescription.equals(other$shortDescription)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$addDescription = this.getAddDescription();
        final Object other$addDescription = other.getAddDescription();
        Label_0102: {
            if (this$addDescription == null) {
                if (other$addDescription == null) {
                    break Label_0102;
                }
            }
            else if (this$addDescription.equals(other$addDescription)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$versionDescription = this.getVersionDescription();
        final Object other$versionDescription = other.getVersionDescription();
        Label_0139: {
            if (this$versionDescription == null) {
                if (other$versionDescription == null) {
                    break Label_0139;
                }
            }
            else if (this$versionDescription.equals(other$versionDescription)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$tMonitoringLink = this.getTMonitoringLink();
        final Object other$tMonitoringLink = other.getTMonitoringLink();
        Label_0176: {
            if (this$tMonitoringLink == null) {
                if (other$tMonitoringLink == null) {
                    break Label_0176;
                }
            }
            else if (this$tMonitoringLink.equals(other$tMonitoringLink)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$image = this.getImage();
        final Object other$image = other.getImage();
        Label_0213: {
            if (this$image == null) {
                if (other$image == null) {
                    break Label_0213;
                }
            }
            else if (this$image.equals(other$image)) {
                break Label_0213;
            }
            return false;
        }
        final Object this$online = this.getOnline();
        final Object other$online = other.getOnline();
        Label_0250: {
            if (this$online == null) {
                if (other$online == null) {
                    break Label_0250;
                }
            }
            else if (this$online.equals(other$online)) {
                break Label_0250;
            }
            return false;
        }
        final Object this$max = this.getMax();
        final Object other$max = other.getMax();
        Label_0287: {
            if (this$max == null) {
                if (other$max == null) {
                    break Label_0287;
                }
            }
            else if (this$max.equals(other$max)) {
                break Label_0287;
            }
            return false;
        }
        final Object this$updated = this.getUpdated();
        final Object other$updated = other.getUpdated();
        Label_0324: {
            if (this$updated == null) {
                if (other$updated == null) {
                    break Label_0324;
                }
            }
            else if (this$updated.equals(other$updated)) {
                break Label_0324;
            }
            return false;
        }
        final Object this$imageNumber = this.getImageNumber();
        final Object other$imageNumber = other.getImageNumber();
        if (this$imageNumber == null) {
            if (other$imageNumber == null) {
                return this.isActive() == other.isActive();
            }
        }
        else if (this$imageNumber.equals(other$imageNumber)) {
            return this.isActive() == other.isActive();
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof AdditionalHotServer;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $shortDescription = this.getShortDescription();
        result = result * 59 + (($shortDescription == null) ? 43 : $shortDescription.hashCode());
        final Object $addDescription = this.getAddDescription();
        result = result * 59 + (($addDescription == null) ? 43 : $addDescription.hashCode());
        final Object $versionDescription = this.getVersionDescription();
        result = result * 59 + (($versionDescription == null) ? 43 : $versionDescription.hashCode());
        final Object $tMonitoringLink = this.getTMonitoringLink();
        result = result * 59 + (($tMonitoringLink == null) ? 43 : $tMonitoringLink.hashCode());
        final Object $image = this.getImage();
        result = result * 59 + (($image == null) ? 43 : $image.hashCode());
        final Object $online = this.getOnline();
        result = result * 59 + (($online == null) ? 43 : $online.hashCode());
        final Object $max = this.getMax();
        result = result * 59 + (($max == null) ? 43 : $max.hashCode());
        final Object $updated = this.getUpdated();
        result = result * 59 + (($updated == null) ? 43 : $updated.hashCode());
        final Object $imageNumber = this.getImageNumber();
        result = result * 59 + (($imageNumber == null) ? 43 : $imageNumber.hashCode());
        result = result * 59 + (this.isActive() ? 79 : 97);
        return result;
    }
    
    @Override
    public String toString() {
        return "AdditionalHotServer(super=" + super.toString() + ", shortDescription=" + this.getShortDescription() + ", addDescription=" + this.getAddDescription() + ", versionDescription=" + this.getVersionDescription() + ", tMonitoringLink=" + this.getTMonitoringLink() + ", image=" + this.getImage() + ", online=" + this.getOnline() + ", max=" + this.getMax() + ", updated=" + this.getUpdated() + ", imageNumber=" + this.getImageNumber() + ", active=" + this.isActive() + ")";
    }
}
