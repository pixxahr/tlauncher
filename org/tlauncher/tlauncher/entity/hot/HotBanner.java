// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.hot;

import org.tlauncher.tlauncher.updater.client.Banner;

public class HotBanner extends Banner
{
    private String mouseOnImage;
    
    public String getMouseOnImage() {
        return this.mouseOnImage;
    }
    
    public void setMouseOnImage(final String mouseOnImage) {
        this.mouseOnImage = mouseOnImage;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof HotBanner)) {
            return false;
        }
        final HotBanner other = (HotBanner)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$mouseOnImage = this.getMouseOnImage();
        final Object other$mouseOnImage = other.getMouseOnImage();
        if (this$mouseOnImage == null) {
            if (other$mouseOnImage == null) {
                return true;
            }
        }
        else if (this$mouseOnImage.equals(other$mouseOnImage)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof HotBanner;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $mouseOnImage = this.getMouseOnImage();
        result = result * 59 + (($mouseOnImage == null) ? 43 : $mouseOnImage.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "HotBanner(mouseOnImage=" + this.getMouseOnImage() + ")";
    }
}
