// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity.hot;

import java.util.List;

public class AdditionalHotServers
{
    private List<AdditionalHotServer> list;
    private HotBanner upBanner;
    private HotBanner downBanner;
    private boolean shuffle;
    
    public List<AdditionalHotServer> getList() {
        return this.list;
    }
    
    public HotBanner getUpBanner() {
        return this.upBanner;
    }
    
    public HotBanner getDownBanner() {
        return this.downBanner;
    }
    
    public boolean isShuffle() {
        return this.shuffle;
    }
    
    public void setList(final List<AdditionalHotServer> list) {
        this.list = list;
    }
    
    public void setUpBanner(final HotBanner upBanner) {
        this.upBanner = upBanner;
    }
    
    public void setDownBanner(final HotBanner downBanner) {
        this.downBanner = downBanner;
    }
    
    public void setShuffle(final boolean shuffle) {
        this.shuffle = shuffle;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AdditionalHotServers)) {
            return false;
        }
        final AdditionalHotServers other = (AdditionalHotServers)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$list = this.getList();
        final Object other$list = other.getList();
        Label_0065: {
            if (this$list == null) {
                if (other$list == null) {
                    break Label_0065;
                }
            }
            else if (this$list.equals(other$list)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$upBanner = this.getUpBanner();
        final Object other$upBanner = other.getUpBanner();
        Label_0102: {
            if (this$upBanner == null) {
                if (other$upBanner == null) {
                    break Label_0102;
                }
            }
            else if (this$upBanner.equals(other$upBanner)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$downBanner = this.getDownBanner();
        final Object other$downBanner = other.getDownBanner();
        if (this$downBanner == null) {
            if (other$downBanner == null) {
                return this.isShuffle() == other.isShuffle();
            }
        }
        else if (this$downBanner.equals(other$downBanner)) {
            return this.isShuffle() == other.isShuffle();
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof AdditionalHotServers;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $list = this.getList();
        result = result * 59 + (($list == null) ? 43 : $list.hashCode());
        final Object $upBanner = this.getUpBanner();
        result = result * 59 + (($upBanner == null) ? 43 : $upBanner.hashCode());
        final Object $downBanner = this.getDownBanner();
        result = result * 59 + (($downBanner == null) ? 43 : $downBanner.hashCode());
        result = result * 59 + (this.isShuffle() ? 79 : 97);
        return result;
    }
    
    @Override
    public String toString() {
        return "AdditionalHotServers(list=" + this.getList() + ", upBanner=" + this.getUpBanner() + ", downBanner=" + this.getDownBanner() + ", shuffle=" + this.isShuffle() + ")";
    }
}
