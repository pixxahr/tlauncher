// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity;

import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import java.util.List;

public class AdditionalAsset
{
    private List<String> versions;
    private List<MetadataDTO> files;
    
    public List<String> getVersions() {
        return this.versions;
    }
    
    public List<MetadataDTO> getFiles() {
        return this.files;
    }
    
    public void setVersions(final List<String> versions) {
        this.versions = versions;
    }
    
    public void setFiles(final List<MetadataDTO> files) {
        this.files = files;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AdditionalAsset)) {
            return false;
        }
        final AdditionalAsset other = (AdditionalAsset)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$versions = this.getVersions();
        final Object other$versions = other.getVersions();
        Label_0065: {
            if (this$versions == null) {
                if (other$versions == null) {
                    break Label_0065;
                }
            }
            else if (this$versions.equals(other$versions)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$files = this.getFiles();
        final Object other$files = other.getFiles();
        if (this$files == null) {
            if (other$files == null) {
                return true;
            }
        }
        else if (this$files.equals(other$files)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof AdditionalAsset;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $versions = this.getVersions();
        result = result * 59 + (($versions == null) ? 43 : $versions.hashCode());
        final Object $files = this.getFiles();
        result = result * 59 + (($files == null) ? 43 : $files.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "AdditionalAsset(versions=" + this.getVersions() + ", files=" + this.getFiles() + ")";
    }
}
