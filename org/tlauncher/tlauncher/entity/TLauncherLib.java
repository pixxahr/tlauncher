// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity;

import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.util.OS;
import java.io.File;
import org.tlauncher.tlauncher.repository.Repo;
import java.util.ArrayList;
import net.minecraft.launcher.versions.json.Argument;
import java.util.List;
import net.minecraft.launcher.versions.json.ArgumentType;
import java.util.Map;
import java.util.regex.Pattern;
import net.minecraft.launcher.versions.Library;

public class TLauncherLib extends Library
{
    private Pattern pattern;
    private Map<ArgumentType, List<Argument>> arguments;
    private String mainClass;
    private List<Library> requires;
    private List<String> supports;
    
    public TLauncherLib() {
        this.requires = new ArrayList<Library>();
        this.supports = new ArrayList<String>();
        this.setUrl("/libraries/");
    }
    
    public boolean matches(final Library lib) {
        return this.pattern != null && this.pattern.matcher(lib.getName()).matches();
    }
    
    public boolean isSupport(final String version) {
        return this.supports != null && this.supports.contains(version);
    }
    
    public boolean isApply(final Library lib, final String id) {
        return this.matches(lib) && (this.getSupports().isEmpty() || this.isSupport(id));
    }
    
    @Override
    public Downloadable getDownloadable(final Repo versionSource, final File file, final OS os) {
        U.log("getting downloadable", this.getName(), versionSource, file, os);
        return super.getDownloadable(ClientInstanceRepo.EXTRA_VERSION_REPO, file, os);
    }
    
    public Pattern getPattern() {
        return this.pattern;
    }
    
    public Map<ArgumentType, List<Argument>> getArguments() {
        return this.arguments;
    }
    
    public String getMainClass() {
        return this.mainClass;
    }
    
    public List<Library> getRequires() {
        return this.requires;
    }
    
    public List<String> getSupports() {
        return this.supports;
    }
    
    public void setPattern(final Pattern pattern) {
        this.pattern = pattern;
    }
    
    public void setArguments(final Map<ArgumentType, List<Argument>> arguments) {
        this.arguments = arguments;
    }
    
    public void setMainClass(final String mainClass) {
        this.mainClass = mainClass;
    }
    
    public void setRequires(final List<Library> requires) {
        this.requires = requires;
    }
    
    public void setSupports(final List<String> supports) {
        this.supports = supports;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof TLauncherLib)) {
            return false;
        }
        final TLauncherLib other = (TLauncherLib)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$pattern = this.getPattern();
        final Object other$pattern = other.getPattern();
        Label_0065: {
            if (this$pattern == null) {
                if (other$pattern == null) {
                    break Label_0065;
                }
            }
            else if (this$pattern.equals(other$pattern)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$arguments = this.getArguments();
        final Object other$arguments = other.getArguments();
        Label_0102: {
            if (this$arguments == null) {
                if (other$arguments == null) {
                    break Label_0102;
                }
            }
            else if (this$arguments.equals(other$arguments)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$mainClass = this.getMainClass();
        final Object other$mainClass = other.getMainClass();
        Label_0139: {
            if (this$mainClass == null) {
                if (other$mainClass == null) {
                    break Label_0139;
                }
            }
            else if (this$mainClass.equals(other$mainClass)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$requires = this.getRequires();
        final Object other$requires = other.getRequires();
        Label_0176: {
            if (this$requires == null) {
                if (other$requires == null) {
                    break Label_0176;
                }
            }
            else if (this$requires.equals(other$requires)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$supports = this.getSupports();
        final Object other$supports = other.getSupports();
        if (this$supports == null) {
            if (other$supports == null) {
                return true;
            }
        }
        else if (this$supports.equals(other$supports)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof TLauncherLib;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $pattern = this.getPattern();
        result = result * 59 + (($pattern == null) ? 43 : $pattern.hashCode());
        final Object $arguments = this.getArguments();
        result = result * 59 + (($arguments == null) ? 43 : $arguments.hashCode());
        final Object $mainClass = this.getMainClass();
        result = result * 59 + (($mainClass == null) ? 43 : $mainClass.hashCode());
        final Object $requires = this.getRequires();
        result = result * 59 + (($requires == null) ? 43 : $requires.hashCode());
        final Object $supports = this.getSupports();
        result = result * 59 + (($supports == null) ? 43 : $supports.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "TLauncherLib(super=" + super.toString() + ", pattern=" + this.getPattern() + ", arguments=" + this.getArguments() + ", mainClass=" + this.getMainClass() + ", requires=" + this.getRequires() + ", supports=" + this.getSupports() + ")";
    }
}
