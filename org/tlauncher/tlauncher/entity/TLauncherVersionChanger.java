// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import net.minecraft.launcher.versions.CompleteVersion;
import java.util.List;
import java.util.Set;

public class TLauncherVersionChanger
{
    private Set<String> tlauncherSkinCapeVersion;
    private double version;
    private List<TLauncherLib> libraries;
    private List<AdditionalModLib> additionalMods;
    
    public List<AdditionalModLib> getAddedMods(final CompleteVersion v, final boolean customTexture) {
        return this.additionalMods.stream().filter(l -> customTexture || !l.isActiveCustomUserTexture()).filter(l -> l.getSupports().contains(v.getID())).collect((Collector<? super Object, ?, List<AdditionalModLib>>)Collectors.toList());
    }
    
    public Set<String> getTlauncherSkinCapeVersion() {
        return this.tlauncherSkinCapeVersion;
    }
    
    public double getVersion() {
        return this.version;
    }
    
    public List<TLauncherLib> getLibraries() {
        return this.libraries;
    }
    
    public List<AdditionalModLib> getAdditionalMods() {
        return this.additionalMods;
    }
    
    public void setTlauncherSkinCapeVersion(final Set<String> tlauncherSkinCapeVersion) {
        this.tlauncherSkinCapeVersion = tlauncherSkinCapeVersion;
    }
    
    public void setVersion(final double version) {
        this.version = version;
    }
    
    public void setLibraries(final List<TLauncherLib> libraries) {
        this.libraries = libraries;
    }
    
    public void setAdditionalMods(final List<AdditionalModLib> additionalMods) {
        this.additionalMods = additionalMods;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof TLauncherVersionChanger)) {
            return false;
        }
        final TLauncherVersionChanger other = (TLauncherVersionChanger)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$tlauncherSkinCapeVersion = this.getTlauncherSkinCapeVersion();
        final Object other$tlauncherSkinCapeVersion = other.getTlauncherSkinCapeVersion();
        Label_0065: {
            if (this$tlauncherSkinCapeVersion == null) {
                if (other$tlauncherSkinCapeVersion == null) {
                    break Label_0065;
                }
            }
            else if (this$tlauncherSkinCapeVersion.equals(other$tlauncherSkinCapeVersion)) {
                break Label_0065;
            }
            return false;
        }
        if (Double.compare(this.getVersion(), other.getVersion()) != 0) {
            return false;
        }
        final Object this$libraries = this.getLibraries();
        final Object other$libraries = other.getLibraries();
        Label_0118: {
            if (this$libraries == null) {
                if (other$libraries == null) {
                    break Label_0118;
                }
            }
            else if (this$libraries.equals(other$libraries)) {
                break Label_0118;
            }
            return false;
        }
        final Object this$additionalMods = this.getAdditionalMods();
        final Object other$additionalMods = other.getAdditionalMods();
        if (this$additionalMods == null) {
            if (other$additionalMods == null) {
                return true;
            }
        }
        else if (this$additionalMods.equals(other$additionalMods)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof TLauncherVersionChanger;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $tlauncherSkinCapeVersion = this.getTlauncherSkinCapeVersion();
        result = result * 59 + (($tlauncherSkinCapeVersion == null) ? 43 : $tlauncherSkinCapeVersion.hashCode());
        final long $version = Double.doubleToLongBits(this.getVersion());
        result = result * 59 + (int)($version >>> 32 ^ $version);
        final Object $libraries = this.getLibraries();
        result = result * 59 + (($libraries == null) ? 43 : $libraries.hashCode());
        final Object $additionalMods = this.getAdditionalMods();
        result = result * 59 + (($additionalMods == null) ? 43 : $additionalMods.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "TLauncherVersionChanger(tlauncherSkinCapeVersion=" + this.getTlauncherSkinCapeVersion() + ", version=" + this.getVersion() + ", libraries=" + this.getLibraries() + ", additionalMods=" + this.getAdditionalMods() + ")";
    }
}
