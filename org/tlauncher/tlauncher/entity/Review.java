// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.entity;

import java.io.File;
import java.util.List;

public class Review
{
    private String title;
    private String description;
    private String mailReview;
    private String typeReview;
    private List<File> listFiles;
    
    public Review() {
    }
    
    public Review(final String title, final String description, final String mailReview, final String typeReview, final List<File> listFiles) {
        this.title = title;
        this.description = description;
        this.mailReview = mailReview;
        this.typeReview = typeReview;
        this.listFiles = listFiles;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public String getMailReview() {
        return this.mailReview;
    }
    
    public void setMailReview(final String mailReview) {
        this.mailReview = mailReview;
    }
    
    public String getTypeReview() {
        return this.typeReview;
    }
    
    public void setTypeReview(final String typeReview) {
        this.typeReview = typeReview;
    }
    
    public List<File> getListFiles() {
        return this.listFiles;
    }
    
    public void setListFiles(final List<File> listFiles) {
        this.listFiles = listFiles;
    }
}
