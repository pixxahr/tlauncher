// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.crash;

import com.google.gson.JsonObject;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonParseException;
import java.util.Iterator;
import com.google.gson.reflect.TypeToken;
import com.google.gson.JsonElement;
import java.util.HashMap;
import org.tlauncher.tlauncher.rmo.TLauncher;
import com.google.gson.Gson;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CrashSignatureContainer
{
    private static final int universalExitCode = 0;
    private Map<String, String> variables;
    private List<CrashSignature> signatures;
    
    public CrashSignatureContainer() {
        this.variables = new LinkedHashMap<String, String>();
        this.signatures = new ArrayList<CrashSignature>();
    }
    
    public Map<String, String> getVariables() {
        return this.variables;
    }
    
    public List<CrashSignature> getSignatures() {
        return this.signatures;
    }
    
    public String getVariable(final String key) {
        return this.variables.get(key);
    }
    
    public Pattern getPattern(final String key) {
        return Pattern.compile(this.variables.get(key));
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{\nvariables='" + this.variables + "',\nsignatures='" + this.signatures + "'}";
    }
    
    public class CrashSignature
    {
        private String name;
        private String version;
        private String path;
        private String pattern;
        private int exit;
        private boolean fake;
        private boolean forge;
        private Pattern versionPattern;
        private Pattern linePattern;
        
        public String getName() {
            return this.name;
        }
        
        public Pattern getVersion() {
            return this.versionPattern;
        }
        
        public boolean hasVersion() {
            return this.version != null;
        }
        
        public boolean isFake() {
            return this.fake;
        }
        
        public Pattern getPattern() {
            return this.linePattern;
        }
        
        public boolean hasPattern() {
            return this.pattern != null;
        }
        
        public String getPath() {
            return this.path;
        }
        
        public int getExitCode() {
            return this.exit;
        }
        
        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "{name='" + this.name + "', version='" + this.version + "', path='" + this.path + "', pattern='" + this.pattern + "', exitCode=" + this.exit + ", forge=" + this.forge + ", versionPattern='" + this.versionPattern + "', linePattern='" + this.linePattern + "'}";
        }
    }
    
    private static class CrashSignatureListSimpleDeserializer
    {
        private final Gson defaultContext;
        private Map<String, String> variables;
        private String forgePrefix;
        
        CrashSignatureListSimpleDeserializer() {
            this.defaultContext = TLauncher.getGson();
        }
        
        public void setVariables(final Map<String, String> vars) {
            this.variables = ((vars == null) ? new HashMap<String, String>() : vars);
            this.forgePrefix = (this.variables.containsKey("forge") ? this.variables.get("forge") : "");
        }
        
        public List<CrashSignature> deserialize(final JsonElement elem) throws JsonParseException {
            final List<CrashSignature> signatureList = this.defaultContext.fromJson(elem, new TypeToken<List<CrashSignature>>() {}.getType());
            for (final CrashSignature signature : signatureList) {
                this.analyzeSignature(signature);
            }
            return signatureList;
        }
        
        private CrashSignature analyzeSignature(final CrashSignature signature) {
            if (signature.name == null || signature.name.isEmpty()) {
                throw new JsonParseException("Invalid name: \"" + signature.name + "\"");
            }
            if (signature.version != null) {
                String pattern = signature.version;
                for (final Map.Entry<String, String> en : this.variables.entrySet()) {
                    final String varName = en.getKey();
                    final String varVal = en.getValue();
                    pattern = pattern.replace("${" + varName + "}", varVal);
                }
                signature.versionPattern = Pattern.compile(pattern);
            }
            if (signature.pattern != null) {
                String pattern = signature.pattern;
                for (final Map.Entry<String, String> en : this.variables.entrySet()) {
                    final String varName = en.getKey();
                    final String varVal = en.getValue();
                    pattern = pattern.replace("${" + varName + "}", varVal);
                }
                if (signature.forge) {
                    pattern = this.forgePrefix + pattern;
                }
                signature.linePattern = Pattern.compile(pattern);
            }
            if (signature.versionPattern == null && signature.linePattern == null && signature.exit == 0) {
                throw new JsonParseException("Useless signature found: " + signature.name);
            }
            return signature;
        }
    }
    
    static class CrashSignatureContainerDeserializer implements JsonDeserializer<CrashSignatureContainer>
    {
        private final CrashSignatureListSimpleDeserializer listDeserializer;
        private final Gson defaultContext;
        
        CrashSignatureContainerDeserializer() {
            this.defaultContext = TLauncher.getGson();
            this.listDeserializer = new CrashSignatureListSimpleDeserializer();
        }
        
        @Override
        public CrashSignatureContainer deserialize(final JsonElement element, final Type type, final JsonDeserializationContext context) throws JsonParseException {
            final JsonObject object = element.getAsJsonObject();
            final Map<String, String> rawVariables = this.defaultContext.fromJson(object.get("variables"), new TypeToken<Map<String, String>>() {}.getType());
            final Map<String, String> variables = new LinkedHashMap<String, String>();
            for (final Map.Entry<String, String> rawEn : rawVariables.entrySet()) {
                final String varName = rawEn.getKey();
                String varVal = rawEn.getValue();
                for (final Map.Entry<String, String> en : variables.entrySet()) {
                    final String replaceName = en.getKey();
                    final String replaceVal = en.getValue();
                    varVal = varVal.replace("${" + replaceName + "}", replaceVal);
                }
                variables.put(varName, varVal);
            }
            this.listDeserializer.setVariables(variables);
            final List<CrashSignature> signatures = this.listDeserializer.deserialize(object.get("signatures"));
            final CrashSignatureContainer list = new CrashSignatureContainer();
            list.variables = variables;
            list.signatures = signatures;
            return list;
        }
    }
}
