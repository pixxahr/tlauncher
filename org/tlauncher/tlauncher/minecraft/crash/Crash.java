// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.crash;

import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

public class Crash
{
    private String file;
    private List<CrashSignatureContainer.CrashSignature> signatures;
    
    public Crash() {
        this.signatures = new ArrayList<CrashSignatureContainer.CrashSignature>();
    }
    
    void addSignature(final CrashSignatureContainer.CrashSignature sign) {
        this.signatures.add(sign);
    }
    
    void removeSignature(final CrashSignatureContainer.CrashSignature sign) {
        this.signatures.remove(sign);
    }
    
    void setFile(final String path) {
        this.file = path;
    }
    
    public String getFile() {
        return this.file;
    }
    
    public List<CrashSignatureContainer.CrashSignature> getSignatures() {
        return Collections.unmodifiableList((List<? extends CrashSignatureContainer.CrashSignature>)this.signatures);
    }
    
    public boolean hasSignature(final CrashSignatureContainer.CrashSignature s) {
        return this.signatures.contains(s);
    }
    
    public boolean isRecognized() {
        return !this.signatures.isEmpty();
    }
}
