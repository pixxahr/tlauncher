// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.crash;

import com.google.gson.Gson;
import org.tlauncher.util.FileUtil;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.lang.reflect.Type;
import com.google.gson.GsonBuilder;
import org.tlauncher.util.U;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public class CrashDescriptor
{
    private static CrashSignatureContainer container;
    public static final int goodExitCode = 0;
    private static final String loggerPrefix = "[Crash]";
    private final MinecraftLauncher launcher;
    
    public CrashDescriptor(final MinecraftLauncher launcher) {
        if (launcher == null) {
            throw new NullPointerException();
        }
        this.launcher = launcher;
    }
    
    public Crash scan() {
        final int exitCode = this.launcher.getExitCode();
        if (exitCode == 0) {
            return null;
        }
        final Crash crash = new Crash();
        final Pattern filePattern = CrashDescriptor.container.getPattern("crash");
        final String version = this.launcher.getVersionName();
        final Scanner scanner = new Scanner(this.launcher.getOutput());
        while (scanner.hasNextLine()) {
            final String line = scanner.nextLine();
            if (filePattern.matcher(line).matches()) {
                final Matcher fileMatcher = filePattern.matcher(line);
                if (!fileMatcher.matches() || fileMatcher.groupCount() != 1) {
                    continue;
                }
                crash.setFile(fileMatcher.group(1));
                this.log("Found crash report file:", crash.getFile());
            }
            else {
                for (final CrashSignatureContainer.CrashSignature signature : CrashDescriptor.container.getSignatures()) {
                    if (signature.hasVersion() && !signature.getVersion().matcher(version).matches()) {
                        continue;
                    }
                    if (signature.getExitCode() != 0 && signature.getExitCode() != exitCode) {
                        continue;
                    }
                    if (signature.hasPattern() && !signature.getPattern().matcher(line).matches()) {
                        continue;
                    }
                    if (signature.isFake()) {
                        this.log("Minecraft closed with an illegal exit code not due to error. Scanning has been cancelled");
                        this.log("Fake signature:", signature.getName());
                        scanner.close();
                        return null;
                    }
                    if (crash.hasSignature(signature)) {
                        continue;
                    }
                    this.log("Signature \"" + signature.getName() + "\" matches!");
                    crash.addSignature(signature);
                }
            }
        }
        scanner.close();
        if (crash.isRecognized()) {
            this.log("Crash has been recognized!");
        }
        return crash;
    }
    
    void log(final Object... w) {
        this.launcher.log("[Crash]", w);
        U.log("[Crash]", w);
    }
    
    static {
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(CrashSignatureContainer.class, new CrashSignatureContainer.CrashSignatureContainerDeserializer());
        final Gson gson = builder.create();
        try {
            CrashDescriptor.container = gson.fromJson(FileUtil.getResource(TLauncher.class.getResource("/signatures.json")), CrashSignatureContainer.class);
        }
        catch (Exception e) {
            U.log("Cannot parse crash signatures!", e);
            CrashDescriptor.container = new CrashSignatureContainer();
        }
    }
}
