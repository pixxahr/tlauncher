// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher;

import org.tlauncher.tlauncher.ui.loc.Localizable;

public class MinecraftException extends Exception
{
    private static final long serialVersionUID = -2415374288600214879L;
    private final String langPath;
    private final String[] langVars;
    
    MinecraftException(final String message, final String langPath, final Throwable cause, Object... langVars) {
        super(message, cause);
        if (langPath == null) {
            throw new NullPointerException("Lang path required!");
        }
        if (langVars == null) {
            langVars = new Object[0];
        }
        this.langPath = langPath;
        this.langVars = Localizable.checkVariables(langVars);
    }
    
    public MinecraftException(final String message, final String langPath, final Throwable cause) {
        this(message, langPath, cause, new Object[0]);
    }
    
    MinecraftException(final String message, final String langPath, final Object... vars) {
        this(message, langPath, null, vars);
    }
    
    public String getLangPath() {
        return this.langPath;
    }
    
    public String[] getLangVars() {
        return this.langVars;
    }
}
