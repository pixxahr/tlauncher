// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher;

import java.util.zip.ZipOutputStream;
import java.util.zip.ZipInputStream;
import java.nio.charset.StandardCharsets;
import java.io.InputStream;
import java.io.FileInputStream;
import java.nio.file.LinkOption;
import org.tlauncher.tlauncher.minecraft.crash.Crash;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.TreeSet;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang3.text.StrSubstitutor;
import java.util.UUID;
import java.util.HashMap;
import net.minecraft.launcher.versions.json.ArgumentType;
import net.minecraft.launcher.versions.json.Argument;
import java.util.Enumeration;
import net.minecraft.launcher.versions.ExtractRules;
import java.util.Collection;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import java.util.Map;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.ui.swing.notification.skin.SkinNotification;
import java.net.UnknownHostException;
import org.tlauncher.util.U;
import ch.jamiete.mcping.MinecraftPingOptions;
import ch.jamiete.mcping.MinecraftPing;
import java.net.InetAddress;
import org.apache.commons.lang3.StringUtils;
import java.util.Arrays;
import org.tlauncher.tlauncher.downloader.AbortedDownloadException;
import net.minecraft.launcher.updater.AssetIndex;
import org.tlauncher.tlauncher.downloader.RetryDownloadException;
import net.minecraft.launcher.versions.Library;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;
import org.tlauncher.tlauncher.downloader.DownloadableContainerHandler;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import org.tlauncher.util.OS;
import java.io.IOException;
import net.minecraft.launcher.versions.Version;
import org.tlauncher.tlauncher.entity.server.Server;
import org.tlauncher.util.TlauncherUtil;
import sun.security.provider.certpath.SunCertPathBuilderException;
import javax.net.ssl.SSLException;
import java.util.Iterator;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.alert.Notification;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.util.FileUtil;
import org.tlauncher.util.MinecraftUtil;
import org.tlauncher.tlauncher.configuration.enums.ActionOnLaunch;
import com.google.inject.assistedinject.Assisted;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Collections;
import org.tlauncher.util.stream.LinkedStringStream;
import java.util.ArrayList;
import org.tlauncher.tlauncher.managers.ComponentManager;
import net.minecraft.launcher.process.JavaProcess;
import org.tlauncher.tlauncher.entity.server.RemoteServer;
import net.minecraft.launcher.process.JavaProcessLauncher;
import java.io.File;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import net.minecraft.launcher.versions.CompleteVersion;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.util.guice.LanguageAssistFactory;
import org.tlauncher.tlauncher.minecraft.launcher.assitent.AdditionalFileAssistanceFactory;
import org.tlauncher.util.guice.SoundAssistFactory;
import com.google.inject.Inject;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.tlauncher.minecraft.crash.CrashDescriptor;
import org.tlauncher.tlauncher.ui.console.Console;
import org.tlauncher.tlauncher.minecraft.launcher.assitent.MinecraftLauncherAssistantInterface;
import java.util.List;
import org.tlauncher.util.stream.PrintLogger;
import org.tlauncher.tlauncher.managers.ProfileManager;
import org.tlauncher.tlauncher.managers.AssetsManager;
import org.tlauncher.tlauncher.managers.VersionManager;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.downloader.Downloader;
import net.minecraft.launcher.versions.json.DateTypeAdapter;
import com.google.gson.Gson;
import net.minecraft.launcher.process.JavaProcessListener;

public class MinecraftLauncher implements JavaProcessListener
{
    private static final boolean[] ASSETS_PROBLEM;
    private final Thread parentThread;
    private final Gson gson;
    private final DateTypeAdapter dateAdapter;
    private final Downloader downloader;
    private final Configuration settings;
    private final boolean forceUpdate;
    private final boolean assistLaunch;
    private final VersionManager vm;
    private final AssetsManager am;
    private final ProfileManager pm;
    private final StringBuffer output;
    private final PrintLogger logger;
    private final ConsoleVisibility consoleVis;
    private final List<MinecraftListener> listeners;
    private final List<MinecraftExtendedListener> extListeners;
    private final List<MinecraftLauncherAssistantInterface> assistants;
    private boolean working;
    private boolean killed;
    private Console console;
    private CrashDescriptor descriptor;
    private MinecraftLauncherStep step;
    @Inject
    private ModpackManager modpackManager;
    @Inject
    private SoundAssistFactory soundAssistFactory;
    @Inject
    private AdditionalFileAssistanceFactory additionalFileAssistanceFactory;
    @Inject
    private LanguageAssistFactory languageAssistFactory;
    private String versionName;
    private VersionSyncInfo versionSync;
    private CompleteVersion version;
    private CompleteVersion deJureVersion;
    private String accountName;
    private Account account;
    private File javaDir;
    private File gameDir;
    private File runningMinecraftDir;
    private File localAssetsDir;
    private File nativeDir;
    private File globalAssetsDir;
    private File assetsIndexesDir;
    private File assetsObjectsDir;
    private int[] windowSize;
    private boolean fullScreen;
    private int ramSize;
    private JavaProcessLauncher launcher;
    private String javaArgs;
    private String programArgs;
    private boolean minecraftWorking;
    private long startupTime;
    private int exitCode;
    private RemoteServer server;
    private boolean clearNatives;
    private JavaProcess process;
    private boolean firstLine;
    
    private MinecraftLauncher(final ComponentManager manager, final Downloader downloader, final Configuration configuration, final boolean forceUpdate, final ConsoleVisibility visibility, final boolean exit) {
        this.assistants = new ArrayList<MinecraftLauncherAssistantInterface>();
        this.firstLine = true;
        if (manager == null) {
            throw new NullPointerException("Ti sovsem s duba ruhnul?");
        }
        if (downloader == null) {
            throw new NullPointerException("Downloader is NULL!");
        }
        if (configuration == null) {
            throw new NullPointerException("Configuration is NULL!");
        }
        if (visibility == null) {
            throw new NullPointerException("ConsoleVisibility is NULL!");
        }
        this.parentThread = Thread.currentThread();
        this.gson = new Gson();
        this.dateAdapter = new DateTypeAdapter();
        this.downloader = downloader;
        this.settings = configuration;
        this.vm = manager.getComponent(VersionManager.class);
        this.am = manager.getComponent(AssetsManager.class);
        this.pm = manager.getComponent(ProfileManager.class);
        this.forceUpdate = forceUpdate;
        this.assistLaunch = !exit;
        this.consoleVis = visibility;
        this.logger = (this.consoleVis.equals(ConsoleVisibility.NONE) ? null : new PrintLogger(new LinkedStringStream()));
        this.console = ((this.logger == null) ? null : new Console(this.settings, this.logger, "Minecraft", this.consoleVis.equals(ConsoleVisibility.ALWAYS) && this.assistLaunch));
        this.output = new StringBuffer();
        if (this.console != null) {
            this.console = null;
        }
        this.descriptor = new CrashDescriptor(this);
        this.listeners = Collections.synchronizedList(new ArrayList<MinecraftListener>());
        this.extListeners = Collections.synchronizedList(new ArrayList<MinecraftExtendedListener>());
        this.step = MinecraftLauncherStep.NONE;
        this.log("Running under TLauncher " + TLauncher.getVersion());
    }
    
    @Inject
    public MinecraftLauncher(@Assisted final TLauncher t, @Assisted final boolean forceUpdate) {
        this(t.getManager(), t.getDownloader(), t.getConfiguration(), forceUpdate, t.getConfiguration().getConsoleType().getVisibility(), t.getConfiguration().getActionOnLaunch() == ActionOnLaunch.EXIT);
    }
    
    @Inject
    public void postInit() {
        this.assistants.add(this.soundAssistFactory.create(this));
        this.assistants.add(this.additionalFileAssistanceFactory.create(this));
        this.assistants.add(this.languageAssistFactory.create(this));
    }
    
    public Downloader getDownloader() {
        return this.downloader;
    }
    
    public Configuration getConfiguration() {
        return this.settings;
    }
    
    public boolean isForceUpdate() {
        return this.forceUpdate;
    }
    
    public boolean isLaunchAssist() {
        return this.assistLaunch;
    }
    
    public String getOutput() {
        return (this.console == null) ? this.output.toString() : this.console.getOutput();
    }
    
    public ConsoleVisibility getConsoleVisibility() {
        return this.consoleVis;
    }
    
    public Console getConsole() {
        return this.console;
    }
    
    public CrashDescriptor getCrashDescriptor() {
        return this.descriptor;
    }
    
    public MinecraftLauncherStep getStep() {
        return this.step;
    }
    
    public boolean isWorking() {
        return this.working;
    }
    
    public void addListener(final MinecraftListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        if (listener instanceof MinecraftExtendedListener) {
            this.extListeners.add((MinecraftExtendedListener)listener);
        }
        this.listeners.add(listener);
    }
    
    public void start() {
        this.checkWorking();
        this.working = true;
        try {
            this.collectInfo();
            this.downloadResources();
            this.beforeRunTLauncherSkinVersion();
            this.constructProcess();
            this.launchMinecraft();
            this.postLaunch();
        }
        catch (Throwable e) {
            this.log("Error occurred:", e);
            this.checkSSL(e);
            if (e instanceof MinecraftException) {
                if (!this.settings.getBoolean("memory.notification.off") && !FileUtil.checkFreeSpace(MinecraftUtil.getWorkingDirectory(), FileUtil.SIZE_300)) {
                    final String message = Localizable.get("memory.notification.message").replace("disk", MinecraftUtil.getWorkingDirectory().toPath().getRoot().toString());
                    Alert.showCustomMonolog(Localizable.get("memory.notification.title"), new Notification(message, "memory.notification.off"));
                }
                final MinecraftException me = (MinecraftException)e;
                for (final MinecraftListener listener : this.listeners) {
                    listener.onMinecraftKnownError(me);
                }
            }
            else if (e instanceof MinecraftLauncherAborted) {
                for (final MinecraftListener listener2 : this.listeners) {
                    listener2.onMinecraftAbort();
                }
            }
            else {
                for (final MinecraftListener listener2 : this.listeners) {
                    listener2.onMinecraftError(e);
                }
            }
        }
        this.working = false;
        this.step = MinecraftLauncherStep.NONE;
        this.log("Launcher exited.");
    }
    
    private void checkSSL(final Throwable e) {
        if (e.getCause() != null && e.getCause().getCause() != null && (e.getCause().getCause() instanceof SSLException || e.getCause().getCause() instanceof SunCertPathBuilderException)) {
            TlauncherUtil.deactivateSSL();
        }
    }
    
    public void stop() {
        if (this.step == MinecraftLauncherStep.NONE) {
            throw new IllegalStateException();
        }
        if (this.step == MinecraftLauncherStep.DOWNLOADING) {
            this.downloader.stopDownload();
        }
        this.working = false;
    }
    
    public String getVersionName() {
        return this.versionName;
    }
    
    public void setVersionName(final String versionName) {
        this.versionName = versionName;
    }
    
    public CompleteVersion getVersion() {
        return this.version;
    }
    
    public int getExitCode() {
        return this.exitCode;
    }
    
    public Server getServer() {
        return this.server;
    }
    
    public void setServer(final RemoteServer server) {
        this.checkWorking();
        this.server = server;
    }
    
    private void collectInfo() throws MinecraftException {
        this.checkStep(MinecraftLauncherStep.NONE, MinecraftLauncherStep.COLLECTING);
        this.log("Collecting info...");
        for (final MinecraftListener listener : this.listeners) {
            listener.onMinecraftPrepare();
        }
        for (final MinecraftExtendedListener listener2 : this.extListeners) {
            listener2.onMinecraftCollecting();
        }
        this.log("Force update:", this.forceUpdate);
        this.versionName = this.settings.get("login.version.game");
        if (this.versionName == null || this.versionName.isEmpty()) {
            throw new IllegalArgumentException("Version name is NULL or empty!");
        }
        this.log("Selected version:", this.versionName);
        final String type = this.settings.get("running.account.type");
        if (Account.AccountType.valueOf(type).equals(Account.AccountType.FREE)) {
            this.accountName = this.settings.get("login.account");
            if (this.accountName == null || this.accountName.isEmpty()) {
                throw new IllegalArgumentException("login is NULL or empty!");
            }
            this.account = new Account(this.accountName);
        }
        else {
            final String username = this.settings.get("running.account.username");
            if (username == null || username.isEmpty()) {
                throw new IllegalArgumentException("username is NULL or empty!");
            }
            this.account = this.pm.getAuthDatabase().getByUsernameType(username, type);
            this.accountName = this.account.getUsername();
        }
        this.log("Selected account:", this.account.toDebugString());
        this.versionSync = this.vm.getVersionSyncInfo(this.versionName);
        if (this.versionSync == null) {
            throw new IllegalArgumentException("Cannot find version " + this.version);
        }
        this.log("Version sync info:", this.versionSync);
        try {
            if (this.versionSync.getLocal() != null && ((CompleteVersion)this.versionSync.getLocal()).getDownloads() == null && this.versionSync.getRemote() != null) {
                this.versionSync.setLocal(null);
            }
            this.deJureVersion = this.versionSync.resolveCompleteVersion(this.vm, this.forceUpdate);
        }
        catch (IOException e2) {
            Alert.showMessage(Localizable.get("version.manager.resolve.title"), Localizable.get("version.manager.resolve.message"));
            throw new MinecraftLauncherAborted(e2);
        }
        if (this.deJureVersion == null) {
            throw new NullPointerException("Complete version is NULL");
        }
        this.version = this.deJureVersion;
        if (this.console != null) {
            this.console.setName(this.version.getID());
        }
        final String javaDirPath = this.settings.get("minecraft.javadir");
        this.javaDir = new File((javaDirPath == null) ? OS.getJavaPathByHome(true) : javaDirPath);
        this.log("Java path:", this.javaDir);
        this.gameDir = new File(this.settings.get("minecraft.gamedir"));
        try {
            FileUtil.createFolder(this.gameDir);
        }
        catch (IOException e3) {
            throw new MinecraftException("Cannot createScrollWrapper working directory!", "folder-not-found", e3);
        }
        if (this.version.isModpack()) {
            this.runningMinecraftDir = ModpackUtil.getPathByVersion(this.version).toFile();
        }
        else {
            this.runningMinecraftDir = this.gameDir;
        }
        final File serverResourcePacks = new File(this.runningMinecraftDir, "server-resource-packs");
        try {
            FileUtil.createFolder(serverResourcePacks);
        }
        catch (IOException e4) {
            throw new RuntimeException("Can't create " + serverResourcePacks, e4);
        }
        final File modsFolder = new File(this.runningMinecraftDir, "mods");
        try {
            FileUtil.createFolder(modsFolder);
        }
        catch (IOException e5) {
            throw new RuntimeException("Can't create " + modsFolder, e5);
        }
        this.globalAssetsDir = new File(this.gameDir, "assets");
        try {
            FileUtil.createFolder(this.globalAssetsDir);
        }
        catch (IOException e5) {
            throw new RuntimeException("Cannot createScrollWrapper assets directory!", e5);
        }
        this.assetsIndexesDir = new File(this.globalAssetsDir, "indexes");
        try {
            FileUtil.createFolder(this.assetsIndexesDir);
        }
        catch (IOException e5) {
            throw new RuntimeException("Cannot createScrollWrapper assets indexes directory!", e5);
        }
        this.assetsObjectsDir = new File(this.globalAssetsDir, "objects");
        try {
            FileUtil.createFolder(this.assetsObjectsDir);
        }
        catch (IOException e5) {
            throw new RuntimeException("Cannot createScrollWrapper assets objects directory!", e5);
        }
        this.nativeDir = new File(this.gameDir, "versions/" + this.version.getID() + "/natives");
        if (!OS.is(OS.WINDOWS) && this.nativeDir.getPath().chars().anyMatch(e -> Character.UnicodeBlock.of(e).equals(Character.UnicodeBlock.CYRILLIC))) {
            try {
                (this.nativeDir = Files.createTempDirectory("natives", (FileAttribute<?>[])new FileAttribute[0]).toFile()).deleteOnExit();
                this.clearNatives = true;
            }
            catch (IOException e5) {
                throw new RuntimeException("Cannot createScrollWrapper native files directory!", e5);
            }
        }
        try {
            FileUtil.createFolder(this.nativeDir);
        }
        catch (IOException e5) {
            throw new RuntimeException("Cannot createScrollWrapper native files directory!", e5);
        }
        this.programArgs = this.settings.get("minecraft.args");
        if (this.programArgs != null && this.programArgs.isEmpty()) {
            this.programArgs = null;
        }
        this.javaArgs = this.settings.get("minecraft.javaargs");
        if (this.javaArgs != null && this.javaArgs.isEmpty()) {
            this.javaArgs = null;
        }
        this.windowSize = this.settings.getClientWindowSize();
        if (this.windowSize[0] < 1) {
            throw new IllegalArgumentException("Invalid window width!");
        }
        if (this.windowSize[1] < 1) {
            throw new IllegalArgumentException("Invalid window height!");
        }
        this.fullScreen = this.settings.getBoolean("minecraft.fullscreen");
        if (this.version.isModpack() && this.version.getModpack().isModpackMemory()) {
            this.ramSize = this.version.getModpack().getMemory();
        }
        else {
            this.ramSize = this.settings.getInteger("minecraft.memory.ram2");
        }
        if (this.ramSize < 512) {
            throw new IllegalArgumentException("Invalid RAM size!");
        }
        for (final MinecraftLauncherAssistantInterface assistant : this.assistants) {
            assistant.collectInfo();
        }
        this.log("Checking conditions...");
        if (!this.version.appliesToCurrentEnvironment()) {
            Alert.showLocWarning("launcher.warning.title", (Object)"launcher.warning.incompatible.environment");
        }
        if (this.version.getMinecraftArguments() == null && this.version.getArguments() == null) {
            throw new MinecraftException("Can't run version, missing minecraftArguments", "noArgs", new Object[0]);
        }
    }
    
    private void downloadResources() throws MinecraftException {
        this.checkStep(MinecraftLauncherStep.COLLECTING, MinecraftLauncherStep.DOWNLOADING);
        for (final MinecraftExtendedListener listener : this.extListeners) {
            listener.onMinecraftComparingAssets();
        }
        final List<AssetIndex.AssetObject> assets = this.compareAssets();
        for (final MinecraftExtendedListener listener2 : this.extListeners) {
            listener2.onMinecraftDownloading();
        }
        DownloadableContainer execContainer;
        try {
            execContainer = this.vm.downloadVersion(this.versionSync, this.settings.getBoolean("skin.status.checkbox.state"), this.forceUpdate);
        }
        catch (IOException e) {
            throw new MinecraftException("Cannot download version!", "download-jar", e);
        }
        execContainer.setConsole(this.console);
        execContainer.addHandler(new DownloadableContainerHandler() {
            @Override
            public void onStart(final DownloadableContainer c) {
            }
            
            @Override
            public void onAbort(final DownloadableContainer c) {
            }
            
            @Override
            public void onError(final DownloadableContainer c, final Downloadable d, final Throwable e) {
            }
            
            @Override
            public void onComplete(final DownloadableContainer c, final Downloadable d) throws RetryDownloadException {
                if (!(d instanceof Library.LibraryDownloadable)) {
                    return;
                }
                final Library lib = ((Library.LibraryDownloadable)d).getDownloadableLibrary();
                if (lib.getChecksum() == null) {
                    return;
                }
                final String fileHash = FileUtil.getChecksum(d.getDestination(), "sha1");
                if (fileHash == null || fileHash.equals(lib.getChecksum())) {
                    return;
                }
                throw new RetryDownloadException("illegal library hash. got: " + fileHash + "; expected: " + lib.getChecksum());
            }
            
            @Override
            public void onFullComplete(final DownloadableContainer c) {
            }
        });
        final DownloadableContainer assetsContainer = this.am.downloadResources(this.version, assets, this.forceUpdate);
        final boolean[] kasperkyBlocking = { false };
        if (assetsContainer != null) {
            assetsContainer.addHandler(new DownloadableContainerHandler() {
                @Override
                public void onStart(final DownloadableContainer c) {
                }
                
                @Override
                public void onAbort(final DownloadableContainer c) {
                }
                
                @Override
                public void onError(final DownloadableContainer c, final Downloadable d, final Throwable e) {
                }
                
                @Override
                public void onComplete(final DownloadableContainer c, final Downloadable d) throws RetryDownloadException {
                    final String filename = d.getFilename();
                    AssetIndex.AssetObject object = null;
                    for (final AssetIndex.AssetObject asset : assets) {
                        if (filename.equals(asset.getHash())) {
                            object = asset;
                        }
                    }
                    if (object == null) {
                        MinecraftLauncher.this.log("Couldn't find object:", filename);
                        return;
                    }
                    final File destination = d.getDestination();
                    final String hash = FileUtil.getDigest(destination, "SHA-1", 40);
                    if (hash == null) {
                        throw new RetryDownloadException("File hash is NULL!");
                    }
                    final String assetHash = object.getHash();
                    if (assetHash == null) {
                        MinecraftLauncher.this.log("Hash of", object.getHash(), "is NULL");
                        return;
                    }
                    if (!hash.equals(assetHash)) {
                        if (hash.equals("2daeaa8b5f19f0bc209d976c02bd6acb51b00b0a")) {
                            kasperkyBlocking[0] = true;
                        }
                        throw new RetryDownloadException("illegal hash. got: " + hash + "; expected: " + assetHash);
                    }
                }
                
                @Override
                public void onFullComplete(final DownloadableContainer c) {
                    MinecraftLauncher.this.log("Assets have been downloaded");
                }
            });
            assetsContainer.setConsole(this.console);
        }
        if (assetsContainer != null) {
            this.downloader.add(assetsContainer);
        }
        this.downloader.add(execContainer);
        this.downloader.add(this.modpackManager.getContainer(this.version, this.forceUpdate));
        for (final MinecraftLauncherAssistantInterface assistant : this.assistants) {
            assistant.collectResources(this.downloader, this.forceUpdate);
        }
        this.downloader.startDownloadAndWait();
        if (execContainer.isAborted()) {
            throw new MinecraftLauncherAborted(new AbortedDownloadException());
        }
        if (!execContainer.getErrors().isEmpty()) {
            final boolean onlyOne = execContainer.getErrors().size() == 1;
            final StringBuilder message = new StringBuilder();
            message.append(execContainer.getErrors().size()).append(" error").append(onlyOne ? "" : "s").append(" occurred while trying to download binaries.");
            if (!onlyOne) {
                message.append(" Cause is the first of them.");
            }
            throw new MinecraftException(message.toString(), "download", execContainer.getErrors().get(0));
        }
        if (assetsContainer != null && !assetsContainer.getErrors().isEmpty() && !(assetsContainer.getErrors().get(0) instanceof AbortedDownloadException)) {
            if (kasperkyBlocking[0] && !MinecraftLauncher.ASSETS_PROBLEM[1]) {
                Alert.showLocWarning("launcher.warning.title", "launcher.warning.assets.kaspersky", "http://resources.download.minecraft.net/ad/ad*");
                MinecraftLauncher.ASSETS_PROBLEM[1] = true;
            }
            else if (!MinecraftLauncher.ASSETS_PROBLEM[0]) {
                Alert.showLocMessageWithoutTitle("launcher.warning.assets");
                MinecraftLauncher.ASSETS_PROBLEM[0] = true;
            }
        }
        else {
            Arrays.fill(MinecraftLauncher.ASSETS_PROBLEM, false);
        }
        try {
            this.vm.getLocalList().saveVersion(this.deJureVersion);
        }
        catch (IOException e2) {
            this.log("Cannot save version!", e2);
        }
    }
    
    private void constructProcess() throws MinecraftException {
        this.version = TLauncher.getInstance().getTLauncherManager().replacedLibraries(this.version);
        for (final MinecraftExtendedListener listener : this.extListeners) {
            listener.onMinecraftReconstructingAssets();
        }
        try {
            this.localAssetsDir = this.reconstructAssets();
        }
        catch (IOException e) {
            throw new MinecraftException("Cannot recounstruct assets!", "download-jar", e);
        }
        for (final MinecraftExtendedListener listener : this.extListeners) {
            listener.onMinecraftUnpackingNatives();
        }
        try {
            this.unpackNatives(this.forceUpdate);
        }
        catch (IOException e) {
            throw new MinecraftException("Cannot unpack natives!", "unpack-natives", e);
        }
        this.checkAborted();
        for (final MinecraftExtendedListener listener : this.extListeners) {
            listener.onMinecraftDeletingEntries();
        }
        try {
            this.deleteEntries();
        }
        catch (IOException e) {
            throw new MinecraftException("Cannot delete entries!", "delete-entries", e);
        }
        try {
            this.deleteLibraryEntries();
        }
        catch (Exception e2) {
            throw new MinecraftException("Cannot delete library entries!", "delete-entries", e2);
        }
        this.checkAborted();
        this.log("Constructing process...");
        for (final MinecraftExtendedListener listener : this.extListeners) {
            listener.onMinecraftConstructing();
        }
        (this.launcher = new JavaProcessLauncher(this.javaDir.getAbsolutePath(), new String[0])).directory(this.runningMinecraftDir);
        if (OS.OSX.isCurrent()) {
            File icon = null;
            try {
                icon = this.getAssetObject("icons/minecraft.icns");
            }
            catch (IOException e3) {
                this.log("Cannot get icon file from assets.", e3);
            }
            if (icon != null) {
                this.launcher.addCommand("-Xdock:icon=\"" + icon.getAbsolutePath() + "\"", "-Xdock:name=Minecraft");
            }
        }
        this.launcher.addCommands(this.getJVMArguments());
        if (this.javaArgs != null) {
            this.launcher.addSplitCommands(this.javaArgs);
        }
        for (final MinecraftLauncherAssistantInterface assistant : this.assistants) {
            assistant.constructJavaArguments();
        }
        this.launcher.addCommand(this.version.getMainClass());
        this.launcher.addCommands(this.getMinecraftArguments());
        if (this.fullScreen) {
            this.launcher.addCommand("--fullscreen");
        }
        if (this.server != null) {
            String[] address = StringUtils.split(this.server.getAddress(), ':');
            address = this.checkAndReplaceWrongAddress(address);
            switch (address.length) {
                case 2: {
                    this.launcher.addCommand("--port", address[1]);
                }
                case 1: {
                    this.launcher.addCommand("--server", address[0]);
                    break;
                }
                default: {
                    this.log("Cannot recognize server:", this.server);
                    break;
                }
            }
            if (this.server.getName() == null) {
                this.server = null;
            }
        }
        if (this.programArgs != null) {
            this.launcher.addSplitCommands(this.programArgs);
        }
        for (final MinecraftLauncherAssistantInterface assistant : this.assistants) {
            assistant.constructProgramArguments();
        }
        this.log("Full command (characters are not escaped):\n" + this.launcher.getCommandsAsString());
    }
    
    private String[] checkAndReplaceWrongAddress(final String[] address) {
        try {
            InetAddress.getByName(address[0]);
        }
        catch (UnknownHostException e2) {
            final MinecraftPing p = new MinecraftPing();
            final MinecraftPingOptions options = new MinecraftPingOptions().setHostname(address[0]);
            if (address.length == 2) {
                options.setPort(Integer.parseInt(address[1]));
            }
            try {
                p.resolveDNS(options);
                return new String[] { options.getHostname(), String.valueOf(options.getPort()) };
            }
            catch (Throwable e1) {
                U.log(e1);
            }
        }
        return address;
    }
    
    private void beforeRunTLauncherSkinVersion() {
        this.checkStep(MinecraftLauncherStep.DOWNLOADING, MinecraftLauncherStep.CONSTRUCTING);
        final Configuration conf = TLauncher.getInstance().getConfiguration();
        final boolean skinVersion = TLauncher.getInstance().getTLauncherManager().useTlauncherAuthlib(this.version);
        if (skinVersion && !conf.getBoolean("skin.notification.off")) {
            if (!conf.getBoolean("skin.notification.off.temp")) {
                conf.set("skin.notification.off.temp", true);
                SwingUtilities.invokeLater(SkinNotification::showMessage);
                TLauncher.getInstance().getVersionManager().startRefresh(true);
                throw new MinecraftLauncherAborted("shown skin message");
            }
            conf.set("skin.notification.off.temp", false);
        }
        else if (this.account.getType().equals(Account.AccountType.TLAUNCHER) && !skinVersion && !conf.getBoolean("skin.not.work.notification.hide")) {
            final String message = String.format(Localizable.get("skin.not.work.notification"), ImageCache.getRes("tlauncher-user.png").toExternalForm(), ImageCache.getRes("need-tl-version-for-skins.png").toExternalForm());
            if (Alert.showWarningMessageWithCheckBox("skin.notification.title", message, 500)) {
                conf.set("skin.not.work.notification.hide", true);
            }
        }
    }
    
    private File reconstructAssets() throws IOException {
        final String assetVersion = (this.version.getAssets() == null) ? "legacy" : this.version.getAssets();
        final File indexFile = new File(this.assetsIndexesDir, assetVersion + ".json");
        final File virtualRoot = new File(new File(this.globalAssetsDir, "virtual"), assetVersion);
        if (!indexFile.isFile()) {
            this.log("No assets index file " + virtualRoot + "; can't reconstruct assets");
            return virtualRoot;
        }
        final AssetIndex index = this.gson.fromJson(FileUtil.readFile(indexFile), AssetIndex.class);
        if (index.isVirtual()) {
            this.log("Reconstructing virtual assets folder at " + virtualRoot);
            for (final Map.Entry<String, AssetIndex.AssetObject> entry : index.getFileMap().entrySet()) {
                this.checkAborted();
                final File target = new File(virtualRoot, entry.getKey());
                final File original = new File(new File(this.assetsObjectsDir, entry.getValue().getHash().substring(0, 2)), entry.getValue().getHash());
                if (!original.isFile()) {
                    this.log("Skipped reconstructing:", original);
                }
                else {
                    if (!this.forceUpdate && target.isFile()) {
                        continue;
                    }
                    FileUtils.copyFile(original, target, false);
                    this.log(original, "->", target);
                }
            }
            FileUtil.writeFile(new File(virtualRoot, ".lastused"), this.dateAdapter.toString(new Date()));
        }
        return virtualRoot;
    }
    
    private File getAssetObject(final String name) throws IOException {
        final String assetVersion = this.version.getAssets();
        final File indexFile = new File(this.assetsIndexesDir, assetVersion + ".json");
        final AssetIndex index = this.gson.fromJson(FileUtil.readFile(indexFile), AssetIndex.class);
        if (index.getFileMap() == null) {
            throw new IOException("Cannot get filemap!");
        }
        final String hash = index.getFileMap().get(name).getHash();
        return new File(this.assetsObjectsDir, hash.substring(0, 2) + "/" + hash);
    }
    
    private void unpackNatives(final boolean force) throws IOException {
        this.log("Unpacking natives...");
        final Collection<Library> libraries = this.version.getRelevantLibraries();
        final OS os = OS.CURRENT;
        if (force) {
            this.nativeDir.delete();
        }
        for (final Library library : libraries) {
            final Map<OS, String> nativesPerOs = library.getNatives();
            if (nativesPerOs != null && nativesPerOs.get(os) != null) {
                final File file = new File(MinecraftUtil.getWorkingDirectory(), "libraries/" + library.getArtifactPath(nativesPerOs.get(os)));
                if (!file.isFile()) {
                    throw new IOException("Required archive doesn't exist: " + file.getAbsolutePath());
                }
                ZipFile zip;
                try {
                    zip = new ZipFile(file);
                }
                catch (IOException e) {
                    throw new IOException("Error opening ZIP archive: " + file.getAbsolutePath(), e);
                }
                final ExtractRules extractRules = library.getExtractRules();
                final Enumeration<? extends ZipEntry> entries = zip.entries();
                while (entries.hasMoreElements()) {
                    final ZipEntry entry = (ZipEntry)entries.nextElement();
                    if (extractRules == null || extractRules.shouldExtract(entry.getName())) {
                        final File targetFile = new File(this.nativeDir, entry.getName());
                        if (!force && targetFile.isFile()) {
                            continue;
                        }
                        if (targetFile.getParentFile() != null) {
                            targetFile.getParentFile().mkdirs();
                        }
                        if (entry.isDirectory()) {
                            continue;
                        }
                        final BufferedInputStream inputStream = new BufferedInputStream(zip.getInputStream(entry));
                        final byte[] buffer = new byte[2048];
                        final FileOutputStream outputStream = new FileOutputStream(targetFile);
                        final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
                        int length;
                        while ((length = inputStream.read(buffer, 0, buffer.length)) != -1) {
                            bufferedOutputStream.write(buffer, 0, length);
                        }
                        inputStream.close();
                        bufferedOutputStream.close();
                    }
                }
                zip.close();
            }
        }
    }
    
    private void deleteEntries() throws IOException {
        final List<String> entries = this.version.getDeleteEntries();
        if (entries == null || entries.size() == 0) {
            return;
        }
        this.log("Removing entries...");
        final File file = this.version.getFile(this.gameDir);
        this.removeFrom(file, entries);
    }
    
    private void deleteLibraryEntries() throws IOException {
        for (final Library lib : this.version.getLibraries()) {
            final List<String> entries = lib.getDeleteEntriesList();
            if (entries != null) {
                if (entries.isEmpty()) {
                    continue;
                }
                this.log("Processing entries of", lib.getName());
                this.removeFrom(new File(this.gameDir, "libraries/" + lib.getArtifactPath()), entries);
            }
        }
    }
    
    private String constructClassPath(final CompleteVersion version) throws MinecraftException {
        this.log("Constructing classpath...");
        final StringBuilder result = new StringBuilder();
        final Collection<File> classPath = version.getClassPath(OS.CURRENT, this.gameDir);
        final String separator = System.getProperty("path.separator");
        for (final File file : classPath) {
            if (!file.isFile()) {
                throw new MinecraftException("Classpath is not found: " + file, "classpath", new Object[] { file });
            }
            if (result.length() > 0) {
                result.append(separator);
            }
            result.append(file.getAbsolutePath());
        }
        return result.toString();
    }
    
    private String[] getMinecraftArguments() {
        this.log("Getting Minecraft arguments...");
        final List<String> list = new ArrayList<String>();
        if (this.version.getArguments() != null) {
            final List<Argument> arguments = this.version.getArguments().get(ArgumentType.GAME);
            for (final Argument argument : arguments) {
                if (argument.appliesToCurrentEnvironment()) {
                    list.addAll(Arrays.asList(argument.getValues()));
                }
            }
        }
        else {
            list.addAll(Arrays.asList(this.version.getMinecraftArguments().split(" ")));
            list.add("--width");
            list.add("${resolution_width}");
            list.add("--height");
            list.add("${resolution_height}");
        }
        final Map<String, String> map = new HashMap<String, String>();
        final String assets = this.version.getAssets();
        map.put("auth_username", this.accountName);
        if (!this.account.isFree()) {
            map.put("auth_session", String.format("token:%s:%s", this.account.getAccessToken(), this.account.getProfile().getId()));
            map.put("auth_access_token", this.account.getAccessToken());
            map.put("user_properties", this.gson.toJson(this.account.getProperties()));
            if (this.settings.getBoolean("skip.account.property")) {
                map.put("user_properties", "{}");
            }
            map.put("auth_player_name", this.account.getDisplayName());
            map.put("auth_uuid", this.account.getUUID());
            map.put("user_type", "mojang");
            map.put("profile_name", this.account.getProfile().getName());
        }
        else {
            map.put("auth_session", "null");
            map.put("auth_access_token", "null");
            map.put("user_properties", "[]");
            map.put("auth_player_name", this.accountName);
            map.put("auth_uuid", new UUID(0L, 0L).toString());
            map.put("user_type", "legacy");
            map.put("profile_name", "(Default)");
        }
        map.put("version_type", this.version.getReleaseType().toString());
        map.put("version_name", this.version.getID());
        map.put("game_directory", this.runningMinecraftDir.getAbsolutePath());
        map.put("game_assets", this.localAssetsDir.getAbsolutePath());
        map.put("assets_root", this.globalAssetsDir.getAbsolutePath());
        map.put("assets_index_name", (assets == null) ? "legacy" : assets);
        map.put("resolution_width", String.valueOf(this.windowSize[0]));
        map.put("resolution_height", String.valueOf(this.windowSize[1]));
        final StrSubstitutor substitutor = new StrSubstitutor((Map)map);
        final String[] split = list.toArray(new String[0]);
        for (int i = 0; i < split.length; ++i) {
            split[i] = substitutor.replace(split[i]);
        }
        return split;
    }
    
    private String[] getJVMArguments() throws MinecraftException {
        final List<String> list = new ArrayList<String>();
        final Map<String, String> map = new HashMap<String, String>();
        list.add("-Djava.net.preferIPv4Stack=true");
        if (OS.CURRENT == OS.WINDOWS && System.getProperty("os.version").startsWith("10.")) {
            list.add("-Dos.name=Windows 10");
            list.add("-Dos.version=10.0");
        }
        if (this.version.getArguments() != null) {
            final List<Argument> arguments = this.version.getArguments().get(ArgumentType.JVM);
            list.add("-Xmn128M");
            list.add("-Xmx" + this.ramSize + "M");
            for (final Argument argument : arguments) {
                if (argument.appliesToCurrentEnvironment()) {
                    list.addAll(Arrays.asList(argument.getValues()));
                }
            }
        }
        else {
            list.add("-Xmn128M");
            list.add("-Xmx" + this.ramSize + "M");
            if (this.version.getJVMArguments() != null) {
                final String[] array = StringUtils.split(this.version.getJVMArguments(), ' ');
                list.addAll(Arrays.asList(array));
            }
            list.add("-Djava.library.path=${natives_directory}");
            list.add("-cp");
            list.add("${classpath}");
        }
        list.add("-Dminecraft.applet.TargetDirectory=${game_directory}");
        map.put("game_directory", this.runningMinecraftDir.getAbsolutePath());
        if (OS.Arch.TOTAL_RAM_MB > 4000 && !this.getConfiguration().getBoolean("not.proper.g1gc")) {
            list.add("-XX:+UnlockExperimentalVMOptions");
            list.add("-XX:+UseG1GC");
            list.add("-XX:G1NewSizePercent=20");
            list.add("-XX:G1ReservePercent=20");
            list.add("-XX:MaxGCPauseMillis=50");
            list.add("-XX:G1HeapRegionSize=32M");
        }
        else {
            list.add("-XX:+UseConcMarkSweepGC");
        }
        list.add("-Dfml.ignoreInvalidMinecraftCertificates=true");
        list.add("-Dfml.ignorePatchDiscrepancies=true");
        if (this.version.isModpack()) {
            list.add("-DLibLoader.modsFolder=" + ModpackUtil.getPathByVersion(this.version, "mods"));
        }
        final String[] split = list.toArray(new String[0]);
        map.put("natives_directory", this.nativeDir.toString());
        map.put("classpath", this.constructClassPath(this.version));
        map.put("launcher_name", "minecraft-launcher");
        map.put("launcher_version", "2.0.1003");
        final StrSubstitutor substitutor = new StrSubstitutor((Map)map);
        for (int i = 0; i < split.length; ++i) {
            split[i] = substitutor.replace(split[i]);
        }
        return split;
    }
    
    private List<AssetIndex.AssetObject> compareAssets() {
        this.migrateOldAssets();
        this.log("Comparing assets...");
        final long start = System.nanoTime();
        final List<AssetIndex.AssetObject> result = this.am.checkResources(this.version, !this.forceUpdate);
        final long end = System.nanoTime();
        final long delta = end - start;
        this.log("Delta time to compare assets: " + delta / 1000000L + " ms.");
        return result;
    }
    
    private void migrateOldAssets() {
        if (!this.globalAssetsDir.isDirectory()) {
            return;
        }
        final File skinsDir = new File(this.globalAssetsDir, "skins");
        if (skinsDir.isDirectory()) {
            FileUtil.deleteDirectory(skinsDir);
        }
        final IOFileFilter migratableFilter = FileFilterUtils.notFileFilter(FileFilterUtils.or(FileFilterUtils.nameFileFilter("indexes"), FileFilterUtils.nameFileFilter("objects"), FileFilterUtils.nameFileFilter("virtual")));
        for (final File file : new TreeSet<File>(FileUtils.listFiles(this.globalAssetsDir, TrueFileFilter.TRUE, migratableFilter))) {
            final String hash = FileUtil.getDigest(file, "SHA-1", 40);
            if (hash != null) {
                final File destinationFile = new File(this.assetsObjectsDir, hash.substring(0, 2) + "/" + hash);
                if (!destinationFile.exists()) {
                    this.log("Migrated old asset", file, "into", destinationFile);
                    try {
                        FileUtils.copyFile(file, destinationFile);
                    }
                    catch (IOException e) {
                        this.log("Couldn't migrate old asset", e);
                    }
                }
            }
            FileUtils.deleteQuietly(file);
        }
        final File[] assets = this.globalAssetsDir.listFiles();
        if (assets != null) {
            for (final File file2 : assets) {
                if (!file2.getName().equals("indexes") && !file2.getName().equals("objects") && !file2.getName().equals("virtual")) {
                    this.log("Cleaning up old assets directory", file2, "after migration");
                    FileUtils.deleteQuietly(file2);
                }
            }
        }
    }
    
    private void launchMinecraft() throws MinecraftException {
        this.checkStep(MinecraftLauncherStep.CONSTRUCTING, MinecraftLauncherStep.LAUNCHING);
        this.log("Launching Minecraft...");
        for (final MinecraftListener listener : this.listeners) {
            listener.onMinecraftLaunch();
        }
        this.log("Starting Minecraft " + this.versionName + "...");
        this.log("Launching in:", this.runningMinecraftDir.getAbsolutePath());
        this.startupTime = System.currentTimeMillis();
        TLauncher.getConsole().setLauncher(this);
        if (this.console != null) {
            final Calendar startDate = Calendar.getInstance();
            startDate.setTimeInMillis(this.startupTime);
            this.console.setName(this.version.getID() + " (" + DateFormat.getDateTimeInstance().format(startDate.getTime()) + ")");
            this.console.setLauncher(this);
        }
        U.gc();
        try {
            this.launcher.setListener(this);
            this.process = this.launcher.start();
            this.minecraftWorking = true;
        }
        catch (Exception e) {
            this.notifyClose();
            throw new MinecraftException("Cannot start the game!", "start", e);
        }
    }
    
    private void postLaunch() {
        this.checkStep(MinecraftLauncherStep.LAUNCHING, MinecraftLauncherStep.POSTLAUNCH);
        this.log("Processing post-launch actions. Assist launch:", this.assistLaunch);
        for (final MinecraftExtendedListener listener : this.extListeners) {
            listener.onMinecraftPostLaunch();
        }
        if (this.assistLaunch) {
            this.waitForClose();
        }
        else {
            U.sleepFor(30000L);
            if (this.minecraftWorking) {
                TLauncher.kill();
            }
        }
    }
    
    public void killProcess() {
        if (!this.minecraftWorking) {
            throw new IllegalStateException();
        }
        this.log("Killing Minecraft forcefully");
        this.killed = true;
        this.process.stop();
    }
    
    private void plog(final Object... o) {
        final String text = U.toLog(o);
        if (this.console == null) {
            synchronized (this.output) {
                this.output.append(text).append('\n');
            }
        }
        else {
            this.console.log(text);
        }
    }
    
    public void log(final Object... o) {
        U.log("[Launcher]", o);
        this.plog("[L]", o);
    }
    
    private void checkThread() {
        if (!Thread.currentThread().equals(this.parentThread)) {
            throw new IllegalStateException("Illegal thread!");
        }
    }
    
    private void checkStep(final MinecraftLauncherStep prevStep, final MinecraftLauncherStep currentStep) {
        this.checkAborted();
        if (prevStep == null || currentStep == null) {
            throw new NullPointerException("NULL: " + prevStep + " " + currentStep);
        }
        if (!this.step.equals(prevStep)) {
            throw new IllegalStateException("Called from illegal step: " + this.step);
        }
        this.checkThread();
        this.step = currentStep;
    }
    
    private void checkAborted() {
        if (!this.working) {
            throw new MinecraftLauncherAborted("Aborted at step: " + this.step);
        }
    }
    
    private void checkWorking() {
        if (this.working) {
            throw new IllegalStateException("Launcher is working!");
        }
    }
    
    @Override
    public void onJavaProcessLog(final JavaProcess jp, final String line) {
        if (this.firstLine) {
            this.firstLine = false;
            U.plog("===============================================================================================");
            this.plog("===============================================================================================");
        }
        U.plog(">", line);
        this.plog(line);
    }
    
    @Override
    public void onJavaProcessEnded(final JavaProcess jp) {
        this.notifyClose();
        if (TLauncher.getConsole().getLauncher() == this) {
            TLauncher.getConsole().setLauncher(null);
        }
        if (this.console != null) {
            this.console.setLauncher(null);
        }
        final int exit = jp.getExitCode();
        this.log("Minecraft closed with exit code: " + exit);
        this.exitCode = exit;
        final Crash crash = this.killed ? null : this.descriptor.scan();
        if (crash == null) {
            if (!this.assistLaunch) {
                TLauncher.kill();
            }
            if (this.console != null) {
                this.console.killIn(7000L);
            }
        }
        else {
            if (TLauncher.getConsole() != null) {
                TLauncher.getConsole().show();
            }
            for (final MinecraftListener listener : this.listeners) {
                listener.onMinecraftCrash(crash);
            }
        }
    }
    
    @Override
    public void onJavaProcessError(final JavaProcess jp, final Throwable e) {
        this.notifyClose();
        for (final MinecraftListener listener : this.listeners) {
            listener.onMinecraftError(e);
        }
    }
    
    private synchronized void waitForClose() {
        while (this.minecraftWorking) {
            try {
                this.wait();
            }
            catch (InterruptedException ex) {}
        }
    }
    
    private synchronized void notifyClose() {
        this.minecraftWorking = false;
        if (System.currentTimeMillis() - this.startupTime < 5000L) {
            U.sleepFor(1000L);
        }
        this.notifyAll();
        for (final MinecraftListener listener : this.listeners) {
            listener.onMinecraftClose();
        }
        if (this.clearNatives && Files.exists(this.nativeDir.toPath(), new LinkOption[0])) {
            FileUtil.deleteDirectory(this.nativeDir);
        }
    }
    
    private void removeFrom(final File zipFile, final List<String> entries) throws IOException {
        final File tempFile = File.createTempFile(zipFile.getName(), null);
        tempFile.delete();
        tempFile.deleteOnExit();
        final boolean renameOk = zipFile.renameTo(tempFile);
        if (!renameOk) {
            throw new IOException("Could not rename the file " + zipFile.getAbsolutePath() + " -> " + tempFile.getAbsolutePath());
        }
        this.log("Removing entries from", zipFile);
        final byte[] buf = new byte[1024];
        final ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(tempFile)), StandardCharsets.UTF_8);
        final ZipOutputStream zout = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)), StandardCharsets.UTF_8);
        for (ZipEntry entry = zin.getNextEntry(); entry != null; entry = zin.getNextEntry()) {
            final String name = entry.getName();
            if (entries.contains(name)) {
                this.log("Removed:", name);
            }
            else {
                zout.putNextEntry(new ZipEntry(name));
                int len;
                while ((len = zin.read(buf)) > 0) {
                    zout.write(buf, 0, len);
                }
            }
        }
        zin.close();
        zout.close();
        tempFile.delete();
    }
    
    public File getRunningMinecraftDir() {
        return this.runningMinecraftDir;
    }
    
    static {
        ASSETS_PROBLEM = new boolean[2];
    }
    
    public enum MinecraftLauncherStep
    {
        NONE, 
        COLLECTING, 
        DOWNLOADING, 
        CONSTRUCTING, 
        LAUNCHING, 
        POSTLAUNCH;
    }
    
    public enum ConsoleVisibility
    {
        ALWAYS, 
        ON_CRASH, 
        NONE;
    }
    
    public static class MinecraftLauncherAborted extends RuntimeException
    {
        private static final long serialVersionUID = -3001265213093607559L;
        
        MinecraftLauncherAborted(final String message) {
            super(message);
        }
        
        MinecraftLauncherAborted(final Throwable cause) {
            super(cause);
        }
    }
}
