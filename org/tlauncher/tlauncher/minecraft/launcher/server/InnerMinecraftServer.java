// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher.server;

import net.minecraft.launcher.versions.Version;
import org.tlauncher.tlauncher.entity.server.Server;
import java.io.IOException;

public interface InnerMinecraftServer
{
    void initInnerServers();
    
    void prepareInnerServer() throws IOException;
    
    void searchRemovedServers() throws IOException;
    
    void addPageServer(final Server p0) throws IOException;
    
    void addPageServerToModpack(final Server p0, final Version p1) throws IOException;
}
