// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher.server;

import org.tlauncher.util.MinecraftUtil;
import net.minecraft.common.NBTBase;
import net.minecraft.common.NBTTagList;
import org.tlauncher.exceptions.ParseException;
import net.minecraft.common.NBTTagCompound;
import java.util.Objects;
import net.minecraft.common.CompressedStreamTools;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.util.ArrayList;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import net.minecraft.launcher.versions.Version;
import java.io.IOException;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.managers.ServerList;
import org.tlauncher.util.FileUtil;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import org.apache.commons.lang3.time.DateUtils;
import java.util.Date;
import org.tlauncher.tlauncher.managers.ServerListManager;
import org.tlauncher.util.U;
import javax.inject.Named;
import com.google.gson.Gson;
import org.tlauncher.tlauncher.entity.server.Server;
import org.tlauncher.tlauncher.entity.server.RemoteServer;
import java.util.List;
import com.google.inject.Inject;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.io.File;
import com.google.inject.Singleton;

@Singleton
public class InnerMinecraftServersImpl implements InnerMinecraftServer
{
    private static final File INNER_SERVERS_FILE;
    private static final File SERVERS_DAT_FILE;
    @Inject
    private TLauncher tlauncher;
    private List<RemoteServer> innerServers;
    private List<Server> localServerList;
    @Inject
    @Named("GsonCompleteVersion")
    private Gson gson;
    
    @Override
    public void prepareInnerServer() throws IOException {
        U.classNameLog(InnerMinecraftServersImpl.class, "prepare inner servers");
        final ServerList serverList = this.tlauncher.getManager().getComponent(ServerListManager.class).getList();
        final Configuration settings = this.tlauncher.getConfiguration();
        if (!this.tlauncher.getProfileManager().hasPremium() || settings.getBoolean("gui.settings.servers.recommendation")) {
            this.addNewServers(serverList.getNewServers());
            final int hours;
            final Date addedDate;
            this.innerServers.stream().filter(s -> s.getState().equals(RemoteServer.ServerState.DEACTIVATED)).filter(s -> s.getMaxRemovingCountServer() > s.getRemovedTime().size()).filter(s -> {
                hours = s.getRecoveryServerTime();
                addedDate = DateUtils.addHours(new Date(s.getRemovedTime().get(s.getRemovedTime().size() - 1)), hours);
                return new Date().after(addedDate);
            }).forEach(s -> {
                s.setState(RemoteServer.ServerState.ACTIVE);
                if (!this.localServerList.contains(s)) {
                    this.localServerList.add(0, s);
                }
                return;
            });
        }
        if (!this.tlauncher.getProfileManager().hasPremium() || settings.getBoolean("gui.settings.guard.checkbox")) {
            this.innerServers.removeIf(s -> serverList.getRemovedServers().stream().map((Function<? super Object, ?>)String::toLowerCase).collect((Collector<? super Object, ?, List<? super Object>>)Collectors.toList()).contains(s.getAddress().toLowerCase()));
            this.localServerList.removeIf(s -> serverList.getRemovedServers().stream().map((Function<? super Object, ?>)String::toLowerCase).collect((Collector<? super Object, ?, List<? super Object>>)Collectors.toList()).contains(s.getAddress().toLowerCase()));
        }
        try {
            this.writeServerDatFile(InnerMinecraftServersImpl.SERVERS_DAT_FILE, this.localServerList);
            FileUtil.writeFile(InnerMinecraftServersImpl.INNER_SERVERS_FILE, this.gson.toJson(this.innerServers));
        }
        catch (RuntimeException e) {
            U.log(e);
        }
    }
    
    @Override
    public void searchRemovedServers() throws IOException {
        U.classNameLog(InnerMinecraftServersImpl.class, "search changers of the servers");
        this.localServerList = this.readServerDatFile(InnerMinecraftServersImpl.SERVERS_DAT_FILE);
        this.innerServers.stream().filter(s -> s.getState().equals(RemoteServer.ServerState.ACTIVE)).filter(s -> !this.localServerList.contains(s)).forEach(s -> {
            s.setState(RemoteServer.ServerState.DEACTIVATED);
            s.getRemovedTime().add(new Date().getTime());
            return;
        });
        final boolean b;
        this.innerServers.removeIf(s -> {
            if (s.getState().equals(RemoteServer.ServerState.DEACTIVATED) && s.getRemovedTime().size() >= s.getMaxRemovingCountServer()) {
                if (new Date().after(DateUtils.addDays(s.getAddedDate(), 5))) {
                    return b;
                }
            }
            return b;
        });
    }
    
    @Override
    public void addPageServer(final Server server) throws IOException {
        (this.localServerList = this.readServerDatFile(InnerMinecraftServersImpl.SERVERS_DAT_FILE)).removeIf(s -> s.getAddress().equals(server.getAddress()));
        this.localServerList.add(0, server);
        this.writeServerDatFile(InnerMinecraftServersImpl.SERVERS_DAT_FILE, this.localServerList);
    }
    
    @Override
    public void addPageServerToModpack(final Server server, final Version c) throws IOException {
        final Path path = ModpackUtil.getPathByVersion(c, "servers.dat");
        List<Server> list = new ArrayList<Server>();
        if (Files.exists(path, new LinkOption[0])) {
            list = this.readServerDatFile(path.toFile());
        }
        list.remove(server);
        list.add(0, server);
        this.writeServerDatFile(path.toFile(), list);
    }
    
    private void addNewServers(final List<RemoteServer> remoteServers) {
        final String lang = Localizable.get().getSelected().toString();
        final Object o;
        remoteServers.stream().filter(s -> !this.localServerList.contains(s)).filter(s -> s.getLocales().isEmpty() || s.getLocales().contains(o)).filter(s -> !this.innerServers.contains(s) || this.innerServers.stream().filter(s1 -> s1.equals(s)).anyMatch(s1 -> s1.getState().equals(RemoteServer.ServerState.ACTIVE))).forEach(s -> {
            s.initRemote();
            if (!this.innerServers.contains(s)) {
                this.innerServers.add(0, s);
            }
            this.localServerList.add(0, s);
        });
    }
    
    @Override
    public void initInnerServers() {
        if (Files.exists(InnerMinecraftServersImpl.INNER_SERVERS_FILE.toPath(), new LinkOption[0])) {
            try {
                this.innerServers = this.gson.fromJson(FileUtil.readFile(InnerMinecraftServersImpl.INNER_SERVERS_FILE), new TypeToken<List<RemoteServer>>() {}.getType());
                return;
            }
            catch (JsonParseException | IOException ex2) {
                final Exception ex;
                final Exception e = ex;
                U.log(e);
            }
        }
        this.innerServers = new ArrayList<RemoteServer>();
    }
    
    private List<Server> readServerDatFile(final File file) throws IOException {
        final List<Server> list = new ArrayList<Server>();
        final NBTTagCompound compound = CompressedStreamTools.read(file);
        if (Objects.nonNull(compound)) {
            final NBTTagList servers = compound.getTagList("servers");
            for (int i = 0; i < servers.tagCount(); ++i) {
                try {
                    list.add(Server.loadFromNBT((NBTTagCompound)servers.tagAt(i)));
                }
                catch (ParseException e) {
                    U.log("found server ", e);
                }
            }
            U.log("read servers from servers.dat", list);
        }
        return list;
    }
    
    private void writeServerDatFile(final File file, final List<Server> list) throws IOException {
        U.log("save servers to servers.dat");
        final NBTTagList servers = new NBTTagList();
        list.forEach(s -> servers.appendTag(s.getNBT()));
        final NBTTagCompound compound = new NBTTagCompound();
        compound.setTag("servers", servers);
        CompressedStreamTools.safeWrite(compound, file);
    }
    
    static {
        INNER_SERVERS_FILE = MinecraftUtil.getTLauncherFile("InnerStateServer-1.2.json");
        SERVERS_DAT_FILE = FileUtil.getRelative("servers.dat").toFile();
    }
}
