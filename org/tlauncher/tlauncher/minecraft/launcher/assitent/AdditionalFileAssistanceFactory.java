// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher.assitent;

import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public interface AdditionalFileAssistanceFactory
{
    AdditionalFileAssistance create(final MinecraftLauncher p0);
}
