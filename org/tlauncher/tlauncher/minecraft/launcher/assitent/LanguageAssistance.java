// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher.assitent;

import org.tlauncher.tlauncher.ui.loc.Localizable;
import java.util.List;
import java.util.Date;
import net.minecraft.launcher.versions.CompleteVersion;
import org.tlauncher.util.U;
import org.tlauncher.util.FileUtil;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;
import java.io.File;
import java.text.SimpleDateFormat;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public class LanguageAssistance extends MinecraftLauncherAssistantWrapper
{
    public static String OPTIONS;
    
    @Inject
    public LanguageAssistance(@Assisted final MinecraftLauncher launcher) {
        super(launcher);
    }
    
    @Override
    public void constructProgramArguments() {
        final CompleteVersion v = this.launcher.getVersion();
        final Date date = v.getReleaseTime();
        try {
            final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            final Date checkDate = format.parse("2016-11-14");
            final File gameFolder = this.launcher.getRunningMinecraftDir();
            final File options = new File(gameFolder, LanguageAssistance.OPTIONS);
            if (options.exists()) {
                final List<String> lines = Files.readAllLines(options.toPath(), StandardCharsets.UTF_8);
                int index = -1;
                for (int i = 0; i < lines.size(); ++i) {
                    if (lines.get(i).contains("lang:")) {
                        index = i;
                        break;
                    }
                }
                if (index != -1 && date.before(checkDate)) {
                    String line = lines.get(index);
                    final String[] array = line.split(":");
                    final String[] arraysLang = array[1].split("_");
                    final String end = arraysLang[1];
                    if (!end.toUpperCase().equals(end)) {
                        line = "lang:" + arraysLang[0] + "_" + end.toUpperCase();
                        lines.remove(index);
                        lines.add(index, line);
                        FileUtil.writeFile(options, String.join(System.lineSeparator(), lines));
                    }
                }
                else if (index == -1) {
                    final String lang = this.getLang(date, checkDate);
                    lines.add(lang);
                    FileUtil.writeFile(options, String.join(System.lineSeparator(), lines));
                }
            }
            else {
                final String lang2 = this.getLang(date, checkDate);
                FileUtil.writeFile(options, lang2);
            }
        }
        catch (Throwable e) {
            U.log(e);
        }
    }
    
    private String getLang(final Date date, final Date checkDate) {
        String lang = Localizable.get().getSelected().toString();
        if (date.after(checkDate)) {
            lang = lang.toLowerCase();
        }
        return "lang:" + lang;
    }
    
    static {
        LanguageAssistance.OPTIONS = "options.txt";
    }
}
