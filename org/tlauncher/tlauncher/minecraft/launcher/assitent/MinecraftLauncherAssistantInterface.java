// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher.assitent;

import org.tlauncher.tlauncher.downloader.Downloader;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;

public interface MinecraftLauncherAssistantInterface
{
    void collectInfo() throws MinecraftException;
    
    void collectResources(final Downloader p0, final boolean p1) throws MinecraftException;
    
    void constructJavaArguments() throws MinecraftException;
    
    void constructProgramArguments() throws MinecraftException;
}
