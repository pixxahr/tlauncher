// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher.assitent;

import org.tlauncher.util.FileUtil;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import org.tlauncher.tlauncher.downloader.DownloadableContainerHandler;
import org.tlauncher.tlauncher.downloader.RetryDownloadException;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.downloader.mods.GameEntityDownloader;
import org.tlauncher.tlauncher.downloader.mods.GameEntityHandler;
import java.io.IOException;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.downloader.mods.ArchiveGameEntityDownloader;
import org.tlauncher.util.MinecraftUtil;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import org.tlauncher.tlauncher.entity.AdditionalAsset;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.downloader.Downloader;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public class SoundAssist extends MinecraftLauncherAssistantWrapper
{
    @Inject
    public SoundAssist(@Assisted final MinecraftLauncher launcher) {
        super(launcher);
    }
    
    @Override
    public void collectResources(final Downloader d, final boolean force) throws MinecraftException {
        final List<AdditionalAsset> assets = TLauncher.getInstance().getAdditionalAssetsComponent().getAdditionalAssets();
        final DownloadableContainer c = new DownloadableContainer();
        try {
            for (final AdditionalAsset r : assets) {
                if (r.getVersions().contains(this.launcher.getVersion().getJar()) || r.getVersions().contains(this.launcher.getVersionName())) {
                    for (final MetadataDTO m : r.getFiles()) {
                        if (this.notExistsOrCorrect(m) && !this.copyFromLocalRepo(m)) {
                            c.add(new ArchiveGameEntityDownloader(ClientInstanceRepo.EXTRA_VERSION_REPO, true, m, MinecraftUtil.getTLauncherFile("repo").toPath()));
                        }
                    }
                }
            }
        }
        catch (IOException e) {
            throw new MinecraftException(e.getMessage(), "download-jar", e);
        }
        if (!c.getList().isEmpty()) {
            c.addHandler(new GameEntityHandler() {
                @Override
                public void onComplete(final DownloadableContainer c, final Downloadable d) throws RetryDownloadException {
                    super.onComplete(c, d);
                    final GameEntityDownloader g = (GameEntityDownloader)d;
                    try {
                        SoundAssist.this.copyFromLocalRepo(g.getMetadata());
                    }
                    catch (IOException e) {
                        U.log(e);
                        throw new RetryDownloadException("problem with sound", e);
                    }
                }
            });
            d.add(c);
        }
    }
    
    private boolean notExistsOrCorrect(final MetadataDTO m) {
        final File f = new File(this.launcher.getRunningMinecraftDir(), m.getPath());
        return !f.exists() || !m.getSha1().equalsIgnoreCase(FileUtil.getChecksum(f));
    }
    
    private boolean copyFromLocalRepo(final MetadataDTO m) throws IOException {
        final File source = MinecraftUtil.getTLauncherFile("repo/" + m.getPath());
        final File target = new File(this.launcher.getRunningMinecraftDir(), m.getPath());
        if (m.getSha1().equalsIgnoreCase(FileUtil.getChecksum(source))) {
            FileUtil.copyFile(source, target, true);
            return true;
        }
        return false;
    }
}
