// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher.assitent;

import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import org.tlauncher.tlauncher.downloader.Downloader;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public abstract class MinecraftLauncherAssistantWrapper implements MinecraftLauncherAssistantInterface
{
    protected MinecraftLauncher launcher;
    
    @Override
    public void collectInfo() {
    }
    
    @Override
    public void collectResources(final Downloader d, final boolean force) throws MinecraftException {
    }
    
    @Override
    public void constructJavaArguments() {
    }
    
    @Override
    public void constructProgramArguments() {
    }
    
    public MinecraftLauncherAssistantWrapper(final MinecraftLauncher launcher) {
        this.launcher = launcher;
    }
}
