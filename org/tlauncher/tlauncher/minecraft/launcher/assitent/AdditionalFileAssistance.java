// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher.assitent;

import net.minecraft.launcher.Http;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import org.tlauncher.util.FileUtil;
import java.io.File;
import java.net.URL;
import org.tlauncher.util.MinecraftUtil;
import java.util.Iterator;
import java.util.List;
import org.tlauncher.tlauncher.downloader.DownloadableContainerHandler;
import org.tlauncher.tlauncher.downloader.RetryDownloadException;
import org.tlauncher.tlauncher.downloader.mods.GameEntityDownloader;
import org.tlauncher.tlauncher.downloader.mods.GameEntityHandler;
import java.io.IOException;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;
import java.util.Objects;
import org.tlauncher.tlauncher.downloader.Downloader;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public class AdditionalFileAssistance extends MinecraftLauncherAssistantWrapper
{
    @Inject
    public AdditionalFileAssistance(@Assisted final MinecraftLauncher launcher) {
        super(launcher);
    }
    
    @Override
    public void collectResources(final Downloader d, final boolean force) throws MinecraftException {
        final List<MetadataDTO> list = this.launcher.getVersion().getAdditionalFiles();
        if (Objects.isNull(list)) {
            return;
        }
        final DownloadableContainer c = new DownloadableContainer();
        try {
            for (final MetadataDTO m : list) {
                if (this.notExistsOrCorrect(m) && !this.copyFromLocalRepo(m)) {
                    c.add(new AdditionalFileDownloader(m));
                }
            }
        }
        catch (IOException e) {
            U.log(e);
            throw new MinecraftException(e.getMessage(), "download-jar", e);
        }
        if (!c.getList().isEmpty()) {
            c.addHandler(new GameEntityHandler() {
                @Override
                public void onComplete(final DownloadableContainer c, final Downloadable d) throws RetryDownloadException {
                    super.onComplete(c, d);
                    final GameEntityDownloader g = (GameEntityDownloader)d;
                    try {
                        AdditionalFileAssistance.this.copyFromLocalRepo(g.getMetadata());
                    }
                    catch (IOException e) {
                        U.log(e);
                        throw new RetryDownloadException("problem with file", e);
                    }
                }
            });
            d.add(c);
        }
    }
    
    private boolean copyFromLocalRepo(final MetadataDTO m) throws IOException {
        final File source = new File(MinecraftUtil.getWorkingDirectory().getAbsolutePath(), new URL(m.getUrl()).getPath());
        final File target = new File(MinecraftUtil.getWorkingDirectory(), m.getPath());
        if (!m.getSha1().equalsIgnoreCase(FileUtil.getChecksum(source))) {
            return false;
        }
        if (source.equals(target)) {
            return true;
        }
        FileUtil.copyFile(source, target, true);
        return true;
    }
    
    private boolean notExistsOrCorrect(final MetadataDTO m) {
        final File target = new File(MinecraftUtil.getWorkingDirectory(), m.getPath());
        return !target.exists() || !m.getSha1().equalsIgnoreCase(FileUtil.getChecksum(target));
    }
    
    private class AdditionalFileDownloader extends GameEntityDownloader
    {
        public AdditionalFileDownloader(final MetadataDTO metadata) {
            super(ClientInstanceRepo.EMPTY_REPO, true, metadata.getUrl(), new File(MinecraftUtil.getWorkingDirectory().getAbsolutePath(), Http.constantURL(metadata.getUrl()).getPath()));
            this.setMetadata(metadata);
        }
    }
}
