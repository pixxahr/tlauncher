// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher;

import org.tlauncher.tlauncher.minecraft.crash.Crash;

public interface MinecraftListener
{
    void onMinecraftPrepare();
    
    void onMinecraftAbort();
    
    void onMinecraftLaunch();
    
    void onMinecraftClose();
    
    void onMinecraftError(final Throwable p0);
    
    void onMinecraftKnownError(final MinecraftException p0);
    
    void onMinecraftCrash(final Crash p0);
}
