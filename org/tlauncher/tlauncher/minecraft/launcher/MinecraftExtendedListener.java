// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher;

public interface MinecraftExtendedListener extends MinecraftListener
{
    void onMinecraftCollecting();
    
    void onMinecraftComparingAssets();
    
    void onMinecraftDownloading();
    
    void onMinecraftReconstructingAssets();
    
    void onMinecraftUnpackingNatives();
    
    void onMinecraftDeletingEntries();
    
    void onMinecraftConstructing();
    
    void onMinecraftLaunch();
    
    void onMinecraftPostLaunch();
}
