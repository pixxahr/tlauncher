// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.launcher;

import org.tlauncher.tlauncher.minecraft.crash.Crash;

public class MinecraftExtendedAdapter implements MinecraftExtendedListener
{
    @Override
    public void onMinecraftCollecting() {
    }
    
    @Override
    public void onMinecraftComparingAssets() {
    }
    
    @Override
    public void onMinecraftDownloading() {
    }
    
    @Override
    public void onMinecraftReconstructingAssets() {
    }
    
    @Override
    public void onMinecraftUnpackingNatives() {
    }
    
    @Override
    public void onMinecraftDeletingEntries() {
    }
    
    @Override
    public void onMinecraftConstructing() {
    }
    
    @Override
    public void onMinecraftPrepare() {
    }
    
    @Override
    public void onMinecraftAbort() {
    }
    
    @Override
    public void onMinecraftLaunch() {
    }
    
    @Override
    public void onMinecraftClose() {
    }
    
    @Override
    public void onMinecraftError(final Throwable e) {
    }
    
    @Override
    public void onMinecraftKnownError(final MinecraftException e) {
    }
    
    @Override
    public void onMinecraftCrash(final Crash crash) {
    }
    
    @Override
    public void onMinecraftPostLaunch() {
    }
}
