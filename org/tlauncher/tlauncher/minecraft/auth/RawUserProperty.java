// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

import java.util.LinkedHashMap;

class RawUserProperty extends LinkedHashMap<String, String>
{
    private static final long serialVersionUID = 1281494999913983388L;
    
    public UserProperty toProperty() {
        return new UserProperty(((LinkedHashMap<K, String>)this).get("name"), ((LinkedHashMap<K, String>)this).get("value"));
    }
}
