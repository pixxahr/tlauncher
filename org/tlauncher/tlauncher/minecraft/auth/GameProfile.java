// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

import org.apache.commons.lang3.StringUtils;

public class GameProfile
{
    public static final GameProfile DEFAULT_PROFILE;
    private final String id;
    private final String name;
    
    private GameProfile(final String id, final String name) {
        if (StringUtils.isBlank((CharSequence)id) && StringUtils.isBlank((CharSequence)name)) {
            throw new IllegalArgumentException("Name and ID cannot both be blank");
        }
        this.id = id;
        this.name = name;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public boolean isComplete() {
        return StringUtils.isNotBlank((CharSequence)this.getId()) && StringUtils.isNotBlank((CharSequence)this.getName());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final GameProfile that = (GameProfile)o;
        return this.id.equals(that.id) && this.name.equals(that.name);
    }
    
    @Override
    public int hashCode() {
        int result = this.id.hashCode();
        result = 31 * result + this.name.hashCode();
        return result;
    }
    
    @Override
    public String toString() {
        return "GameProfile{id='" + this.id + '\'' + ", name='" + this.name + '\'' + '}';
    }
    
    static {
        DEFAULT_PROFILE = new GameProfile("0", "(Default)");
    }
}
