// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

import java.util.Map;
import java.util.List;

public class User
{
    private String id;
    private List<Map<String, String>> properties;
    
    public String getID() {
        return this.id;
    }
    
    public List<Map<String, String>> getProperties() {
        return this.properties;
    }
}
