// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

import org.tlauncher.tlauncher.exceptions.auth.UserMigratedException;
import java.io.IOException;
import javax.net.ssl.SSLHandshakeException;
import org.tlauncher.tlauncher.entity.auth.Response;
import org.tlauncher.tlauncher.exceptions.auth.NotCorrectTokenOrIdException;
import org.apache.commons.lang3.StringUtils;
import org.tlauncher.tlauncher.entity.auth.RefreshResponse;
import org.tlauncher.tlauncher.entity.auth.RefreshRequest;
import org.tlauncher.tlauncher.exceptions.auth.InvalidCredentialsException;
import org.tlauncher.tlauncher.exceptions.auth.NotCorrectPasswordOrLogingException;
import org.tlauncher.tlauncher.entity.auth.Request;
import org.tlauncher.tlauncher.entity.auth.AuthenticationResponse;
import org.tlauncher.tlauncher.entity.auth.AuthenticationRequest;
import org.tlauncher.tlauncher.exceptions.auth.AuthenticatorException;
import net.minecraft.launcher.Http;
import java.lang.reflect.Type;
import java.util.UUID;
import com.google.gson.GsonBuilder;
import java.net.URL;
import com.google.gson.Gson;

public class StandardAuthenticator extends Authenticator
{
    private static int count;
    protected final Gson gson;
    private final URL AUTHENTICATE_URL;
    private final URL REFRESH_URL;
    
    StandardAuthenticator(final Account account, final String authUrl, final String refreshUrl) {
        super(account);
        this.gson = new GsonBuilder().registerTypeAdapter(UUID.class, new UUIDTypeAdapter()).create();
        this.AUTHENTICATE_URL = Http.constantURL(authUrl);
        this.REFRESH_URL = Http.constantURL(refreshUrl);
    }
    
    @Override
    protected void pass() throws AuthenticatorException {
        if (this.account.isFree()) {
            throw new IllegalArgumentException("invalid account type");
        }
        if (this.account.getPassword() == null && this.account.getAccessToken() == null) {
            throw new AuthenticatorException(new NullPointerException("password/accessToken"));
        }
        this.log("Staring to authenticate:", this.account);
        this.log("hasUsername:", this.account.getUsername());
        this.log("hasPassword:", this.account.getPassword() != null);
        this.log("hasAccessToken:", this.account.getAccessToken() != null);
        if (this.account.getPassword() == null) {
            this.tokenLogin();
        }
        else {
            this.passwordLogin();
        }
        this.log("Log in successful!");
        this.log("hasUUID:", this.account.getUUID() != null);
        this.log("hasAccessToken:", this.account.getAccessToken() != null);
        this.log("hasProfiles:", this.account.getProfiles() != null);
        this.log("hasProfile:", this.account.getProfiles() != null);
        this.log("hasProperties:", this.account.getProperties() != null);
    }
    
    private void passwordLogin() throws AuthenticatorException {
        this.log("Loggining in with password");
        final AuthenticationRequest request = new AuthenticationRequest(this);
        try {
            final AuthenticationResponse response = this.makeRequest(this.AUTHENTICATE_URL, request, AuthenticationResponse.class);
            this.account.setPassword(null);
            this.account.setUserID((response.getUserId() != null) ? response.getUserId() : this.account.getUsername());
            this.account.setAccessToken(response.getAccessToken());
            this.account.setProfiles(response.getAvailableProfiles());
            this.account.setProfile(response.getSelectedProfile());
            this.account.setUser(response.getUser());
            Authenticator.setClientToken(response.getClientToken());
            if (response.getSelectedProfile() != null) {
                this.account.setUUID(response.getSelectedProfile().getId());
                this.account.setDisplayName(response.getSelectedProfile().getName());
            }
            StandardAuthenticator.count = 0;
        }
        catch (InvalidCredentialsException e) {
            if (++StandardAuthenticator.count <= 1) {
                throw new NotCorrectPasswordOrLogingException();
            }
            if (this.account.getType().equals(Account.AccountType.TLAUNCHER)) {
                throw new AuthenticatorException("Invalid user or password", "restore.on.site.tlauncher");
            }
            throw new AuthenticatorException("Invalid user or password", "restore.on.site.mojang");
        }
    }
    
    private void tokenLogin() throws AuthenticatorException {
        this.log("Loggining in with token");
        final RefreshRequest request = new RefreshRequest(this);
        try {
            final RefreshResponse response = this.makeRequest(this.REFRESH_URL, request, RefreshResponse.class);
            Authenticator.setClientToken(response.getClientToken());
            this.account.setAccessToken(response.getAccessToken());
            if (StringUtils.isNotBlank((CharSequence)response.getAccessToken())) {
                if (this instanceof TlauncherAuthenticator) {
                    this.account.setType(Account.AccountType.TLAUNCHER);
                }
                else {
                    this.account.setType(Account.AccountType.MOJANG);
                }
            }
            else {
                this.account.setType(Account.AccountType.FREE);
            }
            this.account.setUser(response.getUser());
            this.account.setProfile(response.getSelectedProfile());
            this.account.setUser(response.getUser());
        }
        catch (InvalidCredentialsException e) {
            throw new NotCorrectTokenOrIdException();
        }
    }
    
    private <T extends Response> T makeRequest(final URL url, final Request input, final Class<T> classOfT) throws AuthenticatorException {
        if (url == null) {
            throw new NullPointerException("url");
        }
        T result;
        try {
            String jsonResult;
            if (input == null) {
                jsonResult = Http.performGet(url);
            }
            else {
                jsonResult = Http.performPost(url, this.gson.toJson(input), "application/json");
            }
            try {
                result = this.gson.fromJson(jsonResult, classOfT);
            }
            catch (RuntimeException rE) {
                throw new AuthenticatorException("Error parsing response: \"" + jsonResult + "\"", "unparseable", rE);
            }
        }
        catch (IOException e) {
            if (e instanceof SSLHandshakeException) {}
            if (e.getMessage().contains("Server returned HTTP response code: 403")) {
                throw new InvalidCredentialsException();
            }
            throw new AuthenticatorException("Error making request, uncaught IOException", "unreachable", e);
        }
        if (result == null) {
            return null;
        }
        if (StringUtils.isBlank((CharSequence)result.getError())) {
            return result;
        }
        throw this.getException(result);
    }
    
    protected AuthenticatorException getException(final Response result) {
        if ("UserMigratedException".equals(result.getCause())) {
            return new UserMigratedException();
        }
        if ("ForbiddenOperationException".equals(result.getError())) {
            return new InvalidCredentialsException();
        }
        return new AuthenticatorException(result, "internal");
    }
}
