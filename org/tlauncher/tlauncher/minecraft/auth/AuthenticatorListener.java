// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

public interface AuthenticatorListener
{
    void onAuthPassing(final Authenticator p0);
    
    void onAuthPassingError(final Authenticator p0, final Throwable p1);
    
    void onAuthPassed(final Authenticator p0);
}
