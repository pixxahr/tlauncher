// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.tlauncher.exceptions.auth.AuthenticatorException;
import java.util.UUID;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.util.U;

public abstract class Authenticator
{
    protected final Account account;
    private final String logPrefix;
    
    public final Account getAccount() {
        return this.account;
    }
    
    public boolean pass(final AuthenticatorListener l) {
        if (l != null) {
            l.onAuthPassing(this);
        }
        try {
            this.pass();
        }
        catch (Exception e) {
            this.log("Cannot authenticate:", e);
            if (l != null) {
                l.onAuthPassingError(this, e);
            }
            return false;
        }
        if (l != null) {
            l.onAuthPassed(this);
        }
        return true;
    }
    
    Authenticator(final Account account) {
        this.logPrefix = '[' + this.getClass().getSimpleName() + ']';
        if (account == null) {
            throw new NullPointerException("account");
        }
        this.account = account;
    }
    
    public static Authenticator instanceFor(final Account account) {
        if (account == null) {
            throw new NullPointerException("account");
        }
        U.log("instancefor:", account);
        switch (account.getType()) {
            case TLAUNCHER: {
                return new TlauncherAuthenticator(account);
            }
            case MOJANG: {
                return new MojangAuthenticator(account, TLauncher.getInnerSettings().get("authserver.mojang"));
            }
            default: {
                throw new IllegalArgumentException("illegal account type");
            }
        }
    }
    
    public static UUID getClientToken() {
        return TLauncher.getInstance().getProfileManager().getClientToken();
    }
    
    protected abstract void pass() throws AuthenticatorException;
    
    protected void log(final Object... o) {
        U.log(this.logPrefix, o);
    }
    
    public void asyncPass(final AuthenticatorListener l) {
        AsyncThread.execute(() -> this.pass(l));
    }
    
    protected static void setClientToken(final String uuid) {
        TLauncher.getInstance().getProfileManager().setClientToken(uuid);
    }
}
