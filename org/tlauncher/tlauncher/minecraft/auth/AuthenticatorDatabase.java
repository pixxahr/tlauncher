// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonArray;
import com.google.gson.JsonParseException;
import com.google.gson.JsonObject;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonDeserializer;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.lang3.StringUtils;
import java.util.Collections;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class AuthenticatorDatabase
{
    private final Map<String, Account> accounts;
    private AccountListener listener;
    
    public AuthenticatorDatabase(final Map<String, Account> map) {
        if (map == null) {
            throw new NullPointerException();
        }
        this.accounts = map;
    }
    
    public AuthenticatorDatabase() {
        this(new LinkedHashMap<String, Account>());
    }
    
    public Collection<Account> getAccounts() {
        return Collections.unmodifiableCollection((Collection<? extends Account>)this.accounts.values());
    }
    
    public Account getByUUID(final String uuid) {
        for (final Account account : this.accounts.values()) {
            if (StringUtils.equals((CharSequence)account.getUUID(), (CharSequence)uuid)) {
                return account;
            }
        }
        return null;
    }
    
    public Account getByUsername(final String username) {
        if (username == null) {
            throw new NullPointerException();
        }
        for (final Account acc : this.accounts.values()) {
            if (username.equals(acc.getUsername())) {
                return acc;
            }
        }
        return null;
    }
    
    public void registerAccount(final Account acc) {
        if (acc == null) {
            throw new NullPointerException();
        }
        if (this.accounts.containsValue(acc)) {
            throw new IllegalArgumentException("This account already exists!");
        }
        final String uuid = (acc.getUUID() == null) ? acc.getUsername() : acc.getUUID();
        this.accounts.put(uuid, acc);
        this.fireRefresh();
    }
    
    public void unregisterAccount(final Account acc) {
        if (acc == null) {
            throw new NullPointerException();
        }
        if (!this.accounts.containsValue(acc)) {
            throw new IllegalArgumentException("This account doesn't exist!");
        }
        this.accounts.values().remove(acc);
        this.fireRefresh();
    }
    
    private void fireRefresh() {
        if (this.listener == null) {
            return;
        }
        this.listener.onAccountsRefreshed(this);
    }
    
    public void setListener(final AccountListener listener) {
        this.listener = listener;
    }
    
    public void cleanFreeAccount() {
        final List<String> list = new ArrayList<String>();
        for (final Map.Entry<String, Account> account : this.accounts.entrySet()) {
            if (account.getValue().getType().equals(Account.AccountType.FREE)) {
                list.add(account.getKey());
            }
        }
        for (final String string : list) {
            this.accounts.remove(string);
        }
    }
    
    public Account getByUsernameType(final String username, final String type) {
        if (username == null || type == null) {
            throw new NullPointerException();
        }
        for (final Account acc : this.accounts.values()) {
            if (username.equals(acc.getUsername()) && type.equals(acc.getType().name())) {
                return acc;
            }
        }
        return null;
    }
    
    public static class Serializer implements JsonDeserializer<AuthenticatorDatabase>, JsonSerializer<AuthenticatorDatabase>
    {
        @Override
        public AuthenticatorDatabase deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            final Map<String, Account> services = new LinkedHashMap<String, Account>();
            final Map<String, Map<String, Object>> credentials = this.deserializeCredentials((JsonObject)json, context);
            for (final Map.Entry<String, Map<String, Object>> en : credentials.entrySet()) {
                services.put(en.getKey(), new Account(en.getValue()));
            }
            return new AuthenticatorDatabase(services);
        }
        
        Map<String, Map<String, Object>> deserializeCredentials(final JsonObject json, final JsonDeserializationContext context) {
            final Map<String, Map<String, Object>> result = new LinkedHashMap<String, Map<String, Object>>();
            for (final Map.Entry<String, JsonElement> authEntry : json.entrySet()) {
                final Map<String, Object> credentials = new LinkedHashMap<String, Object>();
                for (final Map.Entry<String, JsonElement> credentialsEntry : authEntry.getValue().entrySet()) {
                    credentials.put(credentialsEntry.getKey(), this.deserializeCredential(credentialsEntry.getValue()));
                }
                result.put(authEntry.getKey(), credentials);
            }
            return result;
        }
        
        private Object deserializeCredential(final JsonElement element) {
            if (element instanceof JsonObject) {
                final Map<String, Object> result = new LinkedHashMap<String, Object>();
                for (final Map.Entry<String, JsonElement> entry : ((JsonObject)element).entrySet()) {
                    result.put(entry.getKey(), this.deserializeCredential(entry.getValue()));
                }
                return result;
            }
            if (element instanceof JsonArray) {
                final List<Object> result2 = new ArrayList<Object>();
                for (final JsonElement entry2 : (JsonArray)element) {
                    result2.add(this.deserializeCredential(entry2));
                }
                return result2;
            }
            return element.getAsString();
        }
        
        @Override
        public JsonElement serialize(final AuthenticatorDatabase src, final Type typeOfSrc, final JsonSerializationContext context) {
            final Map<String, Account> services = src.accounts;
            final Map<String, Map<String, Object>> credentials = new LinkedHashMap<String, Map<String, Object>>();
            for (final Map.Entry<String, Account> en : services.entrySet()) {
                credentials.put(en.getKey(), en.getValue().createMap());
            }
            return context.serialize(credentials);
        }
    }
}
