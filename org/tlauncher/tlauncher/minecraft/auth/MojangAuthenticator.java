// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

class MojangAuthenticator extends StandardAuthenticator
{
    public MojangAuthenticator(final Account account, final String url) {
        super(account, url + "authenticate", url + "refresh");
    }
}
