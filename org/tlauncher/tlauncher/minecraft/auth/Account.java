// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

import java.util.Random;
import org.tlauncher.util.Reflect;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import com.google.gson.annotations.Expose;
import java.util.Map;
import java.util.List;

public class Account
{
    private String username;
    private String userID;
    private String displayName;
    private String password;
    private String accessToken;
    private String uuid;
    private List<Map<String, String>> userProperties;
    private AccountType type;
    @Expose
    private boolean premiumAccount;
    private GameProfile[] profiles;
    private GameProfile selectedProfile;
    private User user;
    
    public Account() {
        this.type = AccountType.FREE;
    }
    
    public Account(final String username) {
        this();
        this.setUsername(username);
    }
    
    public Account(final Map<String, Object> map) {
        this();
        this.fillFromMap(map);
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public boolean hasUsername() {
        return this.username != null;
    }
    
    public void setUsername(final String username) {
        this.username = username;
    }
    
    public String getUserID() {
        return this.userID;
    }
    
    public void setUserID(final String userID) {
        this.userID = userID;
    }
    
    public String getDisplayName() {
        return (this.displayName == null) ? this.username : this.displayName;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    public String getAccessToken() {
        return this.accessToken;
    }
    
    public void setAccessToken(String accessToken) {
        if ("null".equals(accessToken)) {
            accessToken = null;
        }
        this.accessToken = accessToken;
    }
    
    public String getUUID() {
        return this.uuid;
    }
    
    public void setUUID(final String uuid) {
        this.uuid = uuid;
    }
    
    public GameProfile[] getProfiles() {
        return this.profiles;
    }
    
    public void setProfiles(final GameProfile[] p) {
        this.profiles = p;
    }
    
    public GameProfile getProfile() {
        return (this.selectedProfile != null) ? this.selectedProfile : GameProfile.DEFAULT_PROFILE;
    }
    
    public void setProfile(final GameProfile p) {
        this.selectedProfile = p;
    }
    
    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }
    
    public User getUser() {
        return this.user;
    }
    
    public void setUser(final User user) {
        this.user = user;
    }
    
    public Map<String, List<String>> getProperties() {
        final Map<String, List<String>> map = new HashMap<String, List<String>>();
        final List<UserProperty> list = new ArrayList<UserProperty>();
        if (this.userProperties != null) {
            for (final Map<String, String> properties : this.userProperties) {
                list.add(new UserProperty(properties.get("name"), properties.get("value")));
            }
        }
        if (this.user != null && this.user.getProperties() != null) {
            for (final Map<String, String> properties : this.user.getProperties()) {
                list.add(new UserProperty(properties.get("name"), properties.get("value")));
            }
        }
        for (final UserProperty property : list) {
            final List<String> values = new ArrayList<String>();
            values.add(property.getValue());
            map.put(property.getKey(), values);
        }
        return map;
    }
    
    void setProperties(final List<Map<String, String>> properties) {
        this.userProperties = properties;
    }
    
    public AccountType getType() {
        return this.type;
    }
    
    public void setType(final AccountType type) {
        if (type == null) {
            throw new NullPointerException();
        }
        this.type = type;
    }
    
    public boolean isFree() {
        return this.type.equals(AccountType.FREE);
    }
    
    Map<String, Object> createMap() {
        final Map<String, Object> r = new HashMap<String, Object>();
        r.put("username", this.username);
        r.put("userid", this.userID);
        r.put("uuid", UUIDTypeAdapter.toUUID(this.uuid));
        r.put("displayName", this.displayName);
        if (!this.isFree()) {
            r.put("type", this.type);
            r.put("accessToken", this.accessToken);
        }
        if (this.userProperties != null) {
            r.put("userProperties", this.userProperties);
        }
        return r;
    }
    
    void fillFromMap(final Map<String, Object> map) {
        if (map.containsKey("username")) {
            this.setUsername(map.get("username").toString());
        }
        this.setUserID(map.containsKey("userid") ? map.get("userid").toString() : this.getUsername());
        this.setDisplayName(map.containsKey("displayName") ? map.get("displayName").toString() : this.getUsername());
        this.setProperties(map.containsKey("userProperties") ? map.get("userProperties") : null);
        this.setUUID(map.containsKey("uuid") ? UUIDTypeAdapter.toUUID(map.get("uuid").toString()) : null);
        final boolean hasAccessToken = map.containsKey("accessToken");
        if (hasAccessToken) {
            this.setAccessToken(map.get("accessToken").toString());
        }
        this.setType(map.containsKey("type") ? Reflect.parseEnum(AccountType.class, map.get("type").toString()) : (hasAccessToken ? AccountType.MOJANG : AccountType.FREE));
    }
    
    public void complete(final Account acc) {
        if (acc == null) {
            throw new NullPointerException();
        }
        final boolean sameName = acc.username.equals(this.username);
        this.username = acc.username;
        this.type = acc.type;
        if (acc.userID != null) {
            this.userID = acc.userID;
        }
        if (acc.displayName != null) {
            this.displayName = acc.displayName;
        }
        if (acc.password != null) {
            this.password = acc.password;
        }
        if (!sameName) {
            this.accessToken = null;
        }
    }
    
    public boolean equals(final Account acc) {
        if (acc == null) {
            return false;
        }
        if (this.username == null) {
            return acc.username == null;
        }
        return this.username.equals(acc.username) && this.type.equals(acc.type) && (this.password == null || this.password.equals(acc.password));
    }
    
    @Override
    public String toString() {
        return this.toDebugString();
    }
    
    public String toDebugString() {
        final Map<String, Object> map = this.createMap();
        if (map.containsKey("accessToken")) {
            map.remove("accessToken");
            map.put("accessToken", "(not null)");
        }
        return "Account" + map;
    }
    
    public static Account randomAccount() {
        return new Account("random" + new Random().nextLong());
    }
    
    public boolean isPremiumAccount() {
        return this.premiumAccount;
    }
    
    public void setPremiumAccount(final boolean premiumAccount) {
        this.premiumAccount = premiumAccount;
    }
    
    public enum AccountType
    {
        TLAUNCHER, 
        MOJANG, 
        FREE, 
        SPECIAL;
    }
}
