// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

import org.tlauncher.tlauncher.exceptions.auth.ServiceUnavailableException;
import org.tlauncher.tlauncher.exceptions.auth.AuthenticatorException;
import org.tlauncher.tlauncher.entity.auth.Response;
import org.tlauncher.tlauncher.rmo.TLauncher;

public class TlauncherAuthenticator extends StandardAuthenticator
{
    public TlauncherAuthenticator(final Account account) {
        super(account, TLauncher.getInnerSettings().get("skin.authentication.authenticate"), TLauncher.getInnerSettings().get("skin.authentication.refresh"));
    }
    
    @Override
    protected AuthenticatorException getException(final Response result) {
        final AuthenticatorException exception = super.getException(result);
        if (exception.getClass().equals(AuthenticatorException.class) && "ServiceUnavailableException".equals(result.getError())) {
            return new ServiceUnavailableException(result.getErrorMessage());
        }
        return exception;
    }
}
