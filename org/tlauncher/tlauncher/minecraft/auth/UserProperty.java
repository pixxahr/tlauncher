// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.minecraft.auth;

public class UserProperty
{
    private String name;
    private String value;
    
    public UserProperty(final String name, final String value) {
        this.name = name;
        this.value = value;
    }
    
    public String getKey() {
        return this.name;
    }
    
    public String getValue() {
        return this.value;
    }
    
    @Override
    public String toString() {
        return "Property{" + this.name + " = " + this.value + "}";
    }
}
