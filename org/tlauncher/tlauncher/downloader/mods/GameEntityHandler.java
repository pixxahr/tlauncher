// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader.mods;

import org.tlauncher.tlauncher.downloader.RetryDownloadException;
import org.tlauncher.util.FileUtil;
import org.tlauncher.tlauncher.downloader.Downloadable;
import org.tlauncher.tlauncher.downloader.DownloadableContainer;
import org.tlauncher.tlauncher.downloader.DownloadableContainerHandler;

public class GameEntityHandler implements DownloadableContainerHandler
{
    @Override
    public void onStart(final DownloadableContainer c) {
    }
    
    @Override
    public void onAbort(final DownloadableContainer c) {
    }
    
    @Override
    public void onError(final DownloadableContainer c, final Downloadable d, final Throwable e) {
    }
    
    @Override
    public void onComplete(final DownloadableContainer c, final Downloadable d) throws RetryDownloadException {
        if (!(d instanceof GameEntityDownloader)) {
            return;
        }
        final GameEntityDownloader g = (GameEntityDownloader)d;
        final String fileHash = FileUtil.getChecksum(d.getDestination(), "sha1");
        if (fileHash == null || fileHash.equals(g.getMetadata().getSha1())) {
            return;
        }
        throw new RetryDownloadException("illegal library hash. got: " + fileHash + "; expected: " + g.getMetadata().getSha1());
    }
    
    @Override
    public void onFullComplete(final DownloadableContainer c) {
    }
}
