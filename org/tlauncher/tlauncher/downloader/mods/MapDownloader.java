// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader.mods;

import org.tlauncher.tlauncher.downloader.RetryDownloadException;
import org.tlauncher.util.FileUtil;
import java.io.File;
import org.apache.commons.io.FilenameUtils;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import java.nio.file.Path;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;

public class MapDownloader extends GameEntityDownloader
{
    public MapDownloader(final boolean forceDownload, final MetadataDTO metadata, final Path folder) {
        super(ClientInstanceRepo.createModpackRepo(), forceDownload, metadata.getUrl(), new File(folder.toFile(), "saves/" + FilenameUtils.getName(metadata.getPath())));
        this.setMetadata(metadata);
    }
    
    @Override
    protected void onComplete() throws RetryDownloadException {
        super.onComplete();
        try {
            final File f = this.getDestination();
            FileUtil.unzipUniversal(f, f.getParentFile());
            FileUtil.deleteFile(f);
        }
        catch (Throwable t) {
            throw new RetryDownloadException("cannot unpack archive", t);
        }
    }
}
