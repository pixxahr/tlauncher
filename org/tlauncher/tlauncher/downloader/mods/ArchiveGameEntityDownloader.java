// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader.mods;

import java.nio.file.Paths;
import java.nio.file.Path;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import org.tlauncher.tlauncher.repository.Repo;

public class ArchiveGameEntityDownloader extends GameEntityDownloader
{
    public ArchiveGameEntityDownloader(final Repo repo, final boolean forceDownload, final MetadataDTO metadata, final Path folder) {
        super(repo, forceDownload, metadata.getUrl(), Paths.get(folder.toString(), metadata.getPath()).toFile());
        this.setMetadata(metadata);
    }
}
