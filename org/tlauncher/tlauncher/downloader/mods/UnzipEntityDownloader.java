// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader.mods;

import java.io.File;
import org.tlauncher.tlauncher.downloader.RetryDownloadException;
import org.tlauncher.util.FileUtil;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import java.nio.file.Path;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;

public class UnzipEntityDownloader extends ArchiveGameEntityDownloader
{
    public UnzipEntityDownloader(final boolean forceDownload, final MetadataDTO metadata, final Path folder) {
        super(ClientInstanceRepo.createModpackRepo(), forceDownload, metadata, folder);
    }
    
    @Override
    protected void onComplete() throws RetryDownloadException {
        super.onComplete();
        try {
            final File f = this.getDestination();
            FileUtil.unzipUniversal(f, f.getParentFile());
        }
        catch (Throwable t) {
            throw new RetryDownloadException("cannot unpack archive", t);
        }
    }
}
