// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader.mods;

import java.io.File;
import org.tlauncher.tlauncher.repository.Repo;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import org.tlauncher.tlauncher.downloader.Downloadable;

public abstract class GameEntityDownloader extends Downloadable
{
    private MetadataDTO metadata;
    
    public GameEntityDownloader(final Repo repo, final boolean forceDownload, final String url, final File file) {
        super(repo, url, file, forceDownload);
    }
    
    public MetadataDTO getMetadata() {
        return this.metadata;
    }
    
    public void setMetadata(final MetadataDTO meta) {
        this.metadata = meta;
    }
}
