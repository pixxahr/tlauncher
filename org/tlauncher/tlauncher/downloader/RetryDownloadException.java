// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

import java.io.IOException;

public class RetryDownloadException extends IOException
{
    private static final long serialVersionUID = 2968569164701826930L;
    
    public RetryDownloadException(final String message) {
        super(message);
    }
    
    public RetryDownloadException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
