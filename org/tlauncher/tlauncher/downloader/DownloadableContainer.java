// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.tlauncher.tlauncher.ui.console.Console;
import java.util.List;

public class DownloadableContainer
{
    private final List<DownloadableContainerHandler> handlers;
    private final List<Throwable> errors;
    final List<Downloadable> list;
    private Console console;
    private final AtomicInteger sum;
    private boolean locked;
    private boolean aborted;
    
    public DownloadableContainer() {
        this.list = Collections.synchronizedList(new ArrayList<Downloadable>());
        this.handlers = Collections.synchronizedList(new ArrayList<DownloadableContainerHandler>());
        this.errors = Collections.synchronizedList(new ArrayList<Throwable>());
        this.sum = new AtomicInteger();
    }
    
    public List<Downloadable> getList() {
        return Collections.unmodifiableList((List<? extends Downloadable>)this.list);
    }
    
    public void add(final Downloadable d) {
        if (d == null) {
            throw new NullPointerException();
        }
        this.checkLocked();
        if (this.list.contains(d)) {
            return;
        }
        this.list.add(d);
        d.setContainer(this);
        this.sum.incrementAndGet();
    }
    
    public void addAll(final Downloadable... ds) {
        if (ds == null) {
            throw new NullPointerException();
        }
        for (int i = 0; i < ds.length; ++i) {
            if (ds[i] == null) {
                throw new NullPointerException("Downloadable at " + i + " is NULL!");
            }
            if (!this.list.contains(ds[i])) {
                this.list.add(ds[i]);
                ds[i].setContainer(this);
                this.sum.incrementAndGet();
            }
        }
    }
    
    public void addAll(final Collection<Downloadable> coll) {
        if (coll == null) {
            throw new NullPointerException();
        }
        int i = -1;
        for (final Downloadable d : coll) {
            ++i;
            if (d == null) {
                throw new NullPointerException("Downloadable at" + i + " is NULL!");
            }
            this.list.add(d);
            d.setContainer(this);
            this.sum.incrementAndGet();
        }
    }
    
    public void addHandler(final DownloadableContainerHandler handler) {
        if (handler == null) {
            throw new NullPointerException();
        }
        this.checkLocked();
        this.handlers.add(handler);
    }
    
    public List<Throwable> getErrors() {
        return Collections.unmodifiableList((List<? extends Throwable>)this.errors);
    }
    
    public Console getConsole() {
        return this.console;
    }
    
    public boolean hasConsole() {
        return this.console != null;
    }
    
    public void setConsole(final Console console) {
        this.checkLocked();
        this.console = console;
    }
    
    public boolean isAborted() {
        return this.aborted;
    }
    
    void setLocked(final boolean locked) {
        this.locked = locked;
    }
    
    void checkLocked() {
        if (this.locked) {
            throw new IllegalStateException("Downloadable is locked!");
        }
    }
    
    void onStart() {
        for (final DownloadableContainerHandler handler : this.handlers) {
            handler.onStart(this);
        }
    }
    
    void onComplete(final Downloadable d) throws RetryDownloadException {
        for (final DownloadableContainerHandler handler : this.handlers) {
            handler.onComplete(this, d);
        }
        if (this.sum.decrementAndGet() > 0) {
            return;
        }
        for (final DownloadableContainerHandler handler : this.handlers) {
            handler.onFullComplete(this);
        }
    }
    
    void onAbort(final Downloadable d) {
        this.aborted = true;
        this.errors.add(d.getError());
        if (this.sum.decrementAndGet() > 0) {
            return;
        }
        for (final DownloadableContainerHandler handler : this.handlers) {
            handler.onAbort(this);
        }
    }
    
    void onError(final Downloadable d, final Throwable e) {
        this.errors.add(e);
        for (final DownloadableContainerHandler handler : this.handlers) {
            handler.onError(this, d, e);
        }
    }
    
    void log(final Object... o) {
        if (this.console == null) {
            return;
        }
        this.console.log(o);
    }
    
    public static void removeDuplicates(final DownloadableContainer a, final DownloadableContainer b) {
        if (a.locked) {
            throw new IllegalStateException("First conatiner is already locked!");
        }
        if (b.locked) {
            throw new IllegalStateException("Second container is already locked!");
        }
        a.locked = true;
        b.locked = true;
        try {
            final List<Downloadable> aList = a.list;
            final List<Downloadable> bList = b.list;
            final List<Downloadable> deleteList = new ArrayList<Downloadable>();
            for (final Downloadable aDownloadable : aList) {
                for (final Downloadable bDownloadable : bList) {
                    if (aDownloadable.equals(bDownloadable)) {
                        deleteList.add(bDownloadable);
                    }
                }
            }
            bList.removeAll(deleteList);
        }
        finally {
            a.locked = false;
            b.locked = false;
        }
    }
    
    public static void removeDuplicates(final List<? extends DownloadableContainer> list) {
        if (list == null) {
            throw new NullPointerException();
        }
        if (list.size() < 2) {
            return;
        }
        for (int i = 0; i < list.size() - 1; ++i) {
            for (int k = i + 1; k < list.size(); ++k) {
                removeDuplicates((DownloadableContainer)list.get(i), (DownloadableContainer)list.get(k));
            }
        }
    }
}
