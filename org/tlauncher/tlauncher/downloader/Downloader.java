// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

import org.tlauncher.util.U;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import java.util.List;
import org.tlauncher.util.async.ExtendedThread;

public class Downloader extends ExtendedThread
{
    public static final int MAX_THREADS = 3;
    public static final String DOWNLOAD_BLOCK = "download";
    static final String ITERATION_BLOCK = "iteration";
    private final DownloaderThread[] threads;
    private final List<Downloadable> list;
    private final List<DownloaderListener> listeners;
    private ConnectionQuality configuration;
    private final AtomicInteger remainingObjects;
    private int runningThreads;
    private int workingThreads;
    private final double[] speedContainer;
    private final double[] progressContainer;
    private double lastAverageProgress;
    private double averageProgress;
    private double averageSpeed;
    private boolean aborted;
    private final Object workLock;
    private boolean haveWork;
    
    public Downloader(final ConnectionQuality configuration) {
        super("MD");
        this.remainingObjects = new AtomicInteger();
        this.setConfiguration(configuration);
        this.threads = new DownloaderThread[3];
        this.list = Collections.synchronizedList(new ArrayList<Downloadable>());
        this.listeners = Collections.synchronizedList(new ArrayList<DownloaderListener>());
        this.speedContainer = new double[3];
        this.progressContainer = new double[3];
        this.workLock = new Object();
        this.startAndWait();
    }
    
    public ConnectionQuality getConfiguration() {
        return this.configuration;
    }
    
    public int getRemaining() {
        return this.remainingObjects.get();
    }
    
    public double getProgress() {
        return this.averageProgress;
    }
    
    public double getSpeed() {
        return this.averageSpeed;
    }
    
    public void add(final Downloadable d) {
        if (d == null) {
            throw new NullPointerException();
        }
        this.list.add(d);
    }
    
    public void add(final DownloadableContainer c) {
        if (c == null) {
            throw new NullPointerException();
        }
        this.list.addAll(c.list);
    }
    
    public void addAll(final Downloadable... ds) {
        if (ds == null) {
            throw new NullPointerException();
        }
        for (int i = 0; i < ds.length; ++i) {
            if (ds[i] == null) {
                throw new NullPointerException("Downloadable at " + i + " is NULL!");
            }
            this.list.add(ds[i]);
        }
    }
    
    public void addAll(final Collection<Downloadable> coll) {
        if (coll == null) {
            throw new NullPointerException();
        }
        int i = -1;
        for (final Downloadable d : coll) {
            ++i;
            if (d == null) {
                throw new NullPointerException("Downloadable at" + i + " is NULL!");
            }
            this.list.add(d);
        }
    }
    
    public void addListener(final DownloaderListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        this.listeners.add(listener);
    }
    
    public boolean startDownload() {
        final boolean haveWork = !this.list.isEmpty();
        if (haveWork) {
            this.unlockThread("iteration");
        }
        return haveWork;
    }
    
    public void startDownloadAndWait() {
        if (this.startDownload()) {
            this.waitWork();
        }
    }
    
    private void waitWork() {
        this.haveWork = true;
        while (this.haveWork) {
            synchronized (this.workLock) {
                try {
                    this.workLock.wait();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private void notifyWork() {
        this.haveWork = false;
        synchronized (this.workLock) {
            this.workLock.notifyAll();
        }
    }
    
    public void stopDownload() {
        if (!this.isThreadLocked()) {
            try {
                Thread.sleep(2000L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (!this.isThreadLocked()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < this.runningThreads; ++i) {
            this.threads[i].stopDownload();
        }
        this.aborted = true;
        if (this.isThreadLocked()) {
            this.tryUnlock("download");
        }
    }
    
    public void stopDownloadAndWait() {
        this.stopDownload();
        this.waitForThreads();
    }
    
    public void setConfiguration(final ConnectionQuality configuration) {
        if (configuration == null) {
            throw new NullPointerException();
        }
        log("Loaded configuration:", configuration);
        this.configuration = configuration;
    }
    
    @Override
    public void run() {
        this.checkCurrent();
        while (true) {
            this.lockThread("iteration");
            log("Files in queue", this.list.size());
            synchronized (this.list) {
                this.sortOut();
            }
            for (int i = 0; i < this.runningThreads; ++i) {
                this.threads[i].startDownload();
            }
            this.lockThread("download");
            if (this.aborted) {
                this.waitForThreads();
                this.onAbort();
                this.aborted = false;
            }
            this.notifyWork();
            Arrays.fill(this.speedContainer, 0.0);
            Arrays.fill(this.progressContainer, 0.0);
            this.averageProgress = 0.0;
            this.lastAverageProgress = 0.0;
            this.workingThreads = 0;
            this.remainingObjects.set(0);
        }
    }
    
    private void sortOut() {
        int size = this.list.size();
        if (size == 0) {
            return;
        }
        int downloadablesAtThread = U.getMaxMultiply(size, 3);
        int x = 0;
        log("Starting download " + size + " files...");
        this.onStart(size);
        final int max = this.configuration.getMaxThreads();
        final boolean[] workers = new boolean[max];
        while (size > 0) {
            for (int i = 0; i < max; ++i) {
                workers[i] = true;
                size -= downloadablesAtThread;
                if (this.threads[i] == null) {
                    this.threads[i] = new DownloaderThread(this, ++this.runningThreads);
                }
                int y;
                for (y = x; y < x + downloadablesAtThread; ++y) {
                    this.threads[i].add(this.list.get(y));
                }
                x = y;
                if (size == 0) {
                    break;
                }
            }
            downloadablesAtThread = U.getMaxMultiply(size, 3);
        }
        for (final boolean worker : workers) {
            if (worker) {
                ++this.workingThreads;
            }
        }
        this.list.clear();
    }
    
    private void onStart(final int size) {
        for (final DownloaderListener listener : this.listeners) {
            listener.onDownloaderStart(this, size);
        }
        this.remainingObjects.addAndGet(size);
    }
    
    private void onAbort() {
        for (final DownloaderListener listener : this.listeners) {
            listener.onDownloaderAbort(this);
        }
    }
    
    void onProgress(final DownloaderThread thread, final double curprogress, final double curspeed) {
        final int id = thread.getID() - 1;
        this.progressContainer[id] = curprogress;
        this.speedContainer[id] = curspeed;
        this.averageProgress = U.getAverage(this.progressContainer, this.workingThreads);
        if (this.averageProgress - this.lastAverageProgress < 0.01) {
            return;
        }
        this.lastAverageProgress = this.averageProgress;
        this.averageSpeed = U.getSum(this.speedContainer);
        for (final DownloaderListener listener : this.listeners) {
            listener.onDownloaderProgress(this, this.averageProgress, this.averageSpeed);
        }
    }
    
    void onFileComplete(final DownloaderThread thread, final Downloadable file) {
        final int remaining = this.remainingObjects.decrementAndGet();
        for (final DownloaderListener listener : this.listeners) {
            listener.onDownloaderFileComplete(this, file);
        }
        if (remaining < 1) {
            this.onComplete();
        }
    }
    
    private void onComplete() {
        for (final DownloaderListener listener : this.listeners) {
            listener.onDownloaderComplete(this);
        }
        this.unlockThread("download");
    }
    
    private void waitForThreads() {
        log("Waiting for", this.workingThreads, "threads...");
        boolean blocked;
        do {
            blocked = true;
            for (int i = 0; i < this.workingThreads; ++i) {
                if (!this.threads[i].isThreadLocked()) {
                    blocked = false;
                }
            }
        } while (!blocked);
        log("All threads are blocked by now");
    }
    
    private static void log(final Object... o) {
        U.log("[Downloader2]", o);
    }
}
