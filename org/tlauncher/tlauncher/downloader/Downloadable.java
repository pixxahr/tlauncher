// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

import org.tlauncher.util.OS;
import org.tlauncher.util.Reflect;
import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.Iterator;
import org.tlauncher.util.FileUtil;
import org.tlauncher.util.U;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import org.tlauncher.tlauncher.repository.Repo;

public class Downloadable
{
    private static final boolean DEFAULT_FORCE = false;
    private static final boolean DEFAULT_FAST = false;
    private String path;
    private Repo repo;
    private File destination;
    private final List<File> additionalDestinations;
    private boolean forceDownload;
    private boolean fastDownload;
    private boolean insertUseragent;
    private boolean locked;
    private DownloadableContainer container;
    private final List<DownloadableHandler> handlers;
    private Throwable error;
    
    private Downloadable() {
        this.additionalDestinations = Collections.synchronizedList(new ArrayList<File>());
        this.handlers = Collections.synchronizedList(new ArrayList<DownloadableHandler>());
    }
    
    public Downloadable(final Repo repo, final String path, final File destination, final boolean forceDownload, final boolean fastDownload) {
        this();
        this.setURL(repo, path);
        this.setDestination(destination);
        this.forceDownload = forceDownload;
        this.fastDownload = fastDownload;
    }
    
    public Downloadable(final Repo repo, final String path, final File destination, final boolean forceDownload) {
        this(repo, path, destination, forceDownload, false);
    }
    
    public Downloadable(final Repo repo, final String path, final File destination) {
        this(repo, path, destination, false, false);
    }
    
    private Downloadable(final String url, final File destination, final boolean forceDownload, final boolean fastDownload) {
        this();
        this.setURL(url);
        this.setDestination(destination);
        this.forceDownload = forceDownload;
        this.fastDownload = fastDownload;
    }
    
    public Downloadable(final String url, final File destination) {
        this(url, destination, false, false);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        if (!(o instanceof Downloadable)) {
            return false;
        }
        final Downloadable c = (Downloadable)o;
        return U.equal(this.path, c.path) && U.equal(this.repo, c.repo) && U.equal(this.destination, c.destination) && U.equal(this.additionalDestinations, c.additionalDestinations);
    }
    
    public boolean getInsertUA() {
        return this.insertUseragent;
    }
    
    public void setInsertUA(final boolean ua) {
        this.checkLocked();
        this.insertUseragent = ua;
    }
    
    public boolean isForce() {
        return this.forceDownload;
    }
    
    public void setForce(final boolean force) {
        this.checkLocked();
        this.forceDownload = force;
    }
    
    public boolean isFast() {
        return this.fastDownload;
    }
    
    public void setFast(final boolean fast) {
        this.checkLocked();
        this.fastDownload = fast;
    }
    
    public String getURL() {
        return this.path;
    }
    
    public Repo getRepository() {
        return this.repo;
    }
    
    public boolean hasRepository() {
        return this.repo != null;
    }
    
    protected void setURL(final Repo repo, final String path) {
        if (repo == null) {
            throw new NullPointerException("Repository is NULL!");
        }
        if (path == null) {
            throw new NullPointerException("Path is NULL!");
        }
        this.checkLocked();
        this.repo = repo;
        this.path = path;
    }
    
    protected void setURL(final String url) {
        if (url == null) {
            throw new NullPointerException();
        }
        if (url.isEmpty()) {
            throw new IllegalArgumentException("URL cannot be empty!");
        }
        this.checkLocked();
        this.repo = null;
        this.path = url;
    }
    
    public File getDestination() {
        return this.destination;
    }
    
    public String getFilename() {
        return FileUtil.getFilename(this.path);
    }
    
    protected void setDestination(final File file) {
        if (file == null) {
            throw new NullPointerException();
        }
        this.checkLocked();
        this.destination = file;
    }
    
    public List<File> getAdditionalDestinations() {
        return Collections.unmodifiableList((List<? extends File>)this.additionalDestinations);
    }
    
    public void addAdditionalDestination(final File file) {
        if (file == null) {
            throw new NullPointerException();
        }
        this.checkLocked();
        this.additionalDestinations.add(file);
    }
    
    public DownloadableContainer getContainer() {
        return this.container;
    }
    
    public boolean hasContainer() {
        return this.container != null;
    }
    
    public boolean hasConsole() {
        return this.container != null && this.container.hasConsole();
    }
    
    public void addHandler(final DownloadableHandler handler) {
        if (handler == null) {
            throw new NullPointerException();
        }
        this.checkLocked();
        this.handlers.add(handler);
    }
    
    protected void setContainer(final DownloadableContainer container) {
        this.checkLocked();
        this.container = container;
    }
    
    public Throwable getError() {
        return this.error;
    }
    
    private void setLocked(final boolean locked) {
        this.locked = locked;
    }
    
    protected void checkLocked() {
        if (this.locked) {
            throw new IllegalStateException("Downloadable is locked!");
        }
    }
    
    protected void onStart() {
        this.setLocked(true);
        for (final DownloadableHandler handler : this.handlers) {
            handler.onStart(this);
        }
    }
    
    protected void onAbort(final AbortedDownloadException ae) {
        this.setLocked(false);
        this.error = ae;
        for (final DownloadableHandler handler : this.handlers) {
            handler.onAbort(this);
        }
        if (this.container != null) {
            this.container.onAbort(this);
        }
    }
    
    protected void onComplete() throws RetryDownloadException {
        this.setLocked(false);
        for (final DownloadableHandler handler : this.handlers) {
            handler.onComplete(this);
        }
        if (this.container != null) {
            this.container.onComplete(this);
        }
    }
    
    protected void onError(final Throwable e) {
        this.error = e;
        if (e == null) {
            return;
        }
        this.setLocked(false);
        for (final DownloadableHandler handler : this.handlers) {
            handler.onError(this, e);
        }
        if (this.container != null) {
            this.container.onError(this, e);
        }
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{path='" + this.path + "'; repo=" + this.repo + "; destinations=" + this.destination + "," + this.additionalDestinations + "; force=" + this.forceDownload + "; fast=" + this.fastDownload + "; locked=" + this.locked + "; container=" + this.container + "; handlers=" + this.handlers + "; error=" + this.error + ";}";
    }
    
    public static HttpURLConnection setUp(final URLConnection connection0, final int timeout, final boolean fake) {
        if (connection0 == null) {
            throw new NullPointerException();
        }
        if (!(connection0 instanceof HttpURLConnection)) {
            throw new IllegalArgumentException("Unknown connection protocol: " + connection0);
        }
        final HttpURLConnection connection = (HttpURLConnection)connection0;
        connection.setConnectTimeout(timeout);
        connection.setReadTimeout(timeout);
        connection.setUseCaches(false);
        connection.setDefaultUseCaches(false);
        connection.setRequestProperty("Cache-Control", "no-store,max-age=0,no-cache");
        connection.setRequestProperty("Expires", "0");
        connection.setRequestProperty("Pragma", "no-cache");
        final HttpsURLConnection securedConnection = Reflect.cast(connection, HttpsURLConnection.class);
        if (!fake) {
            return connection;
        }
        String userAgent = null;
        switch (OS.CURRENT) {
            case OSX: {
                userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8) AppleWebKit/535.18.5 (KHTML, like Gecko) Version/5.2 Safari/535.18.5";
                break;
            }
            case WINDOWS: {
                userAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0; .NET4.0C)";
                break;
            }
            default: {
                userAgent = "Mozilla/5.0 (Linux; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0";
                break;
            }
        }
        connection.setRequestProperty("User-Agent", userAgent);
        return connection;
    }
    
    public static HttpURLConnection setUp(final URLConnection connection, final int timeout) {
        return setUp(connection, timeout, false);
    }
    
    public static HttpURLConnection setUp(final URLConnection connection, final boolean fake) {
        return setUp(connection, U.getConnectionTimeout(), fake);
    }
    
    public static HttpURLConnection setUp(final URLConnection connection) {
        return setUp(connection, false);
    }
    
    public static String getEtag(final String etag) {
        if (etag == null) {
            return "-";
        }
        if (etag.startsWith("\"") && etag.endsWith("\"")) {
            return etag.substring(1, etag.length() - 1);
        }
        return etag;
    }
}
