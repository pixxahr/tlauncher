// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

public interface DownloaderListener
{
    void onDownloaderStart(final Downloader p0, final int p1);
    
    void onDownloaderAbort(final Downloader p0);
    
    void onDownloaderProgress(final Downloader p0, final double p1, final double p2);
    
    void onDownloaderFileComplete(final Downloader p0, final Downloadable p1);
    
    void onDownloaderComplete(final Downloader p0);
}
