// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import org.tlauncher.util.U;
import java.net.URL;
import org.tlauncher.tlauncher.repository.Repo;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.tlauncher.tlauncher.repository.ClientInstanceRepo;
import java.util.Iterator;
import java.util.Arrays;
import java.io.File;
import org.tlauncher.util.FileUtil;
import java.util.ArrayList;
import java.util.List;
import org.tlauncher.util.async.ExtendedThread;

public class DownloaderThread extends ExtendedThread
{
    private static final String ITERATION_BLOCK = "iteration";
    private static final int CONTAINER_SIZE = 100;
    private static final int NOTIFY_TIMER = 15000;
    private final int ID;
    private final String LOGGER_PREFIX;
    private final Downloader downloader;
    private final List<Downloadable> list;
    private final double[] averageSpeedContainer;
    private int speedCaret;
    private double currentProgress;
    private double lastProgress;
    private double doneProgress;
    private double eachProgress;
    private double speed;
    private Downloadable current;
    private boolean launched;
    
    DownloaderThread(final Downloader d, final int id) {
        super("DT#" + id);
        this.ID = id;
        this.LOGGER_PREFIX = "[D#" + id + "]";
        this.downloader = d;
        this.list = new ArrayList<Downloadable>();
        this.averageSpeedContainer = new double[100];
        this.startAndWait();
    }
    
    int getID() {
        return this.ID;
    }
    
    void add(final Downloadable d) {
        this.list.add(d);
    }
    
    void startDownload() {
        this.launched = true;
        this.unlockThread("iteration");
    }
    
    void stopDownload() {
        this.launched = false;
    }
    
    @Override
    public void run() {
        while (true) {
            this.launched = true;
            this.eachProgress = 1.0 / this.list.size();
            final double n = 0.0;
            this.doneProgress = n;
            this.currentProgress = n;
            for (final Downloadable d : this.list) {
                this.current = d;
                this.onStart();
                int attempt = 0;
                Throwable error = null;
                int max;
                while (attempt < (max = this.downloader.getConfiguration().getTries(d.isFast()))) {
                    final int timeout = ++attempt * this.downloader.getConfiguration().getTimeout();
                    try {
                        this.download(timeout);
                    }
                    catch (GaveUpDownloadException e) {
                        this.dlog("File is not reachable at all.");
                        error = e;
                        if (attempt < max) {
                            continue;
                        }
                        FileUtil.deleteFile(d.getDestination());
                        for (final File file : d.getAdditionalDestinations()) {
                            FileUtil.deleteFile(file);
                        }
                        this.dlog("Gave up trying to download this file.", error);
                        this.onError(error);
                        continue;
                    }
                    catch (AbortedDownloadException ex) {}
                    break;
                }
                if (error instanceof AbortedDownloadException) {
                    this.tlog("Thread is aborting...");
                    for (final Downloadable downloadable : this.list) {
                        downloadable.onAbort((AbortedDownloadException)error);
                    }
                    break;
                }
            }
            Arrays.fill(this.averageSpeedContainer, 0.0);
            this.list.clear();
            this.lockThread("iteration");
            this.launched = false;
        }
    }
    
    private void download(final int timeout) throws GaveUpDownloadException, AbortedDownloadException {
        final boolean hasRepo = this.current.hasRepository();
        int attempt = 0;
        final int max = hasRepo ? this.current.getRepository().getCount() : 1;
        IOException cause = null;
        while (attempt < max) {
            ++attempt;
            String url = "";
            try {
                if (hasRepo) {
                    final Repo r = this.current.getRepository();
                    if (r.equals(ClientInstanceRepo.EMPTY_REPO) && r.getSelected() == 1) {
                        url = this.current.getRepository().getSelectedRepo() + URLEncoder.encode(this.current.getURL(), StandardCharsets.UTF_8.name());
                    }
                    else {
                        url = this.current.getRepository().getSelectedRepo() + this.current.getURL();
                    }
                }
                else {
                    url = this.current.getURL();
                }
                this.dlog(url);
                this.downloadURL(url, timeout);
                return;
            }
            catch (IOException e) {
                this.dlog("Failed to download from: ", url, e);
                cause = e;
                if (!hasRepo) {
                    continue;
                }
                this.current.getRepository().selectNext();
                continue;
            }
            break;
        }
        throw new GaveUpDownloadException(this.current, cause);
    }
    
    private void downloadURL(final String path, final int timeout) throws IOException, AbortedDownloadException {
        final URL url = new URL(path);
        final URLConnection urlConnection = url.openConnection(U.getProxy());
        if (!(urlConnection instanceof HttpURLConnection)) {
            throw new IOException("Invalid protocol: " + url.getProtocol());
        }
        final HttpURLConnection connection = (HttpURLConnection)urlConnection;
        Downloadable.setUp(connection, timeout, this.current.getInsertUA());
        if (!this.launched) {
            throw new AbortedDownloadException();
        }
        final long reply_s = System.currentTimeMillis();
        connection.connect();
        final long reply = System.currentTimeMillis() - reply_s;
        final InputStream in = new BufferedInputStream(connection.getInputStream());
        final File file = this.current.getDestination();
        final File temp = FileUtil.makeTemp(new File(file.getAbsolutePath() + ".tlauncherdownload"));
        final OutputStream out = new BufferedOutputStream(new FileOutputStream(temp));
        long read = 0L;
        final long length = connection.getContentLength();
        long speed_s;
        long timer;
        final long downloaded_s = timer = (speed_s = System.currentTimeMillis());
        final byte[] buffer = new byte[65536];
        int curread = in.read(buffer);
        while (curread > 0) {
            if (!this.launched) {
                out.close();
                throw new AbortedDownloadException();
            }
            read += curread;
            out.write(buffer, 0, curread);
            curread = in.read(buffer);
            if (curread == -1) {
                break;
            }
            final long speed_e = System.currentTimeMillis() - speed_s;
            if (speed_e < 50L) {
                continue;
            }
            speed_s = System.currentTimeMillis();
            final long downloaded_e = speed_s - downloaded_s;
            final double curdone = read / (float)length;
            final double curspeed = read / (double)downloaded_e;
            if (speed_s - timer > 15000L) {
                timer = speed_s;
            }
            this.onProgress(curread, curdone, curspeed);
        }
        final long downloaded_e = System.currentTimeMillis() - downloaded_s;
        final double downloadSpeed = (downloaded_e != 0L) ? (read / (double)downloaded_e) : 0.0;
        in.close();
        out.close();
        connection.disconnect();
        FileUtil.copyFile(temp, file, true);
        FileUtil.deleteFile(temp);
        final List<File> copies = this.current.getAdditionalDestinations();
        if (copies.size() > 0) {
            this.dlog("Found additional destinations. Copying...");
            for (final File copy : copies) {
                this.dlog("Copying " + copy + "...");
                FileUtil.copyFile(file, copy, this.current.isForce());
                this.dlog("Success!");
            }
            this.dlog("Copying completed.");
        }
        this.onComplete();
    }
    
    private void onStart() {
        this.current.onStart();
    }
    
    private void onError(final Throwable e) {
        this.current.onError(e);
        this.downloader.onFileComplete(this, this.current);
    }
    
    private void onProgress(final double curread, final double curdone, final double curspeed) {
        if (++this.speedCaret == 100) {
            this.speedCaret = 0;
        }
        this.averageSpeedContainer[this.speedCaret] = curspeed;
        this.currentProgress = this.doneProgress + this.eachProgress * curdone;
        if (this.currentProgress - this.lastProgress < 0.01) {
            return;
        }
        this.lastProgress = this.currentProgress;
        this.speed = U.getAverage(this.averageSpeedContainer);
        this.downloader.onProgress(this, this.currentProgress, this.speed);
    }
    
    private void onComplete() throws RetryDownloadException {
        this.doneProgress += this.eachProgress;
        this.current.onComplete();
        this.downloader.onProgress(this, this.doneProgress, this.speed);
        this.downloader.onFileComplete(this, this.current);
    }
    
    private void tlog(final Object... o) {
        U.plog(this.LOGGER_PREFIX, o);
    }
    
    private void dlog(final String message, final String url, final Throwable ex) {
        if (ex == null) {
            U.plog(this.LOGGER_PREFIX, "> ", message, url);
        }
        else {
            U.plog(this.LOGGER_PREFIX, "> ", message, url, ex);
        }
        if (this.current.hasConsole()) {
            if (ex == null) {
                this.current.getContainer().getConsole().log(this.LOGGER_PREFIX, "> ", message, url);
            }
            else {
                this.current.getContainer().getConsole().log(this.LOGGER_PREFIX, "> ", message, url, ex);
            }
        }
    }
    
    private void dlog(final String message) {
        this.dlog(message, "", null);
    }
    
    private void dlog(final String message, final Throwable ex) {
        this.dlog(message, "", ex);
    }
    
    private void dlog(final String message, final String url) {
        this.dlog(message, url, null);
    }
}
