// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

import java.io.IOException;

class GaveUpDownloadException extends IOException
{
    private static final long serialVersionUID = 5762388485267411115L;
    
    GaveUpDownloadException(final Downloadable d, final Throwable cause) {
        super(d.getURL(), cause);
    }
}
