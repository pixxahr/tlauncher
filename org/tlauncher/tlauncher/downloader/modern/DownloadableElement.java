// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader.modern;

import java.nio.file.Path;
import java.util.List;

public class DownloadableElement
{
    private List<String> inList;
    private Path out;
    
    public DownloadableElement(final List<String> outList, final Path in) {
        this.inList = outList;
        this.out = in;
    }
    
    public List<String> getInList() {
        return this.inList;
    }
    
    public void setInList(final List<String> inList) {
        this.inList = inList;
    }
    
    public Path getOut() {
        return this.out;
    }
    
    public void setOut(final Path out) {
        this.out = out;
    }
}
