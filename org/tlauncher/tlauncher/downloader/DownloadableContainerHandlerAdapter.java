// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

public abstract class DownloadableContainerHandlerAdapter implements DownloadableContainerHandler
{
    @Override
    public void onStart(final DownloadableContainer c) {
    }
    
    @Override
    public void onAbort(final DownloadableContainer c) {
    }
    
    @Override
    public void onError(final DownloadableContainer c, final Downloadable d, final Throwable e) {
    }
    
    @Override
    public void onComplete(final DownloadableContainer c, final Downloadable d) throws RetryDownloadException {
    }
    
    @Override
    public void onFullComplete(final DownloadableContainer c) {
    }
}
