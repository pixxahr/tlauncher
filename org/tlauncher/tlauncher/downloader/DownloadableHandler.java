// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

public interface DownloadableHandler
{
    void onStart(final Downloadable p0);
    
    void onAbort(final Downloadable p0);
    
    void onComplete(final Downloadable p0) throws RetryDownloadException;
    
    void onError(final Downloadable p0, final Throwable p1);
}
