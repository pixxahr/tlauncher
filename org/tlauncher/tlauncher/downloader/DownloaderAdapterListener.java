// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

public abstract class DownloaderAdapterListener implements DownloaderListener
{
    @Override
    public void onDownloaderStart(final Downloader d, final int files) {
    }
    
    @Override
    public void onDownloaderAbort(final Downloader d) {
    }
    
    @Override
    public void onDownloaderProgress(final Downloader d, final double progress, final double speed) {
    }
    
    @Override
    public void onDownloaderFileComplete(final Downloader d, final Downloadable file) {
    }
    
    @Override
    public void onDownloaderComplete(final Downloader d) {
    }
}
