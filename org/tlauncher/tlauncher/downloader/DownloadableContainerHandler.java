// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.downloader;

public interface DownloadableContainerHandler
{
    void onStart(final DownloadableContainer p0);
    
    void onAbort(final DownloadableContainer p0);
    
    void onError(final DownloadableContainer p0, final Downloadable p1, final Throwable p2);
    
    void onComplete(final DownloadableContainer p0, final Downloadable p1) throws RetryDownloadException;
    
    void onFullComplete(final DownloadableContainer p0);
}
