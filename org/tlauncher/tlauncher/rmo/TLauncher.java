// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.rmo;

import java.util.List;
import org.tlauncher.tlauncher.downloader.DownloaderListener;
import org.tlauncher.util.statistics.InstallVersionListener;
import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.site.play.SitePlay;
import org.tlauncher.tlauncher.managers.VersionManagerListener;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.tlauncher.tlauncher.modpack.ModpackUtil;
import org.tlauncher.tlauncher.managers.ModpackManager;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftExtendedAdapter;
import org.tlauncher.tlauncher.ui.scenes.PseudoScene;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftException;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListenerAdapter;
import org.tlauncher.util.statistics.GameRunningListener;
import org.tlauncher.tlauncher.managers.popup.menu.HotServerManager;
import org.tlauncher.tlauncher.entity.server.RemoteServer;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListener;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import org.tlauncher.tlauncher.ui.progress.ProgressFrame;
import com.google.inject.Guice;
import com.google.inject.Module;
import org.tlauncher.util.guice.GuiceModule;
import org.tlauncher.tlauncher.configuration.ArgumentParser;
import org.apache.log4j.LogManager;
import org.tlauncher.tlauncher.configuration.enums.ActionOnLaunch;
import javax.swing.LookAndFeel;
import java.util.Objects;
import org.tlauncher.tlauncher.ui.explorer.FileExplorer;
import java.nio.file.Path;
import java.util.Collection;
import com.google.common.collect.Sets;
import com.sothawo.mapjfx.cache.OfflineCache;
import java.util.Iterator;
import java.util.Map;
import org.tlauncher.util.statistics.StatisticsUtil;
import com.google.common.collect.Maps;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.util.async.AsyncThread;
import org.tlauncher.tlauncher.configuration.test.environment.JavaBitTestEnvironment;
import org.tlauncher.tlauncher.configuration.test.environment.TestEnvironment;
import java.util.ArrayList;
import org.tlauncher.util.DoubleRunningTimer;
import javax.swing.UIManager;
import java.io.PrintStream;
import org.tlauncher.util.stream.Logger;
import org.tlauncher.util.stream.LinkedStringStream;
import java.io.OutputStream;
import org.tlauncher.util.stream.MirroredLinkedStringStream;
import org.tlauncher.tlauncher.handlers.ExceptionHandler;
import org.tlauncher.tlauncher.managers.ComponentManagerListenerHelper;
import org.tlauncher.tlauncher.managers.ServerListManager;
import org.tlauncher.tlauncher.ui.browser.BrowserHolder;
import org.tlauncher.tlauncher.ui.browser.JFXStartPageLoader;
import org.tlauncher.util.TlauncherUtil;
import java.time.chrono.ChronoLocalDate;
import java.time.LocalDate;
import java.util.Locale;
import org.tlauncher.util.SwingUtil;
import org.tlauncher.tlauncher.configuration.enums.ConsoleType;
import org.tlauncher.util.OS;
import java.io.IOException;
import org.tlauncher.util.U;
import org.tlauncher.util.FileUtil;
import org.tlauncher.util.MinecraftUtil;
import org.tlauncher.util.Time;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.Inject;
import org.tlauncher.util.guice.MinecraftLauncherFactory;
import org.tlauncher.tlauncher.managers.AdditionalAssetsComponent;
import org.tlauncher.tlauncher.ui.listener.MinecraftUIListener;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;
import org.tlauncher.tlauncher.managers.TLauncherManager;
import org.tlauncher.tlauncher.managers.ProfileManager;
import org.tlauncher.tlauncher.managers.VersionManager;
import org.tlauncher.tlauncher.managers.ComponentManager;
import org.tlauncher.tlauncher.ui.TLauncherFrame;
import org.tlauncher.tlauncher.updater.client.AutoUpdater;
import org.tlauncher.tlauncher.downloader.Downloader;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.configuration.LangConfiguration;
import joptsimple.OptionSet;
import com.google.inject.Injector;
import org.tlauncher.tlauncher.configuration.InnerConfiguration;
import com.google.gson.Gson;
import org.tlauncher.tlauncher.ui.console.Console;
import org.tlauncher.util.stream.PrintLogger;
import java.io.File;

public class TLauncher
{
    public static boolean DEBUG;
    private static volatile TLauncher instance;
    private static String[] sargs;
    private static File directory;
    private static PrintLogger print;
    private static Console console;
    private static Gson gson;
    private static InnerConfiguration innerSettings;
    private static String family;
    private static Injector injector;
    private final OptionSet args;
    private final String defaultPrefix;
    private LangConfiguration lang;
    private Configuration configuration;
    private Downloader downloader;
    private AutoUpdater updater;
    private TLauncherFrame frame;
    private ComponentManager manager;
    private VersionManager versionManager;
    private ProfileManager profileManager;
    private TLauncherManager tlauncherManager;
    private MinecraftLauncher launcher;
    private MinecraftUIListener minecraftListener;
    private AdditionalAssetsComponent additionalAssetsComponent;
    @Inject
    private MinecraftLauncherFactory minecraftLauncherFactory;
    private boolean ready;
    
    @Inject
    public TLauncher(@Assisted final OptionSet set) throws Exception {
        this.defaultPrefix = this.getPagePrefix();
        Time.start(this);
        TLauncher.instance = this;
        this.args = set;
        TLauncher.gson = new Gson();
        File oldConfig = MinecraftUtil.getSystemRelatedDirectory(TLauncher.innerSettings.get("settings"));
        final File newConfig = MinecraftUtil.getSystemRelatedDirectory(TLauncher.innerSettings.get("settings.new"));
        if (!oldConfig.isFile()) {
            oldConfig = MinecraftUtil.getSystemRelatedDirectory(TLauncher.innerSettings.get("tlauncher.folder"));
        }
        if (oldConfig.isFile() && !newConfig.isFile()) {
            boolean copied = true;
            try {
                FileUtil.createFile(newConfig);
                FileUtil.copyFile(oldConfig, newConfig, true);
            }
            catch (IOException ioE) {
                U.log("Cannot copy old configuration to the new place", oldConfig, newConfig, ioE);
                copied = false;
            }
            if (copied) {
                U.log("Old configuration successfully moved to the new place:", newConfig);
                FileUtil.deleteFile(oldConfig);
            }
        }
        U.setLoadingStep(Bootstrapper.LoadingStep.LOADING_CONFIGURATION);
        this.configuration = Configuration.createConfiguration(set);
        U.log("Machine info:", OS.getSummary());
        this.reloadLocale();
        U.setLoadingStep(Bootstrapper.LoadingStep.LOADING_CONSOLE);
        (TLauncher.console = new Console(this.configuration, TLauncher.print, this.lang.get("console"), this.configuration.getConsoleType() == ConsoleType.GLOBAL)).setCloseAction(Console.CloseAction.KILL);
        System.out.println(TLauncher.console.frame.textarea.getText());
        Console.updateLocale();
        this.initLogs();
        SwingUtil.init();
        U.logField.info(TLauncher.console.frame.textarea.getText());
        if (!this.configuration.getLocale().getLanguage().equals(new Locale("zh").getLanguage())) {
            TLauncher.family = "Roboto";
        }
        if (this.configuration.get("ssl.deactivate.date") != null && LocalDate.parse(this.configuration.get("ssl.deactivate.date")).isAfter(LocalDate.now())) {
            TlauncherUtil.deactivateSSL();
        }
        this.downloader = new Downloader(this.configuration.getConnectionQuality());
        this.minecraftListener = new MinecraftUIListener(this);
        (this.updater = new AutoUpdater(this)).asyncFindUpdate();
        JFXStartPageLoader.getInstance();
        BrowserHolder.getInstance();
        U.setLoadingStep(Bootstrapper.LoadingStep.LOADING_MANAGERS);
        this.manager = new ComponentManager(this);
        this.tlauncherManager = this.manager.loadComponent(TLauncherManager.class);
        this.versionManager = this.manager.loadComponent(VersionManager.class);
        this.profileManager = this.manager.loadComponent(ProfileManager.class);
        ProfileManager.init();
        this.manager.loadComponent(ServerListManager.class).asyncRefresh();
        this.manager.loadComponent(ComponentManagerListenerHelper.class);
        this.additionalAssetsComponent = this.manager.loadComponent(AdditionalAssetsComponent.class);
    }
    
    public static String getFamily() {
        return TLauncher.family;
    }
    
    public static void main(final String[] args) {
        try {
            final ExceptionHandler handler = ExceptionHandler.getInstance();
            Thread.setDefaultUncaughtExceptionHandler(handler);
            Thread.currentThread().setUncaughtExceptionHandler(handler);
            final MirroredLinkedStringStream stream = new MirroredLinkedStringStream() {
                @Override
                public void flush() {
                    if (TLauncher.console == null) {
                        try {
                            this.getMirror().flush();
                        }
                        catch (IOException ex) {}
                    }
                    else {
                        super.flush();
                    }
                }
            };
            stream.setMirror(System.out);
            stream.setLogger(TLauncher.print = new PrintLogger(stream));
            System.setOut(TLauncher.print);
            U.setLoadingStep(Bootstrapper.LoadingStep.INITIALIZING);
            UIManager.put("FileChooser.useSystemExtensionHiding", false);
            System.setProperty("java.net.preferIPv4Stack", "true");
            Class.forName("org.apache.log4j.helpers.NullEnumeration");
            initLookAndFeel();
            initUrlCache();
            launch(args);
            ((DoubleRunningTimer)getInjector().getInstance((Class)DoubleRunningTimer.class)).clearTimeLabel();
            final Configuration c = getInstance().getConfiguration();
            c.set("memory.problem.message", null, false);
            if (OS.Arch.CURRENT != OS.Arch.x64) {
                final Configuration configuration;
                final ArrayList<TestEnvironment> list;
                final Iterator<TestEnvironment> iterator;
                TestEnvironment t;
                AsyncThread.execute(() -> {
                    OS.fillSystemInfo();
                    list = new ArrayList<TestEnvironment>() {
                        final /* synthetic */ Configuration val$c;
                        
                        {
                            ((ArrayList<JavaBitTestEnvironment>)this).add(new JavaBitTestEnvironment(this.val$c));
                        }
                    };
                    list.iterator();
                    while (iterator.hasNext()) {
                        t = iterator.next();
                        if (!t.testEnvironment()) {
                            t.fix();
                        }
                    }
                    configuration.store();
                    return;
                });
            }
        }
        catch (Throwable e) {
            U.log("Error launching TLauncher:");
            U.log(e);
            e.printStackTrace(TLauncher.print);
            if (Localizable.exists()) {
                TlauncherUtil.showCriticalProblem(Localizable.get("alert.error.not.run") + "<br><br>" + TlauncherUtil.getStringError(e));
                kill();
            }
            else {
                TlauncherUtil.showCriticalProblem(e);
            }
        }
        final Configuration conf = ((TLauncher)TLauncher.injector.getInstance((Class)TLauncher.class)).getConfiguration();
        if (TlauncherUtil.contains_OPTIONS_JAVA_XMX() && !conf.getBoolean("java_options.skip") && Alert.showWarningMessageWithCheckBox("", "crash.remove._java_options", 400)) {
            conf.set("java_options.skip", true);
        }
        if (OS.is(OS.WINDOWS)) {
            String pr = OS.executeByTerminal("wmic CPU get NAME");
            final String[] array = pr.split(System.lineSeparator());
            if (array.length == 4) {
                pr = array[2];
            }
            conf.set("process.info", pr.trim());
        }
        TlauncherUtil.fillGPUInfo(conf, true);
        final Configuration c2 = getInstance().getConfiguration();
        StatisticsUtil.startSending("save/run/tlauncher", null, Maps.newHashMap());
        if (!enterGap(c2.getLong("sending.tlauncher.unique"))) {
            StatisticsUtil.sendMachineInfo(conf);
            c2.set("sending.tlauncher.unique", System.currentTimeMillis());
        }
        if (TlauncherUtil.hostAvailabilityCheck("http://page.tlauncher.org") == 503 && TlauncherUtil.hostAvailabilityCheck("https://www.minecraft.net") == 200) {
            Alert.showErrorHtml("", "alert.block.ip");
        }
        if (OS.is(OS.WINDOWS)) {
            final boolean KB4515384Exists = OS.executeByTerminal("wmic qfe get HotFixID", 5).contains("KB4515384");
            if (KB4515384Exists) {
                conf.set("block.updater.message", true);
            }
        }
    }
    
    private static void initUrlCache() throws IOException {
        final OfflineCache c = OfflineCache.INSTANCE;
        final Path urlCache = MinecraftUtil.getTLauncherFile("cache" + File.separator + "requests").toPath();
        FileUtil.createFolder(urlCache.toFile());
        c.setCacheDirectory(urlCache);
        c.setNoCacheFilters(Sets.newHashSet((Object[])new String[] { ".*jar$", ".*zip$", ".*js$" }));
        c.removeExpiredCache();
        c.setActive(true);
    }
    
    private static void initLookAndFeel() {
        LookAndFeel defaultLookAndFeel = null;
        try {
            defaultLookAndFeel = UIManager.getLookAndFeel();
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            new FileExplorer();
        }
        catch (Throwable t) {
            U.log("problem with ", t);
            if (Objects.nonNull(defaultLookAndFeel)) {
                try {
                    UIManager.setLookAndFeel(defaultLookAndFeel);
                }
                catch (Throwable e) {
                    U.log("problem with look and feel ", e);
                }
            }
        }
    }
    
    public static Console getConsole() {
        return TLauncher.console;
    }
    
    public static Gson getGson() {
        return TLauncher.gson;
    }
    
    public static void kill() {
        U.log("Good bye!");
        try {
            final Class cl = Class.forName("org.tlauncher.tlauncher.managers.TLauncherManager$3");
            if (Objects.nonNull(cl) && Objects.nonNull(getInstance()) && Objects.nonNull(getInstance().tlauncherManager)) {
                final String value = getInstance().getConfiguration().get("minecraft.onlaunch");
                if (!ActionOnLaunch.EXIT.name().equalsIgnoreCase(value)) {
                    getInstance().tlauncherManager.cleanMods();
                }
            }
        }
        catch (ClassNotFoundException ex) {}
        catch (Throwable e) {
            U.log(e);
        }
        LogManager.shutdown();
        System.exit(0);
    }
    
    private static void launch(final String[] args) throws Exception {
        final GuiceModule guiceModule = new GuiceModule(ArgumentParser.parseArgs(args));
        guiceModule.setInjector(TLauncher.injector = Guice.createInjector(new Module[] { (Module)guiceModule }));
        final ProgressFrame customBar = new ProgressFrame(TLauncher.innerSettings.get("version"));
        U.log(String.format("Starting TLauncher %s %s", TLauncher.innerSettings.get("version"), TLauncher.innerSettings.get("type")));
        U.log("For more information, visit https://tlauncher.org/");
        U.log("Startup time:", DateFormat.getDateTimeInstance().format(new Date()));
        U.log("---");
        ((DoubleRunningTimer)TLauncher.injector.getInstance((Class)DoubleRunningTimer.class)).startProtection();
        TLauncher.sargs = args;
        final TLauncher t = (TLauncher)TLauncher.injector.getInstance((Class)TLauncher.class);
        U.setLoadingStep(Bootstrapper.LoadingStep.LOADING_WINDOW);
        t.frame = new TLauncherFrame(t);
        t.init(customBar);
    }
    
    private static void log(final String line) {
        U.log("[TLauncher] " + line);
    }
    
    public static InnerConfiguration getInnerSettings() {
        return TLauncher.innerSettings;
    }
    
    public static String[] getArgs() {
        if (TLauncher.sargs == null) {
            TLauncher.sargs = new String[0];
        }
        return TLauncher.sargs;
    }
    
    public static File getDirectory() {
        if (TLauncher.directory == null) {
            TLauncher.directory = new File(".");
        }
        return TLauncher.directory;
    }
    
    public static TLauncher getInstance() {
        return TLauncher.instance;
    }
    
    public static double getVersion() {
        return TLauncher.innerSettings.getDouble("version");
    }
    
    public static String getFolder() {
        return TLauncher.innerSettings.get("folder");
    }
    
    public static String[] getUpdateRepos() {
        return TLauncher.innerSettings.getArrayAccess("update.repo");
    }
    
    public static String[] getStatisticsConfigUrl() {
        return TLauncher.innerSettings.getArray("statistics.config.url");
    }
    
    public static Injector getInjector() {
        return TLauncher.injector;
    }
    
    private void initLogs() {
        int counter = this.configuration.getInteger("log.file.counter");
        U.initializeLoggerU(MinecraftUtil.getWorkingDirectory().getPath(), "tlauncher", counter);
        if (++counter > 10) {
            this.configuration.set("log.file.counter", 0);
        }
        else {
            this.configuration.set("log.file.counter", counter);
        }
    }
    
    private static boolean enterGap(final Long last) {
        if (last == 0L) {
            return false;
        }
        final Calendar start = Calendar.getInstance();
        start.set(10, 0);
        start.set(12, 0);
        start.set(13, 0);
        final Calendar end = Calendar.getInstance();
        end.set(10, 23);
        end.set(12, 59);
        end.set(13, 59);
        return start.getTimeInMillis() < last && last < end.getTimeInMillis();
    }
    
    public Downloader getDownloader() {
        return this.downloader;
    }
    
    public LangConfiguration getLang() {
        return this.lang;
    }
    
    public Configuration getConfiguration() {
        return this.configuration;
    }
    
    public AutoUpdater getUpdater() {
        return this.updater;
    }
    
    public OptionSet getArguments() {
        return this.args;
    }
    
    public TLauncherFrame getFrame() {
        return this.frame;
    }
    
    public ComponentManager getManager() {
        return this.manager;
    }
    
    public VersionManager getVersionManager() {
        return this.versionManager;
    }
    
    public ProfileManager getProfileManager() {
        return this.profileManager;
    }
    
    public TLauncherManager getTLauncherManager() {
        return this.tlauncherManager;
    }
    
    public MinecraftLauncher getLauncher() {
        return this.launcher;
    }
    
    public boolean isReady() {
        return this.ready;
    }
    
    public void reloadLocale() throws IOException {
        final Locale locale = this.configuration.getLocale();
        U.log("Selected locale: " + locale);
        if (this.lang == null) {
            this.lang = new LangConfiguration(this.configuration.getLocales(), locale, "/lang/tlauncher/");
        }
        else {
            this.lang.setSelected(locale);
        }
        Localizable.setLang(this.lang);
        Alert.prepareLocal();
        if (TLauncher.console != null) {
            TLauncher.console.setName(this.lang.get("console"));
        }
    }
    
    public void launch(final MinecraftListener listener, final RemoteServer server, final boolean forceupdate) {
        (this.launcher = this.minecraftLauncherFactory.create(this, forceupdate)).addListener(this.minecraftListener);
        this.launcher.addListener(listener);
        this.launcher.addListener(this.frame.mp.getProgress());
        this.launcher.addListener(getInstance().getTLauncherManager());
        this.launcher.addListener((MinecraftListener)TLauncher.injector.getInstance((Class)HotServerManager.class));
        this.launcher.addListener(new GameRunningListener(this.launcher));
        this.launcher.addListener(new MinecraftListenerAdapter() {
            @Override
            public void onMinecraftKnownError(final MinecraftException e) {
                if (e.getLangPath().equalsIgnoreCase("start")) {
                    TLauncher.this.frame.mp.setScene(TLauncher.this.frame.mp.settingsScene);
                }
            }
        });
        this.launcher.setServer(server);
        this.launcher.addListener(new MinecraftExtendedAdapter() {
            @Override
            public void onMinecraftDownloading() {
                if (TLauncher.this.launcher.getVersion().isModpack()) {
                    final String mask = "download";
                    try {
                        final ModpackVersionDTO v = (ModpackVersionDTO)TLauncher.this.launcher.getVersion().getModpack().getVersion();
                        boolean cleanOldDownloadFile = false;
                        for (final GameType type : GameType.getSubEntities()) {
                            if (type.equals(GameType.MAP)) {
                                continue;
                            }
                            for (int i = 0; i < v.getByType(type).size(); ++i) {
                                final GameEntityDTO b = (GameEntityDTO)v.getByType(type).get(i);
                                if (b.getVersion().getMetadata().getPath().endsWith(mask)) {
                                    final GameEntityDTO replacer = ((ModpackManager)TLauncher.injector.getInstance((Class)ModpackManager.class)).readFromServer(b.getClass(), b, b.getVersion());
                                    U.log(String.format("replace broken element %s %s", b.getVersion().getMetadata().getPath(), replacer.getVersion().getMetadata().getPath()));
                                    b.getVersion().setMetadata(replacer.getVersion().getMetadata());
                                    cleanOldDownloadFile = true;
                                }
                            }
                            if (!cleanOldDownloadFile) {
                                continue;
                            }
                            Files.deleteIfExists(Paths.get(ModpackUtil.getPath(TLauncher.this.launcher.getVersion(), type).toString(), mask));
                        }
                    }
                    catch (Throwable e) {
                        U.log("got problem with fixing download link", e);
                    }
                }
            }
        });
        this.launcher.start();
    }
    
    public boolean isLauncherWorking() {
        return this.launcher != null && this.launcher.isWorking();
    }
    
    public void hide() {
        if (this.frame != null) {
            boolean doAgain = true;
            while (doAgain) {
                try {
                    this.frame.setVisible(false);
                    doAgain = false;
                }
                catch (Exception ex) {}
            }
        }
        U.log("I'm hiding!");
    }
    
    public void show() {
        if (this.frame != null) {
            boolean doAgain = true;
            while (doAgain) {
                try {
                    this.frame.setVisible(true);
                    doAgain = false;
                }
                catch (Exception e) {
                    U.log(e);
                }
            }
        }
        U.log("Here I am!");
    }
    
    public void newInstance() {
        Bootstrapper.main(TLauncher.sargs);
    }
    
    public String getPagePrefix() {
        if (this.defaultPrefix != null) {
            return this.defaultPrefix;
        }
        if (TlauncherUtil.hostAvailabilityCheck("http://page.tlauncher.org") == 200) {
            return "http://page.tlauncher.org/update/downloads/configs/client/";
        }
        if (TlauncherUtil.hostAvailabilityCheck("http://repo.tlauncher.org") == 200) {
            return "http://repo.tlauncher.org/update/downloads/configs/client/";
        }
        if (TlauncherUtil.hostAvailabilityCheck("http://repo.tlauncher.org") == 200) {
            return "http://advancedrepository.com/update/downloads/configs/client/";
        }
        return "127.0.0.1";
    }
    
    public void init(final ProgressFrame customBar) {
        this.frame.afterInitProfile();
        U.setLoadingStep(Bootstrapper.LoadingStep.REFRESHING_INFO);
        this.versionManager.addListener((VersionManagerListener)TLauncher.injector.getInstance((Class)SitePlay.class));
        this.versionManager.addListener((VersionManagerListener)TLauncher.injector.getInstance((Class)HotServerManager.class));
        this.versionManager.asyncRefresh();
        this.additionalAssetsComponent.asyncRefresh();
        SwingUtilities.invokeLater(() -> {
            this.frame.mp.defaultScene.loginForm.accountComboBox.updateLocale();
            this.frame.mp.accountEditor.list.refreshFrom(this.profileManager.getAuthDatabase());
            return;
        });
        this.downloader.addListener(new InstallVersionListener());
        final ModpackManager modpackManager = (ModpackManager)TLauncher.injector.getInstance((Class)ModpackManager.class);
        U.log("Started! (" + Time.stop(this) + " ms.)");
        this.ready = true;
        U.setLoadingStep(Bootstrapper.LoadingStep.SUCCESS);
        customBar.setVisible(false);
        customBar.dispose();
        this.frame.setVisible(true);
        log("show tlauncher!!!");
        this.frame.showTips();
    }
    
    public AdditionalAssetsComponent getAdditionalAssetsComponent() {
        return this.additionalAssetsComponent;
    }
    
    static {
        TLauncher.DEBUG = false;
        U.log("[TLAUNCHER][STATIC] initialize inner config");
        try {
            TLauncher.innerSettings = new InnerConfiguration(FileUtil.getResourceAppStream("/inner.tlauncher.properties"));
        }
        catch (IOException e) {
            U.log("[TLAUNCHER][STATIC] couldn't initialize the inner config");
            System.exit(-1);
        }
    }
}
