// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.tlauncher.rmo;

import org.tlauncher.util.Reflect;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.nio.file.OpenOption;
import net.minecraft.launcher.process.JavaProcessListener;
import org.apache.log4j.LogManager;
import org.tlauncher.util.StringUtil;
import javax.swing.Icon;
import java.awt.Component;
import javax.swing.JOptionPane;
import java.util.List;
import java.io.Reader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.tlauncher.tlauncher.configuration.Configuration;
import java.util.Locale;
import com.google.gson.Gson;
import org.tlauncher.util.MinecraftUtil;
import java.nio.file.Path;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import org.tlauncher.tlauncher.updater.bootstrapper.model.DownloadedBootInfo;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.util.Objects;
import java.io.IOException;
import org.tlauncher.tlauncher.downloader.DownloaderListener;
import org.tlauncher.tlauncher.updater.bootstrapper.PreparedEnvironmentComponentImpl;
import org.tlauncher.tlauncher.updater.bootstrapper.view.DownloadingFrameElement;
import org.tlauncher.tlauncher.downloader.Downloader;
import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import org.tlauncher.util.FileUtil;
import org.tlauncher.util.TlauncherUtil;
import org.tlauncher.util.OS;
import javax.swing.UIManager;
import org.tlauncher.util.U;
import java.nio.channels.FileLock;
import org.tlauncher.tlauncher.updater.bootstrapper.PreparedEnvironmentComponent;
import net.minecraft.launcher.process.JavaProcess;
import org.tlauncher.util.DoubleRunningTimer;
import net.minecraft.launcher.process.JavaProcessLauncher;
import org.tlauncher.tlauncher.updater.bootstrapper.model.LibraryConfig;
import org.tlauncher.tlauncher.updater.bootstrapper.model.JavaConfig;
import org.tlauncher.tlauncher.configuration.LangConfiguration;
import org.tlauncher.tlauncher.configuration.SimpleConfiguration;
import java.io.File;

public final class Bootstrapper
{
    private static final String linkUpdateErrorRu = "https://tlauncher.org/ru/error-kb4515384.html";
    private static final String linkUpdateErrorEn = "https://tlauncher.org/en/error-kb4515384.html";
    public static final File directory;
    public static SimpleConfiguration innerConfig;
    public static LangConfiguration langConfiguration;
    private static SimpleConfiguration launcherConfig;
    private static JavaConfig javaConfig;
    private static LibraryConfig libraryConfig;
    private final BootstrapperListener listener;
    private JavaProcessLauncher processLauncher;
    private DoubleRunningTimer doubleRunningTimer;
    private JavaProcess process;
    private boolean started;
    private String[] args;
    private PreparedEnvironmentComponent preparedEnvironmentComponent;
    private File jvmFolder;
    public static final String PROTECTION = "protection.txt";
    private FileLock lock;
    private static int i;
    
    public Bootstrapper(final String[] args) {
        this.args = args;
        (this.doubleRunningTimer = new DoubleRunningTimer()).startProtection();
        this.listener = new BootstrapperListener();
    }
    
    public static void main(final String[] args) {
        File jvmFolder = null;
        try {
            System.setProperty("java.net.preferIPv4Stack", "true");
            initConfig();
            U.initializeLoggerU(Bootstrapper.launcherConfig.get("minecraft.gamedir"), "boot", Bootstrapper.launcherConfig.getInteger("log.file.counter", 0));
            U.log("");
            U.log("-------------------------------------------------------------------");
            Class.forName("org.apache.log4j.helpers.NullEnumeration");
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
            catch (ExceptionInInitializerError error) {
                if (error.getCause() instanceof IllegalArgumentException && error.getCause().getMessage().contains("Text-specific LCD")) {
                    String link = "https://tlauncher.org/ru/font-error.html";
                    final boolean ussr = Bootstrapper.launcherConfig.isUSSRLocale();
                    if (!ussr) {
                        link = "https://tlauncher.org/en/font-error.html";
                    }
                    OS.openLink(link);
                    System.exit(-1);
                }
            }
            catch (Throwable ex) {
                U.log(ex);
            }
            if (TlauncherUtil.isDoubleRunning() && showQuestion(Bootstrapper.langConfiguration.get("double.running.title"), Bootstrapper.langConfiguration.get("double.running"), Bootstrapper.langConfiguration.get("yes"), Bootstrapper.langConfiguration.get("no")) != 0) {
                return;
            }
            if (!checkFreeSpace(FileUtil.SIZE_200)) {
                showDiskProblem();
            }
            validateWorkDir();
            validateTempDir();
            valitdateKB4515384();
            boolean properJRE = false;
            if (Bootstrapper.launcherConfig.getBoolean("not.work.jfxwebkit.dll") || Bootstrapper.launcherConfig.getBoolean("fixed.gpu.jre.error")) {
                properJRE = true;
            }
            final File jvm = getJVM(properJRE);
            jvmFolder = jvm.getParentFile();
            final Bootstrapper bootstrapper = new Bootstrapper(args);
            bootstrapper.activeDoublePreparingJVM();
            final Downloader downloader = new Downloader(ConnectionQuality.NORMAL);
            final DownloadingFrameElement downloadingBarElement = new DownloadingFrameElement(Bootstrapper.langConfiguration.get("updater.frame.name"));
            final PreparedEnvironmentComponent prepareLauncher = new PreparedEnvironmentComponentImpl(Bootstrapper.libraryConfig, Bootstrapper.javaConfig, getWorkFolder(), jvmFolder, downloader, properJRE);
            downloader.addListener(downloadingBarElement);
            final DownloadedBootInfo info = prepareLauncher.validateLibraryAndJava();
            prepareLauncher.preparedLibrariesAndJava(info);
            bootstrapper.setPreparedEnvironmentComponent(prepareLauncher);
            bootstrapper.setJVMFolder(jvm);
            bootstrapper.diactivateDoublePreparingJVM();
            bootstrapper.start();
        }
        catch (Throwable e) {
            if (e instanceof IOException && Objects.nonNull(jvmFolder) && jvmFolder.toString().contains("jvms")) {
                fixedOnce(args, jvmFolder);
            }
            e.printStackTrace();
            final String message = e.getMessage();
            if (Objects.nonNull(message) && message.contains("GetIpAddrTable")) {
                Alert.showErrorHtml("", Bootstrapper.langConfiguration.get("addr.table.error"));
                TLauncher.kill();
            }
            U.log("problem with preparing boostrapper");
            TlauncherUtil.showCriticalProblem(e);
        }
    }
    
    private static void valitdateKB4515384() {
        if (Bootstrapper.launcherConfig.getBoolean("block.updater.message")) {
            return;
        }
        if (!Bootstrapper.launcherConfig.getBoolean("retest.update")) {
            Bootstrapper.launcherConfig.set("retest.update", true, true);
            return;
        }
        if (OS.is(OS.WINDOWS)) {
            final boolean KB4515384Exists = OS.executeByTerminal("wmic qfe get HotFixID", 5).contains("KB4515384");
            if (KB4515384Exists) {
                showUpdateWinError();
                Alert.showErrorHtml(Bootstrapper.langConfiguration.get("warning.KB4515384.problem"), 500);
            }
        }
    }
    
    private static void validateTempDir() throws IOException {
        try {
            Files.createTempFile("test", "txt", (FileAttribute<?>[])new FileAttribute[0]);
        }
        catch (IOException e) {
            if (Objects.nonNull(System.getProperty("java.io.tmpdir"))) {
                final Path folder = Paths.get(System.getProperty("java.io.tmpdir"), new String[0]);
                if (Files.isRegularFile(folder, new LinkOption[0])) {
                    Files.delete(folder);
                }
                if (!Files.exists(folder, new LinkOption[0])) {
                    FileUtil.createFolder(folder.toFile());
                }
            }
        }
    }
    
    private static void fixedOnce(final String[] args, final File jvmFolder) {
        if (Bootstrapper.i != 0) {
            return;
        }
        ++Bootstrapper.i;
        FileUtil.deleteDirectory(jvmFolder);
        Alert.showErrorHtml("", Bootstrapper.langConfiguration.get("run.again.launcher"));
    }
    
    private static void validateWorkDir() {
        final String minecraftConfigDir = Bootstrapper.launcherConfig.get("minecraft.gamedir");
        try {
            if (Objects.nonNull(minecraftConfigDir)) {
                FileUtil.makeTemp(new File(minecraftConfigDir, "test.folder.txt"));
            }
        }
        catch (IOException e) {
            U.log(e);
            Bootstrapper.launcherConfig.set("minecraft.gamedir", null);
        }
    }
    
    private static File getJVM(final boolean properJVM) {
        if (OS.isJava8() && !TlauncherUtil.useX64JavaInsteadX32Java() && TlauncherUtil.hasCorrectJavaFX() && !properJVM) {
            return new File(OS.getJavaPathByHome(false));
        }
        final File tlauncherFolder = MinecraftUtil.getSystemRelatedDirectory("tlauncher");
        return TlauncherUtil.getJVMFolder(Bootstrapper.javaConfig, tlauncherFolder);
    }
    
    private static void showDiskProblem() {
        final String minecraftGamedir = Bootstrapper.launcherConfig.get("minecraft.gamedir");
        final File minecraftFolder = (minecraftGamedir == null) ? MinecraftUtil.getSystemRelatedDirectory(Bootstrapper.innerConfig.get("folder")) : new File(minecraftGamedir);
        final String url = Bootstrapper.launcherConfig.isUSSRLocale() ? "http://www.inetkomp.ru/uroki/488-osvobodit-mesto-na-diske-c.html" : "https://www.windowscentral.com/best-7-ways-free-hard-drive-space-windows-10";
        final String path = minecraftFolder.toPath().getRoot().toString();
        final String message = Bootstrapper.langConfiguration.get("place.disk.warning", path) + "<br><br>" + Bootstrapper.langConfiguration.get("alert.start.message", url);
        U.log(message);
        TlauncherUtil.showCriticalProblem(message);
        TLauncher.kill();
    }
    
    public static JavaProcessLauncher restartLauncher() {
        initConfig();
        final File directory = new File(".");
        final String path = OS.getJavaPathByHome(true);
        final JavaProcessLauncher processLauncher = new JavaProcessLauncher(path, new String[0]);
        log("choose jvm for restart:" + path);
        final String classPath = FileUtil.getRunningJar().getPath();
        processLauncher.directory(directory);
        processLauncher.addCommand("-cp");
        processLauncher.addCommand(classPath + System.getProperty("path.separator"));
        processLauncher.addCommand(Bootstrapper.innerConfig.get("bootstrapper.class"));
        U.debug(processLauncher);
        return processLauncher;
    }
    
    private static File getWorkFolder() {
        final String minecraftGamedir = Bootstrapper.launcherConfig.get("minecraft.gamedir");
        return (minecraftGamedir == null) ? MinecraftUtil.getSystemRelatedDirectory(Bootstrapper.innerConfig.get("folder")) : new File(minecraftGamedir);
    }
    
    private static void initConfig() {
        final Gson g = new Gson();
        try {
            try {
                Bootstrapper.innerConfig = new SimpleConfiguration(Bootstrapper.class.getResourceAsStream("/inner.tlauncher.properties"));
            }
            catch (NullPointerException ex) {
                final String path = FileUtil.getRunningJar().toString();
                if (path.contains("!" + File.separator)) {
                    Alert.showError("Error", String.format("Java can't work with path that contains symbol '!', create new local user without characters '!'(use new local user for game) and use path to TLauncher without '!' characters \r\ncurrent: %s\r\n\r\n\u0414\u0436\u0430\u0432\u0430 \u043d\u0435 \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442 c \u043f\u0443\u0442\u044f\u043c\u0438 \u0432 \u043a\u043e\u0442\u043e\u0440\u044b\u0445 \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u0442\u0441\u044f \u0432\u043e\u0441\u043a\u043b\u0438\u0446\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u0437\u043d\u0430\u043a '!' , \u0441\u043e\u0437\u0434\u0430\u0439\u0442\u0435 \u043d\u043e\u0432\u0443\u044e \u0443\u0447\u0435\u0442\u043d\u0443\u044e \u0437\u0430\u043f\u0438\u0441\u044c \u0431\u0435\u0437 '!' \u0437\u043d\u0430\u043a\u043e\u0432(\u0438\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0439\u0442\u0435 \u0435\u0451 \u0434\u043b\u044f \u0438\u0433\u0440\u044b) \u0438 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0439\u0442\u0435 \u043f\u0443\u0442\u044c \u043a \u0444\u0430\u0439\u043b\u0443 TLauncher \u0431\u0435\u0437 '!'\r\n \u0442\u0435\u043a\u0443\u0449\u0438\u0439: %s", path, path));
                    System.exit(-2);
                }
            }
            Bootstrapper.launcherConfig = new SimpleConfiguration(new File(MinecraftUtil.getSystemRelatedDirectory(Bootstrapper.innerConfig.get("settings.new")).getCanonicalPath()));
            final String locale = Bootstrapper.launcherConfig.get("locale");
            Locale locale2 = Locale.getDefault();
            if (locale != null) {
                locale2 = Configuration.getLocaleOf(locale);
            }
            final List<Locale> listLocales = Configuration.getDefaultLocales(Bootstrapper.innerConfig);
            final Locale selected = Configuration.findSuitableLocale(locale2, listLocales);
            Bootstrapper.langConfiguration = new LangConfiguration(listLocales.toArray(new Locale[0]), selected, Bootstrapper.innerConfig.get("bootstrapper.language.folder"));
            Bootstrapper.libraryConfig = g.fromJson(new InputStreamReader(Bootstrapper.class.getResourceAsStream("/bootstrapper.libraries.json"), Charset.forName("utf-8")), LibraryConfig.class);
            String fileName = "/bootstrapper.jre.json";
            if (Bootstrapper.launcherConfig.getBoolean("fixed.gpu.jre.error")) {
                fileName = "/bootstrapper.jre_fixed_gpu.json";
            }
            Bootstrapper.javaConfig = g.fromJson(new InputStreamReader(Bootstrapper.class.getResourceAsStream(fileName), Charset.forName("utf-8")), JavaConfig.class);
        }
        catch (Throwable e1) {
            TlauncherUtil.showCriticalProblem(e1);
        }
    }
    
    private static int showQuestion(final String title, final String messae, final String button, final String button2) {
        return JOptionPane.showOptionDialog(null, messae, title, 0, 2, null, new Object[] { button, button2 }, button2);
    }
    
    private static boolean checkFreeSpace(final long size) {
        final String minecraftGamedir = Bootstrapper.launcherConfig.get("minecraft.gamedir");
        final File minecraftFolder = (minecraftGamedir == null) ? MinecraftUtil.getSystemRelatedDirectory(Bootstrapper.innerConfig.get("folder")) : new File(minecraftGamedir);
        return FileUtil.checkFreeSpace(minecraftFolder, size);
    }
    
    private static void log(final Object... s) {
        U.log("[Bootstrapper]", s);
    }
    
    private JavaProcessLauncher createLauncher(final String[] args) {
        log("createLauncher");
        final String jvm = OS.appendBootstrapperJvm(this.jvmFolder.getPath());
        log("choose jvm:" + jvm);
        final JavaProcessLauncher processLauncher = new JavaProcessLauncher(jvm, new String[0]);
        processLauncher.directory(Bootstrapper.directory);
        processLauncher.addCommand("-Xmx" + Bootstrapper.innerConfig.get("max.memory") + "m");
        processLauncher.addCommand("-Dfile.encoding=UTF8");
        String classPath = FileUtil.getRunningJar().getPath();
        final String separator = File.pathSeparator;
        log("validate files");
        try {
            classPath += separator;
            classPath += StringUtil.convertListToString(separator, this.preparedEnvironmentComponent.getLibrariesForRunning());
        }
        catch (Throwable e) {
            U.log(e);
            StringBuilder builder = new StringBuilder(Bootstrapper.langConfiguration.get("updater.download.fail"));
            builder.append("<br>");
            builder = new StringBuilder(builder.toString().replace("- problem1", ""));
            TlauncherUtil.showCriticalProblem(builder.toString());
            System.exit(-1);
        }
        log("end validated files");
        processLauncher.addCommand("-cp", classPath);
        processLauncher.addCommand(Bootstrapper.innerConfig.get("main.class"));
        if (args != null && args.length > 0) {
            processLauncher.addCommands(args);
        }
        return processLauncher;
    }
    
    private void die(final int status) {
        log("I can be terminated now: " + status);
        if (!this.started && this.process.isRunning()) {
            log("...started instance also will be terminated.");
            this.process.stop();
        }
        LogManager.shutdown();
        System.exit(status);
    }
    
    public void start() throws IOException {
        this.processLauncher = this.createLauncher(this.args);
        this.doubleRunningTimer.cancel();
        log("Starting launcher...");
        this.processLauncher.setListener(this.listener);
        this.process = this.processLauncher.start();
    }
    
    public void setPreparedEnvironmentComponent(final PreparedEnvironmentComponent preparedEnvironmentComponent) {
        this.preparedEnvironmentComponent = preparedEnvironmentComponent;
    }
    
    public void setJVMFolder(final File jvmFolder) {
        this.jvmFolder = jvmFolder;
    }
    
    private void activeDoublePreparingJVM() throws IOException {
        final File tlauncherFolder = MinecraftUtil.getSystemRelatedDirectory("tlauncher");
        final File f = new File(tlauncherFolder, "protection.txt");
        FileUtil.createFile(f);
        if (f.exists()) {
            final FileChannel ch = FileChannel.open(f.toPath(), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
            this.lock = ch.tryLock();
            if (Objects.isNull(this.lock)) {
                LogManager.shutdown();
                System.exit(4);
            }
        }
    }
    
    private void diactivateDoublePreparingJVM() throws IOException {
        if (Objects.nonNull(this.lock)) {
            this.lock.release();
        }
    }
    
    private static void showUpdateWinError() {
        final boolean ussr = Bootstrapper.launcherConfig.isUSSRLocale();
        if (ussr) {
            OS.openLink("https://tlauncher.org/ru/error-kb4515384.html");
        }
        else {
            OS.openLink("https://tlauncher.org/en/error-kb4515384.html");
        }
    }
    
    static {
        directory = new File(".");
        Bootstrapper.i = 0;
    }
    
    public enum LoadingStep
    {
        INITIALIZING(21), 
        LOADING_CONFIGURATION(35), 
        LOADING_CONSOLE(41), 
        LOADING_MANAGERS(51), 
        LOADING_WINDOW(62), 
        PREPARING_MAINPANE(77), 
        POSTINIT_GUI(82), 
        REFRESHING_INFO(91), 
        SUCCESS(100);
        
        public static final String LOADING_PREFIX = "[Loading]";
        private final int percentage;
        
        private LoadingStep(final int percentage) {
            this.percentage = percentage;
        }
        
        public int getPercentage() {
            return this.percentage;
        }
    }
    
    private class BootstrapperListener implements JavaProcessListener
    {
        private StringBuffer buffer;
        
        private BootstrapperListener() {
            this.buffer = new StringBuffer();
        }
        
        @Override
        public void onJavaProcessLog(final JavaProcess jp, final String line) {
            U.plog('>', line);
            this.buffer.append(line).append('\n');
            if (line.startsWith("[Loading]")) {
                if (line.length() < "[Loading]".length() + 2) {
                    log(new Object[] { "Cannot parse line: content is empty." });
                    return;
                }
                final String content = line.substring("[Loading]".length() + 1);
                final LoadingStep step = Reflect.parseEnum(LoadingStep.class, content);
                if (step == null) {
                    log(new Object[] { "Cannot parse line: cannot parse step" });
                    return;
                }
                if (step.getPercentage() == 100) {
                    Bootstrapper.this.started = true;
                    Bootstrapper.this.die(0);
                }
            }
        }
        
        @Override
        public void onJavaProcessEnded(final JavaProcess jp) {
            final int exit = jp.getExitCode();
            if (exit == 1 && Bootstrapper.this.jvmFolder.toString().contains("jvms")) {
                fixedOnce(Bootstrapper.this.args, Bootstrapper.this.jvmFolder);
            }
            if (exit == -1073740791) {
                TlauncherUtil.showCriticalProblem(Bootstrapper.langConfiguration.get("alert.start.message", "https://tlauncher.org/ru/closed-minecraft-1073740791.html"));
                TlauncherUtil.showCriticalProblem(Bootstrapper.langConfiguration.get("alert.start.message", "https://tlauncher.org/en/closed-minecraft-1073740791.html"));
                Bootstrapper.this.die(exit);
            }
            else if (exit == -1073740771) {
                showUpdateWinError();
                Alert.showWarning("", Bootstrapper.langConfiguration.get("warning.KB4515384.problem"));
                Bootstrapper.this.die(exit);
            }
            if (exit != 0) {
                TlauncherUtil.showCriticalProblem(this.buffer.toString());
            }
            Bootstrapper.this.die(exit);
        }
        
        @Override
        public void onJavaProcessError(final JavaProcess jp, final Throwable e) {
            log(new Object[] { e });
        }
    }
}
