// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.statistics;

import org.codehaus.jackson.annotate.JsonProperty;

public class UpdaterDTO
{
    private String client;
    private String offer;
    private String country;
    private double currentVersion;
    private double newVersion;
    private String args;
    @JsonProperty("isUpdaterLater")
    private boolean updaterLater;
    private String requestTime;
    
    public String getClient() {
        return this.client;
    }
    
    public String getOffer() {
        return this.offer;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public double getCurrentVersion() {
        return this.currentVersion;
    }
    
    public double getNewVersion() {
        return this.newVersion;
    }
    
    public String getArgs() {
        return this.args;
    }
    
    public boolean isUpdaterLater() {
        return this.updaterLater;
    }
    
    public String getRequestTime() {
        return this.requestTime;
    }
    
    public void setClient(final String client) {
        this.client = client;
    }
    
    public void setOffer(final String offer) {
        this.offer = offer;
    }
    
    public void setCountry(final String country) {
        this.country = country;
    }
    
    public void setCurrentVersion(final double currentVersion) {
        this.currentVersion = currentVersion;
    }
    
    public void setNewVersion(final double newVersion) {
        this.newVersion = newVersion;
    }
    
    public void setArgs(final String args) {
        this.args = args;
    }
    
    public void setUpdaterLater(final boolean updaterLater) {
        this.updaterLater = updaterLater;
    }
    
    public void setRequestTime(final String requestTime) {
        this.requestTime = requestTime;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof UpdaterDTO)) {
            return false;
        }
        final UpdaterDTO other = (UpdaterDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$client = this.getClient();
        final Object other$client = other.getClient();
        Label_0065: {
            if (this$client == null) {
                if (other$client == null) {
                    break Label_0065;
                }
            }
            else if (this$client.equals(other$client)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$offer = this.getOffer();
        final Object other$offer = other.getOffer();
        Label_0102: {
            if (this$offer == null) {
                if (other$offer == null) {
                    break Label_0102;
                }
            }
            else if (this$offer.equals(other$offer)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$country = this.getCountry();
        final Object other$country = other.getCountry();
        Label_0139: {
            if (this$country == null) {
                if (other$country == null) {
                    break Label_0139;
                }
            }
            else if (this$country.equals(other$country)) {
                break Label_0139;
            }
            return false;
        }
        if (Double.compare(this.getCurrentVersion(), other.getCurrentVersion()) != 0) {
            return false;
        }
        if (Double.compare(this.getNewVersion(), other.getNewVersion()) != 0) {
            return false;
        }
        final Object this$args = this.getArgs();
        final Object other$args = other.getArgs();
        Label_0208: {
            if (this$args == null) {
                if (other$args == null) {
                    break Label_0208;
                }
            }
            else if (this$args.equals(other$args)) {
                break Label_0208;
            }
            return false;
        }
        if (this.isUpdaterLater() != other.isUpdaterLater()) {
            return false;
        }
        final Object this$requestTime = this.getRequestTime();
        final Object other$requestTime = other.getRequestTime();
        if (this$requestTime == null) {
            if (other$requestTime == null) {
                return true;
            }
        }
        else if (this$requestTime.equals(other$requestTime)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof UpdaterDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $client = this.getClient();
        result = result * 59 + (($client == null) ? 43 : $client.hashCode());
        final Object $offer = this.getOffer();
        result = result * 59 + (($offer == null) ? 43 : $offer.hashCode());
        final Object $country = this.getCountry();
        result = result * 59 + (($country == null) ? 43 : $country.hashCode());
        final long $currentVersion = Double.doubleToLongBits(this.getCurrentVersion());
        result = result * 59 + (int)($currentVersion >>> 32 ^ $currentVersion);
        final long $newVersion = Double.doubleToLongBits(this.getNewVersion());
        result = result * 59 + (int)($newVersion >>> 32 ^ $newVersion);
        final Object $args = this.getArgs();
        result = result * 59 + (($args == null) ? 43 : $args.hashCode());
        result = result * 59 + (this.isUpdaterLater() ? 79 : 97);
        final Object $requestTime = this.getRequestTime();
        result = result * 59 + (($requestTime == null) ? 43 : $requestTime.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "UpdaterDTO(client=" + this.getClient() + ", offer=" + this.getOffer() + ", country=" + this.getCountry() + ", currentVersion=" + this.getCurrentVersion() + ", newVersion=" + this.getNewVersion() + ", args=" + this.getArgs() + ", updaterLater=" + this.isUpdaterLater() + ", requestTime=" + this.getRequestTime() + ")";
    }
}
