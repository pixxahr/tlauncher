// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.statistics;

public class UniqueClientDTO
{
    private String os;
    private String javaVersion;
    private String resolution;
    private double clientVersion;
    private String osVersion;
    private String uuid;
    private String cpu;
    private String gpu;
    private int ramGpu;
    private int ram;
    
    public String getOs() {
        return this.os;
    }
    
    public String getJavaVersion() {
        return this.javaVersion;
    }
    
    public String getResolution() {
        return this.resolution;
    }
    
    public double getClientVersion() {
        return this.clientVersion;
    }
    
    public String getOsVersion() {
        return this.osVersion;
    }
    
    public String getUuid() {
        return this.uuid;
    }
    
    public String getCpu() {
        return this.cpu;
    }
    
    public String getGpu() {
        return this.gpu;
    }
    
    public int getRamGpu() {
        return this.ramGpu;
    }
    
    public int getRam() {
        return this.ram;
    }
    
    public void setOs(final String os) {
        this.os = os;
    }
    
    public void setJavaVersion(final String javaVersion) {
        this.javaVersion = javaVersion;
    }
    
    public void setResolution(final String resolution) {
        this.resolution = resolution;
    }
    
    public void setClientVersion(final double clientVersion) {
        this.clientVersion = clientVersion;
    }
    
    public void setOsVersion(final String osVersion) {
        this.osVersion = osVersion;
    }
    
    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }
    
    public void setCpu(final String cpu) {
        this.cpu = cpu;
    }
    
    public void setGpu(final String gpu) {
        this.gpu = gpu;
    }
    
    public void setRamGpu(final int ramGpu) {
        this.ramGpu = ramGpu;
    }
    
    public void setRam(final int ram) {
        this.ram = ram;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof UniqueClientDTO)) {
            return false;
        }
        final UniqueClientDTO other = (UniqueClientDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$os = this.getOs();
        final Object other$os = other.getOs();
        Label_0065: {
            if (this$os == null) {
                if (other$os == null) {
                    break Label_0065;
                }
            }
            else if (this$os.equals(other$os)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$javaVersion = this.getJavaVersion();
        final Object other$javaVersion = other.getJavaVersion();
        Label_0102: {
            if (this$javaVersion == null) {
                if (other$javaVersion == null) {
                    break Label_0102;
                }
            }
            else if (this$javaVersion.equals(other$javaVersion)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$resolution = this.getResolution();
        final Object other$resolution = other.getResolution();
        Label_0139: {
            if (this$resolution == null) {
                if (other$resolution == null) {
                    break Label_0139;
                }
            }
            else if (this$resolution.equals(other$resolution)) {
                break Label_0139;
            }
            return false;
        }
        if (Double.compare(this.getClientVersion(), other.getClientVersion()) != 0) {
            return false;
        }
        final Object this$osVersion = this.getOsVersion();
        final Object other$osVersion = other.getOsVersion();
        Label_0192: {
            if (this$osVersion == null) {
                if (other$osVersion == null) {
                    break Label_0192;
                }
            }
            else if (this$osVersion.equals(other$osVersion)) {
                break Label_0192;
            }
            return false;
        }
        final Object this$uuid = this.getUuid();
        final Object other$uuid = other.getUuid();
        Label_0229: {
            if (this$uuid == null) {
                if (other$uuid == null) {
                    break Label_0229;
                }
            }
            else if (this$uuid.equals(other$uuid)) {
                break Label_0229;
            }
            return false;
        }
        final Object this$cpu = this.getCpu();
        final Object other$cpu = other.getCpu();
        Label_0266: {
            if (this$cpu == null) {
                if (other$cpu == null) {
                    break Label_0266;
                }
            }
            else if (this$cpu.equals(other$cpu)) {
                break Label_0266;
            }
            return false;
        }
        final Object this$gpu = this.getGpu();
        final Object other$gpu = other.getGpu();
        if (this$gpu == null) {
            if (other$gpu == null) {
                return this.getRamGpu() == other.getRamGpu() && this.getRam() == other.getRam();
            }
        }
        else if (this$gpu.equals(other$gpu)) {
            return this.getRamGpu() == other.getRamGpu() && this.getRam() == other.getRam();
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof UniqueClientDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $os = this.getOs();
        result = result * 59 + (($os == null) ? 43 : $os.hashCode());
        final Object $javaVersion = this.getJavaVersion();
        result = result * 59 + (($javaVersion == null) ? 43 : $javaVersion.hashCode());
        final Object $resolution = this.getResolution();
        result = result * 59 + (($resolution == null) ? 43 : $resolution.hashCode());
        final long $clientVersion = Double.doubleToLongBits(this.getClientVersion());
        result = result * 59 + (int)($clientVersion >>> 32 ^ $clientVersion);
        final Object $osVersion = this.getOsVersion();
        result = result * 59 + (($osVersion == null) ? 43 : $osVersion.hashCode());
        final Object $uuid = this.getUuid();
        result = result * 59 + (($uuid == null) ? 43 : $uuid.hashCode());
        final Object $cpu = this.getCpu();
        result = result * 59 + (($cpu == null) ? 43 : $cpu.hashCode());
        final Object $gpu = this.getGpu();
        result = result * 59 + (($gpu == null) ? 43 : $gpu.hashCode());
        result = result * 59 + this.getRamGpu();
        result = result * 59 + this.getRam();
        return result;
    }
    
    @Override
    public String toString() {
        return "UniqueClientDTO(os=" + this.getOs() + ", javaVersion=" + this.getJavaVersion() + ", resolution=" + this.getResolution() + ", clientVersion=" + this.getClientVersion() + ", osVersion=" + this.getOsVersion() + ", uuid=" + this.getUuid() + ", cpu=" + this.getCpu() + ", gpu=" + this.getGpu() + ", ramGpu=" + this.getRamGpu() + ", ram=" + this.getRam() + ")";
    }
}
