// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.statistics;

public class InstallVersionDTO
{
    private String installVersion;
    
    public String getInstallVersion() {
        return this.installVersion;
    }
    
    public void setInstallVersion(final String installVersion) {
        this.installVersion = installVersion;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof InstallVersionDTO)) {
            return false;
        }
        final InstallVersionDTO other = (InstallVersionDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$installVersion = this.getInstallVersion();
        final Object other$installVersion = other.getInstallVersion();
        if (this$installVersion == null) {
            if (other$installVersion == null) {
                return true;
            }
        }
        else if (this$installVersion.equals(other$installVersion)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof InstallVersionDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $installVersion = this.getInstallVersion();
        result = result * 59 + (($installVersion == null) ? 43 : $installVersion.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "InstallVersionDTO(installVersion=" + this.getInstallVersion() + ")";
    }
}
