// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client;

import java.util.HashSet;
import java.util.Set;

public class ModDTO extends SubModpackDTO
{
    public static final Long OPTIFINE_ID;
    public static final Long TL_SKIN_ID;
    public static final Long TL_SKIN_CAPE_ID;
    public static final Set<Long> SKIN_MODS;
    
    @Override
    public String toString() {
        return "ModDTO(super=" + super.toString() + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ModDTO)) {
            return false;
        }
        final ModDTO other = (ModDTO)o;
        return other.canEqual(this) && super.equals(o);
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof ModDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * 59 + super.hashCode();
        return result;
    }
    
    static {
        OPTIFINE_ID = 9999999L;
        TL_SKIN_ID = 10000014L;
        TL_SKIN_CAPE_ID = 10000020L;
        SKIN_MODS = new HashSet<Long>() {
            {
                this.add(ModDTO.TL_SKIN_ID);
                this.add(ModDTO.TL_SKIN_CAPE_ID);
            }
        };
    }
}
