// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client;

import java.beans.ConstructorProperties;
import org.tlauncher.modpack.domain.client.share.DependencyType;
import org.tlauncher.modpack.domain.client.share.GameType;

public class GameEntityDependencyDTO
{
    private GameType gameType;
    private Long id;
    private DependencyType dependencyType;
    
    public GameType getGameType() {
        return this.gameType;
    }
    
    public Long getId() {
        return this.id;
    }
    
    public DependencyType getDependencyType() {
        return this.dependencyType;
    }
    
    public void setGameType(final GameType gameType) {
        this.gameType = gameType;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public void setDependencyType(final DependencyType dependencyType) {
        this.dependencyType = dependencyType;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof GameEntityDependencyDTO)) {
            return false;
        }
        final GameEntityDependencyDTO other = (GameEntityDependencyDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$gameType = this.getGameType();
        final Object other$gameType = other.getGameType();
        Label_0065: {
            if (this$gameType == null) {
                if (other$gameType == null) {
                    break Label_0065;
                }
            }
            else if (this$gameType.equals(other$gameType)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        Label_0102: {
            if (this$id == null) {
                if (other$id == null) {
                    break Label_0102;
                }
            }
            else if (this$id.equals(other$id)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$dependencyType = this.getDependencyType();
        final Object other$dependencyType = other.getDependencyType();
        if (this$dependencyType == null) {
            if (other$dependencyType == null) {
                return true;
            }
        }
        else if (this$dependencyType.equals(other$dependencyType)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof GameEntityDependencyDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $gameType = this.getGameType();
        result = result * 59 + (($gameType == null) ? 43 : $gameType.hashCode());
        final Object $id = this.getId();
        result = result * 59 + (($id == null) ? 43 : $id.hashCode());
        final Object $dependencyType = this.getDependencyType();
        result = result * 59 + (($dependencyType == null) ? 43 : $dependencyType.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "GameEntityDependencyDTO(gameType=" + this.getGameType() + ", id=" + this.getId() + ", dependencyType=" + this.getDependencyType() + ")";
    }
    
    @ConstructorProperties({ "gameType", "id", "dependencyType" })
    public GameEntityDependencyDTO(final GameType gameType, final Long id, final DependencyType dependencyType) {
        this.gameType = gameType;
        this.id = id;
        this.dependencyType = dependencyType;
    }
}
