// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.core.JsonGenerator;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.DateFormat;
import org.springframework.stereotype.Component;
import java.util.Date;
import com.fasterxml.jackson.databind.JsonSerializer;

@Component
public class JsonDateSerializer extends JsonSerializer<Date>
{
    private final DateFormat enUsFormat;
    private final DateFormat iso8601Format;
    
    public JsonDateSerializer() {
        this.enUsFormat = DateFormat.getDateTimeInstance(2, 2, Locale.US);
        this.iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    }
    
    public void serialize(final Date value, final JsonGenerator gen, final SerializerProvider serializers) throws IOException {
        gen.writeString(this.toString(value));
    }
    
    public String toString(final Date date) {
        synchronized (this.enUsFormat) {
            final String result = this.iso8601Format.format(date);
            return result.substring(0, 22) + ":" + result.substring(22);
        }
    }
}
