// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.core.JsonParser;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.DateFormat;
import org.springframework.stereotype.Component;
import java.util.Date;
import com.fasterxml.jackson.databind.JsonDeserializer;

@Component
public class JsonDateDeserializer extends JsonDeserializer<Date>
{
    private final DateFormat enUsFormat;
    private final DateFormat iso8601Format;
    
    public JsonDateDeserializer() {
        this.enUsFormat = DateFormat.getDateTimeInstance(2, 2, Locale.US);
        this.iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    }
    
    public Date deserialize(final JsonParser p, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return this.toDate(p.getText().toString());
    }
    
    public Date toDate(final String string) {
        synchronized (this.enUsFormat) {
            try {
                return this.enUsFormat.parse(string);
            }
            catch (Exception ex) {
                try {
                    return this.iso8601Format.parse(string);
                }
                catch (Exception ex2) {
                    try {
                        String cleaned = string.replace("Z", "+00:00");
                        cleaned = cleaned.substring(0, 22) + cleaned.substring(23);
                        // monitorexit(this.enUsFormat)
                        return this.iso8601Format.parse(cleaned);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    // monitorexit(this.enUsFormat)
                }
            }
        }
        throw new IllegalArgumentException(this.getClass() + " cannot deserialize " + string);
    }
}
