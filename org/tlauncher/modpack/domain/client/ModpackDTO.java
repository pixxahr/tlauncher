// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client;

import java.util.ArrayList;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import java.util.List;
import java.util.Collection;
import org.tlauncher.modpack.domain.client.share.Category;

public class ModpackDTO extends GameEntityDTO
{
    private boolean modpackMemory;
    private int memory;
    
    public void copy(final ModpackDTO c) {
        c.setId(this.getId());
        c.setAuthor(this.getAuthor());
        c.setCategories(new CheckNullList<Category>(this.getCategories()));
        c.setDescription(this.getDescription());
        c.setDownloadALL(this.getDownloadALL());
        c.setDownloadMonth(this.getDownloadMonth());
        c.setMemory(this.memory);
        c.setModpackMemory(this.modpackMemory);
        c.setOfficialSite(this.getOfficialSite());
        c.setName(this.getName());
        c.setPicture(this.getPicture());
        c.setPictures(new CheckNullList<Integer>(this.getPictures()));
        c.setPopulateStatus(this.isPopulateStatus());
        c.setTags(new CheckNullList<TagDTO>(this.getTags()));
        c.setUserInstall(this.isUserInstall());
        final ModpackVersionDTO version = (ModpackVersionDTO)this.getVersion();
        final ModpackVersionDTO modpackVersionDTO = new ModpackVersionDTO();
        version.copy(modpackVersionDTO);
        c.setVersion(modpackVersionDTO);
        c.setVersions(new CheckNullList<VersionDTO>(this.getVersions()));
    }
    
    public boolean isModpackMemory() {
        return this.modpackMemory;
    }
    
    public int getMemory() {
        return this.memory;
    }
    
    public void setModpackMemory(final boolean modpackMemory) {
        this.modpackMemory = modpackMemory;
    }
    
    public void setMemory(final int memory) {
        this.memory = memory;
    }
    
    @Override
    public String toString() {
        return "ModpackDTO(super=" + super.toString() + ", modpackMemory=" + this.isModpackMemory() + ", memory=" + this.getMemory() + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ModpackDTO)) {
            return false;
        }
        final ModpackDTO other = (ModpackDTO)o;
        return other.canEqual(this) && super.equals(o) && this.isModpackMemory() == other.isModpackMemory() && this.getMemory() == other.getMemory();
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof ModpackDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * 59 + super.hashCode();
        result = result * 59 + (this.isModpackMemory() ? 79 : 97);
        result = result * 59 + this.getMemory();
        return result;
    }
    
    public static class CheckNullList<E> extends ArrayList<E>
    {
        public CheckNullList(final Collection<? extends E> c) {
            super((Collection)((c == null) ? new ArrayList<E>() : c));
        }
    }
}
