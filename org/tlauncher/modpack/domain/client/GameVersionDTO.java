// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client;

import java.util.List;

public class GameVersionDTO
{
    private String name;
    List<String> forgeVersions;
    
    public String getName() {
        return this.name;
    }
    
    public List<String> getForgeVersions() {
        return this.forgeVersions;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setForgeVersions(final List<String> forgeVersions) {
        this.forgeVersions = forgeVersions;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof GameVersionDTO)) {
            return false;
        }
        final GameVersionDTO other = (GameVersionDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        Label_0065: {
            if (this$name == null) {
                if (other$name == null) {
                    break Label_0065;
                }
            }
            else if (this$name.equals(other$name)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$forgeVersions = this.getForgeVersions();
        final Object other$forgeVersions = other.getForgeVersions();
        if (this$forgeVersions == null) {
            if (other$forgeVersions == null) {
                return true;
            }
        }
        else if (this$forgeVersions.equals(other$forgeVersions)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof GameVersionDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $name = this.getName();
        result = result * 59 + (($name == null) ? 43 : $name.hashCode());
        final Object $forgeVersions = this.getForgeVersions();
        result = result * 59 + (($forgeVersions == null) ? 43 : $forgeVersions.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "GameVersionDTO(name=" + this.getName() + ", forgeVersions=" + this.getForgeVersions() + ")";
    }
}
