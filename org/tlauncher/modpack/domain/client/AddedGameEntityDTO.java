// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client;

public class AddedGameEntityDTO
{
    private String url;
    
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(final String url) {
        this.url = url;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AddedGameEntityDTO)) {
            return false;
        }
        final AddedGameEntityDTO other = (AddedGameEntityDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$url = this.getUrl();
        final Object other$url = other.getUrl();
        if (this$url == null) {
            if (other$url == null) {
                return true;
            }
        }
        else if (this$url.equals(other$url)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof AddedGameEntityDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $url = this.getUrl();
        result = result * 59 + (($url == null) ? 43 : $url.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "AddedGameEntityDTO(url=" + this.getUrl() + ")";
    }
}
