// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.version;

import org.tlauncher.modpack.domain.client.share.JavaEnum;
import java.util.List;
import java.util.Set;
import org.tlauncher.modpack.domain.client.share.Type;

public class VersionDTO
{
    private Long id;
    private String name;
    private Long updateDate;
    private Type type;
    private Set<String> gameVersions;
    private List<JavaEnum> javaVersions;
    private MetadataDTO metadata;
    
    public Long getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public Long getUpdateDate() {
        return this.updateDate;
    }
    
    public Type getType() {
        return this.type;
    }
    
    public Set<String> getGameVersions() {
        return this.gameVersions;
    }
    
    public List<JavaEnum> getJavaVersions() {
        return this.javaVersions;
    }
    
    public MetadataDTO getMetadata() {
        return this.metadata;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setUpdateDate(final Long updateDate) {
        this.updateDate = updateDate;
    }
    
    public void setType(final Type type) {
        this.type = type;
    }
    
    public void setGameVersions(final Set<String> gameVersions) {
        this.gameVersions = gameVersions;
    }
    
    public void setJavaVersions(final List<JavaEnum> javaVersions) {
        this.javaVersions = javaVersions;
    }
    
    public void setMetadata(final MetadataDTO metadata) {
        this.metadata = metadata;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VersionDTO)) {
            return false;
        }
        final VersionDTO other = (VersionDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        Label_0065: {
            if (this$id == null) {
                if (other$id == null) {
                    break Label_0065;
                }
            }
            else if (this$id.equals(other$id)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        Label_0102: {
            if (this$name == null) {
                if (other$name == null) {
                    break Label_0102;
                }
            }
            else if (this$name.equals(other$name)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$updateDate = this.getUpdateDate();
        final Object other$updateDate = other.getUpdateDate();
        Label_0139: {
            if (this$updateDate == null) {
                if (other$updateDate == null) {
                    break Label_0139;
                }
            }
            else if (this$updateDate.equals(other$updateDate)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$type = this.getType();
        final Object other$type = other.getType();
        Label_0176: {
            if (this$type == null) {
                if (other$type == null) {
                    break Label_0176;
                }
            }
            else if (this$type.equals(other$type)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$gameVersions = this.getGameVersions();
        final Object other$gameVersions = other.getGameVersions();
        Label_0213: {
            if (this$gameVersions == null) {
                if (other$gameVersions == null) {
                    break Label_0213;
                }
            }
            else if (this$gameVersions.equals(other$gameVersions)) {
                break Label_0213;
            }
            return false;
        }
        final Object this$javaVersions = this.getJavaVersions();
        final Object other$javaVersions = other.getJavaVersions();
        Label_0250: {
            if (this$javaVersions == null) {
                if (other$javaVersions == null) {
                    break Label_0250;
                }
            }
            else if (this$javaVersions.equals(other$javaVersions)) {
                break Label_0250;
            }
            return false;
        }
        final Object this$metadata = this.getMetadata();
        final Object other$metadata = other.getMetadata();
        if (this$metadata == null) {
            if (other$metadata == null) {
                return true;
            }
        }
        else if (this$metadata.equals(other$metadata)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof VersionDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * 59 + (($id == null) ? 43 : $id.hashCode());
        final Object $name = this.getName();
        result = result * 59 + (($name == null) ? 43 : $name.hashCode());
        final Object $updateDate = this.getUpdateDate();
        result = result * 59 + (($updateDate == null) ? 43 : $updateDate.hashCode());
        final Object $type = this.getType();
        result = result * 59 + (($type == null) ? 43 : $type.hashCode());
        final Object $gameVersions = this.getGameVersions();
        result = result * 59 + (($gameVersions == null) ? 43 : $gameVersions.hashCode());
        final Object $javaVersions = this.getJavaVersions();
        result = result * 59 + (($javaVersions == null) ? 43 : $javaVersions.hashCode());
        final Object $metadata = this.getMetadata();
        result = result * 59 + (($metadata == null) ? 43 : $metadata.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "VersionDTO(id=" + this.getId() + ", name=" + this.getName() + ", updateDate=" + this.getUpdateDate() + ", type=" + this.getType() + ", gameVersions=" + this.getGameVersions() + ", javaVersions=" + this.getJavaVersions() + ", metadata=" + this.getMetadata() + ")";
    }
}
