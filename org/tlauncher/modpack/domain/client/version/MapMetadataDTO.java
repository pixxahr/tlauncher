// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.version;

import java.util.List;

public class MapMetadataDTO extends MetadataDTO
{
    private List<String> folders;
    
    public List<String> getFolders() {
        return this.folders;
    }
    
    public void setFolders(final List<String> folders) {
        this.folders = folders;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof MapMetadataDTO)) {
            return false;
        }
        final MapMetadataDTO other = (MapMetadataDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$folders = this.getFolders();
        final Object other$folders = other.getFolders();
        if (this$folders == null) {
            if (other$folders == null) {
                return true;
            }
        }
        else if (this$folders.equals(other$folders)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof MapMetadataDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $folders = this.getFolders();
        result = result * 59 + (($folders == null) ? 43 : $folders.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "MapMetadataDTO(folders=" + this.getFolders() + ")";
    }
}
