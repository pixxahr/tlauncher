// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.version;

import java.util.List;

public class ModVersionDTO extends VersionDTO
{
    private String downForge;
    private String upForge;
    private List<String> incompatibleMods;
    
    public String getDownForge() {
        return this.downForge;
    }
    
    public String getUpForge() {
        return this.upForge;
    }
    
    public List<String> getIncompatibleMods() {
        return this.incompatibleMods;
    }
    
    public void setDownForge(final String downForge) {
        this.downForge = downForge;
    }
    
    public void setUpForge(final String upForge) {
        this.upForge = upForge;
    }
    
    public void setIncompatibleMods(final List<String> incompatibleMods) {
        this.incompatibleMods = incompatibleMods;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ModVersionDTO)) {
            return false;
        }
        final ModVersionDTO other = (ModVersionDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$downForge = this.getDownForge();
        final Object other$downForge = other.getDownForge();
        Label_0065: {
            if (this$downForge == null) {
                if (other$downForge == null) {
                    break Label_0065;
                }
            }
            else if (this$downForge.equals(other$downForge)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$upForge = this.getUpForge();
        final Object other$upForge = other.getUpForge();
        Label_0102: {
            if (this$upForge == null) {
                if (other$upForge == null) {
                    break Label_0102;
                }
            }
            else if (this$upForge.equals(other$upForge)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$incompatibleMods = this.getIncompatibleMods();
        final Object other$incompatibleMods = other.getIncompatibleMods();
        if (this$incompatibleMods == null) {
            if (other$incompatibleMods == null) {
                return true;
            }
        }
        else if (this$incompatibleMods.equals(other$incompatibleMods)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof ModVersionDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $downForge = this.getDownForge();
        result = result * 59 + (($downForge == null) ? 43 : $downForge.hashCode());
        final Object $upForge = this.getUpForge();
        result = result * 59 + (($upForge == null) ? 43 : $upForge.hashCode());
        final Object $incompatibleMods = this.getIncompatibleMods();
        result = result * 59 + (($incompatibleMods == null) ? 43 : $incompatibleMods.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "ModVersionDTO(downForge=" + this.getDownForge() + ", upForge=" + this.getUpForge() + ", incompatibleMods=" + this.getIncompatibleMods() + ")";
    }
}
