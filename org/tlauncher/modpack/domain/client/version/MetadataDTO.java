// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.version;

public class MetadataDTO
{
    protected String sha1;
    protected long size;
    protected String path;
    protected String url;
    
    public String getSha1() {
        return this.sha1;
    }
    
    public long getSize() {
        return this.size;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public String getUrl() {
        return this.url;
    }
    
    public void setSha1(final String sha1) {
        this.sha1 = sha1;
    }
    
    public void setSize(final long size) {
        this.size = size;
    }
    
    public void setPath(final String path) {
        this.path = path;
    }
    
    public void setUrl(final String url) {
        this.url = url;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof MetadataDTO)) {
            return false;
        }
        final MetadataDTO other = (MetadataDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$sha1 = this.getSha1();
        final Object other$sha1 = other.getSha1();
        Label_0065: {
            if (this$sha1 == null) {
                if (other$sha1 == null) {
                    break Label_0065;
                }
            }
            else if (this$sha1.equals(other$sha1)) {
                break Label_0065;
            }
            return false;
        }
        if (this.getSize() != other.getSize()) {
            return false;
        }
        final Object this$path = this.getPath();
        final Object other$path = other.getPath();
        Label_0116: {
            if (this$path == null) {
                if (other$path == null) {
                    break Label_0116;
                }
            }
            else if (this$path.equals(other$path)) {
                break Label_0116;
            }
            return false;
        }
        final Object this$url = this.getUrl();
        final Object other$url = other.getUrl();
        if (this$url == null) {
            if (other$url == null) {
                return true;
            }
        }
        else if (this$url.equals(other$url)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof MetadataDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $sha1 = this.getSha1();
        result = result * 59 + (($sha1 == null) ? 43 : $sha1.hashCode());
        final long $size = this.getSize();
        result = result * 59 + (int)($size >>> 32 ^ $size);
        final Object $path = this.getPath();
        result = result * 59 + (($path == null) ? 43 : $path.hashCode());
        final Object $url = this.getUrl();
        result = result * 59 + (($url == null) ? 43 : $url.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "MetadataDTO(sha1=" + this.getSha1() + ", size=" + this.getSize() + ", path=" + this.getPath() + ", url=" + this.getUrl() + ")";
    }
}
