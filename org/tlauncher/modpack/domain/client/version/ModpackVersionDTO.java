// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.version;

import java.util.Collection;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import java.util.ArrayList;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import org.tlauncher.modpack.domain.client.MapDTO;
import org.tlauncher.modpack.domain.client.ResourcePackDTO;
import org.tlauncher.modpack.domain.client.ModDTO;
import java.util.List;

public class ModpackVersionDTO extends VersionDTO
{
    private List<ModDTO> mods;
    private List<ResourcePackDTO> resourcePacks;
    private List<MapDTO> maps;
    private MetadataDTO additionalFile;
    private String forgeVersion;
    private String gameVersion;
    
    public List<? extends GameEntityDTO> getByType(final GameType type) {
        List<? extends GameEntityDTO> list = new ArrayList<GameEntityDTO>();
        switch (type) {
            case MOD: {
                list = this.mods;
                break;
            }
            case MAP: {
                list = this.maps;
                break;
            }
            case RESOURCEPACK: {
                list = this.resourcePacks;
                break;
            }
        }
        return list;
    }
    
    public ModpackVersionDTO copy(final ModpackVersionDTO version) {
        version.setId(this.getId());
        version.setAdditionalFile(this.additionalFile);
        version.setForgeVersion(this.forgeVersion);
        version.setGameVersions(this.getGameVersions());
        version.setMaps(new ModpackDTO.CheckNullList<MapDTO>(this.maps));
        version.setMods(new ModpackDTO.CheckNullList<ModDTO>(this.mods));
        version.setName(this.getName());
        version.setResourcePacks(new ModpackDTO.CheckNullList<ResourcePackDTO>(this.resourcePacks));
        version.setType(this.getType());
        version.setUpdateDate(this.getUpdateDate());
        version.setJavaVersions(this.getJavaVersions());
        version.setGameVersion(this.getGameVersion());
        return version;
    }
    
    public ModpackVersionDTO() {
        this.mods = new ArrayList<ModDTO>();
        this.resourcePacks = new ArrayList<ResourcePackDTO>();
        this.maps = new ArrayList<MapDTO>();
    }
    
    public List<ModDTO> getMods() {
        return this.mods;
    }
    
    public List<ResourcePackDTO> getResourcePacks() {
        return this.resourcePacks;
    }
    
    public List<MapDTO> getMaps() {
        return this.maps;
    }
    
    public MetadataDTO getAdditionalFile() {
        return this.additionalFile;
    }
    
    public String getForgeVersion() {
        return this.forgeVersion;
    }
    
    public String getGameVersion() {
        return this.gameVersion;
    }
    
    public void setMods(final List<ModDTO> mods) {
        this.mods = mods;
    }
    
    public void setResourcePacks(final List<ResourcePackDTO> resourcePacks) {
        this.resourcePacks = resourcePacks;
    }
    
    public void setMaps(final List<MapDTO> maps) {
        this.maps = maps;
    }
    
    public void setAdditionalFile(final MetadataDTO additionalFile) {
        this.additionalFile = additionalFile;
    }
    
    public void setForgeVersion(final String forgeVersion) {
        this.forgeVersion = forgeVersion;
    }
    
    public void setGameVersion(final String gameVersion) {
        this.gameVersion = gameVersion;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ModpackVersionDTO)) {
            return false;
        }
        final ModpackVersionDTO other = (ModpackVersionDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$mods = this.getMods();
        final Object other$mods = other.getMods();
        Label_0065: {
            if (this$mods == null) {
                if (other$mods == null) {
                    break Label_0065;
                }
            }
            else if (this$mods.equals(other$mods)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$resourcePacks = this.getResourcePacks();
        final Object other$resourcePacks = other.getResourcePacks();
        Label_0102: {
            if (this$resourcePacks == null) {
                if (other$resourcePacks == null) {
                    break Label_0102;
                }
            }
            else if (this$resourcePacks.equals(other$resourcePacks)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$maps = this.getMaps();
        final Object other$maps = other.getMaps();
        Label_0139: {
            if (this$maps == null) {
                if (other$maps == null) {
                    break Label_0139;
                }
            }
            else if (this$maps.equals(other$maps)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$additionalFile = this.getAdditionalFile();
        final Object other$additionalFile = other.getAdditionalFile();
        Label_0176: {
            if (this$additionalFile == null) {
                if (other$additionalFile == null) {
                    break Label_0176;
                }
            }
            else if (this$additionalFile.equals(other$additionalFile)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$forgeVersion = this.getForgeVersion();
        final Object other$forgeVersion = other.getForgeVersion();
        Label_0213: {
            if (this$forgeVersion == null) {
                if (other$forgeVersion == null) {
                    break Label_0213;
                }
            }
            else if (this$forgeVersion.equals(other$forgeVersion)) {
                break Label_0213;
            }
            return false;
        }
        final Object this$gameVersion = this.getGameVersion();
        final Object other$gameVersion = other.getGameVersion();
        if (this$gameVersion == null) {
            if (other$gameVersion == null) {
                return true;
            }
        }
        else if (this$gameVersion.equals(other$gameVersion)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof ModpackVersionDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $mods = this.getMods();
        result = result * 59 + (($mods == null) ? 43 : $mods.hashCode());
        final Object $resourcePacks = this.getResourcePacks();
        result = result * 59 + (($resourcePacks == null) ? 43 : $resourcePacks.hashCode());
        final Object $maps = this.getMaps();
        result = result * 59 + (($maps == null) ? 43 : $maps.hashCode());
        final Object $additionalFile = this.getAdditionalFile();
        result = result * 59 + (($additionalFile == null) ? 43 : $additionalFile.hashCode());
        final Object $forgeVersion = this.getForgeVersion();
        result = result * 59 + (($forgeVersion == null) ? 43 : $forgeVersion.hashCode());
        final Object $gameVersion = this.getGameVersion();
        result = result * 59 + (($gameVersion == null) ? 43 : $gameVersion.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "ModpackVersionDTO(mods=" + this.getMods() + ", resourcePacks=" + this.getResourcePacks() + ", maps=" + this.getMaps() + ", additionalFile=" + this.getAdditionalFile() + ", forgeVersion=" + this.getForgeVersion() + ", gameVersion=" + this.getGameVersion() + ")";
    }
}
