// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client;

public class MapDTO extends SubModpackDTO
{
    @Override
    public String toString() {
        return "MapDTO(super=" + super.toString() + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof MapDTO)) {
            return false;
        }
        final MapDTO other = (MapDTO)o;
        return other.canEqual(this) && super.equals(o);
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof MapDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * 59 + super.hashCode();
        return result;
    }
}
