// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client;

import org.tlauncher.modpack.domain.client.share.StateGameElement;

public class SubModpackDTO extends GameEntityDTO
{
    private StateGameElement stateGameElement;
    
    public SubModpackDTO() {
        this.stateGameElement = StateGameElement.ACTIVE;
    }
    
    public StateGameElement getStateGameElement() {
        return this.stateGameElement;
    }
    
    public void setStateGameElement(final StateGameElement stateGameElement) {
        this.stateGameElement = stateGameElement;
    }
    
    @Override
    public String toString() {
        return "SubModpackDTO(super=" + super.toString() + ", stateGameElement=" + this.getStateGameElement() + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof SubModpackDTO)) {
            return false;
        }
        final SubModpackDTO other = (SubModpackDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final Object this$stateGameElement = this.getStateGameElement();
        final Object other$stateGameElement = other.getStateGameElement();
        if (this$stateGameElement == null) {
            if (other$stateGameElement == null) {
                return true;
            }
        }
        else if (this$stateGameElement.equals(other$stateGameElement)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof SubModpackDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * 59 + super.hashCode();
        final Object $stateGameElement = this.getStateGameElement();
        result = result * 59 + (($stateGameElement == null) ? 43 : $stateGameElement.hashCode());
        return result;
    }
}
