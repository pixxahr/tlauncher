// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.share;

import org.tlauncher.modpack.domain.client.GameEntityDTO;
import java.util.ArrayList;
import org.tlauncher.modpack.domain.client.MapDTO;
import org.tlauncher.modpack.domain.client.ResourcePackDTO;
import org.tlauncher.modpack.domain.client.ModDTO;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.modpack.domain.client.GameVersionDTO;
import java.util.List;

public class InfoMod
{
    List<GameVersionDTO> gameVersions;
    List<ModpackDTO> modPacks;
    List<ModDTO> mods;
    List<ResourcePackDTO> resourcePacks;
    List<MapDTO> maps;
    
    public InfoMod() {
        this.gameVersions = new ArrayList<GameVersionDTO>();
        this.modPacks = new ArrayList<ModpackDTO>();
        this.mods = new ArrayList<ModDTO>();
        this.resourcePacks = new ArrayList<ResourcePackDTO>();
        this.maps = new ArrayList<MapDTO>();
    }
    
    public List<? extends GameEntityDTO> getByType(final GameType type) {
        switch (type) {
            case MOD: {
                return this.mods;
            }
            case MAP: {
                return this.maps;
            }
            case RESOURCEPACK: {
                return this.resourcePacks;
            }
            case MODPACK: {
                return this.modPacks;
            }
            default: {
                return null;
            }
        }
    }
    
    public List<GameVersionDTO> getGameVersions() {
        return this.gameVersions;
    }
    
    public List<ModpackDTO> getModPacks() {
        return this.modPacks;
    }
    
    public List<ModDTO> getMods() {
        return this.mods;
    }
    
    public List<ResourcePackDTO> getResourcePacks() {
        return this.resourcePacks;
    }
    
    public List<MapDTO> getMaps() {
        return this.maps;
    }
    
    public void setGameVersions(final List<GameVersionDTO> gameVersions) {
        this.gameVersions = gameVersions;
    }
    
    public void setModPacks(final List<ModpackDTO> modPacks) {
        this.modPacks = modPacks;
    }
    
    public void setMods(final List<ModDTO> mods) {
        this.mods = mods;
    }
    
    public void setResourcePacks(final List<ResourcePackDTO> resourcePacks) {
        this.resourcePacks = resourcePacks;
    }
    
    public void setMaps(final List<MapDTO> maps) {
        this.maps = maps;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof InfoMod)) {
            return false;
        }
        final InfoMod other = (InfoMod)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$gameVersions = this.getGameVersions();
        final Object other$gameVersions = other.getGameVersions();
        Label_0065: {
            if (this$gameVersions == null) {
                if (other$gameVersions == null) {
                    break Label_0065;
                }
            }
            else if (this$gameVersions.equals(other$gameVersions)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$modPacks = this.getModPacks();
        final Object other$modPacks = other.getModPacks();
        Label_0102: {
            if (this$modPacks == null) {
                if (other$modPacks == null) {
                    break Label_0102;
                }
            }
            else if (this$modPacks.equals(other$modPacks)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$mods = this.getMods();
        final Object other$mods = other.getMods();
        Label_0139: {
            if (this$mods == null) {
                if (other$mods == null) {
                    break Label_0139;
                }
            }
            else if (this$mods.equals(other$mods)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$resourcePacks = this.getResourcePacks();
        final Object other$resourcePacks = other.getResourcePacks();
        Label_0176: {
            if (this$resourcePacks == null) {
                if (other$resourcePacks == null) {
                    break Label_0176;
                }
            }
            else if (this$resourcePacks.equals(other$resourcePacks)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$maps = this.getMaps();
        final Object other$maps = other.getMaps();
        if (this$maps == null) {
            if (other$maps == null) {
                return true;
            }
        }
        else if (this$maps.equals(other$maps)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof InfoMod;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $gameVersions = this.getGameVersions();
        result = result * 59 + (($gameVersions == null) ? 43 : $gameVersions.hashCode());
        final Object $modPacks = this.getModPacks();
        result = result * 59 + (($modPacks == null) ? 43 : $modPacks.hashCode());
        final Object $mods = this.getMods();
        result = result * 59 + (($mods == null) ? 43 : $mods.hashCode());
        final Object $resourcePacks = this.getResourcePacks();
        result = result * 59 + (($resourcePacks == null) ? 43 : $resourcePacks.hashCode());
        final Object $maps = this.getMaps();
        result = result * 59 + (($maps == null) ? 43 : $maps.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "InfoMod(gameVersions=" + this.getGameVersions() + ", modPacks=" + this.getModPacks() + ", mods=" + this.getMods() + ", resourcePacks=" + this.getResourcePacks() + ", maps=" + this.getMaps() + ")";
    }
}
