// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.share;

import java.util.ArrayList;
import java.util.List;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import org.tlauncher.modpack.domain.client.ResourcePackDTO;
import org.tlauncher.modpack.domain.client.ModDTO;
import org.tlauncher.modpack.domain.client.MapDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum GameType
{
    MAP, 
    MOD, 
    MODPACK, 
    RESOURCEPACK, 
    NOT_MODPACK;
    
    @JsonValue
    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
    
    @JsonCreator
    public static GameType create(final String value) {
        return valueOf(value.toUpperCase());
    }
    
    public static Class<? extends GameEntityDTO> createDTO(final GameType type) {
        switch (type) {
            case MAP: {
                return MapDTO.class;
            }
            case MOD: {
                return ModDTO.class;
            }
            case RESOURCEPACK: {
                return ResourcePackDTO.class;
            }
            case MODPACK: {
                return ModpackDTO.class;
            }
            default: {
                throw new NullPointerException();
            }
        }
    }
    
    public static GameType create(final Class<? extends GameEntityDTO> c) {
        return create(c.getSimpleName().replaceAll("DTO", ""));
    }
    
    public static List<GameType> getSubEntities() {
        return new ArrayList<GameType>() {
            {
                this.add(GameType.MAP);
                this.add(GameType.MOD);
                this.add(GameType.RESOURCEPACK);
            }
        };
    }
}
