// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.share;

import java.util.Comparator;

public class ForgeStringComparator implements Comparator<String>
{
    @Override
    public int compare(final String o2, final String o1) {
        final String[] versions1 = o1.split("\\.");
        final String[] versions2 = o2.split("\\.");
        final int length = Math.min(versions1.length, versions2.length);
        for (int i = 0; i < length; ++i) {
            final int res = Integer.valueOf(versions1[i]).compareTo(Integer.valueOf(versions2[i]));
            if (res != 0) {
                return res;
            }
        }
        if (versions1.length == versions2.length) {
            return 0;
        }
        if (length == versions1.length) {
            return -1;
        }
        return 1;
    }
}
