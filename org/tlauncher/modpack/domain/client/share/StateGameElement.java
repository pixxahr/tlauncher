// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.share;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum StateGameElement
{
    NO_ACTIVE, 
    ACTIVE, 
    BLOCK;
    
    @JsonCreator
    public static StateGameElement createCategory(final String value) {
        return valueOf(value.toUpperCase());
    }
    
    @JsonValue
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
