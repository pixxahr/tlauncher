// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.share;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum JavaEnum
{
    JAVA_10, 
    JAVA_11, 
    JAVA_9, 
    JAVA_8, 
    JAVA_7, 
    JAVA_6;
    
    @JsonCreator
    public static JavaEnum create(final String value) {
        return valueOf(value.toUpperCase());
    }
    
    @JsonValue
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
