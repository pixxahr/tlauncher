// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client.share;

import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Category
{
    ALL, 
    MAGIC, 
    ADVENTURE_RPG, 
    COSMETIC, 
    ARMOR_WEAPONS_TOOLS, 
    TECHNOLOGY, 
    MAP_INFORMATION, 
    LIBRARY_API, 
    CHITA, 
    ADDONS_FORESTRY, 
    TECHNOLOGY_FARMING, 
    ADDONS_THERMALEXPANSION, 
    TECHNOLOGY_ENERGY, 
    WORLD_MOBS, 
    WORLD_BIOMES, 
    SERVER_UTILITY, 
    BLOOD_MAGIC, 
    WORLD_DIMENSIONS, 
    ADDONS_BUILDCRAFT, 
    APPLIED_ENERGISTICS_2, 
    TECHNOLOGY_ITEM_FLUID_ENERGY_TRANSPORT, 
    TECHNOLOGY_PLAYER_TRANSPORT, 
    MC_MISCELLANEOUS, 
    WORLD_ORES_RESOURCES, 
    MC_FOOD, 
    REDSTONE, 
    MC_ADDONS, 
    WORLD_STRUCTURES, 
    ADDONS_INDUSTRIALCRAFT, 
    WORLD_GEN, 
    TECHNOLOGY_PROCESSING, 
    ADDONS_THAUMCRAFT, 
    ADDONS_TINKERS_CONSTRUCT, 
    TECHNOLOGY_GENETICS, 
    ADVENTURE, 
    CREATION, 
    GAME_MAP, 
    PARKOUR, 
    SURVIVAL, 
    PUZZLE, 
    MODDED_WORLD, 
    SIXTEEN_X, 
    THIRTY_TWO_X, 
    SIXTY_FOUR_X, 
    ONE_TWENTY_EIGHT_X, 
    TWO_FIFTY_SIX_X, 
    FIVE_TWELVE_X_AND_BEYOND, 
    ANIMATED, 
    MEDIEVAL, 
    MOD_SUPPORT, 
    MODERN, 
    PHOTO_REALISTIC, 
    STEAMPUNK, 
    TRADITIONAL, 
    MISCELLANEOUS, 
    ADVENTURE_AND_RPG, 
    COMBAT_PVP, 
    FTB_OFFICIAL_PACK, 
    HARDCORE, 
    MAP_BASED, 
    MINI_GAME, 
    MULTIPLAYER, 
    QUESTS, 
    SCI_FI, 
    TECH, 
    EXPLORATION, 
    EXTRA_LARGE, 
    SMALL_LIGHT;
    
    @JsonCreator
    public static Category createCategory(final String value) {
        return valueOf(value.toUpperCase());
    }
    
    @JsonValue
    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
    
    public static Category[] getCategories(final GameType type) {
        switch (type) {
            case MODPACK: {
                return new Category[] { Category.ALL, Category.ADVENTURE_AND_RPG, Category.COMBAT_PVP, Category.EXPLORATION, Category.EXTRA_LARGE, Category.FTB_OFFICIAL_PACK, Category.HARDCORE, Category.MAGIC, Category.MAP_BASED, Category.MINI_GAME, Category.MULTIPLAYER, Category.QUESTS, Category.SCI_FI, Category.SMALL_LIGHT, Category.TECH };
            }
            case MOD: {
                return new Category[] { Category.ALL, Category.ADVENTURE_RPG, Category.ARMOR_WEAPONS_TOOLS, Category.COSMETIC, Category.MC_FOOD, Category.MAGIC, Category.MAP_INFORMATION, Category.REDSTONE, Category.SERVER_UTILITY, Category.TECHNOLOGY, Category.TECHNOLOGY_ENERGY, Category.TECHNOLOGY_ITEM_FLUID_ENERGY_TRANSPORT, Category.TECHNOLOGY_FARMING, Category.TECHNOLOGY_GENETICS, Category.TECHNOLOGY_PLAYER_TRANSPORT, Category.TECHNOLOGY_PROCESSING, Category.WORLD_GEN, Category.WORLD_BIOMES, Category.WORLD_DIMENSIONS, Category.WORLD_MOBS, Category.WORLD_ORES_RESOURCES, Category.WORLD_STRUCTURES, Category.MC_ADDONS, Category.APPLIED_ENERGISTICS_2, Category.BLOOD_MAGIC, Category.ADDONS_BUILDCRAFT, Category.ADDONS_FORESTRY, Category.ADDONS_INDUSTRIALCRAFT, Category.ADDONS_THAUMCRAFT, Category.ADDONS_THERMALEXPANSION, Category.ADDONS_TINKERS_CONSTRUCT, Category.LIBRARY_API, Category.MC_MISCELLANEOUS, Category.CHITA };
            }
            case MAP: {
                return new Category[] { Category.ALL, Category.ADVENTURE, Category.CREATION, Category.GAME_MAP, Category.MODDED_WORLD, Category.PARKOUR, Category.PUZZLE, Category.SURVIVAL };
            }
            case RESOURCEPACK: {
                return new Category[] { Category.ALL, Category.SIXTEEN_X, Category.THIRTY_TWO_X, Category.SIXTY_FOUR_X, Category.ONE_TWENTY_EIGHT_X, Category.TWO_FIFTY_SIX_X, Category.FIVE_TWELVE_X_AND_BEYOND, Category.ANIMATED, Category.MEDIEVAL, Category.MOD_SUPPORT, Category.MODERN, Category.PHOTO_REALISTIC, Category.STEAMPUNK, Category.TRADITIONAL, Category.MISCELLANEOUS };
            }
            default: {
                return null;
            }
        }
    }
    
    public static Set<Category> getSubCategories() {
        final Category[] categories = { Category.TECHNOLOGY_ENERGY, Category.TECHNOLOGY_ITEM_FLUID_ENERGY_TRANSPORT, Category.TECHNOLOGY_FARMING, Category.TECHNOLOGY_GENETICS, Category.TECHNOLOGY_PLAYER_TRANSPORT, Category.TECHNOLOGY_PROCESSING, Category.WORLD_BIOMES, Category.WORLD_DIMENSIONS, Category.WORLD_MOBS, Category.WORLD_ORES_RESOURCES, Category.WORLD_STRUCTURES, Category.APPLIED_ENERGISTICS_2, Category.BLOOD_MAGIC, Category.ADDONS_BUILDCRAFT, Category.ADDONS_FORESTRY, Category.ADDONS_INDUSTRIALCRAFT, Category.ADDONS_THAUMCRAFT, Category.ADDONS_THERMALEXPANSION, Category.ADDONS_TINKERS_CONSTRUCT };
        return new HashSet<Category>(Arrays.asList(categories));
    }
}
