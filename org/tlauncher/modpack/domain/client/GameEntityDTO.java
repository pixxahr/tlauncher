// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.modpack.domain.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.share.Category;
import java.util.List;

public class GameEntityDTO
{
    private Long id;
    private String name;
    private String author;
    private String shortDescription;
    private String description;
    private String enDescription;
    private List<Category> categories;
    private List<TagDTO> tags;
    private Integer picture;
    private List<Integer> pictures;
    private String officialSite;
    private VersionDTO version;
    private List<VersionDTO> versions;
    private Integer downloadALL;
    private Integer downloadMonth;
    @JsonIgnore
    private boolean userInstall;
    @JsonIgnore
    private boolean populateStatus;
    private List<GameEntityDependencyDTO> dependencies;
    
    public Long getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getAuthor() {
        return this.author;
    }
    
    public String getShortDescription() {
        return this.shortDescription;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public String getEnDescription() {
        return this.enDescription;
    }
    
    public List<Category> getCategories() {
        return this.categories;
    }
    
    public List<TagDTO> getTags() {
        return this.tags;
    }
    
    public Integer getPicture() {
        return this.picture;
    }
    
    public List<Integer> getPictures() {
        return this.pictures;
    }
    
    public String getOfficialSite() {
        return this.officialSite;
    }
    
    public VersionDTO getVersion() {
        return this.version;
    }
    
    public List<VersionDTO> getVersions() {
        return this.versions;
    }
    
    public Integer getDownloadALL() {
        return this.downloadALL;
    }
    
    public Integer getDownloadMonth() {
        return this.downloadMonth;
    }
    
    public boolean isUserInstall() {
        return this.userInstall;
    }
    
    public boolean isPopulateStatus() {
        return this.populateStatus;
    }
    
    public List<GameEntityDependencyDTO> getDependencies() {
        return this.dependencies;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setAuthor(final String author) {
        this.author = author;
    }
    
    public void setShortDescription(final String shortDescription) {
        this.shortDescription = shortDescription;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public void setEnDescription(final String enDescription) {
        this.enDescription = enDescription;
    }
    
    public void setCategories(final List<Category> categories) {
        this.categories = categories;
    }
    
    public void setTags(final List<TagDTO> tags) {
        this.tags = tags;
    }
    
    public void setPicture(final Integer picture) {
        this.picture = picture;
    }
    
    public void setPictures(final List<Integer> pictures) {
        this.pictures = pictures;
    }
    
    public void setOfficialSite(final String officialSite) {
        this.officialSite = officialSite;
    }
    
    public void setVersion(final VersionDTO version) {
        this.version = version;
    }
    
    public void setVersions(final List<VersionDTO> versions) {
        this.versions = versions;
    }
    
    public void setDownloadALL(final Integer downloadALL) {
        this.downloadALL = downloadALL;
    }
    
    public void setDownloadMonth(final Integer downloadMonth) {
        this.downloadMonth = downloadMonth;
    }
    
    public void setUserInstall(final boolean userInstall) {
        this.userInstall = userInstall;
    }
    
    public void setPopulateStatus(final boolean populateStatus) {
        this.populateStatus = populateStatus;
    }
    
    public void setDependencies(final List<GameEntityDependencyDTO> dependencies) {
        this.dependencies = dependencies;
    }
    
    @Override
    public String toString() {
        return "GameEntityDTO(id=" + this.getId() + ", name=" + this.getName() + ", author=" + this.getAuthor() + ", shortDescription=" + this.getShortDescription() + ", description=" + this.getDescription() + ", enDescription=" + this.getEnDescription() + ", categories=" + this.getCategories() + ", tags=" + this.getTags() + ", picture=" + this.getPicture() + ", pictures=" + this.getPictures() + ", officialSite=" + this.getOfficialSite() + ", version=" + this.getVersion() + ", versions=" + this.getVersions() + ", downloadALL=" + this.getDownloadALL() + ", downloadMonth=" + this.getDownloadMonth() + ", userInstall=" + this.isUserInstall() + ", populateStatus=" + this.isPopulateStatus() + ", dependencies=" + this.getDependencies() + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof GameEntityDTO)) {
            return false;
        }
        final GameEntityDTO other = (GameEntityDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        Label_0065: {
            if (this$id == null) {
                if (other$id == null) {
                    break Label_0065;
                }
            }
            else if (this$id.equals(other$id)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        Label_0102: {
            if (this$name == null) {
                if (other$name == null) {
                    break Label_0102;
                }
            }
            else if (this$name.equals(other$name)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$author = this.getAuthor();
        final Object other$author = other.getAuthor();
        Label_0139: {
            if (this$author == null) {
                if (other$author == null) {
                    break Label_0139;
                }
            }
            else if (this$author.equals(other$author)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$shortDescription = this.getShortDescription();
        final Object other$shortDescription = other.getShortDescription();
        Label_0176: {
            if (this$shortDescription == null) {
                if (other$shortDescription == null) {
                    break Label_0176;
                }
            }
            else if (this$shortDescription.equals(other$shortDescription)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$description = this.getDescription();
        final Object other$description = other.getDescription();
        Label_0213: {
            if (this$description == null) {
                if (other$description == null) {
                    break Label_0213;
                }
            }
            else if (this$description.equals(other$description)) {
                break Label_0213;
            }
            return false;
        }
        final Object this$enDescription = this.getEnDescription();
        final Object other$enDescription = other.getEnDescription();
        Label_0250: {
            if (this$enDescription == null) {
                if (other$enDescription == null) {
                    break Label_0250;
                }
            }
            else if (this$enDescription.equals(other$enDescription)) {
                break Label_0250;
            }
            return false;
        }
        final Object this$categories = this.getCategories();
        final Object other$categories = other.getCategories();
        Label_0287: {
            if (this$categories == null) {
                if (other$categories == null) {
                    break Label_0287;
                }
            }
            else if (this$categories.equals(other$categories)) {
                break Label_0287;
            }
            return false;
        }
        final Object this$tags = this.getTags();
        final Object other$tags = other.getTags();
        Label_0324: {
            if (this$tags == null) {
                if (other$tags == null) {
                    break Label_0324;
                }
            }
            else if (this$tags.equals(other$tags)) {
                break Label_0324;
            }
            return false;
        }
        final Object this$picture = this.getPicture();
        final Object other$picture = other.getPicture();
        Label_0361: {
            if (this$picture == null) {
                if (other$picture == null) {
                    break Label_0361;
                }
            }
            else if (this$picture.equals(other$picture)) {
                break Label_0361;
            }
            return false;
        }
        final Object this$pictures = this.getPictures();
        final Object other$pictures = other.getPictures();
        Label_0398: {
            if (this$pictures == null) {
                if (other$pictures == null) {
                    break Label_0398;
                }
            }
            else if (this$pictures.equals(other$pictures)) {
                break Label_0398;
            }
            return false;
        }
        final Object this$officialSite = this.getOfficialSite();
        final Object other$officialSite = other.getOfficialSite();
        Label_0435: {
            if (this$officialSite == null) {
                if (other$officialSite == null) {
                    break Label_0435;
                }
            }
            else if (this$officialSite.equals(other$officialSite)) {
                break Label_0435;
            }
            return false;
        }
        final Object this$version = this.getVersion();
        final Object other$version = other.getVersion();
        Label_0472: {
            if (this$version == null) {
                if (other$version == null) {
                    break Label_0472;
                }
            }
            else if (this$version.equals(other$version)) {
                break Label_0472;
            }
            return false;
        }
        final Object this$versions = this.getVersions();
        final Object other$versions = other.getVersions();
        Label_0509: {
            if (this$versions == null) {
                if (other$versions == null) {
                    break Label_0509;
                }
            }
            else if (this$versions.equals(other$versions)) {
                break Label_0509;
            }
            return false;
        }
        final Object this$downloadALL = this.getDownloadALL();
        final Object other$downloadALL = other.getDownloadALL();
        Label_0546: {
            if (this$downloadALL == null) {
                if (other$downloadALL == null) {
                    break Label_0546;
                }
            }
            else if (this$downloadALL.equals(other$downloadALL)) {
                break Label_0546;
            }
            return false;
        }
        final Object this$downloadMonth = this.getDownloadMonth();
        final Object other$downloadMonth = other.getDownloadMonth();
        Label_0583: {
            if (this$downloadMonth == null) {
                if (other$downloadMonth == null) {
                    break Label_0583;
                }
            }
            else if (this$downloadMonth.equals(other$downloadMonth)) {
                break Label_0583;
            }
            return false;
        }
        if (this.isUserInstall() != other.isUserInstall()) {
            return false;
        }
        if (this.isPopulateStatus() != other.isPopulateStatus()) {
            return false;
        }
        final Object this$dependencies = this.getDependencies();
        final Object other$dependencies = other.getDependencies();
        if (this$dependencies == null) {
            if (other$dependencies == null) {
                return true;
            }
        }
        else if (this$dependencies.equals(other$dependencies)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof GameEntityDTO;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * 59 + (($id == null) ? 43 : $id.hashCode());
        final Object $name = this.getName();
        result = result * 59 + (($name == null) ? 43 : $name.hashCode());
        final Object $author = this.getAuthor();
        result = result * 59 + (($author == null) ? 43 : $author.hashCode());
        final Object $shortDescription = this.getShortDescription();
        result = result * 59 + (($shortDescription == null) ? 43 : $shortDescription.hashCode());
        final Object $description = this.getDescription();
        result = result * 59 + (($description == null) ? 43 : $description.hashCode());
        final Object $enDescription = this.getEnDescription();
        result = result * 59 + (($enDescription == null) ? 43 : $enDescription.hashCode());
        final Object $categories = this.getCategories();
        result = result * 59 + (($categories == null) ? 43 : $categories.hashCode());
        final Object $tags = this.getTags();
        result = result * 59 + (($tags == null) ? 43 : $tags.hashCode());
        final Object $picture = this.getPicture();
        result = result * 59 + (($picture == null) ? 43 : $picture.hashCode());
        final Object $pictures = this.getPictures();
        result = result * 59 + (($pictures == null) ? 43 : $pictures.hashCode());
        final Object $officialSite = this.getOfficialSite();
        result = result * 59 + (($officialSite == null) ? 43 : $officialSite.hashCode());
        final Object $version = this.getVersion();
        result = result * 59 + (($version == null) ? 43 : $version.hashCode());
        final Object $versions = this.getVersions();
        result = result * 59 + (($versions == null) ? 43 : $versions.hashCode());
        final Object $downloadALL = this.getDownloadALL();
        result = result * 59 + (($downloadALL == null) ? 43 : $downloadALL.hashCode());
        final Object $downloadMonth = this.getDownloadMonth();
        result = result * 59 + (($downloadMonth == null) ? 43 : $downloadMonth.hashCode());
        result = result * 59 + (this.isUserInstall() ? 79 : 97);
        result = result * 59 + (this.isPopulateStatus() ? 79 : 97);
        final Object $dependencies = this.getDependencies();
        result = result * 59 + (($dependencies == null) ? 43 : $dependencies.hashCode());
        return result;
    }
}
