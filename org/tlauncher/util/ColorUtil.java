// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import java.awt.Color;

public class ColorUtil
{
    public static final Color COLOR_248;
    public static final Color COLOR_247;
    public static final Color COLOR_246;
    public static final Color COLOR_244;
    public static final Color COLOR_241;
    public static final Color COLOR_235;
    public static final Color COLOR_233;
    public static final Color COLOR_215;
    public static final Color COLOR_213;
    public static final Color COLOR_202;
    public static final Color COLOR_195;
    public static final Color COLOR_193;
    public static final Color COLOR_169;
    public static final Color COLOR_149;
    public static final Color COLOR_77;
    public static final Color COLOR_64;
    public static final Color COLOR_25;
    public static final Color COLOR_16;
    public static final Color BLUE_MODPACK_BUTTON_UP;
    public static final Color BLUE_MODPACK;
    
    public static Color get(final int i) {
        return new Color(i, i, i);
    }
    
    static {
        COLOR_248 = new Color(248, 248, 248);
        COLOR_247 = new Color(247, 247, 247);
        COLOR_246 = new Color(246, 246, 246);
        COLOR_244 = new Color(244, 244, 244);
        COLOR_241 = new Color(241, 241, 241);
        COLOR_235 = new Color(235, 235, 235);
        COLOR_233 = new Color(233, 233, 233);
        COLOR_215 = new Color(215, 215, 215);
        COLOR_213 = new Color(213, 213, 213);
        COLOR_202 = new Color(202, 202, 202);
        COLOR_195 = new Color(195, 195, 195);
        COLOR_193 = new Color(193, 193, 193);
        COLOR_169 = new Color(169, 169, 169);
        COLOR_149 = new Color(149, 149, 149);
        COLOR_77 = new Color(77, 77, 77);
        COLOR_64 = new Color(64, 64, 64);
        COLOR_25 = new Color(25, 25, 25);
        COLOR_16 = new Color(16, 16, 16);
        BLUE_MODPACK_BUTTON_UP = new Color(2, 161, 221);
        BLUE_MODPACK = new Color(60, 170, 232);
    }
}
