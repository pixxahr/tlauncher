// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import java.lang.management.ManagementFactory;
import com.sun.management.OperatingSystemMXBean;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.net.URISyntaxException;
import java.net.URL;
import javax.swing.SwingUtilities;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.awt.Desktop;
import java.net.URI;
import org.tlauncher.tlauncher.configuration.Configuration;
import java.util.Objects;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.io.File;
import java.util.Map;

public enum OS
{
    LINUX(new String[] { "linux", "unix" }), 
    WINDOWS(new String[] { "win" }), 
    OSX(new String[] { "mac" }), 
    SOLARIS(new String[] { "solaris", "sunos" }), 
    UNKNOWN(new String[] { "unknown" });
    
    public static final String NAME;
    public static final String VERSION;
    public static final double JAVA_VERSION;
    public static final OS CURRENT;
    private static final Map<String, String> systemInfo;
    private final String name;
    private final String[] aliases;
    
    private OS(final String[] aliases) {
        if (aliases == null) {
            throw new NullPointerException();
        }
        this.name = this.toString().toLowerCase();
        this.aliases = aliases;
    }
    
    private static OS getCurrent() {
        final String osName = OS.NAME.toLowerCase();
        for (final OS os : values()) {
            for (final String alias : os.aliases) {
                if (osName.contains(alias)) {
                    return os;
                }
            }
        }
        return OS.UNKNOWN;
    }
    
    private static double getJavaVersion() {
        String version;
        int count;
        int pos;
        for (version = System.getProperty("java.version"), count = 0, pos = 0; pos < version.length() && count < 2; ++pos) {
            if (version.charAt(pos) == '.') {
                ++count;
            }
        }
        --pos;
        final String doubleVersion = version.substring(0, pos);
        return Double.parseDouble(doubleVersion);
    }
    
    public static boolean isJava8() {
        return "8".equals(getJavaNumber());
    }
    
    public static boolean is(final OS... any) {
        if (any == null) {
            throw new NullPointerException();
        }
        if (any.length == 0) {
            return false;
        }
        for (final OS compare : any) {
            if (OS.CURRENT == compare) {
                return true;
            }
        }
        return false;
    }
    
    public static String getJavaPathByHome(final boolean appendBinFolder) {
        String path = System.getProperty("java.home");
        if (appendBinFolder) {
            path = appendToJVM(path);
        }
        return path;
    }
    
    public static String appendToJVM(final String path) {
        final char separator = File.separatorChar;
        final StringBuilder b = new StringBuilder(path);
        b.append(separator);
        b.append("bin").append(separator).append("java");
        if (OS.CURRENT == OS.WINDOWS) {
            b.append("w.exe");
        }
        return b.toString();
    }
    
    public static String appendBootstrapperJvm(final String path) {
        U.log("path " + path);
        final StringBuilder b = new StringBuilder();
        if (OS.CURRENT == OS.OSX && !path.toLowerCase().endsWith("jre") && !path.toLowerCase().endsWith("home")) {
            b.append("Contents").append(File.separatorChar).append("Home").append(File.separatorChar).append("jre").append(File.separatorChar);
        }
        return appendToJVM(new File(path, b.toString()).getPath());
    }
    
    public static String getSummary() {
        final Configuration c = TLauncher.getInstance().getConfiguration();
        final String bitMessage = c.get("memory.problem.message");
        final String options = Objects.nonNull(System.getenv("_java_options")) ? (System.lineSeparator() + "_java_options " + System.getenv("_java_options")) : "";
        final StringBuilder builder = new StringBuilder();
        builder.append("-------------------------------------------------------").append(System.lineSeparator());
        builder.append(OS.NAME).append(" ").append(OS.VERSION).append(", Java").append(" ").append(System.getProperty("java.version")).append(", jvm bit ").append(getJavaBit()).append(", ").append(Arch.TOTAL_RAM_MB).append(" MB RAM");
        if (bitMessage != null && Arch.CURRENT == Arch.x32) {
            builder.append(", ").append(bitMessage);
        }
        builder.append(System.lineSeparator());
        builder.append("java path=").append(getJavaPathByHome(true));
        builder.append(options);
        final String processInfo = c.get("process.info");
        if (Objects.nonNull(processInfo)) {
            builder.append(System.lineSeparator()).append(processInfo);
        }
        final String gpu = c.get("gpu.info.full");
        if (Objects.nonNull(gpu)) {
            builder.append(System.lineSeparator()).append(gpu);
        }
        builder.append(System.lineSeparator()).append("-------------------------------------------------------");
        return builder.toString();
    }
    
    public static Arch getJavaBit() {
        final String res = System.getProperty("sun.arch.data.model");
        if (res != null && res.equalsIgnoreCase("64")) {
            return Arch.x64;
        }
        return Arch.x32;
    }
    
    private static void rawOpenLink(final URI uri) throws Throwable {
        if (!Desktop.isDesktopSupported()) {
            log("Your system doesnt'have a Desktop object");
            return;
        }
        Desktop.getDesktop().browse(uri);
    }
    
    public static boolean openLink(final URI uri, final boolean alertError) {
        log("Trying to open link with default browser:", uri);
        try {
            if (is(OS.LINUX)) {
                Runtime.getRuntime().exec("gnome-open " + uri);
                return true;
            }
            if (!Desktop.isDesktopSupported()) {
                log("Your system doesnt'have a Desktop object");
                return false;
            }
            Desktop.getDesktop().browse(uri);
        }
        catch (Throwable e) {
            log("Failed to open link with default browser:", uri, e);
            if (alertError) {
                SwingUtilities.invokeLater(() -> Alert.showLocError("ui.error.openlink", uri));
            }
            return false;
        }
        return true;
    }
    
    public static boolean openLink(final URI uri) {
        return openLink(uri, true);
    }
    
    public static boolean openLink(final URL url, final boolean alertError) {
        log("Trying to open URL with default browser:", url);
        URI uri = null;
        try {
            uri = url.toURI();
        }
        catch (Exception ex) {}
        return openLink(uri, alertError);
    }
    
    public static boolean openLink(final URL url) {
        return openLink(url, true);
    }
    
    public static boolean openLink(final String url) {
        try {
            return openLink(new URI(url), true);
        }
        catch (URISyntaxException e) {
            U.log(e);
            return false;
        }
    }
    
    private static void openPath(final File path, final boolean appendSeparator) throws Throwable {
        final String absPath = path.getAbsolutePath() + File.separatorChar;
        final Runtime r = Runtime.getRuntime();
        Throwable t = null;
        Label_0209: {
            switch (OS.CURRENT) {
                case OSX: {
                    final String[] cmdArr = { "/usr/bin/open", absPath };
                    try {
                        r.exec(cmdArr);
                        return;
                    }
                    catch (Throwable e) {
                        t = e;
                        log("Cannot open folder using:\n", cmdArr, e);
                        break Label_0209;
                    }
                }
                case WINDOWS: {
                    final String cmd = String.format("cmd.exe /C start \"Open path\" \"%s\"", absPath);
                    try {
                        r.exec(cmd);
                        return;
                    }
                    catch (Throwable e2) {
                        t = e2;
                        log("Cannot open folder using CMD.exe:\n", cmd, e2);
                        break Label_0209;
                    }
                }
                case LINUX: {
                    if (Desktop.isDesktopSupported()) {
                        Desktop.getDesktop().open(new File(absPath));
                        return;
                    }
                    break;
                }
            }
            log("... will use desktop");
            try {
                rawOpenLink(path.toURI());
            }
            catch (Throwable e3) {
                t = e3;
            }
        }
        if (t == null) {
            return;
        }
        throw t;
    }
    
    public static boolean openFolder(final File folder, final boolean alertError) {
        log("Trying to open folder:", folder);
        if (!folder.isDirectory()) {
            log("This path is not a directory, sorry.");
            return false;
        }
        try {
            openPath(folder, true);
        }
        catch (Throwable e) {
            log("Failed to open folder:", e);
            if (alertError) {
                Alert.showLocError("ui.error.openfolder", folder);
            }
            return false;
        }
        return true;
    }
    
    public static boolean openFolder(final File folder) {
        return openFolder(folder, true);
    }
    
    public static boolean openFile(final File file, final boolean alertError) {
        log("Trying to open file:", file);
        if (!file.isFile()) {
            log("This path is not a file, sorry.");
            return false;
        }
        try {
            openPath(file, false);
        }
        catch (Throwable e) {
            log("Failed to open file:", e);
            if (alertError) {
                Alert.showLocError("ui.error.openfolder", file);
            }
            return false;
        }
        return true;
    }
    
    public static boolean openFile(final File file) {
        return openFile(file, true);
    }
    
    protected static void log(final Object... o) {
        U.log("[OS]", o);
    }
    
    public static String getJavaNumber() {
        if (String.valueOf(OS.JAVA_VERSION).startsWith("10")) {
            return "10";
        }
        if (String.valueOf(OS.JAVA_VERSION).startsWith("11")) {
            return "11";
        }
        if (OS.JAVA_VERSION > 2.0) {
            return String.valueOf(OS.JAVA_VERSION).substring(0, 1);
        }
        return String.valueOf(OS.JAVA_VERSION).substring(2, 3);
    }
    
    public static void fillSystemInfo() {
        try {
            if (getCurrent() == OS.WINDOWS) {
                final Process p = Runtime.getRuntime().exec("cmd.exe /C chcp 437 & systeminfo");
                p.waitFor(30L, TimeUnit.SECONDS);
                final String res = FileUtil.readStream(p.getInputStream(), "cp866");
                final String[] array = res.split("\r\n");
                for (int i = 0; i < array.length; ++i) {
                    final String r = array[i];
                    if (!r.isEmpty()) {
                        final int first = r.indexOf(":");
                        if (first > 0) {
                            if (r.substring(0, first).equalsIgnoreCase("Processor(s)")) {
                                OS.systemInfo.put(r.substring(0, first), r.substring(first + 1) + array[++i]);
                            }
                            else {
                                OS.systemInfo.put(r.substring(0, first), r.substring(first + 1));
                            }
                        }
                    }
                }
            }
        }
        catch (Throwable e) {
            U.log(e);
        }
    }
    
    public static String executeByTerminal(final String command, final int time) {
        String res = "";
        try {
            if (getCurrent() == OS.WINDOWS) {
                final Process p = Runtime.getRuntime().exec("cmd.exe /C chcp 437 & " + command);
                p.waitFor(time, TimeUnit.SECONDS);
                res = FileUtil.readStream(p.getInputStream(), "IBM437");
            }
            else if (is(OS.LINUX)) {
                final Process p = Runtime.getRuntime().exec(command);
                p.waitFor(time, TimeUnit.SECONDS);
                res = FileUtil.readStream(p.getInputStream());
            }
            else if (is(OS.OSX)) {
                final Process p = Runtime.getRuntime().exec(command);
                p.waitFor(time, TimeUnit.SECONDS);
                res = FileUtil.readStream(p.getInputStream());
            }
        }
        catch (Throwable e) {
            log(e);
        }
        return res;
    }
    
    public static String executeByTerminal(final String command) {
        return executeByTerminal(command, 30);
    }
    
    public static String getSystemInfo(final String key) {
        return OS.systemInfo.get(key);
    }
    
    public String getName() {
        return this.name;
    }
    
    public boolean isUnsupported() {
        return this == OS.UNKNOWN;
    }
    
    public boolean isCurrent() {
        return this == OS.CURRENT;
    }
    
    static {
        NAME = System.getProperty("os.name");
        VERSION = System.getProperty("os.version");
        JAVA_VERSION = getJavaVersion();
        CURRENT = getCurrent();
        systemInfo = new HashMap<String, String>();
    }
    
    public enum Arch
    {
        x32, 
        x64, 
        UNKNOWN;
        
        public static final Arch CURRENT;
        public static final long TOTAL_RAM;
        public static final int TOTAL_RAM_MB;
        public static final int MIN_MEMORY = 512;
        public static final int PREFERRED_MEMORY;
        public static final int MAX_MEMORY;
        private final String asString;
        private final int asInt;
        
        private Arch() {
            this.asString = this.toString().substring(1);
            int asInt_temp = 0;
            try {
                asInt_temp = Integer.parseInt(this.asString);
            }
            catch (RuntimeException ex) {}
            this.asInt = asInt_temp;
        }
        
        private static Arch getCurrent() {
            final String curArch = System.getProperty("sun.arch.data.model");
            for (final Arch arch : values()) {
                if (arch.asString.equals(curArch)) {
                    return arch;
                }
            }
            return Arch.UNKNOWN;
        }
        
        private static long getTotalRam() {
            try {
                return ((OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize();
            }
            catch (Throwable e) {
                U.log("[ERROR] Cannot allocate total physical memory size!", e);
                return 0L;
            }
        }
        
        private static int getPreferredMemory() {
            final String name = OS.NAME;
            int value = 0;
            if (name.contains("Windows 10")) {
                value = Arch.TOTAL_RAM_MB - 1400;
            }
            else if (name.contains("Windows 8")) {
                value = Arch.TOTAL_RAM_MB - 1200;
            }
            else if (name.contains("Windows 7")) {
                value = Arch.TOTAL_RAM_MB - 900;
            }
            else if (OS.is(OS.LINUX)) {
                value = Arch.TOTAL_RAM_MB - 900;
            }
            else if (name.contains("Windows XP")) {
                value = Arch.TOTAL_RAM_MB - 256;
            }
            if (value > 0) {
                return value;
            }
            switch (Arch.CURRENT) {
                case x64: {
                    if (Arch.TOTAL_RAM_MB > 5000) {
                        return Arch.TOTAL_RAM_MB * 2 / 3;
                    }
                    if (Arch.TOTAL_RAM_MB > 4000) {
                        return 2500;
                    }
                    if (Arch.TOTAL_RAM_MB > 3000) {
                        return 1500;
                    }
                    if (Arch.TOTAL_RAM_MB > 2000) {
                        return 1250;
                    }
                    if (Arch.TOTAL_RAM_MB > 1500) {
                        return 750;
                    }
                    break;
                }
                case x32: {
                    if (Arch.TOTAL_RAM_MB > 2000) {
                        return 1024;
                    }
                    if (Arch.TOTAL_RAM_MB > 1500) {
                        return 750;
                    }
                    break;
                }
            }
            return 512;
        }
        
        private static int getPrefferWrapper() {
            final int value = getPreferredMemory();
            if (value < 512) {
                return 512;
            }
            if (Arch.CURRENT == Arch.x32 && value > 1024) {
                return 1024;
            }
            return value;
        }
        
        private static int getMaximumMemory() {
            switch (Arch.CURRENT) {
                case x64: {
                    return Arch.TOTAL_RAM_MB;
                }
                case x32: {
                    if (Arch.TOTAL_RAM_MB > 1500) {
                        return 1500;
                    }
                    break;
                }
            }
            return 512;
        }
        
        public String asString() {
            return (this == Arch.UNKNOWN) ? this.toString() : this.asString;
        }
        
        public int asInteger() {
            return this.asInt;
        }
        
        public boolean isCurrent() {
            return this == Arch.CURRENT;
        }
        
        static {
            CURRENT = getCurrent();
            TOTAL_RAM = getTotalRam();
            TOTAL_RAM_MB = (int)(Arch.TOTAL_RAM / 1024L / 1024L);
            PREFERRED_MEMORY = getPrefferWrapper();
            MAX_MEMORY = getMaximumMemory();
        }
    }
}
