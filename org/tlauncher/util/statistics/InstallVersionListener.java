// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.statistics;

import java.util.Map;
import com.google.common.collect.Maps;
import org.tlauncher.statistics.InstallVersionDTO;
import net.minecraft.launcher.versions.Version;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.rmo.TLauncher;
import net.minecraft.launcher.updater.VersionSyncInfo;
import org.tlauncher.tlauncher.downloader.Downloader;
import org.tlauncher.tlauncher.downloader.DownloaderAdapterListener;

public class InstallVersionListener extends DownloaderAdapterListener
{
    private String version;
    
    @Override
    public void onDownloaderStart(final Downloader d, final int files) {
        try {
            Version v = TLauncher.getInstance().getFrame().mp.defaultScene.loginForm.versions.getSelectedValue().getRemote();
            if (v == null) {
                v = TLauncher.getInstance().getFrame().mp.defaultScene.loginForm.versions.getSelectedValue().getLocal();
            }
            if (v == null) {
                this.version = "version_not_found";
                return;
            }
            this.version = v.getID();
        }
        catch (NullPointerException exception) {
            U.log(exception);
            this.version = "version_not_found";
        }
    }
    
    @Override
    public void onDownloaderComplete(final Downloader d) {
        final InstallVersionDTO v = new InstallVersionDTO();
        v.setInstallVersion(this.version);
        StatisticsUtil.startSending("save/install/version", v, Maps.newHashMap());
    }
}
