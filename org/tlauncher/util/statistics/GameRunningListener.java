// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.statistics;

import java.util.Map;
import java.util.HashMap;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftListenerAdapter;

public class GameRunningListener extends MinecraftListenerAdapter
{
    private MinecraftLauncher game;
    
    public GameRunningListener(final MinecraftLauncher game) {
        this.game = game;
    }
    
    @Override
    public void onMinecraftLaunch() {
        final Map<String, Object> map = new HashMap<String, Object>();
        map.put("version", this.game.getVersion().getID());
        StatisticsUtil.startSending("save/run/version", null, map);
    }
}
