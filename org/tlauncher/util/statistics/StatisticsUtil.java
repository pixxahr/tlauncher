// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.statistics;

import java.util.Date;
import org.tlauncher.tlauncher.updater.client.Offer;
import org.tlauncher.statistics.UpdaterDTO;
import org.tlauncher.tlauncher.controller.UpdaterFormController;
import org.tlauncher.tlauncher.updater.client.Update;
import java.awt.Dimension;
import com.google.common.collect.Maps;
import java.util.Objects;
import java.awt.Toolkit;
import org.tlauncher.util.OS;
import org.tlauncher.statistics.UniqueClientDTO;
import org.tlauncher.tlauncher.configuration.Configuration;
import java.net.URL;
import org.tlauncher.util.TlauncherUtil;
import net.minecraft.launcher.Http;
import org.tlauncher.util.async.AsyncThread;
import java.io.IOException;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.util.Map;

public class StatisticsUtil
{
    public static void startSending(final String path, final Object ob, final Map<String, Object> queries) {
        if (!TLauncher.getInstance().getConfiguration().getBoolean("gui.statistics.checkbox")) {
            AsyncThread.execute(() -> {
                try {
                    send(path, ob, queries);
                }
                catch (IOException e) {
                    U.log(e);
                }
            });
        }
    }
    
    public static void send(final String path, final Object ob, final Map<String, Object> queries) throws IOException {
        final String domain = TLauncher.getInnerSettings().get("statistics.url");
        final URL url = Http.constantURL(TlauncherUtil.resolveHostName(Http.get(domain + path, queries)));
        Http.performPost(url, TLauncher.getGson().toJson(ob), "application/json");
    }
    
    public static void sendMachineInfo(final Configuration conf) {
        final UniqueClientDTO running = new UniqueClientDTO();
        running.setClientVersion(TLauncher.getVersion());
        running.setOs(OS.CURRENT.name());
        final Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        running.setResolution(size.width + "x" + size.height);
        running.setJavaVersion(System.getProperty("java.version"));
        running.setOsVersion(OS.VERSION);
        running.setUuid(conf.get("client").replace("-", ""));
        running.setGpu(conf.get("gpu.info"));
        running.setRam(OS.Arch.TOTAL_RAM_MB);
        String processor = conf.get("process.info");
        if (Objects.nonNull(processor)) {
            processor = processor.trim();
        }
        running.setCpu(processor);
        startSending("save/run/tlauncher/unique/month", running, Maps.newHashMap());
    }
    
    public static void sendUpdatingInfo(final Update update, final UpdaterFormController.UserResult res) {
        try {
            final UpdaterDTO dto = preparedUpdaterDTO(update, res);
            send("updater/save", dto, Maps.newHashMap());
        }
        catch (Throwable t) {
            U.log(t);
        }
    }
    
    public static UpdaterDTO preparedUpdaterDTO(final Update update, final UpdaterFormController.UserResult res) {
        final UpdaterDTO dto = new UpdaterDTO();
        dto.setClient(TLauncher.getInstance().getConfiguration().get("client"));
        dto.setOffer(update.getOffers().get(0).getOffer());
        dto.setArgs(res.getOfferArgs());
        dto.setCurrentVersion(TLauncher.getInnerSettings().getDouble("version"));
        dto.setNewVersion(update.getVersion());
        dto.setUpdaterLater(res.getUserChooser() == 0);
        dto.setRequestTime(new Date().toString());
        return dto;
    }
}
