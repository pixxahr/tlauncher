// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import org.apache.log4j.Appender;
import java.nio.charset.StandardCharsets;
import org.apache.log4j.Priority;
import org.apache.log4j.Level;
import org.apache.log4j.Layout;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.FileAppender;
import java.io.Closeable;
import java.awt.Color;
import java.util.LinkedHashMap;
import java.util.Map;
import java.net.Proxy;
import org.tlauncher.tlauncher.configuration.enums.ConnectionQuality;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.net.URI;
import java.net.URL;
import org.tlauncher.util.async.ExtendedThread;
import java.util.Random;
import org.tlauncher.tlauncher.rmo.Bootstrapper;
import java.util.Enumeration;
import java.util.Iterator;
import java.io.File;
import org.apache.log4j.Logger;

public class U
{
    public static final String PROGRAM_PACKAGE = "org.tlauncher";
    public static final int CONNECTION_TIMEOUT = 15000;
    public static Logger logField;
    private static final int ST_TOTAL = 100;
    private static final int ST_PROGRAM = 10;
    private static String PREFIX;
    private static final Object lock;
    
    private U() {
    }
    
    public static void log(final Object... what) {
        hlog(U.PREFIX, what);
    }
    
    public static void plog(final Object... what) {
        hlog(null, what);
    }
    
    private static void hlog(final String prefix, final Object[] append) {
        synchronized (U.lock) {
            final String line = toLog(prefix, append);
            if (U.logField != null) {
                U.logField.info(line);
            }
            System.out.println(line);
        }
    }
    
    private static String toLog(final String prefix, final Object... append) {
        final StringBuilder b = new StringBuilder();
        boolean first = true;
        if (prefix != null) {
            b.append(prefix);
            first = false;
        }
        if (append != null) {
            for (final Object e : append) {
                Label_0395: {
                    if (e != null) {
                        if (e.getClass().isArray()) {
                            if (!first) {
                                b.append(" ");
                            }
                            if (e instanceof Object[]) {
                                b.append(toLog((Object[])e));
                                break Label_0395;
                            }
                            b.append(arrayToLog(e));
                            break Label_0395;
                        }
                        else {
                            if (e instanceof Throwable) {
                                if (!first) {
                                    b.append("\n");
                                }
                                b.append(stackTrace((Throwable)e));
                                b.append("\n");
                                break Label_0395;
                            }
                            if (e instanceof File) {
                                if (!first) {
                                    b.append(" ");
                                }
                                final File file = (File)e;
                                final String absPath = file.getAbsolutePath();
                                b.append(absPath);
                                if (file.isDirectory() && !absPath.endsWith(File.separator)) {
                                    b.append(File.separator);
                                }
                            }
                            else if (e instanceof Iterator) {
                                final Iterator<?> i = (Iterator<?>)e;
                                while (i.hasNext()) {
                                    b.append(" ");
                                    b.append(toLog(i.next()));
                                }
                            }
                            else if (e instanceof Enumeration) {
                                final Enumeration<?> en = (Enumeration<?>)e;
                                while (en.hasMoreElements()) {
                                    b.append(" ");
                                    b.append(toLog(en.nextElement()));
                                }
                            }
                            else {
                                if (!first) {
                                    b.append(" ");
                                }
                                b.append(e);
                            }
                        }
                    }
                    else {
                        if (!first) {
                            b.append(" ");
                        }
                        b.append("null");
                    }
                    if (first) {
                        first = false;
                    }
                }
            }
        }
        else {
            b.append("null");
        }
        return b.toString();
    }
    
    public static String toLog(final Object... append) {
        return toLog(null, append);
    }
    
    private static String arrayToLog(final Object e) {
        if (!e.getClass().isArray()) {
            throw new IllegalArgumentException("Given object is not an array!");
        }
        final StringBuilder b = new StringBuilder();
        boolean first = true;
        if (e instanceof Object[]) {
            for (final Object i : (Object[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(i);
            }
        }
        else if (e instanceof int[]) {
            for (final int j : (int[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(j);
            }
        }
        else if (e instanceof boolean[]) {
            for (final boolean k : (boolean[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(k);
            }
        }
        else if (e instanceof long[]) {
            for (final long l : (long[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(l);
            }
        }
        else if (e instanceof float[]) {
            for (final float m : (float[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(m);
            }
        }
        else if (e instanceof double[]) {
            for (final double i2 : (double[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(i2);
            }
        }
        else if (e instanceof byte[]) {
            for (final byte i3 : (byte[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(i3);
            }
        }
        else if (e instanceof short[]) {
            for (final short i4 : (short[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(i4);
            }
        }
        else if (e instanceof char[]) {
            for (final char i5 : (char[])e) {
                if (!first) {
                    b.append(" ");
                }
                else {
                    first = false;
                }
                b.append(i5);
            }
        }
        if (b.length() == 0) {
            throw new UnknownError("Unknown array type given.");
        }
        return b.toString();
    }
    
    public static void setLoadingStep(final Bootstrapper.LoadingStep step) {
        if (step == null) {
            throw new NullPointerException();
        }
        plog("[Loading]", step.toString());
    }
    
    public static boolean ok(final int d) {
        return new Random(System.currentTimeMillis()).nextInt(d) == 0;
    }
    
    public static double getAverage(final double[] d) {
        double a = 0.0;
        int k = 0;
        for (final double curd : d) {
            if (curd != 0.0) {
                a += curd;
                ++k;
            }
        }
        if (k == 0) {
            return 0.0;
        }
        return a / k;
    }
    
    public static double getAverage(final double[] d, final int max) {
        double a = 0.0;
        int k = 0;
        for (final double curd : d) {
            a += curd;
            if (++k == max) {
                break;
            }
        }
        if (k == 0) {
            return 0.0;
        }
        return a / k;
    }
    
    public static double getSum(final double[] d) {
        double a = 0.0;
        for (final double curd : d) {
            a += curd;
        }
        return a;
    }
    
    public static int getMaxMultiply(final int i, final int max) {
        if (i <= max) {
            return 1;
        }
        for (int x = max; x > 1; --x) {
            if (i % x == 0) {
                return x;
            }
        }
        return (int)Math.ceil(i / max);
    }
    
    private static String stackTrace(final Throwable e) {
        final StringBuilder trace = rawStackTrace(e);
        final ExtendedThread currentAsExtended = getAs(Thread.currentThread(), ExtendedThread.class);
        if (currentAsExtended != null) {
            trace.append("\nThread called by: ").append((CharSequence)rawStackTrace(currentAsExtended.getCaller()));
        }
        return trace.toString();
    }
    
    private static StringBuilder rawStackTrace(final Throwable e) {
        if (e == null) {
            return null;
        }
        final StackTraceElement[] elems = e.getStackTrace();
        int programElements = 0;
        int totalElements = 0;
        final StringBuilder builder = new StringBuilder();
        builder.append(e.toString());
        final StackTraceElement[] array = elems;
        final int length = array.length;
        int i = 0;
        while (i < length) {
            final StackTraceElement elem = array[i];
            ++totalElements;
            final String description = elem.toString();
            if (description.startsWith("org.tlauncher")) {
                ++programElements;
            }
            builder.append("\nat ").append(description);
            if (totalElements == 100 || programElements == 10) {
                final int remain = elems.length - totalElements;
                if (remain != 0) {
                    builder.append("\n... and ").append(remain).append(" more");
                    break;
                }
                break;
            }
            else {
                ++i;
            }
        }
        final Throwable cause = e.getCause();
        if (cause != null) {
            builder.append("\nCaused by: ").append((CharSequence)rawStackTrace(cause));
        }
        return builder;
    }
    
    public static long getUsingSpace() {
        return getTotalSpace() - getFreeSpace();
    }
    
    public static long getFreeSpace() {
        return Runtime.getRuntime().freeMemory() / 1048576L;
    }
    
    public static long getTotalSpace() {
        return Runtime.getRuntime().totalMemory() / 1048576L;
    }
    
    public static String memoryStatus() {
        return getUsingSpace() + " / " + getTotalSpace() + " MB";
    }
    
    public static void gc() {
        log("Starting garbage collector: " + memoryStatus());
        System.gc();
        log("Garbage collector completed: " + memoryStatus());
    }
    
    public static void sleepFor(final long millis) {
        try {
            Thread.sleep(millis);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static URL makeURL(final String p) {
        try {
            return new URL(p);
        }
        catch (Exception e) {
            log("Cannot make URL from string: " + p + ".", e);
            return null;
        }
    }
    
    public static URI makeURI(final URL url) {
        try {
            return url.toURI();
        }
        catch (Exception e) {
            log("Cannot make URI from URL: " + url + ".", e);
            return null;
        }
    }
    
    public static URI makeURI(final String p) {
        return makeURI(makeURL(p));
    }
    
    private static int fitInterval(final int val, final int min, final int max) {
        if (val > max) {
            return max;
        }
        if (val < min) {
            return min;
        }
        return val;
    }
    
    public static long m() {
        return System.currentTimeMillis();
    }
    
    public static long n() {
        return System.nanoTime();
    }
    
    public static int getReadTimeout() {
        return getConnectionTimeout();
    }
    
    public static int getConnectionTimeout() {
        final TLauncher t = TLauncher.getInstance();
        if (t == null) {
            return 15000;
        }
        final ConnectionQuality quality = t.getConfiguration().getConnectionQuality();
        if (quality == null) {
            return 15000;
        }
        return quality.getTimeout();
    }
    
    public static Proxy getProxy() {
        return Proxy.NO_PROXY;
    }
    
    public static <K, E> LinkedHashMap<K, E> sortMap(final Map<K, E> map, final K[] sortedKeys) {
        if (map == null) {
            return null;
        }
        if (sortedKeys == null) {
            throw new NullPointerException("Keys cannot be NULL!");
        }
        final LinkedHashMap<K, E> result = new LinkedHashMap<K, E>();
        for (final K key : sortedKeys) {
            for (final Map.Entry<K, E> entry : map.entrySet()) {
                final K entryKey = entry.getKey();
                final E value = entry.getValue();
                if (key == null && entryKey == null) {
                    result.put(null, value);
                    break;
                }
                if (key == null) {
                    continue;
                }
                if (!key.equals(entryKey)) {
                    continue;
                }
                result.put(key, value);
                break;
            }
        }
        return result;
    }
    
    public static Color shiftColor(final Color color, final int bits) {
        if (color == null) {
            return null;
        }
        if (bits == 0) {
            return color;
        }
        final int newRed = fitInterval(color.getRed() + bits, 0, 255);
        final int newGreen = fitInterval(color.getGreen() + bits, 0, 255);
        final int newBlue = fitInterval(color.getBlue() + bits, 0, 255);
        return new Color(newRed, newGreen, newBlue, color.getAlpha());
    }
    
    public static Color shiftAlpha(final Color color, final int bits) {
        if (color == null) {
            return null;
        }
        if (bits == 0) {
            return color;
        }
        final int newAlpha = fitInterval(color.getAlpha() + bits, 0, 255);
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), newAlpha);
    }
    
    @Deprecated
    public static <T> T getAs(final Object o, final Class<T> classOfT) {
        return Reflect.cast(o, classOfT);
    }
    
    public static boolean equal(final Object a, final Object b) {
        return a == b || (a != null && a.equals(b));
    }
    
    public static void close(final Closeable c) {
        try {
            c.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    public static <T> int find(final T obj, final T[] array) {
        if (obj == null) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i] == null) {
                    return i;
                }
            }
        }
        else {
            for (int i = 0; i < array.length; ++i) {
                if (obj.equals(array[i])) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    public static void initializeLoggerU(String path, final String type, final int counter) {
        if (path == null || path.isEmpty()) {
            path = MinecraftUtil.getDefaultWorkingDirectory().getAbsolutePath();
        }
        final String separator = System.getProperty("file.separator");
        path = path + separator + "logs" + separator + "tlauncher" + separator + type + counter + ".log";
        final FileAppender appender = new FileAppender();
        appender.setName("fileAppender");
        appender.setLayout(new PatternLayout("%d{ISO8601} %t %m%n"));
        appender.setFile(path);
        appender.setAppend(true);
        appender.setThreshold(Level.INFO);
        appender.activateOptions();
        appender.setBufferedIO(true);
        appender.setEncoding(StandardCharsets.UTF_8.displayName());
        Logger.getRootLogger().addAppender(appender);
        U.logField = Logger.getLogger("main");
    }
    
    public static void debug(final Object... ob) {
        if (!TLauncher.DEBUG) {
            return;
        }
        plog("[DEBUG] ----- ", ob);
    }
    
    public static URI fixInvallidLink(String link) {
        try {
            if (link.contains("|")) {
                debug("U", "replace |");
                return new URI(link.replace("|", "%7C"));
            }
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        try {
            if (link.contains("|")) {
                debug("U", "replace |");
                link = link.replace("|", "%7C");
            }
            if (link.contains("?")) {
                return new URI(link.substring(0, link.indexOf("?")));
            }
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }
    
    public static <T> void classNameLog(final Class<T> cl, final Object message) {
        log("[" + cl.getSimpleName() + "] ", message);
    }
    
    static {
        lock = new Object();
    }
}
