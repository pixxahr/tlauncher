// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.swing;

public enum FontTL
{
    ROBOTO_BOLD, 
    ROBOTO_REGULAR, 
    ROBOTO_MEDIUM, 
    CALIBRI, 
    CALIBRI_BOLD;
}
