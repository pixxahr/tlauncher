// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.gson.serializer;

import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonParseException;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import com.google.gson.JsonDeserializer;
import org.tlauncher.modpack.domain.client.version.MapMetadataDTO;
import com.google.gson.JsonSerializer;

public class MetadataDTOAdapter implements JsonSerializer<MapMetadataDTO>, JsonDeserializer<MapMetadataDTO>
{
    @Override
    public MapMetadataDTO deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        final MapMetadataDTO meta = context.deserialize(json, MapMetadataDTO.class);
        return meta;
    }
    
    @Override
    public JsonElement serialize(final MapMetadataDTO src, final Type typeOfSrc, final JsonSerializationContext context) {
        return context.serialize(src);
    }
}
