// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.gson.serializer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.HashSet;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import org.tlauncher.modpack.domain.client.share.Category;
import org.tlauncher.modpack.domain.client.share.JavaEnum;
import java.util.List;
import java.util.Set;
import java.util.Map;

public class ElementCollectionsPool
{
    private static Map<String, String> stringElements;
    private static Map<Set<String>, Set<String>> set;
    private static Map<List<JavaEnum>, List<JavaEnum>> javaEnums;
    private static Map<List<Category>, List<Category>> categories;
    
    public static void fill(final GameEntityDTO en) {
        if (en.getVersions() == null) {
            return;
        }
        for (final VersionDTO v : en.getVersions()) {
            if (v.getGameVersions() != null) {
                final Set<String> games = new HashSet<String>();
                for (String s : v.getGameVersions()) {
                    if (!ElementCollectionsPool.stringElements.containsKey(s)) {
                        ElementCollectionsPool.stringElements.put(s, s);
                        s = ElementCollectionsPool.stringElements.get(s);
                    }
                    games.add(ElementCollectionsPool.stringElements.get(s));
                }
                if (ElementCollectionsPool.set.containsKey(games)) {
                    v.setGameVersions(ElementCollectionsPool.set.get(games));
                }
                else {
                    v.setGameVersions(games);
                    ElementCollectionsPool.set.put(games, games);
                }
            }
            final List<JavaEnum> java = v.getJavaVersions();
            if (java != null) {
                if (ElementCollectionsPool.javaEnums.containsKey(java)) {
                    v.setJavaVersions(ElementCollectionsPool.javaEnums.get(java));
                }
                else {
                    ElementCollectionsPool.javaEnums.put(java, java);
                }
            }
            final List<Category> listCategories = en.getCategories();
            if (listCategories != null) {
                if (ElementCollectionsPool.categories.containsKey(listCategories)) {
                    en.setCategories(ElementCollectionsPool.categories.get(listCategories));
                }
                else {
                    ElementCollectionsPool.categories.put(listCategories, listCategories);
                }
            }
        }
    }
    
    static {
        ElementCollectionsPool.stringElements = new HashMap<String, String>();
        ElementCollectionsPool.set = new HashMap<Set<String>, Set<String>>();
        ElementCollectionsPool.javaEnums = new HashMap<List<JavaEnum>, List<JavaEnum>>();
        ElementCollectionsPool.categories = new HashMap<List<Category>, List<Category>>();
    }
}
