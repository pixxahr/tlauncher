// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.gson.serializer;

import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonParseException;
import com.google.gson.JsonObject;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import org.tlauncher.modpack.domain.client.version.ModpackVersionDTO;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import org.tlauncher.modpack.domain.client.ResourcePackDTO;
import org.tlauncher.modpack.domain.client.MapDTO;
import org.tlauncher.modpack.domain.client.ModDTO;
import java.lang.reflect.Type;
import net.minecraft.launcher.versions.json.DateTypeAdapter;
import java.util.Date;
import com.google.gson.TypeAdapterFactory;
import net.minecraft.launcher.versions.json.LowerCaseEnumTypeAdapterFactory;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializer;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import com.google.gson.JsonSerializer;

public class ModpackDTOTypeAdapter implements JsonSerializer<ModpackDTO>, JsonDeserializer<ModpackDTO>
{
    private Gson gson;
    
    public ModpackDTOTypeAdapter() {
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        builder.registerTypeAdapter(ModDTO.class, new ModDTOTypeAdapter());
        builder.registerTypeAdapter(MapDTO.class, new MapDTOTypeAdapter());
        builder.registerTypeAdapter(ResourcePackDTO.class, new ResourcePackDTOTypeAdapter());
        builder.enableComplexMapKeySerialization();
        this.gson = builder.create();
    }
    
    @Override
    public ModpackDTO deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final ModpackDTO dto = this.gson.fromJson(jsonElement, ModpackDTO.class);
        final JsonObject object = jsonElement.getAsJsonObject();
        dto.setVersion(this.gson.fromJson(object.get("version"), ModpackVersionDTO.class));
        dto.setVersions(this.gson.fromJson(object.get("versions"), new TypeToken<List<ModpackVersionDTO>>() {}.getType()));
        ElementCollectionsPool.fill(dto);
        return dto;
    }
    
    @Override
    public JsonElement serialize(final ModpackDTO modpackDTO, final Type type, final JsonSerializationContext jsonSerializationContext) {
        return this.gson.toJsonTree(modpackDTO);
    }
}
