// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.gson.serializer;

import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonParseException;
import com.google.gson.JsonObject;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import org.tlauncher.modpack.domain.client.version.VersionDTO;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import java.lang.reflect.Type;
import net.minecraft.launcher.versions.json.DateTypeAdapter;
import java.util.Date;
import com.google.gson.TypeAdapterFactory;
import net.minecraft.launcher.versions.json.LowerCaseEnumTypeAdapterFactory;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializer;
import org.tlauncher.modpack.domain.client.MapDTO;
import com.google.gson.JsonSerializer;

public class MapDTOTypeAdapter implements JsonSerializer<MapDTO>, JsonDeserializer<MapDTO>
{
    private Gson gson;
    
    public MapDTOTypeAdapter() {
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        builder.registerTypeAdapter(MetadataDTO.class, new MetadataDTOAdapter());
        builder.enableComplexMapKeySerialization();
        this.gson = builder.create();
    }
    
    @Override
    public MapDTO deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final MapDTO dto = this.gson.fromJson(jsonElement, MapDTO.class);
        final JsonObject object = jsonElement.getAsJsonObject();
        final VersionDTO version = this.gson.fromJson(object.get("version"), VersionDTO.class);
        dto.setVersions(this.gson.fromJson(object.get("versions"), new TypeToken<List<VersionDTO>>() {}.getType()));
        dto.setVersion(version);
        ElementCollectionsPool.fill(dto);
        return dto;
    }
    
    @Override
    public JsonElement serialize(final MapDTO modpackDTO, final Type type, final JsonSerializationContext jsonSerializationContext) {
        return this.gson.toJsonTree(modpackDTO);
    }
}
