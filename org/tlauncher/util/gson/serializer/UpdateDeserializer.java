// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.gson.serializer;

import com.google.gson.JsonParseException;
import com.google.gson.JsonObject;
import org.tlauncher.tlauncher.updater.client.Offer;
import java.util.function.Consumer;
import java.util.Collections;
import org.tlauncher.tlauncher.updater.client.Banner;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import java.util.Map;
import org.tlauncher.util.U;
import com.google.gson.JsonDeserializationContext;
import java.lang.reflect.Type;
import com.google.gson.JsonElement;
import org.tlauncher.tlauncher.updater.client.Update;
import com.google.gson.JsonDeserializer;

public class UpdateDeserializer implements JsonDeserializer<Update>
{
    @Override
    public Update deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) {
        try {
            return this.deserialize0(json, context);
        }
        catch (Exception e) {
            U.log("Cannot parse update:", e);
            return new Update();
        }
    }
    
    private Update deserialize0(final JsonElement json, final JsonDeserializationContext context) {
        final JsonObject object = json.getAsJsonObject();
        final Update update = new Update();
        update.setVersion(object.get("version").getAsDouble());
        update.setMandatory(object.get("mandatory").getAsBoolean());
        update.setRequiredAtLeastFor(object.has("requiredAtLeastFor") ? object.get("requiredAtLeastFor").getAsDouble() : 0.0);
        final Map<String, String> description = context.deserialize(object.get("description"), new TypeToken<Map<String, String>>() {}.getType());
        if (description != null) {
            update.setDescription(description);
        }
        final List<String> jarLinks = context.deserialize(object.get("jarLinks"), new TypeToken<List<String>>() {}.getType());
        if (jarLinks != null) {
            update.setJarLinks(jarLinks);
        }
        final List<String> exeLinks = context.deserialize(object.get("exeLinks"), new TypeToken<List<String>>() {}.getType());
        if (exeLinks != null) {
            update.setExeLinks(exeLinks);
        }
        update.setUpdaterView(object.get("updaterView").getAsInt());
        update.setOfferDelay(object.get("offerDelay").getAsInt());
        update.setOfferEmptyCheckboxDelay(object.get("offerEmptyCheckboxDelay").getAsInt());
        update.setUpdaterLaterInstall(object.get("updaterLaterInstall").getAsBoolean());
        final Map<String, List<Banner>> banners = context.deserialize(object.get("banners"), new TypeToken<Map<String, List<Banner>>>() {}.getType());
        banners.values().forEach(Collections::shuffle);
        update.setBanners(banners);
        final List<Offer> offers = context.deserialize(object.get("offers"), new TypeToken<List<Offer>>() {}.getType());
        Collections.shuffle(offers);
        update.setOffers(offers);
        update.setRootAccessExe(context.deserialize(object.get("rootAccessExe"), new TypeToken<List<String>>() {}.getType()));
        return update;
    }
}
