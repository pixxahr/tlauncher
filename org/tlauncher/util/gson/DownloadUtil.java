// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.gson;

import net.minecraft.launcher.versions.json.PatternTypeAdapter;
import java.util.regex.Pattern;
import com.google.gson.GsonBuilder;
import org.tlauncher.util.U;
import java.util.Iterator;
import java.util.List;
import net.minecraft.launcher.Http;
import java.net.URL;
import org.tlauncher.tlauncher.rmo.Bootstrapper;
import java.util.Arrays;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.lang.reflect.Type;
import org.tlauncher.tlauncher.repository.Repo;
import java.io.IOException;
import com.google.gson.Gson;

public class DownloadUtil
{
    private static final Gson gson;
    private static final int CONNECT_TIMEOUT = 5000;
    private static final int READ_TIMEOUT = 15000;
    
    public static <T> T loadObjectByKey(final String key, final Class<T> cl) throws IOException {
        final String result = readTextByKey(key, "", true);
        return DownloadUtil.gson.fromJson(result, cl);
    }
    
    public static <T> T loadByRepository(final Repo repo, final Class<T> cl) throws IOException {
        return DownloadUtil.gson.fromJson(repo.getUrl(), cl);
    }
    
    public static <T> T loadByRepository(final Repo repository, final Type type) throws IOException {
        return DownloadUtil.gson.fromJson(repository.getUrl(), type);
    }
    
    public static <T> T loadObjectByKey(final String key, final Type type, final boolean flag) throws IOException {
        return DownloadUtil.gson.fromJson(readTextByKey(key, "", flag), type);
    }
    
    public static String readTextByKey(final String key, final String postfix, final boolean innerConfig) throws IOException {
        List<String> urls;
        if (innerConfig) {
            urls = Arrays.asList(TLauncher.getInnerSettings().get(key).split(","));
        }
        else {
            urls = Arrays.asList(Bootstrapper.innerConfig.get(key).split(","));
        }
        IOException error = null;
        for (final String url : urls) {
            log("request to " + url);
            try {
                return Http.performGet(new URL(url + postfix), 5000, 15000);
            }
            catch (IOException ex) {
                error = ex;
                continue;
            }
            break;
        }
        throw new IOException("couldn't download ", error);
    }
    
    private static void log(final String line) {
        U.log("[GsonUtil] ", line);
    }
    
    static {
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Pattern.class, new PatternTypeAdapter());
        builder.setPrettyPrinting();
        gson = builder.create();
    }
}
