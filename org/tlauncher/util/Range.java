// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

public class Range<T extends Number>
{
    private final T minValue;
    private final T maxValue;
    private final boolean including;
    private final double doubleMin;
    private final double doubleMax;
    
    public Range(final T minValue, final T maxValue, final boolean including) {
        if (minValue == null) {
            throw new NullPointerException("min");
        }
        if (maxValue == null) {
            throw new NullPointerException("max");
        }
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.doubleMin = minValue.doubleValue();
        this.doubleMax = maxValue.doubleValue();
        if (this.doubleMin >= this.doubleMax) {
            throw new IllegalArgumentException("min >= max");
        }
        this.including = including;
    }
    
    public Range(final T minValue, final T maxValue) {
        this(minValue, maxValue, true);
    }
    
    public T getMinValue() {
        return this.minValue;
    }
    
    public T getMaxValue() {
        return this.maxValue;
    }
    
    public boolean getIncluding() {
        return this.including;
    }
    
    public RangeDifference getDifference(final T value) {
        if (value == null) {
            throw new NullPointerException("value");
        }
        final double doubleValue = value.doubleValue();
        final double min = doubleValue - this.doubleMin;
        if (min == 0.0) {
            return this.including ? RangeDifference.FITS : RangeDifference.LESS;
        }
        if (min < 0.0) {
            return RangeDifference.LESS;
        }
        final double max = doubleValue - this.doubleMax;
        if (max == 0.0) {
            return this.including ? RangeDifference.FITS : RangeDifference.GREATER;
        }
        if (max > 0.0) {
            return RangeDifference.GREATER;
        }
        return RangeDifference.FITS;
    }
    
    public boolean fits(final T value) {
        return this.getDifference(value) == RangeDifference.FITS;
    }
    
    public enum RangeDifference
    {
        LESS, 
        FITS, 
        GREATER;
    }
}
