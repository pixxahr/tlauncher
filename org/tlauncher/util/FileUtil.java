// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import org.tlauncher.modpack.domain.client.ResourcePackDTO;
import org.tlauncher.modpack.domain.client.ModDTO;
import org.tlauncher.modpack.domain.client.share.GameType;
import java.io.FileNotFoundException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.tlauncher.exceptions.ParseModPackException;
import com.github.junrar.extract.ExtractArchive;
import org.apache.commons.io.FilenameUtils;
import java.io.ByteArrayInputStream;
import java.util.Map;
import java.util.Iterator;
import com.google.common.collect.Lists;
import org.tlauncher.modpack.domain.client.version.MapMetadataDTO;
import org.tlauncher.modpack.domain.client.MapDTO;
import org.tlauncher.modpack.domain.client.version.MetadataDTO;
import org.tlauncher.modpack.domain.client.GameEntityDTO;
import java.util.Enumeration;
import org.apache.commons.io.IOUtils;
import java.util.zip.ZipFile;
import com.github.junrar.rarfile.FileHeader;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import com.github.junrar.Archive;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.FileStore;
import java.util.zip.ZipEntry;
import java.nio.file.Files;
import java.util.zip.ZipInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.math.BigInteger;
import java.io.Closeable;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.net.URL;
import org.apache.commons.io.Charsets;
import java.io.InputStreamReader;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.zip.ZipOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Set;
import org.apache.log4j.Logger;

public class FileUtil
{
    public static final String DEFAULT_CHARSET = "UTF-8";
    private static final Logger log4j;
    public static final String NAME_ARCHIVE = "logOfFiles.zip";
    public static final Long SIZE_100;
    public static final Long SIZE_200;
    public static final Long SIZE_300;
    public static Set<PosixFilePermission> PERMISSIONS;
    
    public static Charset getCharset() {
        try {
            return Charset.forName("UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static void writeFile(final File file, final String text) throws IOException {
        createFile(file);
        final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        final OutputStreamWriter ow = new OutputStreamWriter(os, "UTF-8");
        ow.write(text);
        ow.close();
        os.close();
    }
    
    public static void writeZipEntry(final ZipOutputStream zip, final File fileRead) throws IOException {
        try (final FileInputStream in = new FileInputStream(fileRead)) {
            final byte[] array = new byte[8192];
            while (in.read(array) != -1) {
                zip.write(array);
            }
        }
        catch (IOException e) {
            throw new IOException(e);
        }
    }
    
    public static String readStream(final InputStream is, final String charset) throws IOException {
        try (final InputStreamReader reader = new InputStreamReader(new BufferedInputStream(is), charset)) {
            final StringBuilder b = new StringBuilder();
            while (reader.ready()) {
                b.append((char)reader.read());
            }
            return b.toString();
        }
    }
    
    public static String readStream(final InputStream is) throws IOException {
        return readStream(is, Charsets.UTF_8.displayName());
    }
    
    public static String getTextResource(final URL url, final String charset) throws IOException {
        return readStream(url.openStream(), charset);
    }
    
    public static String readFile(final File file, final String charset) throws IOException {
        return readStream(new FileInputStream(file), charset);
    }
    
    public static String readFile(final File file) throws IOException {
        return readFile(file, "UTF-8");
    }
    
    public static String getFilename(final String path) {
        final String[] folders = path.split("/");
        final int size = folders.length;
        if (size == 0) {
            return "";
        }
        return folders[size - 1];
    }
    
    public static String getDigest(final File file, final String algorithm, final int hashLength) {
        DigestInputStream stream = null;
        try {
            stream = new DigestInputStream(new FileInputStream(file), MessageDigest.getInstance(algorithm));
            final byte[] buffer = new byte[65536];
            int read;
            do {
                read = stream.read(buffer);
            } while (read > 0);
        }
        catch (Exception ignored) {
            return null;
        }
        finally {
            close(stream);
        }
        return String.format("%1$0" + hashLength + "x", new BigInteger(1, stream.getMessageDigest().digest()));
    }
    
    private static byte[] createChecksum(final File file, final String algorithm) {
        InputStream fis = null;
        try {
            fis = new BufferedInputStream(new FileInputStream(file));
            final byte[] buffer = new byte[8192];
            final MessageDigest complete = MessageDigest.getInstance(algorithm);
            int numRead;
            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);
            return complete.digest();
        }
        catch (Exception e) {
            return null;
        }
        finally {
            close(fis);
        }
    }
    
    public static String getChecksum(final File file, final String algorithm) {
        if (file == null) {
            return null;
        }
        if (!file.exists()) {
            return null;
        }
        final byte[] b = createChecksum(file, algorithm);
        if (b == null) {
            return null;
        }
        final StringBuilder result = new StringBuilder();
        for (final byte cb : b) {
            result.append(Integer.toString((cb & 0xFF) + 256, 16).substring(1));
        }
        return result.toString();
    }
    
    public static String getChecksum(final File file) {
        return getChecksum(file, "SHA-1");
    }
    
    private static void close(final Closeable a) {
        try {
            a.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static File getRunningJar() {
        try {
            return new File(URLDecoder.decode(FileUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8"));
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Cannot get running file!", e);
        }
    }
    
    public static void copyFile(final File source, final File dest, final boolean replace) throws IOException {
        if (dest.isFile()) {
            if (!replace) {
                return;
            }
        }
        else {
            createFile(dest);
        }
        try (final InputStream is = new BufferedInputStream(new FileInputStream(source));
             final OutputStream os = new BufferedOutputStream(new FileOutputStream(dest))) {
            final byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        }
    }
    
    public static void deleteFile(final File file) {
        final boolean onExit = !file.delete();
        if (onExit) {
            file.deleteOnExit();
            return;
        }
        final File parent = file.getParentFile();
        if (parent == null) {
            return;
        }
        final File[] list = parent.listFiles();
        if (list == null || list.length > 0) {
            return;
        }
        deleteFile(parent);
    }
    
    public static void deleteDirectory(final File dir) {
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("Specified path is not a directory: " + dir.getAbsolutePath());
        }
        final File[] files = dir.listFiles();
        if (Objects.isNull(files)) {
            return;
        }
        for (final File file : files) {
            if (file.isDirectory()) {
                deleteDirectory(file);
            }
            else {
                deleteFile(file);
            }
        }
        deleteFile(dir);
    }
    
    public static File makeTemp(final File file) throws IOException {
        createFile(file);
        file.deleteOnExit();
        return file;
    }
    
    public static boolean createFolder(final File dir) throws IOException {
        if (dir == null) {
            throw new NullPointerException();
        }
        if (dir.isDirectory()) {
            return false;
        }
        if (!dir.mkdirs()) {
            throw new IOException("Cannot createScrollWrapper folders: " + dir.getAbsolutePath());
        }
        if (!dir.canWrite()) {
            throw new IOException("Ceated directory is not accessible: " + dir.getAbsolutePath());
        }
        return true;
    }
    
    public static void createFile(final File file) throws IOException {
        if (file.isFile()) {
            return;
        }
        if (file.getParentFile() != null) {
            file.getParentFile().mkdirs();
        }
        if (!file.createNewFile()) {
            throw new IOException("Cannot createScrollWrapper file, or it was created during runtime: " + file.getAbsolutePath());
        }
    }
    
    public static void unZip(final File zip, final File folder, final boolean replace, final boolean deleteEmptyFile) throws IOException {
        createFolder(folder);
        try (final ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zip)), StandardCharsets.UTF_8)) {
            final byte[] buffer = new byte[1024];
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                final String fileName = ze.getName();
                if (ze.isDirectory()) {
                    continue;
                }
                final File newFile = new File(folder, fileName);
                if (!replace && newFile.isFile()) {
                    continue;
                }
                createFile(newFile);
                final OutputStream fos = new BufferedOutputStream(new FileOutputStream(newFile));
                int count = 0;
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    count += len;
                    fos.write(buffer, 0, len);
                }
                fos.close();
                if (!deleteEmptyFile || count != 0) {
                    continue;
                }
                Files.delete(newFile.toPath());
            }
            zis.closeEntry();
        }
    }
    
    public static void unZip(final File zip, final File folder, final boolean replace) throws IOException {
        unZip(zip, folder, replace, true);
    }
    
    public static String getResource(final URL resource, final String charset) throws IOException {
        final InputStream is = new BufferedInputStream(resource.openStream());
        final InputStreamReader reader = new InputStreamReader(is, charset);
        final StringBuilder b = new StringBuilder();
        while (reader.ready()) {
            b.append((char)reader.read());
        }
        reader.close();
        return b.toString();
    }
    
    public static String getResource(final URL resource) throws IOException {
        return getResource(resource, "UTF-8");
    }
    
    public static String getFolder(final URL url, final String separator) {
        final String[] folders = url.toString().split(separator);
        final StringBuilder s = new StringBuilder();
        for (int i = 0; i < folders.length - 1; ++i) {
            s.append(folders[i]).append(separator);
        }
        return s.toString();
    }
    
    public static String getExtension(final File f) {
        if (!f.isFile() && f.isDirectory()) {
            return null;
        }
        String ext = "";
        final String s = f.getName();
        final int i = s.lastIndexOf(46);
        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }
    
    public static boolean checkFreeSpace(final File file, final long size) {
        try {
            final FileStore store = Files.getFileStore(file.toPath().getRoot());
            final long res = store.getUsableSpace();
            return res > size;
        }
        catch (IOException ex) {
            return true;
        }
    }
    
    public static InputStream getResourceAppStream(final String name) {
        return FileUtil.class.getResourceAsStream(name);
    }
    
    public static Path getRelative(final String path) {
        return Paths.get(MinecraftUtil.getWorkingDirectory().getAbsolutePath(), path);
    }
    
    public static Path getRelativeConfig(final String path) {
        return getRelative(TLauncher.getInnerSettings().get(path));
    }
    
    public static File getRelativeConfigFile(final String path) {
        return getRelative(TLauncher.getInnerSettings().get(path)).toFile();
    }
    
    public static List<String> topFolders(final Archive rar) {
        FileHeader fh = rar.nextFileHeader();
        final Set<String> list = new HashSet<String>();
        while (fh != null) {
            final String line = Paths.get(fh.getFileNameW(), new String[0]).toString();
            if (line.indexOf(File.separator) > 0) {
                list.add(line.substring(0, line.indexOf(File.separator)));
            }
            fh = rar.nextFileHeader();
        }
        return new ArrayList<String>(list);
    }
    
    public static List<String> topFolders(final ZipFile zipFile) {
        final Set<String> list = new HashSet<String>();
        final Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            final ZipEntry entry = (ZipEntry)entries.nextElement();
            final String line = Paths.get(entry.getName(), new String[0]).toString();
            int index = line.indexOf("/");
            if (index == -1) {
                index = line.indexOf("\\");
            }
            if (index > 0) {
                list.add(line.substring(0, index));
            }
        }
        IOUtils.closeQuietly(zipFile);
        return new ArrayList<String>(list);
    }
    
    public static MetadataDTO createMetadata(final File file, final File rootFolder, final Class<? extends GameEntityDTO> c) {
        MetadataDTO metadata;
        if (c == MapDTO.class) {
            final MapMetadataDTO map = new MapMetadataDTO();
            map.setFolders(Lists.newArrayList((Object[])new String[] { file.getName() }));
            metadata = map;
        }
        else {
            metadata = new MetadataDTO();
            metadata.setSha1(getChecksum(file, "SHA-1"));
            metadata.setSize(file.length());
        }
        metadata.setPath(rootFolder.toPath().relativize(file.toPath()).toString().replace("\\", "/"));
        metadata.setUrl(rootFolder.toPath().relativize(file.toPath()).toString().replace("\\", "/"));
        return metadata;
    }
    
    public static void zipFiles(final List<File> files, final Path root, final File result) throws IOException {
        final FileOutputStream fos = new FileOutputStream(result);
        final ZipOutputStream zipOut = new ZipOutputStream(new BufferedOutputStream(fos), StandardCharsets.UTF_8);
        for (final File input : files) {
            final FileInputStream fis = new FileInputStream(input);
            final ZipEntry ze = new ZipEntry(root.relativize(input.toPath()).toString().replaceAll("\\\\", "/"));
            zipOut.putNextEntry(ze);
            final byte[] tmp = new byte[4096];
            int size;
            while ((size = fis.read(tmp)) != -1) {
                zipOut.write(tmp, 0, size);
            }
            zipOut.flush();
            fis.close();
        }
        zipOut.close();
    }
    
    public static void backupModpacks(final Map<String, String> map, final List<File> files, final Path root, final File result, final List<String> modpacks) throws IOException {
        final FileOutputStream fos = new FileOutputStream(result);
        final ZipOutputStream zipOut = new ZipOutputStream(new BufferedOutputStream(fos), StandardCharsets.UTF_8);
        for (final File input : files) {
            InputStream fis;
            if (map.containsKey(input.getName())) {
                fis = new ByteArrayInputStream(map.get(input.getName()).getBytes(StandardCharsets.UTF_8));
            }
            else {
                fis = new FileInputStream(input);
            }
            final ZipEntry ze = new ZipEntry(root.relativize(input.toPath()).toString().replaceAll("\\\\", "/"));
            U.log(ze.getName());
            zipOut.putNextEntry(ze);
            final byte[] tmp = new byte[4096];
            int size;
            while ((size = fis.read(tmp)) != -1) {
                zipOut.write(tmp, 0, size);
            }
            zipOut.flush();
            fis.close();
        }
        zipOut.close();
    }
    
    public static void unzipUniversal(final File file, final File folder) throws IOException, ParseModPackException {
        final String extension;
        final String ext = extension = FilenameUtils.getExtension(file.getCanonicalPath());
        switch (extension) {
            case "rar": {
                final ExtractArchive extractArchive = new ExtractArchive();
                extractArchive.extractArchive(file, folder);
                break;
            }
            case "zip": {
                unZip(file, folder, true);
                break;
            }
            default: {
                throw new ParseModPackException("problem with format of the modpack");
            }
        }
    }
    
    public static void zipFolder(final File srcFolder, final File destZipFile) throws Exception {
        final List<File> files = (List<File>)(List)FileUtils.listFiles(srcFolder, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        zipFiles(files, srcFolder.toPath().getParent(), destZipFile);
    }
    
    public static String readZippedFile(final File file, final String name) throws IOException {
        try (final ZipFile f = new ZipFile(file)) {
            final Enumeration<? extends ZipEntry> en = f.entries();
            while (en.hasMoreElements()) {
                final ZipEntry entry = (ZipEntry)en.nextElement();
                if (entry.getName().endsWith(name)) {
                    return readStream(f.getInputStream(entry));
                }
            }
        }
        throw new FileNotFoundException(file.toString());
    }
    
    static {
        log4j = Logger.getLogger(FileUtil.class);
        SIZE_100 = 104857600L;
        SIZE_200 = 209715200L;
        SIZE_300 = 314572800L;
        FileUtil.PERMISSIONS = new HashSet<PosixFilePermission>() {
            {
                this.add(PosixFilePermission.OWNER_READ);
                this.add(PosixFilePermission.OWNER_WRITE);
                this.add(PosixFilePermission.OWNER_EXECUTE);
                this.add(PosixFilePermission.OTHERS_READ);
                this.add(PosixFilePermission.OTHERS_WRITE);
                this.add(PosixFilePermission.OTHERS_EXECUTE);
                this.add(PosixFilePermission.GROUP_READ);
                this.add(PosixFilePermission.GROUP_WRITE);
                this.add(PosixFilePermission.GROUP_EXECUTE);
            }
        };
    }
    
    public enum GameEntityFolder
    {
        MODS, 
        RESOURCEPACKS, 
        MAPS, 
        SAVES, 
        RESOURCES;
        
        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
        
        public static String getPath(final GameType type) {
            switch (type) {
                case MOD: {
                    return GameEntityFolder.MODS.toString().toLowerCase();
                }
                case MAP: {
                    return GameEntityFolder.SAVES.toString().toLowerCase();
                }
                case RESOURCEPACK: {
                    return GameEntityFolder.RESOURCEPACKS.toString().toLowerCase();
                }
                default: {
                    return "";
                }
            }
        }
        
        public static String getPath(final Class<? extends GameEntityDTO> type, final boolean folderSeparate) {
            String path = "";
            if (type == ModDTO.class) {
                path = getPath(GameType.MOD);
            }
            else if (type == MapDTO.class) {
                path = getPath(GameType.MAP);
            }
            else if (type == ResourcePackDTO.class) {
                path = getPath(GameType.RESOURCEPACK);
            }
            if (folderSeparate) {
                return path + "/";
            }
            return path;
        }
    }
}
