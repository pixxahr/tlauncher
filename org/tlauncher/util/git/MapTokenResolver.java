// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.git;

import java.util.HashMap;
import java.util.Map;

public class MapTokenResolver implements ITokenResolver
{
    protected Map<String, String> tokenMap;
    
    public MapTokenResolver(final Map<String, String> tokenMap) {
        this.tokenMap = new HashMap<String, String>();
        this.tokenMap = tokenMap;
    }
    
    @Override
    public String resolveToken(final String tokenName) {
        return this.tokenMap.get(tokenName);
    }
}
