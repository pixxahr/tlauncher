// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.git;

import java.io.IOException;
import java.nio.CharBuffer;
import java.io.PushbackReader;
import java.io.Reader;

public class TokenReplacingReader extends Reader
{
    protected PushbackReader pushbackReader;
    protected ITokenResolver tokenResolver;
    protected StringBuilder tokenNameBuffer;
    protected String tokenValue;
    protected int tokenValueIndex;
    
    public TokenReplacingReader(final Reader source, final ITokenResolver resolver) {
        this.pushbackReader = null;
        this.tokenResolver = null;
        this.tokenNameBuffer = new StringBuilder();
        this.tokenValue = null;
        this.tokenValueIndex = 0;
        this.pushbackReader = new PushbackReader(source, 2);
        this.tokenResolver = resolver;
    }
    
    @Override
    public int read(final CharBuffer target) throws IOException {
        throw new RuntimeException("Operation Not Supported");
    }
    
    @Override
    public int read() throws IOException {
        if (this.tokenValue != null) {
            if (this.tokenValueIndex < this.tokenValue.length()) {
                return this.tokenValue.charAt(this.tokenValueIndex++);
            }
            if (this.tokenValueIndex == this.tokenValue.length()) {
                this.tokenValue = null;
                this.tokenValueIndex = 0;
            }
        }
        int data = this.pushbackReader.read();
        if (data != 36) {
            return data;
        }
        data = this.pushbackReader.read();
        if (data != 123) {
            this.pushbackReader.unread(data);
            return 36;
        }
        this.tokenNameBuffer.delete(0, this.tokenNameBuffer.length());
        for (data = this.pushbackReader.read(); data != 125; data = this.pushbackReader.read()) {
            this.tokenNameBuffer.append((char)data);
        }
        this.tokenValue = this.tokenResolver.resolveToken(this.tokenNameBuffer.toString());
        if (this.tokenValue == null) {
            this.tokenValue = "${" + this.tokenNameBuffer.toString() + "}";
        }
        if (this.tokenValue.length() == 0) {
            return this.read();
        }
        return this.tokenValue.charAt(this.tokenValueIndex++);
    }
    
    @Override
    public int read(final char[] cbuf) throws IOException {
        return this.read(cbuf, 0, cbuf.length);
    }
    
    @Override
    public int read(final char[] cbuf, final int off, final int len) throws IOException {
        int charsRead = 0;
        int i = 0;
        while (i < len) {
            final int nextChar = this.read();
            if (nextChar == -1) {
                if (charsRead == 0) {
                    charsRead = -1;
                    break;
                }
                break;
            }
            else {
                charsRead = i + 1;
                cbuf[off + i] = (char)nextChar;
                ++i;
            }
        }
        return charsRead;
    }
    
    @Override
    public void close() throws IOException {
        this.pushbackReader.close();
    }
    
    @Override
    public long skip(final long n) throws IOException {
        throw new RuntimeException("Operation Not Supported");
    }
    
    @Override
    public boolean ready() throws IOException {
        return this.pushbackReader.ready();
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public void mark(final int readAheadLimit) throws IOException {
        throw new RuntimeException("Operation Not Supported");
    }
    
    @Override
    public void reset() throws IOException {
        throw new RuntimeException("Operation Not Supported");
    }
}
