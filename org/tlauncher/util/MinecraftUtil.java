// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import org.tlauncher.tlauncher.configuration.Configuration;
import java.io.IOException;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.io.File;

public class MinecraftUtil
{
    public static File getWorkingDirectory() {
        if (TLauncher.getInstance() == null) {
            return getDefaultWorkingDirectory();
        }
        final Configuration settings = TLauncher.getInstance().getConfiguration();
        final String sdir = settings.get("minecraft.gamedir");
        if (sdir == null) {
            return getDefaultWorkingDirectory();
        }
        final File dir = new File(sdir);
        try {
            FileUtil.createFolder(dir);
        }
        catch (IOException e) {
            U.log("Cannot createScrollWrapper specified Minecraft folder:", dir.getAbsolutePath());
            return getDefaultWorkingDirectory();
        }
        return dir;
    }
    
    public static File getSystemRelatedFile(final String path) {
        final String userHome = System.getProperty("user.home", ".");
        File file = null;
        switch (OS.CURRENT) {
            case LINUX:
            case SOLARIS: {
                file = new File(userHome, path);
                break;
            }
            case WINDOWS: {
                final String applicationData = System.getenv("APPDATA");
                final String folder = (applicationData != null) ? applicationData : userHome;
                file = new File(folder, path);
                break;
            }
            case OSX: {
                file = new File(userHome, "Library/Application Support/" + path);
                break;
            }
            default: {
                file = new File(userHome, path);
                break;
            }
        }
        return file;
    }
    
    public static File getSystemRelatedDirectory(String path) {
        if (!OS.is(OS.OSX, OS.UNKNOWN)) {
            path = '.' + path;
        }
        return getSystemRelatedFile(path);
    }
    
    public static File getDefaultWorkingDirectory() {
        return getSystemRelatedDirectory(TLauncher.getFolder());
    }
    
    public static File getTLauncherFile(final String path) {
        return new File(getSystemRelatedDirectory("tlauncher"), path);
    }
}
