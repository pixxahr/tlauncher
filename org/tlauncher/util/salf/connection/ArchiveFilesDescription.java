// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.salf.connection;

public class ArchiveFilesDescription
{
    private String nameFileArchive;
    private int countFiles;
    private int[] sizeFiles;
    
    public ArchiveFilesDescription() {
    }
    
    public ArchiveFilesDescription(final String nameFileArchive, final int countFiles, final int[] sizeFiles) {
        this.nameFileArchive = nameFileArchive;
        this.countFiles = countFiles;
        this.sizeFiles = sizeFiles;
    }
    
    public String getNameFileArchive() {
        return this.nameFileArchive;
    }
    
    public void setNameFileArchive(final String nameFileArchive) {
        this.nameFileArchive = nameFileArchive;
    }
    
    public int getCountFiles() {
        return this.countFiles;
    }
    
    public void setCountFiles(final int countFiles) {
        this.countFiles = countFiles;
    }
    
    public int[] getSizeFiles() {
        return this.sizeFiles;
    }
    
    public void setSizeFiles(final int[] sizeFiles) {
        this.sizeFiles = sizeFiles;
    }
}
