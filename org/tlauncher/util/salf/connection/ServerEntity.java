// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.salf.connection;

public class ServerEntity
{
    private String url;
    private int port;
    
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(final String url) {
        this.url = url;
    }
    
    public int getPort() {
        return this.port;
    }
    
    public void setPort(final int port) {
        this.port = port;
    }
    
    @Override
    public String toString() {
        return "SALFServerEntity [url=" + this.url + ", port=" + this.port + "]";
    }
}
