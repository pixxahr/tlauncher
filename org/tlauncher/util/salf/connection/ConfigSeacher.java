// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.salf.connection;

import org.tlauncher.util.U;
import com.google.gson.JsonSyntaxException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import org.apache.commons.io.IOUtils;
import java.net.URL;
import java.io.IOException;
import java.nio.file.Path;
import com.google.gson.Gson;
import org.tlauncher.util.FileUtil;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.tlauncher.util.MinecraftUtil;

public class ConfigSeacher
{
    private String[] urlServer;
    private String nameFile;
    
    public ConfigSeacher(final String[] urlServer, final String nameFile) {
        this.urlServer = urlServer;
        this.nameFile = nameFile;
    }
    
    private ServerEntity readConfigFromFile() throws IOException {
        final Path p = Paths.get(MinecraftUtil.getWorkingDirectory().getCanonicalPath(), this.nameFile);
        if (!Files.isReadable(p)) {
            this.log("file doesn't readable or exist is " + p.toString());
            return null;
        }
        final String dataJson = FileUtil.readFile(new File(MinecraftUtil.getWorkingDirectory(), this.nameFile));
        return new Gson().fromJson(dataJson, ServerEntity.class);
    }
    
    public ServerEntity saveConfigFromServer() throws IOException {
        IOException io = null;
        final String[] urlServer = this.urlServer;
        final int length = urlServer.length;
        int i = 0;
        while (i < length) {
            final String s = urlServer[i];
            try {
                final String serverText = IOUtils.toString(new URL(s), "utf-8");
                IOUtils.write(serverText, new FileOutputStream(new File(MinecraftUtil.getWorkingDirectory().getCanonicalPath(), this.nameFile)), "utf-8");
                return new Gson().fromJson(serverText, ServerEntity.class);
            }
            catch (IOException e) {
                io = e;
                ++i;
                continue;
            }
            break;
        }
        throw io;
    }
    
    private ServerEntity readOldServer() throws IOException {
        ServerEntity server = null;
        try {
            server = this.readConfigFromFile();
        }
        catch (JsonSyntaxException ex) {
            this.log(ex.getMessage());
        }
        if (server == null) {
            server = this.saveConfigFromServer();
        }
        if (server == null) {
            throw new NullPointerException("didn't receive data from filenameFile=" + this.nameFile + " urlServer=" + this.urlServer);
        }
        return server;
    }
    
    public ServerEntity readServer() throws IOException {
        return this.readOldServer();
    }
    
    private void log(final String line) {
        U.log("[ConfigSeacher] ", line);
    }
}
