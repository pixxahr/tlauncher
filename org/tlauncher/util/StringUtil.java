// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import java.util.Random;
import org.tlauncher.exceptions.ParseException;
import java.nio.ByteBuffer;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CharsetDecoder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class StringUtil
{
    public static final String LINK_PATTERN = "<a style='color:#585858;' href='link'>link</a>";
    
    public static String addSlashes(final String str, final EscapeGroup group) {
        if (str == null) {
            return "";
        }
        final StringBuilder s = new StringBuilder(str);
        for (int i = 0; i < s.length(); ++i) {
            final char curChar = s.charAt(i);
            for (final char c : group.getChars()) {
                if (curChar == c) {
                    s.insert(i++, '\\');
                }
            }
        }
        return s.toString();
    }
    
    public static String[] addSlashes(final String[] str, final EscapeGroup group) {
        if (str == null) {
            return null;
        }
        final int len = str.length;
        final String[] ret = new String[len];
        for (int i = 0; i < len; ++i) {
            ret[i] = addSlashes(str[i], group);
        }
        return ret;
    }
    
    public static String iconv(final String inChar, final String outChar, final String str) {
        final Charset in = Charset.forName(inChar);
        final Charset out = Charset.forName(outChar);
        final CharsetDecoder decoder = in.newDecoder();
        final CharsetEncoder encoder = out.newEncoder();
        try {
            final ByteBuffer bbuf = encoder.encode(CharBuffer.wrap(str));
            final CharBuffer cbuf = decoder.decode(bbuf);
            return cbuf.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static boolean parseBoolean(final String b) throws ParseException {
        if (b == null) {
            throw new ParseException("String cannot be NULL!");
        }
        if (b.equalsIgnoreCase("true")) {
            return true;
        }
        if (b.equalsIgnoreCase("false")) {
            return false;
        }
        throw new ParseException("Cannot parse value (" + b + ")!");
    }
    
    public static int countLines(final String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        int lines = 1;
        for (int len = str.length(), pos = 0; pos < len; ++pos) {
            final char c = str.charAt(pos);
            if (c == '\r') {
                ++lines;
                if (pos + 1 < len && str.charAt(pos + 1) == '\n') {
                    ++pos;
                }
            }
            else if (c == '\n') {
                ++lines;
            }
        }
        return lines;
    }
    
    public static char lastChar(final String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        final int len = str.length();
        if (len == 0) {
            return '\0';
        }
        if (len == 1) {
            return str.charAt(0);
        }
        return str.charAt(len - 1);
    }
    
    public static String randomizeWord(final String str, final boolean softly) {
        if (str == null) {
            return null;
        }
        final int len = str.length();
        if (len < 4) {
            return str;
        }
        final boolean[] reversedFlag = new boolean[len];
        if (softly) {
            reversedFlag[0] = true;
        }
        boolean chosenLastLetter = !softly;
        final char[] chars = str.toCharArray();
        for (int i = len - 1; i > -1; --i) {
            final char curChar = chars[i];
            final int charType = Character.getType(curChar);
            final boolean canBeReversed = charType == 1 || charType == 2;
            final boolean[] array = reversedFlag;
            final int n = i;
            array[n] |= !canBeReversed;
            if (canBeReversed && !chosenLastLetter) {
                reversedFlag[i] = true;
                chosenLastLetter = true;
            }
        }
        for (int i = 0; i < len; ++i) {
            if (!reversedFlag[i]) {
                int newPos = i;
                int tries = 0;
                while (tries < 3) {
                    ++tries;
                    newPos = new Random().nextInt(len);
                    if (reversedFlag[newPos]) {
                        continue;
                    }
                    tries = 10;
                    break;
                }
                if (tries == 10) {
                    final char curChar2 = chars[i];
                    final char replaceChar = chars[newPos];
                    chars[i] = replaceChar;
                    chars[newPos] = curChar2;
                    reversedFlag[newPos] = (reversedFlag[i] = true);
                }
            }
        }
        return new String(chars);
    }
    
    public static String randomizeWord(final String str) {
        return randomizeWord(str, true);
    }
    
    public static String randomize(final String str, final boolean softly) {
        if (str == null) {
            return null;
        }
        if (str.isEmpty()) {
            return str;
        }
        final String[] lines = str.split("\n");
        final StringBuilder lineBuilder = new StringBuilder();
        boolean isFirstLine = true;
        for (final String line : lines) {
            final String[] words = line.split(" ");
            final StringBuilder wordBuilder = new StringBuilder();
            boolean isFirstWord = true;
            for (final String word : words) {
                if (isFirstWord) {
                    isFirstWord = false;
                }
                else {
                    wordBuilder.append(' ');
                }
                wordBuilder.append(randomizeWord(word));
            }
            if (isFirstLine) {
                isFirstLine = false;
            }
            else {
                lineBuilder.append('\n');
            }
            lineBuilder.append((CharSequence)wordBuilder);
        }
        return lineBuilder.toString();
    }
    
    public static String randomize(final String str) {
        return randomize(str, true);
    }
    
    public static boolean isHTML(final char[] s) {
        if (s != null && s.length >= 6 && s[0] == '<' && s[5] == '>') {
            final String tag = new String(s, 1, 4);
            return tag.equalsIgnoreCase("html");
        }
        return false;
    }
    
    public static String wrap(final char[] s, final int maxChars, final boolean rudeBreaking, boolean detectHTML) {
        if (s == null) {
            throw new NullPointerException("sequence");
        }
        if (maxChars < 1) {
            throw new IllegalArgumentException("maxChars < 1");
        }
        detectHTML = (detectHTML && isHTML(s));
        final String lineBreak = detectHTML ? "<br />" : "\n";
        final StringBuilder builder = new StringBuilder();
        final int len = s.length;
        int remaining = maxChars;
        boolean tagDetecting = false;
        boolean ignoreCurrent = false;
        for (int x = 0; x < len; ++x) {
            final char current = s[x];
            if (current == '<' && detectHTML) {
                tagDetecting = true;
                ignoreCurrent = true;
            }
            else if (tagDetecting) {
                if (current == '>') {
                    tagDetecting = false;
                }
                ignoreCurrent = true;
            }
            if (ignoreCurrent) {
                ignoreCurrent = false;
                builder.append(current);
            }
            else {
                --remaining;
                if (s[x] == '\n' || (remaining < 1 && current == ' ')) {
                    remaining = maxChars;
                    builder.append(lineBreak);
                }
                else {
                    if (lookForward(s, x, lineBreak)) {
                        remaining = maxChars;
                    }
                    builder.append(current);
                    if (remaining <= 0) {
                        if (rudeBreaking) {
                            remaining = maxChars;
                            builder.append(lineBreak);
                        }
                    }
                }
            }
        }
        return builder.toString();
    }
    
    private static boolean lookForward(final char[] c, final int caret, final CharSequence search) {
        if (c == null) {
            throw new NullPointerException("char array");
        }
        if (caret < 0) {
            throw new IllegalArgumentException("caret < 0");
        }
        if (caret >= c.length) {
            return false;
        }
        final int length = search.length();
        final int available = c.length - caret;
        if (length < available) {
            return false;
        }
        for (int i = 0; i < length; ++i) {
            if (c[caret + i] != search.charAt(i)) {
                return false;
            }
        }
        return true;
    }
    
    public static String wrap(final String s, final int maxChars, final boolean rudeBreaking, final boolean detectHTML) {
        return wrap(s.toCharArray(), maxChars, rudeBreaking, detectHTML);
    }
    
    public static String wrap(final char[] s, final int maxChars, final boolean rudeBreaking) {
        return wrap(s, maxChars, rudeBreaking, true);
    }
    
    public static String wrap(final char[] s, final int maxChars) {
        return wrap(s, maxChars, false);
    }
    
    public static String wrap(final String s, final int maxChars) {
        return wrap(s.toCharArray(), maxChars);
    }
    
    public static String cut(final String string, final int max) {
        if (string == null) {
            return null;
        }
        final int len = string.length();
        if (len <= max) {
            return string;
        }
        final String[] words = string.split(" ");
        String ret = "";
        int remaining = max + 1;
        int x = 0;
        while (x < words.length) {
            final String curword = words[x];
            final int curlen = curword.length();
            if (curlen < remaining) {
                ret = ret + " " + curword;
                remaining -= curlen + 1;
                ++x;
            }
            else {
                if (x == 0) {
                    ret = ret + " " + curword.substring(0, remaining - 1);
                    break;
                }
                break;
            }
        }
        if (ret.length() == 0) {
            return "";
        }
        return ret.substring(1) + "...";
    }
    
    public static String requireNotBlank(final String s, final String name) {
        if (s == null) {
            throw new NullPointerException(name);
        }
        if (StringUtils.isBlank((CharSequence)s)) {
            throw new IllegalArgumentException(name);
        }
        return s;
    }
    
    public static String getLink(final String link) {
        return "<a style='color:#585858;' href='link'>link</a>".replaceAll("link", link);
    }
    
    public static String convertListToString(final String c, final List<String> l) {
        final StringBuilder b = new StringBuilder();
        for (final String string : l) {
            b.append(string).append(c);
        }
        return b.toString();
    }
    
    public enum EscapeGroup
    {
        DOUBLE_QUOTE(new char[] { '\"' }), 
        COMMAND(EscapeGroup.DOUBLE_QUOTE, new char[] { '\'', ' ' }), 
        REGEXP(EscapeGroup.COMMAND, new char[] { '/', '\\', '?', '*', '+', '[', ']', ':', '{', '}', '(', ')' });
        
        private final char[] chars;
        
        private EscapeGroup(final char[] symbols) {
            this.chars = symbols;
        }
        
        private EscapeGroup(final EscapeGroup extend, final char[] symbols) {
            final int len = extend.chars.length + symbols.length;
            this.chars = new char[len];
            int x;
            for (x = 0; x < extend.chars.length; ++x) {
                this.chars[x] = extend.chars[x];
            }
            System.arraycopy(symbols, 0, this.chars, x, symbols.length);
        }
        
        public char[] getChars() {
            return this.chars;
        }
    }
}
