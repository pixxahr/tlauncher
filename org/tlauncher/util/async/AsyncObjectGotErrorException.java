// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.async;

public class AsyncObjectGotErrorException extends AsyncObjectException
{
    private static final long serialVersionUID = -1016561584766422788L;
    private final AsyncObject<?> object;
    
    public AsyncObjectGotErrorException(final AsyncObject<?> object, final Throwable error) {
        super(error);
        this.object = object;
    }
    
    public AsyncObject<?> getObject() {
        return this.object;
    }
}
