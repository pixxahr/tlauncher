// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.async;

import org.tlauncher.util.U;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class ExtendedThread extends Thread
{
    private static AtomicInteger threadNum;
    private final ExtendedThreadCaller caller;
    private final Object monitor;
    private volatile String blockReason;
    
    public ExtendedThread(final String name) {
        super(name + "#" + ExtendedThread.threadNum.incrementAndGet());
        this.monitor = new Object();
        this.caller = new ExtendedThreadCaller();
    }
    
    public ExtendedThread() {
        this("ExtendedThread");
    }
    
    public ExtendedThreadCaller getCaller() {
        return this.caller;
    }
    
    public void startAndWait() {
        super.start();
        while (!this.isThreadLocked()) {
            U.sleepFor(100L);
        }
    }
    
    @Override
    public abstract void run();
    
    protected void lockThread(final String reason) {
        if (reason == null) {
            throw new NullPointerException();
        }
        this.checkCurrent();
        this.blockReason = reason;
        synchronized (this.monitor) {
            while (this.blockReason != null) {
                try {
                    this.monitor.wait();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void unlockThread(final String reason) {
        if (reason == null) {
            throw new NullPointerException();
        }
        if (!reason.equals(this.blockReason)) {
            throw new IllegalStateException("Unlocking denied! Locked with: " + this.blockReason + ", tried to unlock with: " + reason);
        }
        this.blockReason = null;
        synchronized (this.monitor) {
            this.monitor.notifyAll();
        }
    }
    
    public void tryUnlock(final String reason) {
        if (reason == null) {
            throw new NullPointerException();
        }
        if (reason.equals(this.blockReason)) {
            this.unlockThread(reason);
        }
    }
    
    public boolean isThreadLocked() {
        return this.blockReason != null;
    }
    
    public boolean isCurrent() {
        return Thread.currentThread().equals(this);
    }
    
    protected void checkCurrent() {
        if (!this.isCurrent()) {
            throw new IllegalStateException("Illegal thread!");
        }
    }
    
    protected void threadLog(final Object... o) {
        U.log("[" + this.getName() + "]", o);
    }
    
    static {
        ExtendedThread.threadNum = new AtomicInteger();
    }
    
    public class ExtendedThreadCaller extends RuntimeException
    {
        private ExtendedThreadCaller() {
        }
    }
}
