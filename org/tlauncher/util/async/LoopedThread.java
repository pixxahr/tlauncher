// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.async;

public abstract class LoopedThread extends ExtendedThread
{
    protected static final String LOOPED_BLOCK = "iteration";
    
    public LoopedThread(final String name) {
        super(name);
    }
    
    public LoopedThread() {
        this("LoopedThread");
    }
    
    @Override
    protected final void lockThread(final String reason) {
        if (reason == null) {
            throw new NullPointerException();
        }
        if (!reason.equals("iteration")) {
            throw new IllegalArgumentException("Illegal block reason. Expected: iteration, got: " + reason);
        }
        super.lockThread(reason);
    }
    
    public final boolean isIterating() {
        return !this.isThreadLocked();
    }
    
    public final void iterate() {
        if (!this.isIterating()) {
            this.unlockThread("iteration");
        }
    }
    
    @Override
    public final void run() {
        while (true) {
            this.lockThread("iteration");
            this.iterateOnce();
        }
    }
    
    protected abstract void iterateOnce();
}
