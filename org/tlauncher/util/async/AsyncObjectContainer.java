// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.async;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

public class AsyncObjectContainer<T>
{
    private final List<AsyncObject<T>> objects;
    private final Map<AsyncObject<T>, T> values;
    private boolean executionLock;
    
    public AsyncObjectContainer() {
        this.objects = new ArrayList<AsyncObject<T>>();
        this.values = new LinkedHashMap<AsyncObject<T>, T>();
    }
    
    public AsyncObjectContainer(final AsyncObject<T>[] asyncObjects) {
        this();
        for (final AsyncObject<T> object : asyncObjects) {
            this.add(object);
        }
    }
    
    public Map<AsyncObject<T>, T> execute() {
        this.executionLock = true;
        this.values.clear();
        synchronized (this.objects) {
            int i = 0;
            final int size = this.objects.size();
            for (final AsyncObject<T> object : this.objects) {
                object.start();
            }
            while (i < size) {
                for (final AsyncObject<T> object : this.objects) {
                    try {
                        if (this.values.containsKey(object)) {
                            continue;
                        }
                        this.values.put(object, object.getValue());
                        ++i;
                    }
                    catch (AsyncObjectNotReadyException ex) {}
                    catch (AsyncObjectGotErrorException ignored0) {
                        this.values.put(object, null);
                        ++i;
                    }
                }
            }
        }
        this.executionLock = false;
        return this.values;
    }
    
    public void add(final AsyncObject<T> object) {
        if (object == null) {
            throw new NullPointerException();
        }
        synchronized (this.objects) {
            if (this.executionLock) {
                throw new AsyncContainerLockedException();
            }
            this.objects.add(object);
        }
    }
    
    public void remove(final AsyncObject<T> object) {
        if (object == null) {
            throw new NullPointerException();
        }
        synchronized (this.objects) {
            if (this.executionLock) {
                throw new AsyncContainerLockedException();
            }
            this.objects.remove(object);
        }
    }
    
    public void removeAll() {
        synchronized (this.objects) {
            if (this.executionLock) {
                throw new AsyncContainerLockedException();
            }
            this.objects.clear();
        }
    }
}
