// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.async;

public class RunnableThread extends ExtendedThread
{
    private final Runnable r;
    
    public RunnableThread(final Runnable r) {
        if (r == null) {
            throw new NullPointerException();
        }
        this.r = r;
    }
    
    @Override
    public void run() {
        this.r.run();
    }
}
