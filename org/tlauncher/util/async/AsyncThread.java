// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.async;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ExecutorService;

public class AsyncThread
{
    private static ExecutorService service;
    
    public static void execute(final Runnable r) {
        AsyncThread.service.execute(r);
    }
    
    public static ExecutorService getService() {
        return AsyncThread.service;
    }
    
    static {
        AsyncThread.service = Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(final Runnable r) {
                return new RunnableThread(r);
            }
        });
    }
}
