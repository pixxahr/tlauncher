// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.async;

public abstract class AsyncObject<E> extends ExtendedThread
{
    private boolean gotValue;
    private E value;
    private AsyncObjectGotErrorException error;
    
    protected AsyncObject() {
        super("AsyncObject");
    }
    
    @Override
    public void run() {
        try {
            this.value = this.execute();
        }
        catch (Throwable e) {
            this.error = new AsyncObjectGotErrorException(this, e);
            return;
        }
        this.gotValue = true;
    }
    
    public E getValue() throws AsyncObjectNotReadyException, AsyncObjectGotErrorException {
        if (this.error != null) {
            throw this.error;
        }
        if (!this.gotValue) {
            throw new AsyncObjectNotReadyException();
        }
        return this.value;
    }
    
    public AsyncObjectGotErrorException getError() {
        return this.error;
    }
    
    protected abstract E execute() throws AsyncObjectGotErrorException;
}
