// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.async;

class AsyncObjectException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    AsyncObjectException() {
    }
    
    AsyncObjectException(final Throwable cause) {
        super(cause);
    }
}
