// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.pastebin;

public interface PasteListener
{
    void pasteUploading(final Paste p0);
    
    void pasteDone(final Paste p0);
}
