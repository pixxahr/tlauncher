// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.pastebin;

import java.net.URL;

public abstract class PasteResult
{
    private final Paste paste;
    
    PasteResult(final Paste paste) {
        this.paste = paste;
    }
    
    public final Paste getPaste() {
        return this.paste;
    }
    
    public static class PasteUploaded extends PasteResult
    {
        private final URL url;
        
        PasteUploaded(final Paste paste, final URL url) {
            super(paste);
            this.url = url;
        }
        
        public final URL getURL() {
            return this.url;
        }
        
        @Override
        public String toString() {
            return "PasteUploaded{url='" + this.url + "'}";
        }
    }
    
    public static class PasteFailed extends PasteResult
    {
        private final Throwable error;
        
        PasteFailed(final Paste paste, final Throwable error) {
            super(paste);
            this.error = error;
        }
        
        public final Throwable getError() {
            return this.error;
        }
        
        @Override
        public String toString() {
            return "PasteFailed{error='" + this.error + "'}";
        }
    }
}
