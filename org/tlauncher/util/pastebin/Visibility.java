// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.pastebin;

public enum Visibility
{
    PUBLIC(0), 
    NOT_LISTED(1);
    
    private final int value;
    
    private Visibility(final int value) {
        this.value = value;
    }
    
    public int getValue() {
        return this.value;
    }
}
