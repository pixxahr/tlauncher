// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.pastebin;

import java.io.IOException;
import java.util.Map;
import net.minecraft.launcher.Http;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;
import java.util.Iterator;
import java.util.ArrayList;
import java.net.URL;

public class Paste
{
    private static final String DEV_KEY = "394691d8a27e3852a24969dbeba85e53";
    private static final URL POST_URL;
    private String title;
    private String content;
    private String format;
    private ExpireDate expires;
    private Visibility visibility;
    private final ArrayList<PasteListener> listeners;
    private PasteResult result;
    
    public Paste() {
        this.expires = ExpireDate.ONE_WEEK;
        this.visibility = Visibility.NOT_LISTED;
        this.listeners = new ArrayList<PasteListener>();
    }
    
    public final String getTitle() {
        return this.title;
    }
    
    public final void setTitle(final String title) {
        this.title = title;
    }
    
    public final String getContent() {
        return this.content;
    }
    
    public final void setContent(final String content) {
        this.content = content;
    }
    
    public final String getFormat() {
        return this.format;
    }
    
    public final void setFormat(final String format) {
        this.format = format;
    }
    
    public final ExpireDate getExpireDate() {
        return this.expires;
    }
    
    public final void setExpireDate(final ExpireDate date) {
        this.expires = date;
    }
    
    public final Visibility getVisibility() {
        return this.visibility;
    }
    
    public final void setVisibility(final Visibility vis) {
        this.visibility = vis;
    }
    
    public void addListener(final PasteListener listener) {
        this.listeners.add(listener);
    }
    
    public void removeListener(final PasteListener listener) {
        this.listeners.remove(listener);
    }
    
    public PasteResult paste() {
        for (final PasteListener listener : this.listeners) {
            listener.pasteUploading(this);
        }
        try {
            this.result = this.doPaste();
        }
        catch (Throwable t) {
            this.result = new PasteResult.PasteFailed(this, t);
        }
        for (final PasteListener listener : this.listeners) {
            listener.pasteDone(this);
        }
        return this.result;
    }
    
    private PasteResult.PasteUploaded doPaste() throws IOException {
        if (StringUtils.isEmpty((CharSequence)this.getContent())) {
            throw new IllegalArgumentException("content is empty");
        }
        if (this.getVisibility() == null) {
            throw new NullPointerException("visibility");
        }
        if (this.getExpireDate() == null) {
            throw new NullPointerException("expire date");
        }
        final HashMap<String, Object> query = new HashMap<String, Object>();
        query.put("api_dev_key", "394691d8a27e3852a24969dbeba85e53");
        query.put("api_option", "paste");
        query.put("api_paste_name", this.getTitle());
        query.put("api_paste_code", this.getContent());
        query.put("api_paste_private", this.getVisibility().getValue());
        query.put("api_paste_expire_date", this.getExpireDate().getValue());
        final String answer = Http.performPost(Paste.POST_URL, query);
        if (answer.startsWith("http")) {
            return new PasteResult.PasteUploaded(this, new URL(answer));
        }
        throw new IOException("illegal answer: \"" + answer + '\"');
    }
    
    static {
        POST_URL = Http.constantURL("http://pastebin.com/api/api_post.php");
    }
}
