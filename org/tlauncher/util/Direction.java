// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

public enum Direction
{
    TOP_LEFT, 
    TOP, 
    TOP_RIGHT, 
    CENTER_LEFT, 
    CENTER, 
    CENTER_RIGHT, 
    BOTTOM_LEFT, 
    BOTTOM, 
    BOTTOM_RIGHT;
    
    private final String lower;
    
    private Direction() {
        this.lower = this.name().toLowerCase();
    }
    
    @Override
    public String toString() {
        return this.lower;
    }
}
