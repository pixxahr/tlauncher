// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.stream;

public class StringStream extends SafeOutputStream
{
    protected final StringBuilder buffer;
    protected int caret;
    
    public StringStream() {
        this.buffer = new StringBuilder();
    }
    
    @Override
    public void write(final int b) {
        this.write((char)b);
    }
    
    protected synchronized void write(final char c) {
        this.buffer.append(c);
        ++this.caret;
    }
    
    public void write(final char[] c) {
        if (c == null) {
            throw new NullPointerException();
        }
        if (c.length == 0) {
            return;
        }
        for (int i = 0; i < c.length; ++i) {
            this.write(c[i]);
        }
    }
    
    @Override
    public synchronized void flush() {
        this.caret = 0;
        this.buffer.setLength(0);
    }
}
