// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.stream;

public class LinkedStringStream extends BufferedStringStream
{
    private Logger logger;
    
    public LinkedStringStream() {
    }
    
    LinkedStringStream(final Logger logger) {
        this.logger = logger;
    }
    
    public Logger getLogger() {
        return this.logger;
    }
    
    public void setLogger(final Logger logger) {
        this.logger = logger;
    }
    
    @Override
    public synchronized void flush() {
        if (this.logger != null) {
            final char[] chars = new char[this.caret];
            this.buffer.getChars(0, this.caret, chars, 0);
            this.logger.rawlog(chars);
        }
        super.flush();
    }
}
