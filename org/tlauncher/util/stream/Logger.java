// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.stream;

public interface Logger
{
    void log(final String p0);
    
    void log(final Object... p0);
    
    void rawlog(final String p0);
    
    void rawlog(final char[] p0);
}
