// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.stream;

public class BufferedStringStream extends StringStream
{
    public void write(final char b) {
        super.write(b);
        if (b == '\n') {
            this.flush();
        }
    }
}
