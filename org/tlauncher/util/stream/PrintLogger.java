// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.stream;

import java.io.IOException;
import java.util.Locale;
import org.tlauncher.util.U;
import java.io.OutputStream;
import java.util.Formatter;
import java.io.PrintStream;

public class PrintLogger extends PrintStream implements Logger
{
    private LinkedStringStream stream;
    private Logger mirror;
    private Formatter formatter;
    
    private PrintLogger(final LinkedStringStream stream, final Logger mirror) {
        super(stream);
        this.stream = stream;
        this.mirror = mirror;
    }
    
    public PrintLogger(final LinkedStringStream stream) {
        this(stream, null);
    }
    
    public void setMirror(final Logger logger) {
        if (this == logger) {
            throw new IllegalArgumentException();
        }
        this.mirror = logger;
    }
    
    @Override
    public void log(final Object... o) {
        this.log(U.toLog(o));
    }
    
    @Override
    public void log(final String s) {
        this.println(s);
        if (this.mirror != null) {
            this.mirror.log(s);
        }
    }
    
    @Override
    public void rawlog(final String s) {
        if (this.mirror != null) {
            this.mirror.rawlog(s);
        }
    }
    
    @Override
    public void rawlog(final char[] c) {
        if (this.mirror != null) {
            this.mirror.rawlog(c);
        }
    }
    
    public LinkedStringStream getStream() {
        return this.stream;
    }
    
    @Override
    public synchronized void write(final int b) {
        this.stream.write(b);
    }
    
    @Override
    public synchronized void write(final byte[] buf, final int off, final int len) {
        this.stream.write(buf, off, len);
    }
    
    private synchronized void write(String s) {
        if (s == null) {
            s = "null";
        }
        this.stream.write(s.toCharArray());
    }
    
    @Override
    public synchronized void print(final char c) {
        this.stream.write(c);
    }
    
    @Override
    public synchronized void print(final char[] s) {
        this.stream.write(s);
    }
    
    @Override
    public synchronized void print(final boolean b) {
        this.write(b ? "true" : "false");
    }
    
    @Override
    public synchronized void print(final int i) {
        this.write(String.valueOf(i));
    }
    
    @Override
    public synchronized void print(final long l) {
        this.write(String.valueOf(l));
    }
    
    @Override
    public synchronized void print(final float f) {
        this.write(String.valueOf(f));
    }
    
    @Override
    public synchronized void print(final double d) {
        this.write(String.valueOf(d));
    }
    
    @Override
    public synchronized void print(String s) {
        if (s == null) {
            s = "null";
        }
        this.write(s);
    }
    
    @Override
    public synchronized void print(final Object obj) {
        this.write(String.valueOf(obj));
    }
    
    private void newLine() {
        this.write(10);
    }
    
    @Override
    public synchronized void println() {
        this.newLine();
    }
    
    @Override
    public synchronized void println(final boolean x) {
        this.print(x);
        this.newLine();
    }
    
    @Override
    public synchronized void println(final char x) {
        this.print(x);
        this.newLine();
    }
    
    @Override
    public synchronized void println(final int x) {
        this.print(x);
        this.newLine();
    }
    
    @Override
    public synchronized void println(final long x) {
        this.print(x);
        this.newLine();
    }
    
    @Override
    public synchronized void println(final float x) {
        this.print(x);
        this.newLine();
    }
    
    @Override
    public synchronized void println(final double x) {
        this.print(x);
        this.newLine();
    }
    
    @Override
    public synchronized void println(final char[] x) {
        this.print(x);
        this.newLine();
    }
    
    @Override
    public synchronized void println(final String x) {
        this.print(x);
        this.newLine();
    }
    
    @Override
    public synchronized void println(final Object x) {
        this.print(String.valueOf(x));
        this.newLine();
    }
    
    @Override
    public synchronized PrintStream printf(final String format, final Object... args) {
        return this.format(format, args);
    }
    
    @Override
    public synchronized PrintStream printf(final Locale l, final String format, final Object... args) {
        return this.format(l, format, args);
    }
    
    @Override
    public synchronized PrintStream format(final String format, final Object... args) {
        if (this.formatter == null || this.formatter.locale() != Locale.getDefault()) {
            this.formatter = new Formatter((Appendable)this);
        }
        this.formatter.format(Locale.getDefault(), format, args);
        return this;
    }
    
    @Override
    public synchronized PrintStream format(final Locale l, final String format, final Object... args) {
        if (this.formatter == null || this.formatter.locale() != l) {
            this.formatter = new Formatter(this, l);
        }
        this.formatter.format(l, format, args);
        return this;
    }
    
    @Override
    public synchronized PrintStream append(final CharSequence csq) {
        if (csq == null) {
            this.print("null");
        }
        else {
            this.print(csq.toString());
        }
        return this;
    }
    
    @Override
    public synchronized PrintStream append(final CharSequence csq, final int start, final int end) {
        final CharSequence cs = (csq == null) ? "null" : csq;
        this.write(cs.subSequence(start, end).toString());
        return this;
    }
    
    @Override
    public synchronized PrintStream append(final char c) {
        this.print(c);
        return this;
    }
}
