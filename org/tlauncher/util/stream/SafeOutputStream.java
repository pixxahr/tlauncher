// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.stream;

import java.io.IOException;
import java.io.OutputStream;

public abstract class SafeOutputStream extends OutputStream
{
    @Override
    public void write(final byte[] b) {
        try {
            super.write(b);
        }
        catch (IOException ex) {}
    }
    
    @Override
    public void write(final byte[] b, final int off, final int len) {
        try {
            super.write(b, off, len);
        }
        catch (IOException ex) {}
    }
    
    @Override
    public void flush() {
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public abstract void write(final int p0);
}
