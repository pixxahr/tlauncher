// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.stream;

import java.io.IOException;
import java.io.OutputStream;

public class MirroredLinkedStringStream extends LinkedStringStream
{
    private OutputStream mirror;
    
    protected MirroredLinkedStringStream() {
    }
    
    protected OutputStream getMirror() {
        return this.mirror;
    }
    
    public void setMirror(final OutputStream stream) {
        this.mirror = stream;
    }
    
    @Override
    public void write(final char b) {
        super.write(b);
        if (this.mirror != null) {
            try {
                this.mirror.write(b);
            }
            catch (IOException e) {
                throw new RuntimeException("Cannot log into the mirror!", e);
            }
        }
    }
    
    @Override
    public void flush() {
        super.flush();
        if (this.mirror != null) {
            try {
                this.mirror.flush();
            }
            catch (IOException e) {
                throw new RuntimeException("Cannot flush the mirror!", e);
            }
        }
    }
}
