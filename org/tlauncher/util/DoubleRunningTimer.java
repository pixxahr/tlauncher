// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.TimerTask;
import java.util.Timer;

public class DoubleRunningTimer extends Timer
{
    public void startProtection() {
        this.schedule(new DoubleRunningTask(), 0L, 3000L);
    }
    
    public void clearTimeLabel() {
        U.log("[Double running]", "clear time label");
        this.cancel();
        try {
            FileUtil.deleteFile(MinecraftUtil.getTLauncherFile("doubleRunningProtection.txt"));
        }
        catch (Exception e) {
            U.log("can't delete file", e);
        }
    }
    
    private class DoubleRunningTask extends TimerTask
    {
        @Override
        public void run() {
            try {
                final File file = MinecraftUtil.getTLauncherFile("doubleRunningProtection.txt");
                FileUtil.writeFile(file, "" + new Date().getTime());
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
