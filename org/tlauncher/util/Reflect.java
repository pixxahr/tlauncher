// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import org.tlauncher.exceptions.ParseException;
import java.lang.reflect.Field;

public class Reflect
{
    public static Field getField0(final Class<?> clazz, final String name) throws NoSuchFieldException, SecurityException {
        if (clazz == null) {
            throw new NullPointerException("class is null");
        }
        if (name == null || name.isEmpty()) {
            throw new NullPointerException("name is null or empty");
        }
        return clazz.getField(name);
    }
    
    public static Field getField(final Class<?> clazz, final String name) {
        try {
            return getField0(clazz, name);
        }
        catch (Exception e) {
            U.log("Error getting field", name, "from", clazz, e);
            return null;
        }
    }
    
    public static <T> T getValue0(final Field field, final Class<T> classOfT, final Object parent) throws IllegalArgumentException, IllegalAccessException {
        if (field == null) {
            throw new NullPointerException("field is null");
        }
        if (classOfT == null) {
            throw new NullPointerException("classOfT is null");
        }
        if (parent == null) {
            throw new NullPointerException("parent is NULL");
        }
        final Class<?> fieldClass = field.getType();
        if (fieldClass == null) {
            throw new NullPointerException("field has no shell");
        }
        if (!fieldClass.equals(classOfT) && !fieldClass.isAssignableFrom(classOfT)) {
            throw new IllegalArgumentException("field is not assignable from return type class");
        }
        return (T)field.get(parent);
    }
    
    public static <T> T getValue(final Field field, final Class<T> classOfT, final Object parent) {
        try {
            return (T)getValue0(field, (Class<Object>)classOfT, parent);
        }
        catch (Exception e) {
            U.log("Cannot get value of", field, "from", classOfT, parent, e);
            return null;
        }
    }
    
    public static <T> T cast(final Object o, final Class<T> classOfT) {
        if (classOfT == null) {
            throw new NullPointerException();
        }
        return classOfT.isInstance(o) ? classOfT.cast(o) : null;
    }
    
    public static <T extends Enum<T>> T parseEnum0(final Class<T> enumClass, final String string) throws ParseException {
        if (enumClass == null) {
            throw new NullPointerException("class is null");
        }
        if (string == null) {
            throw new NullPointerException("string is null");
        }
        final T[] array;
        final T[] constants = array = enumClass.getEnumConstants();
        for (final T constant : array) {
            if (string.equalsIgnoreCase(constant.toString())) {
                return constant;
            }
        }
        throw new ParseException("Cannot parse value:\"" + string + "\"; enum: " + enumClass.getSimpleName());
    }
    
    public static <T extends Enum<T>> T parseEnum(final Class<T> enumClass, final String string) {
        try {
            return (T)parseEnum0((Class<Enum>)enumClass, string);
        }
        catch (Exception e) {
            U.log(e);
            return null;
        }
    }
    
    private Reflect() {
        throw new RuntimeException();
    }
}
