// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.review;

public enum FolderEnum
{
    COMPLAINT {
        @Override
        public String getName() {
            return "Complaint";
        }
    }, 
    THANK {
        @Override
        public String getName() {
            return "Thank";
        }
    }, 
    BUG {
        @Override
        public String getName() {
            return "Bug";
        }
    }, 
    LACK {
        @Override
        public String getName() {
            return "Lack";
        }
    }, 
    WISH {
        @Override
        public String getName() {
            return "Wish";
        }
    };
    
    public abstract String getName();
}
