// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.guice;

import org.tlauncher.tlauncher.minecraft.launcher.assitent.SoundAssist;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public interface SoundAssistFactory
{
    SoundAssist create(final MinecraftLauncher p0);
}
