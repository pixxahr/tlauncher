// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.guice;

import com.google.inject.Inject;
import org.tlauncher.tlauncher.site.play.SitePlay;
import com.google.inject.assistedinject.Assisted;
import org.tlauncher.tlauncher.rmo.TLauncher;

public interface SitePlayFactory
{
    @Inject
    SitePlay create(@Assisted final TLauncher p0);
}
