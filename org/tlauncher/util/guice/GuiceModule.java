// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.guice;

import net.minecraft.launcher.versions.json.PatternTypeAdapter;
import java.util.regex.Pattern;
import org.tlauncher.util.gson.serializer.ResourcePackDTOTypeAdapter;
import org.tlauncher.modpack.domain.client.ResourcePackDTO;
import org.tlauncher.util.gson.serializer.MapDTOTypeAdapter;
import org.tlauncher.modpack.domain.client.MapDTO;
import org.tlauncher.util.gson.serializer.ModDTOTypeAdapter;
import org.tlauncher.modpack.domain.client.ModDTO;
import org.tlauncher.util.gson.serializer.ModpackDTOTypeAdapter;
import org.tlauncher.modpack.domain.client.ModpackDTO;
import net.minecraft.launcher.versions.CompleteVersion;
import java.lang.reflect.Type;
import net.minecraft.launcher.versions.json.DateTypeAdapter;
import java.util.Date;
import com.google.gson.TypeAdapterFactory;
import net.minecraft.launcher.versions.json.LowerCaseEnumTypeAdapterFactory;
import org.tlauncher.util.DoubleRunningTimer;
import com.google.gson.GsonBuilder;
import com.google.inject.Singleton;
import com.google.inject.Provides;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.ui.alert.Alert;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.util.TlauncherUtil;
import org.tlauncher.tlauncher.minecraft.launcher.assitent.LanguageAssistance;
import org.tlauncher.tlauncher.minecraft.launcher.assitent.AdditionalFileAssistanceFactory;
import org.tlauncher.tlauncher.minecraft.launcher.assitent.AdditionalFileAssistance;
import org.tlauncher.tlauncher.minecraft.launcher.assitent.SoundAssist;
import org.tlauncher.tlauncher.site.play.SitePlay;
import org.tlauncher.tlauncher.ui.progress.ProgressFrame;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import org.tlauncher.tlauncher.ui.explorer.FileChooser;
import org.tlauncher.tlauncher.ui.explorer.FileWrapper;
import org.tlauncher.tlauncher.minecraft.launcher.server.InnerMinecraftServersImpl;
import org.tlauncher.tlauncher.minecraft.launcher.server.InnerMinecraftServer;
import java.lang.annotation.Annotation;
import com.google.inject.name.Names;
import com.google.gson.Gson;
import org.tlauncher.tlauncher.ui.explorer.FileExplorer;
import com.google.inject.Injector;
import joptsimple.OptionSet;
import com.google.inject.AbstractModule;

public class GuiceModule extends AbstractModule
{
    private OptionSet set;
    private Injector injector;
    private boolean usedDefaultChooser;
    
    public Injector getInjector() {
        return this.injector;
    }
    
    public void setInjector(final Injector injector) {
        this.injector = injector;
    }
    
    public GuiceModule(final OptionSet set) {
        this.usedDefaultChooser = true;
        this.set = set;
        try {
            new FileExplorer();
        }
        catch (Throwable t) {
            this.usedDefaultChooser = false;
        }
    }
    
    protected void configure() {
        this.bind((Class)Gson.class).annotatedWith((Annotation)Names.named("GsonCompleteVersion")).toInstance((Object)this.createGsonCompleteVersion());
        this.bind((Class)Gson.class).annotatedWith((Annotation)Names.named("GsonAdditionalFile")).toInstance((Object)this.createAddittionalFileGson());
        this.bind((Class)InnerMinecraftServer.class).to((Class)InnerMinecraftServersImpl.class);
        Class<? extends FileChooser> c = FileExplorer.class;
        if (!this.usedDefaultChooser) {
            c = FileWrapper.class;
        }
        this.bind((Class)FileChooser.class).to((Class)c);
        this.install(new FactoryModuleBuilder().implement((Class)MinecraftLauncher.class, (Class)MinecraftLauncher.class).build((Class)MinecraftLauncherFactory.class));
        this.install(new FactoryModuleBuilder().implement((Class)TLauncher.class, (Class)TLauncher.class).build((Class)TlauncherFactory.class));
        this.install(new FactoryModuleBuilder().implement((Class)ProgressFrame.class, (Class)ProgressFrame.class).build((Class)CustomBarFactory.class));
        this.install(new FactoryModuleBuilder().implement((Class)SitePlay.class, (Class)SitePlay.class).build((Class)SitePlayFactory.class));
        this.install(new FactoryModuleBuilder().implement((Class)SoundAssist.class, (Class)SoundAssist.class).build((Class)SoundAssistFactory.class));
        this.install(new FactoryModuleBuilder().implement((Class)AdditionalFileAssistance.class, (Class)AdditionalFileAssistance.class).build((Class)AdditionalFileAssistanceFactory.class));
        this.install(new FactoryModuleBuilder().implement((Class)LanguageAssistance.class, (Class)LanguageAssistance.class).build((Class)LanguageAssistFactory.class));
    }
    
    @Provides
    @Singleton
    public TLauncher getTlauncher() {
        try {
            return ((TlauncherFactory)TLauncher.getInjector().getInstance((Class)TlauncherFactory.class)).create(this.set);
        }
        catch (Throwable e) {
            if (TlauncherUtil.getStringError(e).contains("Problem reading font data.")) {
                final String text = Localizable.get("alert.error.font.problem").replaceAll("pageLang", TlauncherUtil.getPageLanguage());
                Alert.showErrorHtml(text, 400);
                TLauncher.kill();
            }
            else {
                U.log("can't create TLauncher instance", e);
            }
            throw new NullPointerException("can't create TLauncher instance");
        }
    }
    
    @Provides
    @Singleton
    public Gson getGson() {
        final GsonBuilder builder = new GsonBuilder().setPrettyPrinting();
        return builder.create();
    }
    
    @Provides
    @Singleton
    public DoubleRunningTimer getDoubleRunningTimer() {
        return new DoubleRunningTimer();
    }
    
    private Gson createGsonCompleteVersion() {
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        builder.registerTypeAdapter(CompleteVersion.class, new CompleteVersion.CompleteVersionSerializer());
        builder.registerTypeAdapter(ModpackDTO.class, new ModpackDTOTypeAdapter());
        builder.registerTypeAdapter(ModDTO.class, new ModDTOTypeAdapter());
        builder.registerTypeAdapter(MapDTO.class, new MapDTOTypeAdapter());
        builder.registerTypeAdapter(ResourcePackDTO.class, new ResourcePackDTOTypeAdapter());
        builder.enableComplexMapKeySerialization();
        builder.setPrettyPrinting();
        return builder.create();
    }
    
    private Gson createAddittionalFileGson() {
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Pattern.class, new PatternTypeAdapter());
        builder.setPrettyPrinting();
        return builder.create();
    }
}
