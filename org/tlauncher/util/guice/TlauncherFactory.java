// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.guice;

import org.tlauncher.tlauncher.rmo.TLauncher;
import joptsimple.OptionSet;

public interface TlauncherFactory
{
    TLauncher create(final OptionSet p0);
}
