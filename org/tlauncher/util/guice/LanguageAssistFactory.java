// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.guice;

import org.tlauncher.tlauncher.minecraft.launcher.assitent.LanguageAssistance;
import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;

public interface LanguageAssistFactory
{
    LanguageAssistance create(final MinecraftLauncher p0);
}
