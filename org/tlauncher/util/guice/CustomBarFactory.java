// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.guice;

import org.tlauncher.tlauncher.ui.progress.ProgressFrame;
import com.google.inject.assistedinject.Assisted;

public interface CustomBarFactory
{
    ProgressFrame create(@Assisted("info") final String p0, @Assisted("bottomIcon") final String p1, @Assisted("tlauncher") final String p2);
}
