// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.guice;

import org.tlauncher.tlauncher.minecraft.launcher.MinecraftLauncher;
import org.tlauncher.tlauncher.rmo.TLauncher;

public interface MinecraftLauncherFactory
{
    MinecraftLauncher create(final TLauncher p0, final boolean p1);
}
