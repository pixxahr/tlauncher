// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import javax.net.ssl.SSLSession;
import java.nio.file.Path;
import org.apache.commons.lang3.StringUtils;
import java.nio.charset.Charset;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import java.nio.file.Paths;
import org.tlauncher.tlauncher.ui.alert.Alert;
import java.util.Map;
import org.tlauncher.tlauncher.updater.bootstrapper.model.JavaDownloadedElement;
import org.tlauncher.tlauncher.updater.bootstrapper.model.JavaConfig;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Objects;
import java.net.UnknownHostException;
import java.net.MalformedURLException;
import java.net.InetAddress;
import java.io.File;
import java.util.Date;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.concurrent.Executor;
import org.tlauncher.util.async.AsyncThread;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import java.security.SecureRandom;
import javax.net.ssl.TrustManager;
import javax.net.ssl.KeyManager;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.SSLContext;
import java.time.chrono.ChronoLocalDate;
import java.time.LocalDate;
import java.net.HttpURLConnection;
import org.tlauncher.tlauncher.downloader.Downloadable;
import java.net.URL;
import java.util.Locale;
import javax.swing.JFrame;
import org.tlauncher.tlauncher.ui.log.LogFrame;
import java.io.IOException;
import org.tlauncher.tlauncher.configuration.LangConfiguration;
import org.tlauncher.tlauncher.configuration.InnerConfiguration;
import org.tlauncher.tlauncher.configuration.Configuration;
import org.tlauncher.tlauncher.ui.loc.Localizable;
import org.tlauncher.tlauncher.rmo.TLauncher;

public class TlauncherUtil
{
    public static final String PROTECTED_DOUBLE_RUNNING_FILE = "doubleRunningProtection.txt";
    private static final int CAN_RUNNING_AFTER = 4;
    
    public static void sendLog(Throwable e) {
        if (TLauncher.DEBUG) {
            return;
        }
        if (Localizable.get() == null) {
            try {
                final Configuration settings = Configuration.createConfiguration();
                final Locale locale = settings.getLocale();
                final InnerConfiguration innerConfig = new InnerConfiguration(FileUtil.getResourceAppStream("/inner.tlauncher.properties"));
                Localizable.setLang(new LangConfiguration(settings.getLocales(), locale, innerConfig.get("tlauncher.language.folder")));
            }
            catch (IOException e2) {
                e2.addSuppressed(e);
                e = e2;
                e2.printStackTrace();
            }
        }
        new LogFrame(TLauncher.getInstance().getFrame(), e).setVisible(true);
    }
    
    public static void checkRedirect() {
    }
    
    public static int hostAvailabilityCheck(String host) {
        if (host.endsWith("/")) {
            host = host.substring(0, host.length() - 1);
        }
        try {
            final URL url = new URL(host);
            final HttpURLConnection httpConn = Downloadable.setUp(url.openConnection(), true);
            httpConn.setRequestMethod("HEAD");
            httpConn.setInstanceFollowRedirects(true);
            httpConn.connect();
            U.debug(host + " : " + httpConn.getResponseCode());
            return httpConn.getResponseCode();
        }
        catch (Throwable e) {
            U.debug(host + " is down ");
            return 500;
        }
    }
    
    public static void deactivateSSL() {
        final Configuration c = TLauncher.getInstance().getConfiguration();
        if (c.get("ssl.deactivate.date") == null || LocalDate.parse(c.get("ssl.deactivate.date")).isBefore(LocalDate.now())) {
            TLauncher.getInstance().getConfiguration().set("ssl.deactivate.date", LocalDate.now().plusMonths(1L));
            try {
                TLauncher.getInstance().getConfiguration().save();
            }
            catch (IOException e1) {
                U.log(e1);
            }
        }
        try {
            final SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, new X509TrustManager[] { new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(final X509Certificate[] chain, final String authType) {
                    }
                    
                    @Override
                    public void checkServerTrusted(final X509Certificate[] chain, final String authType) {
                    }
                    
                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                } }, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        }
        catch (Throwable var1) {
            U.log(var1);
        }
        HttpsURLConnection.setDefaultHostnameVerifier((s, sslSession) -> true);
    }
    
    public static void checkServersAvalability(final String[] array) {
        final List<CompletableFuture<Integer>> servers = Arrays.stream(array).map(link -> CompletableFuture.supplyAsync(() -> {
            if (hostAvailabilityCheck(link) == 503) {
                U.log("     server code 503 -> " + link);
                return Integer.valueOf(503);
            }
            else {
                return Integer.valueOf(200);
            }
        }, AsyncThread.getService())).collect((Collector<? super Object, ?, List<CompletableFuture<Integer>>>)Collectors.toList());
        try {
            CompletableFuture.allOf((CompletableFuture<?>[])servers.toArray(new CompletableFuture[0])).get();
            if (servers.stream().allMatch(e -> {
                try {
                    return e.get().equals(200);
                }
                catch (Exception ex) {
                    return false;
                }
            })) {
                U.log("#####    all servers are available   ######");
            }
        }
        catch (Exception ex2) {}
    }
    
    public static boolean isDoubleRunning() {
        final File f = MinecraftUtil.getTLauncherFile("doubleRunningProtection.txt");
        if (Files.exists(f.toPath(), new LinkOption[0])) {
            try {
                final Date start = new Date(Long.parseLong(FileUtil.readFile(f)));
                final Date end = new Date(start.getTime() + 4000L);
                final Date current = new Date();
                if (current.after(start) && current.before(end)) {
                    return true;
                }
            }
            catch (IOException | NumberFormatException ex2) {
                final Exception ex;
                final Exception e = ex;
                e.printStackTrace();
            }
        }
        return false;
    }
    
    public static String resolveHostName(final String path) throws MalformedURLException, UnknownHostException {
        final URL url = new URL(path);
        return url.getProtocol() + "://" + InetAddress.getByName(url.getHost()).getHostAddress() + ":" + url.getPort() + url.getFile();
    }
    
    public static boolean isAdmin() {
        if (!OS.is(OS.WINDOWS)) {
            return true;
        }
        try {
            final Class cl = Class.forName("com.sun.security.auth.module.NTSystem");
            if (Objects.isNull(cl)) {
                return true;
            }
            final Method method = cl.getMethod("getGroupIDs", (Class[])new Class[0]);
            final String[] array;
            final String[] groups = array = (String[])method.invoke(cl.newInstance(), new Object[0]);
            for (final String group : array) {
                if (group.equals("S-1-5-32-544")) {
                    return true;
                }
            }
            return false;
        }
        catch (Exception e) {
            U.log(e);
            return true;
        }
    }
    
    public static String getPageLanguage() {
        if (Objects.nonNull(TLauncher.getInstance()) && Objects.nonNull(TLauncher.getInstance().getConfiguration()) && TLauncher.getInstance().getConfiguration().isUSSRLocale()) {
            return "ru";
        }
        return "en";
    }
    
    public static boolean contains_OPTIONS_JAVA_XMX() {
        return Objects.nonNull(System.getenv("_java_options")) && System.getenv("_java_options").toLowerCase().contains("xmx");
    }
    
    public static String getStringError(final Throwable e) {
        final StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString().replaceAll(System.lineSeparator(), "<br>");
    }
    
    public static File getJVMFolder(final JavaConfig config, final File tlauncherFolder) {
        final JavaDownloadedElement java = getProperJavaElement(config);
        return new File(new File(tlauncherFolder, "jvms"), java.getJavaFolder());
    }
    
    public static JavaDownloadedElement getProperJavaElement(final JavaConfig config) {
        if (useX64JavaInsteadX32Java()) {
            return config.getConfig().get(OS.CURRENT).get(OS.Arch.x64);
        }
        return config.getConfig().get(OS.CURRENT).get(OS.Arch.CURRENT);
    }
    
    public static boolean useX64JavaInsteadX32Java() {
        if (OS.is(OS.WINDOWS) && OS.Arch.CURRENT.equals(OS.Arch.x32)) {
            final String s = OS.executeByTerminal("wmic os get osarchitecture");
            return s.contains("64");
        }
        return false;
    }
    
    public static void showCriticalProblem(final String message) {
        Alert.showErrorHtml("A critical error has occurred, ask for help <br> <a href='https://vk.me/tlauncher'> https://vk.me/tlauncher </a> or by mail <b> support@tlauncher.org </b><br><br>" + message, 500);
    }
    
    public static void showCriticalProblem(final Throwable e) {
        showCriticalProblem(getStringError(e));
        TLauncher.kill();
    }
    
    public static boolean hasCorrectJavaFX() {
        try {
            Class.forName("javafx.embed.swing.JFXPanel");
        }
        catch (Throwable e) {
            return false;
        }
        return true;
    }
    
    public static void fillGPUInfo(final Configuration con, final boolean wait) {
        try {
            if (OS.is(OS.WINDOWS)) {
                final Path dxdiag = Paths.get(MinecraftUtil.getWorkingDirectory().getAbsolutePath(), "logs", "tlauncher", "dxdiag.txt");
                final boolean dxdiagExist = Files.exists(dxdiag, new LinkOption[0]);
                if (!dxdiagExist || FileUtils.isFileOlder(dxdiag.toFile(), DateUtils.addDays(new Date(), -10))) {
                    final String command = String.format("dxdiag /whql:off /t %s", dxdiag.toString());
                    OS.executeByTerminal(command);
                }
                final Path path;
                String file;
                String[] params;
                String name;
                List<String> names;
                String gpu;
                AsyncThread.execute(() -> {
                    if (wait) {
                        U.sleepFor(15000L);
                    }
                    if (Files.exists(path, new LinkOption[0])) {
                        try {
                            file = FileUtil.readFile(path.toFile(), Charset.defaultCharset().name());
                            params = file.split(System.lineSeparator());
                            name = Arrays.stream(params).filter(e -> {
                                e = e.toLowerCase();
                                return e.contains("card name:") || e.contains("chip type:") || e.contains("display memory:");
                            }).map(s -> s.split(":")[1]).collect((Collector<? super Object, ?, String>)Collectors.joining(","));
                            if (StringUtils.isNotBlank((CharSequence)name)) {
                                con.set("gpu.info.full", name);
                            }
                            names = Arrays.stream(params).filter(e -> {
                                e = e.toLowerCase();
                                return e.contains("card name:");
                            }).map(s -> s.split(":")[1]).collect((Collector<? super Object, ?, List<String>>)Collectors.toList());
                            if (!names.isEmpty()) {
                                gpu = names.get(names.size() - 1).trim();
                                if (!gpu.equalsIgnoreCase("Intel(R) HD Graphics")) {
                                    con.set("gpu.info", gpu);
                                }
                            }
                        }
                        catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    }
                });
            }
            else if (OS.is(OS.LINUX)) {
                final String res = OS.executeByTerminal("lshw -C display");
                final String[] params2 = res.split(System.lineSeparator());
                final List<String> names2 = Arrays.stream(params2).filter(e -> e.contains("product:")).map(s -> s.split(":")[1]).collect((Collector<? super Object, ?, List<String>>)Collectors.toList());
                if (!names2.isEmpty()) {
                    con.set("gpu.info", names2.get(names2.size() - 1).trim());
                    con.set("gpu.info.full", names2.get(names2.size() - 1).trim());
                }
            }
            else if (OS.is(OS.OSX)) {
                final String res = OS.executeByTerminal("system_profiler SPDisplaysDataType");
                final List<String> names3 = Arrays.stream(res.split(System.lineSeparator())).filter(e -> e.toLowerCase().contains("chipset model:")).map(s -> s.split(":")[1]).collect((Collector<? super Object, ?, List<String>>)Collectors.toList());
                if (!names3.isEmpty()) {
                    con.set("gpu.info", names3.get(names3.size() - 1).trim());
                    con.set("gpu.info.full", names3.get(names3.size() - 1).trim());
                }
            }
        }
        catch (Throwable e3) {
            U.log(e3);
        }
    }
}
