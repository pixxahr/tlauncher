// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import java.util.Hashtable;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.geom.Rectangle2D;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.net.MalformedURLException;
import org.tlauncher.util.async.AsyncThread;
import java.net.URL;
import javax.swing.SpringLayout;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Container;
import java.util.Locale;
import org.tlauncher.tlauncher.rmo.TLauncher;
import java.awt.Point;
import java.awt.Component;
import javax.swing.JComponent;
import java.awt.Cursor;
import java.util.Enumeration;
import javax.swing.UIDefaults;
import javax.swing.plaf.FontUIResource;
import javax.swing.UIManager;
import java.lang.reflect.Method;
import java.awt.Toolkit;
import javax.swing.JFrame;
import java.awt.FontFormatException;
import java.io.IOException;
import org.launcher.resource.TlauncherResource;
import org.tlauncher.tlauncher.ui.images.ImageCache;
import java.util.Collections;
import java.awt.Image;
import java.util.List;
import java.awt.Font;
import org.tlauncher.util.swing.FontTL;
import java.util.Map;

public class SwingUtil
{
    private static Map<FontTL, Font> FONTS;
    private static final List<Image> favicons;
    private static final String methodName = "getApplication";
    private static final String className = "com.apple.eawt.Application";
    private static final String methodSetDocIconImage = "setDockIconImage";
    
    private static List<Image> getFavicons() {
        if (!SwingUtil.favicons.isEmpty()) {
            return Collections.unmodifiableList((List<? extends Image>)SwingUtil.favicons);
        }
        final int[] sizes = { 256, 128, 96, 64, 48, 32, 24, 16 };
        final StringBuilder loadedBuilder = new StringBuilder();
        for (final int i : sizes) {
            final Image image = ImageCache.getImage("fav" + i + ".png", false);
            if (image != null) {
                loadedBuilder.append(", ").append(i).append("px");
                SwingUtil.favicons.add(image);
            }
        }
        final String loaded = loadedBuilder.toString();
        if (loaded.isEmpty()) {
            log("No favicon is loaded.");
        }
        else {
            log("Favicons loaded:", loaded.substring(2));
        }
        return SwingUtil.favicons;
    }
    
    public static void init() throws IOException, FontFormatException {
        SwingUtil.FONTS.put(FontTL.ROBOTO_REGULAR, Font.createFont(0, TlauncherResource.getResource("Roboto-Regular.ttf").openStream()));
        SwingUtil.FONTS.put(FontTL.ROBOTO_BOLD, Font.createFont(0, TlauncherResource.getResource("Roboto-Bold.ttf").openStream()));
        SwingUtil.FONTS.put(FontTL.ROBOTO_MEDIUM, Font.createFont(0, TlauncherResource.getResource("Roboto-Medium.ttf").openStream()));
        SwingUtil.FONTS.put(FontTL.CALIBRI, Font.createFont(0, TlauncherResource.getResource("Calibri.ttf").openStream()));
        SwingUtil.FONTS.put(FontTL.CALIBRI_BOLD, Font.createFont(0, TlauncherResource.getResource("Calibri-Bold.ttf").openStream()));
    }
    
    public static void setFavicons(final JFrame frame) {
        if (OS.is(OS.OSX)) {
            try {
                final Image image = Toolkit.getDefaultToolkit().getImage(ImageCache.getRes("fav256.png"));
                final Class app = Class.forName("com.apple.eawt.Application");
                Method method = app.getMethod("getApplication", (Class[])new Class[0]);
                final Object instanceApplication = method.invoke(null, new Object[0]);
                method = instanceApplication.getClass().getMethod("setDockIconImage", Image.class);
                method.invoke(instanceApplication, image);
            }
            catch (Exception e) {
                U.log("couldn't set a favicon for mac os platform", e);
            }
        }
        else {
            frame.setIconImages(getFavicons());
        }
    }
    
    public static void initFont(final int defSize) {
        try {
            final UIDefaults defaults = UIManager.getDefaults();
            final int maxSize = defSize + 2;
            final Enumeration<?> e = ((Hashtable<?, V>)defaults).keys();
            while (e.hasMoreElements()) {
                final Object key = e.nextElement();
                final Object value = defaults.get(key);
                if (value instanceof Font) {
                    final Font font = (Font)value;
                    int size = font.getSize();
                    if (size < defSize) {
                        size = defSize;
                    }
                    else if (size > maxSize) {
                        size = maxSize;
                    }
                    if (value instanceof FontUIResource) {
                        defaults.put(key, new FontUIResource(font.getFamily(), font.getStyle(), size));
                    }
                    else {
                        defaults.put(key, new Font("Roboto", font.getStyle(), size));
                    }
                }
            }
        }
        catch (Exception e2) {
            log("Cannot change font sizes!", e2);
        }
    }
    
    public static Cursor getCursor(final int type) {
        try {
            return Cursor.getPredefinedCursor(type);
        }
        catch (IllegalArgumentException iaE) {
            iaE.printStackTrace();
            return null;
        }
    }
    
    public static void setFontSize(final JComponent comp, final float size) {
        comp.setFont(comp.getFont().deriveFont(size));
    }
    
    public static void setFontSize(final JComponent comp, final float size, final int type) {
        comp.setFont(comp.getFont().deriveFont(type, size));
    }
    
    public static Point getRelativeLocation(final Component parent, final Component comp) {
        final Point compLocation = comp.getLocationOnScreen();
        final Point parentLocation = parent.getLocationOnScreen();
        return new Point(compLocation.x - parentLocation.x, compLocation.y - parentLocation.y);
    }
    
    private static void log(final Object... o) {
        U.log("[Swing]", o);
    }
    
    public static void changeFontFamily(final JComponent component, final FontTL family, final int size) {
        if (TLauncher.getInstance().getConfiguration().getLocale().getLanguage().equals(new Locale("zh").getLanguage())) {
            component.setFont(component.getFont().deriveFont((float)size));
            return;
        }
        component.setFont(SwingUtil.FONTS.get(family).deriveFont((float)size));
    }
    
    public static void changeFontFamily(final Component component, final FontTL family) {
        Font f = component.getFont();
        if (f != null) {
            f = SwingUtil.FONTS.get(family).deriveFont(f.getStyle(), (float)f.getSize());
            component.setFont(f);
        }
        if (component instanceof Container) {
            for (final Component child : ((Container)component).getComponents()) {
                changeFontFamily(child, family);
            }
        }
    }
    
    public static void changeFontFamily(final JComponent component, final FontTL family, final int size, final Color foreground) {
        changeFontFamily(component, family, size);
        component.setForeground(foreground);
    }
    
    public static void setImageJLabel(final JLabel label, final String url) {
        setImageJLabel(label, url, null);
    }
    
    public static void configHorizontalSpingLayout(final SpringLayout spring, final JComponent target, final JComponent coordinated, final int width) {
        spring.putConstraint("West", target, 0, "East", coordinated);
        spring.putConstraint("East", target, width, "East", coordinated);
        spring.putConstraint("North", target, 0, "North", coordinated);
        spring.putConstraint("South", target, 0, "South", coordinated);
    }
    
    public static void setImageJLabel(final JLabel label, final String url, final Dimension dimension) {
        try {
            final URL link = new URL(url);
            if (!ImageCache.setLocalIcon(label, url)) {
                final URL link2;
                AsyncThread.execute(() -> {
                    try {
                        setIcon(label, link2, dimension);
                        label.repaint();
                    }
                    catch (RuntimeException e) {
                        U.log(e);
                    }
                });
            }
            else {
                setIcon(label, link, dimension);
            }
        }
        catch (MalformedURLException e2) {
            log(e2);
        }
    }
    
    private static void setIcon(final JLabel label, final URL link, final Dimension dimension) {
        final BufferedImage bufferedImage = ImageCache.loadImage(link);
        if (dimension != null) {
            label.setIcon(new ImageIcon(bufferedImage.getScaledInstance(dimension.width, dimension.height, 4)));
        }
        else {
            label.setIcon(new ImageIcon(bufferedImage));
        }
    }
    
    public static int getWidthText(final JComponent c, final String text) {
        final AffineTransform affinetransform = new AffineTransform();
        final FontRenderContext frc = new FontRenderContext(affinetransform, true, true);
        return (int)c.getFont().getStringBounds(text, frc).getWidth();
    }
    
    public static void paintShadowLine(final Rectangle rec, final Graphics g, int i, final int max) {
        if (i < 0) {
            i = 0;
        }
        int y = rec.y;
        int current = 0;
        final Graphics2D g2 = (Graphics2D)g;
        while (y < rec.height + rec.y) {
            g2.setColor(new Color(i, i, i));
            if (current != max && i != 255) {
                ++current;
                ++i;
            }
            g2.drawLine(rec.x, y, rec.x + rec.width, y);
            ++y;
        }
    }
    
    public static void paintText(final Graphics2D g2d, final JComponent comp, final String text) {
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        final FontMetrics fm = g2d.getFontMetrics();
        final Rectangle2D r = fm.getStringBounds(text, g2d);
        g2d.setFont(comp.getFont());
        g2d.setColor(comp.getForeground());
        final int x = (comp.getWidth() - (int)r.getWidth()) / 2;
        final int y = (comp.getHeight() - (int)r.getHeight()) / 2 + fm.getAscent() - 1;
        g2d.drawString(text, x, y);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    }
    
    static {
        SwingUtil.FONTS = new HashMap<FontTL, Font>();
        favicons = new ArrayList<Image>();
    }
}
