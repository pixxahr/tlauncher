// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import java.util.Hashtable;
import java.util.Map;

public class Time
{
    private static Map<Object, Long> timers;
    
    public static void start(final Object holder) {
        if (Time.timers.containsKey(holder)) {
            throw new IllegalArgumentException("This holder (" + holder.toString() + ") is already in use!");
        }
        Time.timers.put(holder, System.currentTimeMillis());
    }
    
    public static long stop(final Object holder) {
        final long current = System.currentTimeMillis();
        final Long l = Time.timers.get(holder);
        if (l == null) {
            return 0L;
        }
        Time.timers.remove(holder);
        return current - l;
    }
    
    public static void start() {
        start(Thread.currentThread());
    }
    
    public static long stop() {
        return stop(Thread.currentThread());
    }
    
    static {
        Time.timers = new Hashtable<Object, Long>();
    }
}
