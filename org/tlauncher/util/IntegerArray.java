// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util;

import org.tlauncher.exceptions.ParseException;

public class IntegerArray
{
    public static final char defaultDelimiter = ';';
    private final int[] integers;
    private final char delimiter;
    private final int length;
    
    private IntegerArray(final char del, final int... values) {
        this.delimiter = del;
        this.length = values.length;
        System.arraycopy(values, 0, this.integers = new int[this.length], 0, this.length);
    }
    
    public IntegerArray(final int... values) {
        this(';', values);
    }
    
    public int get(final int pos) {
        if (pos < 0 || pos >= this.length) {
            throw new ArrayIndexOutOfBoundsException("Invalid position (" + pos + " / " + this.length + ")!");
        }
        return this.integers[pos];
    }
    
    public void set(final int pos, final int val) {
        if (pos < 0 || pos >= this.length) {
            throw new ArrayIndexOutOfBoundsException("Invalid position (" + pos + " / " + this.length + ")!");
        }
        this.integers[pos] = val;
    }
    
    public int size() {
        return this.length;
    }
    
    public int[] toArray() {
        final int[] r = new int[this.length];
        System.arraycopy(this.integers, 0, r, 0, this.length);
        return r;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (final int i : this.integers) {
            if (!first) {
                sb.append(this.delimiter);
            }
            else {
                first = false;
            }
            sb.append(i);
        }
        return sb.toString();
    }
    
    public static IntegerArray parseIntegerArray(final String val, final char del) throws ParseException {
        if (val == null) {
            throw new ParseException("String cannot be NULL!");
        }
        if (val.length() <= 1) {
            throw new ParseException("String mustn't equal or be less than delimiter!");
        }
        String regexp = "(?<!\\\\)";
        if (del != 'x') {
            regexp += "\\";
        }
        regexp += del;
        final String[] ints = val.split(regexp);
        final int l = ints.length;
        final int[] arr = new int[l];
        for (int i = 0; i < l; ++i) {
            int cur;
            try {
                cur = Integer.parseInt(ints[i]);
            }
            catch (NumberFormatException e) {
                U.log("Cannot parse integer (iteration: " + i + ")", e);
                throw new ParseException("Cannot parse integer (iteration: " + i + ")", e);
            }
            arr[i] = cur;
        }
        return new IntegerArray(del, arr);
    }
    
    public static IntegerArray parseIntegerArray(final String val) throws ParseException {
        return parseIntegerArray(val, ';');
    }
    
    private static int[] toArray(final String val, final char del) throws ParseException {
        final IntegerArray arr = parseIntegerArray(val, del);
        return arr.toArray();
    }
    
    public static int[] toArray(final String val) throws ParseException {
        return toArray(val, ';');
    }
}
