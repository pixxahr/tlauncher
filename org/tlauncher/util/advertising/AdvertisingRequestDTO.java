// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.advertising;

import javax.xml.bind.annotation.XmlElement;

public class AdvertisingRequestDTO
{
    @XmlElement
    private String accessToken;
    @XmlElement
    private String clientToken;
    
    public String getAccessToken() {
        return this.accessToken;
    }
    
    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }
    
    public String getClientToken() {
        return this.clientToken;
    }
    
    public void setClientToken(final String clientToken) {
        this.clientToken = clientToken;
    }
}
