// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.advertising;

import java.util.Iterator;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.net.MalformedURLException;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;
import org.tlauncher.skin.domain.responce.AdvertisingResponseDTO;
import net.minecraft.launcher.Http;
import java.net.URL;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonObject;
import org.tlauncher.util.U;
import org.tlauncher.tlauncher.minecraft.auth.Account;
import org.tlauncher.tlauncher.rmo.TLauncher;
import org.tlauncher.skin.domain.AdvertisingDTO;
import java.util.ArrayList;
import org.tlauncher.tlauncher.managers.ProfileManager;
import java.util.List;

public class AdvertisingStatusObservable implements Runnable
{
    private List<AdvertisingStatusObserver> listeners;
    private ProfileManager manager;
    
    public AdvertisingStatusObservable() {
        this.listeners = new ArrayList<AdvertisingStatusObserver>();
    }
    
    @Override
    public void run() {
        this.log("started to get ad information");
        AdvertisingDTO dto = new AdvertisingDTO();
        try {
            TLauncher.getInstance();
            final String url = TLauncher.getInnerSettings().get("skin.server.advertising");
            for (final Account acc : this.manager.getAuthDatabase().getAccounts()) {
                if (acc.getType() == Account.AccountType.TLAUNCHER) {
                    U.debug(acc);
                    final JsonObject send = new JsonObject();
                    send.add("clientToken", new JsonPrimitive(this.manager.getClientToken().toString()));
                    send.add("accessToken", new JsonPrimitive((acc.getAccessToken() == null) ? "null" : acc.getAccessToken()));
                    final String res = Http.performPost(new URL(url), send.toString(), "application/json");
                    final AdvertisingResponseDTO advertisingResponseDTO = new Gson().fromJson(res, AdvertisingResponseDTO.class);
                    if (StringUtils.isNotBlank((CharSequence)advertisingResponseDTO.getError())) {
                        this.log(advertisingResponseDTO.getError() + " " + advertisingResponseDTO.getErrorMessage());
                    }
                    else {
                        acc.setAccessToken(advertisingResponseDTO.getAccessToken());
                        if (advertisingResponseDTO.getUser().isAccountStatus()) {
                            acc.setPremiumAccount(true);
                            dto = advertisingResponseDTO.getAdvertising();
                            break;
                        }
                        continue;
                    }
                }
            }
        }
        catch (MalformedURLException e) {
            this.log(e);
        }
        catch (IOException | JsonSyntaxException ex2) {
            final Exception ex;
            final Exception e2 = ex;
            this.log(e2);
        }
        this.log("finished to get add information");
        this.notifyObserver(dto);
    }
    
    public void addListeners(final AdvertisingStatusObserver advertisingStatusListerner) {
        this.listeners.add(advertisingStatusListerner);
    }
    
    public void removeListener(final AdvertisingStatusObserver advertisingStatusListerner) {
        this.listeners.remove(advertisingStatusListerner);
    }
    
    public ProfileManager getManager() {
        return this.manager;
    }
    
    public void setManager(final ProfileManager manager) {
        this.manager = manager;
    }
    
    public void notifyObserver(final AdvertisingDTO advertisingDTO) {
        for (final AdvertisingStatusObserver advertisingStatusObserver : this.listeners) {
            advertisingStatusObserver.advertisingReceived(advertisingDTO);
        }
    }
    
    private void log(final Object o) {
        U.log("[AdvertisingStatusObserver] ", o);
    }
}
