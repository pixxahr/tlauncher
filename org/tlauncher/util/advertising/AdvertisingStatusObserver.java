// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.util.advertising;

import org.tlauncher.skin.domain.AdvertisingDTO;

public interface AdvertisingStatusObserver
{
    void advertisingReceived(final AdvertisingDTO p0);
}
