// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.exceptions;

public class ParseException extends RuntimeException
{
    private static final long serialVersionUID = -3231272464953548141L;
    
    public ParseException(final String string) {
        super(string);
    }
    
    public ParseException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public ParseException(final Throwable cause) {
        super(cause);
    }
}
