// 
// Decompiled by Procyon v0.5.36
// 

package org.tlauncher.exceptions;

public class ParseModPackException extends Exception
{
    public ParseModPackException(final String message) {
        super(message);
    }
    
    public ParseModPackException(final Exception ex) {
        super(ex);
    }
}
