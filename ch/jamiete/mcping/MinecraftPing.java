// 
// Decompiled by Procyon v0.5.36
// 

package ch.jamiete.mcping;

import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Record;
import org.xbill.DNS.SRVRecord;
import org.xbill.DNS.Lookup;
import com.google.gson.Gson;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.io.IOException;

public class MinecraftPing
{
    public MinecraftPingReply getPing(final String hostname) throws IOException {
        return this.getPing(new MinecraftPingOptions().setHostname(hostname));
    }
    
    public MinecraftPingReply getPing(final MinecraftPingOptions options) throws IOException {
        MinecraftPingUtil.validate(options.getHostname(), "Hostname cannot be null.");
        MinecraftPingUtil.validate(options.getPort(), "Port cannot be null.");
        try (final Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(options.getHostname(), options.getPort()), options.getTimeout());
            final DataInputStream in = new DataInputStream(socket.getInputStream());
            final DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            final ByteArrayOutputStream handshake_bytes = new ByteArrayOutputStream();
            final DataOutputStream handshake = new DataOutputStream(handshake_bytes);
            handshake.writeByte(MinecraftPingUtil.PACKET_HANDSHAKE);
            MinecraftPingUtil.writeVarInt(handshake, MinecraftPingUtil.PROTOCOL_VERSION);
            MinecraftPingUtil.writeVarInt(handshake, options.getHostname().length());
            handshake.writeBytes(options.getHostname());
            handshake.writeShort(options.getPort());
            MinecraftPingUtil.writeVarInt(handshake, MinecraftPingUtil.STATUS_HANDSHAKE);
            MinecraftPingUtil.writeVarInt(out, handshake_bytes.size());
            out.write(handshake_bytes.toByteArray());
            out.writeByte(1);
            out.writeByte(MinecraftPingUtil.PACKET_STATUSREQUEST);
            MinecraftPingUtil.readVarInt(in);
            int id = MinecraftPingUtil.readVarInt(in);
            MinecraftPingUtil.io(id == -1, "Server prematurely ended stream.");
            MinecraftPingUtil.io(id != MinecraftPingUtil.PACKET_STATUSREQUEST, "Server returned invalid packet.");
            final int length = MinecraftPingUtil.readVarInt(in);
            MinecraftPingUtil.io(length == -1, "Server prematurely ended stream.");
            MinecraftPingUtil.io(length == 0, "Server returned unexpected value.");
            final byte[] data = new byte[length];
            in.readFully(data);
            final String json = new String(data, options.getCharset());
            out.writeByte(9);
            out.writeByte(MinecraftPingUtil.PACKET_PING);
            out.writeLong(System.currentTimeMillis());
            MinecraftPingUtil.readVarInt(in);
            id = MinecraftPingUtil.readVarInt(in);
            MinecraftPingUtil.io(id == -1, "Server prematurely ended stream.");
            MinecraftPingUtil.io(id != MinecraftPingUtil.PACKET_PING, "Server returned invalid packet.");
            handshake.close();
            handshake_bytes.close();
            out.close();
            in.close();
            return new Gson().fromJson(json, MinecraftPingReply.class);
        }
    }
    
    public void resolveDNS(final MinecraftPingOptions options) throws TextParseException {
        final String service = "_minecraft._tcp." + new InetSocketAddress(options.getHostname(), options.getPort()).getHostName();
        final Record[] records = new Lookup(service, 33).run();
        if (records != null) {
            for (final Record record : records) {
                final SRVRecord srv = (SRVRecord)record;
                final String hostname = srv.getTarget().toString();
                final int port = srv.getPort();
                options.setHostname(hostname);
                options.setPort(port);
            }
        }
    }
}
