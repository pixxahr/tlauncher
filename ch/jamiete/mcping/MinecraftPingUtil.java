// 
// Decompiled by Procyon v0.5.36
// 

package ch.jamiete.mcping;

import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class MinecraftPingUtil
{
    public static byte PACKET_HANDSHAKE;
    public static byte PACKET_STATUSREQUEST;
    public static byte PACKET_PING;
    public static int PROTOCOL_VERSION;
    public static int STATUS_HANDSHAKE;
    
    public static void validate(final Object o, final String m) {
        if (o == null) {
            throw new RuntimeException(m);
        }
    }
    
    public static void io(final boolean b, final String m) throws IOException {
        if (b) {
            throw new IOException(m);
        }
    }
    
    public static int readVarInt(final DataInputStream in) throws IOException {
        int i = 0;
        int j = 0;
        while (true) {
            final int k = in.readByte();
            i |= (k & 0x7F) << j++ * 7;
            if (j > 5) {
                throw new RuntimeException("VarInt too big");
            }
            if ((k & 0x80) != 0x80) {
                return i;
            }
        }
    }
    
    public static void writeVarInt(final DataOutputStream out, int paramInt) throws IOException {
        while ((paramInt & 0xFFFFFF80) != 0x0) {
            out.writeByte((paramInt & 0x7F) | 0x80);
            paramInt >>>= 7;
        }
        out.writeByte(paramInt);
    }
    
    static {
        MinecraftPingUtil.PACKET_HANDSHAKE = 0;
        MinecraftPingUtil.PACKET_STATUSREQUEST = 0;
        MinecraftPingUtil.PACKET_PING = 1;
        MinecraftPingUtil.PROTOCOL_VERSION = 4;
        MinecraftPingUtil.STATUS_HANDSHAKE = 1;
    }
}
