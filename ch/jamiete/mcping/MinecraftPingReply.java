// 
// Decompiled by Procyon v0.5.36
// 

package ch.jamiete.mcping;

import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.awt.image.BufferedImage;

public class MinecraftPingReply
{
    private Players players;
    private Version version;
    private String favicon;
    
    public Players getPlayers() {
        return this.players;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public BufferedImage getFavicon() throws IOException {
        final String part = this.favicon.split(",")[1];
        final byte[] imageByte = Base64.getDecoder().decode(part.replaceAll("\\n", "").getBytes(StandardCharsets.UTF_8));
        final ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        return ImageIO.read(bis);
    }
    
    public String getFaviconString() {
        return this.favicon;
    }
    
    public class Description
    {
        private String text;
        
        public String getText() {
            return this.text;
        }
    }
    
    public class Players
    {
        private int max;
        private int online;
        private List<Player> sample;
        
        public int getMax() {
            return this.max;
        }
        
        public int getOnline() {
            return this.online;
        }
        
        public List<Player> getSample() {
            return this.sample;
        }
    }
    
    public class Player
    {
        private String name;
        private String id;
        
        public String getName() {
            return this.name;
        }
        
        public String getId() {
            return this.id;
        }
    }
    
    public class Version
    {
        private String name;
        private int protocol;
        
        public String getName() {
            return this.name;
        }
        
        public int getProtocol() {
            return this.protocol;
        }
    }
}
