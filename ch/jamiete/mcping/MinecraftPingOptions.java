// 
// Decompiled by Procyon v0.5.36
// 

package ch.jamiete.mcping;

public class MinecraftPingOptions
{
    private String hostname;
    private int port;
    private int timeout;
    private String charset;
    
    public MinecraftPingOptions() {
        this.port = 25565;
        this.timeout = 2000;
        this.charset = "UTF-8";
    }
    
    public String getHostname() {
        return this.hostname;
    }
    
    public MinecraftPingOptions setHostname(final String hostname) {
        this.hostname = hostname;
        return this;
    }
    
    public int getPort() {
        return this.port;
    }
    
    public MinecraftPingOptions setPort(final int port) {
        this.port = port;
        return this;
    }
    
    public int getTimeout() {
        return this.timeout;
    }
    
    public MinecraftPingOptions setTimeout(final int timeout) {
        this.timeout = timeout;
        return this;
    }
    
    public String getCharset() {
        return this.charset;
    }
    
    public MinecraftPingOptions setCharset(final String charset) {
        this.charset = charset;
        return this;
    }
}
